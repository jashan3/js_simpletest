
/**
 * epub BDBSetting Instance
 */
var BDBSetting = BDBSetting || {}; // namespace

/**
 * Base class
 */
BDBSetting.Config = function(){
   const Store = function(){
       this.id = null
   }
   Store.prototype.constructor = Store;
   Store.prototype.load = function(){
     console.log(this)
   }
   Store.prototype.save = function(){
     console.log(this)
   }

   this.store = new Store();
}

BDBSetting.Config.prototype.constructor = BDBSetting.Config;

/**
 * Style class
 */
BDBSetting.Style = function(){
   BDBSetting.Config.call(this)
   const FontFamaily = function(){
    this.default = 0;
    this.current = 0;
   }
   const FontSize = function(){
    this.default = 0;
    this.current = 0;
   }
   const LineHeight = function(){
    this.default = 0;
    this.current = 0;
   }
   const TBMargin = function(){
    this.default = 0;
    this.current = 0;
   }
   const Indent = function(){
    this.default = 0;
    this.current = 0;
   }
   const Paragragh = function(){
    this.default = 0;
    this.current = 0;
   }
   const TextAlign = function(){
    this.default = 0;
    this.current = 0;
   }

   this.fontFamaily = new FontFamaily();
   this.fontSize = new FontSize();
   this.lineHeight = new LineHeight();
   this.tbmargin = new TBMargin();
   this.indent = new Indent();
   this.paragragh = new Paragragh();
   this.textAlign = new TextAlign();
}
BDBSetting.Style.prototype = Object.create(BDBSetting.Config.prototype);
BDBSetting.Style.prototype.constructor = BDBSetting.Style;


BDBSetting.Style.prototype.reset = function(){
    this.fontFamaily.current = this.fontFamaily.default
    this.fontSize.current = this.fontSize.default
    this.lineHeight.current = this.lineHeight.default
    this.tbmargin.current = this.tbmargin.default
    this.indent.current = this.indent.default
    this.paragragh.current = this.paragragh.default
    this.textAlign.current = this.textAlign.default
}


BDBSetting.View = function(){
    const Single = function(){

    }
    const Dual = function(){
        
    }
    const Scroll = function(){
        
    }
    this.single = new Single()
    this.dual = new Dual()
    this.scroll = new Scroll()
}

BDBSetting.Theme = function(){
    
}

function BDBSettingStore(){
    this.style = new BDBSetting.Style();
    this.viewMode = new BDBSetting.View();
    this.theme = new BDBSetting.Theme();
}

BDBSettingStore.prototype.load = function(){
    this.style.store.load()
    // this.viewMode.store.load()
    // this.theme.store.load()
}
BDBSettingStore.prototype.save = function(){

}


let settingStore = new BDBSettingStore()
settingStore.load()
