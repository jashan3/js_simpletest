
/**
 * id: 보낸아이디
 * data: 파싱할 데이터
 */
onmessage = ({data})=>{
    // console.log(data)
    if (!data){
        postMessage(null);
        return;
    }
    if (!data['id'] || !data['path']){
        postMessage(null);
        return;
    }
    let file = '../'+data.path
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function (){
        if(rawFile.readyState === 4){
            if(rawFile.status === 200 || rawFile.status == 0){
                postMessage({id:data.id, data: rawFile.responseText});
               return
            }
        }
        postMessage(null);
    }
    rawFile.send(null); 
};

// const getRects = (startNode,startPos,endNode,endPos)=>{
//     let range = document.createRange();
//     range.setStart(startNode ,startPos);
//     range.setEnd(endNode, endPos);
//     console.log('getRects',range)
//     return range.getClientRects();
// }


// const getRectsFromSelection = ()=>{
//     let w = window.getSelection()
//     if (!w){
//         return null
//     }
//     let range = w.getRangeAt(0)
//     if (!range){
//         return null
//     }

//     return range.getClientRects()
// }


// let range = document.createRange();
// range.setStart(s.anchorNode, s.anchorOffset);
// range.setEnd(s.focusNode,s.focusOffset)