/**
 * 이캐시 팝업 viewModel
 */
const EcashPopupViewModel = (function(){
    var Instance = {}

    /**
     * 상단
     */
    let title = null
    const titleBtnSrc = ''

    /**
     * 중단
     */
    let contents = null
    let contentsMsg = null
    let contentsLeft = null 
    let contentsRight = null

    /**
     * 하단
     */
    let footerVisible = false
    let recVisible = false
    let recPopupVisible = false
    let recType = 1
    let recData = null

    /**
     * Util Event publish
     * @param {*} type 
     * @param {*} data 
     * @returns 
     */
    const __publish_key = 'EcashPopupViewModel'
    const __publish = (data)=> {
        window.dispatchEvent(new CustomEvent(__publish_key,{ detail: data })) 
    }
    const addEventListener = Instance.addEventListener = (callback)=>{ 
        window.addEventListener(__publish_key, callback)
    }


    const KEYS = Instance.KEYS = {
        title: 'title',
        contents: 'contents',
        contentsMsg: 'contentsMsg',
        contentsLeft: 'contentsLeft',
        contentsRight: 'contentsRight',
        footerVisible: 'footerVisible',
        recVisible: 'recVisible',
        recPopupVisible: 'recPopupVisible',
        recType: 'recType',
        recData: 'recData',
    }


    /**
     * 상단
     */
    const Header = Instance.Header = {

        setTitle(value){
            title = value
            __publish({'title':value })
        },
        getTitle(){
            return title
        },
        
        getTitleBtnSrc:()=>titleBtnSrc
    }

    /**
     * 본문
     */
    const Contents = Instance.Contents ={
        /**
         * 본문 내용
         * @returns 
         */
        getContents(){
            return contents
        },
        setContents(value){
            contents = value
            __publish({'contents':value })
        },
        /**
         * 본문 내용 추가 메시지
         * @returns 
         */
        getContentsMsg(){
            return contentsMsg
        },
        setContentsMsg(value){
            contentsMsg = value
            __publish({'contentsMsg':value })
        },
        /**
         * 왼쪽버튼
         * @returns 
         */
        getContentsLeft(){
            return contentsLeft
        }, 
        setContentsLeft(value){
            contentsLeft = value
            __publish({'contentsLeft':value })
        }, 
        /**
         * 오른쪽버튼
         * @returns 
         */
        getContentsRight(){
            return contentsRight
        },
        setContentsRight(value){
            contentsRight = value
            __publish({'contentsRight':value })
        },
    }

    /**
     * 하단
     */
    const Footer = Instance.Footer = {
        /**
         * footer visible
         */
        getFooterVisible(){ },
        setFooterVisible(value){
            footerVisible = value
            __publish({'footerVisible':value })
        },
        /**
         * 영수증 visible
         */
        getRecVisible(){ },
        setRecVisible(value){
            recVisible = value
            __publish({'recVisible':value })
        },
        /**
         * 안내팝업 visible
         */
        getRecPopupVisible(){},
        setRecPopupVisible(value){
            recPopupVisible = value
            __publish({'recPopupVisible':value })
        },
        /**
         * 현금영수증 발급타입
         */
        getRecType(){},
        setRecType(value){
            recType = value
            __publish({'recType':value })
        },
        /**
         * 현금영수증 데이터
         */
        getRecData(){ },
        setRecData(value){
            recData = value
            __publish({'recData':value })
        },
    }

    return Instance
})();