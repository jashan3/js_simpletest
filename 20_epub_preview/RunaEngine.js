
///////////////////////////////////////////////////////////////////////
//  Created by eric.c
//  this is the history of engine
//
//  modified by eric.c at 07.10.2019
//  - delete engine type for android
//
///////////////////////////////////////////////////////////////////////

var pickerX = 0, pickerY = 0;
var checkDpi = 1;
var currentPage = -1;
var arrImgsRect = null;
var g_computedHeight = null;
var g_computedWidth = null;
///////////////////////////////////////////////////////////////////////
// [ Desc   ] 페이지 split을 진행 한후 TOC 위치 및 annotation 위치값을 변환 또는 그리는 역활
// width : 페이지 가로 크기
// height : 페이지 높이
// isTablet : 태블릿 여부(안드로이드 전용)
// fileindex : 파일 순서(index)
// dpi : 디바이스 해상도
// toclist : toc list(json)
// pagelist : page-toc list(json)
// annotationlist : list(json)
// postype : (마지막 위치 읽은 값, 0: 없음, 1 : cfi, 2 : xpath, 3: anchor, 4: page)
// pos : 마지막 읽은 위치
// posoffset : 마지막 읽은 위치 offset(xpath 전용)
// margin : 양면페이지로 나눌때의 왼쪽 마진 값
// scroll : 세로 스크롤 여부 ( 1: enable, 0: disable)
// useAccessbility : 0 [ AVDTTS 초기화 하지 않음], 1 [AVDTTS 초기화]
function bdb_page_init(width, height, isTablet, orientation, fileindex, dpi, toclist, annotaionlist, postype, pos, posoffset, margin, scroll, useAccessbility) {
    var rtnVal = '';
    var curPage = "", curPos = "";
    var curCfi = "";
    var curXPath = "";
    var curXPathOffset = 0;
    var total = 0;
    var annos = null;
    var tocs = null;
    var pageTocs = null;
    var i = 0;
    var resConv = null;


    try {
        checkDpi = dpi;

        if (scroll == 1) {
            RunaEngine.DeviceInfo.setVscrollModeEnable(true);
        }
        else {
            RunaEngine.DeviceInfo.setVscrollModeEnable(false);
        }

        //page 구성을 한다.
        total = RunaEngine.HtmlCtrl.InitPage(isTablet, orientation, width, height, 20, fileindex, dpi);

        rtnVal += "[ {\"total\" :" + total + "}";

        if (total > 0) {

            //calculate toc's page
            rtnVal += ", {\"tocs\" :[ ";

            tocs = eval(toclist);

            if (typeof tocs === 'object' && Object.prototype.toString.call(tocs) == '[object Array]') {

                for (i = 0; i < tocs.length; i++) {
                    //
                    curPage = RunaEngine.HtmlCtrl.PageNavi.GetAnchorPageNumber(tocs[i].anchor);

                    if (i > 0)
                        rtnVal += ',';
                    rtnVal += "{\"id\" : \"" + tocs[i].id + "\", \"page\" : \"" + curPage + "\"}";
                }
            }

            rtnVal += "]}";

            //convert annotations
            rtnVal += ", {\"annos\" : ";

            rtnVal += bdb_getAnnotationJson(annotaionlist);

            rtnVal += "}";
            // Media
            rtnVal += ", {\"media\" : ";
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                rtnVal += RunaEngine.HtmlCtrl.GetMultimediaList();
            }
            rtnVal += "}";

            //anchor 텍스트 추출
            rtnVal += ", {\"anchors\" :[ ";
            // 안드로이드 앵커는 파일 추가할때 계산해서 저장해둔다  isPC 체크에 0.1초 이상 걸릴때가 있어 처리
            //rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetAnchorsText();
            rtnVal += "]}";

            //image 추출
            rtnVal += ", {\"imglist\" :[ ";
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetImageList();
            }

            rtnVal += "]}";

            //table-data 추출
            rtnVal += ", {\"tablelist\" :[ ";
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetTableList();
            }

            rtnVal += "]}";

            //Math 텍스트 추출
            rtnVal += ", {\"mathlist\" :[ ";
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetMathList();
            }

            rtnVal += "]}";

            rtnVal += ", {\"paperpagelist\" :[ ";
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetPaperPageList();
            }

            rtnVal += "]}";

            rtnVal += ", {\"paragraphlist\" :[ ";
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetParagraphList();
            }

            rtnVal += "]}";

            // 마지막 읽은 변환
            if (postype == 0) {
                //xpath
                resConv = RunaEngine.HtmlCtrl.SearchControl._XPathToCfi2(pos, posoffset);

                if (resConv == null) {
                    curPage = RunaEngine.HtmlCtrl.PageNavi.MoveFirstPage();
                    curPos = RunaEngine.HtmlCtrl.PageInfo.GetLastReadPageCfi(0);

                    curCfi = curPos[0];
                    curXPath = curPos[1];
                    curXPathOffset = curPos[2];

                    rtnVal += ", {\"xpathtocfi\" :\"-1\"}";
                }
                else {

                    curPage = RunaEngine.HtmlCtrl.PageNavi.MoveCfiPage(resConv.start);
                    curPos = RunaEngine.HtmlCtrl.PageInfo.GetLastReadPageCfi(0);

                    curCfi = curPos[0];
                    curXPath = curPos[1];
                    curXPathOffset = curPos[2];
                    rtnVal += ", {\"xpathtocfi\" :\"0\", \"cfi\" : \"" + resConv.start + "\"}";
                }
            }

            rtnVal += "]";

            if (useAccessbility == 1) {
                RunaEngine.HtmlCtrl.AVTTSEngine.initialize(0);
            }
        }
        else {
            //페이지 구성이 안됐을때
            rtnVal += "]";
        }

        return rtnVal;
    }
    catch (ex) {
        //RunaEngine.log('[★Exception★][bdb_page_init] ' + (ex.number & 0xFFFF));
        RunaEngine.log('[★Exception★][bdb_page_init] ' + ex);

        rtnVal += "[ {\"total\" :" + total + "} ]";

        return rtnVal;
    }
    finally {
        rtnVal = null;
        curPage = null;
        curPos = null;
        curXPathOffset = null;
        total = null;
        annos = null;
        tocs = null;
        i = null;
    }
}

///////////////////////////////////////////////////////////////////////
// [ Desc   ] 고정형 레이아웃 페이지 초기화
function bdb_page_fixed_init(width, height, isTablet, orientation, fileindex, dpi, toclist, annotaionlist, type, postype, pos, posoffset, margin) {
    var rtnVal = '';
    var total = 0;
    var annos = null;
    var tocs = null;
    var i = 0;
    var resConv = null;
    var viewport = null;

    try {

        checkTablet = isTablet;
        checkDpi = dpi;
        /**
         * 2022.04.13 hack
         * InitPageFixed 에 실제 웹뷰의 width와 height를 넣어준다
         */
        //viewport data
        viewport = RunaEngine.HtmlCtrl.GetViewPort();
        //page 구성을 한다.
        if(viewport != null) {
            total = RunaEngine.HtmlCtrl.InitPageFixed(isTablet, orientation, viewport["width"], viewport["height"], null, fileindex, dpi);
        }else {
            // 실제 사이즈가 들어가지 않은경우
            // 북마크와 TTS에서 caretRangeFromPoint로 범위를 잡을수 없어서 북마크,TTS 기능사용 불가
            total = RunaEngine.HtmlCtrl.InitPageFixed(isTablet, orientation, width, height, null, fileindex, dpi);
        }
        // total = 1;

        rtnVal += "[ {\"total\" :" + total + "}";

        //calculate toc's page
        rtnVal += ", {\"tocs\" : [";

        tocs = eval(toclist);

        if (typeof tocs === 'object' && Object.prototype.toString.call(tocs) == '[object Array]') {
            for (i = 0; i < tocs.length; i++) {
                //
                curPage = 1;

                if (i > 0)
                    rtnVal += ',';
                rtnVal += "{\"id\" : \"" + tocs[i].id + "\", \"page\" : \"" + curPage + "\"}";
            }
        }

        rtnVal += "]}";

        //convert annotations
        rtnVal += ", {\"annos\" : ";
        rtnVal += bdb_getAnnotationJson(annotaionlist);
        rtnVal += "}";

        //anchor 텍스트 추출
        rtnVal += ", {\"anchors\" : [";
        //rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetAnchorsText();
        rtnVal += "]}";
        // //viewport data
        // viewport = RunaEngine.HtmlCtrl.GetViewPort();
        if (viewport != null) {
            rtnVal += ", {\"viewport\" :\"1\", \"width\" :\"" + viewport["width"] + "\", \"height\" :\"" + viewport["height"] + "\", \"minimum-sacle\" :\"" + viewport["minimum-sacle"] + "\", \"maximum-sacle\" :\"" + viewport["maximum-sacle"] + "\", \"initial-scale\" : \"" + viewport["initial-scale"] + "\", \"user-scalable\" : \"" + viewport["user-scalable"] + "\"}";
        }
        else {
            rtnVal += ", {\"viewport\" :\"0\", \"width\" :\"0\", \"height\" :\"0\", \"minimum-sacle\" :\"0\", \"maximum-sacle\" :\"0\", \"initial-scale\" : \"0\", \"user-scalable\" : \"0\"}";
        }

        // image 추출
        rtnVal += ", { \"imglist\" :[ ";
        if(RunaEngine.DeviceInfo.IsAndroid()) {
            rtnVal += RunaEngine.HtmlCtrl.PageNavi.GetImageList();
        }
        rtnVal += "]}";

        rtnVal += "]";
        // TTS data
        RunaEngine.HtmlCtrl.AVTTSEngine.initialize(0);

        return rtnVal;

    }
    catch (ex) {
        //RunaEngine.log('[★Exception★][bdb_page_init] ' + (ex.number & 0xFFFF));
        //RunaEngine.log('[★Exception★][bdb_page_fixed_init] ' + ex);

        rtnVal += "[ {\"total\" :" + total + "} ]";

        return rtnVal;

    }
    finally {
        rtnVal = null;
        curPage = null;
        curPos = null;
        curXPathOffset = null;
        total = null;
        annos = null;
        tocs = null;
        i = null;
    }
}

///////////////////////////////////////////////////////////////////////
// [ Desc   ] ios 용 페이지 split
// vscorll을 지원하기 위한 init 함수
// width : 페이지 가로 크기, height : 페이지 높이
// fileindex : 파일 순서(index)
// vscrollmode : 세로 스크롤 모드 (disable : 0 enable : 1)
function bdb_page_init2_ios(orientation, width, height, columnpadding, fileindex, vscollmode) {
    if (vscollmode == 1) {
        RunaEngine.DeviceInfo.setVscrollModeEnable(true);
    } else {
        RunaEngine.DeviceInfo.setVscrollModeEnable(false);
    }
    total = RunaEngine.HtmlCtrl.InitPage(false, orientation, width, height, columnpadding, fileindex, 1);
    return total;
}
///////////////////////////////////////////////////////////////////////
// [ Desc   ] ios 용 고정형 레이아웃 페이지 초기화
// fixed를 지원하기 위한 init 함수
// width : 페이지 가로 크기, height : 페이지 높이
// fileindex : 파일 순서(index)
// vscrollmode : 세로 스크롤 모드 (disable : 0 enable : 1)
function bdb_page_fixed_init2_ios(orientation, width, height, columnpadding, fileindex, vscollmode) {
    if (vscollmode == 1) {
        RunaEngine.DeviceInfo.setVscrollModeEnable(true);
    } else {
        RunaEngine.DeviceInfo.setVscrollModeEnable(false);
    }

    total = RunaEngine.HtmlCtrl.InitPageFixed(false, orientation, width, height, columnpadding, fileindex, 1);
    return total;
}
///////////////////////////////////////////////////////////////////////
// [ Desc   ] 고정형 레이아웃의 뷰포트 정보를 리턴한다.
function get_fixed_viewport() {
    var viewPort = RunaEngine.HtmlCtrl.GetViewPort();
    if (viewPort != null) {
        var vWidth = viewPort["width"];
        var vHeight = viewPort["height"];
        rtnVal = "{\"width\" : \"" + vWidth + "\" , \"height\" : \"" + vHeight + "\"}";

        return rtnVal;
    }
    else {
        return 1;
    }
}



function bdb_getAnnotationJson(annotaionlist) {
    var annos = eval(annotaionlist);

    var pageWidth = RunaEngine.HtmlCtrl.getPageWidth();
    var pageHeight = RunaEngine.HtmlCtrl.getPageHeight();

    var rtnVal = new Array();
    if (typeof annos === 'object' && Object.prototype.toString.call(annos) == '[object Array]') {
        for (var i = 0; i < annos.length; i++) {
            //ver == 0 xpath -> cfi
            if (parseInt(annos[i].ver) == 0) {
                if (parseInt(annos[i].type) == 1) {
                    resConv = RunaEngine.HtmlCtrl.SearchControl._XPathToCfi2(annos[i].startxpath, annos[i].startoffset);
                }
                else {
                    resConv = RunaEngine.HtmlCtrl.SearchControl._XPathToCfi(annos[i].startxpath, annos[i].startoffset, annos[i].endxpath, annos[i].endoffset);
                }

                if (resConv == null) {
                    var json = new Object();
                    json["id"] = annos[i].id;
                    json["type"] = 0;
                    json["res"] = -1;
                    json["page"] = -1;
                    rtnVal[i] = json;
                }
                else {
                    if (parseInt(annos[i].type) == 1) {
                        //bookmark
                        var json = new Object();
                        json["id"] = annos[i].id;
                        json["type"] = 0;
                        json["res"] = 0;
                        json["page"] = RunaEngine.HtmlCtrl.PageInfo.GetCfiPageNumber(resConv.start);
                        json["start"] = resConv.start;
                        json["end"] = resConv.end;
                        json["text"] = resConv.text;
                        rtnVal[i] = json;
                    }
                    else {
                        //highlight
                        var res = RunaEngine.HtmlCtrl.Highlight.Create(resConv.start, resConv.end, annos[i].color);

                        var json = new Object();
                        json["id"] = annos[i].id;
                        json["type"] = 0;
                        json["res"] = res['res'];
                        json["pageWidth"] = pageWidth;
                        json["pageHeight"] = pageHeight;
                        json["start"] = resConv.start;
                        json["end"] = resConv.end;
                        json["selectedText"] = res['SelectionText'];

                        var startPage = 0;
                        var positions = new Array();
                        for (var j = 0; j < res.length; j++) {
                            var pJson = new Object();

                            pJson["left"] = res[j]['left'];
                            pJson["top"] = res[j]['top'];
                            pJson["width"] = res[j]['width'];
                            pJson["height"] = res[j]['height'];

                            if (RunaEngine.DeviceInfo.IsVScrollMode() == false) {
                                var left = parseInt(res[j]['left'], 10);
                                var pageNo = parseInt(left / pageWidth) + 1;

                                if (j == 0) {
                                    startPage = pageNo;
                                }
                                else {
                                    if (pageNo < startPage) {
                                        startPage = pageNo;
                                    }
                                }

                                pJson["pageNo"] = pageNo;
                            }
                            else {
                                var top = parseInt(res[j]['top'], 10);
                                var pageNo = parseInt(top / pageHeight) + 1;

                                if (j == 0) {
                                    startPage = pageNo;
                                }
                                else {
                                    if (pageNo < startPage) {
                                        startPage = pageNo;
                                    }
                                }

                                pJson["pageNo"] = pageNo;

                            }

                            positions.push(pJson);
                        }

                        json["page"] = startPage;
                        json["positions"] = positions;

                        rtnVal[i] = json;
                    }
                }
            }
            else {
                if (parseInt(annos[i].type) == 1) {
                    //bookmark
                    var json = new Object();
                    json["id"] = annos[i].id;
                    json["type"] = 1;
                    json["res"] = 0;
                    json["page"] = RunaEngine.HtmlCtrl.PageInfo.GetCfiPageNumber(annos[i].start);
                    json["start"] = annos[i].start;
                    json["end"] = annos[i].end;
                    rtnVal[i] = json;
                }
                else {
                    //highlight
                    var res = RunaEngine.HtmlCtrl.Highlight.Create(annos[i].start, annos[i].end, annos[i].color);

                    var json = new Object();
                    json["id"] = annos[i].id;
                    json["type"] = 0;
                    json["res"] = res['res'];
                    json["pageWidth"] = pageWidth;
                    json["pageHeight"] = pageHeight;
                    json["start"] = annos[i].start;
                    json["end"] = annos[i].end;
                    json["selectedText"] = res['SelectionText'];

                    var startPage = 0;
                    var positions = new Array();
                    for (var j = 0; j < res.length; j++) {

                        var pJson = new Object();
                        pJson["left"] = res[j]['left'];
                        pJson["top"] = res[j]['top'];
                        pJson["width"] = res[j]['width'];
                        pJson["height"] = res[j]['height'];
                        pJson["mode"] = RunaEngine.DeviceInfo.IsVScrollMode();

                        if (RunaEngine.DeviceInfo.IsVScrollMode() == false) {
                            var left = parseInt(res[j]['left'], 10);
                            var pageNo = parseInt(left / pageWidth) + 1;

                            if (j == 0) {
                                startPage = pageNo;
                            }
                            else {
                                if (pageNo < startPage) {
                                    startPage = pageNo;
                                }
                            }

                            pJson["pageNo"] = pageNo;
                        }
                        else {
                            var top = parseInt(res[j]['top'], 10);
                            var pageNo = parseInt(top / pageHeight) + 1;

                            if (j == 0) {
                                startPage = pageNo;
                            }
                            else {
                                if (pageNo < startPage) {
                                    startPage = pageNo;
                                }
                            }

                            pJson["pageNo"] = pageNo;

                        }

                        positions.push(pJson);
                    }

                    json["page"] = startPage;
                    json["positions"] = positions;

                    rtnVal[i] = json;
                }
            }
        }
    }

    return JSON.stringify(rtnVal);
};

function bdb_recal_annotation(annotaionlist) {

    var rtnVal = "[{\"annos\" : ";
    var val = bdb_getAnnotationJson(annotaionlist);

    rtnVal += val;
    rtnVal += "}]";

    if (RunaEngine.DeviceInfo.isPC()) {
        rtnVal = "{\"annos\" : ";
        rtnVal += val;
        rtnVal += "}";

        return rtnVal;
    }
    else {
        return rtnVal;
    }
}



///////////////////////////////////////////////////////////////////////
// [ Desc   ] 안드로이드를 위한 스크립트 결과를 리턴한다.
function evalJsForAndroid(evalJs_index, jsString) {
    var evalJs_result = "";
    try {
        evalJs_result = ""+eval(jsString);
    } catch (e) {
        console.log(e);
    }
    window.android.processReturnValue(evalJs_index, evalJs_result);
}


///////////////////////////////////////////////////////////////////////
// [ Desc   ] 현재 페이지의 위치 정보를 리턴한다.
function bdb_getLastReadPageCfi(side) {
    var res = "", curPos = "";
    curPos = RunaEngine.HtmlCtrl.PageInfo.GetLastReadPageCfi(side);
    res = curPos[0] + ';' + curPos[1] + ';' + curPos[2];
    return res;
}
///////////////////////////////////////////////////////////////////////
// [ Desc   ] Convert xpath to cfi
function bdb_XPathToCfi(start, end, startOffset, endOffset) {

    try {
        var res = RunaEngine.HtmlCtrl.SearchControl._XPathToCfi(start, startOffset, end, endOffset);

        return res.start + ',' + res.end;
    }
    catch (ex) {
        RunaEngine.log(ex);
    }

    return '';

}
///////////////////////////////////////////////////////////////////////
// [ Desc   ] Convert xpath to cfi
function bdb_XPathToCfi2(start, startOffset) {

    var res = RunaEngine.HtmlCtrl.SearchControl._XPathToCfi2(start, startOffset);

    return res.start + ',' + res.end;
}

//[sjhan][210604]android 용 script
// 하이라이트 잡힌 left,top,w,h 좌표를 파라미터로 받으면
// 해당 블록의 가운데 좌표 탐색 후  p tag 안쪽 문자열 return
function bdb_element_from_position(left,top,W,H){

    //좌측 상단의 좌표가 ( 0, 0 )이라고 할때.
    var xx = left + ( W/2 )
    var yy = top + ( H/2 )
    // 해당좌표의 node 탐색
    var selnode = document.elementFromPoint(xx,yy)
    if (selnode){
       var selnodename = selnode.tagName.toLowerCase()
        var mmresult = ''
        if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
            return '';
        }
        mmresult = selnode.innerText;
        if (mmresult){
            return mmresult
        }
    }
    return '';
}

function bdb_tts_getSentence(index, color) {
    var sentenceInfo = RunaEngine.HtmlCtrl.TTSHandler.GetSentence(index, color);
    var id = "bdb_tts_selection";
    var res = RunaEngine.HtmlCtrl.Highlight.Create(sentenceInfo.start, sentenceInfo.end, color);

    var rtnVal = RunaEngine.HtmlCtrl.EventHandler.getDrawSelectionJson(id, res, color, sentenceInfo.sentence);
    return rtnVal
}

//ios 에서 사용 중임
function bdb_tts_getSentences(color) {
    var sentences = RunaEngine.HtmlCtrl.TTSHandler.GetSentences();

    if (sentences != null) {
        var result = "[";
        for (var i = 0; i < sentences.length; i++) {
            var info = bdb_tts_getSentence(i, color);
            result = result + info;
            if (i < sentences.length - 1) {
                result = result + ", ";
            }
        }
        result = result + "]";
        return result;
    }

    return '';
}

function bdb_getBookmarkInfo() {
    var val;
    var curCfi = "";
    var curXPath = "";
    var curXPathOffset = 0;
    var curPage = "";
    var headline = "";
    var bfind = false;
    var i = 0;

    val = RunaEngine.HtmlCtrl.PageInfo.GetLastReadPageCfiAndText_ReturnArray(0);
    curPage = RunaEngine.HtmlCtrl.PageInfo.GetCurrPageNo();
    curCfi = val[0][0];
    curXPath = val[0][1];
    curXPathOffset = val[0][2];
    headline = val[1];

    //21.03.04 text 내뇬 부분에 "" 가 없어서 에러나서 추가해줌
    var rtnVal = "{\"page\" : \"" + curPage + "\", \"cfi\" : \"" + curCfi + "\", \"xpath\" : \"" + curXPath + "\", \"offset\" : \"" + curXPathOffset + "\", \"text\": \""+ headline+ "\"}";

    return rtnVal;
}

// 안드로이드를 위한 세로형 북마크 함수
function bdb_getVSBookmarkInfo(offsetY) {
    var val;
    var curCfi = "";
    var curXPath = "";
    var curXPathOffset = 0;
    var curPage = "";
    var headline = "";
    var bfind = false;
    var i = 0;

    val = RunaEngine.HtmlCtrl.PageInfo.GetLastReadPageCfiAndText_VS_ReturnString(offsetY);
    curPage = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(offsetY);
    curCfi = val[0][0];
    curXPath = val[0][1];
    curXPathOffset = val[0][2];
    headline = val[1];

    //21.03.04 text 내뇬 부분에 "" 가 없어서 에러나서 추가해줌
    var rtnVal = "{\"page\" : \"" + curPage + "\", \"cfi\" : \"" + curCfi + "\", \"xpath\" : \"" + curXPath + "\", \"offset\" : \"" + curXPathOffset + "\", \"text\":  \""+ headline + "\"}";

    return rtnVal;
}
///////////////////////////////////////////////////////////////////////
// [ Desc   ] Create highlight
function bdb_add_highlight(id, start, end, color) {
    var res = RunaEngine.HtmlCtrl.Highlight.Create(start, end, color);
    var curPage;

    if (!RunaEngine.DeviceInfo.IsAndroid()) {
        RunaEngine.HtmlCtrl.Selection.DeleteSelection();
        curPage = RunaEngine.HtmlCtrl.PageInfo.GetCfiPageNumber(start);
    }

    var rtnVal = RunaEngine.HtmlCtrl.EventHandler.getDrawSelectionJson(id, res, color, res['SelectionText']);
    return rtnVal;
}


// 해당 cfi로 부터 cfi가 포함된 문단을 얻어오는 함수
function bdb_getParagraphByCfi(startCfi, endCfi) {
    var range = document.createRange();

    var startPoint = RunaEngine.HtmlCtrl.DPControl._pointFromCFI(startCfi);
    var endPoint = RunaEngine.HtmlCtrl.DPControl._pointFromCFI(endCfi);
    var start = startPoint.point;
    var end = endPoint.point;

    var paragraph = RunaEngine.HtmlCtrl.DPControl._GetText(startCfi, start.node, 0, endCfi, end.node, end.node.length);

    return paragraph;
}

/**
 * redmine refs #16619
 * 단어 단위 여부를 판단하도록 수정
 * by junghoon.kim
*/

function bdb_selection_start(x, y, wordBoundary) {
//    if (RunaEngine.DeviceInfo.IsAndroid()) {
//        x = x / RunaEngine.HtmlCtrl.getDevicePixelRatio();
//        y = y / RunaEngine.HtmlCtrl.getDevicePixelRatio();
//    }

    RunaEngine.HtmlCtrl.EventHandler.OnSelectionStart(x, y, wordBoundary);

//    if (engineType == 1) {
        var start, end, text, rect, startxpath, endxpath, startoffset = 0, endoffset = 0;
        start = RunaEngine.HtmlCtrl.EventHandler.getStartCfi();
        end = RunaEngine.HtmlCtrl.EventHandler.getEndCfi();
        text = RunaEngine.HtmlCtrl.EventHandler.getSelectionText();
        rect = RunaEngine.HtmlCtrl.EventHandler.getSelectionRect();
        startxpath = RunaEngine.HtmlCtrl.EventHandler.getStartXPath();
        endxpath = RunaEngine.HtmlCtrl.EventHandler.getEndXPath();
        startoffset = RunaEngine.HtmlCtrl.EventHandler.getStartXPathOffset();
        endoffset = RunaEngine.HtmlCtrl.EventHandler.getEndXPathOffset();

        if (text === '' || text === undefined) {
            RunaEngine.HtmlCtrl.Selection.DeleteSelection();
        }
        else {
            window.android.SetSelectionData(start, end, startxpath, startoffset, endxpath, endoffset, text, rect[0], rect[1], rect[2], rect[3]);
        }
//    }
}

function bdb_selection_start_front(x, y, wordBoundary) {
    RunaEngine.HtmlCtrl.EventHandler.OnSelectionReStartFront(x, y, wordBoundary);
}

function bdb_selection_start_back(x, y, wordBoundary) {
    RunaEngine.HtmlCtrl.EventHandler.OnSelectionReStartBack(x, y, wordBoundary);
}

function bdb_selection_move_front(x, y, wordBoundary) {
    RunaEngine.HtmlCtrl.EventHandler.OnSelectionMoveFront(x, y, wordBoundary);
}

function bdb_selection_move(x, y, wordBoundary) {
    RunaEngine.HtmlCtrl.EventHandler.OnSelectionMove(x, y, wordBoundary);
}

function bdb_selection_end(x, y, wordBoundary) {
    RunaEngine.HtmlCtrl.EventHandler.OnSelectionEnd(x, y, wordBoundary);
    var start, end, text, rect, startxpath, endxpath, startoffset = 0, endoffset = 0;
    start = RunaEngine.HtmlCtrl.EventHandler.getStartCfi();
    end = RunaEngine.HtmlCtrl.EventHandler.getEndCfi();
    text = RunaEngine.HtmlCtrl.EventHandler.getSelectionText();
    rect = RunaEngine.HtmlCtrl.EventHandler.getSelectionRect();
    startxpath = RunaEngine.HtmlCtrl.EventHandler.getStartXPath();
    endxpath = RunaEngine.HtmlCtrl.EventHandler.getEndXPath();
    startoffset = RunaEngine.HtmlCtrl.EventHandler.getStartXPathOffset();
    endoffset = RunaEngine.HtmlCtrl.EventHandler.getEndXPathOffset();

    if (text === '' || text === undefined) {
        RunaEngine.HtmlCtrl.Selection.DeleteSelection();
    }
    else {
        window.android.SetSelectionData(start, end, startxpath, startoffset, endxpath, endoffset, text, rect[0], rect[1], rect[2], rect[3]);
    }
}



function bdb_onFlingCheck(x, y, selection) {
    var res = RunaEngine.HtmlCtrl.PageInfo.GetHitTest(x, y);

    if (RunaEngine.DeviceInfo.IsAndroid()) {
        window.android.OnFlingMoveCheckResult(res, selection)
    }
}


function bdb_gethrefinfo(x, y) {
    var res = RunaEngine.HtmlCtrl.PageInfo.GetHrefInfo(x, y);

    if (RunaEngine.DeviceInfo.IsAndroid()) {
        window.android.GetHrefInfoResult(res, x, y);
    }
}



function bdb_getimginfo(x, y) {
    var res = RunaEngine.HtmlCtrl.PageInfo.GetImageSrc(x, y);

    if (RunaEngine.DeviceInfo.IsAndroid()) {
        window.android.GetHrefInfoResult(res, x, y);
    }
    else if (RunaEngine.DeviceInfo.isPC()) {
        if (res != null && res != undefined && res != '')
            RunaPC.OnSelectImage(res);
    }
}



var doubleOverTouchStartTimestamp = 0;

function touchStartHandler(event) {

    var touch = event.changedTouches[0];
    document.location = 'touchstart:' + touch.pageX + ',' + touch.pageY;
}

function touchEndHandler(event) {
    var touch = event.changedTouches[0];
    document.location = 'touchend:' + touch.pageX + ',' + touch.pageY;
}

function mouseWheelEventHandler(e) {
    e = e || window.event;

    if (RunaEngine.DeviceInfo.isPC()) {
        delta = 0;

        if (e.detail) {
            delta = e.detail * -40;
        }
        else {
            delta = e.wheelDelta;
        }
        RunaPC.OnMouseWheel(delta);
    }

    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}



RunaEngineMap = function () {
    this.map = new Object();
};
RunaEngineMap.prototype = {
    put: function (key, value) {
        this.map[key] = value;
    },
    get: function (key) {
        return this.map[key];
    },
    containsKey: function (key) {
        return key in this.map;
    },
    containsValue: function (value) {
        for (var prop in this.map) {
            if (this.map[prop] == value) return true;
        }
        return false;
    },
    isEmpty: function (key) {
        return (this.size() == 0);
    },
    clear: function () {
        for (var prop in this.map) {
            delete this.map[prop];
        }
    },
    remove: function (key) {
        delete this.map[key];
    },
    keys: function () {
        var keys = new Array();
        for (var prop in this.map) {
            keys.push(prop);
        }
        return keys;
    },
    values: function () {
        var values = new Array();
        for (var prop in this.map) {
            values.push(this.map[prop]);
        }
        return values;
    },
    size: function () {
        var count = 0;
        for (var prop in this.map) {
            count++;
        }
        return count;
    },
    toString: function () {
        var str = "{";
        var keys = this.keys();
        for (var i = 0; i < keys.length; i++) {
            if (i != 0) {
                str += ",";
            }
            str += "\"";
            str += keys[i];
            str += "\"";
            str += ":";
            if (this.get(keys[i])) {
                str += "\"";
                str += this.get(keys[i]);
                str += "\"";
            } else {
                str += "null";
            }
        }
        str += "}";
        return str;
    }
};

var RunaEngine = new function _RunaEngine() {

    var g_console_mode = null;
    var g_console_log = false;
    var g_console_sublog = false;
    var g_console_sublog2 = false;

    this.log = function (msg) {

        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
            if (g_console_log) {
                console.log(RunaEngine.FileInfo.getPageTitle() + '[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
            }
        }
        else {
            if (g_console_log) {
                var iframe = null;
                try {
                    iframe = document.createElement("IFRAME");
                    iframe.setAttribute("src", "ios-log:#iOS#" + RunaEngine.FileInfo.getPageTitle() + '[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
                    document.documentElement.appendChild(iframe);
                    iframe.parentNode.removeChild(iframe);
                    iframe = null;
                }
                finally {
                    if (iframe != null)
                        iframe = null;
                }
            }
        }
    }

    this.sublog = function (msg) {
        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
            if (g_console_sublog) {
                console.log(RunaEngine.FileInfo.getPageTitle() + '[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
            }
        }
        else {
            if (g_console_sublog) {
                var iframe = null;
                try {
                    iframe = document.createElement("IFRAME");
                    iframe.setAttribute("src", "ios-log:#iOS#" + RunaEngine.FileInfo.getPageTitle() + '[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
                    document.documentElement.appendChild(iframe);
                    iframe.parentNode.removeChild(iframe);
                    iframe = null;
                }
                finally {
                    if (iframe != null)
                        iframe = null;
                }
            }
        }
    }

    this.sublog2 = function (msg) {
        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
            if (g_console_sublog2) {
                console.log(RunaEngine.FileInfo.getPageTitle() + '[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
            }
        }
        else {
            if (g_console_sublog2) {
                var iframe = null;
                try {
                    iframe = document.createElement("IFRAME");
                    iframe.setAttribute("src", "ios-log:#iOS#" + RunaEngine.FileInfo.getPageTitle() + '[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
                    document.documentElement.appendChild(iframe);
                    iframe.parentNode.removeChild(iframe);
                    iframe = null;
                }
                finally {
                    if (iframe != null)
                        iframe = null;
                }
            }
        }
    }

    this.sendNative = function (data) {
        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {

        }
        else {
            var iframe = null;
            try {
                iframe = document.createElement("IFRAME");
                iframe.setAttribute("src", data);
                document.documentElement.appendChild(iframe);
                iframe.parentNode.removeChild(iframe);
                iframe = null;
            }
            finally {
                if (iframe != null)
                    iframe = null;
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    // FileInfo
    // 로딩 되는 html 파일에 관한 정보
    this.FileInfo = new function _FileInfo() {
        var m_PageTitle = null;

        this.getPageTitle = function () {
            m_PageTitle = m_PageTitle == null ? document.title : m_PageTitle;
            return m_PageTitle;
        }

    }
    // FileInfo
    /////////////////////////////////////////////////////////////////////////////////
    // DeviceInfo
    // 로딩 되는 디바이스의 종류를 확인하는 함수
    this.DeviceInfo = new function _DeviceInfo() {

        var m_isIE = null;
        var m_isPC = null;
        var m_isAndroid = null;
        var m_AndroidVer = null;
        var m_isVega = null;
        var m_isXT7320 = null;
        var m_isOSX = null;
        var m_isiOS4 = null;
        var m_iOSVer = null;
        var m_isAndroid_PAD = null;
        var m_isIPAD = null;
        var m_isIPHONE = null;
        var m_ScrollType = null;

        this.getISIE = function () {
            if (m_isIE == null) {
                if (navigator.appName == 'Microsoft Internet Explorer') {
                    var ua = navigator.userAgent;
                    //                    var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                    //                    if (re.exec(ua) != null)
                    //                        rv = parseFloat(RegExp.$1);
                    m_isIE = true;
                }
                else m_isIE = false;
            }

            return m_isIE;
        }
        this.isPC = function () {

            if (m_isPC == null) {
                console.log(navigator.userAgent);
                if (navigator.userAgent.indexOf("Windows") != -1) {
                    m_isPC = true;
                }
                else m_isPC = false;
            }

            return m_isPC;
        }

        this.getAndroidVer = function () {

            if (m_AndroidVer != null) {
                return m_AndroidVer;
            }

            if (navigator.userAgent.indexOf("Android 1.") != -1) {
                m_AndroidVer = 220;
            }
            else {
                if (navigator.userAgent.indexOf("Android 2.2") != -1) {
                    m_AndroidVer = 220;
                }
                else {
                    if (navigator.userAgent.indexOf("Android 2.3") != -1) {
                        m_AndroidVer = 230;
                    }
                    else {
                        if (navigator.userAgent.indexOf("Android 3.2") != -1) {
                            m_AndroidVer = 320;
                        }
                        else {
                            if (navigator.userAgent.indexOf("Android 4.0") != -1) {
                                m_AndroidVer = 400;
                            }
                            else {
                                if (navigator.userAgent.indexOf("Android 4.1") != -1 || navigator.userAgent.indexOf("Android 4.2") != -1 || navigator.userAgent.indexOf("Android 4.3") != -1) {
                                    m_AndroidVer = 430;
                                }
                                else {
                                    if (navigator.userAgent.indexOf("Android 4.4") != -1 && navigator.userAgent.indexOf("Android 4.4.") == -1) {
                                        m_AndroidVer = 440;
                                    }
                                    else {
                                        if (navigator.userAgent.indexOf("Android 4.4.1") != -1) {
                                            m_AndroidVer = 441;
                                        }
                                        else {
                                            if (navigator.userAgent.indexOf("Android 4.4.2") != -1) {
                                                m_AndroidVer = 442;
                                            }
                                            else {
                                                if (navigator.userAgent.indexOf("Android 4.4.3") != -1) {
                                                    m_AndroidVer = 443;
                                                }
                                                else {
                                                    if (navigator.userAgent.indexOf("Android 4.4.") != -1) {
                                                        m_AndroidVer = 444;
                                                    }
                                                    else {

                                                        if (navigator.userAgent.indexOf("Android 5.") != -1) {
                                                            m_AndroidVer = 500;
                                                        }
                                                        else {
                                                            m_AndroidVer = 10000;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return m_AndroidVer;
        }

        this.IsAndroid = function () {

            if (m_isAndroid == null) {
                m_isAndroid = (navigator.userAgent.indexOf('Android') != -1) ? true : false;
            }

            if (m_isAndroid) m_isPC = false;

            return m_isAndroid;
        }

        this.IsAndroidVega = function () {
            if (m_isVega == null) {
                if (this.IsAndroid() == true) {
                    if (navigator.userAgent.indexOf("IM-A") > -1) {
                        m_isVega = true;
                    }
                    else {
                        m_isVega = false;
                    }
                }
                else {
                    m_isVega = false;
                }
            }
            return m_isVega;
        };


        this.IsAndroidShine = function () {
            if (m_isShine == null) {
                if (this.IsAndroid() == true) {
                    RunaEngine.log("agent = " + navigator.userAgent);
                    if (navigator.userAgent.indexOf("CREMA-0610L") > -1) {
                        m_isShine = true;
                    }
                    else {
                        m_isShine = false;
                    }
                }
                else {
                    m_isShine = false;
                }
            }
            return m_isShine;
        };
        this.IsAndroid_PAD = function () {
            if (m_isAndroid_PAD == null) {
                m_isAndroid_PAD = (navigator.userAgent.indexOf('SHW-M380W') != -1) ? true : false;
            }

            if (m_isAndroid_PAD) m_isPC = false;

            return m_isAndroid_PAD;
        }

        this.setAndroid_PAD = function (isTablet) {
            m_isAndroid_PAD = isTablet;
            if (m_isAndroid_PAD) m_isPC = false;
        }

        this.IsAndroid_XT7320 = function () {
            if (m_isXT7320 == null) {
                o = (navigator.userAgent.indexOf("XT720") != -1) ? true : false;
            }

            return m_isXT7320;
        };

        this.GetVerIOS = function () {
            if (m_iOSVer == null) {
                if (navigator.userAgent.indexOf(" OS 4_") != -1) {
                    m_iOSVer = 4;
                }
                else {
                    if (navigator.userAgent.indexOf(" OS 5_") != -1) {
                        m_iOSVer = 5;
                    }
                    else {
                        if (navigator.userAgent.indexOf(" OS 6_") != -1) {
                            m_iOSVer = 6;
                        }
                        else {
                            if (navigator.userAgent.indexOf(" OS 7_") != -1) {
                                m_iOSVer = 7;
                            }
                            else {
                                if (navigator.userAgent.indexOf(" OS 8_") != -1) {
                                    m_iOSVer = 8;
                                }
                                else {
                                    if (navigator.userAgent.indexOf(" OS 9_") != -1) {
                                        m_iOSVer = 9;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (m_iOSVer != null) m_isPC = false;

            return m_iOSVer;
        };

        this.IsIOS4 = function () {
            if (m_isiOS4 == null) {
                m_isiOS4 = (navigator.userAgent.indexOf(' OS 4_') != -1) ? true : false;
            }

            if (m_isiOS4) m_isPC = false;

            return m_isiOS4;
        }

        this.IsIPAD = function () {
            if (m_isIPAD == null) {
                m_isIPAD = (navigator.userAgent.indexOf('iPad') != -1) ? true : false;
            }

            if (m_isIPAD) m_isPC = false;

            return m_isIPAD;
        }

        this.IsIPHONE = function () {
            if (m_isIPHONE == null) {
                m_isIPHONE = (navigator.userAgent.indexOf('iPhone') != -1) ? true : false;
            }

            if (m_isIPHONE) m_isPC = false;

            return m_isIPHONE;
        }

        this.IsIOS = function () {
            return RunaEngine.DeviceInfo.IsIPHONE() || RunaEngine.DeviceInfo.IsIPAD();
        }

        this.IsOSX = function () {
            if (m_isOSX == null) {
                m_isOSX = (navigator.userAgent.indexOf('OS X') != -1) ? true : false;
            }

            if (m_isOSX) m_isPC = false;

            return m_isOSX;
        }

        this.setVscrollModeEnable = function (enable) {
            m_ScrollType = enable;
        }

        this.IsVScrollMode = function () {

            if (m_ScrollType == null) {
                if (this.IsAndroid() == true) {
                    if (navigator.userAgent.indexOf("crema") > -1 || navigator.userAgent.indexOf("CREMA") > -1 || navigator.userAgent.indexOf("YP-GP") > -1 || navigator.userAgent.indexOf("SHW-M") > -1 || navigator.userAgent.indexOf("SHV-E") > -1 || navigator.userAgent.indexOf("Galaxy Nexus") > -1) {
                        m_ScrollType = false;
                    }
                    else {
                        if (this.IsAndroidVega() == true && navigator.userAgent.indexOf("Android 4.") > -1) {
                            m_ScrollType = false;
                        }
                        else {
                            if (this.getAndroidVer() >= 400) {
                                m_ScrollType = false;
                            }
                            else {
                                m_ScrollType = true;
                            }
                        }
                    }
                }
                else {
                    m_ScrollType = false;
                }

            }
            return m_ScrollType;
        }
    }
    // DeviceInfo
    /////////////////////////////////////////////////////////////////////////////////
    // 현재 페이지
    //
    this.HtmlCtrl = new function _HtmlCtrl() {
        var m_body = null;
        var m_divColumnLayout = null;
        var m_divContentLayout = null;
        var m_divLastBlankPageWithVerticalLayout = null;

        var m_CurPageCFI = null;
        // 화면 모드 (가로가 길면 : 0 , 세로가 길면 : 1)
        var m_Orientation = null;
        var m_ColPaddingWidth = 0;
        var m_ColMarginWidth = 0;

        //기본 변수들
        var m_PageWidth = 0;
        var m_PageHeight = 0;
        var m_TotalPageCount = 0;
        var m_CurPageNo = 0;

        var m_TotalPageWidthAfterSplit;
        var m_SplitPageWidth = 0;
        var m_isSinglePage = false;
        var m_CurScrollPos = 0;
        // PageInfo Array
        var g_arrPageInfo = new Array();

        // multimedia resource list
        var g_arrMultimedia = null;
        var g_jsonMultimedia = null;

        //variable for only changing a layout
        var m_arrPageCFIForOnlyChangeLayout = '';
        var m_arrPageCFIForCheckOnlyChangeLayout = null;

        this.getOrientation = function () {
            return m_Orientation;
        }

        this.getPageWidth = function () {
            return m_PageWidth;
        }

        this.getPageHeight = function () {
            return m_PageHeight;
        }

        this.getDocWidth = function () {
            if (RunaEngine.DeviceInfo.isPC()) {
                return document.body.scrollWidth;
            }
            else {
                if (RunaEngine.DeviceInfo.IsAndroid()) {
                    if (RunaEngine.DeviceInfo.getAndroidVer() >= 443) {
                        return document.body.scrollWidth;
                    }
                    else {
                        return document.width;
                    }
                }
                else {
                    return document.body.scrollWidth;
                }
            }
        };

        this.getTotalPage = function () {
            return m_TotalPageCount;
        }

        /////////////////////////////////////////////////////////////////////////////////
        // 다바이스 레이아웃 변경(가로->세로, 세로-> 가로)시 해당 페이지로 찾아 갈 수 있도록
        // CFI값으로 페이지 번호를 찾는다.
        this.PrepareChangeLayout = function (orientation) {

            var sCurPageCFI = null;

            try {

                if (m_Orientation == null) {
                    //처음 로딩
                    m_arrPageCFIForOnlyChangeLayout = '';
                }
                else if (m_Orientation == orientation) {
                    //동일한 레이아웃 변경
                }
                else {

                    sCurPageCFI = RunaEngine.HtmlCtrl.Page.getLastReadedPageCFI();

                    if (sCurPageCFI == m_arrPageCFIForOnlyChangeLayout ||
                        m_arrPageCFIForOnlyChangeLayout == '') {
                        //
                        m_arrPageCFIForOnlyChangeLayout = sCurPageCFI;
                    }
                    else {

                        //RunaEngine.log('ERIC ---------- [HtmlCtrl.PrepareChangeLayout] Page = ' + RunaEngine.HtmlCtrl.PageInfo.GetCfiPageNumber(m_arrPageCFIForOnlyChangeLayout));

                        if (RunaEngine.HtmlCtrl.Page.getPageNoFromCFI(sCurPageCFI) ==
                            RunaEngine.HtmlCtrl.Page.getPageNoFromCFI(m_arrPageCFIForOnlyChangeLayout)) {
                            // 같은 페이지 이므로 그대로 둔다
                        }
                        else {
                            m_arrPageCFIForOnlyChangeLayout = sCurPageCFI;
                        }
                    }

                    //RunaEngine.log('ERIC ---------- [HtmlCtrl.PrepareChangeLayout] strCurrPageCfi = ' + strCurrPageCfi + '   , m_arrPageCFIForOnlyChangeLayout = ' + m_arrPageCFIForOnlyChangeLayout);
                }
            }
            catch (ex) {
                //RunaEngine.log('ERIC ---------- [★Exception★][HtmlCtrl.PrepareChangeLayout] ' + (ex.number & 0xFFFF));
                //RunaEngine.log('ERIC ---------- [★Exception★][HtmlCtrl.PrepareChangeLayout] ' + ex.description);
            }
            finally {
                if (sCurPageCFI != null) {
                    sCurPageCFI = null;
                }
            }
        }
        // PrepareChangeLayout
        /////////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////////////
        // 가로 및 세로 모드 변경시, CFI값으로 페이지 이동한다. 단, 페이지 레이아웃 셋업 후 동작
        //
        this.ChangedLayout = function (orientation) {
            if (m_arrPageCFIForOnlyChangeLayout != '') {
                RunaEngine.HtmlCtrl.Navigator.MovePageWithCFI(m_arrPageCFIForOnlyChangeLayout);
            }
        }
        // ChangedLayout
        /////////////////////////////////////////////////////////////////////////////////


        /////////////////////////////////////////////////////////////////////////////////
        // css를 변경한다.
        //
        this.ChangeCss = function (typeAndClass, selector, newValue) {
            var cssRules = null, styleSheet = null, cssRule = null, targetRule = null;
            var isCssRules = false;
            var i = 0, a = 0;

            if (!document.styleSheets)
                return;

            try {
                for (a = 0; a < document.styleSheets.length; a++) {
                    styleSheet = document.styleSheets[a];

                    if (styleSheet.cssRules) {
                        cssRules = styleSheet.cssRules;
                        isCssRules = true;
                    }
                    else {
                        cssRules = styleSheet.rules;
                    }

                    if (cssRules) {
                        for (i = 0; i < cssRules.length; i++) {
                            cssRule = cssRules[i];
                            if (cssRule) {

                                if (cssRule.selectorText != null && (cssRule.selectorText.toLowerCase() == typeAndClass.toLowerCase())) {
                                    targetRule = cssRule;
                                    //RunaEngine.log('ERIC ---------- [★Exception★][HtmlCtrl.ChangeCss] ' + cssRule.selectorText + ', ' + newRule + ' : ' + newValue);
                                    break;
                                }
                            }
                        }
                    }

                    if (targetRule)
                        break;
                }


                if (targetRule)
                    targetRule.style[selector] = newValue;
            }
            catch (ex) {
                //RunaEngine.log('ERIC ---------- [★Exception★][HtmlCtrl.ChangeCss] ' + (ex.number & 0xFFFF));
                //RunaEngine.log('ERIC ---------- [★Exception★][HtmlCtrl.ChangeCss] ' + ex.description);
            }
            finally {
                cssRules = null, styleSheet = null, cssRule = null, targetRule = null;
            }
        }

        // ChangeCss
        /////////////////////////////////////////////////////////////////////////////////

        this.addStyle = function(selectorText, newValue) {
            var cssRules = null, styleSheet = null;

            if(!document.styleSheets) {
                return;
            }

            try {
                styleSheet = document.styleSheets[document.styleSheets.length - 2];

                if (styleSheet.cssRules) {
                    cssRules = styleSheet.cssRules;
                    isCssRules = true;
                }
                else {
                    cssRules = styleSheet.rules;
                }

                if(styleSheet.insertRule) {
                    len = cssRules.length;
                    styleSheet.insertRule(selectorText + "{" + newValue + "}", len);
                }
                else if(styleSheet.addRule) {
                    len = cssRules.length;
                    styleSheet.addRule(selectorText, newValue, len);
                }

            }
            catch(ex) {
                RunaEngine.log("ERIC ---------- [★Exception★][HtmlCtrl.addStyle] " + (ex.number & 0xFFFF));
                RunaEngine.log("ERIC ---------- [★Exception★][HtmlCtrl.addStyle] " + ex.description);
            }
            finally {
                cssRules = null, styleSheet = null;
            }
        }

        this.deleteStyle = function(selectorText, matcher) {
            var cssRule = null, cssRules = null, styleSheet = null;

            if(!document.styleSheets) {
                return;
            }

            try {
                styleSheet = document.styleSheets[document.styleSheets.length - 2];
                if (styleSheet.cssRules) {
                        cssRules = styleSheet.cssRules;
                        isCssRules = true;
                    }
                    else {
                        cssRules = styleSheet.rules;
                    }

                    if (cssRules) {
                        for (i = 0; i < cssRules.length; i++) {
                            cssRule = cssRules[i];

                            if(cssRule.selectorText != null) {
                                var ruleSelector=cssRule.selectorText.replace(/\s/g,"");
                                selectorText=selectorText.replace(/\s/g,"");

                                if (ruleSelector.toLowerCase() == selectorText.toLowerCase()
                                    && cssRule.style.cssText.match(matcher) != null) {
                                    if(styleSheet.removeRule) {
                                        styleSheet.removeRule(i);
                                    }
                                    else if(styleSheet.deleteRule) {
                                        styleSheet.deleteRule(i);
                                    }
                                }
                            }
                            else {
                                if(cssRule.style != null && cssRule.style.cssText != null && cssRule.style.cssText.match(matcher) != null) {
                                    if(styleSheet.removeRule) {
                                        styleSheet.removeRule(i);
                                    }
                                    else if(styleSheet.deleteRule) {
                                        styleSheet.deleteRule(i);
                                    }
                                }
                            }
                        }
                    }
            }
            catch(ex) {
                RunaEngine.log("ERIC ---------- [★Exception★][HtmlCtrl.deleteStyle] " + (ex.number & 0xFFFF));
                RunaEngine.log("ERIC ---------- [★Exception★][HtmlCtrl.deleteStyle] " + ex.description);
            }
            finally {
                cssRule = null, cssRules = null, styleSheet = null;
            }
        }

        // '파일이름#아이디' 형태가 아닌 '#아이디' 형식의 내부링크를 처리하기 위한 메소드
        // 안드로이드에서 내부링크 차단을 위해사용
        this.bdb_HrefClick = function (e) {
            if (window.event) {
                e.returnValue = false;
            }
            else if (e.preventDefault) {
                e.preventDefault();
            }
            window.android.HrefClick(e.getAttribute('href'));
        }


        /////////////////////////////////////////////////////////////////////////////////
        // 페이지를 초기화하여 다시 페이지 계산한다.
        //
        this.InitPage = function (isTablet, orientation, width, height, column_padding, fileindex, dpi) {

            RunaEngine.log('ERIC ----------- ViewInfo (width) : ' + width);
            RunaEngine.log('ERIC ----------- ViewInfo (height) : ' + height);


            RunaEngine.DeviceInfo.setAndroid_PAD(isTablet);

            if (column_padding == "undefined" || column_padding == null) {
                m_ColPaddingWidth = 2;
            }
            else {
                m_ColPaddingWidth = column_padding;
            }

            /**
             * redmine refs #20510,20494
             * 페이지 로딩이 느림 (안드로이드 크롬 버전과도 연관성 있음 63.0.3239.111)
             * by junghoon.kim
             */
            //remove a selection or Highlight
            // RunaEngine.HtmlCtrl.Highlight.RemoveAll();
            // RunaEngine.HtmlCtrl.Selection.HideSelection();

            if (!RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC() || RunaEngine.DeviceInfo.IsOSX()) {
                if (RunaEngine.DeviceInfo.IsIPAD() || RunaEngine.DeviceInfo.isPC() || RunaEngine.DeviceInfo.IsOSX()) {
                    // IPAD (IPAD는 두페이지 처리때매 자동동작 제거)
                }
                else {
                    //IPHONE은 아래 주석코드 있어야 좌우동작함. (안드로이드는 View에서 컨트롤)
                    this.PrepareChangeLayout(orientation);
                }
            }
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                var anchorArrays = document.getElementsByTagName('a');
                var count = anchorArrays.length;
                var aHref;
                for (var i = 0; i < count ; i++) {
                    aHref = anchorArrays[i].getAttribute('href');
                    /**
                     * 빌드북 rev.582
                     * 빌드북에서 "#아이디" 형식의 내부 링크를 막기위해 수정한 사항에서 null point 에러 발생하는 부분 추가 수정
                     * by junghoon.kim
                     */
                    if (aHref != null && aHref != undefined && aHref.indexOf('#') >= 0 && aHref.split('#')[0] == "") {
                        anchorArrays[i].setAttribute('onclick', "RunaEngine.HtmlCtrl.bdb_HrefClick(this);return false;");
                    }
                }
            }

            if (m_body == null) {
                m_body = document.getElementsByTagName("BODY")[0];
            }
            if (m_divColumnLayout == null) {
                m_divColumnLayout = document.getElementById('runaengine_columns_Layout');
            }
            if (m_divContentLayout == null) {
                m_divContentLayout = document.getElementById('runaengine_content');
            }
            if (m_divLastBlankPageWithVerticalLayout == null) {
                m_divLastBlankPageWithVerticalLayout = document.getElementById('runaengine_vertical_layout');
            }

            m_Orientation = parseInt(orientation);
            m_TotalPageCount = 0;
            m_CurPageNo = 1;

            m_TotalPageWidthAfterSplit = 0;
            g_nOnePageWidthAfterSplit = 0;

            m_PageWidth = width;
            m_PageHeight = height;


            m_divContentLayout.style.width = (m_PageWidth - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2)) + 'px';

            //이미지를 먼저 리싸이징하면 컨텐츠넓이가 원래보다 더 크게 셋팅되어 나오는 현상이 생겨서 변경
            this.ResizeImage(fileindex, dpi);

            this.PageSplit();

            RunaEngine.log('ERIC ----------- [HtmlCtrl.Init] Split After - TotalPage:' + m_TotalPageCount + ' width : [' + RunaEngine.HtmlCtrl.getDocWidth() + ' :' + m_PageWidth + ']');

            if (!RunaEngine.DeviceInfo.IsVScrollMode()) {
                this.BalanceLayoutAfterSplit();
            }

            RunaEngine.log('ERIC ----------- [HtmlCtrl.Init] Balance After - TotalPage:' + m_TotalPageCount);
            // RunaEngine.log('ERIC ----------- [HtmlCtrl.Init] ' + document.body.innerHTML);

            if (!RunaEngine.DeviceInfo.IsAndroid()) {
                if (RunaEngine.DeviceInfo.IsIPAD() || RunaEngine.DeviceInfo.IsOSX()) {
                    // IPAD (IPAD는 두페이지 처리때매 자동동작 제거)
                }
                else {
                    // IPHONE은 아래 주석코드 있어야 좌우동작함. (안드로이드는 View에서 컨트롤)
                    this.ChangedLayout(orientation);
                }
            }
            return m_TotalPageCount;

        }
        // InitPage End
        /////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////
        // InitPageFixed
        this.InitPageFixed = function (isTablet, orientation, width, height, column_padding, fileindex, dpi) {

            RunaEngine.log('ERIC ----------- ViewInfo (width) : ' + width);
            RunaEngine.log('ERIC ----------- ViewInfo (height) : ' + height);


            RunaEngine.DeviceInfo.setAndroid_PAD(isTablet);

            /**
             * 2022.04.13 hack
             * fixed layout에서 실제 사이즈로 caretRangeFromPoint 범위를 가져올경우 null이 나오는 경우가있어서
             * InitPage와 같은 기본 padding 2를 넣어놨습니다
             * ex) -> layout size width:524 height:666 -> caretRangeFromPoint(document, 524, 666) -> null
             * 원인 파악후 삭제하겠습니다
             */
            if (column_padding == "undefined" || column_padding == null) {
                m_ColPaddingWidth = 2;
            }
            else {
                m_ColPaddingWidth = column_padding;
            }

            if (!RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC() || RunaEngine.DeviceInfo.IsOSX()) {
                if (RunaEngine.DeviceInfo.IsIPAD() || RunaEngine.DeviceInfo.isPC() || RunaEngine.DeviceInfo.IsOSX()) {
                    // IPAD (IPAD는 두페이지 처리때매 자동동작 제거)
                }
                else {
                    //IPHONE은 아래 주석코드 있어야 좌우동작함. (안드로이드는 View에서 컨트롤)
                    this.PrepareChangeLayout(orientation);
                }
            }

            if (m_body == null) {
                m_body = document.getElementsByTagName("BODY")[0];
            }
            if (m_divColumnLayout == null) {
                m_divColumnLayout = document.getElementById('runaengine_columns_Layout');
            }
            if (m_divContentLayout == null) {
                m_divContentLayout = document.getElementById('runaengine_content');
            }
            if (m_divLastBlankPageWithVerticalLayout == null) {
                m_divLastBlankPageWithVerticalLayout = document.getElementById('runaengine_vertical_layout');
            }

            m_Orientation = parseInt(orientation);
            m_TotalPageCount = 1;
            m_CurPageNo = 1;

            m_TotalPageWidthAfterSplit = 0;
            g_nOnePageWidthAfterSplit = 0;

            m_PageWidth = width;
            m_PageHeight = height;

            RunaEngine.log('ERIC ----------- [HtmlCtrl.Init] Balance After - TotalPage:' + m_TotalPageCount);

            return m_TotalPageCount;

        }
        // InitPageFixed End
        /////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////
        // viewport 정보를 가져온다.
        this.GetViewPort = function () {
            var viewportInfo = null;
            var viewports = null;
            var viewport = null;
            var ele = null;
            var idx = null;
            var vcontent = null;
            var arrContent = null;

            try {
                viewports = document.getElementsByName("viewport");

                //RunaEngine.log('GetViewPort, '+ viewports.length);

                for (idx = 0; idx < viewports.length; idx++) {
                    ele = viewports[idx];
                    if (RunaEngine.HtmlCtrl.PageInfo._NodeName(ele) == "meta") {
                        viewport = ele;
                        //RunaEngine.log('GetViewPort, '+ viewport);
                        break;
                    }
                }

                if (viewport == null) {
                    return null;
                }

                var regWidth = /^width=/i;
                var regHeight = /^height=/i;
                vcontent = viewport.content;
                arrContent = vcontent.split(",");

                viewportInfo = new Object();

                for (var i = 0; i < arrContent.length; i++) {
                    var arrInfo = arrContent[i].split("=");
                    if (arrInfo.length == 2) {
                        var key = RunaEngine.Util.Trim(arrInfo[0]);
                        var value = RunaEngine.Util.Trim(arrInfo[1]);

                        if (regWidth.test(key) || regHeight.test(key)) {
                            // 간혹 px 을 포함한 값이 지정됨.
                            value.replace(/px/ig, "");
                        }

                        viewportInfo[key] = value;
                    }
                }

                return viewportInfo;
            }
            catch (ex) {
                //RunaEngine.log('[★Exception★][HtmlCtrl.GetViewPort] ' + (ex.number & 0xFFFF));
                //RunaEngine.log('[★Exception★][HtmlCtrl.GetViewPort] ' + ex);
                return null; //예외
            }
            finally {
                viewportInfo = null;
                viewports = null;
                viewport = null;
                ele = null;
                idx = null;
                vcontent = null;
                arrContent = null;
            }
        }
        // GetViewPort End
        /////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////
        // 실제 페이지는 나눈다.
        //

        this.PageSplit = function () {
            //variable
            var D = true;

            var m_styleContent = null;
            var m_styleColumn = null;
            var m_styleOnePageOneImage = null;
            var m_styleOnePageOneImageWidth = 0;
            var m_styleOnePageOneImageHeight = 0;

            try {

                RunaEngine.log("page split!!!");

                m_PageWidthAfterDivide = m_PageWidth;
                m_styleContent = m_divContentLayout != null ? m_divContentLayout.style : null;
                m_styleColumn = m_divColumnLayout != null ? m_divColumnLayout.style : null;

                m_styleContent.marginLeft = m_ColMarginWidth + 'px';
                m_styleContent.marginRight = m_ColMarginWidth + 'px';
                //m_styleContent.paddingLeft = m_ColPaddingWidth + 'px';
                //m_styleContent.paddingRight = m_ColPaddingWidth + 'px';
                //m_styleContent.marginTop = m_ColMarginWidth + 'px';
                //m_styleColumn.marginBottom = m_ColMarginWidth + 'px';

                if ((RunaEngine.DeviceInfo.IsIPAD() && RunaEngine.HtmlCtrl.getOrientation() == 0) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.isPC()) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.IsOSX())) {
                    m_styleContent.setProperty('margin-left', m_ColPaddingWidth.toString() + 'px', 'important');
                    //RunaEngine.log('ERIC ----------- orientation '+ m_ColPaddingWidth);
                }

                // if(RunaEngine.DeviceInfo.IsAndroid() && RunaEngine.DeviceInfo.getAndroidVer() < 440) {
                //     RunaEngine.log("android version < 440");
                //     var firstChildNode = m_divContentLayout.firstChild;
                //     RunaEngine.log("firstChild = " + firstChildNode.tagName + " // node name = " + firstChildNode.nodeName + " // value = " + firstChildNode.nodeValue + " // html = " + firstChildNode.outerHTML);
                //     if(RunaEngine.HtmlCtrl.TTSHandler.checkBlockLevelElement(firstChildNode)) {
                //         RunaEngine.log("first child is block element so margin top set 0");
                //         firstChildNode.style.marginTop = '0px !important';
                //     }
                // }

                //이미지만 있는 커버 페이지이거나 한 파일에 이미지 하나만 존재 할 경우
                /*
                 * 2017-8-17
                 * 조건 추가됨
                 * epub 3.0 에서 audio 테그가 있는데도 도비라에 걸려 안보이는 이슈가 있음
                 * 이에 audio, video 테그가 있으면 도비라 조건에서 제외
                 */
                var arrAudio = document.getElementsByTagName('audio');
                var arrVideo = document.getElementsByTagName('video');
                var arrImg = document.getElementsByTagName('img');

                if (document.getElementsByTagName('img').length == 1
                    && m_divContentLayout.innerText.search(/[^ \n\r\t\f\x0b\xa0]/) == -1
                    && arrAudio.length <= 0 && arrVideo.length <= 0) {

                    m_isSinglePage = true;
                    m_styleContent.marginLeft = '0px';
                    m_styleContent.marginRight = '0px';

                    if ((RunaEngine.DeviceInfo.IsIPAD() && RunaEngine.HtmlCtrl.getOrientation() == 0) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.isPC()) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.IsOSX())) {
                        m_styleContent.setProperty('margin-left', m_ColPaddingWidth.toString() + 'px', 'important');
                        //RunaEngine.log('ERIC ----------- orientation '+ m_ColPaddingWidth);
                    }

                    m_styleOnePageOneImage = document.getElementsByTagName('img')[0].style;

                    m_divContentLayout.innerHTML = '<div style=\'display:table;text-indent:0px;table-layout:fixed;\'><div style=\'text-indent:0px;display:table-cell;margin:0px;border-spacing:0px;border-style:none;padding:0px;vertical-align:middle;text-align:center !important;width:'
                    // m_divContentLayout.innerHTML = '<table style=\'margin:0px;border-spacing:0px;border-style:none;padding:0px;width:'
                    + (m_PageWidth - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth))
                    + 'px; height:'
                    + (m_PageHeight - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth))
                    //+ 'px;\'><tr><td align=center style=\'border-spacing:0px;border-style:none;padding:0px;vertical-align:middle;\'>' + document.getElementsByTagName('img')[0].outerHTML + '</td></tr></table>';
                    + 'px;\'>' + document.getElementsByTagName('img')[0].outerHTML + '</div></div>';
                    /*21.02.25 안드로이드 커버 이미지가 안 나오는 경우 화면 사이즈로 크기를 조정해 준다.*/
                    if (!RunaEngine.DeviceInfo.IsAndroid() ){
                        document.getElementsByTagName('img')[0].style.width = m_styleOnePageOneImage.width;
                        document.getElementsByTagName('img')[0].style.height = m_styleOnePageOneImage.height;
                    }
                    else{
                       if(m_styleOnePageOneImage.width == "0px"){
                             document.getElementsByTagName('img')[0].style.width = (m_PageWidth - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth))+"px";
                             RunaEngine.sublog2('[HtmlCtrl.ResizeImage] m_styleOnePageOneImage.width:11111111' + m_styleOnePageOneImage.width + ' , m_styleOnePageOneImage.height :' + m_styleOnePageOneImage.height);
                        }
                        else{
                           document.getElementsByTagName('img')[0].style.width =   m_styleOnePageOneImage.width;
                           RunaEngine.sublog2('[HtmlCtrl.ResizeImage] m_styleOnePageOneImage.width22222222:' + m_styleOnePageOneImage.width + ' , m_styleOnePageOneImage.height :' + m_styleOnePageOneImage.height);
                        }
                        if(m_styleOnePageOneImage.height  == "0px"){
                           document.getElementsByTagName('img')[0].style.height =  (m_PageHeight - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth))+"px";
                        }
                        else{
                           document.getElementsByTagName('img')[0].style.height =  m_styleOnePageOneImage.height;
                        }
                   }

                    if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                        // 컬럼 브레이크 걸린것이 있으면 무시
                        document.getElementsByTagName('img')[0].style.pageBreakInside = 'auto';
                    }
                    else {
                        // 컬럼 브레이크 걸린것이 있으면 무시
                        document.getElementsByTagName('img')[0].style.webkitColumnBreakBefore = 'avoid';
                    }
                } // single image

                // #46657 [Yes24 eBook iOS] EPUB하단 짤림 현상
                // 싱글 페이지 로직 체크 전과 체크 후에 높이가 다르게 나오는 경우가 있음
                // 검토 해 보니 아래 css 변환 로직 후에 높이가 다르게 나옴
                // 이에 변환 로직을 제일 위로 올려서 싱글페이지 검토전과 검토후의 스타일 변화가 없도록 수정
                 if (!(navigator.userAgent.indexOf("crema") > -1 || navigator.userAgent.indexOf("CREMA") > -1)) {
				RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-width', m_PageWidth + 'px');
                RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-gap', '0px');
                RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-width', m_PageWidth + 'px');
                RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-gap', '0px');
                RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-width', m_PageWidth + 'px');
                RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-gap', '0px');
				}

                if (m_isSinglePage == false) {
                    // single page check시 실제 하나의 페이지 싸이즈를 계산
                    // view size가 2배이고, 안에 2개의 페이지가 노출되는 형태(가로모드 2페이지)
                    // 에서는 window.innerWidth등의 값이 실제 2페이지의 싸이즈이기 때문에 보정
                    RunaEngine.log("m_isSinglePage false!!");

                    if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                        m_styleColumn.width = m_PageWidth + 'px';
                        m_styleColumn.height = '';
                        m_styleContent.width = (m_PageWidth - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2)) + 'px';
                        m_styleContent.height = '';


                        m_TotalPageCount = this.PageInfoMaker_ForVScrollMode();

                        RunaEngine.log('ERIC ----------- Vertical Scroll ' + m_TotalPageCount);
                    }
                    else {

                        var computedHeight = g_computedHeight;
                        var computedWidth = g_computedWidth;

                        if (!RunaEngine.DeviceInfo.IsAndroid() || g_computedHeight == null) {
                            if(parseInt(window.getComputedStyle(m_divColumnLayout, null).height) >= parseInt(window.getComputedStyle(m_divColumnLayout, null).height)) {
                                computedHeight = parseInt(window.getComputedStyle(m_divColumnLayout, null).height);
                            }
                            else {
                                computedHeight = parseInt(window.getComputedStyle(m_divContentLayout, null).height);
                            }
                            computedWidth = parseInt(window.getComputedStyle(m_divColumnLayout, null).width);
                        }

                        RunaEngine.sublog('ERIC ----------- computedHeight ' + computedHeight + 'body height : ' + parseInt(window.getComputedStyle(m_divColumnLayout, null).height));

                        // 강제로 가로를 지정해놓고, 높이를 보자
                        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                            if (RunaEngine.DeviceInfo.IsAndroid_PAD()) {
                                // 싱글페이지에서 사용하면, IPhone/Android에서 간혹 가로인데 세로넓이로 Split되는 버그가 있다
                                m_styleColumn.width = m_PageWidth + 'px';

                                /**
                                 * redmine #refs 3354
                                 * 안드로이드 글자 잘림 현상 문제 수정
                                 * by junghoon.kim
                                 */
                                if (m_PageWidth >= computedWidth
                                    && m_PageHeight >= computedHeight && computedHeight > 0) {

                                    m_isSinglePage = true;

                                    m_styleColumn.height = ''; //m_PageHeight + 'px';
                                    // if (!RunaEngine.DeviceInfo.IsAndroid()) {
                                    //     RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-width', '');
                                    //     RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-gap', '');
                                    //     RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-width', '');
                                    //     RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-gap', '');
                                    //     RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-width', '');
                                    //     RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-gap', '');
                                    // }
                                    //m_styleColumn.width = '';
                                    //m_styleColumn.height = '';
                                }
                            }
                            else {
                                // Android 폰은 컬럼을 지정하면 Content Div가 짤려나오는 현상이 보여서 아무짓도 안함
                                /**
                                 * redmine #refs 3354
                                 * 안드로이드 글자 잘림 현상 문제 수정
                                 * by junghoon.kim
                                 */
                                if (m_PageWidth >= computedWidth
                                    && m_PageHeight >= computedHeight && computedHeight > 0) {
                                    m_isSinglePage = true;

                                    m_styleColumn.height = ''; //m_PageHeight + 'px';

                                    if (!RunaEngine.DeviceInfo.IsAndroid()) {
                                        RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-width', '');
                                        RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-gap', '');
                                        RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-width', '');
                                        RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-gap', '');
                                        RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-width', '');
                                        RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-gap', '');
                                    }
                                }
                            }
                        }
                        else {
                            if (RunaEngine.DeviceInfo.IsIPAD() || RunaEngine.DeviceInfo.IsOSX()) {
                                // IPAD용, 실제로는 가로모드 듀얼페이지용
                                // 싱글페이지에서 사용하면, IPhone/Android에서 간혹 가로인데 세로넓이로 Split되는 버그가 있다
                                m_styleColumn.width = m_PageWidth + 'px';

                                // 2016.02.23 - 아이패드에서 간혹가다가 높이가 0인 경우가 있다.
                                // added by nspark
                                if (m_PageWidth >= computedWidth
                                    && (m_PageHeight) >= computedHeight
                                    && computedHeight > 0) {

                                    m_isSinglePage = true;

                                    //RunaEngine.log('ERIC ----------- PAD - Fake SINGLE PAGE - H2');

                                    m_styleColumn.height = '';//(m_PageHeight - m_ColMarginWidth - m_ColPaddingWidth ) + 'px';
                                    m_styleContent.width = '';
                                    m_styleContent.height = '';

                                }

                                // if(RunaEngine.DeviceInfo.GetVerIOS() > 7) {
                                //     m_styleContent.height = (m_PageHeight - (m_ColMarginWidth)) + 'px';
                                // }
                            }
                            else {
                                // IPot은 첫 로딩에서 Init할때 기기 싸이즈가 320:480 이고, Init시 300:400 으로 주면
                                // document.width/height 와 window.getComputedStyle 는 320:480 기준으로 데이터를 내어준다.
                                // 희안한 현상. -_-;;
                                // 가로로 틀었을떄는 또 정상이다. -_-;; 이것도 희안함
                                // 따라서, Ipot의 경우만 별도로 document 기준으로 싱글페이지 계산해야함.

                                // IPot에서 한페이지 모드계산을 잘못함. (Column을 조절하면 가로/세로 이동시 깨져나와서 Content를 수정함)
                                m_styleContent.width = (m_PageWidth - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2)) + 'px';

                                //RunaEngine.log('ERIC ----------- check document.width:' + RunaEngine.HtmlCtrl.getDocWidth() + ', document.height:' + (document.height !== undefined ? document.height : window.innerHeight) + ', window.innerWidth:' + window.innerWidth + ', window.innerHeight:' + window.innerHeight);
                                //RunaEngine.log('ERIC ----------- heck m_PageWidth:' + m_PageWidth + ', m_PageHeight:' + m_PageHeight + ', ColumnWidth:' + parseInt(window.getComputedStyle(m_divColumnLayout, null).width) + ', ColumnHeight:' + parseInt(window.getComputedStyle(m_divColumnLayout, null).height));

                                if (window.innerWidth >= m_PageWidth  //width비교는 사족이긴 하다
                                    && window.innerHeight >= computedHeight
                                    && computedHeight > 0) {

                                    m_isSinglePage = true;


                                    //RunaEngine.log('ERIC ----------- Fake SINGLE PAGE - H3');

                                    m_styleColumn.height = ''; //m_PageHeight + 'px';
                                    m_styleContent.width = (m_PageWidth - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2)) + 'px'; // IPot 한페이지 모드계산때 바꾼코드
                                    m_styleContent.height = '';
                                    //RunaEngine.log('ERIC ----------- Fake SINGLE PAGE - H3');
                                }
                            }
                        }
                    }
                }

                if ((RunaEngine.DeviceInfo.IsIPAD() && RunaEngine.HtmlCtrl.getOrientation() == 0) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.IsOSX()) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.isPC())) {
                    //m_styleContent.setProperty('margin-left', m_ColPaddingWidth.toString() + 'px', 'important');
                    //RunaEngine.log('ERIC ----------- orientation '+ m_ColPaddingWidth);
                }

                if (RunaEngine.DeviceInfo.IsVScrollMode() == false) {
                    if (!m_isSinglePage) {

                        m_styleColumn.width = m_PageWidth + 'px';

                        /**
                         * redmine #refs 5910, 6247
                         * 카르타에서 리어왕 컨텐츠 하단 글자 잘리는 이슈 및 텍스트가 겹치는 문제 수정
                         * by junghoon.kim
                         */
                        m_styleColumn.height = (m_PageHeight - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2)) + 'px';
                        //   m_styleColumn.height = m_PageHeight + 'px';

                        RunaEngine.log('ERIC ----------- !!!!!!!!!!!!!!!!!!');
                        // RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-width', m_PageWidth + 'px');
                        // RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-webkit-column-gap', '0px');
                        // RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-width', m_PageWidth + 'px');
                        // RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', '-moz-column-gap', '0px');
                        // RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-width', m_PageWidth + 'px');
                        // RunaEngine.HtmlCtrl.ChangeCss('div#runaengine_columns_Layout', 'column-gap', '0px');

                        m_styleContent.width = (m_PageWidth - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2)) + 'px'; //이거없으면 Iphone에서 넓이가 넘어버림
                        m_styleContent.height = '';

                        if (RunaEngine.DeviceInfo.IsAndroid()) {
                            // Android는 IOS처럼 CSS 에, overflow:hidden으로 아래쪽 글자 짤려서 보여주는 현상이 사라지긴 하는데
                            // 가로모드시 종종 세로모드의 width만큼으로 hidden 되버리는 현상이 있어서 JS에서 잡아준다.
                            // 2016-12-26
                            // 넉서스 (5.11) 에서 hidden  일경우 잘리는 현상있어서 수정
                            // 이부분을 다른 값으로 해야 할경우 E-Ink 디바이스에서 꼭 테스트 해볼것
                            m_styleContent.overflow = 'visible';
                        }
                        else if (RunaEngine.DeviceInfo.isPC()) {
                            m_styleContent.overflow = 'visible';
                        }

                        // ios - jdm
                        // if ((RunaEngine.DeviceInfo.IsIOS() && RunaEngine.HtmlCtrl.getOrientation() == 0)){
                        //
                        //     for (i = 1; i < 10; i++) {
                        //         var bg = 'bg-color'+i;
                        //         var bgColor = document.getElementsByClassName(bg);
                        //         if (bgColor.length > 0) {
                        //             RunaEngine.log('jdm ----------- !!!!!!!!!!!!!!!!!!');
                        //             m_isSinglePage = true;
                        //             m_TotalPageCount = 1;
                        //         }
                        //     }
                        // } else {
                            RunaEngine.log('ERIC -- width : ' + m_PageWidth + ', scroll width = ' + RunaEngine.HtmlCtrl.getDocWidth() + ', height : ' + m_styleContent.height);
                            // 컬럼레이아웃 적용후라서, 현재 도큐먼트 기준으로 나누어야함

                            // m_TotalPageCount = Math.round(RunaEngine.HtmlCtrl.getDocWidth() / m_PageWidth);
                            m_TotalPageCount = RunaEngine.HtmlCtrl.getDualTotalPageCount(m_PageWidth);
                        // }
                    }
                    else {
                        m_styleColumn.width = m_PageWidth + 'px';
                        m_styleColumn.height = '';
                        m_styleContent.height = '';
                        m_styleContent.width = (m_PageWidth - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2)) + 'px';

                        // Android는 IOS처럼 CSS 에, overflow:hidden으로 아래쪽 글자 짤려서 보여주는 현상이 사라지긴 하는데
                        // 가로모드시 종종 세로모드의 width만큼으로 hidden 되버리는 현상이 있어서 JS에서 잡아준다.
                        // 이부분을 다른 값으로 해야 할경우 E-Ink 디바이스에서 꼭 테스트 해볼것
                        // ios에서 특별한 문제가 없어 같은 로직 적용
                        m_styleContent.overflow = 'visible';
                        m_styleColumn.width = (m_PageWidth) + 'px';

                        m_TotalPageCount = 1;
                    }
                }
                else {

                    if (m_isSinglePage) {
                        m_styleContent.overflow = 'visible';
                        m_styleColumn.width = (m_PageWidth) + 'px';

                        m_TotalPageCount = 1;
                    }
                }

                try {
                    /**
                     * 2015-11-26 박주일
                     * http://211.205.64.188:5876/redmine/issues/530
                     * 카르타 뷰어에서만 getComputedStyle의 height 가 작게오는 문제가있음
                     * (CSS 와의 복합적인 문제 인듯함)
                     * 안드로이드 일경우에만 해당로직 적용
                     */
                    if ((RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) && RunaEngine.DeviceInfo.IsVScrollMode() == false) {
                        if (m_styleColumn.height == null || m_styleColumn.height == '') {
                            //RunaEngine.log("m_styleColumn.height = [null]");
                            m_styleColumn.height = m_PageHeight + "px"
                        }
                    }

                } catch (e) {
                    RunaEngine.log("Height Error");
                }
                return m_TotalPageCount;
            }
            finally {
                D = null;
                m_styleContent = null;
                m_styleColumn = null;
                ImgStyleSingle = null;
            }
        }
        // PageSplit
        /////////////////////////////////////////////////////////////////////////////////

        // this.getDualTotalPageCount = function (width) {
        //     var iterator = null;
        //     var node = null;
        //     // var range = null;
        //     // var rect = null;
        //     // var startPage = 0, endPage = 0;
        //     var totalCount = 0;
        //
        //     try {
        //
        //         if (m_divContentLayout == null) {
        //             m_divContentLayout = document.getElementById('runaengine_content');
        //         }
        //
        //         iterator = document.createTreeWalker(div#runaengine_columns_Layout
        //             m_divContentLayout, NodeFilter.SHOW_ALL,
        //             { acceptNode: function(node) { return NodeFilter.FILTER_ACCEPT; } }, false
        //         );
        //         node = iterator.nextNode();
        //         // node = iterator.firstChild();
        //         var i = 0;
        //         while (node != null) {
        //             var nodeType = node.nodeType;
        //             var nodeName = node.nodeName;
        //             // RunaEngine.log('jdm nodeType' + nodeType);
        //
        //             if (nodeType == 1 || nodeType == 3) {
        //
        //                 // if (nodeType == 3){
        //                 //     node = iterator.nextNode();
        //                 //     continue;
        //                 // }
        //
        //                 if (nodeName == "DIV"){
        //                     node = iterator.nextNode();
        //
        //                     while (node != null) {
        //                         if (node.nodeName != undefined) {
        //                             // node = iterator.lastChild();
        //                             RunaEngine.log('jdm nodeName' + node.nodeName + ' textContent ' + node.textContent);
        //                             if (node.nodeType == 1 || node.nodeType == 3) {
        //                                 var range = document.createRange();
        //                                 range.selectNode(node);
        //                                 var rect = range.getBoundingClientRect();
        //                                 range.detach();
        //
        //                                 RunaEngine.log('jdm rect width, tlbr : [' + rect.width + ',' + rect.top + ', ' + rect.left + ', ' + rect.bottom + ', ' + rect.right + ']');
        //                                 // RunaEngine.log('jdm rect width, yxbr : [' + rect.width + ',' + rect.y + ', ' + rect.x + ', ' + rect.bottom + ', ' + rect.right + ']');
        //
        //                                 if (RunaEngine.DeviceInfo.IsVScrollMode()) {
        //                                     // startPage = parseInt(rect.top / height) + 1;
        //                                     // endPage = parseInt((rect.top + rect.height) / height) + 1;
        //                                 } else {
        //                                     var startPage = parseInt(rect.left / width) + 1;
        //                                     var endPage = parseInt((rect.left + rect.width) / width) + 1;
        //
        //                                     // if (endPage == 0) endPage = 1;
        //
        //                                     if (totalCount < endPage) {
        //                                         totalCount = endPage;
        //                                     }
        //
        //                                 }
        //                             }
        //                         }
        //                         node = iterator.nextSibling();
        //                     } // while
        //                 } // div
        //                 else {
        //                     if (node.nodeName != undefined) {
        //                         // node = iterator.lastChild();
        //                         if (node.childNodes.length == 1) {
        //                             node = iterator.firstChild()
        //                             RunaEngine.log('jdm nodeName' + node.nodeName + ' textContent ' + node.textContent);
        //
        //                             var range = document.createRange();
        //                             range.selectNode(node);
        //                             var rect = range.getBoundingClientRect();
        //                             range.detach();
        //
        //                             RunaEngine.log('jdm rect width, tlbr : [' + rect.width + ',' + rect.top + ', ' + rect.left + ', ' + rect.bottom + ', ' + rect.right + ']');
        //
        //                             if (RunaEngine.DeviceInfo.IsVScrollMode()) {
        //                             } else {
        //                                 var startPage = parseInt(rect.left / width) + 1;
        //                                 var endPage = parseInt((rect.left + rect.width) / width) + 1;
        //
        //                                 // if (endPage == 0) endPage = 1;
        //
        //                                 if (totalCount < endPage) {
        //                                     totalCount = endPage;
        //                                 }
        //
        //                             }
        //                             RunaEngine.log('jdm totalCount startPage, endPage : [' + totalCount + ', ' + startPage + ', ' + endPage + ']');
        //                         }
        //                     }
        //                 }
        //             }
        //             node = iterator.nextNode();
        //         }
        //
        //         RunaEngine.log('jdm getDualTotalPageCount : ==========> ' + totalCount);
        //         // return endPage;
        //         return totalCount;
        //     }
        //     catch (ex) {
        //         RunaEngine.log('jdm ex' + ex);
        //     }
        //     finally {
        //         iterator = null;
        //         node = null;
        //         // range = null;
        //         // rect = null;
        //         // fendDate = new Date();
        //         // fcalDate = fendDate - fstartDate;
        //         // RunaEngine.sublog2('jdm node, iterate date = ' + (fcalDate / 1000));
        //     }
        // }

        this.getDualTotalPageCount = function (width) {

            var node = null;
            var totalCount = 0;

            try {

                if (m_divContentLayout == null) {
                    m_divContentLayout = document.getElementById('runaengine_content');
                }

                function findAllNode(oParent, oCallback) {
                    RunaEngine.log("oParent hasChildNodes => " + oParent.hasChildNodes() + ", nodeName => " + oParent.nodeName + ", nodeType => " + oParent.nodeType + ", nodeValue => " + oParent.nodeValue);
                    if (oParent.hasChildNodes()) {
                        for (var oNode = oParent.firstChild; oNode; oNode = oNode.nextSibling) {
                            node = oNode;
                            findAllNode(oNode, oCallback);
                        }
                    }
                    oCallback.call(oParent);
                }

                function getContentTotalCount() {
                    if (this.nodeValue || this.nodeName.toUpperCase() == "IMG") {
                        RunaEngine.log("---- nodeName => " + this.nodeName + ", nodeType => " + this.nodeType + ", textContent => " + this.textContent + ", length => " + this.textContent.length);
                        if (this.nodeType == 1 || this.nodeType == 3) {
                            var range = document.createRange();
                            range.selectNode(node);
                            var rect = range.getBoundingClientRect();
                            range.detach();

                            RunaEngine.log('---- jdm rect : width => [' + rect.width + '], left => [' + rect.left + ']');

                            if (parseInt(rect.right) >= width) {
                                console.log('---- 수정필요 jdm crect : width => [' + rect.width + '], left => [' + rect.left + '], right => [' + rect.right + ']');
                            }

                            var startPage = parseInt(rect.left / width) + 1;
                            
                            var fPage = (rect.left + rect.width) / width;
                            var endPage = parseInt(fPage);
                            if( fPage - endPage > 0) {
                                endPage += 1;
                            }

                            if (totalCount < endPage) {
                                totalCount = endPage;
                            }

                            console.log('---- jdm totalCount, startPage, endPage : [' + totalCount + ', ' + startPage + ', ' + endPage + ']');
                        }
                    }
                }

                findAllNode(m_divContentLayout, getContentTotalCount);
                return totalCount;
            } catch (ex) {
                RunaEngine.log('jdm ex' + ex);
            } finally {
                node = null;
            }
        }
        //////////////////////////////////////////////////////////////
        //
        // [VScrollMode] 현재 가려야할 페이지 하단 정보리턴
        //
        this.GetCurrentHideHeight_ForVScrollMode = function () {
            if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                var arrPageInfo = RunaEngine.HtmlCtrl.GetPageInfo_ForVScrollMode(RunaEngine.HtmlCtrl.PageInfo.GetCurrPageNo());

                //RunaEngine.log('ERIC ----------- GetCurrentHideHeight_ForVScrollMode : ' + (m_PageHeight - (arrPageInfo.pageBottomPos - arrPageInfo.pageTopPos)));

                return m_PageHeight - (arrPageInfo.pageBottomPos - arrPageInfo.pageTopPos);
            }
            else {
                return 0;
            }
        }
        //////////////////////////////////////////////////////////////
        //
        // [VScrollMode] 페이지정보 Array에서 특정 페이지정보 가져옴
        //
        this.GetPageInfo_ForVScrollMode = function (nPageNo) {
            if (nPageNo <= g_arrPageInfo.length) {
                return g_arrPageInfo[nPageNo - 1];
            }
            else {
                //예외케이스
                if (g_arrPageInfo.length == 0) {
                    var a = new Array();
                    a.pageTopPos = 0;
                    a.pageBottomPos = m_PageHeight;
                    return a;
                }
                else {
                    return g_arrPageInfo[g_arrPageInfo.length - 1];
                }
            }
        }

        //////////////////////////////////////////////////////////////
        //
        // [VScrollMode] 지정한 Top Position을 가지고 페이지번호 추출
        //
        this.GetPageNumberFromTopPos_ForVScrollMode = function (TopPos) {
            for (var i = 0; i < g_arrPageInfo.length; i++) {

                //RunaEngine.log('ERIC ----------- GetPageNumberFromTopPos_ForVScrollMode   [Top:' + TopPos + '][i:' + i + '][pageTopPos:' + g_arrPageInfo[i].pageTopPos + '][pageBottomPos:' + g_arrPageInfo[i].pageBottomPos + ']==========  ' + (i + 1));

                if (g_arrPageInfo[i].pageBottomPos >= TopPos) {
                    return i + 1;
                }
            }
            return 1;
        }

        //////////////////////////////////////////////////////////////
        //
        // [VScrollMode] 페이지 정보 Array를 만들고, 페이지 카운트 리턴
        // 지연을 위해사 -1을 리턴할 때도 있음
        //
        this.PageInfoMaker_ForVScrollMode = function () {

            var range = null;
            var rects = null;
            var pageIdx = null;
            var rect = null;
            var k = null;

            try {

                var doc_width = RunaEngine.HtmlCtrl.getDocWidth();
                var doc_height = (document.height !== undefined ? document.height : window.innerHeight);

                if (doc_width != m_PageWidth) {
                    // 계산할수 없는(잘못계산될 상태)
                    RunaEngine.log('ERIC ----------- BAD MAKE PAGE INFO - BREAK REASON 1 - ' + ' doc_width:' + doc_width + ', page_width:' + m_PageWidth);
                    //return -1;
                }

                g_arrPageInfo = null;
                g_arrPageInfo = new Array();

                range = document.createRange();
                range.selectNodeContents(m_divContentLayout);
                rects = range.getClientRects();

                if (rects.length == 0) {
                    RunaEngine.log('ERIC ----------- BAD MAKE PAGE INFO - BREAK REASON 2 -  ' + ' doc_width:' + doc_width + ', page_width:' + m_PageWidth + " contentlayout :" + m_divContentLayout);
                    m_TotalPageCount = 1;
                    return 1;
                }
                else {
                    pageIdx = 0;

                    //RunaEngine.log('ERIC ----------- LAST HEIGHT :' + document.body.scrollHeight);
                    var extraHeight = 0;
                    var totalHeight = document.body.scrollHeight;


                    if (totalHeight < m_PageHeight) {

                        g_arrPageInfo[pageIdx] = new Array();
                        g_arrPageInfo[pageIdx].pageTopPos = 0;
                        g_arrPageInfo[pageIdx].pageBottomPos = m_PageHeight; //checkTop_old;
                        //RunaEngine.log('ERIC ----------- ' + pageIdx + ' ) ');
                        pageIdx++;
                    }
                    else {
                        var calBottom = 0;

                        while (true) {
                            g_arrPageInfo[pageIdx] = new Array();
                            g_arrPageInfo[pageIdx].pageTopPos = m_PageHeight * pageIdx;
                            g_arrPageInfo[pageIdx].pageBottomPos = m_PageHeight * (pageIdx + 1);
                            calBottom = g_arrPageInfo[pageIdx].pageBottomPos;
                            //RunaEngine.log('ERIC ----------- (' + pageIdx + ' ) top : ' + g_arrPageInfo[pageIdx].pageTopPos + '     bottom : ' + g_arrPageInfo[pageIdx].pageBottomPos);

                            pageIdx++;

                            if ((totalHeight - calBottom) <= 0) {
                                extraHeight = calBottom - totalHeight;
                                break;
                            }
                        }
                    }

                    // 마지막페이지의 스크롤을 위해서 마지막에 빈페이지를 하나 채워봅세
                    m_divLastBlankPageWithVerticalLayout.style.width = m_PageWidth + 'px';
                    m_divLastBlankPageWithVerticalLayout.style.height = extraHeight + 'px';

                    RunaEngine.log('ERIC ----------- LAST HEIGHT :' + document.body.scrollHeight);
                    //RunaEngine.log('ERIC ----------- SIZE TWO  H ' + parseInt(window.getComputedStyle(document.body, null).height));
                    // 실제 페이지계산 완료
                    m_TotalPageCount = pageIdx;

                    return pageIdx; //이미 ++하였음
                }
            }
            catch (ex) {
                //RunaEngine.log('[★Exception★][HtmlCtrl.PageInfoMaker_ForVScrollMode] ' + (ex.number & 0xFFFF));
                RunaEngine.log('[★Exception★][HtmlCtrl.PageInfoMaker_ForVScrollMode] ' + ex);
                return 1; //예외
            }
            finally {
                range = null;
                rects = null;
                drawList = null;
                drawList_Idx = null;
                rect = null;
            }
        }

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 정상적인 Split이후에 이미지나 비디오가 잘려보이는 부분을 막기 위해
        //          페이지 넘김 강제 처리를 해주며, 페이지 싸이즈도 다시 한다.
        this.BalanceLayoutAfterSplit = function () {

            var arrImg = null
            var objImgStyle = null;
            var i = null;
            var range = null;
            var rects = null;
            var rect = null;

            try {
                if (m_isSinglePage == true) {
                    arrImg = document.getElementsByTagName('img') || [];

                    for (i = 0; i < arrImg.length; i++) {
                        // [DebugTip] window.getComputedStyle(arrImg[i],null).top
                        // [DebugTip] arrImg[i].style.offsetWidth / clientWidth / scrollWidth / scrollLeft / top
                        range = document.createRange();
                        range.selectNodeContents(arrImg[i]);
                        rects = range.getClientRects();
                        rect = rects[0];

                        RunaEngine.log('[asset][BalanceLayoutAfterSplit] single image size : [' + (rect.right - rect.left) + ', ' + (rect.bottom - rect.top) + ']');

                        if (rects.length > 0) {
                            if (parseInt(rect.left / m_PageWidth) != parseInt(rect.right / m_PageWidth)
                                || parseInt(rect.top / m_PageHeight) != parseInt(rect.bottom / m_PageHeight)
                                || rect.top < 0) {
                                objImgStyle = arrImg[i].style;
                                objImgStyle.display = 'inline-block';
                                objImgStyle.padding = '0';
                                objImgStyle.margin = '0';
                                objImgStyle.pageBreakBefore = 'always'; //always
                                objImgStyle.webkitColumnBreakBefore = 'always'; //always/auto/avoid
                            }
                        }
                    }
                    RunaEngine.log("page balance split 1");
                }
                else if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                    // Android는 PageBreak 자체가 안먹힘, 무시
                    arrImg = document.getElementsByTagName('img') || [];
                    RunaEngine.log("page balance split 2");
                    for (i = 0; i < arrImg.length; i++) {
                        // [DebugTip] window.getComputedStyle(arrImg[i],null).top
                        // [DebugTip] arrImg[i].style.offsetWidth / clientWidth / scrollWidth / scrollLeft / top
                        range = document.createRange();
                        range.selectNodeContents(arrImg[i]);
                        rects = range.getClientRects();
                        rect = rects[0];

                        if (rects.length > 0) {
                            if (parseInt(rect.left / m_PageWidth) != parseInt(rect.right / m_PageWidth)
                                || parseInt(rect.top / m_PageHeight) != parseInt(rect.bottom / m_PageHeight)
                                || rect.top < 0) {
                                objImgStyle = arrImg[i].style;
                                objImgStyle.display = 'inline-block';
                                objImgStyle.padding = '0';
                                objImgStyle.margin = '0';
                                objImgStyle.pageBreakBefore = 'always'; //always
                                objImgStyle.webkitColumnBreakBefore = 'always'; //always/auto/avoid
                            }
                        }
                    }
                }
                else {
                    // check image
                    if (!RunaEngine.DeviceInfo.IsAndroid()) {
                        // 안드로이드에서 백그라운드 뷰어에서 GetVerIOS 계산시  0.2초 이상 소모되어 안드로이드 체크 추가
                        if (RunaEngine.DeviceInfo.GetVerIOS() < 8) {
                            arrImg = document.getElementsByTagName('img') || [];

                            for (i = 0; i < arrImg.length; i++) {
                                // [DebugTip] window.getComputedStyle(arrImg[i],null).top
                                // [DebugTip] arrImg[i].style.offsetWidth / clientWidth / scrollWidth / scrollLeft / top
                                range = document.createRange();
                                range.selectNodeContents(arrImg[i]);
                                rects = range.getClientRects();
                                rect = rects[0];

                                if (rects.length > 0) {
                                    if (parseInt(rect.left / m_PageWidth) != parseInt(rect.right / m_PageWidth)
                                        || parseInt(rect.top / m_PageHeight) != parseInt(rect.bottom / m_PageHeight)
                                        || rect.top < 0) {
                                        objImgStyle = arrImg[i].style;
                                        objImgStyle.display = 'inline-block';
                                        objImgStyle.padding = '0';
                                        objImgStyle.margin = '0';
                                        objImgStyle.pageBreakBefore = 'always'; //always
                                        objImgStyle.webkitColumnBreakBefore = 'always'; //always/auto/avoid
                                    }
                                }
                            }
                        }
                    }


                }
            }
            catch (ex) {
                RunaEngine.log('[assert][BalanceLayoutAfterSplit]' + ex);
                //RunaEngine.log('[★Exception★][HtmlCtrl._BalanceLayoutAfterSplit]' + (ex.number & 0xFFFF));
                //RunaEngine.log('[★Exception★][HtmlCtrl._BalanceLayoutAfterSplit]' + ex);
            }
            finally {
                arrImg = null;
                objImgStyle = null;
                i = null;
                range = null;
                rects = null;
                rect = null;
            }

            // re calc page count
            RunaEngine.log('[BalanceLayoutAfterSplit] m_isSinglePage = ' + m_isSinglePage + ' total : ' + m_TotalPageCount);

            //양면에서 한페이지를 두페이지로 인식하는 버그를 수정
            // modified by nspark
            if (m_isSinglePage == true && m_TotalPageCount == 1)
                m_TotalPageCount = 1;
            else
                // m_TotalPageCount = Math.round(RunaEngine.HtmlCtrl.getDocWidth() / m_PageWidth);
                m_TotalPageCount = RunaEngine.HtmlCtrl.getDualTotalPageCount(m_PageWidth);
        }

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 이미지를 화면싸이즈에 맞게 가로/세로를 리싸이징함
        // [ Param  ]
        // [ Return ] void
        this.ResizeImage = function (fileindex, dpi) {
            var isDebug = true;
            var arrImg = null;
            var nViewWidth = null;
            var nViewHeight = null;
            var i = null;

            var nOrgImgWidth = null;
            var nOrgImgHeight = null;

            var range = null;
            var rects = null;

            try {
                //RunaEngine.log('[HtmlCtrl.ResizeImage] Div-size width:' + m_divContentLayout.offsetWidth + ' , height :' + m_PageHeight);
                arrImg = document.getElementsByTagName('img');
                if (!RunaEngine.DeviceInfo.IsAndroid() ){
                    nViewWidth = (m_PageWidth - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth));
                    nViewHeight = m_PageHeight * 0.9;
                }

                //if (RunaEngine.DeviceInfo.isPC()) {
                //} else {
                // #16652 table 태그에 삽입된 텍스트 잘림 - iOS
                // 위 이슈 처리와 더불어 기존에 복잡하고 오래 걸리던 리사이징 로직을 개편함
                var viewWidth = (m_PageWidth - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth));
                var viewHeight = m_PageHeight;

                if (RunaEngine.DeviceInfo.IsAndroid() && !RunaEngine.DeviceInfo.IsVScrollMode() ) {
                  viewHeight = ((m_PageHeight - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth)));
                }
                // offset 0.95 의경우 그림이 한페이지를 다 채우면
                // 밑에 한줄로 설명문구가 있는 경우가 많은데 이 경우 다음 페이지로 설명 문구가 넘어가 하단에 조금의 여백을 주기 위함
                // 화면의 95%도 충분히 크다고 생각됨
                viewWidth = parseInt(viewWidth * 0.95);
                viewHeight = parseInt(viewHeight * 0.95);
                // RunaEngine.log('(ios image resize) viewWidth:' + viewWidth + ', viewHeight:' + viewHeight);
                for (i = 0, len = arrImg.length ; i < len; i++) {
                    /**
                     * redmine refs #13631,4141
                     * 테이블 내 이미지 리사이즈 오류 수정(target-densitydpi 를 제거 하고 나서 이 로직은 제거 가능 할 수도 있으므로 추후에 확인)
                     * by junghoon.kim
                     * 2022.04.14 hack
                     * android fontSize 설정하는 부분을
                     * WebView setTextZoom -> CSS font-size로 변경해서 img 사이즈를 따로 설정하지 않고 width 100%로 들어와도 폰트사이즈에 맞게 사이즈가잡힘
                     */
                    // if (RunaEngine.DeviceInfo.IsAndroid()) {
                    //     if (arrImg[i].parentElement.tagName.toLowerCase() == 'td') {
                    //         var getStyle = arrImg[i].parentElement.currentStyle ? arrImg[i].parentElement.currentStyle : getComputedStyle(arrImg[i].parentElement, null);
                    //         var parentWidth = getStyle.width.replace('px', '')
                    //         if (parentWidth < arrImg[i].naturalWidth) {
                    //             arrImg[i].parentElement.style.width = arrImg[i].naturalWidth + 'px';
                    //         }
                    //
                    //         continue;
                    //     } else if (arrImg[i].parentElement.parentElement.tagName.toLowerCase() == 'td') {
                    //         var getStyle = arrImg[i].parentElement.parentElement.currentStyle ? arrImg[i].parentElement.parentElement.currentStyle : getComputedStyle(arrImg[i].parentElement.parentElement, null);
                    //         var parentWidth = getStyle.width.replace('px', '')
                    //         if (parentWidth < arrImg[i].naturalWidth) {
                    //             arrImg[i].parentElement.parentElement.style.width = arrImg[i].naturalWidth + 'px';
                    //         }
                    //         continue;
                    //     }
                    // }

                    arrImg[i].style.maxWidth = '100%';

//                    if (RunaEngine.DeviceInfo.IsAndroid() && !RunaEngine.DeviceInfo.IsVScrollMode() ) {
                        arrImg[i].style.maxHeight = '100%';
//                    }
//                    else{
//                      if (!RunaEngine.DeviceInfo.IsAndroid()){
//                         arrImg[i].style.maxHeight = '100%';
//                      }
//                    }

                    //                        RunaEngine.log('(ios image resize) image.src:' + arrImg[i].src);
                    var imgWidth = arrImg[i].width;
                    var imgHeight = arrImg[i].height;
                    var imgStyleWidth = arrImg[i].style.width;
                    var imgStyleHeight = arrImg[i].style.height;
                    //                        RunaEngine.log('(ios image resize) image.style.width:' + imgStyleWidth + ', image.style.height:' + imgStyleHeight);
                    // 이미지 테그의 스타일 설정에 따라 이미지 width값이 이상하게(뷰 넓이값) 보정이 되는 경우가 있음
                    // 이에 상황에 맞게 imgWidth값을 보정함
                    // 검토한바로는 아래의 경우임
                    // 1. height에만 값이 설정된 경우 (%, px, em)
                    // 2. height, width 모두 값이 px로 설정된 경우
                    // 3. height, width 모두 값이 em으로 설정된 경우
                    // 보정시 imgHeight을 기준으로 함(imgHeight값은 보정이 되어 내려옴)
                    // 결과적으로는 height값이 설정되어 있으면 보정하면됨
                    // #17129 아이폰에서 추가적으로 이상 현상 발생
                    // 4. 스타일 설정이 안되어 있는데도 가로 값은 보정이 되어 있는데 세로 값이 보정이 안되는 경우가 있음
                    //RunaEngine.log('(ios image resize) imgWidth:' + imgWidth + ', imgHeight:' + imgHeight);
                    var orgWidth = arrImg[i].naturalWidth;
                    var orgHeight = arrImg[i].naturalHeight;
                    //                        RunaEngine.log('(ios image resize) orgWidth:' + orgWidth + ', orgHeight:' + orgHeight);
                    if (imgStyleHeight != undefined && imgStyleHeight != null && imgStyleHeight != '') {
                        // 1.2.3.
                        var scale = imgHeight / orgHeight;
                        imgWidth = orgWidth * scale;
                        RunaEngine.log("scale @@ = " + scale + " imgWidth = " + imgWidth);
                    } else {
                        // 4.
                        if (orgHeight == imgHeight) {
                            var scale = imgWidth / orgWidth
                            imgHeight = orgHeight * scale;
                            RunaEngine.log("scale !! = " + scale + " imgWidth = " + imgWidth);
                        }
                    }

                    /**
                     * 2022.05.25 hack
                     * android 이미지사이즈 dpi 적용
                     * 2022.06.10 hack
                     * dpi 적용시 이미지가 한쪽으로 찌그러지던 이슈
                     */
                    if(RunaEngine.DeviceInfo.IsAndroid()) {
                        // 1. 이미지 태그의 사이즈에 dpi를 곱해준다
                        var dpiWidth = imgWidth * dpi;
                        var dpiHeight = imgHeight * dpi;
                        // 2. dpi가 적용된 값을 이미지 스타일에 넣어준다
                        arrImg[i].style.width = dpiWidth + 'px';
                        arrImg[i].style.height = dpiHeight + 'px';
                        // 3. 이미지스타일 적용 후 다시 이미지태그의 사이즈를 다시 가져온다 // 값이 변경되었는지 확인하기 위함
                        var tagWidth = arrImg[i].width;
                        var tagHeight = arrImg[i].height;
                        // 4. 이미지 태그의 사이즈가 변경되지 않았을경우 이미지태그 부모태그의 사이즈로 max 사이즈가 설정되어있기 때문에 비율을 조정해줘야한다
                        // width height 중 한곳에만 사이즈가 설정되어있는 경우도 있고 둘다설정되어있는 경우도 있고 둘다 설정되어있지 않은경우도있음
                        // 그래서 dpi를 적용한 값이 이미지 태그의 값과 다를 경우 태그의 값으로 넣어주고 비율을 맞춰준다
                        if(dpiWidth !== tagWidth) {
                            arrImg[i].style.width = tagWidth + 'px';
                            arrImg[i].style.height = (dpiHeight / dpiWidth * tagWidth) + 'px';
                        }else if(dpiHeight !== tagHeight) {
                            arrImg[i].style.width = (dpiWidth / dpiHeight * tagHeight) + 'px';
                            arrImg[i].style.height = tagHeight + 'px';
                        }
                        //6. 이미지 태그가 디바이스 사이즈보다 커질수 있기때문에 다시 태그사이즈를 넣어준다
                        imgWidth = arrImg[i].width;
                        imgHeight = arrImg[i].height;
                    }

                    // 이미지가 뷰 크기보다 크면 잘리므로 보정, 최종적으로 viewWidth, viewHeight을 넘지 않음
                    //                        RunaEngine.log('(ios image resize) fixedImgWidth:' + imgWidth + ', fixedIHeight:' + imgHeight);
                    if (imgWidth > viewWidth || imgHeight > viewHeight) {
                        var result = this.getResizedImageSize(viewWidth, viewHeight, imgWidth, imgHeight);
                        arrImg[i].style.width = result.scaledWidth + 'px';
                        arrImg[i].style.height = result.scaledHeight + 'px';
                    }
                    else {
                        /**
                         * 현재는 density 적용으로 작은 크기의 이미지는 보정되지 않는다.
                         * 그래서 기존에 도비라라고 판단 했던 로직을 적용해서 작은 이미지는 크게 보정해주도록 적용
                         * 추후 이 로직은 img len 이 1일 때만 적용 또는 아예 없애도 된다.
                         * 2022.04.14 hack
                         * android fontSize 설정하는 부분을
                         * WebView setTextZoom -> CSS font-size로 변경해서 img 사이즈를 따로 설정하지 않고 width 100%로 들어와도 폰트사이즈에 맞게 사이즈가잡힘
                         */
                        // if (RunaEngine.DeviceInfo.IsAndroid() /*&& m_divContentLayout.innerText.length < 22*/) {
                        //     viewHeight = ((m_PageHeight - (2 * m_ColMarginWidth) - (2 * m_ColPaddingWidth)));
                        //     var hScale = viewHeight / imgHeight;
                        //     var result = this.getResizedImageSize(viewWidth, viewHeight, imgWidth * dpi, imgHeight * dpi);
                        //     arrImg[i].style.width = result.scaledWidth + 'px';
                        //     arrImg[i].style.height = result.scaledHeight + 'px';
                        // }
                    }
                }
                //}
            }
            catch (ex) {
                RunaEngine.log('[assert][ResetImage]' + ex.description);
                RunaEngine.log('[★Exception★][HtmlCtrl.ResizeImage]' + (ex.number & 0xFFFF));
                RunaEngine.log('[★Exception★][HtmlCtrl.ResizeImage]' + ex);
            }
            finally {
                isDebug = null;
                arrImg = null;
                nViewWidth = null;
                nViewHeight = null;
                i = null;
                range = null;
                rects = null;
            }
        }

        this.getResizedImageSize = function (viewWidth, viewHeight, imgWidth, imgHeight) {
            var result = null;
            var scaledWidth = imgWidth;
            var scaledHeight = imgHeight;

            if (imgWidth > viewWidth) {
                var wScale = viewWidth / imgWidth;
                scaledWidth = imgWidth * wScale;
                scaledHeight = imgHeight * wScale;
                if (scaledHeight > viewHeight) {
                    var hScale = viewHeight / scaledHeight;
                    scaledWidth = scaledWidth * hScale;
                    scaledHeight = scaledHeight * hScale;
                }
                scaledWidth = parseInt(scaledWidth);
                scaledHeight = parseInt(scaledHeight);
            } else if (imgHeight > viewHeight) {
                var hScale = viewHeight / imgHeight;
                scaledWidth = imgWidth * hScale;
                scaledHeight = imgHeight * hScale;

                scaledWidth = parseInt(scaledWidth);
                scaledHeight = parseInt(scaledHeight);
            }


            RunaEngine.log("getResizedImageSize width = " + scaledWidth + " height = " + scaledHeight);
            result = { scaledWidth: scaledWidth, scaledHeight: scaledHeight };

            return result;
        }

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 리소스 재생 멈춤
        // [ Param  ]
        // [ Return ] void
        this.StopMultimedia = function (index) {
            try {

                if (g_arrMultimedia == null)
                    return;

                if (index < 0 || index >= g_arrMultimedia.length)
                    return;

                g_arrMultimedia[index].pause();
                g_arrMultimedia[index].currentTime = 0;
            }
            catch (ex) {
                RunaEngine.log('[★Exception★][HtmlCtrl.StopMultimedia] ' + ex);
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 리소스 재생 멈춤
        // [ Param  ]
        // [ Return ] void
        this.PauseMultimedia = function (index) {
            try {

                if (g_arrMultimedia == null)
                    return;

                if (index < 0 || index >= g_arrMultimedia.length)
                    return;

                g_arrMultimedia[index].pause();
            }
            catch (ex) {
                RunaEngine.log('[★Exception★][HtmlCtrl.PauseMultimedia] ' + ex);
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 리소스 재생 멈춤
        // [ Param  ]
        // [ Return ] void
        this.StopAllMultimedia = function (type) {
            var arrVideo = null;
            var arrAudio = null;

            try {
                if (type == 0) {
                    // video
                    arrVideo = document.getElementsByTagName('video') || [];

                    for (i = 0; i < arrVideo.length; i++) {
                        arrVideo[i].pause();
                        arrVideo[i].currentTime = 0;
                    }
                }
                else if (type == 1) {
                    // audio
                    arrAudio = document.getElementsByTagName('audio') || [];

                    for (i = 0; i < arrAudio.length; i++) {
                        arrAudio[i].pause();
                        arrAudio[i].currentTime = 0;
                    }
                }
                else if (type == 2) {
                    // all

                    arrVideo = document.getElementsByTagName('video') || [];

                    for (i = 0; i < arrVideo.length; i++) {
                        arrVideo[i].pause();
                        arrVideo[i].currentTime = 0;
                    }

                    arrAudio = document.getElementsByTagName('audio') || [];

                    for (i = 0; i < arrAudio.length; i++) {
                        arrAudio[i].pause();
                        arrAudio[i].currentTime = 0;
                    }
                }
            }
            catch (ex) {

            }
            finally {
                arrVideo = null;
                arrAudio = null;
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 리소스 재생 멈춤
        // [ Param  ]
        // [ Return ] void
        this.PauseAllMultimedia = function (type) {
            var arrVideo = null;
            var arrAudio = null;

            try {
                if (type == 0) {
                    // video
                    arrVideo = document.getElementsByTagName('video') || [];

                    for (i = 0; i < arrVideo.length; i++) {
                        arrVideo[i].pause();
                    }
                }
                else if (type == 1) {
                    // audio
                    arrAudio = document.getElementsByTagName('audio') || [];

                    for (i = 0; i < arrAudio.length; i++) {
                        arrAudio[i].pause();
                    }
                }
                else if (type == 2) {
                    // all

                    arrVideo = document.getElementsByTagName('video') || [];

                    for (i = 0; i < arrVideo.length; i++) {
                        arrVideo[i].pause();
                    }

                    arrAudio = document.getElementsByTagName('audio') || [];

                    for (i = 0; i < arrAudio.length; i++) {
                        arrAudio[i].pause();
                    }
                }
            }
            catch (ex) {

            }
            finally {
                arrVideo = null;
                arrAudio = null;
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 리소스 재생 리스트
        // [ Param  ]
        // [ Return ] json string
        this.GetMultimediaList = function () {
            var arrVideo = null;
            var arrAudio = null;
            var arrSource = null;
            var cfi = null;

            if (g_jsonMultimedia != null) {
                return g_jsonMultimedia;
            }

            g_arrMultimedia = new Array();
            g_jsonMultimedia = "[";

            try {
                arrVideo = document.getElementsByTagName('video') || [];

                for (i = 0; i < arrVideo.length; i++) {
                    cfi = null;

                    if (arrVideo[i].src != null && arrVideo[i].src != undefined) {

                        if (i > 0)
                            g_jsonMultimedia += ',';

                        cfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrVideo[i], '', "");
                        g_jsonMultimedia += "{\"type\" : \"video\", \"src\" : \"" + arrVideo[i].src + "\", \"pos\" : \"" + cfi + "\"}";
                        g_arrMultimedia.push(arrVideo[i]);
                    }
                    else {
                        cfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrVideo[i], '', "");
                        arrSource = null;
                        arrSource = arrVideo[i].children;
                        for (j = 0; j < arrSource.length; j++) {
                            if (arrSource[j].tagName == "source") {

                                if (i > 0)
                                    g_jsonMultimedia += ',';

                                g_jsonMultimedia += "{\"type\" : \"video\", \"src\" : \"" + arrSource[j].src + "\", \"pos\" : \"" + cfi + "\"}";

                                g_arrMultimedia.push(arrVideo[i]);
                            }
                        }
                    }

                    arrVideo[i].setAttribute('ruaIdx', g_arrMultimedia.length - 1);
                    arrVideo[i].setAttribute('controlsList', 'nodownload');

                    arrVideo[i].ontimeupdate = function () {

                        var idx = this.getAttribute('ruaIdx');

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnTimeUpdate(idx, this.currentTime, this.duration);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            window.android.OnTimeUpdate(idx, this.currentTime, this.duration);
                        }
                        else {
                            //잦은 progress 호출을 막기 위해 250ns 이하만 보낸다
                            var current = this.currentTime;
                            var dp = current - parseInt(current);
                            if (dp <= 250) {
                                RunaEngine.sendNative("runaMediaTime:" + idx + "," + this.currentTime + "," + this.duration);
                            }
                        }
                    };
                    if(RunaEngine.DeviceInfo.IsAndroid()) {
                        arrVideo[i].onwebkitfullscreenchange = function () {
                            var isFullScreen = document.webkitFullscreenElement !== null
                            if(!isFullScreen) {
                                this.pause();
                            }
                            window.android.OnMediaFullScreen(isFullScreen);
                        };
                    }
                    arrVideo[i].onplaying = function () {
                        var idx = this.getAttribute('ruaIdx');

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnStart(idx);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            this.webkitEnterFullscreen();
                            window.android.OnStart(idx);
                        }
                        else {
                            document.location = "runaMediaStart:" + idx + "," + this.duration;
                        }
                    };
                    arrVideo[i].onended = function () {
                        var idx = this.getAttribute('ruaIdx');

                        if (this.currentTime >= this.duration && this.paused)
                            this.currentTime = 0;

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnEnded(idx);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            window.android.OnEnded(idx);
                        }
                        else {
                            document.location = "runaMediaEnd:" + idx;
                        }
                    };
                    arrVideo[i].onpause = function () {
                        var idx = this.getAttribute('ruaIdx');

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnPauseed(idx);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            window.android.OnPauseed(idx);
                        }
                        else {
                            document.location = "runaMediaPause:" + idx;
                        }
                    };
                }

                arrAudio = document.getElementsByTagName('audio') || [];

                for (i = 0; i < arrAudio.length; i++) {

                    cfi = null;

                    if (arrAudio[i].src != null && arrAudio[i].src != undefined) {

                        if (i > 0)
                            g_jsonMultimedia += ',';

                        cfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrAudio[i], '', "");
                        g_arrMultimedia.push(arrAudio[i]);
                        g_jsonMultimedia += "{\"type\" : \"audio\", \"src\" : \"" + arrAudio[i].src + "\", \"pos\" : \"" + cfi + "\"}";
                    }
                    else {
                        cfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrAudio[i], '', "");
                        arrSource = null;
                        arrSource = arrAudio[i].children;
                        for (j = 0; j < arrSource.length; j++) {
                            if (arrSource[j].tagName == "source") {

                                if (i > 0)
                                    g_jsonMultimedia += ',';

                                g_jsonMultimedia += "{\"type\" : \"audio\", \"src\" : \"" + arrSource[j].src + "\", \"pos\" : \"" + cfi + "\"}";
                                g_arrMultimedia.push(arrAudio[i]);
                            }
                        }
                    }

                    //RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(e);

                    arrAudio[i].setAttribute('ruaIdx', g_arrMultimedia.length - 1);

                    arrAudio[i].ontimeupdate = function () {
                        var idx = this.getAttribute('ruaIdx');

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnTimeUpdate(idx, this.currentTime, this.duration);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            window.android.OnTimeUpdate(idx, this.currentTime, this.duration);
                        }
                        else {
                            //잦은 progress 호출을 막기 위해 250ns 이하만 보낸다
                            var current = this.currentTime;
                            var dp = current - parseInt(current);
                            if (dp <= 250) {
                                RunaEngine.sendNative("runaMediaTime:" + idx + "," + this.currentTime + "," + this.duration);
                            }
                        }
                    };
                    arrAudio[i].onplaying = function () {
                        var idx = this.getAttribute('ruaIdx');

                        this.setAttribute('state', 'play');

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnStart(idx);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            window.android.OnStart(idx);
                        }
                        else {
                            document.location = "runaMediaStart:" + idx + "," + this.duration;
                        }
                    };
                    arrAudio[i].onended = function () {
                        var idx = this.getAttribute('ruaIdx');

                        if (this.currentTime >= this.duration && this.paused)
                            this.currentTime = 0;

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnEnded(idx);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            window.android.OnEnded(idx);
                        }
                        else {
                            document.location = "runaMediaEnd:" + idx;
                        }
                    };
                    arrAudio[i].onpause = function () {
                        var idx = this.getAttribute('ruaIdx');

                        if (RunaEngine.DeviceInfo.isPC()) {
                            RunaPC.OnPauseed(idx);
                        }
                        else if (RunaEngine.DeviceInfo.IsAndroid()) {
                            window.android.OnPauseed(idx);
                        }
                        else {
                            document.location = "runaMediaPause:" + idx;
                        }
                    };
                }

                g_jsonMultimedia += "]";

                return g_jsonMultimedia;

            }
            catch (ex) {
                g_jsonMultimedia = null;
                g_arrMultimedia = null;
                RunaEngine.log('[★Exception★][HtmlCtrl.GetMultimediaList] ' + ex);
            }
            finally {
                arrSource = null;
                arrVideo = null;
                arrAudio = null;
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 특정 리소스 재생 및 멈춤
        // [ Param  ]
        // [ Return ] void
        this.PlayMultimedia = function (index) {

            try {

                if (g_arrMultimedia == null)
                    return;

                if (index < 0 || index >= g_arrMultimedia.length)
                    return;

                for (i = 0; i < g_arrMultimedia.length; i++) {
                    if (i != index) {
                        if (!g_arrMultimedia[i].paused)
                            g_arrMultimedia[i].pause();
                    }
                }

                if (g_arrMultimedia[index].paused)
                    g_arrMultimedia[index].play();

                return;
            }
            catch (ex) {
                RunaEngine.log('[★Exception★][HtmlCtrl.PlayMultimedia] ' + ex);
            }
        };
        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 특정 리소스 재생 및 멈춤
        // [ Param  ]
        // [ Return ] void
        this.PlayTimeMultimedia = function (index, current) {

            try {

                if (g_arrMultimedia == null)
                    return;

                if (index < 0 || index >= g_arrMultimedia.length)
                    return;

                for (i = 0; i < g_arrMultimedia.length; i++) {
                    if (i != index) {
                        if (!g_arrMultimedia[i].paused)
                            g_arrMultimedia[i].pause();
                    }
                }

                if (!g_arrMultimedia[index].paused && g_arrMultimedia[index].currentTime > 0) {
                    // g_arrMultimedia[index].pause();
                    g_arrMultimedia[index].currentTime = current;
                    // g_arrMultimedia[index].play();
                }
                else {
                    g_arrMultimedia[index].load();
                    g_arrMultimedia[index].oncanplay = function () {
                        this.currentTime = current;
                    }
                    g_arrMultimedia[index].play();
                }

                return;
            }
            catch (ex) {
                RunaEngine.log('[★Exception★][HtmlCtrl.PlayTimeMultimedia] ' + ex);
            }
        };
        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 멀티미디어 리소스 재생 여부 체크
        // [ Param  ]
        // [ Return ] array
        this.CheckPlayStateMultimedia = function () {
            var jsonList = "";
            var count = 0;

            jsonList = "[";

            try {
                if (g_arrMultimedia == null) {
                    jsonList += "]";
                    return jsonList;
                }

                for (i = 0; i < g_arrMultimedia.length; i++) {
                    if (g_arrMultimedia[i].duration > 0 && !g_arrMultimedia[i].paused) {
                        if (count > 0)
                            jsonList += ",";

                        jsonList += "{\"index\" : \"" + i + "\", \"duration\" : \"" + g_arrMultimedia[i].duration + "\", \"currentTime\" : \"" + g_arrMultimedia[i].currentTime + "\"}";
                        count++;
                    }
                }

                jsonList += "]";

                return jsonList;

            }
            catch (ex) {
                RunaEngine.log('[★Exception★][HtmlCtrl.CheckPlayStateMultimedia] ' + ex);
            }
            finally {
                count = null;
                jsonList = null;
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // 백지화면 제거
        ///////////////////////////////////////////////////////////////////////
        // ios - jdm
        this.addOverflowHidden = function () {
            if (RunaEngine.DeviceInfo.IsIOS) {
                document.getElementById("runaengine_columns_Layout").classList.add("overflowhidden");
            }
        }
        this.removeOverflowHidden = function () {
            if (RunaEngine.DeviceInfo.IsIOS) {
                document.getElementById("runaengine_columns_Layout").classList.remove("overflowhidden");
            }
        }

        ///////////////////////////////////////////////////////////////////////
        // 페이지 정보 제공 함수
        ///////////////////////////////////////////////////////////////////////
        this.PageInfo = new function __PageInfo() {
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 페이지번호 리턴
            // [ Param  ]
            // [ Return ] (number) : 현재 페이지번호
            this.GetCurrPageNo = function () {
                return m_CurPageNo;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 총페이지번호 리턴
            // [ Param  ]
            // [ Return ] (number) : 총페이지번호
            this.GetTotalPageCount = function () {
                return m_TotalPageCount;
            }


            this.getDevicePixelRatio = function () {
                if(RunaEngine.DeviceInfo.IsAndroidShine()) {
                    return parseInt(window.devicePixelRatio * 100) / 100.0;
                } else {
                    return window.devicePixelRatio;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 보이는 페이지의 맨 왼쪽 상단에 해당하는 CFI 값 리턴. 없으면 계산 찾음
            // [ Param  ] side : 0 - left, 1 : right
            // [ Return ] (string) : CFI
            this.GetLastReadPageCfi = function (side) {
                var strRtnArray = null;
                try {
                    strRtnArray = RunaEngine.HtmlCtrl.PageInfo._GetLastReadPageCfiImpl(side, false);
                    return strRtnArray[0];
                }
                catch (ex) {
                    return '';
                }
                finally {
                    strRtnArray = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 보이는 페이지의 맨 왼쪽 상단에 해당하는 CFI 값 리턴. 없으면 계산 찾음
            //            리턴되는 CFI를 가진 엘리먼트의 텍스트도 같이 리턴
            // [ Param  ] side : 0 - left, 1 : right
            // [ Return ] Array
            //            (string) : cfi 값
            //            (string) : text 값
            this.GetLastReadPageCfiAndText_ReturnArray = function (side) {
                var strRtnArray = null;
                try {
                    strRtnArray = RunaEngine.HtmlCtrl.PageInfo._GetLastReadPageCfiImpl(side, true);
                    return strRtnArray;
                }
                catch (ex) {
                    return ';';
                }
                finally {
                    strRtnArray = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 세로보기 함수, 현재 보이는 페이지의 맨 왼쪽 상단에 해당하는 CFI 값 리턴. 없으면 계산 찾음
            //            리턴되는 CFI를 가진 엘리먼트의 텍스트도 같이 리턴
            // [ Param  ] side : 0 - left, 1 : right
            // [ Return ] Array
            //            (string) : cfi 값
            //            (string) : text 값
            this.GetLastReadPageCfiAndText_VS_ReturnString = function (offset) {
                var strRtnArray = null;
                try {
                    strRtnArray = RunaEngine.HtmlCtrl.PageInfo._GetLastReadPageCfiImpl(offset, true);
                    if (RunaEngine.DeviceInfo.IsAndroid()) {
                        return strRtnArray;
                    }
                    else {
                        return strRtnArray[0][0] + ';' + strRtnArray[0][1] + ';' + strRtnArray[0][2] + ';' + strRtnArray[1];
                    }
                }
                catch (ex) {
                    return ';';
                }
                finally {
                    strRtnArray = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 보이는 페이지의 맨 왼쪽 상단에 해당하는 CFI 값 리턴. 없으면 계산 찾음
            //            리턴되는 CFI를 가진 엘리먼트의 텍스트도 같이 리턴
            // [ Param  ] side : 0 - left, 1 : right
            // [ Return ]
            //            (string) : IPHONE에서 어레이 처리를 못해서 구분자로 전달
            //                       주의점은 ";" 구분자로 되지만, text값에 ;이 있을수 있으니 맨처음 ;을 기준으로 잘라야함
            //                       cfi 값 + ";" + text 값
            this.GetLastReadPageCfiAndText_ReturnString = function (side) {
                var strRtnArray = null;
                try {
                    strRtnArray = RunaEngine.HtmlCtrl.PageInfo._GetLastReadPageCfiImpl(side, true);
                    return strRtnArray[0][0] + ';' + strRtnArray[0][1] + ';' + strRtnArray[0][2] + ';' + strRtnArray[1];
                }
                catch (ex) {
                    return strRtnArray;
                }
                finally {
                    strRtnArray = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 보이는 페이지의 맨 왼쪽 상단에 해당하는 CFI 값 리턴
            // [ Param  ] side : 0 - left, 1 : right , 세로 보기 시에는 offet 값
            // [ Param  ] isIncludeText : 현재 선택된 노드의 텍스트를 가져오기
            // [ Return ] Array
            //            (string) : cfi 값
            //            (string) : text 값
            this._GetLastReadPageCfiImpl = function (side, isIncludeText) {
                //RunaEngine.log('△△△△△△      CALL  _GetLastReadPageCfiImpl     △△△△△△');

                var e = null;
                var i = null;
                var gap = null;
                var isSelectionLayerShow = null;
                var caretRangeStart = null;
                var caretRangeEnd = null;
                var strRtnArray = new Array();
                var range = null;
                var maxNumber = 30; //가능한 20글자씩 잘라낼것임
                var strRtnLog_Debug = null;

                try {
                    strRtnArray[0] = new Array(); //'cfi'
                    strRtnArray[1] = ''; //'text'

                    /**
                     * redmine refs #20510,20494
                     * 챕터가 큰 컨텐츠와 하드웨어 렌더링이 되는 단말의 경우 페이지 넘김 느려지는 문제 수정
                     * by junghoon.kim
                     */

                    // // Selection Layer Hide
                    // isSelectionLayerShow = RunaEngine.HtmlCtrl.Selection.IsShowSelection();
                    // if (isSelectionLayerShow == true) {
                    //     RunaEngine.HtmlCtrl.Selection.HideSelection();
                    // }

                    // // HighLighting Layer Hide
                    // RunaEngine.HtmlCtrl.Highlight.HideAll();

                    if (RunaEngine.DeviceInfo.IsAndroid()) {
                        gap = (m_PageWidth * side);
                    }
                    else if (RunaEngine.DeviceInfo.isPC()) {
                        gap = (m_PageWidth * side);
                    }
                    else {
                        if (RunaEngine.DeviceInfo.IsIOS4()) {
                            // IOS 4버전에서는 document.elementFromPoint 를 처리함에 있어서
                            // ViewPoint일 때, 안드로이드처럼 표준을 안지키고, 현재보이는 포지션을 정확히 계산해야만 한다.
                            // 이것은 버그로 보이며, NULL을 리턴하는 것을 기준으로 확인할 수 있다고도 한다.
                            gap = m_PageWidth * (m_CurPageNo - 1);
                            //RunaEngine.log( 'gap : ' + gap + ' , pw : ' + m_PageWidth + ' , curr : ' + m_CurPageNo);
                        }
                        else {
                            // IOS 5 이상
                            gap = (m_PageWidth * side);
                        }
                    }

                    //RunaEngine.log("_GetLastReadPageCfiImpl !!!!");
                    if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                        if (RunaEngine.DeviceInfo.IsAndroid()) {
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, m_ColPaddingWidth, side);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - 1, side + m_PageHeight - 1);
                        }
                        else if (RunaEngine.DeviceInfo.isPC()) {
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, m_ColPaddingWidth, m_ColPaddingWidth + side);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - (m_ColPaddingWidth * 2), side + m_PageHeight - (m_ColPaddingWidth * 3));
                        } else {
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, 1, m_ColPaddingWidth + side);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - 3 - (m_ColPaddingWidth * 3), side + (m_PageHeight - 23));
                        }
                    }
                    else {

                        if (RunaEngine.DeviceInfo.IsAndroid()) {
                            if (RunaEngine.DeviceInfo.IsAndroid_PAD() && RunaEngine.HtmlCtrl.getOrientation() == 1) {
                                caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, (side ? m_PageWidth : 0), 0);
                                caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, (side ? (m_PageWidth + m_PageWidth - 1) : m_PageWidth - 1), m_PageHeight - 1);
                            }
                            else {
                                caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, m_ColPaddingWidth, m_ColPaddingWidth);
                                caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - (m_ColPaddingWidth * 2), m_PageHeight - (m_ColPaddingWidth * 3));
                            }
                        }
                        else if (RunaEngine.DeviceInfo.isPC()) {
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, m_ColPaddingWidth, m_ColPaddingWidth);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - 3 - (m_ColPaddingWidth * 2), m_PageHeight - 23);
                        }
                        else {
                            // ios 보정 너무 빡빡하게 잡으면 null이 나오는 경우가 많아서 -3씩 보정함 ios 11 대비 하단은 23보정
                            if ((RunaEngine.DeviceInfo.IsIPAD() && RunaEngine.HtmlCtrl.getOrientation() == 0) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.IsOSX())) {
                                caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, (side ? m_PageWidth - 3 : 1), 1);
                                caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, (side ? (m_PageWidth - 3 + m_PageWidth - 1) : m_PageWidth - 1), m_PageHeight - 1 - 23);
                            }
                            else {
                                caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, 1, 1);
                                caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - 3 - (m_ColPaddingWidth * 2), m_PageHeight - 23);
                            }
                        }
                    }

                    /**
                     * redmine refs #20510,20494
                     * 챕터가 큰 컨텐츠와 하드웨어 렌더링이 되는 단말의 경우 페이지 넘김 느려지는 문제 수정
                     * by junghoon.kim
                     */
                    // RunaEngine.HtmlCtrl.Highlight.ShowAll();

                    if (caretRangeStart == null || caretRangeEnd == null) {
                        if(isIncludeText) {
                            RunaEngine.sublog2(' PageInfo._GetLastReadPageCfiImpl: null' + caretRangeStart +', '+ caretRangeEnd);
                            strRtnLog_Debug = '스캔해도 CFI를 찾을 수 없음';
                            return strRtnArray;
                        }
                        else {
                            if(caretRangeStart == null)
                            {
                                RunaEngine.sublog2(' PageInfo._GetLastReadPageCfiImpl: null' + caretRangeStart +', '+ caretRangeEnd);
                                strRtnLog_Debug = '스캔해도 CFI를 찾을 수 없음';
                                return strRtnArray;
                            }
                        }
                    }

                    if (isIncludeText) {
                        range = document.createRange();
                        range.setStart(caretRangeStart.startContainer, caretRangeStart.startOffset);
                        range.setEnd(caretRangeEnd.endContainer, caretRangeEnd.endOffset);

                        strRtnArray[1] = range.toString();
                        strRtnArray[1] = strRtnArray[1].replace(/\n\n/gi, '');

                        if (strRtnArray[1].length > caretRangeStart.startOffset && strRtnArray[1].length > (maxNumber + caretRangeStart.startOffset)) {
                            strRtnArray[1] = strRtnArray[1].substr(0, maxNumber);
                        }

                        RunaEngine.log(' PageInfo._GetLastReadPageCfiImpl : ' + strRtnArray[1]);
                    }

                    i = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, caretRangeStart.startContainer, caretRangeStart.startOffset, "");

                    if (i != null) {
                        strRtnArray[0][2] = caretRangeStart.startOffset;
                        strRtnArray[0][1] = RunaEngine.HtmlCtrl.DPControl._encodeXPath(document, caretRangeStart.startContainer);
                        RunaEngine.log('[PageInfo._GetLastReadPageCfiImpl] 2 getlastcfi ' + i + ' WWW:' + m_PageWidth + ' HHH:' + m_PageHeight);
                        strRtnLog_Debug = i;
                        strRtnArray[0][0] = i;
                        return strRtnArray;
                    }
                    else {
                        RunaEngine.log(' PageInfo._GetLastReadPageCfiImpl: null');
                        strRtnLog_Debug = '스캔해도 CFI를 찾을 수 없음';
                        return strRtnArray;
                    }

                }
                catch (ex) {
                    RunaEngine.log('[★Exception★][PageInfo._GetLastReadPageCfiImpl] ' + (ex.number & 0xFFFF));
                    RunaEngine.sublog2('[★Exception★][PageInfo._GetLastReadPageCfiImpl] ' + ex);
                    strRtnLog_Debug = '예외로 인한 실패';
                    return strRtnArray;
                }
                finally {
                    e = null;
                    i = null;
                    caretRangeStart = null;
                    caretRangeEnd = null;
                    gap = null;
                    range = null;

                    /**
                     * redmine refs #20510,20494
                     * 챕터가 큰 컨텐츠와 하드웨어 렌더링이 되는 단말의 경우 페이지 넘김 느려지는 문제 수정
                     * by junghoon.kim
                     */
                    // RunaEngine.HtmlCtrl.Highlight.ShowAll();
                    // if (isSelectionLayerShow == true) {
                    //     RunaEngine.HtmlCtrl.Selection.ShowSelection();
                    // }

                    isSelectionLayerShow = null;
                    strRtnArray = null;

                    if (strRtnLog_Debug != null) {
                        //RunaEngine.log('△△△△△△      [PageInfo.GetLastReadPageCfi] Return CFI : ' + strRtnLog_Debug + '    △△△△△△');
                    }
                    strRtnLog_Debug = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] GetLastReadPageCfiAndText의 텍스트 추출 함수
            // [ Param  ] (string) : CFI
            //            (node) : target DomElement
            // [ Return ] (string) : 추출된 텍스트
            this._GetTextFromCfiAndTargetNode = function (startcfi, endcfi, target) {
                var strText = '';
                var rtnDraw = null;
                var strCfiNext = null;

                try {

                    RunaEngine.sublog('_GetTextFromCfiAndTargetNode : ' + startcfi);
                    RunaEngine.sublog('_GetTextFromCfiAndTargetNode : ' + endcfi);
                    RunaEngine.sublog('_GetTextFromCfiAndTargetNode : ' + target.id);

                    var maxNumber = 30; //가능한 20글자씩 잘라낼것임


                    if ((startcfi.indexOf(':') > -1 || (endcfi != null && endcfi.indexOf(':') > -1))
                        && target.innerText != null
                        && target.innerText.length != 0
                        && target.innerText.search(/[^ \n\r\t\f\x0b\xa0]/) != -1) {
                        var num = parseInt(startcfi.split(':')[1]);

                        rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(startcfi, endcfi, true, true);

                        if (rtnDraw.constructor == Array) {
                            RunaEngine.sublog('selNode ---- : ' + '(000)' + rtnDraw[0]['SelectionText']);
                            strText = rtnDraw[0]['SelectionText'].toString().replace('/[\r\n]/g', '');
                        }
                        else {
                            strText = '';
                        }

                        if (strText.length > maxNumber) {
                            //RunaEngine.sublog( '1 - num:' + num + ', maxNumber:' + maxNumber + ', text:' + target.innerText.substr(num,maxNumber ) + ', ori_text:' + target.innerText );
                            return strText.substr(0, maxNumber);
                        }
                        else {
                            //RunaEngine.sublog('2- num:' + num + ', maxNumber:' + maxNumber + ', len:' + target.innerText.length + ', sublen:' + (target.innerText.length - num) + ', text:' + target.innerText.substr(num, target.innerText.length - num) + ', ori_text:' + target.innerText);
                            return strText;
                        }

                    }
                    else {
                        // 하위 자식 태그들을 찾아 본다.
                        var text = this._GetTextFromTargetNode(target, maxNumber);
                        if (text != undefined && text != '') {
                            return text.replace('/[\r\n]/g', '');
                        }

                        // 하필 빈 <p>태그, <img> 등등이면.. 다음 녀석을 찾아가본다.
                        var next_target = target.nextSibling;

                        // 동일레벨 오른쪽것을 뒤지다 없으면 부모의 오른쪽 것을 뒤진다.(여기서 부터는 content div 는 괜찮지만, column div는 종료. 더이상 없음)
                        var prev_target = target;
                        while (true) {
                            //RunaEngine.sublog('prev : ' + prev_target + '    ' + prev_target.outerHTML + '    ' + prev_target.innerHTML + '    ' + prev_target.innerText);
                            //RunaEngine.sublog('next : ' + next_target + '    ' + next_target.outerHTML + '    ' + next_target.innerHTML + '    ' + next_target.innerText);

                            if (next_target == undefined || next_target == null) {
                                next_target = prev_target.parentNode;
                                if (next_target == m_divColumnLayout) {
                                    return '';
                                }
                            }

                            if (next_target.innerText != null
                                && next_target.innerText.search(/[^ \n\r\t\f\x0b\xa0]/) != -1) {
                                if (next_target.innerText.length < maxNumber) {
                                    RunaEngine.sublog('3- num:' + 0 + ', maxNumber:' + next_target.innerText.length + ', text:' + next_target.innerText + ', ori_text:' + next_target.innerText);
                                    return next_target.innerText;
                                }
                                else {
                                    RunaEngine.sublog('4- num:' + 0 + ', maxNumber:' + maxNumber + ', text:' + next_target.innerText.substr(0, maxNumber) + ', ori_text:' + next_target.innerText);
                                    return next_target.innerText.substr(0, maxNumber);
                                }
                            }

                            prev_target = next_target;
                            next_target = next_target.nextSibling;
                        }

                        return '';
                    }
                }
                catch (ex) {
                    RunaEngine.sublog('[★Exception★][PageInfo._GetTextFromCfiAndTargetNode] ' + (ex.number & 0xFFFF));
                    RunaEngine.sublog('[★Exception★][PageInfo._GetTextFromCfiAndTargetNode] ' + ex);

                    try {
                        return target.innerText;
                    }
                    catch (ex) {
                        return '';
                    }
                }
            }

            this._GetTextFromTargetNode = function (target, maxNumber) {


                for (var a = 0; a < target.childNodes.length; a++) {
                    var childNode = target.childNodes[a];

                    RunaEngine.sublog('childNode ---- : ' + childNode + '    ' + childNode.outerHTML + '    ' + childNode.innerHTML + '    ' + childNode.innerText);

                    if (childNode.innerText != null
                        && childNode.innerText.search(/[^ \n\r\t\f\x0b\xa0]/) != -1) {
                        if (childNode.innerText.length < maxNumber) {
                            RunaEngine.sublog('5- num:' + 0 + ', maxNumber:' + childNode.innerText.length + ', text:' + childNode.innerText + ', ori_text:' + childNode.innerText);
                            return childNode.innerText;
                        }
                        else {
                            RunaEngine.sublog('6- num:' + 0 + ', maxNumber:' + maxNumber + ', text:' + childNode.innerText.substr(0, maxNumber) + ', ori_text:' + childNode.innerText);
                            return childNode.innerText.substr(0, maxNumber);
                        }
                    }

                    if (childNode.childNodes.length > 0) {
                        var text = this._GetTextFromTargetNode(childNode, maxNumber);

                        if (text != undefined && text != '') {
                            return text;
                        }
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] CFI를 포함하는 페이지번호 리턴
            // [ Param  ] (string) : CFI
            // [ Return ] (number) : CFI를 포함하는 페이지 번호
            this.GetCfiPageNumber = function (cfi) {
                //RunaEngine..log('    GetCfiPageNumber : cfi = ' + cfi + '          PageCount = ' + m_CurPageNo + '/' + RunaEngine.HtmlCtrl.getTotalPage() + ', Width : '+RunaEngine.HtmlCtrl.getPageWidth());
                var rtnDraw = null;
                var nMovePageNo = null;
                var strCfiNext = null;
                var di = null;
                var maxLeft = null;
                var maxTop = null;

                try {
                  
                    // 원래 IOS4에서는
                    // rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(cfi,cfi,false,true); 을 사용하고, IOS5 부터, 아래처럼 변경해서 사용하면 되는 거였는데, 잠수함패치당한듯 하다. 4.3버전에서부터는 전부 아래코드로 해결됨
                    strCfiNext = RunaEngine.HtmlCtrl.Highlight._PlusCfiCharIndex(cfi, 1);
                    if (strCfiNext.indexOf(':0') > -1 && cfi.indexOf(':1') > -1) {
                        // 1인 상태에서 Plus하려했으나, 뒤가 없어서 뺐음.(마지막글자임) 이런경우 뒤집음.
                        rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(strCfiNext, cfi, false, true);
                    }
                    else {
                        rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(cfi, strCfiNext, false, true);
                    }
                

                    if (rtnDraw.constructor == Array) {
                        // left value is like '8px'
                        if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                            maxTop = 0;
                            for (di = 0; di < rtnDraw.length; di++) {
                                //RunaEngine.loge.log('[PageInfo.GetCfiPageNumber] ***  top : ' + parseInt(rtnDraw[di]['top'],10));
                                if (maxTop < parseInt(rtnDraw[di]['top'], 10)) {
                                    maxTop = parseInt(rtnDraw[di]['top'], 10);
                                }
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw['+di+'][left] =' + rtnDraw[di]['left']);
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw['+di+'][top] =' + rtnDraw[di]['top']);
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw['+di+'][width] =' + rtnDraw[di]['width']);
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw['+di+'][height] =' + rtnDraw[di]['height']);
                            }

                            nMovePageNo = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(maxTop) - 1;
                            //RunaEngine..log('[1 PageInfo.GetCfiPageNumber] nMovePageNo : (' + nMovePageNo + ') maxLeft :' + maxLeft );

                            if (isNaN(nMovePageNo) == true) {
                                //RunaEngine.log('[-------------------------------Ciytical Error----------------------------]' );
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] *error GetCfiPageNumber : DrawNodeByCfi is return (' + rtnDraw + ')' );
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] *강제로 1Page로 이동' );
                                //RunaEngine.log('[-------------------------------Ciitical Error----------------------------]' );
                                nMovePageNo = 0;
                            }
                        }
                        else {
                            maxLeft = 0;
                            for (di = 0; di < rtnDraw.length; di++) {
                                if (maxLeft < parseInt(rtnDraw[di]['left'], 10)) {
                                    //RunaEngine..log('left : ' +rtnDraw[di]['left']);
                                    maxLeft = parseInt(rtnDraw[di]['left'], 10);
                                }
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw[' + di + '][left] =' + rtnDraw[di]['left']);
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw[' + di + '][top] =' + rtnDraw[di]['top']);
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw[' + di + '][width] =' + rtnDraw[di]['width']);
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] rtnDraw[' + di + '][height] =' + rtnDraw[di]['height']);
                            }
                            //RunaEngine.log('[6 PageInfo.GetCfiPageNumber] ***  '+nMovePageNo);
                            //RunaEngine.log('[6 PageInfo.GetCfiPageNumber] ***  '+nMovePageNo);
                            //RunaEngine.log('[6 PageInfo.GetCfiPageNumber] ***  '+nMovePageNo);
                            //RunaEngine.log('[6 PageInfo.GetCfiPageNumber] ***  '+maxLeft + "][" + RunaEngine.HtmlCtrl.getPageWidth());

                            nMovePageNo = parseInt(maxLeft / RunaEngine.HtmlCtrl.getPageWidth(), 10);
                            //RunaEngine..log('[2 PageInfo.GetCfiPageNumber] nMovePageNo : (' + nMovePageNo + ') maxLeft :' + maxLeft);
                            //RunaEngine.log('[6 PageInfo.GetCfiPageNumber] ***  '+nMovePageNo);
                            if (isNaN(nMovePageNo) == true) {
                                //RunaEngine.log('[-------------------------------Ciytical Error----------------------------]');
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] *error GetCfiPageNumber : DrawNodeByCfi is return (' + rtnDraw + ')');
                                //RunaEngine.log('[PageInfo.GetCfiPageNumber] *강제로 1Page로 이동');
                                //RunaEngine.log('[-------------------------------Ciitical Error----------------------------]');
                                nMovePageNo = 0;
                            }
                        }
                    }
                    else {
                        //RunaEngine.log('[-------------------------------Ciytical Error----------------------------]');
                        //RunaEngine.log('[PageInfo.GetCfiPageNumber] error GetCfiPageNumber : DrawNodeByCfi is return (' + rtnDraw + ')');
                        //RunaEngine.log('[PageInfo.GetCfiPageNumber] 강제로 1Page로 이동');
                        //RunaEngine.log('[-------------------------------Ciitical Error----------------------------]');
                        nMovePageNo = 0;
                    }

                    return parseInt(nMovePageNo, 10) + 1;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo.GetCfiPageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo.GetCfiPageNumber] ' + ex);
                    return '';
                }
                finally {
                    rtnDraw = null;
                    nMovePageNo = null;
                    strCfiNext = null;
                    di = null;
                    maxLeft = null;
                    maxTop = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] CFI를 포함하는 페이지번호 및 위치 리턴
            // [ Param  ] (string) : CFI
            // [ Return ] (string) : CFI를 포함하는 페이지 번호 와 위치 정보 (CFI;LEFT;TOP)
            this.GetCfiPageNumberWithOffset = function (cfi) {

                var rtnDraw = null;
                var nMovePageNo = null;
                var strCfiNext = null;
                var di = null;
                var maxLeft = 0;
                var maxTop = 0;

                try {
                   
                    // 원래 IOS4에서는
                    // rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(cfi,cfi,false,true); 을 사용하고, IOS5 부터, 아래처럼 변경해서 사용하면 되는 거였는데, 잠수함패치당한듯 하다. 4.3버전에서부터는 전부 아래코드로 해결됨
                    strCfiNext = RunaEngine.HtmlCtrl.Highlight._PlusCfiCharIndex(cfi, 1);
                    if (strCfiNext.indexOf(':0') > -1 && cfi.indexOf(':1') > -1) {
                        // 1인 상태에서 Plus하려했으나, 뒤가 없어서 뺐음.(마지막글자임) 이런경우 뒤집음.
                        rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(strCfiNext, cfi, false, true);
                    }
                    else {
                        rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(cfi, strCfiNext, false, true);
                    }
                    

                    if (rtnDraw.constructor == Array) {
                        // left value is like '8px'
                        if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                            maxTop = 0;
                            for (di = 0; di < rtnDraw.length; di++) {
                                if (maxTop < parseInt(rtnDraw[di]['top'], 10)) {
                                    maxTop = parseInt(rtnDraw[di]['top'], 10);
                                }
                            }

                            nMovePageNo = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(maxTop) - 1;

                            if (isNaN(nMovePageNo) == true) {
                                nMovePageNo = 0;
                            }
                        }
                        else {
                            maxLeft = 0;
                            for (di = 0; di < rtnDraw.length; di++) {
                                if (maxLeft < parseInt(rtnDraw[di]['left'], 10)) {
                                    //RunaEngine..log('left : ' +rtnDraw[di]['left']);
                                    maxLeft = parseInt(rtnDraw[di]['left'], 10);
                                }
                            }

                            nMovePageNo = parseInt(maxLeft / RunaEngine.HtmlCtrl.getPageWidth(), 10);

                            if (isNaN(nMovePageNo) == true) {
                                nMovePageNo = 0;
                            }
                        }
                    }
                    else {
                        nMovePageNo = 0;
                    }

                    return "" + (parseInt(nMovePageNo, 10) + 1) + ";" + maxLeft + ";" + maxTop;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo.GetCfiPageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo.GetCfiPageNumber] ' + ex);
                    return '';
                }
                finally {
                    rtnDraw = null;
                    nMovePageNo = null;
                    strCfiNext = null;
                    di = null;
                    maxLeft = null;
                    maxTop = null;
                }
            }

            this.GetNodePageNumber = function (node) {
                var rtnDraw = null;
                var nMovePageNo = null;
                try {
                    rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawInfoByNode(node);
                    if (rtnDraw.constructor == Array) {
                        // left value is like '8px'
                        if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                            nMovePageNo = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(parseInt(rtnDraw[0]['top'], 10)) - 1;
                        }
                        else {
                            nMovePageNo = parseInt(parseInt(rtnDraw[0]['left'], 10) / m_PageWidth, 10);
                        }
                        //RunaEngine.log('[PageInfo.GetNodePageNumber] GetNodePageNumber Left ' + rtnDraw[0]['left'] + '   nMovePageNo : ' + parseInt(nMovePageNo,10)+1);
                    }
                    else {
                        return -1;
                        //RunaEngine.log('[PageInfo.GetNodePageNumber] GetNodePageNumber MoveCfiPage');
                    }
                    return parseInt(nMovePageNo, 10) + 1;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo.GetNodePageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo.GetNodePageNumber] ' + ex.description);
                    return -1;
                }
                finally {
                    rtnDraw = null;
                    nMovePageNo = null;
                }
            }

            this.GetNodePageNumberWithOffset = function (node) {
                var rtnDraw = null;
                var nMovePageNo = null;
                var maxTop = 0;
                var maxLeft = 0;
                var rtnVal = null;

                try {
                    rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawInfoByNode(node);
                    if (rtnDraw.constructor == Array) {

                        maxTop = parseInt(rtnDraw[0]['top'], 10);
                        maxLeft = parseInt(rtnDraw[0]['left'], 10);

                        // left value is like '8px'
                        if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                            nMovePageNo = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(maxTop) - 1;
                        }
                        else {
                            nMovePageNo = parseInt(maxLeft / m_PageWidth, 10);
                        }
                        //RunaEngine.log('[PageInfo.GetNodePageNumber] GetNodePageNumber Left ' + rtnDraw[0]['left'] + '   nMovePageNo : ' + parseInt(nMovePageNo,10)+1);
                    }
                    else {
                        rtnVal = { page: -1, left: maxLeft, top: maxTop };
                        return rtnVal;
                        //RunaEngine.log('[PageInfo.GetNodePageNumber] GetNodePageNumber MoveCfiPage');
                    }
                    rtnVal = { page: parseInt(nMovePageNo, 10) + 1, left: maxLeft, top: maxTop };
                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo.GetNodePageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo.GetNodePageNumber] ' + ex.description);
                    rtnVal = { page: -1, left: maxLeft, top: maxTop };
                    return rtnVal;
                }
                finally {
                    rtnDraw = null;
                    nMovePageNo = null;
                    maxTop = null;
                    maxLeft = null;
                    rtnVal = null;
                }
            }


            this.GetImageSrc = function (posX, posY) {
                var selnode = document.elementFromPoint(posX, posY);
                var imgpath = '';
                var selnodename = '';

                try {

                    if (selnode != null) {
                        selnodename = this._NodeName(selnode);

                        if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
                            return '';
                        }

                        if (selnode == m_divContentLayout ||
                            selnode == m_divColumnLayout ||
                            selnode == m_divLastBlankPageWithVerticalLayout) {
                            return '';
                        }

                        if (selnodename == "img") {
                            imgpath = selnode.src;
                        }
                    }

                    return imgpath;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo.GetImageSrc] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo.GetImageSrc] ' + ex);
                    return '';
                }
                finally {
                    selnode = null;
                    selnodename = null;
                    imgpath = null;
                }
            }

            this.GetHitTest = function (posX, posY) {
                //                var canvas = document.getElementById("canvas");
                //                var ctx = canvas.getContext("2d");
                //                ctx.fillStyle = "#ff2626"; // Red color
                //                ctx.fillRect(posX, posY, 5, 5);

                var result;
                var selnode = document.elementFromPoint(posX, posY);
                RunaEngine.log('[★★★★★★★★★★★★★★]' + posX + ' , ' + posY + ' , ' + selnode);

                var hasInteractive = false;

                try {

                    result = RunaEngine.HtmlCtrl.PageInfo._CheckInteractive(selnode);

                    if (RunaEngine.DeviceInfo.isPC()) {
                        return result.hasInteractive;
                    }
                    else {
                        return JSON.stringify(result);
                    }
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo.GetHitTest] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo.GetHitTest] ' + ex);
                    return false;
                }
            }

            this._CheckInteractive = function (node) {
                var selnodename = '';
                var selnode = node;
                var hasInteractive = false;
                var pselnode = null;
                var pselnodename = '';
                var result;

                try {

                    while (selnode != null || !hasInteractive) {
                        selnodename = this._NodeName(selnode);

                        RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] -- '+ selnode +', '+selnodename +', '+ selnode.innerHTML);

                        if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
                            break;
                        }

                        if (selnode == m_divContentLayout ||
                            selnode == m_divColumnLayout ||
                            selnode == m_divLastBlankPageWithVerticalLayout) {
                            RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] -- '+ selnode.id);
                            break;
                        }

                        if (selnodename == 'a') {
                            var c = selnode.getAttribute("href");

                            if (c != null) {
                                c = c.toLowerCase();
                                c = c.replace(/\s/g, "");

                                RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] a tag -- '+ c);

                                if (c != "#" && c != "javascript:void(0)" && c != "javascript:void(0);") {
                                    RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] 11');
                                    result = {hasInteractive: true, nodeName: selnode.nodeName, src: c};
                                    return result;
                                }
                            }
                        }
                        RunaEngine.log('[PageInfo._CheckInteractive] - ['+selnode.ontouchstart+', '+selnode.touchstart+']');

                        if (selnodename == "audio" || selnodename == "video" || selnodename == "embed" || selnodename == "object" || selnodename == "canvas") {
                            RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] -- multimedia');
                            result = {hasInteractive: true, nodeName: selnode.nodeName, src: selnode.src};
                            return result;
                        }

                        if (selnode.onmouseup != null || selnode.onclick != null || selnode.ondblclick != null || selnode.ontouchstart != null || selnode.ontouchmove != null || selnode.ontouchend != null) {
                            RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] -- onclick');
                            result = {hasInteractive: true, nodeName: selnode.nodeName, src: ""};
                            return result;
                        }

                        var nodescrollstyle = document.defaultView.getComputedStyle(selnode, null);

                        if (nodescrollstyle != null) {
                            if (nodescrollstyle["overflow-x"] == "scroll" || nodescrollstyle["overflow-x"] == "auto") {
                                if (selnode.scrollWidth > selnode.clientWidth && !RunaEngine.DeviceInfo.IsAndroid()) {
                                    hasInteractive = true;
                                    RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] 22');
                                    result = {hasInteractive: hasInteractive, nodeName: selnode.nodeName, src: ""};
                                    return result;
                                }
                            }

                            if (!hasInteractive
                                && (nodescrollstyle["overflow-y"] == "scroll" || nodescrollstyle["overflow-y"] == "auto")) {
                                if (selnode.scrollHeight > selnode.clientHeight && !RunaEngine.DeviceInfo.IsAndroid()) {
                                    hasInteractive = true;
                                    RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] 33 scrollHeight = ' + selnode.scrollHeight + ' clientHeight =' + selnode.clientHeight);
                                    result = {hasInteractive: hasInteractive, nodeName: selnode.nodeName, src: ""};
                                    return result;
                                }
                            }
                        }


                        if (!hasInteractive) {
                            try {
                                if ($(selnode).data("events")) {
                                    hasInteractive = true;
                                    result = {hasInteractive: hasInteractive, nodeName: selnode.nodeName, src: ""};
                                    return result;
                                }
                            } catch (err) { }
                        }

                        // over jQuery 1.8
                        if (!hasInteractive) {
                            try {
                                if ($._data(selnode, "events")) {
                                    hasInteractive = true;
                                    result = {hasInteractive: hasInteractive, nodeName: selnode.nodeName, src: ""};
                                    return result;
                                }
                            } catch (err) { }
                        }


                        if (selnodename == "img") {
                            if (selnode.parentNode != null) {
                                pselnode = selnode.parentNode;
                                pselnodename = this._NodeName(pselnode);

                                if (pselnodename == 'a') {
                                    var c = pselnode.getAttribute("href");

                                    if (c != null) {
                                        c = c.toLowerCase();
                                        c = c.replace(/\s/g, "");

                                        RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] a tag -- '+ c);

                                        if (c != "#" && c != "javascript:void(0)"
                                            && c != "javascript:void(0);") {
                                            result = {hasInteractive: true, nodeName: selnode.nodeName, src: c};
                                            return result;
                                        }
                                    }
                                }
                            }
                        }


                        if (selnode.parentNode != null) {
                            result = RunaEngine.HtmlCtrl.PageInfo._CheckInteractive(selnode.parentNode);
                            hasInteractive = result.hasInteractive;

                            if (hasInteractive) {
                                return result;
                            }

                        }

                        if (selnode.children.length > 0 && selnodename == "div") {
                            // for (var i = selnode.children.length - 1; i >= 0; i--) {
                            //     hasInteractive = RunaEngine.HtmlCtrl.PageInfo._CheckInteractive(selnode.children[i]);

                            //     //RunaEngine.log('[*********][PageInfo._CheckInteractive] -- '+ selnode.children[i] +' , '+ selnode.children[i].innerHTML);

                            //     if(hasInteractive)
                            //         return hasInteractive;
                            // }
                            break;
                        }
                        else {
                            break;
                        }

                    }

                    result = {hasInteractive: hasInteractive, nodeName: selnode.nodeName, src: ""};
                    return result;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo._CheckInteractive] ' + ex);
                    result = {hasInteractive: false, nodeName: '', src: ""};
                    return result;
                }
                finally {
                    selnodename = '';
                    selnode = null;
                    pselnode = null;
                    pselnodename = '';
                }

            }

            this._NodeName = function (node) {
                if (node == null) {
                    return null;
                }

                var nodename = node.nodeName;

                if (nodename != null) {
                    if (nodename != null) {
                        nodename = nodename.toLowerCase();
                    }
                }
                return nodename;
            }


            this.GetHrefInfo = function (posX, posY) {

                RunaEngine.HtmlCtrl.Highlight.HideAll();

                var selnode = document.elementFromPoint(posX, posY);
                var strHrefInfo = '';
                var caretRangeStart = null;
                var caretRangeEnd = null;
                var range = null;
                var rangeRect = null;
                var documentFragment = null;

                try {
                    strHrefInfo = RunaEngine.HtmlCtrl.PageInfo._GetHrefInfo(selnode);

                    return strHrefInfo;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo.GetHrefInfo] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo.GetHrefInfo] ' + ex);
                    return '';
                }
                finally {
                    caretRangeStart = null;
                    caretRangeEnd = null;
                    strHrefInfo = '';
                    selnode = null;
                    RunaEngine.HtmlCtrl.Highlight.ShowAll();
                }
            }

            this._GetHrefInfo = function (node) {
                var selnodename = '';
                var selnode = node;
                var strHrefInfo = '';
                var pselnode = null;
                var pselnodename = '';

                try {

                    while (selnode != null || strHrefInfo != '') {
                        selnodename = this._NodeName(selnode);

                        //RunaEngine.log('[★Exception★][PageInfo._GetHrefInfo] -- '+ selnode +', '+selnodename +', '+ selnode.innerHTML);

                        if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
                            break;
                        }

                        if (selnode == m_divContentLayout ||
                            selnode == m_divColumnLayout ||
                            selnode == m_divLastBlankPageWithVerticalLayout) {
                            //RunaEngine.log('[★Exception★][PageInfo._GetHrefInfo] -- '+ selnode.id);
                            break;
                        }

                        if (selnodename == 'a') {
                            var c = selnode.getAttribute("href");
                            var d = selnode.getAttribute("href");

                            if (c != null) {
                                c = c.toLowerCase();
                                c = c.replace(/\s/g, "");

                                //RunaEngine.log('[★Exception★][PageInfo._GetHrefInfo] a tag -- '+ c);

                                if (c != "#" && c != "javascript:void(0)"
                                    && c != "javascript:void(0);") {
                                    return d;
                                }
                            }
                        }

                        if (selnodename == "img") {
                            if (selnode.parentNode != null) {
                                pselnode = selnode.parentNode;
                                pselnodename = this._NodeName(pselnode);

                                if (pselnodename == 'a') {
                                    var c = pselnode.getAttribute("src");
                                    var d = selnode.getAttribute("src");

                                    if (c != null) {
                                        c = c.toLowerCase();
                                        c = c.replace(/\s/g, "");

                                        //RunaEngine.log('[★Exception★][PageInfo._GetHrefInfo] a tag -- '+ c);

                                        if (c != "#" && c != "javascript:void(0)"
                                            && c != "javascript:void(0);") {
                                            return d;
                                        }
                                    }
                                }
                            }
                        }


                        if (selnode.parentNode != null) {
                            strHrefInfo = RunaEngine.HtmlCtrl.PageInfo._GetHrefInfo(selnode.parentNode);
                        }

                        break;

                    }

                    return strHrefInfo;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageInfo._GetHrefInfo] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageInfo._GetHrefInfo] ' + ex);
                    return '';
                }
                finally {
                    selnodename = '';
                    selnode = null;
                    pselnode = null;
                    pselnodename = '';
                    strHrefInfo = '';
                }

            }
        };

        ///////////////////////////////////////////////////////////////////////
        // 페이지 이동
        //
        // 공통사항 : return page umber 시 -1이면 이동실패를 뜻함
        ///////////////////////////////////////////////////////////////////////
        this.PageNavi = new function __PageNavi() {
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] CF를 포함하는 페이지로 이동 (페이지 나눈것에 맞춰서 이동됨)
            // [ Param  ] (string) : CFI
            // [ Return ] (number) : 이동한 페이지
            this.MoveCfiPage = function (cfi) {

                startPoint = RunaEngine.HtmlCtrl.DPControl._pointFromCFI(cfi);

                if (startPoint == null) {
                    RunaEngine.sublog2('MoveCfiPage error = -5');
                    return m_CurPageNo; //계산할 수 없습니다.
                }

                start = startPoint.point;

                if (start == null) {
                    startPoint = null;
                    RunaEngine.sublog2('MoveCfiPage error = -6');
                    return m_CurPageNo; //계산할 수 없습니다.
                }

                if (cfi.indexOf(':') > 0) {
                    page = RunaEngine.HtmlCtrl.PageInfo.GetCfiPageNumber(cfi);
                }
                else {
                    if (start.node.nodeType == 3) {
                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(start.node.parentNode);
                    }
                    else {
                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(start.node);
                    }
                }

                //val = RunaEngine.HtmlCtrl.AVTTSEngine.FindSentence(cfi, start.node.textContent, true);

                return this.MoveTargetPage(page);
            }

            this.MoveNextPage = function () {
                m_arrPageCFIForOnlyChangeLayout = '';
                return this.MoveTargetPage(parseInt(m_CurPageNo, 10) + 1);
            }

            this.MovePrevPage = function () {
                m_arrPageCFIForOnlyChangeLayout = '';
                return this.MoveTargetPage(parseInt(m_CurPageNo, 10) - 1);
            }

            this.MoveFirstPage = function () {
                m_arrPageCFIForOnlyChangeLayout = '';
                return this.MoveTargetPage(1);
            }

            this.MoveLastPage = function () {
                m_arrPageCFIForOnlyChangeLayout = '';
                return this.MoveTargetPage(RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount());
            }

            this.MoveTargetPage = function (nPageNum) {
                RunaEngine.log("TotalPageCount ===== " + RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount());
                if (nPageNum <= 0 || nPageNum > RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount()) {
                    return -1;
                }

                m_CurPageNo = nPageNum;
                m_CurScrollPos = (nPageNum - 1) * m_PageWidth;

                RunaEngine.HtmlCtrl.PauseAllMultimedia(0);

                if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                    var arrPageInfo = RunaEngine.HtmlCtrl.GetPageInfo_ForVScrollMode(nPageNum);
                    window.scrollTo(0, arrPageInfo.pageTopPos);
                    window.scrollTo(0, arrPageInfo.pageTopPos);
                    // 기존 소스 코드에 있던 코드
                    //window.android.resultGotoAncher(nPageNum);
                    //RunaEngine.log('Move Top : pageTopPos : ' + arrPageInfo.pageTopPos + '             (' + nPageNum + "/" + RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount() + ')');
                }
                else {

                    if ((RunaEngine.DeviceInfo.IsIPAD() && RunaEngine.HtmlCtrl.getOrientation() == 0) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.isPC()) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.IsOSX())) {
                        window.scrollTo(m_CurScrollPos, 0);
                        window.scrollTo(m_CurScrollPos, 0);
                    }
                    else {
                        window.scrollTo(m_CurScrollPos, 0);
                        window.scrollTo(m_CurScrollPos, 0);
                    }

                }

                //RunaEngine.log("[PageNavi.MoveTargetPage] MoveTargetPage : (" + nPageNum + "/" + RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount() + ') scroll to = ' + m_CurScrollPos);
                return nPageNum;
            }

            this.MoveAnchorName = function (strAnchorName) {
                var lname = null;
                var arrAnchors = null;
                var i = null;
                var len = null;
                var e = null;
                var page = 0;
                var nodeCfi = null;
                var attriutedName = 'name';

                try {
                    lname = strAnchorName.toLowerCase();

                    if(RunaEngine.DeviceInfo.IsIOS()) {
                        arrAnchors = document.links;
                        attriutedName = 'id';
                    } else {
                        arrAnchors = document.anchors;
                    }
                     
                     for (i = 0, len = arrAnchors.length; i < len; i++) {
                         if (arrAnchors[i].getAttribute(attriutedName).toLowerCase() === lname) {

                             nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrAnchors[i], 0, "");
                             page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrAnchors[i]);
                             //val = RunaEngine.HtmlCtrl.AVTTSEngine.FindSentence(nodeCfi, arrAnchors[i].textContent, true);
                             return this.MoveTargetPage(page);
                         }
                     }

                    if (e == null) {
                        e = document.getElementById(strAnchorName);
                        if (e != null) {
                            //nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, e, 0, "");
                            page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(e)
                            //val = RunaEngine.HtmlCtrl.AVTTSEngine.FindSentenceById(strAnchorName, e.textContent);
                            return this.MoveTargetPage(page);
                        }
                    }

                    if (RunaEngine.HtmlCtrl.getTotalPage() == 1)
                        return 1;

                    return 1;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.MoveAnchorName] ' + (ex.number & 0xFFFF));
                    RunaEngine.sublog2('[★Exception★][PageNavi.MoveAnchorName] ' + ex);
                    return -1;
                }
                finally {
                    lname = null;
                    arrAnchors = null;
                    i = null;
                    len = null;
                    e = null;
                }
            }

            this.GetAnchorPageNumber = function (strAnchorName) {
                var lname = null;
                var arrAnchors = null;
                var i = null;
                var len = null;
                var e = null;
                var attriutedName = 'name';
                try {
                    lname = strAnchorName.toLowerCase();
                    if(RunaEngine.DeviceInfo.IsIOS()) {
                        arrAnchors = document.links;
                        attriutedName = 'id';
                    } else {
                        arrAnchors = document.anchors;
                    }
                    
                    for (i = 0, len = arrAnchors.length; i < len; i++) {
                        if (arrAnchors[i].getAttribute(attriutedName).toLowerCase() === lname) {
                            return RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrAnchors[i]);
                        }
                    }

                    if (e == null) {
                        e = document.getElementById(strAnchorName);
                        if (e != null) {
                            return RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(e);
                        }
                    }

                    if (RunaEngine.HtmlCtrl.getTotalPage() == 1)
                        return 1;

                    return 1;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + ex.description);
                    return -1;
                }
                finally {
                    lname = null;
                    arrAnchors = null;
                    i = null;
                    len = null;
                    e = null;
                }
            }

            this.GetAnchorPageNumberWithOffset = function (strAnchorName) {
                var lname = null;
                var arrAnchors = null;
                var i = null;
                var len = null;
                var e = null;
                var r = null;

                try {
                    lname = strAnchorName.toLowerCase();
                    arrAnchors = document.anchors;
                    for (i = 0, len = arrAnchors.length; i < len; i++) {
                        if (arrAnchors[i].getAttribute('name').toLowerCase() === lname) {
                            r = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumberWithOffset(arrAnchors[i]);

                            if (r != null) {
                                return "" + r.page + ";" + r.left + ";" + r.top;
                            }
                        }
                    }

                    if (e == null) {
                        e = document.getElementById(strAnchorName);
                        if (e != null) {
                            r = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumberWithOffset(e);

                            if (r != null) {
                                return "" + r.page + ";" + r.left + ";" + r.top;
                            }
                        }
                    }

                    if (RunaEngine.HtmlCtrl.getTotalPage() == 1) {
                        return "1;0;0";
                    }

                    return "1;0;0";
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + ex.description);
                    return "-1;0;0";
                }
                finally {
                    lname = null;
                    arrAnchors = null;
                    i = null;
                    len = null;
                    e = null;
                    r = null;
                }
            }

            this.GetElementPageNumber = function (strId) {
                var e = null;
                try {
                    e = document.getElementById(strAnchorName);
                    if (e != null) {
                        return RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(e);
                    }

                    if (RunaEngine.HtmlCtrl.getTotalPage() == 1)
                        return 1;

                    return 1;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + ex.description);
                    return -1;
                }
                finally {
                    e = null;
                }
            }


            this.GetElementPageNumberWithOffset = function (strAnchorName) {
                var lname = null;
                var arrAnchors = null;
                var i = null;
                var len = null;
                var e = null;
                var r = null;

                try {
                    e = document.getElementById(strAnchorName);
                    if (e != null) {
                        r = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumberWithOffset(e);

                        if (r != null) {
                            return "" + r.page + ";" + r.left + ";" + r.top;
                        }
                    }

                    if (RunaEngine.HtmlCtrl.getTotalPage() == 1) {
                        return "1;0;0";
                    }

                    return "1;0;0";
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorPageNumber] ' + ex.description);
                    return "-1;0;0";
                }
                finally {
                    lname = null;
                    arrAnchors = null;
                    i = null;
                    len = null;
                    e = null;
                    r = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 해당 파일의 모든 A 태그의 정보를 가져온다.
            // [ Param  ]
            // [ Return ] (JSON) : 속성에 해당 되는 정보 및 텍스트를 리턴한다.
            this.GetAnchorsText = function () {
                var rtnVal = '';
                var arrAnchors = null;
                var i = null;
                var len = null;
                var e = null;
                var nextEle = null;
                var parentEle = null;
                var parentePubType = null;
                var strParentePubType = null;

                try {


                    arrAnchors = document.getElementsByTagName('a');

                    //RunaEngine.log('GetAnchorsText : '+ arrAnchors.length);

                    for (i = 0, len = arrAnchors.length; i < len; i++) {
                        //                        parentEle = arrAnchors[i].parentElement;
                        parentEle = RunaEngine.Util.getFirstParentNotEqualTagName(arrAnchors[i], 'span');
                        parentePubType = RunaEngine.Util.getParentTagName(arrAnchors[i], 'li');
                        nextEle = arrAnchors[i].nextSibling;
                        strAnchor = RunaEngine.Util.GetAttrValue(arrAnchors[i], 'name');
                        strText = arrAnchors[i].innerText;
                        strId = RunaEngine.Util.GetAttrValue(arrAnchors[i], 'id');
                        strHref = RunaEngine.Util.GetAttrValue(arrAnchors[i], 'href');
                        strePubType = RunaEngine.Util.GetAttrValue(arrAnchors[i], 'epub:type');
                        anchorType = 0;
                        strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrAnchors[i], 0, "");
                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrAnchors[i]);

                        //RunaEngine.log('Parent  : '+ parentePubType );


                        if (strId == null || strId == '') {

                            if (parentePubType != null && parentePubType != undefined) {
                                strParentePubType = RunaEngine.Util.GetAttrValue(parentePubType, 'epub:type');

                                //RunaEngine.log('Parent  : '+(parentePubType != null ? strePubType : '')+', parent : '+ (parentePubType != null ? parentePubType.getAttribute('id') : ''));
                                // epub:type="noteref" role="doc-noteref" (Front), epub:type="rearnote" role="doc-endnote" (Back), epub:type="endnote" role="doc-endnote" (Back) : 주석
                                // epub:type="biblioref" role="doc-biblioref"(front) epub:type="biblioentry" role="doc-biblioentry"(back) : 참고 문헌
                                // 없음(front), epub:type="index-locator" (Back) 색인
                                if (strParentePubType != null && strParentePubType != '') {
                                    strId = RunaEngine.Util.GetAttrValue(parentePubType, 'id');
                                }
                            }
                        }


                        if (strePubType != null && strePubType != '' && strePubType != undefined) {
                            if (strePubType == 'noteref') {
                                anchorType = 1; //front note
                            }
                            else if (strePubType == 'biblioref') {
                                anchorType = 2; //front biblio
                            }
                            else if (strePubType == 'index-locator') {
                                anchorType = 5; //back index
                            }
                        }

                        if (strParentePubType != null && strParentePubType != '' && strParentePubType != undefined) {
                            if (strParentePubType == 'rearnote' || strParentePubType == 'footnote') {
                                anchorType = 3; //back note
                            }
                            else if (strePubType == 'biblioentry') {
                                anchorType = 4; //back biblio
                            }
                        }

                        //RunaEngine.log('GetAnchorsText , ---- id : '+strId+', anchor : '+strAnchor+', Href :'+strHref+', text :'+strText+', epubtype : '+strePubType+', parentePubType : '+parentePubType);

                        if (strHref != null && strHref != undefined && strHref != '') {
                            //strNextText = parentEle != null ? (parentEle.textContent.length > 30 ? parentEle.textContent.substr(1, 30) : parentEle.textContent) : (nextEle != null ? (nextEle.textContent.length > 30 ? nextEle.textContent.substr(1, 30) : nextEle.textContent) : '');
                            var strNextText = parentEle != null ? parentEle.textContent : nextEle != null ? nextEle.textContent : '';
                            strText = RunaEngine.Util.encodeHtmlEntity(strText);//strText.toString().replace('/[\n\r\t\f\x0b\xa0]/gm', '').replace('/\'/g', '&#39').replace('/\"/g', '&quot;').replace('/</g', '&lt;').replace('/>/g', '&gt;').replace(/\"/g, '');
                            strNextText = strNextText.toString();//.replace('/[\n\r\t\f\x0b\xa0]/gm', '').replace('/\'/g', '&#39').replace('/\"/g', '&quot;').replace('/</g', '&lt;').replace('/>/g', '&gt;').replace(/\"/g, '');
                            strNextText = RunaEngine.Util.encodeHtmlEntity(strNextText);

                            if (rtnVal != '')
                                rtnVal += ',';


                            rtnVal += "{\"id\" : \"" + strId + "\", \"anchor\" : \"" + strAnchor + "\", \"href\" : \"" + strHref + "\", \"text\" : \"" + strText + "\", \"next\" : \"" + strNextText + "\", \"page\" : \"" + page + "\", \"type\" : \"" + anchorType + "\", \"cfi\" : \"" + strCfi + "\"}";
                        }
                        else {

                            if (rtnVal != '')
                                rtnVal += ',';


                            rtnVal += "{\"id\" : \"" + strId + "\", \"anchor\" : \"" + strAnchor + "\", \"href\" : \"" + strHref + "\", \"text\" : \"" + strText + "\", \"next\" : \"" + strNextText + "\", \"page\" : \"" + page + "\", \"type\" : \"" + anchorType + "\", \"cfi\" : \"" + strCfi + "\"}";
                        }

                        //RunaEngine.log('GetAnchorsText : '+rtnVal);
                        //RunaEngine.log('id : '+strId+', anchor : '+strAnchor+', Href :'+strHref+', text :'+strText+', next : '+strNextText);
                    }

                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetAnchorsText] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][PageNavi.GetAnchorsText] ' + ex);
                    return "";
                }
                finally {
                    nextEle = null;
                    rtnVal = null;
                    lname = null;
                    arrAnchors = null;
                    i = null;
                    len = null;
                    e = null;
                    rtnVal = "";
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 해당 파일의 모든 태그 속성 중 epub:type 의 정보를 가져온다.
            // [ Param  ]
            // [ Return ] (JSON) : 속성에 해당 되는 정보 및 텍스트를 리턴한다.
            this.GetAllePubTypeNodes = function () {
                var rtnVal = '';
                var arrTypes = null;
                var arrRoles = null;
                var arrEles = null;
                var bType = true;
                var i = null;
                var len = null;
                var e = null;
                var noteType = -1;
                var strType = null;
                var strText = null;
                var strId = null;
                var strCfi = null;
                var page = 0;
                var strHref = null;

                try {
                    arrEles = RunaEngine.Util.GetElementInsideContainer('runaengine_content', 'epub:type');
                    //arrRoles = RunaEngine.Util.GetElementInsideContainer('runaengine_content', 'role');

                    //RunaEngine.sublog2('GetAllePubTypeNodes : [Types : ' + arrTypes.length +', Roles : '+ arrRoles.length +']');

                    //arrEles = arrTypes.length >= arrRoles.length ? arrTypes : arrRoles;
                    //bType = arrTypes.length >= arrRoles.length;

                    for (i = 0, len = arrEles.length; i < len; i++) {
                        //strType = bType ? RunaEngine.Util.GetAttrValue(arrEles[i], 'epub:type') : RunaEngine.Util.GetAttrValue(arrEles[i], 'role');
                        strType = RunaEngine.Util.GetAttrValue(arrEles[i], 'epub:type')
                        strText = arrEles[i].innerText;
                        strId = RunaEngine.Util.GetAttrValue(arrEles[i], 'id');
                        noteType = -1;
                        strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrEles[i], 0, "");
                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrEles[i]);
                        strHref = RunaEngine.Util.GetAttrValue(arrEles[i], 'href');

                        //RunaEngine.sublog2('GetAllePubTypeNodes : [Type : ' + strType);
                        //RunaEngine.sublog2('GetAllePubTypeNodes : [Text : ' + strText);

                        // epub:type="noteref" role="doc-noteref" (미주, 각주),
                        // epub:type="rearnote" role="doc-endnote" (미주 설명), 하위 태그에 링크가 있을 수 있음.
                        // epub:type="endnote" role="doc-endnote" (Back) : 각주 설명
                        // epub:type="footnote" role="doc-footnote" (Back) : 각주 설명
                        // epub:type="biblioref" role="doc-biblioref"(참고 문헌 링크)
                        // epub:type="biblioentry" role="doc-biblioentry"(back) : 참고 문헌 설명
                        // 없음(front), epub:type="index-locator" (Back) 색인

                        if (strType != null && strType != '' && strType != undefined) {
                            if (strType == 'noteref' || strType == 'doc-noteref') {
                                noteType = 0; //front note
                            }
                            else if (strType == 'rearnote' || strType == 'doc-rearnote') {
                                //하위 태그에서 id를 찾자.
                                //미주
                                noteType = 7;

                                if(strId == null) {
                                    childEle = arrEles[i].firstElementChild;
                                    while(childEle) {
                                        strId = RunaEngine.Util.GetAttrValue(childEle, 'id');
                                        if(strId != null) {
                                            strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, childEle, 0, "");
                                            page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(childEle);
                                            strHref = RunaEngine.Util.GetAttrValue(childEle, 'href');
                                            strText = childEle.innerText;
                                            break;
                                        }
                                        childEle = childEle.nextElementSibling;
                                    }

                                    RunaEngine.sublog2('GetAllePubTypeNodes : child [ 7 Type : ' + strType +' , href : ' + strHref +', id : ' +strId);
                                }

                                RunaEngine.sublog2('GetAllePubTypeNodes : [ 7 Type : ' + strType +' , href : ' + strHref +', id : ' +strId);
                            }
                            else if (strType == 'footnote' || strType == 'doc-footnote') {
                                //하위 태그에서 id를 찾자.
                                // 각주
                                noteType = 8;

                                if(strId == null) {
                                    childEle = arrEles[i].firstElementChild;
                                    while(childEle) {
                                        strId = RunaEngine.Util.GetAttrValue(childEle, 'id');
                                        if(strId != null) {
                                            strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, childEle, 0, "");
                                            page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(childEle);
                                            strHref = RunaEngine.Util.GetAttrValue(childEle, 'href');
                                            strText = childEle.innerText;
                                            break;
                                        }
                                        childEle = childEle.nextElementSibling;
                                    }
                                    RunaEngine.sublog2('GetAllePubTypeNodes : child [ 8 Type : ' + strType +' , href : ' + strHref +', id : ' +strId);
                                }

                                RunaEngine.sublog2('GetAllePubTypeNodes : [ 8 Type : ' + strType +' , href : ' + strHref +', id : ' +strId);
                            }
                            else if (strType == 'endnote' || strType == 'doc-endnote') {
                                //하위 태그에서 id를 찾자.
                                //미주
                                noteType = 9;

                                if(strId == null) {
                                    childEle = arrEles[i].firstElementChild;
                                    while(childEle) {
                                        strId = RunaEngine.Util.GetAttrValue(childEle, 'id');
                                        if(strId != null) {
                                            strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, childEle, 0, "");
                                            page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(childEle);
                                            strHref = RunaEngine.Util.GetAttrValue(childEle, 'href');
                                            strText = childEle.innerText;
                                            break;
                                        }
                                        childEle = childEle.nextElementSibling;
                                    }
                                    RunaEngine.sublog2('GetAllePubTypeNodes : child [ 9 Type : ' + strType +' , href : ' + strHref +', id : ' +strId);
                                }

                                RunaEngine.sublog2('GetAllePubTypeNodes : [ 9 Type : ' + strType +' , href : ' + strHref +', id : ' +strId);
                            }
                        }

                        if(noteType >= 0) {
                            if (rtnVal != '')
                                    rtnVal += ',';

                            rtnVal += "{\"id\" : \"" + strId + "\", \"href\" : \"" + strHref + "\", \"text\" : \"" + strText + "\", \"page\" : \"" + page + "\", \"type\" : \"" + noteType + "\", \"cfi\" : \"" + strCfi + "\"}";
                        }

                    }

                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetAllePubTypeNodes] ' + (ex.number & 0xFFFF));
                    RunaEngine.sublog2('[★Exception★][PageNavi.GetAllePubTypeNodes] ' + ex);
                    return "";
                }
                finally {
                    nextEle = null;
                    rtnVal = null;
                    lname = null;
                    arrAnchors = null;
                    i = null;
                    len = null;
                    e = null;
                    rtnVal = "";
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 이미지 리스트를 가져온다.
            // [ Param  ]
            // [ Return ] (JSON) : 속성에 해당 되는 정보 및 텍스트를 리턴한다.
            this.GetImageList = function () {
                var rtnVal = '';
                var arrImages = null;
                var i = null;
                var len = null;
                var e = null;
                var captionEle = null;
                try {


                    arrImages = document.getElementsByTagName('img');

                    RunaEngine.log('GetImageList : ' + arrImages.length);

                    for (i = 0, len = arrImages.length; i < len; i++) {
                        //                        parentEle = arrAnchors[i].parentElement;
                        strId = arrImages[i].getAttribute('id');
                        strHref = arrImages[i].getAttribute('src');
                        strAriaId = arrImages[i].getAttribute('aria-describedby');
                        strDesc = arrImages[i].getAttribute('alt');
                        strCaption = '';
                        strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrImages[i], 0, "");

                        if (strAriaId != null && strAriaId != undefined && strAriaId != "") {
                            captionEle = document.getElementById(strAriaId);
                            if (captionEle != null) {
                                strCaption = captionEle.innerText;
                            }
                        }

                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrImages[i]);

                        if (strHref != null && strHref != undefined && strHref != '') {
                            if((strDesc != null && strDesc != undefined && strDesc != '') || (strAriaId != null && strAriaId != undefined && strAriaId != '')) {
                                strCaption = RunaEngine.Util.encodeHtmlEntity(strCaption);
                                strDesc = RunaEngine.Util.encodeHtmlEntity(strDesc);
                            }

                            if (rtnVal != '')
                                rtnVal += ',';

                            strVal = "{\"id\" : \"" + strId + "\", \"aria\" : \"" + strAriaId + "\", \"href\" : \"" + strHref + "\", \"text\" : \"" + strDesc + "\", \"caption\" : \"" + strCaption + "\", \"page\" : \"" + page + "\", \"cfi\" : \"" + strCfi + "\"}";
                            rtnVal += strVal;

                            RunaEngine.log("GetImageList : " + strVal);
                        }
                    }

                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetImageList] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][PageNavi.GetImageList] ' + ex);
                    return "";
                }
                finally {
                    captionEle = null;
                    rtnVal = null;
                    arrImages = null;
                    i = null;
                    len = null;
                    e = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 해당 파일의 모든 태그 속성 중 epub:type 종이책 페이지의 정보를 가져온다.
            // [ Param  ]
            // [ Return ] (JSON) : 속성에 해당 되는 정보 및 텍스트를 리턴한다.
            this.GetPaperPageList = function () {
                var rtnVal = '';
                var arrSpans = null;
                var i = null;
                var len = null;
                try {

                    //arrSpans = document.getElementsByTagName('span');
                    arrSpans = RunaEngine.Util.GetElementInsideContainerWithValue('runaengine_content', 'epub:type', 'pagebreak');

                    RunaEngine.log('GetPaperPageList : ' + arrSpans.length);

                    for (i = 0, len = arrSpans.length; i < len; i++) {
                        strId = arrSpans[i].getAttribute('id');
                        strType = arrSpans[i].getAttribute('epub:type');
                        strClass = arrSpans[i].getAttribute('class');
                        strText = arrSpans[i].getAttribute('title');
                        strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrSpans[i], 0, "");

                        if (strType != null && strType != undefined && strType == "pagebreak") {

                            page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrSpans[i]);
                            strText = RunaEngine.Util.encodeHtmlEntity(strText);

                            if (rtnVal != '')
                                rtnVal += ',';

                            strVal = "{\"id\" : \"" + strId + "\", \"text\" : \"" + strText + "\", \"page\" : \"" + page + "\", \"cfi\" : \"" + strCfi + "\"}";
                            rtnVal += strVal;

                            RunaEngine.log("GetPaperPageList : " + strVal);
                        }
                    }

                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetPaperPageList] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][PageNavi.GetPaperPageList] ' + ex);
                    return "";
                }
                finally {
                    rtnVal = null;
                    arrSpans = null;
                    i = null;
                    len = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 테이블 리스트를 가져온다.
            // [ Param  ]
            // [ Return ] (JSON) : 속성에 해당 되는 정보 및 텍스트를 리턴한다.
            this.GetTableList = function () {
                var rtnVal = '';
                var arrTables = null;
                var i = null;
                var len = null;
                try {

                    arrTables = document.getElementsByTagName('table');

                    RunaEngine.log('GetTableList : ' + arrTables.length);

                    for (i = 0, len = arrTables.length; i < len; i++) {
                        strId = arrTables[i].getAttribute('id');
                        captionEle = arrTables[i].firstElementChild;
                        strCaption = '';
                        strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrTables[i], 0, "");

                        if (captionEle != null) {
                            strCaption = captionEle.innerText;
                        }

                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrTables[i]);
                        strCaption = RunaEngine.Util.encodeHtmlEntity(strCaption);

                        if (rtnVal != '')
                            rtnVal += ',';

                        strVal = "{\"id\" : \"" + strId + "\", \"caption\" : \"" + strCaption + "\", \"page\" : \"" + page + "\", \"cfi\" : \"" + strCfi + "\"}";
                        rtnVal += strVal;

                        RunaEngine.log("GetTableList : " + strVal);
                    }

                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetTableList] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][PageNavi.GetTableList] ' + ex);
                    return "";
                }
                finally {
                    rtnVal = null;
                    arrSpans = null;
                    i = null;
                    len = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 수식 리스트를 가져온다.
            // [ Param  ]
            // [ Return ] (JSON) : 속성에 해당 되는 정보 및 텍스트를 리턴한다.
            this.GetMathList = function () {
                var rtnVal = '';
                var arrEles = null;
                var i = null;
                var len = null;
                try {

                    arrEles = document.getElementsByTagName('math');

                    RunaEngine.log('GetMathList : ' + arrEles.length);

                    for (i = 0, len = arrEles.length; i < len; i++) {
                        strCaption = arrEles[i].getAttribute('alttext');
                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrEles[i]);
                        strCaption = RunaEngine.Util.encodeHtmlEntity(strCaption);
                        strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrEles[i], 0, "");

                        if (rtnVal != '')
                            rtnVal += ',';

                        strVal = "{\"caption\" : \"" + strCaption + "\", \"page\" : \"" + page + "\", \"cfi\" : \"" + strCfi + "\"}";
                        rtnVal += strVal;

                        RunaEngine.log("GetMathList : " + strVal);
                    }

                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetMathList] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][PageNavi.GetMathList] ' + ex);
                    return "";
                }
                finally {
                    rtnVal = null;
                    arrEles = null;
                    i = null;
                    len = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 문단 리스트를 가져온다.
            // [ Param  ]
            // [ Return ] (JSON) : 속성에 해당 되는 정보 및 텍스트를 리턴한다.
            this.GetParagraphList = function () {
                var rtnVal = '';
                var arrEles = null;
                var i = null;
                var len = null;
                try {

                    arrEles = RunaEngine.Util.getElementsByTagNames('p,h1,h2,h3,h4,h5,h6,table');

                    RunaEngine.log('GetParagraphList : ' + arrEles.length);

                    for (i = 0, len = arrEles.length; i < len; i++) {
                        page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(arrEles[i]);
                        strCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, arrEles[i], 0, "");

                        if (rtnVal != '')
                            rtnVal += ',';

                        strVal = "{\"index\" : \"" + i + "\", \"page\" : \"" + page + "\", \"cfi\" : \"" + strCfi + "\"}";
                        rtnVal += strVal;

                        RunaEngine.log("GetParagraphList : " + strVal);
                    }

                    return rtnVal;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][PageNavi.GetParagraphList] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][PageNavi.GetParagraphList] ' + ex);
                    return "";
                }
                finally {
                    rtnVal = null;
                    arrEles = null;
                    i = null;
                    len = null;
                }
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // HighLighting
        ///////////////////////////////////////////////////////////////////////
        this.Highlight = new function __Highlight() {
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 모든 하이라이팅 감추기
            this.HideAll = function () {

                //RunaEngine.log('  --- HIDE ALL HL --- ');
                document.getElementById('Rue_Highlight_Layer').style.visibility = 'hidden';
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 모든 하이라이팅 보이기
            this.ShowAll = function () {
                //RunaEngine.log('  --- SHOW ALL HL --- ');
                document.getElementById('Rue_Highlight_Layer').style.visibility = 'visible';
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 모든 하이라이팅 제거
            this.RemoveAll = function () {
                //RunaEngine.log('              REMOVE  HIGHLIGHTING ALL                ');
                document.getElementById('Rue_Highlight_Layer').innerHTML = '';
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 모든 하이라이팅 제거
            this.ChangeBackColor = function (strHLID, strColor) {
                //RunaEngine.log('CHANGE HIGHLIGHT_BGCOLOR');
                var i = null;
                var len = null;
                var res = 0;
                try {
                    for (i = 0, len = document.getElementById('Rue_Highlight_Layer').childNodes.length; i < len; i++) {

                        var se = document.getElementById('Rue_Highlight_Layer').childNodes[i].style;

                        /**
                         * redmine refs #1244
                         * 밑줄 옵션 underline 추가
                         * by junghoon.kim
                         */


                        if (strColor == "UNDERLINE") {

                            se.borderBottom = "thick solid #FF0000";
                            se.borderBottomWidth = "2px";
                            se.paddingBottom = "2px";
                            se.backgroundColor = "";

                            se.opacity = "1";

                        }
                        else {
                            se.backgroundColor = strColor;
                            se.borderBottom = "";
                            se.borderBottomWidth = "";
                            se.paddingBottom = "";
                            se.opacity = "0.2";
                        }

                        res++;
                    }
                }
                finally {
                    i = null;
                    len = null;
                    // RunaEngine.HtmlCtrl.Highlight.showAll();
                }

                return res;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 지정한 하이라이팅 ID만 화면상에서 제거
            // [ Param  ] (string) strHLID: 하이라이팅 ID
            // [ Return ] 삭제된 갯수
            this.Remove = function (strHLID) {
                //RunaEngine.log('              REMOVE  HIGHLIGHTING (" + strHLID + ")       ');
                var i = null;
                var len = null;
                var res = 0;
                try {
                    for (i = 0, len = document.getElementById('Rue_Highlight_Layer').childNodes.length; i < len; i++) {

                        if (document.getElementById('Rue_Highlight_Layer').childNodes[i].id.indexOf('Rue_Highlight_Layer_' + strHLID + '_') == 0) {
                            document.getElementById('Rue_Highlight_Layer').childNodes[i].onclick = null;
                            document.getElementById('Rue_Highlight_Layer').removeChild(document.getElementById('Rue_Highlight_Layer').childNodes[i]);
                            i--;
                            len--;
                            res++;
                        }
                    }
                }
                finally {
                    i = null;
                    len = null;
                }

                return res;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 지정한 하이라이팅 ID로 하이라이팅 생성
            // [ Param  ]
            //   - (string) strHLID: 하이라이팅 ID
            //   - (string) strStartCfi: 시작 CFI값
            //   - (string) strEndCfi: 끝 CFI값
            //   - (string) strColor: 하이라이팅 색상
            //   - (string) strMemo: 하이라이팅 메모
            // [ Return ] (number) 에러코드
            //           안드로이드 개발시 요청사항으로 변경
            //           기존엔  에러코드 + ':' + 에러메시지로, 성공이면 '0:success'
            this.Create = function (strStartCfi, strEndCfi, strColor) {
                // RunaEngine.log('              CREATE HIGHLIGHTING (' + strHLID + ')('+strStartCfi+')                ');
                var isDebug = true;
                var rtnCompare = null;
                var rtnDraw = null;
                var nRtnDraw_Cnt = null;
                var dl_idx = null;
                var result = new Array();
                var isRes = false;

                try {
                    if (strColor == null || strColor == '') {
                        strColor = "red";
                    }
                    if (strStartCfi == null || strStartCfi == '') {
                        result['res'] = -1;
                        return result; //시작위치값이 없습니다.
                    }
                    if (strEndCfi == null || strEndCfi == '') {
                        result['res'] = -2;
                        return result; //종료위치값이 없습니다.
                    }

                    rtnCompare = RunaEngine.HtmlCtrl.Highlight._CheckCfiCompare(strStartCfi, strEndCfi);
                    strStartCfi = rtnCompare['Big'];
                    strEndCfi = rtnCompare['Small'];
                    rtnCompare = null;

                    rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(strStartCfi, strEndCfi, true);
                    if (rtnDraw.constructor == Array) {
                        nRtnDraw_Cnt = rtnDraw.length;
                        for (dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
                            result[dl_idx] = new Array();

                            RunaEngine.log('dl_idx = ' + dl_idx + ' top = '  + rtnDraw[dl_idx]['top'].toString() + ' SelectionText = ' + rtnDraw[0]['SelectionText'].toString());
                            result[dl_idx]['top'] = parseInt(rtnDraw[dl_idx]['top'].toString(), 10);
                            result[dl_idx]['left'] = parseInt(rtnDraw[dl_idx]['left'].toString(), 10);
                            result[dl_idx]['width'] = parseInt(rtnDraw[dl_idx]['width'].toString(), 10);
                            result[dl_idx]['height'] = parseInt(rtnDraw[dl_idx]['height'].toString(), 10);

                            result['SelectionText'] = rtnDraw[0]['SelectionText'].toString();

                        }

                        result['res'] = 0;
                        return result; //success
                    }
                    else {
                        RunaEngine.log('ERIC -- Highlight.create error' + rtnDraw);
                        result['res'] = rtnDraw;
                        return result;
                    }
                }
                catch (ex) {
                    RunaEngine.log(ex);
                }
                finally {
                    isDebug = null;
                    rtnCompare = null;
                    rtnDraw = null;
                    nRtnDraw_Cnt = null;
                    dl_idx = null;
                    re = null;
                    se = null;
                }
            }

            // ios - jdm
            this.CreateSearchResultHighlight = function (strStartCfi, strEndCfi, strColor) {
                RunaEngine.log('CREATE HIGHLIGHTING('+strStartCfi+')('+strEndCfi+')');
                var isDebug = true;
                var rtnCompare = null;
                var rtnDraw = null;
                var nRtnDraw_Cnt = null;
                var dl_idx = null;
                var result = new Array();
                var isRes = false;
                var re = null;
                var se = null;

                try {
                    if (strColor == null || strColor == '') {
                        strColor = "red";
                    }
                    if (strStartCfi == null || strStartCfi == '') {
                        result['res'] = -1;
                        return result; //시작위치값이 없습니다.
                    }
                    if (strEndCfi == null || strEndCfi == '') {
                        result['res'] = -2;
                        return result; //종료위치값이 없습니다.
                    }

                    const parent = document.getElementById("Rue_Highlight_Layer")
                    if (parent.firstChild) {
                        while (parent.firstChild) {
                            parent.firstChild.remove();
                        }
                    }

                    rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(strStartCfi, strEndCfi, true);
                    if (rtnDraw.constructor == Array) {
                        nRtnDraw_Cnt = rtnDraw.length;
                        for (dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
                            // result[dl_idx] = new Array();

                            re = document.createElement("div");
                            se = re.style;
                            re.id = 'Rue_Highlight_Layer_search_result';


                            RunaEngine.log('(div)2 rect.top : ' + rtnDraw[dl_idx]['top'] + ', rect.left : ' + rtnDraw[dl_idx]['left'] + ', rect.width : ' + rtnDraw[dl_idx]['width'] + ', rect.height : ' + rtnDraw[dl_idx]['height']);


                            se.zIndex = 9999;
                            se.position = "absolute";
                            se.top = rtnDraw[dl_idx]['top'].toString();
                            se.left = rtnDraw[dl_idx]['left'].toString();
                            se.width = rtnDraw[dl_idx]['width'].toString();
                            se.height = rtnDraw[dl_idx]['height'].toString();

                            if (strColor == "UNDERLINE") {

                                //RunaEngine.log("underline create");
                                se.borderBottom = "thick solid #FF0000";
                                se.borderBottomWidth = "2px";
                                se.paddingBottom = "2px";
                                se.backgroundColor = "";

                                se.opacity = "1";
                            }
                            else {
                                // RunaEngine.log("color crate  = " + strColor);
                                // se.backgroundColor = strColor;
                                se.borderBottom = "";
                                se.borderBottomWidth = "";
                                se.paddingBottom = "";
                                se.backgroundColor = "yellow";
                                se.opacity = "0.2";
                            }

                            document.getElementById('Rue_Highlight_Layer').appendChild(re);


                            se = null;
                            re = null;

                            RunaEngine.log('dl_idx = ' + dl_idx + ' top = '  + rtnDraw[dl_idx]['top'].toString() + ' SelectionText = ' + rtnDraw[0]['SelectionText'].toString());
                            // result[dl_idx]['top'] = parseInt(rtnDraw[dl_idx]['top'].toString(), 10);
                            // result[dl_idx]['left'] = parseInt(rtnDraw[dl_idx]['left'].toString(), 10);
                            // result[dl_idx]['width'] = parseInt(rtnDraw[dl_idx]['width'].toString(), 10);
                            // result[dl_idx]['height'] = parseInt(rtnDraw[dl_idx]['height'].toString(), 10);
                            //
                            // result[dl_idx]['SelectionText'] = rtnDraw[0]['SelectionText'].toString();

                            var obj = new Object();
                            obj['top'] = parseInt(rtnDraw[dl_idx]['top'].toString(), 10);
                            obj['left'] = parseInt(rtnDraw[dl_idx]['left'].toString(), 10);
                            obj['width'] = parseInt(rtnDraw[dl_idx]['width'].toString(), 10);
                            obj['height'] = parseInt(rtnDraw[dl_idx]['height'].toString(), 10);

                            obj['SelectionText'] = rtnDraw[0]['SelectionText'].toString();
                            result.push(obj);
                        }
                        // result['res'] = 0;
                        RunaEngine.log('result = ' + JSON.stringify(result));
                        result.forEach(function(value){
                            RunaEngine.log(value);
                            var obj_key = Object.keys(value);
                            var obj_value = value[obj_key];
                            RunaEngine.log(obj_key + " : " + result[obj_key]);
                        });
                        // return result; //success
                        return JSON.stringify(result);
                    }
                    else {
                        RunaEngine.log('ERIC -- Highlight.create error' + rtnDraw);
                        // result['res'] = rtnDraw;
                        // return result;
                        // result.push(rtnDraw);
                        // return JSON.stringify(result);
                    }
                }
                catch (ex) {
                    RunaEngine.log(ex);
                }
                finally {
                    isDebug = null;
                    rtnCompare = null;
                    rtnDraw = null;
                    nRtnDraw_Cnt = null;
                    dl_idx = null;
                    re = null;
                    se = null;
                }
            }

            this.CreateHighLight = function (strHLID, strStartCfi, strEndCfi, strColor) {
                //RunaEngine.log('              CREATE HIGHLIGHTING (' + strHLID + ')('+strStartCfi+')                ');
                var isDebug = false;
                var rtnCompare = null;
                var rtnDraw = null;
                var nRtnDraw_Cnt = null;
                var dl_idx = null;
                var re = null;
                var se = null;
                var result = new Array();
                var isRes = false;
                var nMovePageNo = 0;

                try {
                    if (strColor == null || strColor == '') {
                        strColor = "red";
                    }
                    if (strStartCfi == null || strStartCfi == '') {
                        result['res'] = -1;
                        return result; //시작위치값이 없습니다.
                    }
                    if (strEndCfi == null || strEndCfi == '') {
                        result['res'] = -2;
                        return result; //종료위치값이 없습니다.
                    }
                    if (strHLID == null || strHLID == '') {
                        result['res'] = -3;
                        return result; //ID가 없습니다.
                    }

                    rtnCompare = RunaEngine.HtmlCtrl.Highlight._CheckCfiCompare(strStartCfi, strEndCfi);
                    strStartCfi = rtnCompare['Big'];
                    strEndCfi = rtnCompare['Small'];
                    rtnCompare = null;

                    rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(strStartCfi, strEndCfi, false);
                    if (rtnDraw.constructor == Array) {
                        nRtnDraw_Cnt = rtnDraw.length;
                        for (dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
                            re = document.createElement("div");
                            se = re.style;
                            re.id = 'Rue_Highlight_Layer_' + strHLID + '_' + rtnDraw[dl_idx]['id'].toString();


                            //RunaEngine.log('(div)2 rect.top : ' + rtnDraw[dl_idx]['top'] + ', rect.left : ' + rtnDraw[dl_idx]['left'] + ', rect.width : ' + rtnDraw[dl_idx]['width'] + ', rect.height : ' + rtnDraw[dl_idx]['height']);


                            se.zIndex = 9999;
                            se.position = "absolute";
                            se.top = rtnDraw[dl_idx]['top'].toString();
                            se.left = rtnDraw[dl_idx]['left'].toString();
                            se.width = rtnDraw[dl_idx]['width'].toString();
                            se.height = rtnDraw[dl_idx]['height'].toString();

                            if (strColor == "UNDERLINE") {

                                //RunaEngine.log("underline create");
                                se.borderBottom = "thick solid #FF0000";
                                se.borderBottomWidth = "2px";
                                se.paddingBottom = "2px";
                                se.backgroundColor = "";

                                se.opacity = "1";
                            }
                            else {
                                //RunaEngine.log("color crate  = " + strColor);
                                se.backgroundColor = strColor;
                                se.borderBottom = "";
                                se.borderBottomWidth = "";
                                se.paddingBottom = "";
                                se.opacity = "0.2";
                            }

                            document.getElementById('Rue_Highlight_Layer').appendChild(re);


                            se = null;
                            re = null;
                        }

                        if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                            maxTop = 0;
                            for (di = 0; di < rtnDraw.length; di++) {
                                if (maxTop < parseInt(rtnDraw[di]['top'], 10)) {
                                    maxTop = parseInt(rtnDraw[di]['top'], 10);
                                }
                            }

                            nMovePageNo = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(maxTop) - 1;

                            if (isNaN(nMovePageNo) == true) {
                                nMovePageNo = 0;
                            }
                        }
                        else {
                            maxLeft = 0;
                            for (di = 0; di < rtnDraw.length; di++) {
                                if (maxLeft < parseInt(rtnDraw[di]['left'], 10)) {
                                    maxLeft = parseInt(rtnDraw[di]['left'], 10);
                                }
                            }
                            nMovePageNo = parseInt(maxLeft / RunaEngine.HtmlCtrl.getPageWidth(), 10);

                            if (isNaN(nMovePageNo) == true) {
                                nMovePageNo = 0;
                            }
                        }

                        nMovePageNo += 1;

                        result['page'] = nMovePageNo;
                        result['res'] = 0;
                        return result; //success
                    }
                    else {
                        RunaEngine.log('ERIC -- Highlight.create error' + rtnDraw);
                        result['res'] = rtnDraw;
                        result['page'] = 0;
                        return result;
                    }
                }
                catch (ex) {
                    RunaEngine.log(ex);
                }
                finally {
                    isDebug = null;
                    rtnCompare = null;
                    rtnDraw = null;
                    nRtnDraw_Cnt = null;
                    dl_idx = null;
                    re = null;
                    se = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 지정한 CFI 시작/끝 값으로 실제 DIV를 그릴 포지션 정보를 리턴
            //      무조건 start가 end보다 커야 한다.
            // [ Param  ]
            //   - (string) strStartCfi: 시작 CFI값
            //   - (string) strEndCfi: 끝 CFI값
            //   - (bool) isMakeText: 텍스트값도 추출 (Optional)
            //   - (bool) isByPassSameCfi : 같은 CFI여도 정보를 가져오기 (Optional)
            // [ Return ]
            //   - (number or array) number이면 오류, array면 정상
            this._GetDrawNodeByCfi = function (strStartCfi, strEndCfi, isMakeText, isByPassSameCfi) {
                var isDebug = true;
                var startPoint = null;
                var endPoint = null;
                var start = null;
                var end = null;
                var strSelectionID = null;
                var range = null;
                var rects = null;
                var strSelectionText = null;
                var drawList = null;
                var drawList_Idx = null;
                var k = null;
                var rect = null;
                var isDual = null;
                var rect_next = null;
                var dual_idx = null;
                var bookMakeTempNode = null;
                try {
                    if (strStartCfi == strEndCfi && isByPassSameCfi == null) {
                        return -7; //시작과 끝이 같습니다.
                    }


                    // draw calc
                    startPoint = RunaEngine.HtmlCtrl.DPControl._pointFromCFI(strStartCfi);
                    endPoint = RunaEngine.HtmlCtrl.DPControl._pointFromCFI(strEndCfi);

                    if (startPoint == null || endPoint == null) {
                        return -5; //계산할 수 없습니다.
                    }

                    start = startPoint.point;
                    end = endPoint.point;
                    if (start == null || end == null) {
                        startPoint = null;
                        endPoint = null;
                        return -6; //계산할 수 없습니다.
                    }


                    /**
                     * redmine refs #12638
                     * 챕터 처음의 cfi를 인식 하지 못해서 챕터 끝으로 이동됨
                     * by junghoon.kim
                     */
                    if (RunaEngine.DeviceInfo.IsAndroid() && (m_divContentLayout == start.node || start.node == document.getElementsByTagName("BODY")[0])) {
                        return -9; // body 및 runa container 영역
                    }

                    var bookMake = false;
                    try {
                        if (start.node.nodeName != '#text') {
                            bookMake = (start.node.getAttribute("bookMake") != undefined) ? true : false;
                        }
                    } catch (e) {
                        console.log(e.toString());
                    }
                    if (bookMake) {
                        bookMakeTempNode = document.createTextNode("BookMake");
                        start.node.appendChild(bookMakeTempNode);

                    }

                    strSelectionID = 'Rue_Selection_Container_SEL';
                    range = document.createRange();

                    if (bookMake && start.node == end.node && start.offset == end.offset) {
                        range.selectNodeContents(start.node);
                    }
                    else {

                        if (start.node == end.node && start.offset == end.offset) {
                            range.selectNode(start.node);
                        }
                        else {
                            if (typeof start.offset == "number") {
                                range.setStart(start.node, start.offset);
                            }
                            else {
                                range.setStartBefore(start.node);
                            }

                            if (typeof end.offset == "number") {
                                range.setEnd(end.node, end.offset);
                            }
                            else {
                                range.setEndBefore(end.node);
                            }
                        }

                    }

                    start.node.nextNode
                    if (isMakeText) {
                        strSelectionText = RunaEngine.HtmlCtrl.DPControl._GetText(strStartCfi, start.node, start.offset, strEndCfi, end.node, end.offset);
                    }
                    else {
                        strSelectionText = '';
                    }

                    rects = range.getClientRects();
                    drawList = new Array();
                    drawList_Idx = 0;

                    for (k = 0; k < rects.length; k++) {
                        rect = rects[k];
                        if (/*rect.top < 0 || */rect.width <= 0 || rect.height <= 0) {
                            // 내용이 없는 경우 높이 값이 -가 오는 경우는 무시한다 .
                            // 내용이 없기 때문에 표시할 필요가 없음.
                            // 위치 정보는 - 가 올수 있으므로 조건에서 제외한다.
                            continue;
                        }


                        if (RunaEngine.DeviceInfo.IsAndroid()) {
                            // 특정 노드에서 rect가 더 많이 넘어오는데 endNode의 right 보다 벗어난 rect는 제거 하기 위해
                            if (endPoint.tl_t == rect.top && endPoint.br_b == rect.bottom) {
                                if (rect.left > endPoint.br_r) {
                                    continue;
                                }
                            }
                        }
                        /**
                         * redmine refs #16666
                         * br 테그가 있으면 br 바로앞 영역을 건너 뛰어서 셀렉션 되지 않는 문제 수정
                         * 이중으로 셀렉션 되는 문제 방어 코드
                         * by junghoon.kim
                         */
                        // if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                            isDual = false;
                            for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
                                // across check
                                rect_next = rects[dual_idx];
                                if (rect_next) {
                                    // across check
                                    if (rect.top >= rect_next.top
                                        && rect.bottom <= rect_next.bottom
                                        && rect.left <= rect_next.left
                                        && rect.right >= rect_next.right && rect_next.width != 0) {
                                        isDual = true;
                                        break;
                                    }
                                    else if (rect.top <= rect_next.top
                                             && rect.bottom >= rect_next.bottom
                                             && rect.left >= rect_next.left
                                             && rect.right <= rect_next.right && rect_next.width != 0) {
                                        isDual = true;
                                        break;
                                    }
                                    else if (rect.top <= rect_next.top
                                             && rect.bottom >= rect_next.bottom
                                             && rect.left <= rect_next.left
                                             && rect.right >= rect_next.right && rect_next.width != 0) {
                                        isDual = true;
                                        break;
                                    }
                                    //range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
                                }
                            }
                            if (isDual) {
                                continue;
                            }
                        // }
                        // else {
                            // 하이라이트 시 해당 셀렉션 영역에 br 테그가 있으면 아래 로직에 의해 br 바로앞 영역이 패스 되어
                            // 화면에 그려지지 않는 버그로 인해 주석 처리함
                            // 아래 로직이 dual 모드시 동작되는 로직 같은데 정확히 파악된 로직은 아니라
                            // 다른 사이드 이펙트가 없는지 많은 검토가 필요함

                            //                            //kyungmin ios 이미지 사이즈 버그로 인한 수정
                            //                            isDual = false;
                            //                            for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
                            //                                // across check
                            //                                rect_next = rects[dual_idx];
                            //                                if (rect_next) {
                            //                                    // across check
                            //                                    if (rect.top >= rect_next.top
                            //                                        && rect.bottom <= rect_next.bottom
                            //                                        && rect.left <= rect_next.left
                            //                                        && rect.right >= rect_next.right) {
                            //                                        isDual = true;
                            //                                        break;
                            //                                    }
                            //                                    else if (rect.top <= rect_next.top
                            //                                             && rect.bottom >= rect_next.bottom
                            //                                             && rect.left >= rect_next.left
                            //                                             && rect.right <= rect_next.right) {
                            //                                        isDual = true;
                            //                                        break;
                            //                                    }
                            //                                    else if (rect.top <= rect_next.top
                            //                                             && rect.bottom >= rect_next.bottom
                            //                                             && rect.left <= rect_next.left
                            //                                             && rect.right >= rect_next.right) {
                            //                                        isDual = true;
                            //                                        break;
                            //                                    }
                            //                                    //range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
                            //                                }
                            //                            }
                            //                            if (isDual) {
                            //                                continue;
                            //                            }
                        // }

                        if (rect.width == 0) {
                            //LastReadCFI시 width가 0인 것 때문에 CFI 페이지 계산이 전페이지 나오는 현상제거
                            RunaEngine.log("rect width is 0 ");
                            continue;
                        }
                        
                                                //sjhan 210713
                        // 해당 노드의 text가 빈값일때 진행하지 않음.
                        if (RunaEngine.DeviceInfo.IsAndroid()){
                            // 좌측 상단의 좌표가 ( 0, 0 )
                            var xx = parseInt(rect.left) + (parseInt(rect.width)/2)
                            var yy = parseInt(rect.top)  + (parseInt(rect.height)/2)
                            // 해당좌표의 node 탐색
                            var selnode = document.elementFromPoint(xx,yy)
                            if (selnode){
                                if (selnode.innerText){
                                    if (selnode.innerText.replace(/\s+/g, '').length <= 0){
                                        continue;
                                    }
                                }
                            }
                        }

                        //RunaEngine.log('_GetDrawNodeByCfi2');
                        drawList[drawList_Idx] = new Array();
                        drawList[drawList_Idx]['id'] = parseInt(k);
                        drawList[drawList_Idx]['top'] = (parseInt(rect.top) + parseInt(window.scrollY)) + "px";
                        drawList[drawList_Idx]['rleft'] = parseInt(rect.left);

                        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                            drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                        }
                        else {
                            if (RunaEngine.DeviceInfo.GetVerIOS() < 7) {
                                drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                            }
                            else {
                                if (RunaEngine.DeviceInfo.GetVerIOS() < 8) {
                                    drawList[drawList_Idx]['left'] = (parseInt(rect.left)) + "px";
                                }
                                else {
                                    drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                                }
                            }
                        }
                        drawList[drawList_Idx]['width'] = rect.width + "px";
                        drawList[drawList_Idx]['height'] = rect.height + "px";

                        // RunaEngine.log('drawsList_idx = ' + drawList_Idx + ' width = ' + rect.width);
                        if (drawList_Idx == 0) {//index의 0번에만 값을 채움
                            drawList[drawList_Idx]['SelectionText'] = strSelectionText;
                        }

                        // RunaEngine..log('[Highlight._GetDrawNodeByCfi] rect.top = ' + rect.top + ',   rect.left = ' + rect.left + ',   window.scrollX = ' + window.scrollX + ',   window.scrollY = ' + window.scrollY + ',   drawList[drawList_Idx][top] = ' + drawList[drawList_Idx]['top'] + ',   drawList[drawList_Idx][left] = ' + drawList[drawList_Idx]['left']);
                        drawList_Idx++;
                    }
                    if (bookMake) {
                        start.node.removeChild(bookMakeTempNode);
                    }
                    if (drawList_Idx == 0) {
                        return -8; //검색영역없음
                    }

                    return drawList;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][Highlight._GetDrawNodeByCfi] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][Highlight._GetDrawNodeByCfi] ' + ex);
                    return -100; //예외
                }
                finally {
                    isDebug = null;
                    startPoint = null;
                    endPoint = null;
                    start = null;
                    end = null;
                    strSelectionID = null;
                    range = null;
                    rects = null;
                    strSelectionText = null;
                    drawList = null;
                    drawList_Idx = null;
                    k = null;
                    rect = null;
                    isDual = null;
                    rect_next = null;
                    dual_idx = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 지정한 CFI 시작/끝 값으로 실제 DIV를 그릴 포지션 정보를 리턴
            //      무조건 start가 end보다 커야 한다.
            // [ Param  ]
            //   - (string) strStartCfi: 시작 CFI값
            //   - (string) strEndCfi: 끝 CFI값
            //   - (bool) isMakeText: 텍스트값도 추출 (Optional)
            //   - (bool) isByPassSameCfi : 같은 CFI여도 정보를 가져오기 (Optional)
            // [ Return ]
            //   - (number or array) number이면 오류, array면 정상
            this._GetDrawNodeByCfiWithOutEmptyData = function (strStartCfi, strEndCfi, isMakeText, isByPassSameCfi) {
                var isDebug = true;
                var startPoint = null;
                var endPoint = null;
                var start = null;
                var end = null;
                var strSelectionID = null;
                var range = null;
                var rects = null;
                var strSelectionText = null;
                var drawList = null;
                var drawList_Idx = null;
                var k = null;
                var rect = null;
                var isDual = null;
                var rect_next = null;
                var dual_idx = null;
                var bookMakeTempNode = null;
                try {
                    if (strStartCfi == strEndCfi && isByPassSameCfi == null) {
                        return -7; //시작과 끝이 같습니다.
                    }

                    // draw calc
                    startPoint = RunaEngine.HtmlCtrl.DPControl._pointFromCFI(strStartCfi);
                    endPoint = RunaEngine.HtmlCtrl.DPControl._pointFromCFI(strEndCfi);

                    if (startPoint == null || endPoint == null) {
                        return -5; //계산할 수 없습니다.
                    }

                    start = startPoint.point;
                    end = endPoint.point;
                    if (start == null || end == null) {
                        startPoint = null;
                        endPoint = null;
                        return -6; //계산할 수 없습니다.
                    }
                    var bookMake = false;
                    try {
                        if (start.node.nodeName != '#text') {
                            bookMake = (start.node.getAttribute("bookMake") != undefined) ? true : false;
                        }
                    } catch (e) {
                        console.log(e.toString());
                    }
                    if (bookMake) {
                        bookMakeTempNode = document.createTextNode("BookMake");
                        start.node.appendChild(bookMakeTempNode);

                    }

                    strSelectionID = 'Rue_Selection_Container_SEL';
                    range = document.createRange();

                    if (bookMake && start.node == end.node && start.offset == end.offset) {
                        range.selectNodeContents(start.node);
                    }
                    else {

                        if (start.node == end.node && start.offset == end.offset) {
                            range.selectNode(start.node);
                        }
                        else {
                            if (typeof start.offset == "number") {
                                range.setStart(start.node, start.offset);
                            }
                            else {
                                range.setStartBefore(start.node);
                            }

                            if (typeof end.offset == "number") {
                                range.setEnd(end.node, end.offset);
                            }
                            else {
                                range.setEndBefore(end.node);
                            }
                        }
                    }

                    if (isMakeText) {
                        strSelectionText = RunaEngine.HtmlCtrl.DPControl._GetText(strStartCfi, start.node, start.offset, strEndCfi, end.node, end.offset);
                    }
                    else {
                        strSelectionText = '';
                    }

                    rects = range.getClientRects();
                    drawList = new Array();
                    drawList_Idx = 0;
                    RunaEngine.log("rect length = " + rects.length);
                    for (k = 0; k < rects.length; k++) {
                        rect = rects[k];

                        RunaEngine.log('test rect.top : ' + rect.top + ', rect.left : ' + rect.left + ', rect.bottom : ' + rect.bottom + ', rect.right : ' + rect.right + ', rect.width : ' + rect.width + ', rect.height : ' + rect.height);

                        if (rect.top < 0) {
                            // 내용이 없는 경우 높이 값이 -가 오는 경우는 무시한다 .
                            // 내용이 없기 때문에 표시할 필요가 없음.
                            continue;
                        }
                        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                            isDual = false;
                            for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
                                // across check
                                rect_next = rects[dual_idx];
                                if (rect_next) {
                                    // across check
                                    if (rect.top >= rect_next.top
                                        && rect.bottom <= rect_next.bottom
                                        && rect.left <= rect_next.left
                                        && rect.right >= rect_next.right) {
                                        isDual = true;
                                        break;
                                    }
                                    else if (rect.top <= rect_next.top
                                             && rect.bottom >= rect_next.bottom
                                             && rect.left >= rect_next.left
                                             && rect.right <= rect_next.right) {
                                        isDual = true;
                                        break;
                                    }
                                    else if (rect.top <= rect_next.top
                                             && rect.bottom >= rect_next.bottom
                                             && rect.left <= rect_next.left
                                             && rect.right >= rect_next.right) {
                                        isDual = true;
                                        break;
                                    }
                                    //range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
                                }
                            }
                            if (isDual) {
                                RunaEngine.log("rect is dual so continue");
                                continue;
                            }
                        }
                        else {
                            // kyungmin ios 이미지 사이즈 버그로 인한 수정
                            isDual = false;
                            for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
                                // across check
                                rect_next = rects[dual_idx];
                                if (rect_next) {
                                    // across check
                                    if (rect.top >= rect_next.top
                                        && rect.bottom <= rect_next.bottom
                                        && rect.left <= rect_next.left
                                        && rect.right >= rect_next.right) {
                                        isDual = true;
                                        break;
                                    }
                                    else if (rect.top <= rect_next.top
                                             && rect.bottom >= rect_next.bottom
                                             && rect.left >= rect_next.left
                                             && rect.right <= rect_next.right) {
                                        isDual = true;
                                        break;
                                    }
                                    else if (rect.top <= rect_next.top
                                             && rect.bottom >= rect_next.bottom
                                             && rect.left <= rect_next.left
                                             && rect.right >= rect_next.right) {
                                        isDual = true;
                                        break;
                                    }
                                    //range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
                                }
                            }
                            if (isDual) {
                                continue;
                            }
                        }

                        if (rect.width == 0 || rect.height == 0) {
                            //LastReadCFI시 width가 0인 것 때문에 CFI 페이지 계산이 전페이지 나오는 현상제거
                            RunaEngine.log("invaild rect width, height so continue");
                            continue;
                        }

                        var rectElement = document.elementFromPoint((rect.left + rect.right) / 2, (rect.top + rect.bottom) / 2);
                        var innerText;

                        RunaEngine.log("scollX = " + parseInt(window.scrollX));
                        RunaEngine.log("scollY = " + parseInt(window.scrollY));

                        var strCfi_Temp;
                        if (rectElement == null) {
                            RunaEngine.log("rect element is null");
                            continue;
                            // strCfi_Temp = RunaEngine.HtmlCtrl.DPControl._cfiAt(document, rect.right, g_last_draw_y);
                        }
                        // RunaEngine.log("element innterText = "+ rectElement.innerText);
                        // RunaEngine.log("element textContent = "+ rectElement.textContent);

                        var innerText = RunaEngine.Util.Trim(rectElement.textContent);

                        if (innerText.length == 0) {
                            RunaEngine.log("invaild text so continue");
                            continue;
                        }

                        //RunaEngine.log('_GetDrawNodeByCfi2');
                        drawList[drawList_Idx] = new Array();
                        drawList[drawList_Idx]['id'] = parseInt(k);
                        drawList[drawList_Idx]['top'] = (parseInt(rect.top) + parseInt(window.scrollY)) + "px";
                        drawList[drawList_Idx]['rleft'] = parseInt(rect.left);

                        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                            drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                        }
                        else {
                            if (RunaEngine.DeviceInfo.GetVerIOS() < 7) {
                                drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                            }
                            else {
                                if (RunaEngine.DeviceInfo.GetVerIOS() < 8) {
                                    drawList[drawList_Idx]['left'] = (parseInt(rect.left)) + "px";
                                }
                                else {
                                    drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                                }
                            }
                        }
                        drawList[drawList_Idx]['width'] = rect.width + "px";
                        drawList[drawList_Idx]['height'] = rect.height + "px";


                        if (drawList_Idx == 0) {//index의 0번에만 값을 채움
                            drawList[drawList_Idx]['SelectionText'] = strSelectionText;
                        }

                        //RunaEngine..log('[Highlight._GetDrawNodeByCfi] rect.top = ' + rect.top + ',   rect.left = ' + rect.left + ',   window.scrollX = ' + window.scrollX + ',   window.scrollY = ' + window.scrollY + ',   drawList[drawList_Idx][top] = ' + drawList[drawList_Idx]['top'] + ',   drawList[drawList_Idx][left] = ' + drawList[drawList_Idx]['left']);
                        drawList_Idx++;
                    }

                    if (rects.length == 0) {
                        RunaEngine.log(' 000)--------- ' + start.node.offsetLeft + ', top : ' + start.node.offsetTop + ', w : ' + start.node.offsetWidth + ', h : ' + start.node.offsetHeight);

                    }
                    if (bookMake) {
                        start.node.removeChild(bookMakeTempNode);
                    }
                    if (drawList_Idx == 0) {
                        return -8; //검색영역없음
                    }

                    return drawList;
                }
                catch (ex) {
                    RunaEngine.log('[★Exception★][Highlight._GetDrawNodeByCfi] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][Highlight._GetDrawNodeByCfi] ' + ex);
                    return -100; //예외
                }
                finally {
                    isDebug = null;
                    startPoint = null;
                    endPoint = null;
                    start = null;
                    end = null;
                    strSelectionID = null;
                    range = null;
                    rects = null;
                    strSelectionText = null;
                    drawList = null;
                    drawList_Idx = null;
                    k = null;
                    rect = null;
                    isDual = null;
                    rect_next = null;
                    dual_idx = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 지정한 Node의 실제 DIV를 그릴 포지션 정보를 리턴
            // [ Param  ]
            //   - (object) node : 엘리먼트를 넘김
            // [ Return ]
            //   - (number or array) number이면 오류, array면 정상
            this._GetDrawInfoByNode = function (node) {
                var range = null;
                var rects = null;
                var drawList = null;
                var drawList_Idx = null;
                var rect = null;
                try {
                    /**
                     * redmine refs #20797
                     * 특정 컨텐츠에서 목차 이동 시 anchor의 rect len이 0 이어서 페이지 번호가 -1로 넘어가서 annotation이 안보이는 문제 수정
                     * by junghoon.kim
                     */
                    if (RunaEngine.DeviceInfo.IsAndroid() && (m_divContentLayout == node || node == document.getElementsByTagName("BODY")[0])) {
                        return 1; // body 및 runa container 영역
                    }

                    range = document.createRange();
                    range.selectNodeContents(node);
                     if (node.nodeType == 1) {
                        rects = node.getClientRects();
                    }
                    else {
                        rects = range.getClientRects();
                    }
                    drawList = new Array();
                    drawList_Idx = 0;
                    if (rects.length > 0) {
                        // first rect is base
                        rect = rects[0];
                        drawList[drawList_Idx] = new Array();
                        drawList[drawList_Idx]['id'] = parseInt(0);
                        drawList[drawList_Idx]['top'] = (parseInt(rect.top) + parseInt(window.scrollY)) + "px";
                        if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                            drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                        }
                        else {
                            if (RunaEngine.DeviceInfo.GetVerIOS() < 7) {
                                drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                            }
                            else {
                                if (RunaEngine.DeviceInfo.GetVerIOS() < 8) {
                                    drawList[drawList_Idx]['left'] = (parseInt(rect.left)) + "px";
                                }
                                else {
                                    drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
                                }
                            }
                        }
                        drawList[drawList_Idx]['width'] = rect.width + "px";
                        drawList[drawList_Idx]['height'] = rect.height + "px";
                        //RunaEngine.log( '[Highlight._GetDrawInfoByNode] rect.top = ' + rect.top+ ',   rect.left = ' + rect.left + ',   window.scrollX = ' + window.scrollX + ',   window.scrollY = ' + window.scrollY  + ',   drawList[drawList_Idx][top] = ' + drawList[drawList_Idx]['top'] + ',   drawList[drawList_Idx][left] = ' + drawList[drawList_Idx]['left']);
                        drawList_Idx++;
                    }
                    else {
                        if (RunaEngine.DeviceInfo.IsAndroid()) {
                            /**
                             * redmine refs #20797
                             * 특정 컨텐츠에서 목차 이동 시 anchor의 rect len이 0 이어서 페이지 번호가 -1로 넘어가서 annotation이 안보이는 문제 수정
                             * by junghoon.kim
                             */
                            return RunaEngine.HtmlCtrl.Highlight._GetDrawInfoByNode(node.parentElement);
                        }
                        else {
                            return -7; //검색영역없음
                        }
                    }

                    return drawList;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][Highlight._GetDrawInfoByNode] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][Highlight._GetDrawInfoByNode] ' + ex.description);
                    return -100; //예외
                }
                finally {
                    range = null;
                    rects = null;
                    drawList = null;
                    drawList_Idx = null;
                    rect = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 지정한 CFI 시작/끝 값이 실제로 시작이 끝보다 앞서는지 비교 (Draw기준)
            // [ Param  ]
            //   - (string) strStartCfi: 시작 CFI값
            //   - (string) strEndCfi: 끝 CFI값
            // [ Return ]
            //   - (array) [0] Big : array['Big']은 두 값중 실제로 위쪽값
            //             [1] Small : array['Small']은 실제로 아래값
            this._CheckCfiCompare = function (strCfi_A, strCfi_B) {
                var isDebug = false;
                var arrRtn = new Array();
                var arrA = null;
                var arrB = null;
                var nMinLength = null;
                var isRevert = null;
                var idx = null;
                var arrSubA = null;
                var arrSubB = null;
                try {
                    if (strCfi_A == strCfi_B) {
                        arrRtn['Big'] = strCfi_A;
                        arrRtn['Small'] = strCfi_B;
                        return arrRtn;
                    }

                    arrA = strCfi_A.split('/');
                    arrB = strCfi_B.split('/');
                    nMinLength = (arrA.length > arrB.length) ? arrB.length : arrA.length;
                    isRevert = false;
                    for (idx = 0; idx < nMinLength; idx++) {
                        if (idx + 1 == nMinLength) {
                            arrSubA = arrA[idx].split(':');
                            arrSubB = arrB[idx].split(':');

                            if (parseInt(arrSubA[0]) > parseInt(arrSubB[0])) {
                                isRevert = true;
                                break;
                            }
                            else if (arrSubA.length > 1 && arrSubB.length > 1
                                     && parseInt(arrSubA[0]) == parseInt(arrSubB[0])
                                     && parseInt(arrSubA[1]) > parseInt(arrSubB[1])) {
                                isRevert = true;
                                break;
                            }
                            else {
                                break;
                            }
                        }
                        else if (parseInt(arrA[idx]) == parseInt(arrB[idx])) {
                            continue;
                        }
                        else if (parseInt(arrA[idx]) > parseInt(arrB[idx])) {
                            isRevert = true;
                            break;
                        }
                        else if (parseInt(arrA[idx]) < parseInt(arrB[idx])) {
                            isRevert = false;
                            break;
                        }
                    }

                    if (isRevert) {
                        arrRtn['Big'] = strCfi_B.replace('/b/g', 'a');
                        arrRtn['Small'] = strCfi_A.replace('/b/g', 'b');
                    }
                    else {
                        arrRtn['Big'] = strCfi_A;
                        arrRtn['Small'] = strCfi_B;
                    }
                    return arrRtn;
                }
                finally {
                    isDebug = null;
                    arrRtn = null;
                    arrA = null;
                    arrB = null;
                    nMinLength = null;
                    isRevert = null;
                    idx = null;
                    arrSubA = null;
                    arrSubB = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] CFI 가 텍스트 모드인 경우에 텍스트 다음/이전 것을 선택하기 위한 함수로
            //            결국 selectNodeContents 를 쓰냐 안쓰냐의 문제이기 때문에. 한계안에서 처리
            //            Node Find는 성능때매 일단 제거.
            this._PlusCfiCharIndex = function (strCfi, nAddNumber) {
                var isDebug = false;
                var arrI = new Array();
                var temp_number = 0;
                var rtn = null;
                try {
                    if (nAddNumber == 0) {
                        return strCfi;
                    }

                    if (strCfi.indexOf(':') == -1) {
                        return strCfi;
                    }

                    arrI = strCfi.split(':');

                    rtn = RunaEngine.HtmlCtrl.DPControl._decodeCFI(document, strCfi);
                    if (rtn == null) {
                        return strCfi;
                    }

                    if (nAddNumber > 0) {
                        if (parseInt(arrI[1]) + 1 < rtn.node.nodeValue.length) {
                            return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
                        }
                        else {
                            // 텍스트 범위 벗어남
                            if (parseInt(arrI[1]) == 0)
                                return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
                            else
                                return arrI[0] + ':' + parseInt(parseInt(arrI[1]) - 1);
                        }
                    }
                    else {
                        if (parseInt(arrI[1]) + 1 < rtn.nodeValue.length) {
                            // 텍스트 범위 벗어남
                            return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
                        }
                        else {
                            if (parseInt(arrI[1]) == 0)
                                return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
                            else
                                return arrI[0] + ':' + parseInt(parseInt(arrI[1]) - 1);
                        }
                    }
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][Highlight._PlusCfiCharIndex] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][Highlight._PlusCfiCharIndex] ' + ex.description);
                    return strCfi;
                }
                finally {
                    isDebug = null;
                    arrI = null;
                    temp_number = null;
                    rtn = null;
                }
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // 가상의 셀렉션 Divistion 컨트롤
        ///////////////////////////////////////////////////////////////////////
        this.Selection = new function __Selection() {
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 그려진 셀력션을 지운다.
            this.DeleteSelection = function () {
                if (RunaEngine.DeviceInfo.IsAndroid()) {
                    return;
                }

                //RunaEngine.log('              DELETE  SELECTION                 ');
                var isDebug = false;
                var sel = document.getElementById('Rue_Selection_Layer');
                var i = null;

                try {
                    if (RunaEngine.DeviceInfo.isPC()) {
                        if (sel) {
                            for (i = 0; i < sel.childNodes.length; i++) {
                                if (sel.childNodes[i].id != undefined && sel.childNodes[i].id.indexOf('Rue_Selection_Container_SEL_') == 0 ||
                                    sel.childNodes[i].id == 'RueSelStartBar' || sel.childNodes[i].id == 'RueSelEndBar') {
                                    sel.childNodes[i].onclick = null;
                                    sel.removeChild(sel.childNodes[i]);
                                    i--;
                                }
                            }
                        }
                    }
                    else {
                        window.getSelection().removeAllRanges();
                    }
                }
                finally {
                    isDebug = null;
                    sel = null;
                    i = null;
                }

            }

            this.SelectionDivInfo = function () {
                var isDebug = false;
                var sel = document.getElementById('Rue_Selection_Layer');
                var i = null;
                try {
                    if (sel) {
                        for (i = 0; i < sel.childNodes.length; i++) {
                            if (sel.childNodes[i].id.indexOf('Rue_Selection_Container_SEL_') == 0) {
                                //RunaEngine.log('[Selection.SelectionDivInfo]            top=' + sel.childNodes[i].style.top + ',left=' + sel.childNodes[i].style.left + ',width=' + sel.childNodes[i].style.width + ',height=' + sel.childNodes[i].style.height);
                            }
                        }
                    }
                }
                finally {
                    isDebug = null;
                    sel = null;
                    i = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 div를 Visible Status 조회
            this.IsShowSelection = function () {
                if (document.getElementById('Rue_Selection_Layer').style.visibility == 'hidden') {
                    return false;
                }
                return true;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 div를 숨김
            //      셀렉션 div가 cfiAt에서 먼저 찾기 때문에 생성됨
            this.HideSelection = function () {
                //RunaEngine.log('  *** HIDE SEL *** ');
                document.getElementById('Rue_Selection_Layer').style.visibility = 'hidden';
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 div를 보임
            //      셀렉션 div가 cfiAt에서 먼저 찾기 때문에 숨긴후 다시 보일때 사용
            this.ShowSelection = function () {
                //RunaEngine.log('  *** SHOW SEL *** ');
                document.getElementById('Rue_Selection_Layer').style.visibility = 'visible';
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 div를 그림
            // [ Param  ]
            //   - (string) strStartCfi: 시작 CFI값
            //   - (string) strEndCfi: 끝 CFI값
            //   - (string) strColor: 컬러값
            // [ Return ]
            //   - (array) [0] strErrorInfo: 에러코드 + ':' + 에러메시지로, 성공이면 '0:success'
            //             [1] strSelectedText : 선택된 텍스트 값
            this.DrawSelection = function (strStartCfi, strEndCfi, strColor) {
                var isDebug = false;
                var rtnCompare = null;
                var rtnDraw = null;
                var nRtnDraw_Cnt = null;
                var dl_idx = null;
                var re = null;
                var se = null;
                var pop_top = null;
                var pop_new_top = null;
                var isRes = false;
                var pageH = null;
                var imageDpi = 50 * checkDpi;
                var imagePixel = imageDpi;
                var result = new Array();
                var resultIdx = 0;

                try {
                    this.DeleteSelection();

                    if (strColor == null || strColor == '') {
                        strColor = 'red';
                    }
                    if (strStartCfi == null || strStartCfi == '') {
                        rtnInfo[0] = '-1:'; //시작위치값이 없습니다.
                        return rtnInfo;
                    }
                    if (strEndCfi == null || strEndCfi == '') {
                        rtnInfo[0] = '-2:'; //종료위치값이 없습니다.
                        return rtnInfo;
                    }

                    rtnCompare = RunaEngine.HtmlCtrl.Highlight._CheckCfiCompare(strStartCfi, strEndCfi);
                    strStartCfi = rtnCompare['Big'];
                    strEndCfi = rtnCompare['Small'];
                    rtnCompare = null;

                    rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(strStartCfi, strEndCfi, true);

                    if (rtnDraw.constructor == Array) {
                        nRtnDraw_Cnt = rtnDraw.length;
                        var pageWidth = RunaEngine.HtmlCtrl.getPageWidth();

                        for (dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
                            //RunaEngine.log('Draw selection (div) rect.top : ' + rtnDraw[dl_idx]['top'] + ', rect.left : ' + rtnDraw[dl_idx]['left']  + ', rect.width : ' + rtnDraw[dl_idx]['width'] + ', rect.height : ' + rtnDraw[dl_idx]['height']);

                            if (parseInt(rtnDraw[dl_idx]['top'].toString(), 10) < 0 || parseInt(rtnDraw[dl_idx]['height'].toString(), 10) > pageWidth)
                                continue;

                            if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {

                                result[resultIdx] = new Array();

                                // RunaEngine.log('dl_idx = ' + dl_idx + ' top = '  + rtnDraw[dl_idx]['top'].toString());
                                result[resultIdx]['top'] = parseInt(rtnDraw[dl_idx]['top'].toString(), 10);
                                result[resultIdx]['left'] = parseInt(rtnDraw[dl_idx]['left'].toString(), 10);
                                result[resultIdx]['width'] = parseInt(rtnDraw[dl_idx]['width'].toString(), 10);
                                result[resultIdx]['height'] = parseInt(rtnDraw[dl_idx]['height'].toString(), 10);

                                result['SelectionText'] = rtnDraw[0]['SelectionText'].toString();

                            }

                            if (RunaEngine.DeviceInfo.isPC()) {
                                re = document.createElement("div");
                                se = re.style;
                                re.id = 'Rue_Selection_Container_SEL_' + rtnDraw[dl_idx]['id'].toString();
                                se.zIndex = 9998;
                                se.position = "absolute";
                                se.top = rtnDraw[dl_idx]['top'].toString();
                                se.left = rtnDraw[dl_idx]['left'].toString();
                                se.width = rtnDraw[dl_idx]['width'].toString();
                                se.height = rtnDraw[dl_idx]['height'].toString();
                                se.backgroundColor = strColor;

                            	se.opacity = "0.2";

                                document.getElementById('Rue_Selection_Layer').appendChild(re);

                                se = null;
                                re = null;
                            }

                            result['res'] = 0;

                            result[resultIdx]['rleft'] = parseInt(rtnDraw[dl_idx]['rleft'].toString(), 10);
                            resultIdx++;
                        }

                        nRtnDraw_Cnt = null;

                        //rtnInfo[0] = '0:success';
                        //rtnInfo[1] = rtnDraw[0]['SelectionText'].toString().replace('/[\r\n]/g', '');
                        return result;
                    }
                    else {
                        result['res'] = rtnDraw;
                        return result;
                        //rtnInfo[0] = rtnDraw;
                        //return rtnInfo;
                    }
                }
                catch (ex) {
                    RunaEngine.log("exception = " + ex);
                }
                finally {
                    isDebug = null;
                    rtnInfo = null;
                    rtnCompare = null;
                    rtnDraw = null;
                    nRtnDraw_Cnt = null;
                    dl_idx = null;
                    re = null;
                    se = null;
                    pop_top = null;
                    pop_new_top = null;
                    pageH = null;
                }
            }

        };

        ///////////////////////////////////////////////////////////////////////
        // CFI 포지션 컨트롤
        ///////////////////////////////////////////////////////////////////
        this.DPControl = new function __DPControl() {
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 특정노드를 제한노드까지 상/하로 움직이면서 텍스트 추출
            // [ Param  ]
            //   - (string) node: 검색시작위치노드
            //   - (string) offset: 검색시작위치노드의 offset 값(추출텍스트offset)
            //   - (string) LimitNode: 지정한 노드를 만나면 중지
            //   - (string) isIncludeSelf: 자신의 텍스트 포함여부
            //   - (string) isOrderBy: true면 검색시작부터 뒤로검색, false면 앞으로검색
            // [ Return ]
            //   - (string) : 추출 텍스트
            this._GetParentText = function (node, offset, LimitNode, isIncludeSelf, isOrderBy) {
                var pnode = node.parentNode;
                var currNode = null;
                var rtn = '';
                var isFind = null;

                try {
                    if (isOrderBy) {
                        isFind = false;
                        currNode = pnode.firstChild;
                        while (currNode != null) {
                            if (currNode === LimitNode) {
                                break;
                            }
                            if (node === currNode) {
                                isFind = true;
                                if (isIncludeSelf != null && isIncludeSelf == true) {
                                    if (offset != null && offset == '') {
                                        rtn = currNode.nodeValue;
                                    }
                                    else {
                                        if (currNode.nodeValue != null)
                                            rtn = currNode.nodeValue.substr(offset)
                                            }
                                }
                            }
                            else if (isFind) {
                                if (currNode.nodeType === 3) {
                                    rtn += currNode.nodeValue;
                                }
                                else if (currNode.nodeType === 8) {
                                    //#comment
                                }
                                else {
                                    rtn += currNode.innerText;
                                }
                            }
                            currNode = currNode.nextSibling;
                        }
                    }
                    else {
                        isFind = false;
                        currNode = pnode.lastChild;
                        while (currNode != null) {
                            if (currNode === LimitNode) {
                                break;
                            }
                            if (node === currNode) {
                                isFind = true;
                                //rtn.currCnt = i+1;
                                if (isIncludeSelf != null && isIncludeSelf == true) {
                                    if (offset != null && offset == '') {
                                        rtn = currNode.nodeValue;
                                    }
                                    else {
                                        if (currNode.nodeValue != null)
                                            rtn = currNode.nodeValue.substr(0, offset)
                                            }
                                }
                            }
                            else if (isFind) {
                                if (currNode.nodeType === 3) {
                                    rtn = currNode.nodeValue + rtn;
                                }
                                else if (currNode.nodeType === 8) {
                                    //#comment
                                }
                                else {
                                    rtn = currNode.innerText + rtn;
                                }
                            }
                            currNode = currNode.previousSibling;
                        }
                    }

                    return rtn;
                }
                finally {
                    pnode = null;
                    currNode = null;
                    rtn = null;
                    isFind = null;
                }
            }

            ///////////////////////////////////////////////////////////////////
            // [ Desc   ] CFI로 부터 텍스트만 추출
            this._GetText = function (startCfi, start_node, start_offset, endCfi, end_node, end_offset) {
                var strText = '';
                var tmpRtnGetParent = '';
                var isDebug = false;
                var arrStart = startCfi.split(':')[0].split('/');
                var arrEnd = endCfi.split(':')[0].split('/');
                var isGetStartText = false;
                var currNode = null;
                var sameIndex = null;
                var StartNode_End = null;
                var EndNode_Start = null;
                var seq = null;
                var tmpStartNode = null;
                var tmpEndNode = null;
                var tempCurrNode = null;
                var divContentRoot = document.getElementById('runaengine_content');
                try {
                    if (start_node === end_node) {
                        //시작과 끝 노드가 동일
                        return start_node.nodeValue.substr(start_offset, end_offset - start_offset);
                    }

                    // 어디까지 같은지 확인
                    for (sameIndex = 0; sameIndex < arrStart.length; sameIndex++) {
                        if (arrStart[sameIndex] == arrEnd[sameIndex]) {
                            continue;
                        }
                        else {
                            break;
                        }
                    }

                    for (seq = arrStart.length; seq > sameIndex; seq--) {
                        if (StartNode_End == null) {
                            if (seq - 1 > sameIndex) {
                                StartNode_End = start_node.parentNode;
                            }
                            else {
                                StartNode_End = start_node.nextSibling;
                                if (StartNode_End == null) {
                                    return 'not found';
                                }
                            }
                        }
                        else {
                            if (seq - 1 > sameIndex) {
                                StartNode_End = StartNode_End.parentNode;
                            }
                            else {
                                StartNode_End = StartNode_End.nextSibling;
                                if (StartNode_End == null) {
                                    return 'not found';
                                }
                            }
                        }
                    }

                    for (seq = arrEnd.length; seq > sameIndex; seq--) {
                        if (EndNode_Start == null) {
                            if (seq - 1 > sameIndex) {
                                EndNode_Start = end_node.parentNode;
                            }
                            else {
                                EndNode_Start = end_node.previousSibling;
                                if (EndNode_Start == null) {
                                    return 'not found';
                                }
                            }
                        }
                        else {
                            if (seq - 1 > sameIndex) {
                                EndNode_Start = EndNode_Start.parentNode;
                            }
                            else {
                                EndNode_Start = EndNode_Start.previousSibling;
                                if (EndNode_Start == null) {
                                    return 'not found';
                                }
                            }
                        }
                    }

                    // 앞부분부터 중복위치 다음까지  ㄱㄱ

                    for (seq = arrStart.length; seq > sameIndex; seq--) {
                        if (!isGetStartText) {
                            tmpRtnGetParent = this._GetParentText(start_node, start_offset, StartNode_End, true, true);
                            strText += tmpRtnGetParent.toString();
                            isGetStartText = true;
                            currNode = start_node.parentNode;
                            tmpRtnGetParent = null;
                        }
                        else {
                            tmpRtnGetParent = this._GetParentText(currNode, null, StartNode_End, false, true);
                            strText += tmpRtnGetParent;
                            currNode = currNode.parentNode;
                            tmpRtnGetParent = null;
                        }
                    }

                    // 증복 다음처리  부분의 중간땅

                    tmpStartNode = StartNode_End.previousSibling;
                    tmpEndNode = EndNode_Start.nextSibling;
                    if (tmpStartNode != tmpEndNode) {
                        for (tempCurrNode = StartNode_End; tempCurrNode != null && tempCurrNode != tmpEndNode; tempCurrNode = tempCurrNode.nextSibling) {
                            if (tempCurrNode.nodeType === 3) {
                                strText += tempCurrNode.nodeValue;
                            }
                            else if (tempCurrNode.nodeType === 8) {
                                //#comment
                            }
                            else {
                                strText += tempCurrNode.innerText;
                            }
                        }
                    }
                    tmpStartNode = null;
                    tmpEndNode = null;

                    // 마지막 처리
                    isGetStartText = false;
                    for (var seq = arrEnd.length; seq > sameIndex; seq--) {
                        if (!isGetStartText) {
                            tmpRtnGetParent = this._GetParentText(end_node, end_offset, EndNode_Start, true, false);
                            strText += tmpRtnGetParent;
                            tmpRtnGetParent = null;

                            isGetStartText = true;
                            currNode = end_node.parentNode;
                        }
                        else {
                            if (EndNode_Start == divContentRoot) {
                                break;
                            }

                            tmpRtnGetParent = this._GetParentText(currNode, null, EndNode_Start, false, false);
                            strText += tmpRtnGetParent;
                            tmpRtnGetParent = null;

                            currNode = currNode.parentNode;
                        }
                    }
                    return strText;
                }
                finally {
                    isDebug = null;
                    strText = null;
                    tmpRtnGetParent = null;
                    arrStart = null;
                    arrEnd = null;
                    isGetStartText = null;
                    currNode = null;
                    sameIndex = null;
                    StartNode_End = null;
                    EndNode_Start = null;
                    seq = null;
                    tmpStartNode = null;
                    tmpEndNode = null;
                    tempCurrNode = null;
                    divContentRoot = null;
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // @ IOS ONLY
            // [ Desc   ] 현재 셀렉션(window)의 텍스트 리턴
            // [ Param  ]  none
            // [ Return ]
            //   - (string) : 선택된 텍스트값
            this.GetSelectedText = function () {
                return window.getSelection().toString();
            }

            ///////////////////////////////////////////////////////////////////////
            // @ IOS ONLY
            // [ Desc   ] 현재 셀렉션(window)의 셀렉션된 Range를 가져옴 (CFI)
            // [ Param  ]  none
            // [ Return ]
            //   - (string) 시작 CFI + ";" + 끝 CFI값
            //   아래는 예전 인터페이스 (태운이 위 형태로 변경하였음)
            //   - (array) [0] : 시작 CFI값
            //             [1] : 끝 CFI값
            this.GetSelectedRange = function () {
                try {
                    var selection = window.getSelection();

                    if (selection.rangeCount == 0) {
                        var iframes = document.getElementsByTagName("iframe");
                        for (var k = 0; k < iframes.length; k++) {
                            var iframe = iframes.item(k);
                            selection = null;
                            selection = iframe.contentWindow.getSelection();
                            if (selection.rangeCount != 0) {
                                break;
                            }
                        }
                    }

                    if (selection.rangeCount == 0) {
                        return "";
                    }
                    else {
                        var range = selection.getRangeAt(0);

                        var startCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, range.startContainer, range.startOffset, "");
                        var endCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, range.endContainer, range.endOffset, "");

                        var startXpath = RunaEngine.HtmlCtrl.DPControl._encodeXPath(document, range.startContainer);
                        var startXpathOffset = range.startOffset;
                        var endXpath = RunaEngine.HtmlCtrl.DPControl._encodeXPath(document, range.endContainer);
                        var endXPathOffset = range.endOffset;

                        var result = "{ \"startCfi\": \"" + startCfi + "\", \"endCfi\": \"" + endCfi + "\", \"startXpath\": \"" + startXpath + "\", \"startXpathOffset\": " + startXpathOffset + ", \"endXpath\": \"" + endXpath + "\", \"endXPathOffset\": " + endXPathOffset;

                        var rct = range.getBoundingClientRect();
                        var rcts = range.getClientRects();

                        result = result + ", \"boundingRect\": { \"left\" : " + rct.left + ", \"top\": " + rct.top + ", \"width\": " + rct.width + ", \"height\": " + rct.height + " }";

                        result = result + ", \"rects\": [";
                        for (var i = 0; i < rcts.length; i++) {
                            var child = rcts[i];
                            result = result + " { \"left\": " + child.left + ", \"top\": " + child.top + ", \"width\": " + child.width + ", \"height\": " + child.height + "}";
                            if (i < rcts.length - 1) {
                                result = result + ", ";
                            }
                        }
                        result = result + "] }";

                        return result;
                    }
                }
                catch (ex) {
                    RunaEngine.sublog('[★Exception★][Selection.GetSelectedRange] ' + ex);
                }
            }

            this.GetSelectionInfo = function (start, end) {
                try {
                    var color = "#aaaaaa"
                    var res = RunaEngine.HtmlCtrl.Highlight.Create(start, end, color);
                    var rtnVal = RunaEngine.HtmlCtrl.EventHandler.getDrawSelectionJson('test', res, color, res['SelectionText']);

                    if (RunaEngine.DeviceInfo.IsAndroid()) {
                        window.android.CreateHighLightResult(rtnVal);
                    } else {
                        return rtnVal;
                    }
                } finally {
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 선택한 x,y좌표의 cfi값을 가져옴
            // [ Param  ]
            //   - (document) doc: document object
            //   - (number) x: 화면상의 x point ( 범위는 보여지는 화면의 가로를 넘지 못함)
            //   - (number) y: 화면상의 y point ( 범위는 보여지는 화면의 세로를 넘지 못함)
            //   -
            // [ Return ] (object) : CFI 및 XPath, 값을 리턴. CFI값이 '' or null 이면 CFI값 얻어오기 실패
            this._cfiAt = function (doc, x, y, isWordBoundary) {
                var rtnInfo = null;

                if (doc == null || doc == '') {
                    return rtnInfo;
                }

                var isDebug = true;
                var target = null;
                var endTarget = null;
                var cdoc = doc;
                var cwin = cdoc.defaultView;
                var tail = "";
                var offset = null;
                var startOffset = null;
                var endOffset = null;
                var name = null;
                var range = null;
                var px = null;
                var py = null;
                var selText = null;

                rtnInfo = new Array();

                try {
                    // [Comment1 Start]
                    // while문은, x/y포지션 아래의 element가 iframe/object/embed 태그면, 그 내부의 포지션을 잡아내기 위해서
                    // 찾아들어가는 코드, 결과적으로는, iframe이든 아니든 현재 선택될 엘리먼트를 찾아내는 코드
                    while (true) {
                        target = cdoc.elementFromPoint(x, y);
                        if (!target) {
                            return null;
                        }
                        if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                            if (target == m_divLastBlankPageWithVerticalLayout) {
                                return null;
                            }
                        }
                        /*
                         * <p>&nbsp</p> 같은 경우 문제가 생기는걸 막기위해 .
                         * 처리하는 부분 공백문자 단순 /br 등
                         *
                         */
                        var innerText = RunaEngine.Util.Trim(target.innerText);
                        if (innerText.length == 0) {
                            return null;
                        }
                        name = target.localName;

                        /*if (name == "p") {
                         if (target.childNodes.length == 1 && target.childNodes[0].localName == 'br') {
                         RunaEngine.sublog('_CFIAt ---- ' + name + ',    ' + target.childNodes[0].outerHTML);
                         return '';
                         }
                         }*/

                        if (name != "iframe" && name != "object" && name != "embed") {
                            break;
                        }
                        var cd = target.contentDocument;
                        if (!cd) {
                            break;
                        }

                        x = x + cwin.scrollX - target.offsetLeft;
                        y = y + cwin.scrollY - target.offsetTop;
                        cdoc = cd;
                        cwin = cdoc.defaultView;

                        cd = null;
                    }
                    // [Comment1 End]

                    // [Comment2 Start]
                    // 엘리먼트는 최종적으로 찾았음. 그 엘리먼트에 따른 처리
                    if (name == "video" || name == "audio") {
                        //tail = "~" + RunaEngine.HtmlCtrl.DPControl._fstr(target.currentTime);
                    }

                    if (name == "img" || name == "video") {
                        //px = ((x + cwin.scrollX - target.offsetLeft)*100)/target.offsetWidth;
                        //py = ((y + cwin.scrollY - target.offsetTop)*100)/target.offsetHeight;
                        //tail = tail + "@" + RunaEngine.HtmlCtrl.DPControl._fstr(px) + "," + RunaEngine.HtmlCtrl.DPControl._fstr(py);
                        //px = null;
                        //py = null;
                    }
                    else if (name != "audio") {

                        if (cdoc.caretRangeFromPoint) {
                            range = cdoc.caretRangeFromPoint(x, y);
                            if (range) {
                                target = range.startContainer;
                                offset = range.startOffset;

                                selText = target.textContent;
                                RunaEngine.log("selText = " + selText + "offset = " + offset);

                                /**
                                 * redmine refs #14501
                                 * 일본어 하이라이트 안되는 문제로 로직 수정
                                 * by junghoon.kim
                                 */
                                if (selText != null) {
                                    if (isWordBoundary) {
                                        if (selText.lastIndexOf(' ', offset) >= 0) {
                                            startOffset = selText.lastIndexOf(' ', offset);
                                            RunaEngine.log("isWordBoundary startOffset = " + startOffset);
                                            startOffset += 1;
                                        }
                                        else {
                                            startOffset = offset;
                                        }
                                    }
                                    else {
                                        startOffset = offset;
                                    }


                                    RunaEngine.log('startOffset : ' + startOffset);

                                    if (isWordBoundary) {
                                        if (selText.indexOf(' ', offset + 1) > offset) {
                                            endOffset = selText.indexOf(' ', offset + 1)
                                            RunaEngine.log('endOffset !! = ' + endOffset);
                                        }
                                        else {
                                            var endPointIndex = selText.search(/[.?!]/g);

                                            if (endPointIndex != -1) {
                                                endOffset = selText.length;
                                            }
                                            else if (endOffset + 1 < selText.length) {
                                                endOffset = offset + 1;
                                            }
                                            else {
                                                endOffset = offset;
                                            }
                                        }
                                    }
                                    else {
                                        endOffset = offset;
                                        RunaEngine.log('endOffset ## = ' + endOffset);
                                    }

                                    //RunaEngine.log('endOffset : '+endOffset);
                                }
                                else {
                                    startOffset = offset;
                                    endOffset = offset;
                                }



                            }
                            else {
                            }
                            range = null;
                        }
                        else {
                        }
                    }
                    else {
                    }

                    rtnInfo[2] = startOffset;
                    rtnInfo[1] = RunaEngine.HtmlCtrl.DPControl._encodeXPath(doc, target);

                    rtnInfo[5] = endOffset;
                    rtnInfo[4] = RunaEngine.HtmlCtrl.DPControl._encodeXPath(doc, target);

                    rtnInfo[0] = RunaEngine.HtmlCtrl.DPControl._encodeCFI(doc, target, startOffset, tail);
                    rtnInfo[3] = RunaEngine.HtmlCtrl.DPControl._encodeCFI(doc, target, endOffset, tail);

                    rtnInfo[6] = RunaEngine.HtmlCtrl.DPControl._encodeCFI(doc, target, offset, tail);
                    rtnInfo[7] = RunaEngine.HtmlCtrl.DPControl._encodeXPath(doc, target);
                    rtnInfo[8] = offset;

                    //RunaEngine.log(rtnInfo[0]);
                    return rtnInfo;
                }
                finally {
                    isDebug = null;
                    target = null;
                    cdoc = null;
                    cwin = null;
                    tail = null;
                    offset = null;
                    name = null;
                    range = null;
                    px = null;
                    py = null;
                    selText = null;
                    endOffset = null;
                    startOffset = null;
                    rtnInfo = null;
                }
            }

            this._encodeXPath = function (doc, target) {
                var strXPath = '';

                if (target.parentNode == doc) {
                    strXPath = '/HTML' + strXPath;
                }
                else {
                    var siblingIdx = 0;
                    var prevSiblingNode = target.previousSibling;

                    while (prevSiblingNode != null) {
                        if (prevSiblingNode.tagName == target.tagName)
                            siblingIdx++;

                        prevSiblingNode = prevSiblingNode.previousSibling;
                    }

                    if (siblingIdx > 0 && target.nodeType == 1) {
                        strXPath = '/' + target.tagName + '[' + (siblingIdx + 1) + ']' + strXPath;
                    }
                    else if (siblingIdx == 0 && target.nodeType == 1) {
                        strXPath = '/' + target.tagName + strXPath;
                    }
                    else if (siblingIdx == 0 && target.nodeType == 3) {
                        strXPath = '/text()' + strXPath;
                    }
                    else if (siblingIdx > 0 && target.nodeType == 3) {
                        strXPath = '/text()' + '[' + (siblingIdx + 1) + ']' + strXPath;
                    }

                    strXPath = RunaEngine.HtmlCtrl.DPControl._encodeXPath(doc, target.parentNode) + strXPath;
                }

                return strXPath;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] node와 offset으로 CFI값을 만들어 냄
            // [ Param  ]
            //   - (document) doc: document object
            //   - (node) node: node object, 특정위치의 node
            //   - (number) offset: 해당 node의 offset값 (텍스트 offset)
            //   - (string) tail: 꼬리태그
            // [ Return ] (string) : CFI값을 리턴. CFI값이 '' or null 이면 CFI값 얻어오기 실패
            this._encodeCFI = function (doc, node, offset, tail) {

                var isDebug = false;
                var cfi = tail || "";
                var p = null;
                var parent = null;
                var win = null;
                var index = null;
                var child = null;
                try {

                    switch (node.nodeType) {
                        case 1:
                            if (typeof offset == "number") {
                                if (node.childNodes.item(offset) != null)
                                    node = node.childNodes.item(offset);
                                else {
                                    if (node.childNodes.length > 0)
                                        node = node.childNodes.item(node.childNodes.length - 1);
                                }
                            }
                            break;
                        default:
                            offset = offset || 0;
                            while (true) {
                                p = node.previousSibling;
                                if (!p || p.nodeType == 1) {
                                    break;
                                }
                                // CFI r3002
                                switch (p.nodeType) {
                                    case 3:
                                    case 4:
                                    case 5:
                                        offset += p.nodeValue.length;
                                }
                                node = p;
                            }
                            cfi = ":" + offset + cfi;
                            break;
                    }

                    while (node !== doc) {
                        if (node == null)
                            break;

                        parent = node.parentNode;
                        if (!parent) {
                            if (node.nodeType == 9) {
                                win = node.defaultView;
                                if (win.frameElement) {
                                    node = win.frameElement;
                                    cfi = "!" + cfi;
                                    continue;
                                }
                            }
                            break;
                        }


                        index = 0;
                        child = parent.firstChild;
                        while (true) {
                            index |= 1;
                            if (child.nodeType == 1) {
                                index++
                            }
                            if (child === node) {
                                break;
                            }
                            child = child.nextSibling;
                        }
                        // CFI r3002
                        //if( node.id && node.id.match(/^[-a-zA-Z_0-9.\u007F-\uFFFF]+$/) )
                        //{
                        //  index = index + "[" + node.id + "]";
                        //}
                        cfi = "/" + index + cfi;
                        node = parent;
                    }

                    return cfi;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][Selection._encodeCFI] ' + (ex.number & 0xFFFF));
                    //RunaEngine.sublog('[★Exception★][Selection._encodeCFI] ' + ex);
                    return '';
                }
                finally {
                    isDebug = null;
                    cfi = null;
                    p = null;
                    parent = null;
                    win = null;
                    index = null;
                    child = null;

                    //RunaEngine.log('@@@@@@@@@@@@@ _encodeCFI    START @@@@@@@@@@@@@@@');
                }
            }


            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] cfi 값으로 node및 offset,위치등을 가지는 오브젝트를 반환
            // [ Param  ]
            //   - (document) doc: document object
            //   - (string) cfi: CFI값
            // [ Return ]
            //   - (array) : 이고 구하면 offset / node.offsetLeft / node.offsetTop / node.offsetWidth / node.offsetHeight 값들을 가짐
            this._decodeCFI = function (doc, cfi) {


                if (cfi == null || cfi == '') {
                    return null;
                }

                var isDebug = true;
                var node = doc;
                var error = null;
                var r = null;
                var targetIndex = null;
                var index = null;
                var child = null;
                var offset = null;
                var point = {};
                var len = null;
                var next = null;
                var oldNode;// 마지막이 br 일경우를 위함 임시 저장소


                try {

                    while (cfi.length > 0) {
                        //if( (r = cfi.match(/^\/(\d+)/)) !== null )
                        if ((r = cfi.match(/^\/(\d+)/)) !== null)
                            // CFI r3002
                            // if( (r = cfi.match(/^\/(\d+)(\[([-a-zA-Z_0-9.\u007F-\uFFFF]+)\])?/)) !== null )
                        {
                            targetIndex = r[1] - 0;
                            index = 0;
                            child = node.firstChild;
                            try {
                                while (true) {
                                    if (!child) {
                                        error = "child not found: " + cfi;
                                        RunaEngine.log('[Selection._decodeCFI] error : ' + error);
                                        return null;
                                        //break;
                                    }

                                    index |= 1;
                                    if (child.nodeType === 1) {
                                        index++;
                                    }

                                    if (index === targetIndex) {
                                        cfi = cfi.substr(r[0].length);
                                        node = child;

                                        // br로 끝난 다면 북마크를 위한 임시 텍스트 지정
                                        if (node.nodeName.toLowerCase() == 'br') {
                                            node = oldNode;
                                            node.setAttribute("bookMake", "bookMake");
                                        }
                                        break;
                                    }
                                    child = child.nextSibling;
                                    if (node.nodeName.toLowerCase() != 'br') {
                                        oldNode = child;
                                    }
                                }
                            }
                            finally {
                                child = null;
                                targetIndex = null;
                                index = null;
                            }
                        }
                        else if ((r = cfi.match(/^!/)) !== null) {
                            if (node.contentDocument) {
                                node = node.contentDocument;
                                cfi = cfi.substr(1);
                            }
                            else {
                                error = "Cannot reference " + node.nodeName + "'s content: " + cfi;
                            }
                        }
                        else {
                            break;
                        }
                    }

                    if ((r = cfi.match(/^:(\d+)/)) !== null) {
                        offset = r[1] - 0;
                        cfi = cfi.substr(r[0].length);
                    }
                    if ((r = cfi.match(/^~(-?\d+(\.\d+)?)/)) !== null) {
                        point.time = r[1] - 0;
                        cfi = cfi.substr(r[0].length);
                    }
                    if ((r = cfi.match(/^@(-?\d+(\.\d+)?),(-?\d+(\.\d+)?)/)) !== null) {
                        point.x = r[1] - 0;
                        point.y = r[3] - 0;
                        cfi = cfi.substr(r[0].length);
                    }
                    if ((r = cfi.match(/^[ab]/)) !== null) {
                        point.forward = r[0] == "a";
                        cfi = cfi.substr(1);
                    }
                    // CFI r3002
                    //if( (r = cfi.match(/^\[.*(;s=[ab])?.*\]/)) !== null ) // pretty lame
                    //{
                    //  if( r[1] )
                    //  {
                    //      point.forward = r[0] == "s=a";
                    //      cfi = cfi.substr(1);
                    //  }
                    //}

                    if (offset !== null) {
                        while (true) {
                            if (node.nodeValue == null) {
                                if (node.childNodes.item(offset) != null)
                                    node = node.childNodes.item(offset);
                                else {
                                    if (node.childNodes.length > 0)
                                        node = node.childNodes.item(node.childNodes.length - 1);
                                }
                                continue;
                            }
                            else {
                                len = node.nodeValue.length;
                                if (offset < len || (!point.forward && offset === len)) {
                                    len = null;
                                    break;
                                }

                                next = node.nextSibling;
                            }

                            if (next == null) {
                                offset = len;
                                break;
                            }

                            // CFI v3002 [
                            while (true) {
                                var type = next.nodeType;
                                if (type === 3 || type === 4 || type === 5) {
                                    break;
                                }
                                if (type == 1) {
                                    next = null;
                                    break;
                                }
                                next = next.nextSibling;
                            }
                            //]
                            if (!next) {
                                if (offset > len) {
                                    error = "Offset out of range: " + offset;
                                    offset = len;
                                }
                                break;
                            }
                            node = next;
                            offset -= len;
                        }
                        point.offset = offset;
                    }

                    if (error) {
                        RunaEngine.log('[Selection._decodeCFI] error : ' + error);
                        return null;
                    }
                    else if (cfi.length > 0) {
                        RunaEngine.log('[Selection._decodeCFI] cfi.length > 0, cfi : ' + cfi);
                        return null;
                    }


                    point.node = node;
                    return point;
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][Selection._decodeCFI] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][Selection._decodeCFI] ' + ex);
                    return null;
                }
                finally {
                    isDebug = null;
                    node = null;
                    error = null;
                    r = null;
                    targetIndex = null;
                    index = null;
                    child = null;
                    offset = null;
                    point = null;
                    len = null;
                    next = null;

                    //RunaEngine.log('@@@@@@@@@@@@@ _decodeCFI    END @@@@@@@@@@@@@@@');
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] cfi 값으로 실제 포인트를 계산해냄
            // [ Param  ]
            //   - (document) doc: document object
            //   - (string) cfi: CFI값
            // [ Return ]
            //   - (array) : 값을 못구하면 null이고 구하면 x,y,node,time 값들을 가짐
            this._pointFromCFI = function (cfi) {
                if (cfi == null || cfi == '') {
                    return null;
                }

                var isDebug = true;
                var r = null;
                var node = null;
                var ndoc = null;
                var nwin = null;
                var tl_x = null;
                var tl_y = null;
                var br_x = null;
                var br_y = null;
                var range = null;
                var tryList = null;
                var k = null;
                var a = null;
                var nodeLen = null;
                var t = null;
                var startOffset = null;
                var endOffset = null;
                var rect = null;

                var strRtnLog_Debug = null;
                try {
                    r = RunaEngine.HtmlCtrl.DPControl._decodeCFI(document, cfi);
                    if (!r || r == '') {
                        strRtnLog_Debug = '_decodeCFI fail';
                        return null;
                    }

                    node = r.node;
                    ndoc = node.ownerDocument;
                    if (!ndoc) {
                        strRtnLog_Debug = 'ownerDocument fail';
                        node = null;
                        ndoc = null;
                        return null;
                    }

                    nwin = ndoc.defaultView;
                    if (typeof r.offset == "number") {
                        range = ndoc.createRange();
                        tryList = null;
                        if (r.forward) {
                            tryList = [{ start: 0, end: 0, a: 0.5 }, { start: 0, end: 1, a: 1 }, { start: -1, end: 0, a: 0 }];
                        }
                        else {
                            //tryList = [{ start: 0, end: 0, a: 0.5 }, { start: -1, end: 0, a: 0 }, { start: 0, end: 1, a: 1 }, { start: 1, end: 2, a: 1 }, { start: 2, end: 3, a: 1 }];
                            tryList = [{ start: 0, end: 0, a: 0.5 }, { start: -1, end: 0, a: 0 }, { start: 0, end: 1, a: 1 }];

                        }

                        k = 0;
                        nodeLen = node.nodeValue.length;
                        plusOffset = 0;

                        //                        while(plusOffset < nodeLen ) {
                        //                            if(RunaEngine.Util.CheckWhitespace(node.nodeValue.substring(plusOffset, plusOffset+1)))
                        //                                plusOffset++;
                        //                            else
                        //                                break;
                        //                        }

                        do {
                            if (k >= tryList.length) {
                                RunaEngine.log('_pointFromCFI err : [' + r.offet + ', ' + node.nodeName + ', ' + r.forward + ', ' + tryList + ', ' + tryList.length + ', ' + nodeLen + ']');
                                strRtnLog_Debug = 'tryList.length fail';
                                return null;
                            }

                            t = tryList[k++];
                            startOffset = r.offset + plusOffset + t.start;
                            endOffset = r.offset + plusOffset + t.end;
                            a = t.a;
                            //if( startOffset < 0 || endOffset >= nodeLen ) {
                            if (startOffset < 0 || endOffset > nodeLen) {
                                continue;
                            }
                            range.setStart(node, startOffset);
                            range.setEnd(node, endOffset);


                            rects = range.getClientRects();
                        }
                        while (!rects || !rects.length);

                        rect = rects[0];

                        tl_t = rect.top;
                        tl_l = rect.left;
                        br_b = rect.bottom;
                        br_r = rect.right;
                        r.offset = r.offset + plusOffset;
                        rect = null;
                        k = null;
                        a = null;
                        nodeLen = null;
                    }
                    else {
                        tl_t = node.offsetTop;
                        tl_l = node.offsetLeft - nwin.scrollX;
                        br_b = node.offsetBottom;
                        br_r = node.offsetRight - nwin.scrollX;

                        if (typeof r.x == "number" && node.offsetWidth) {
                            tl_t += (r.x * node.offsetWidth) / 100;
                            tl_l += (r.y * node.offsetHeight) / 100;
                        }
                    }

                    if (ndoc != document) {
                        strRtnLog_Debug = 'ndoc != document fail';
                        return null;
                    }

                    return { point: r, tl_t: tl_t, tl_l: tl_l, br_b: br_b, br_r: br_r };
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][Selection._pointFromCFI] ' + (ex.number & 0xFFFF));
                    RunaEngine.log('[★Exception★][Selection._pointFromCFI] ' + ex);
                    throw ex;
                }
                finally {
                    isDebug = null;
                    r = null;
                    node = null;
                    ndoc = null;
                    nwin = null;
                    tl_x = null;
                    tl_y = null;
                    br_x = null;
                    br_y = null;
                    range = null;
                    tryList = null;
                    k = null;
                    a = null;
                    nodeLen = null;
                    t = null;
                    startOffset = null;
                    endOffset = null;
                    rect = null;

                    if (strRtnLog_Debug != null) {
                        RunaEngine.log('[HtmlCtrl.Selection._pointFromCFI] strRtnLog_Debug=' + strRtnLog_Debug);
                    }
                    strRtnLog_Debug = null;
                }
            }

            this._fstr = function (d) {
                var isDebug = false;
                var n = null;
                var s = "";
                try {
                    if (d < 0) {
                        s = "-"
                        d = -d;
                    }
                    n = Math.floor(d);
                    s += n;
                    n = Math.round((d - n) * 100);
                    if (n !== 0) {
                        s += ".";
                        if (n % 10 == 0) {
                            s += (n / 10);
                        }
                        else {
                            s += n;
                        }
                    }
                    return s;
                }
                finally {
                    isDebug = null;
                    n = null;
                    s = null;
                }
            }

        };

        ///////////////////////////////////////////////////////////////////////
        // Search
        //
        // 검색은, 하나의 셀렉션만 가짐
        ///////////////////////////////////////////////////////////////////////
        this.SearchControl = new function __SearchControl() {
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] Timer 체크 Util
            //
            var start_timer1 = null;
            var start_timer2 = null;
            this.Timer = function (tag, command, id) {
                if (command == 'S') {
                    if (id == 2) {
                        start_timer2 = new Date;
                    }
                    else {
                        start_timer1 = new Date;
                    }
                }
                else {
                    if (id == 2) {
                        //RunaEngine.log('[' + tag + ']\t' + (new Date - start_timer2) + ' [ms],  ' + parseInt((new Date - start_timer2) / 1000) + ' [sec]');
                        start_timer2 = null;
                    }
                    else {
                        //RunaEngine.log('[' + tag + ']\t' + (new Date - start_timer1) + ' [ms],  ' + parseInt((new Date - start_timer1) / 1000) + ' [sec]');
                        start_timer1 = null;
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 속성에 cfi 값을 추가함 (cfi를 매번 구하는 시간을 절약하기 위함)
            //
            var g_isMakeCfiAttribute = false;
            this.SetCfiAttribute = function () {
                if (this.g_isMakeCfiAttribute == true) {
                    return;
                }

                try {
                    // nextSibling만 처리
                    var node = document;
                    var cfi = '';
                    var idxCfi = 0;

                    var temp = node.firstChild;
                    while (temp != null) {
                        idxCfi |= 1;
                        if (temp.nodeType == 1) {
                            idxCfi++;
                        }
                        //RunaEngine.log( '1[' + temp.nodeName + '] cfi: ' + cfi+'/'+idxCfi);
                        //try{ if( temp.nodeType != 3 && temp.nodeType != 8 ) temp.setAttribute('cfi',cfi+'/'+idxCfi);}catch(ea){ //RunaEngine.log( ea + ' :' + temp.nodeType + ',' + temp.nodeName );}
                        try {
                            temp['cfi'] = cfi + '/' + idxCfi;
                        }
                        catch (ea) {
                            //RunaEngine.log(ea + ' :' + temp.nodeType + ',' + temp.nodeName);
                        }
                        this._SetCfiAttributeImpl(temp, cfi + '/' + idxCfi);
                        temp = temp.nextSibling;
                    }
                    this.g_isMakeCfiAttribute = true;
                }
                catch (ex) {
                    //RunaEngine.log(ex);
                }
            }
            this._SetCfiAttributeImpl = function (node, cfiHeader) {
                try {
                    // nextSibling만 처리
                    var cfi = cfiHeader;
                    var idxCfi = 0;

                    var temp = node.firstChild;
                    while (temp != null) {
                        idxCfi |= 1;
                        if (temp.nodeType == 1) {
                            idxCfi++;
                        }
                        //RunaEngine.log( '2[' + temp.nodeName + '] cfi: ' + cfi+'/'+idxCfi);

                        //try{ if( temp.nodeType != 3 && temp.nodeType != 8 ) temp.setAttribute('cfi',cfi+'/'+idxCfi);}catch(ea){ //RunaEngine.log( ea + ' :' + temp.nodeType + ',' + temp.nodeName );}
                        try {
                            temp['cfi'] = cfi + '/' + idxCfi;
                        }
                        catch (ea) {
                            //RunaEngine.log(ea + ' :' + temp.nodeType + ',' + temp.nodeName);
                        }
                        this._SetCfiAttributeImpl(temp, cfi + '/' + idxCfi);
                        temp = temp.nextSibling;
                    }
                }
                catch (ex) {
                    //RunaEngine.log(ex);
                }
            }


            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 검색 결과 리턴
            // [ Param  ]
            //   - (string) strOutputType: 결과를 받을 형식정보로, 대문자로 OBJECT/JSON/XML 세가지를 선택할 수 있다.
            //                             각각 Json Object/Json string/xml string 으로 리턴된다. (기본값 XML)
            //   - (string) strSearchText: 검색어
            //   - (string) nResultPageNo: 검색결과를 페이징한다. 몇번째 페이지를 리턴할지 정함 (인덱스아님 1부터 시작)
            //   - (string) nResultPageSize: 검색결과를 페이징한다. 한페이지에 몇개씩 넣을지 정함
            //   - (string) nResultTextMaxCount: 검색결과스트링을 얼마나 추출할지 지정
            //   - (string) strFindTextHeaderTag: 검색결과스트링은 검색결과 이외에도 더 길게 결과를 가지는데 검색어가 중복될수 있다.
            //                                    이때에 현재 검색결과는 어느부분때문에 검색되었는지 태그를 붙일때 앞부분태그
            //   - (string) strFindTextFooterTag: 뒷부분태그
            //                                    ex) Header를 <b>, Footer를 </b>라고 지정하면, "가123나12345"에서 "123"검색시
            //                                        "가<b>123</b>나12345" , "가123나<b>123</b>45" 로 2개를 구분할 수 있다..
            // [ Return ] (string or object) 검색결과로 Json Object/Json string/xml string 으로 넘어가며
            //                             데이터 구조는 아래와 같다.
            //
            //       root > error_code
            //            > error_msg
            //            > total_cnt
            //            > curr_cnt
            //            > items
            //                     > items
            //                             > cfi_start
            //                             > cfi_end
            //                             > page_num
            //                             > result_text
            // [ Important ]
            //
            //   검색결과가 너무 많으면 당연하지만 느려진다.
            //   현재버전은, CFI를 주는데 CFI 가져오는 부분을 매번하지 않도록 해서 1초미만으로 줄인 개선코드 (검색결과 2만건기준 몇분에서 1초이내로, PC기준)
            //   현재버전은, PageNum 도 주는데, 이 부분도 개선코드로 엄청나게 줄였지만 현재 가장 리소스를 가장 많이 먹는 부분이다. (검색결과 2만건기준 2초, PC기준)
            //
            this.Search = function (strOutputType, strSearchText, nResultPageNo, nResultPageSize, nResultTextMaxCount, strFindTextHeaderTag, strFindTextFooterTag) {
                var strOutputType_Impl = 'XML'; //JSON or XML or OBJECT
                if (strOutputType != "undefined" && strOutputType != null) {
                    strOutputType_Impl = strOutputType.toUpperCase();
                }
                var nResultPageNo_Impl = 20;
                if (nResultPageNo != "undefined" && nResultPageNo != null) {
                    nResultPageNo_Impl = parseInt(nResultPageNo);
                }
                var nResultPageSize_Impl = 1;
                if (nResultPageSize != "undefined" && nResultPageSize != null) {
                    nResultPageSize_Impl = parseInt(nResultPageSize);
                }
                var nResultTextMaxCount_Impl = 20;
                if (nResultTextMaxCount != "undefined" && nResultTextMaxCount != null) {
                    nResultTextMaxCount_Impl = parseInt(nResultTextMaxCount);
                }
                var strFindTextHeaderTag_Impl = '';
                if (strFindTextHeaderTag != "undefined" && strFindTextHeaderTag != null) {
                    strFindTextHeaderTag_Impl = strFindTextHeaderTag;
                }
                var strFindTextFooterTag_Impl = '';
                if (strFindTextFooterTag != "undefined" && strFindTextFooterTag != null) {
                    strFindTextFooterTag_Impl = strFindTextFooterTag;
                }

                var jsonResult = '';
                var nRetry = 5;
                var nCurRetry = 0;

                //RunaEngine.log("search : keyword = " + strSearchText);

                jsonResult = this._Search_Cfi_Fast_Impl(strSearchText, nResultPageNo_Impl, nResultPageSize_Impl, nResultTextMaxCount_Impl, strFindTextHeaderTag_Impl, strFindTextFooterTag_Impl);


                if (jsonResult['error_code'] != 0) {

                    while (nRetry > nCurRetry) {
                        jsonResult = this._Search_Cfi_Fast_Impl(strSearchText, nResultPageNo_Impl, nResultPageSize_Impl, nResultTextMaxCount_Impl, strFindTextHeaderTag_Impl, strFindTextFooterTag_Impl);
                        nCurRetry++;

                        if (jsonResult['error_code'] == 0) {
                            break;
                        }
                    }
                }

                try {
                    if (strOutputType_Impl == 'OBJECT') {
                        return jsonResult;
                    }
                    else if (strOutputType_Impl == 'JSON') {
                        //Timer('Make JSON String','S');
                        JSON.stringify(jsonResult);
                        //Timer('Make JSON String','E');

                        return JSON.stringify(jsonResult);
                    }
                    else if (strOutputType_Impl == 'XML') {
                        //Timer('Make XML String','S');
                        var xmlResult = '<?xml version="1.0" encoding="utf-8" ?><search>';
                        xmlResult += '<ercd>' + jsonResult['error_code'] + '</ercd>';
                        xmlResult += '<ermsg>' + jsonResult['error_msg'] + '</ermsg>';
                        xmlResult += '<total>' + jsonResult['total_cnt'] + '</total>';
                        xmlResult += '<curr>' + jsonResult['curr_cnt'] + '</curr>';
                        xmlResult += '<items>';
                        for (var i = 0; i < jsonResult['items'].length; i++) {
                            xmlResult += '<item><start>' + jsonResult['items'][i]['cfi_start'] + '</start><end>' + jsonResult['items'][i]['cfi_end'] + '</end><page>' + jsonResult['items'][i]['page_num'] + '</page><text><![CDATA[' + jsonResult['items'][i]['result_text'] + ']]></text>';
                            xmlResult += '</item>\n';
                        }
                        xmlResult += '</items></search>';
                        //Timer('Make XML String','E');
                        return xmlResult;
                    }
                }
                catch (ex) {
                    //RunaEngine.log("------------ERROR----------\r\n");
                    //RunaEngine.log(ex);
                }
            }

            this._Search_Cfi_Fast_Impl = function (strSearchText, nResultPageNo, nResultPageSize, nResultTextMaxCount, strFindTextHeaderTag, strFindTextFooterTag) {
                //var arrReturn = new Array();
                var jsonReturn = {};
                jsonReturn['items'] = new Array();
                //RunaEngine.HtmlCtrl.Highlight.RemoveAll();
                try {
                    var strContent = document.body.textContent;
                    var strRexSearchWord = '';
                    var strRexGapString = '[ \n\r\t\f\v\x0b\xa0]{0,}';

                    // check
                    if (strSearchText.length > nResultTextMaxCount) {
                        jsonReturn['error_code'] = -1;
                        jsonReturn['error_msg'] = 'search text too long';
                        jsonReturn['total_cnt'] = 0;
                        jsonReturn['curr_cnt'] = 0;
                        return jsonReturn; //너무 길어서 사용할 수 없음.
                    }
                    if (strSearchText.length < 2) {
                        jsonReturn['error_code'] = -2;
                        jsonReturn['error_msg'] = 'search text too short';
                        jsonReturn['total_cnt'] = 0;
                        jsonReturn['curr_cnt'] = 0;
                        return jsonReturn; //너무 길어서 사용할 수 없음.
                    }


                    // 0. make match word string
                    //Timer('0. make match word string','S',2);
                    for (var idxStr = 0; idxStr < strSearchText.length; idxStr++) {
                        if (null != strSearchText.substr(idxStr, 1).match(new RegExp("[\$\(\)\*\+\.\[\?\\\^\{\|]"))) {
                            strRexSearchWord += '\\' + strSearchText.substr(idxStr, 1);
                        }
                        else {
                            strRexSearchWord += strSearchText.substr(idxStr, 1);
                        }
                        strRexSearchWord += strRexGapString;
                    }
                    strRexSearchWord = strRexSearchWord.substring(0, strRexSearchWord.length - strRexGapString.length);
                    //Timer('0. make match word string','E',2);

                    // 1. make array find word list


                    var reg = new RegExp(strRexSearchWord, "gim");
                    var arrMatchString = strContent.match(reg);

                    var isUseCfiAttribute = (this.g_isMakeCfiAttribute == true || (arrMatchString != null && arrMatchString.length > 10)) ? true : false;
                    if (isUseCfiAttribute) {

                        this.SetCfiAttribute();

                    }

                    // 2. make array find position list

                    var nIndexOf_StartPos = 0;
                    var nIndexOf_FindPos = 0;
                    var nIdxPosition = 0;
                    var arrSearchPosition = new Array(); // match count *2 ( start, end )
                    var nResultStartIndex = (nResultPageNo - 1) * nResultPageSize;
                    var nResultEndIndex = (nResultPageNo) * nResultPageSize;
                    if (arrMatchString != null) {
                        for (var nIdxMatch = 0; nIdxMatch < arrMatchString.length; nIdxMatch++) {
                            var strFindString = arrMatchString[nIdxMatch];
                            nIndexOf_FindPos = strContent.indexOf(strFindString, nIndexOf_StartPos);

                            if (nIdxMatch >= nResultStartIndex && nIdxMatch < nResultEndIndex) {
                                arrSearchPosition[nIdxPosition] = new Array();
                                arrSearchPosition[nIdxPosition]['pos'] = nIndexOf_FindPos;
                                arrSearchPosition[nIdxPosition]['findpage'] = false;
                                nIdxPosition++;

                                arrSearchPosition[nIdxPosition] = new Array();
                                arrSearchPosition[nIdxPosition]['pos'] = nIndexOf_FindPos + strFindString.length;
                                arrSearchPosition[nIdxPosition]['findpage'] = false;
                                nIdxPosition++;
                            }
                            nIndexOf_StartPos = nIndexOf_FindPos + strFindString.length;
                        }
                    }
                    //Timer('2. make array find position list','E',2);

                    // 3. position to cfi array
                    //Timer('3. position to cfi array','S',2);
                    if (arrMatchString != null) {
                        //var iTextNode = document.evaluate("/html/body//text()", document.body, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
                        var iTextNode = document.evaluate(".//text()", document.body, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
                        var nSumTotalCount = 0;
                        var nSumCurrentStartCount = 0;
                        var objPosition = null;
                        var nPageWidth = 0;
                        var nIdxArray = 0;
                        var nIdxPosition = 0;
                        var objTextNode_Idx = 0;
                        var nCurrentNodeValueLen = 0;
                        var bNextCalPage = false;

                        nPageWidth = RunaEngine.DeviceInfo.IsVScrollMode() ? m_PageHeight : m_PageWidth;

                        //for (var objTextNode = iTextNode.iterateNext(); objTextNode != null; objTextNode = iTextNode.iterateNext(), objTextNode_Idx++) {
                        for (objTextNode_Idx = 0; objTextNode_Idx < iTextNode.snapshotLength; objTextNode_Idx++) {
                            objTextNode = iTextNode.snapshotItem(objTextNode_Idx);

                            nCurrentNodeValueLen = objTextNode.nodeValue.length
                            nSumTotalCount += nCurrentNodeValueLen;

                            //RunaEngine.log('[' + objTextNode.nodeValue.length + '/' + nSumTotalCount + '] ' + objTextNode.nodeValue);

                            while (arrSearchPosition.length > nIdxPosition) {

                                //RunaEngine.log('[' + nIdxPosition + '/' + arrSearchPosition.length  + '] ');
                                //RunaEngine.log('[' + arrSearchPosition[nIdxPosition]['pos'] + '/' + nSumTotalCount + '] ');

                                if (arrSearchPosition[nIdxPosition]['pos'] <= nSumTotalCount) {

                                    //RunaEngine.log('TRUE [' + arrSearchPosition[nIdxPosition]['pos'] + '] ');

                                    if (isUseCfiAttribute) {
                                        // 여기서 기존 encodeCfi를 구하면 시간이 너무 오래 걸려서, cfi를 속성으로 추가한후에 구함 (이게 바로 FAST CFI)
                                        arrSearchPosition[nIdxPosition]['cfi'] = objTextNode['cfi'] + ':' + (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount);

                                        //RunaEngine.log('[' + arrSearchPosition[nIdxPosition]['cfi'] + '] ');
                                    }
                                    else {
                                        // 10개 이내면, 그냥 CFI사용
                                        arrSearchPosition[nIdxPosition]['cfi'] = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, objTextNode, arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount, '');

                                        //RunaEngine.log('[' + arrSearchPosition[nIdxPosition]['cfi'] + '] ');
                                    }

                                    //RunaEngine.log('FALSE [' + arrSearchPosition[nIdxPosition]['pos'] + '] ');


                                    // 페이지 번호 구하기
                                    if (nIdxPosition % 2 == 0) {
                                        //(2만건에 1.7초 PC기준)
                                        // 짝수인 경우(짝이 시작위치,홀이 종료위치라서 시작위치에만 페이지 번호 넣음)

                                        bNextCalPage = false;
                                        //RunaEngine.log('Search -----[' + nIdxPosition +'], '+ (arrSearchPosition[nIdxPosition]['pos'] +','+ nSumCurrentStartCount));

                                        var range = document.createRange();

                                        try {
                                            //RunaEngine.log("------------ERROR0----------\r\n");
                                            range.setStart(objTextNode, (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount));

                                            if (nCurrentNodeValueLen < (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount + 1)) {
                                                //range.setEnd(objTextNode, nCurrentNodeValueLen);
                                                arrSearchPosition[nIdxPosition]['findpage'] = false;
                                                bNextCalPage = true;
                                                nIdxPosition++;
                                                continue;
                                            }
                                            else {
                                                range.setEnd(objTextNode, (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount) + 1);
                                            }


                                            var rects = range.getClientRects();
                                            var maxLeft = 0;

                                            for (var k = 0; k < rects.length; k++) {
                                                if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                                                    if (maxLeft < parseInt(rects[k]['top'], 10)) {
                                                        maxLeft = parseInt(rects[k]['top'], 10);
                                                    }
                                                }
                                                else {
                                                    if (maxLeft < parseInt(rects[k]['left'], 10)) {
                                                        maxLeft = parseInt(rects[k]['left'], 10);
                                                    }
                                                }
                                            }

                                            //RunaEngine.log("------------ERROR1----------\r\n");

                                            var nMovePageNo = parseInt((maxLeft / nPageWidth));
                                            if (isNaN(nMovePageNo) == true) {
                                                nMovePageNo = 0;
                                            }

                                            //RunaEngine.log('maxLeft : '+ maxLeft + ', MovePageNo : '+ nMovePageNo +', Text : '+objTextNode.nodeValue);

                                            arrSearchPosition[nIdxPosition]['page_no'] = parseInt(nMovePageNo) + 1;
                                            arrSearchPosition[nIdxPosition]['findpage'] = true;
                                        }
                                        catch (eInitialize) {
                                            arrSearchPosition[nIdxPosition]['page_no'] = -1;
                                            //RunaEngine.log("------------ERROR2----------\r\n");
                                            //RunaEngine.log(eInitialize.stack);
                                        }
                                    }
                                    else {
                                        // 빈건으로 등록 (이래야 index 오류 안남)

                                        if (bNextCalPage) {
                                            var range = document.createRange();

                                            try {
                                                //RunaEngine.log("------------ERROR3----------\r\n");
                                                range.setStart(objTextNode, (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount));

                                                if (nCurrentNodeValueLen < (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount + 1)) {
                                                    //range.setEnd(objTextNode, nCurrentNodeValueLen);
                                                }
                                                else {
                                                    range.setEnd(objTextNode, (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount) + 1);
                                                }


                                                var rects = range.getClientRects();
                                                var maxLeft = 0;

                                                for (var k = 0; k < rects.length; k++) {
                                                    if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                                                        if (maxLeft < parseInt(rects[k]['top'], 10)) {
                                                            maxLeft = parseInt(rects[k]['top'], 10);
                                                        }
                                                    }
                                                    else {
                                                        if (maxLeft < parseInt(rects[k]['left'], 10)) {
                                                            maxLeft = parseInt(rects[k]['left'], 10);
                                                        }
                                                    }
                                                }

                                                //RunaEngine.log("------------ERROR4----------\r\n");

                                                var nMovePageNo = parseInt((maxLeft / nPageWidth));
                                                if (isNaN(nMovePageNo) == true) {
                                                    nMovePageNo = 0;
                                                }

                                                //RunaEngine.log('maxLeft : '+ maxLeft + ', MovePageNo : '+ nMovePageNo +', Text : '+objTextNode.nodeValue);

                                                arrSearchPosition[nIdxPosition]['page_no'] = parseInt(nMovePageNo) + 1;
                                                arrSearchPosition[nIdxPosition]['findpage'] = true;
                                            }
                                            catch (eInitialize) {
                                                arrSearchPosition[nIdxPosition]['page_no'] = -1;
                                                //RunaEngine.log("------------ERROR5----------\r\n");
                                                //RunaEngine.log(eInitialize.stack);
                                            }
                                        }
                                        else {
                                            //RunaEngine.log("arrSearchPosition[nIdxPosition]['pos']");
                                            arrSearchPosition[nIdxPosition]['page_no'] = -1;
                                            //RunaEngine.log("arrSearchPosition[nIdxPosition]['page_no']");
                                        }

                                        bNextCalPage = false;

                                    }

                                    // 다음 검색된 텍스트 위치
                                    nIdxPosition++;
                                    continue;
                                }
                                else {
                                    // 범위벗어나면, 다음 텍스트 노드 가져옴
                                    nSumCurrentStartCount += nCurrentNodeValueLen;
                                    break;
                                }
                            }
                        }
                    }

                    // 4. 결과셋만들기 및 텍스트 추출

                    if (arrMatchString != null) {
                        for (var i = 0; i < arrSearchPosition.length; i = i + 2) {
                            jsonReturn['items'][i / 2] = {};
                            jsonReturn['items'][i / 2]['cfi_start'] = arrSearchPosition[i]['cfi'];
                            jsonReturn['items'][i / 2]['cfi_end'] = arrSearchPosition[i + 1]['cfi'];

                            if (arrSearchPosition[i]['findpage'] == true)
                                jsonReturn['items'][i / 2]['page_num'] = arrSearchPosition[i]['page_no']; // page_no 복사
                            else
                                jsonReturn['items'][i / 2]['page_num'] = arrSearchPosition[i + 1]['page_no']; // page_no 복사

                            //RunaEngine.log("arrSearchPosition["+i+"]");

                            var nTextLen = parseInt(arrSearchPosition[i + 1]['pos'] - arrSearchPosition[i]['pos']);
                            var nTextGap = parseInt((nResultTextMaxCount - nTextLen) / 2);

                            var strHeaderLimit = '';
                            if (arrSearchPosition[i]['pos'] - (nTextGap * 2) <= 0) {
                                strHeaderLimit = strContent.substring(0, arrSearchPosition[i]['pos']).replace(/[\n\r\t\f\x0b\xa0]/gm, ""); // 유의 substring
                            }
                            else {
                                strHeaderLimit = strContent.substr(arrSearchPosition[i]['pos'] - (nTextGap * 2), nTextGap * 2)// 유의 substring
                            }
                            strHeaderLimit = strHeaderLimit.replace(/[\n\r\t\f\x0b\xa0]/gm, "");
                            var strFooterLimit = strContent.substr(arrSearchPosition[i + 1]['pos'], nTextGap * 2).replace(/[\n\r\t\f\x0b\xa0]/gm, ""); // gap 2배만큼 많은 텍스트를 얻어와서, 텍스트만 추출해서 Gap만큼만 가져옴
                            jsonReturn['items'][i / 2]['result_text'] = strHeaderLimit + strFindTextHeaderTag + strContent.substr(arrSearchPosition[i]['pos'], nTextLen) + strFindTextFooterTag + strFooterLimit;

                            //RunaEngine.log('id : ' + i + ', ' + jsonReturn['items'][i / 2]['cfi_start'] + ', ' + jsonReturn['items'][i / 2]['cfi_end']);
                        }
                    }


                    jsonReturn['error_code'] = 0;
                    jsonReturn['error_msg'] = '';
                    jsonReturn['total_cnt'] = (arrMatchString != null && arrMatchString.length > 0) ? arrMatchString.length : 0;
                    jsonReturn['curr_cnt'] = (jsonReturn['items'] != null && jsonReturn['items'].length > 0) ? jsonReturn['items'].length : 0;
                    return jsonReturn;
                }
                catch (ex) {
                    //RunaEngine.log("------------ERROR1----------\r\n");
                    //RunaEngine.log(ex.stack);
                    jsonReturn['error_code'] = -1;
                    jsonReturn['error_msg'] = '';
                    return jsonReturn;
                }
            }

            this._XPathToCfi = function (strStartPath, nStartOffset, strEndXPath, nEndOffset) {
                var convCfi = new Array();
                var strConvCfi = new Array();
                var rtnDraw = null;

                var iStartTextNode = document.evaluate(strStartPath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
                var iEndTextNode = document.evaluate(strEndXPath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);

                try {
                    //RunaEngine.log('----------------'+iStartTextNode.singleNodeValue);
                    //RunaEngine.log('----------------'+iEndTextNode.singleNodeValue);

                    if (iStartTextNode.singleNodeValue != null && iEndTextNode.singleNodeValue != null) {

                        convCfi[0] = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, iStartTextNode.singleNodeValue, nStartOffset, '');
                        convCfi[1] = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, iEndTextNode.singleNodeValue, nEndOffset, '');

                        //RunaEngine.log('[_XPathToCfi][SearchControl._XPathToCfi] ' + convCfi[0]);
                        //RunaEngine.log('[_XPathToCfi][SearchControl._XPathToCfi] ' + convCfi[1]);

                        rtnDraw = RunaEngine.HtmlCtrl.Highlight._GetDrawNodeByCfi(convCfi[0], convCfi[1], true, true);

                        if (rtnDraw.constructor == Array) {
                            //RunaEngine.log('selNode ---- : '+'(000)' + rtnDraw[0]['SelectionText']);
                            strConvCfi.start = convCfi[0];
                            strConvCfi.end = convCfi[1];
                            strConvCfi.text = rtnDraw[0]['SelectionText'];
                        }

                        return strConvCfi;
                    }
                    else {
                        //RunaEngine.log('[_XPathToCfi][SearchControl._XPathToCfi] ' + convCfi[0]);
                        //RunaEngine.log('[_XPathToCfi][SearchControl._XPathToCfi] ' + convCfi[1]);
                    }
                }
                catch (ex) {
                    //RunaEngine.log("------------_XPathToCfi error ----------\r\n");
                    //RunaEngine.log(ex);
                }
                finally {
                    rtnDraw = null;
                    convCfi = null;
                    strConvCfi = null;
                }

                return strConvCfi;
            }

            this._XPathToCfi2 = function (strStartPath, nStartOffset) {
                var convCfi = new Array();
                var strConvCfi = new Array();
                var rtnDraw = null;
                var iStartTextNode = null;


                try {

                    iStartTextNode = document.evaluate(strStartPath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);

                    if (iStartTextNode.singleNodeValue != null) {

                        convCfi[0] = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, iStartTextNode.singleNodeValue, nStartOffset, '');

                        strConvCfi.start = convCfi[0];
                        strConvCfi.end = convCfi[0];
                        strConvCfi.text = '';

                        return strConvCfi;
                    }
                    else {
                        //RunaEngine.log('[_XPathToCfi][SearchControl._XPathToCfi] ' + convCfi[0]);
                        //RunaEngine.log('[_XPathToCfi][SearchControl._XPathToCfi] ' + convCfi[1]);
                    }
                }
                catch (ex) {
                    //RunaEngine.log("------------_XPathToCfi error ----------\r\n");
                    //RunaEngine.log(ex);
                }
                finally {
                    rtnDraw = null;
                    convCfi = null;
                    strConvCfi = null;
                }

                return strConvCfi;
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // TTS를 위한 함수들
        ///////////////////////////////////////////////////////////////////////
        this.TTSHandler = new function _TTSHandler() {
            var mSentenceList = null;
            var mSentenceMap = null;
            var mSentenceSeq = 0;
            var hlCreateId = 'bdb_tts_hl';
            var mInitPage = false;
            var mLastNodeInfo = null;

            this.TTSInitPage = function (side) {
                var caretRangeStart = null;
                var caretRangeEnd = null;
                var pageText = null;
                var iterator = null;
                var isStart = false, isEnd = false, isAddedFirstSentence = false;
                var nodeCfi = null, nodeCfiOffset = 0;
                var startCfi = null, endCfi = null;
                var startCfiOffset = 0, endCfiOffset = 0;
                var startCfiVal = 0, endCfiVal = 0;
                var curSentence = null;
                var range = null;

                var modifyOffset = null;
                var modifyRect = false;
                var rangeRect = null;

                var nodeInfo = null;
                var checkFirstSentence = false;

                try {
                    mSentenceList = null;
                    mSentenceSeq = 0;
                    mInitPage = false;
                    mSentenceList = new Array();
                    mSentenceMap = new RunaEngineMap();

                    var ttsRectInfo = RunaEngine.HtmlCtrl.TTSHandler.getTTSRectInfo(side);
                    caretRangeStart = ttsRectInfo.caretRangeStart;
                    caretRangeEnd = ttsRectInfo.caretRangeEnd;
                    modifyRect = ttsRectInfo.modifyRect;
                    pageText = ttsRectInfo.pageText;

                    if (pageText.length == 0) {
                        return;
                    }

                    startCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, caretRangeStart.startContainer, caretRangeStart.startOffset, "");
                    endCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, caretRangeEnd.endContainer, modifyRect ? caretRangeEnd.endOffset : caretRangeEnd.endContainer.length, "");

                    if (startCfi.indexOf(':') > 0) {
                        startCfiOffset = parseInt(startCfi.split(':')[1]);
                        startCfi = startCfi.split(':')[0];
                    }

                    if (endCfi.indexOf(':') > 0) {
                        endCfiOffset = parseInt(endCfi.split(':')[1]);
                        endCfi = endCfi.split(':')[0];
                    }

                    iterator = document.createNodeIterator(m_divContentLayout, NodeFilter.SHOW_ALL, ElementChecker, false);

                    var node = iterator.nextNode();

                    while (node) {
                        nodeCfiOffset = 0;

                        if (mLastNodeInfo != null && !isAddedFirstSentence) {
                            if (mLastNodeInfo.node == node) {
                                isStart = true;

                                nodeInfo = { nodeCfi: mLastNodeInfo.nodeCfi, nodeCfiOffset: mLastNodeInfo.nodeCfiOffset, node: node };

                                curSentence = node.textContent;

                                isAddedFirstSentence = true;
                                if (isStart) {
                                    RunaEngine.HtmlCtrl.TTSHandler.makeSentences(nodeInfo, curSentence, endCfi, endCfiOffset);
                                }

                                mLastNodeInfo = null;

                                node = iterator.nextNode();
                                continue;
                            }
                            else {
                                node = iterator.nextNode();
                                continue;
                            }
                        }

                        var nodeParent = RunaEngine.HtmlCtrl.TTSHandler.upNodeCheck(node);
                        var startCfiParent = RunaEngine.HtmlCtrl.TTSHandler.upNodeCheck(caretRangeStart.startContainer);

                        if (node.nodeType == 1 || node.nodeType == 8) {

                            if (nodeParent == startCfiParent) {
                                // startCfi와 node가 일치 하지만 br tag 등으로 건너 뛰는 경우 체크
                                isStart = true;
                            }

                            node = iterator.nextNode();

                            continue;
                        }

                        nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, nodeCfiOffset, "");

                        if (nodeCfi == null || nodeCfi == '') {
                            node = iterator.nextNode();
                            if (node == null) {
                                break;
                            }
                            continue;
                        }

                        if (nodeCfi.indexOf(':') > 0) {
                            nodeCfiOffset = parseInt(nodeCfi.split(':')[1]);
                            nodeCfi = nodeCfi.split(':')[0];
                        }

                        nodeInfo = { nodeCfi: nodeCfi, nodeCfiOffset: nodeCfiOffset, node: node };
                        curSentence = node.textContent;

                        if (nodeParent == startCfiParent && !isAddedFirstSentence) {
                            isStart = true;
                            checkFirstSentence = true;

                            if (isStart) {
                                isAddedFirstSentence = true;
                                RunaEngine.HtmlCtrl.TTSHandler.makeSentences(nodeInfo, curSentence, endCfi, endCfiOffset);
                            }
                        }
                        else if (nodeCfi == endCfi) {
                            /**
                             * redmine #refs 33946
                             * cfi 값을 비교해서 node cfi가  endCfi보다 클 경우 break;
                             * 만년대리 97페이지 (Expert)
                             * by junghoon.kim
                             */

                            isEnd = RunaEngine.HtmlCtrl.TTSHandler.makeSentences(nodeInfo, curSentence, endCfi, endCfiOffset);

                            if (isEnd) {
                                break;
                            }
                        }
                        else {
                            /**
                             * redmine #refs 3205
                             * cfi 값을 비교해서 node cfi가  endCfi보다 클 경우 break;
                             * by junghoon.kim
                             */
                            var isOver = RunaEngine.HtmlCtrl.TTSHandler.checkOverEndCfi(nodeCfi, endCfi);
                            if (isOver) {
                                break;
                            }

                            if (isStart) {
                                RunaEngine.HtmlCtrl.TTSHandler.makeSentences(nodeInfo, curSentence, endCfi, endCfiOffset);
                            }
                        }

                        node = iterator.nextNode();
                    }

                    RunaEngine.HtmlCtrl.TTSHandler.removeLastNoneMarkSentences(node);
                    RunaEngine.HtmlCtrl.TTSHandler.reArrangeSentences();

                    if (checkFirstSentence) {
                        RunaEngine.HtmlCtrl.TTSHandler.removeSentencesBeforeStartCfi(startCfi, startCfiOffset);
                    }

                    mInitPage = true;
                }
                catch (e) {
                    mInitPage = false;
                }
                finally {
                    caretRangeStart = null;
                    caretRangeEnd = null;
                    pageText = null;
                    iterator = null;
                    pageSentence = null;
                    isStart = false, isEnd = false;
                    nodeCfi = null, nodeCfiOffset = 0;
                    curSentence = null;
                    range = null;
                    startCfi = null, endCfi = null;
                    modifyOffset = null;
                    rangeRect = null;
                    modifyRect = false;

                    if (mSentenceList != null)
                        return mSentenceList.length;
                    else
                        return 0;
                }
            }

            this.makeSentences = function (nodeInfo, curSentence, endCfi, endCfiOffset) {
                var tmpSentence = RunaEngine.HtmlCtrl.TTSHandler.removeWhiteSpace(curSentence);
                var match = RunaEngine.HtmlCtrl.TTSHandler.getMarkChar(curSentence);
                var nodeCfi = nodeInfo.nodeCfi;
                var nodeCfiOffset = nodeInfo.nodeCfiOffset;
                var node = nodeInfo.node;
                var sentence;

                if (match == null || tmpSentence.length == 0) {

                    if (tmpSentence.length > 0) {
                        if (endCfi == nodeCfi && curSentence.length > endCfiOffset) {
                            mLastNodeInfo = { nodeCfi: nodeCfi, nodeCfiOffset: 0, node: node };
                            return true;
                        }

                        RunaEngine.HtmlCtrl.TTSHandler.pushSentence(tmpSentence, curSentence, nodeInfo);
                    }

                    return false;
                }

                if (match.length >= 1) {
                    var i = 0;
                    var lastIndex1 = curSentence.length;
                    var lastIndex2 = 0;

                    var breakLoop = false;

                    while (true) {

                        lastIndex1 = lastIndex2;

                        lastIndex2 = match[i++];

                        if (lastIndex2 == -1) {
                            lastIndex2 = curSentence.length - 1;
                            breakLoop = true;
                        }
                        else {
                            if (mLastNodeInfo != null && lastIndex2 <= mLastNodeInfo.nodeCfiOffset) {
                                continue;
                            }

                            // ” check
                            if (curSentence.substring(lastIndex2 + 1, lastIndex2 + 2) == "”" || curSentence.substring(lastIndex2 + 1, lastIndex2 + 2) == "’") {
                                lastIndex2 = lastIndex2 + 1;
                            }

                            RunaEngine.log('tmpSentence 1313');
                        }

                        if (nodeCfi == endCfi && lastIndex2 >= endCfiOffset) {
                            mLastNodeInfo = { nodeCfi: nodeCfi, nodeCfiOffset: lastIndex1, node: node };
                            return true;
                        }

                        sentence = curSentence.substring(lastIndex1 == 0 ? 0 : (lastIndex1 + 1), lastIndex2 + 1);

                        RunaEngine.HtmlCtrl.TTSHandler.pushSentenceWithLastIndex(sentence, nodeInfo, lastIndex1, lastIndex2);

                        if (breakLoop) {
                            break;
                        }
                    }
                }

                return false;
            }

            this.checkOverEndCfi = function (nodeCfi, endCfi, endCfiOffset, nodeCfiOffset) {
                var rtnCompare = RunaEngine.HtmlCtrl.Highlight._CheckCfiCompare(nodeCfi, endCfi);

                RunaEngine.log("checkCfiCompare big = " + rtnCompare['Big'] + "small = " + rtnCompare['Small']);

                if (nodeCfi == rtnCompare['Small'] && nodeCfi != rtnCompare['Big']) {
                    RunaEngine.log("nodeCfi == rtnCompare['Small'] break!!!!");
                    return true;
                }

                return false;
            }

            this.getTTSRectInfo = function (side) {
                var caretRangeStart = null;
                var caretRangeEnd = null;
                var range = null;
                var modifyRect = false;
                var rangeRect = null;
                var rectInfo = null;

                if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                    var height = (m_PageHeight - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2));

                    caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, m_ColPaddingWidth - 5, m_ColPaddingWidth - 5);
                    caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - 1, height - 1);
                }
                else {
                    if (RunaEngine.DeviceInfo.IsAndroid()) {
                        /**
                         * redmine refs #34101
                         * 마지막 문장을 읽지 않는 경우 발생
                         * sub으로 나눠어져 있을 경우 m_PageHeight로 range를 가져오면 해당 텍스트가 누락됨.
                         * js에서 마진을 주는 부분이 있으므로 동일하게 처리 필요
                         * by junghoon.kim
                         */
                        var height = (m_PageHeight - (m_ColMarginWidth * 2) - (m_ColPaddingWidth * 2));
                        if (RunaEngine.DeviceInfo.IsAndroid_PAD() && RunaEngine.HtmlCtrl.getOrientation() == 1) {
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, (side ? m_PageWidth + m_ColPaddingWidth - 5 : m_ColPaddingWidth - 5), m_ColPaddingWidth - 5);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, (side ? (m_PageWidth + m_PageWidth - 1) : m_PageWidth - 1), height - 1);
                        }
                        else {
                            /**
                             * redmine refs #5115
                             * I don't want to 문장을 한번더 읽음 (페이지의 마지막 문장을 한번 더 읽은 케이스가 있는데 해당 문제 수정)
                             * by junghoon.kim
                             */
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, m_ColPaddingWidth - 5, m_ColPaddingWidth - 5);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - 1, height - 1);
                        }
                    }
                    else if (RunaEngine.DeviceInfo.isPC()) {
                        caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, ((page - 1) * m_PageWidth), 0);
                        caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, ((page - 1) * m_PageWidth), m_PageHeight);
                    }
                    else {
                        if ((RunaEngine.DeviceInfo.IsIPAD() && RunaEngine.HtmlCtrl.getOrientation() == 0) || (RunaEngine.HtmlCtrl.getOrientation() == 0 && RunaEngine.DeviceInfo.IsOSX())) {
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, (side ? m_PageWidth : 0), 0);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, (side ? (m_PageWidth + m_PageWidth - 1) : m_PageWidth - 1), m_PageHeight - 1);
                        }
                        else {
                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, 0, 0);
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, m_PageWidth - 1, m_PageHeight - 1);
                        }
                    }
                }

                var rangeInfo = RunaEngine.HtmlCtrl.TTSHandler.checkRunaContentLayout(caretRangeStart, caretRangeEnd, side);

                caretRangeStart = rangeInfo.caretRangeStart;
                caretRangeEnd = rangeInfo.caretRangeEnd;

                if (caretRangeStart == null || caretRangeEnd == null) {
                    return;
                }

                range = document.createRange();
                range.setStart(caretRangeStart.startContainer, caretRangeStart.startOffset);

                /**
                 * redmine #refs 33294
                 * endContainer len 이 undefined인 경우 방어 코드
                 * by junghoon.kim
                 */
                if (RunaEngine.DeviceInfo.IsAndroid() && caretRangeEnd.endContainer.length == undefined) {
                    var isFind = false;
                    for (var i = m_PageHeight - 1; i > 0; i -= 20) {

                        for (var k = (side ? (m_PageWidth * 2 - 1) : (m_PageWidth - 1)) ; k > (side ? m_PageWidth : 0) ; k -= 20) {

                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, k, i);

                            if (m_divContentLayout == caretRangeEnd.endContainer) {
                            }
                            else {
                                if (caretRangeEnd.endContainer.length != undefined) {
                                    isFind = true;
                                    break;
                                }
                            }
                        }

                        if (isFind) {
                            break;
                        }
                    }

                    range.setEnd(caretRangeEnd.endContainer, caretRangeEnd.endContainer.length);
                }

                // by : Kyungmin, for iOS, OSX
                // #16178 TTS 반복 읽는 현상 특정 상황에서 endNode가 다음페이지의 노드를 가리키는 경우가 생김

                /**
                 * redmine refs #29749
                 * endOffset으로 할 경우 실제로는 lenth가 긴데 offset이 1이 나와 마지막 문장을 찾지 못하는 문제로 인해 length로 주고 rect를 넘어갈 경우의 방어코드 작성
                 * by junghoon.kim
                 */
                if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                    range.setEnd(caretRangeEnd.endContainer, caretRangeEnd.endContainer.length);
                } else {
                    range.setEnd(caretRangeEnd.endContainer, caretRangeEnd.endOffset);
                    modifyRect = true;
                }


                /**
                 * redmine refs #34101
                 * 마지막 문장을 읽지 않는 경우 또는 다음 페이지의 문장까지 읽는 문제 발생
                 * js에서 마진을 주는 부분이 있으므로 동일하게 처리 하였는데 컨텐츠에 따라 rect 정보가 잘못 되어 있다.
                 * 마지막 rect가 중간의 rect보다 위에 있으면 다음 페이지까지 rect가 생성된 것이므로 영역 조절 적용
                 * 슬픔의 세레나데 (161페이지, 181페이지 사운드), 그릿 (15페이지 Expert)
                 * by junghoon.kim
                 */
                var isNeedRecalRectInfo = false;

                if (RunaEngine.DeviceInfo.IsAndroid()) {
                    var rects = range.getClientRects();
                    var firstRectBottom;
                    var centerRectBottom = -1;
                    for (var i = 0; i < rects.length; i++) {
                        if (rects[i].top > 0 && rects[i].width > 1) {
                            firstRectBottom = rects[i].bottom;
                            break;
                        }
                    }

                    if (rects.length > 1) {
                        var cIndex = parseInt(rects.length / 2);
                        centerRectBottom = rects[cIndex].bottom;
                    }

                    if (rects[rects.length - 1].bottom < firstRectBottom || rects[rects.length - 1].bottom < centerRectBottom) {
                        if (rects[rects.length - 1].width > 1) {
                            isNeedRecalRectInfo = true;
                        }
                        else if (rects.length > 2) {
                            if (rects[rects.length - 2].width > 1 && (rects[rects.length - 2].bottom < firstRectBottom || rects[rects.length - 2].bottom < centerRectBottom)) {
                                isNeedRecalRectInfo = true;
                            }
                        }
                    }
                    else if (rects[rects.length - 1].left > m_PageWidth) {
                        isNeedRecalRectInfo = true;
                    }
                }

                if (RunaEngine.DeviceInfo.IsAndroid() && isNeedRecalRectInfo) {
                    var isFind = false;
                    for (var i = m_PageHeight - 1; i > 0; i -= 20) {
                        caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, (side ? (m_PageWidth + m_PageWidth - 1) : m_PageWidth - 1), i);
                        if (m_divContentLayout == caretRangeEnd.endContainer) {
                        }
                        else {
                            range.setEnd(caretRangeEnd.endContainer, caretRangeEnd.endOffset);
                            var rects = range.getClientRects();
                            var firstRectBottom;
                            var centerRectBottom = -1;
                            for (var k = 0; k < rects.length; k++) {
                                if (rects[k].top > 0 && rects[k].width > 1) {
                                    firstRectBottom = rects[k].bottom;
                                    break;
                                }
                            }
                            if (rects.length > 1) {
                                var cIndex = parseInt(rects.length / 2);
                                centerRectBottom = rects[cIndex].bottom;
                            }

                            if (rects[rects.length - 1].width > 1 && (rects[rects.length - 1].bottom < firstRectBottom || rects[rects.length - 1].bottom < centerRectBottom)) {
                                continue;
                            }
                            else {
                                isFind = true;
                            }
                        }

                        if (isFind) {
                            modifyRect = true;
                            break;
                        }
                    }
                }

                rangeRect = range.getBoundingClientRect();

                /**
                 * redmine refs #5115
                 * Range를 못찾는 문제로 변경
                 * by junghoon.kim
                 */

                if (rangeRect == null) {
                    var isFind = false;
                    for (var i = m_PageHeight - 1; i > 0; i -= 20) {
                        for (var k = (side ? (m_PageWidth * 2 - 1) : (m_PageWidth - 1)) ; k > (side ? m_PageWidth : 0) ; k -= 20) {
                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, k, i);
                            if (m_divContentLayout == caretRangeEnd.endContainer) {
                            }
                            else {
                                range.setStart(caretRangeStart.startContainer, caretRangeStart.startOffset);
                                range.setEnd(caretRangeEnd.endContainer, caretRangeEnd.endOffset);
                                rangeRect = range.getBoundingClientRect();
                                if (rangeRect != null) {
                                    isFind = true;
                                    break;
                                }
                            }
                        }

                        if (isFind) {
                            modifyRect = true;
                            break;
                        }
                    }
                }


                if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                }
                else {
                    if (side == 1 && RunaEngine.HtmlCtrl.getOrientation() == 1) {
                        if (rangeRect.left < m_PageWidth) {
                            return;
                        }
                    }
                }

                pageText = range.toString();

                // 마지막 문장에 개행이 있는 경우 pageText에서 개행을 replace하면 매칭이 안되 마지막 문장을 건너 뛰는 경우가 생김 이에 주석 처리함
                //                pageText = pageText.replace(/\n\n/gi, '');

                rectInfo = { caretRangeStart: caretRangeStart, caretRangeEnd: caretRangeEnd, modifyRect: modifyRect, pageText: pageText };

                return rectInfo;
            }

            this.checkRunaContentLayout = function (caretRangeStart, caretRangeEnd, side) {
                if (m_divContentLayout == caretRangeStart.startContainer) {
                    var bfind = false;

                    for (var i = m_ColPaddingWidth; i < m_PageHeight; i += 20) {

                        for (var k = (side ? m_PageWidth : 0) ; k < (side ? m_PageWidth * 2 : m_PageWidth) ; k += 20) {

                            caretRangeStart = RunaEngine.Util.caretRangeFromPoint(document, k, i);

                            if (m_divContentLayout == caretRangeStart.startContainer) {
                            }
                            else {
                                bfind = true;
                                break;
                            }
                        }

                        if (bfind) break;
                    }
                }

                if (m_divContentLayout == caretRangeEnd.endContainer) {
                    var bfind = false;

                    for (var i = m_PageHeight - 1; i > 0; i -= 20) {

                        for (var k = (side ? (m_PageWidth * 2 - 1) : (m_PageWidth - 1)) ; k > (side ? m_PageWidth : 0) ; k -= 20) {

                            caretRangeEnd = RunaEngine.Util.caretRangeFromPoint(document, k, i);

                            if (m_divContentLayout == caretRangeEnd.endContainer) {
                            }
                            else {
                                bfind = true;
                                break;
                            }
                        }

                        if (bfind) break;
                    }
                }

                var rangeInfo = { caretRangeStart: caretRangeStart, caretRangeEnd: caretRangeEnd };
                return rangeInfo;
            }

            this.pushSentenceWithLastIndex = function (sentence, nodeInfo, lastIndex1, lastIndex2) {
                var tmpSentence = RunaEngine.HtmlCtrl.TTSHandler.removeWhiteSpace(sentence);
                if (tmpSentence.length > 0) {
                    if (tmpSentence.length != sentence.length) {
                        modifyOffset = RunaEngine.HtmlCtrl.TTSHandler.getOffsetFromString(sentence);

                        var pageSentence = { start: nodeInfo.nodeCfi + ':' + (lastIndex1 == 0 ? modifyOffset.start : (lastIndex1 + 1 + modifyOffset.start)), end: nodeInfo.nodeCfi + ':' + (lastIndex2 + 1 - modifyOffset.end), sentence: tmpSentence, CurrentNode: nodeInfo.node };
                        if (mSentenceMap.get(pageSentence.start) == null) {
                            mSentenceList.push(pageSentence);
                            mSentenceMap.put(pageSentence.start, pageSentence);
                        }
                    }
                    else {
                        var pageSentence = { start: nodeInfo.nodeCfi + ':' + (lastIndex1 == 0 ? '0' : (lastIndex1 + 1)), end: nodeInfo.nodeCfi + ':' + (lastIndex2 + 1), sentence: tmpSentence, CurrentNode: nodeInfo.node };
                        if (mSentenceMap.get(pageSentence.start) == null) {
                            mSentenceList.push(pageSentence);
                            mSentenceMap.put(pageSentence.start, pageSentence);
                        }
                    }

                    return true;
                }

                return false;
            }

            this.pushSentence = function (tmpSentence, curSentence, nodeInfo) {
                if (tmpSentence.length != curSentence.length) {
                    modifyOffset = RunaEngine.HtmlCtrl.TTSHandler.getOffsetFromString(curSentence);
                    var pageSentence = { start: nodeInfo.nodeCfi + ':' + modifyOffset.start, end: nodeInfo.nodeCfi + ':' + (curSentence.length - modifyOffset.end), sentence: tmpSentence, CurrentNode: nodeInfo.node };
                    if (mSentenceMap.get(pageSentence.start) == null) {
                        mSentenceList.push(pageSentence);
                        mSentenceMap.put(pageSentence.start, pageSentence);
                    }
                } else {
                    var pageSentence = { start: nodeInfo.nodeCfi + ':' + nodeInfo.nodeCfiOffset, end: nodeInfo.nodeCfi + ':' + (curSentence.length), sentence: tmpSentence, CurrentNode: nodeInfo.node };
                    if (mSentenceMap.get(pageSentence.start) == null) {
                        mSentenceList.push(pageSentence);
                        mSentenceMap.put(pageSentence.start, pageSentence);
                    }
                }
            }

            this.removeWhiteSpace = function (sentence) {
                var tmpSentence = sentence;
                tmpSentence = tmpSentence.replace(/^\s/gm, '');
                tmpSentence = tmpSentence.replace(/\r\n$/g, '');
                tmpSentence = tmpSentence.replace(/(^\s*)|(\s*$)/g, '');
                return tmpSentence;
            }

            this.removeLastNoneMarkSentences = function (node) {
                for (var i = mSentenceList.length - 1 ; i > 0; i--) {
                    var match = RunaEngine.HtmlCtrl.TTSHandler.getMarkChar(mSentenceList[i].sentence);
                    if (match == null) {
                        var endNodeParent = RunaEngine.HtmlCtrl.TTSHandler.upNodeCheck(node);
                        var checkNodeParent = RunaEngine.HtmlCtrl.TTSHandler.upNodeCheck(mSentenceList[i].CurrentNode);
                        if (endNodeParent != checkNodeParent) {
                            if (i == mSentenceList.length - 1) {
                                mLastNodeInfo = null;
                            }
                            break;
                        }
                        else {
                            var nodeCfi;
                            var nodeCfiOffset = 0;

                            if (mSentenceList[i].start.indexOf(':') > 0) {
                                nodeCfiOffset = parseInt(mSentenceList[i].start.split(':')[1]);
                                nodeCfi = mSentenceList[i].start.split(':')[0];
                            }

                            mLastNodeInfo = { nodeCfi: nodeCfi, nodeCfiOffset: nodeCfiOffset, node: mSentenceList[i].CurrentNode };
                            delete mSentenceList[i];
                        }
                    }
                    else {
                        break;
                    }
                }
            }

            this.removeSentencesBeforeStartCfi = function (startCfi, startCfiOffset) {
                var index = RunaEngine.HtmlCtrl.TTSHandler.FindSentence(startCfi + ":" + startCfiOffset);

                if (index > 0) {
                    mSentenceList.splice(0, index);
                }
            }

            this.reArrangeSentences = function () {
                var TempArray = new Array();
                var tempSentence;

                TempArray = new Array();
                tempSentence = null;

                for (var item in mSentenceList) {

                    var sentence01 = mSentenceList[item];

                    if (sentence01 == null) {
                        if (tempSentence != null) {
                            TempArray.push(tempSentence);
                            tempSentence = null;
                        }
                        continue;
                    }

                    // CFI 확인해서 마지막 문장이면 종료
                    var TempEndCfi = mSentenceList[item].end.split(":")[0];
                    var TempEndOffset = Number(mSentenceList[item].end.split(":")[1]);
                    var sentence02 = mSentenceList[(Number(item) + 1)];
                    if (sentence02 == null) {
                        if (tempSentence != null) {
                            TempArray.push(tempSentence);
                            tempSentence = null;
                        } else {
                            TempArray.push(mSentenceList[item]);
                        }
                        continue;
                    }

                    var currentParent = RunaEngine.HtmlCtrl.TTSHandler.upNodeCheck(sentence01.CurrentNode);
                    var nextParent = RunaEngine.HtmlCtrl.TTSHandler.upNodeCheck(sentence02.CurrentNode);
                    var match01 = RunaEngine.HtmlCtrl.TTSHandler.getMarkChar(sentence01.sentence);
                    var match02 = RunaEngine.HtmlCtrl.TTSHandler.getMarkChar(sentence02.sentence);

                    // 부모노느 검사
                    if (currentParent != nextParent) {
                        if (tempSentence != null) {
                            TempArray.push(tempSentence);
                            tempSentence = null;
                            continue;
                        } else {
                            TempArray.push(mSentenceList[item]);
                            tempSentence = null;
                            continue;
                        }
                    }

                    if (match01 != null) {
                        if (tempSentence != null) {
                            tempSentence = { start: tempSentence.start, end: sentence02.end, sentence: tempSentence.sentence + sentence02.sentence };
                            TempArray.push(tempSentence);
                            tempSentence = null;
                        } else {
                            TempArray.push(mSentenceList[item]);
                        }
                        tempSentence = null;
                        continue;
                    } else
                        if (match02 != null) {
                            if (tempSentence != null) {
                                tempSentence = { start: tempSentence.start, end: sentence02.end, sentence: tempSentence.sentence + sentence02.sentence };

                            } else {
                                tempSentence = { start: sentence01.start, end: sentence02.end, sentence: sentence01.sentence + sentence02.sentence };
                            }
                            TempArray.push(tempSentence);
                            tempSentence = null;
                            mSentenceList[(Number(item) + 1)] = null;
                            continue;
                        } else {
                            if (tempSentence != null) {
                                tempSentence = { start: tempSentence.start, end: sentence02.end, sentence: tempSentence.sentence + sentence02.sentence };

                            } else {
                                tempSentence = { start: sentence01.start, end: sentence02.end, sentence: sentence01.sentence + sentence02.sentence };
                            }
                        }
                }

                mSentenceList = TempArray;
            }

            this.upNodeCheck = function (TargetNode) {
                var TempNode = TargetNode;
                try {
                    // 컨텐츠 영역의 최상위로 h 테그가 있고 이 테그가 화면 마지막 부분에 걸리면 부모 노드를 얻어올 때 ruanenaginecontent div 가 반환되는 이슈 수정
                    // by kyungmin
                    while (!(TempNode.nodeName == "P" || TempNode.nodeName == "DIV"
                             || TempNode.nodeName == "H1"
                             || TempNode.nodeName == "H2"
                             || TempNode.nodeName == "H3"
                             || TempNode.nodeName == "H4"
                             || TempNode.nodeName == "H5"
                             || TempNode.nodeName == "H6"
                             || TempNode.nodeName == "TD"
                             || TempNode.nodeName == "BLOCKQUOTE")) {
                        TempNode = TempNode.parentNode;

                    }
                } catch (e) {
                    // TODO: handle exception
                }

                return TempNode;
            }

            this.getMarkChar = function (sentence) {
                var match = new Array();
                var regExp = /[!.?](?!\d)/gi;
                var result;
                while ((result = regExp.exec(sentence)) !== null) {

                    match.push(result.index);
                }

                if (match.length > 0) {
                    match.push(-1);
                    return match;
                }

                return null;
            }
            this.FindSentence = function (cfi) {
                var nodeCfi = null, nodeCfiOffset = 0;
                var startCfi = null, endCfi = null;
                var startCfiOffset = 0, endCfiOffset = 0;
                var sentence = null;

                try {

                    if (cfi.indexOf(':') > 0) {
                        nodeCfiOffset = parseInt(cfi.split(':')[1]);
                        nodeCfi = cfi.split(':')[0];
                    }


                    //RunaEngine.log(' RunaEngine.HtmlCtrl.TTSHandler.FindSentence : '+nodeCfi + ":" + nodeCfiOffset);

                    if (mSentenceList == null)
                        return -1;

                    for (var i = 0; i < mSentenceList.length; i++) {

                        startCfi = mSentenceList[i].start;
                        endCfi = mSentenceList[i].end;

                        startCfiOffset = 0, endCfiOffset = 0;

                        if (startCfi.indexOf(':') > 0) {
                            startCfiOffset = parseInt(startCfi.split(':')[1]);
                            startCfi = startCfi.split(':')[0];
                        }

                        if (endCfi.indexOf(':') > 0) {
                            endCfiOffset = parseInt(endCfi.split(':')[1]);
                            endCfi = endCfi.split(':')[0];
                        }

                        //RunaEngine.log(' RunaEngine.HtmlCtrl.TTSHandler.start : '+startCfi +':'+startCfiOffset);
                        //RunaEngine.log(' RunaEngine.HtmlCtrl.TTSHandler.end : '+endCfi +':'+endCfiOffset);

                        if (startCfi == nodeCfi) {
                            if (endCfi == nodeCfi) {
                                if (nodeCfiOffset >= startCfiOffset && nodeCfiOffset <= endCfiOffset) {
                                    //find
                                    //RunaEngine.log ("1-------find------------ sentence : [" + mSentenceList[i].start+", "+mSentenceList[i].end+"], --"+ mSentenceList[i].sentence);
                                    sentence = { seq: i, content: mSentenceList[i].sentence };
                                    break;

                                }
                            }
                            else {
                                //find
                                //RunaEngine.log ("2-------find------------ sentence : [" + mSentenceList[i].start+", "+mSentenceList[i].end+"], --"+ mSentenceList[i].sentence);
                                sentence = { seq: i, content: mSentenceList[i].sentence };
                                break;
                            }
                        }
                        else {
                            if (endCfi == nodeCfi) {
                                if (nodeCfiOffset <= endCfiOffset) {
                                    //find
                                    //RunaEngine.log ("3-------find------------ sentence : [" + mSentenceList[i].start+", "+mSentenceList[i].end+"], --"+ mSentenceList[i].sentence);
                                    sentence = { seq: i, content: mSentenceList[i].sentence };
                                    break;

                                }
                            }
                            else {
                                var rtnCompare = RunaEngine.HtmlCtrl.Highlight._CheckCfiCompare(nodeCfi, endCfi);
                                if (nodeCfi == rtnCompare['Big']) {
                                    //find
                                    //RunaEngine.log ("4-------find------------ sentence : [" + mSentenceList[i].start+", "+mSentenceList[i].end+"], --"+ mSentenceList[i].sentence);
                                    sentence = { seq: i, content: mSentenceList[i].sentence };
                                    break;
                                }
                            }
                        }
                    }

                }
                catch (e) {
                    //RunaEngine.sublog(e.description);
                    //RunaEngine.sublog(e);
                    sentence = null;

                }
                finally {

                    nodeCfi = null, nodeCfiOffset = 0;
                    startCfi = null, endCfi = null;

                    if (sentence != null) {
                        mSentenceSeq = sentence.seq;
                        return sentence.seq;
                    }

                    return -1;
                }
            }

            this.GetSentence = function (index, color) {

                if (mInitPage) {
                    if (mSentenceList.length > index && index >= 0) {
                        mSentenceSeq = index;

                        if (RunaEngine.DeviceInfo.IsAndroid()) {

                        } else if (RunaEngine.DeviceInfo.IsIOS()) {

                        } else {

                        }

                        return mSentenceList[index];
                    }
                }

                return '';
            }

            this.GetSentences = function () {
                if (mInitPage) {
                    return mSentenceList;
                } else {
                    return null
                }
            }

            this.HighlightTTSSentence = function (index, color) {
                if (mInitPage) {
                    RunaEngine.HtmlCtrl.Highlight.Remove(hlCreateId);
                    RunaEngine.HtmlCtrl.Highlight.CreateHighLight(hlCreateId, mSentenceList[index].start, mSentenceList[index].end, color);
                }

                return '';
            }

            this.InitLastNodeInfo = function () {
                mLastNodeInfo = null;
            }

            this.TTSEndPage = function () {
                // RunaEngine.HtmlCtrl.Highlight.Remove(hlCreateId);

                while (mSentenceList.length > 0) {
                    a = mSentenceList.pop();
                    a = null;
                }
                mSentenceMap.clear();
                mSentenceMap = null;
                mSentenceList = null;
                mSentenceSeq = 0;
                mInitPage = false;
                mLastNodeInfo = null;
            }


            function ElementChecker(node) {
                /*if( node.nodeType == 1 ||
                 node.nodeType == 3)
                 return NodeFilter.FILTER_ACCEPT;
                 else
                 return NodeFilter.FILTER_SKIP;*/
                return NodeFilter.FILTER_ACCEPT;
            }

            this.getOffsetFromString = function (str) {
                var offsets = { start: 0, end: 0 };
                var len = str.length;

                for (var i = 0; i < len; i++) {
                    if (str[i] == '\r') {
                        //RunaEngine.log('carrige return 1');
                        offsets.start++;
                    }
                    else if (str[i] == '\n') {
                        //RunaEngine.log('return 1');
                        offsets.start++;
                    }
                    else if (str[i] == ' ') {
                        //RunaEngine.log('space character 1');
                        offsets.start++;
                    }
                    else {
                        break;
                    }
                }

                //RunaEngine.log('RunaEngine.HtmlCtrl.TTSHandler.getOffsetFromString : '+len);

                for (var i = (len - 1) ; i >= 0; i--) {
                    if (str[i] == '\r') {
                        //RunaEngine.log('carrige return 2 : '+ i);
                        offsets.end++;
                    }
                    else if (str[i] == '\n') {
                        //RunaEngine.log('return 2 : '+ i);
                        offsets.end++;
                    }
                    else if (str[i] == ' ') {
                        //RunaEngine.log('space character 2 : '+ i);
                        offsets.end++;
                    }
                    else {
                        break;
                    }
                }

                return offsets;
            }
        };

        ///////////////////////////////////////////////////////////////////////
        // SMIL를 위한 함수들
        ///////////////////////////////////////////////////////////////////////
        this.SMILHandler = new function _SMILHandler() {
            var oldSelectElementID = null;

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] SMIL 개체들의 해당 파일 내 페이지 가져오기
            // [ Param  ]
            //   - (json string) ids: smil 타켓 개체들의 아이디
            this.SMILInitPage = function (ids) {
                var toJson = eval(ids);
                var totalToString = "[";
                for (var a = 0, count = toJson.length ; a < count ; a++) {
                    try {

                        if (a != 0) {
                            totalToString += ',';
                        }

                        var idNode = document.getElementById(toJson[a].id);
                        var page = RunaEngine.HtmlCtrl.PageInfo.GetNodePageNumber(idNode);
                        if (page = -1) {
                            rtnDraw = idNode.getBoundingClientRect()
                            if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                                page = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(parseInt(rtnDraw.top, 10)) - 1;
                            }
                            else {
                                page = parseInt(parseInt(rtnDraw.left, 10) / m_PageWidth, 10);
                            }
                            page = parseInt(page, 10) + 1;
                        }
                        totalToString += "{\"id\":\"" + toJson[a].id + "\",\"page\":\"" + page + "\"}";
                    }
                    catch (ex) {
                        RunaEngine.log(ex);
                        RunaEngine.log(total[a].toString());
                    }
                }
                totalToString += "]";

                return totalToString;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] SMIL 개체의 대상 엘리먼트를 하이라이트 처리 하기
            // [ Param  ]
            //   - (string) sid: smil 타켓 개체의 아이디
            this.id_highlight = function (sid) {
                if (oldSelectElementID != null) {
                    this.removeClass(oldSelectElementID, "smilhighlight");
                    oldSelectElementID = null;
                }

                var Eid = document.getElementById(sid);
                if (Eid != null) {
                    oldSelectElementID = sid;
                    Eid.className += " smilhighlight";
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 특정 엘리먼트의 특정 클래스를 제거
            // [ Param  ]
            //   - (string) oID: smil 타켓 개체의 아이디
            //   - (string) remove: 제거할 css 클래스
            this.removeClass = function (oID, remove) {
                var oldelement = document.getElementById(oID);
                var newClassName = "";
                var i;

                RunaEngine.log('SMILHandler, removeClass : ' + oldelement);

                var classes = oldelement.className.split(" ");

                for (i = 0; i < classes.length; i++) {
                    if (classes[i] !== remove) {
                        newClassName += classes[i] + " ";
                    }
                }

                oldelement.className = newClassName;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] SMIL 개체의 대상 엘리먼트의 하이라이트 제거
            // [ Param  ]
            this.SMILEndPage = function () {
                if (oldSelectElementID != null) {
                    this.removeClass(oldSelectElementID, "smilhighlight");
                    oldSelectElementID = null;
                }
            }
        };


        ///////////////////////////////////////////////////////////////////////
        // 이벤트 핸들러, pc용 debug or Android용 셀렉션을 위한 이벤트 핸들링
        ///////////////////////////////////////////////////////////////////////
        this.EventHandler = new function __EventHandler() {
            var g_isDragStatus = WM_DRAG_INIT;
            var g_strDragStartCfi = '';
            var g_strDragEndCfi = '';
            var g_strDragText = '';
            var g_strDragColor = '#0000FF';
            var g_strDragStartXPath = '';
            var g_strDragEndXPath = '';
            var g_strDragStartXPathOffset = 0;
            var g_strDragEndXPathOffset = 0;
            var g_strDragRegionJson = '';

            var WM_DRAG_INIT = 0;
            var WM_DRAG_START = 1;
            var WM_DRAG_END = 2;

            var g_last_draw_x;
            var g_last_draw_y;
            var g_last_evt_x;
            var g_last_evt_y;
            var g_bSelect = false;
            var g_nTimerCount = 0;
            var g_arrDragRect = new Array();
            var g_bSelectFront = false;

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 div 색상변경
            // [ Param  ]
            //   - (string) color: 컬러
            // [ Return ]  void
            this.SetDrawSelectionColor = function (strColor) {
                g_strDragColor = strColor;
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 div 그리기
            // [ Param  ]
            //   - (document) doc: document object
            //   - (number) x: 화면상의 x point ( 범위는 보여지는 화면의 가로를 넘지 못함)
            //   - (number) y: 화면상의 y point ( 범위는 보여지는 화면의 세로를 넘지 못함)
            // [ Return ]  void
            this.OnDrawTimer = function (wordBoundary) {
                var isDebug = true;
                var strCfi_Temp = null;
                var res = null;
                var rtnVal = '';
                var id = '';
                var curPage = 0;
                g_nTimerCount++;
                var isWordBoundary;
                try {
                    if (g_bSelect == false) {
                        return;
                    }

                    if (g_nTimerCount > 1) {
                        return;
                    }

                    while (true) {
                        if (g_last_evt_x == g_last_draw_x && g_last_evt_y == g_last_draw_y) {
                            // break;
                        }

                        g_last_draw_x = g_last_evt_x;
                        g_last_draw_y = g_last_evt_y;

                        // RunaEngine.HtmlCtrl.Selection.HideSelection();
                        // if(g_strDragStartCfi == '' && g_strDragEndCfi == '') {
                        //     isWordBoundary = true;
                        // }
                        // else {
                        //     isWordBoundary = false;
                        // }

                        isWordBoundary = wordBoundary;
                        strCfi_Temp = RunaEngine.HtmlCtrl.DPControl._cfiAt(document, g_last_draw_x, g_last_draw_y, isWordBoundary);
                        // RunaEngine.HtmlCtrl.Selection.ShowSelection();
                        if (strCfi_Temp == null) {
                            break;
                        }

                        if (g_strDragStartCfi == '') {
                            g_strDragStartCfi = strCfi_Temp[0];
                            g_strDragStartXPath = strCfi_Temp[1];
                            g_strDragStartXPathOffset = strCfi_Temp[2];

                            if (g_strDragEndCfi == '') {
                                g_strDragEndCfi = strCfi_Temp[3];
                                g_strDragEndXPath = strCfi_Temp[4];
                                g_strDragEndXPathOffset = strCfi_Temp[5];
                            }

                            if (RunaEngine.DeviceInfo.IsAndroid() || RunaEngine.DeviceInfo.isPC()) {
                                if (g_bSelectFront && g_strDragEndCfi != '') {
                                    if (g_strDragStartCfi == g_strDragEndCfi) {
                                        break;
                                    }

                                    res = RunaEngine.HtmlCtrl.Selection.DrawSelection(g_strDragStartCfi, g_strDragEndCfi, g_strDragColor);
                                    g_arrDragRect[0] = res[0]['rleft'];
                                    g_arrDragRect[1] = res[0]['top'];
                                    g_arrDragRect[2] = res[0]['width'];
                                    g_arrDragRect[3] = res[0]['height'];
                                    g_strDragText = res['SelectionText'];

                                    g_strDragRegionJson = RunaEngine.HtmlCtrl.EventHandler.getDrawSelectionJson(id, res, g_strDragColor, g_strDragText);

                                    if (RunaEngine.DeviceInfo.IsAndroid()) {
                                        window.android.CreateSelectionResult(g_strDragRegionJson);
                                    }

                                    break;
                                }
                                else {
                                    if (g_strDragStartCfi == g_strDragEndCfi) {
                                        RunaEngine.log("cfi same so break2222;");
                                        break;
                                    }

                                    res = RunaEngine.HtmlCtrl.Selection.DrawSelection(g_strDragStartCfi, g_strDragEndCfi, g_strDragColor);
                                    g_arrDragRect[0] = res[0]['rleft'];
                                    g_arrDragRect[1] = res[0]['top'];
                                    g_arrDragRect[2] = res[0]['width'];
                                    g_arrDragRect[3] = res[0]['height'];
                                    g_strDragText = res['SelectionText'];

                                    g_strDragRegionJson = RunaEngine.HtmlCtrl.EventHandler.getDrawSelectionJson(id, res, g_strDragColor, g_strDragText);

                                    if (RunaEngine.DeviceInfo.IsAndroid()) {
                                        window.android.CreateSelectionResult(g_strDragRegionJson);
                                    }

                                    break;
                                }
                            }
                            break;
                        }
                        else {
                            if (g_strDragStartCfi == strCfi_Temp[0] && g_strDragEndCfi == strCfi_Temp[3]) {
                                RunaEngine.log("cfi same so break  444");
                                // break;
                            }

                            /**
                             * redmine refs #14501
                             * 셀렉션 영역 오류 수정
                             * by junghoon.kim
                             */
                            g_strDragEndCfi = strCfi_Temp[3];
                            g_strDragEndXPath = strCfi_Temp[4];
                            g_strDragEndXPathOffset = strCfi_Temp[5];

                            if (g_strDragStartCfi == g_strDragEndCfi) {
                                RunaEngine.log("cfi same so break 5555");
                                break;
                            }

                            res = RunaEngine.HtmlCtrl.Selection.DrawSelection(g_strDragStartCfi, g_strDragEndCfi, g_strDragColor);
                            g_arrDragRect[0] = res[0]['rleft'];
                            g_arrDragRect[1] = res[0]['top'];
                            g_arrDragRect[2] = res[0]['width'];
                            g_arrDragRect[3] = res[0]['height'];
                            g_strDragText = res['SelectionText'];

                            g_strDragRegionJson = RunaEngine.HtmlCtrl.EventHandler.getDrawSelectionJson(id, res, g_strDragColor, g_strDragText);

                            if (RunaEngine.DeviceInfo.IsAndroid()) {
                                window.android.CreateSelectionResult(g_strDragRegionJson);
                            }
                            break;
                        }
                        break;
                    }

                    if (g_last_evt_x == g_last_draw_x && g_last_evt_y == g_last_draw_y && g_isDragStatus != WM_DRAG_START) {
                        // draw stop
                        //window.setTimeout( function() {RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer();},0);
                    }
                    else if (g_isDragStatus == WM_DRAG_START && g_nTimerCount == 1) {
                        //window.setTimeout( function() {RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer();},500);
                    }
                }
                catch (ex) {
                    //RunaEngine.log('[★Exception★][EventHandler.OnDrawTimer] ' + (ex.number & 0xFFFF));
                    //RunaEngine.log('[★Exception★][EventHandler.OnDrawTimer] ' + ex);
                }
                finally {
                    isDebug = null;
                    strCfi_Temp = null;
                    rtnDrawSelect = null;

                    g_nTimerCount--;

                }
            }

            /**
             * 하이라이트, 셀렉션 정보
             */
            this.getDrawSelectionJson = function (id, res, color, sentence) {
                var pageWidth = RunaEngine.HtmlCtrl.getPageWidth();
                //sentence = sentence.replace(/"/g, "\'");
                // ios - jdm
                if (RunaEngine.DeviceInfo.IsIOS) {
                    sentence = sentence.replace(/\n/g, "\\r\\n");
                    sentence = sentence.replace(/\s/gi, "");
                }
                var rtnVal = "{\"id\" : \"" + id + "\", \"res\" : \"" + res['res'] + "\", \"color\" : \"" + color + "\", \"sentence\" : \"" + sentence + "\", \"pageWidth\": " + pageWidth;
                rtnVal += ", \"positions\" : [";

                for (var j = 0; j < res.length; j++) {
                    if (j > 0) {
                        rtnVal += ',';
                    }

                    var left = parseInt(res[j]['left'], 10);
                    var top = parseInt(res[j]['top'], 10);
                    var pageNo = parseInt(left / pageWidth) + 1;

                    if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                        pageNo = RunaEngine.HtmlCtrl.GetPageNumberFromTopPos_ForVScrollMode(top);
                    }

                    rtnVal += "{\"left\" : \"" + res[j]['left'] + "\", \"top\" : \"" + res[j]['top'] + "\", \"width\" : \"" + res[j]['width'] + "\", \"height\" : \"" + res[j]['height'] + "\", \"pageNo\" : \"" + pageNo + "\"}";
                }

                rtnVal += "]}";

                return rtnVal;
            }


            /**
             * redmine refs #16619
             * 단어 단위 여부를 판단하도록 수정
             * by junghoon.kim
             */
            this.OnSelectionStart = function (x, y, wordBoundary) {
                g_bSelect = true;
                g_isDragStatus = WM_DRAG_START;
                g_strDragStartCfi = '';
                g_strDragEndCfi = '';
                g_strDragText = '';
                g_strDragRegionJson = '';

                if (parseInt(x) < 0 || parseInt(y) < 0) {
                    return true;
                }
                g_last_evt_x = parseInt(x);
                g_last_evt_y = parseInt(y);

                RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer(wordBoundary);
            }

            this.OnSelectionReStartFront = function (x, y, wordBoundary) {
                g_bSelect = true;
                g_isDragStatus = WM_DRAG_START;
                g_strDragStartCfi = '';
                g_strDragText = '';
                g_strDragRegionJson = '';
                g_bSelectFront = true;

                if (parseInt(x) < 0 || parseInt(y) < 0) {
                    return true;
                }
                g_last_evt_x = parseInt(x);
                g_last_evt_y = parseInt(y);

                RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer(wordBoundary);
            }

            this.OnSelectionReStartBack = function (x, y, wordBoundary) {
                g_bSelect = true;
                g_isDragStatus = WM_DRAG_START;
                g_strDragEndCfi = '';
                g_strDragText = '';
                g_strDragRegionJson = '';

                if (parseInt(x) < 0 || parseInt(y) < 0) {
                    return true;
                }
                g_last_evt_x = parseInt(x);
                g_last_evt_y = parseInt(y);

                RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer();
            }

            this.OnSelectionMoveFront = function (x, y, wordBoundary) {
                if (parseInt(x) < 0 || parseInt(y) < 0) {
                    return true;
                }

                if (g_last_evt_x == parseInt(x) && g_last_evt_y == parseInt(y)) {
                    return true;
                }
                g_last_evt_x = parseInt(x);
                g_last_evt_y = parseInt(y);

                if (g_nTimerCount != 0 || g_bSelect == false || g_isDragStatus != WM_DRAG_START) {
                    return false;
                }

                g_strDragStartCfi = '';
                RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer(wordBoundary);
                // RunaEngine.HtmlCtrl.Selection.SelectionDivInfo();
                return true;
            }

            this.OnSelectionMove = function (x, y, wordBoundary) {
                if (parseInt(x) < 0 || parseInt(y) < 0) {
                    return true;
                }

                if (g_last_evt_x == parseInt(x) && g_last_evt_y == parseInt(y)) {
                    // return true;
                }
                g_last_evt_x = parseInt(x);
                g_last_evt_y = parseInt(y);

                if (g_nTimerCount != 0 || g_bSelect == false || g_isDragStatus != WM_DRAG_START) {
                    return false;
                }

                RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer(wordBoundary);
                // RunaEngine.HtmlCtrl.Selection.SelectionDivInfo();
                return true;
            }

            this.OnSelectionEnd = function (x, y, wordBoundary) {
                g_bSelect = false;
                g_bSelectFront = false;
                g_isDragStatus = WM_DRAG_END;

                if (parseInt(x) < 0 || parseInt(y) < 0) {
                    return true;
                }

                g_last_evt_x = parseInt(x);
                g_last_evt_y = parseInt(y);

                var rtnCompare = RunaEngine.HtmlCtrl.Highlight._CheckCfiCompare(g_strDragStartCfi, g_strDragEndCfi);
                g_strDragStartCfi = rtnCompare['Big'];
                g_strDragEndCfi = rtnCompare['Small'];

                RunaEngine.sublog2("x = " + x + " y = " + y + " g_strDragStartCfi = " + g_strDragStartCfi + " g_strDragEndCfi = " + g_strDragEndCfi);
                RunaEngine.HtmlCtrl.EventHandler.OnDrawTimer(wordBoundary);
            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 시작 CFI값 리턴
            // [ Param  ] none
            // [ Return ] (string) : 시작 CFI값
            this.getStartCfi = function () { return g_strDragStartCfi; }

            ///////////////////////////////////////////////////////////////////////
            // @ ANDROID ONLY
            // [ Desc   ] 셀렉션 끝 CFI값 리턴
            // [ Param  ] none
            // [ Return ] (string) : 끝 CFI값
            this.getEndCfi = function () { return g_strDragEndCfi; }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 시작 CFI값 리턴
            // [ Param  ] none
            // [ Return ] (string) : 시작 XPath값
            this.getStartXPath = function () { return g_strDragStartXPath; }

            ///////////////////////////////////////////////////////////////////////
            // @ ANDROID ONLY
            // [ Desc   ] 셀렉션 끝 CFI값 리턴
            // [ Param  ] none
            // [ Return ] (string) : 끝 XPath값
            this.getEndXPath = function () { return g_strDragEndXPath; }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 셀렉션 시작 CFI값 리턴
            // [ Param  ] none
            // [ Return ] (string) : 시작 XPath Offset값
            this.getStartXPathOffset = function () { return g_strDragStartXPathOffset; }

            ///////////////////////////////////////////////////////////////////////
            // @ ANDROID ONLY
            // [ Desc   ] 셀렉션 끝 CFI값 리턴
            // [ Param  ] none
            // [ Return ] (string) : 끝 XPath Offset값
            this.getEndXPathOffset = function () { return g_strDragEndXPathOffset; }

            ///////////////////////////////////////////////////////////////////////
            // @ ANDROID ONLY
            // [ Desc   ] 셀렉션 선택된
            // [ Param  ] none
            // [ Return ] (string) : 선택된 스트링값
            this.getSelectionText = function () { return g_strDragText; }
            ///////////////////////////////////////////////////////////////////////
            // @ ANDROID ONLY
            // [ Desc   ] 셀렉션 선택된
            // [ Param  ] none
            // [ Return ] (array) : 선택된 텍스트의 좌표값
            this.getSelectionRect = function () { return g_arrDragRect; }
            this.getSelectionRectJson = function () { return g_strDragRegionJson; }
        };

        /**
         * 
         */
        this.Finder = new function __AVTTSEngine() {
            this.scan = function(startCfi,endCfi){
                const isStrEmpty = function(str){ 
                    if (typeof str === 'string') {
                        if ( str.trim().length === 0){
                            return true
                        } else {
                            return false
                        }
                    } else {
                        return false
                    }
                }


                
                if (isStrEmpty(startCfi)){
                    return Promise.reject('startCfimust not be empty')
                }
                if (isStrEmpty(endCfi)){
                    return Promise.reject('endCfi must not be empty')
                }
                
                let start = RunaEngine.HtmlCtrl.DPControl._decodeCFI(document, startCfi);
                if (!start){
                    return Promise.reject('fail to find endCfi decode')
                }

                let end = RunaEngine.HtmlCtrl.DPControl._decodeCFI(document, endCfi);
                if (!end){
                    return Promise.reject('fail to find endCfi decode')
                }
                if (!m_divContentLayout){
                    return Promise.reject('element m_divContentLayout is \"'+m_divContentLayout+'\"')
                }
                return Finder.checkEndPoint(m_divContentLayout,start.node,start.offset,end.node,end.offset)
            }
        }
        
        ///////////////////////////////////////////////////////////////////
        // 웹에서 이벤트를 주는 경우의 Interface들 모음
        ///////////////////////////////////////////////////////////////////
        this.AVTTSEngine = new function __AVTTSEngine() {
            var mSentenceList = null;
            var mParagraphMap = null;
            var mTableMap = null;
            var mIdMap = null;
            var hlCreateId = 'bdb_tts_hl';
            var hlAvdTTSColor = '#345345';
            var mInitPage = false;
            var mSelectIndex = -1;
            var mSelectStartCfi = null;
            var mSelectEndCfi = null;
            var mSelectInfo = null;
            var mParagraphIdx = 0;

            var AVTTSTagType = Object.freeze({ NONE: 0, HEADLINE: 1, FIGCAPTION: 2, ASIDE: 3, TABLECAPTION: 4 });
            var AVTTSTableTagType = Object.freeze({ NONE: 0, TH: 1, TD: 2 });

            function is_all_ws(nod) {
                // Use ECMA-262 Edition 3 String and RegExp features
                return !(/[^\t\n\r ]/.test(nod.textContent));
            }

            function is_ignorable(nod) {
                return (nod.nodeType == 8) || // A comment node
                ((nod.nodeType == 3) && is_all_ws(nod)); // a text node, all ws
            }

            function AVD_Node_Before(sib) {
                while ((sib = sib.previousSibling)) {
                    if (!is_ignorable(sib)) return sib;
                }
                return null;
            }

            function AVD_Node_After(sib) {
                while ((sib = sib.nextSibling)) {
                    if (!is_ignorable(sib)) return sib;
                }
                return null;
            }

            function AVDEleCheck(node) {
                return NodeFilter.FILTER_ACCEPT;
            }

            function AVDRemoveWhiteSpace(sentence) {
                var tmpSentence = sentence;
                tmpSentence = tmpSentence.replace(/^\s/gm, '');
                tmpSentence = tmpSentence.replace(/\r\n$/g, '');
                tmpSentence = tmpSentence.replace(/(^\s*)|(\s*$)/g, '');

                return tmpSentence;
            }

            function AVDRemoveLFCR(sentence) {
                var tmpSentence = sentence;
                tmpSentence = tmpSentence.replace(/\r/g, '');
                tmpSentence = tmpSentence.replace(/\n/g, '');
                return tmpSentence;
            }

            function AVDSentenceCompare(sentence) {
                var val = { res: 0, start: -1, end: -1, s: '' };
                var strIdx = 0, nodeLen = 0, difflen = 0;
                var after = null;
                var before = null;

                before = decodeHTMLEntities(sentence);
                nodeLen = before.length;
                after = AVDRemoveWhiteSpace(before);

                val.start = before.indexOf(after);
                val.end = before.lastIndexOf(after);

                if (val.start == -1) {
                    val.start = 0;
                    val.end = 0;
                    while (true) {

                        if (strIdx == before.length) break;

                        if (before.substring(strIdx, strIdx + 1) != after.substring(strIdx + difflen, strIdx + difflen + 1))
                            difflen++;

                        strIdx++;
                    }

                    if (difflen > 0) {
                        RunaEngine.sublog2('AVDSentenceCompare : ' + difflen);
                    }

                }

                if (val.start == val.end) val.end = val.start + after.length;
                val.s = after;

                return val;
            }

            function AVDGetAttrValue(node, name) {
                var val = null;
                var attrs = null;
                var len = 0;
                var attr = null;

                try {
                    attrs = node.attributes;

                    if (attrs != null) {
                        len = attrs.length;

                        if (len > 0) {
                            while (len--) {
                                attr = attrs[len];

                                if (attr.name == name) {
                                    val = attr.value;
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (ex) {
                    RunaEngine.log(ex);
                }
                finally {
                    attrs = null;
                    len = 0;
                    attr = null;
                }

                return val;
            }

            function decodeHTMLEntities(text) {
                var entities = [
                                ['amp', '&'],
                                ['apos', '\''],
                                ['#x27', '\''],
                                ['#x2F', '/'],
                                ['#39', '\''],
                                ['#47', '/'],
                                ['lt', '<'],
                                ['gt', '>'],
                                ['nbsp', ' '],
                                ['quot', '"']
                                ];

                for (var i = 0, max = entities.length; i < max; ++i)
                    text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);

                return text;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 생성된 문자열 리스트
            // [ Return ] JSON 문자열
            this.GetAVDTTSList = function () {

                if (mSentenceList != null) {
                    result = JSON.stringify(mSentenceList);
                    // ios - jdm
                    // if (RunaEngine.DeviceInfo.IsIOS) {
                    //     RunaEngine.log('jdm GetAVDTTSList result = [' + result + ']');
                    // }
                    return result;
                }

                return '';
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파라메터 정보의 텍스트를 구해준다.
            // [ Param  ] idx : 문장번호
            // [ Param  ] start : 시작위치
            // [ Param  ] end : 종료위치
            // [ Param  ] text : 문자열
            // [ Param  ] page : 페이지 번호
            // [ Return ] 없음
            this.AddMultiSelectInfo = function (idx, start, end, text, page) {
                if (mSelectInfo == null)
                    mSelectInfo = new Array();

                var val = { index: idx, startCfi: start, endCfi: end, ch: text, page: page, hlId: hlCreateId + '_' + idx };
                mSelectInfo.push(val);

                mSelectInfo.sort(function (a, b) { if (parseInt(a['index']) > parseInt(b['index'])) return 1; else if (parseInt(a['index']) < parseInt(b['index'])) return -1; else 0; })
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 다중 선택 정보의 인덱스에 해당하는 정보를 삭제 하이라트 포함
            // [ Param  ] idx : 다중 선택 인덱스
            // [ Return ] 없음
            this.RemoveMultiSelectInfo = function (idx) {
                var a = mSelectInfo.splice(idx, 1);
                a = null;
                mSelectInfo.sort(function (a, b) { if (parseInt(a['index']) > parseInt(b['index'])) return 1; else if (parseInt(a['index']) < parseInt(b['index'])) return -1; else 0; })
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ]다중 선택 정보를 삭제한다. 이때 하이라이트도 같이 삭제
            // [ Return ] 없음
            this.ClearMultiSelectInfo = function () {

                if (mSelectInfo != null) {
                    while (mSelectInfo.length > 0) {
                        a = mSelectInfo.splice(0, 1);
                        RunaEngine.HtmlCtrl.Highlight.Remove(a['hlId']);

                        a = null;
                    }
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 다중 선택 정보의 하이라이트를 삭제
            // [ Return ] 없음
            this.RemoveMultiSelectInfoHighlight = function () {

                for (var i = 0; i < mSelectInfo.length; i++) {
                    a = mSelectInfo[i];
                    RunaEngine.HtmlCtrl.Highlight.Remove(a['hlId']);
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 다중 선택 정보의 문자열을 리턴한다.
            // [ Return ] string 형태로 결과를 리턴한다.
            this.GetMultiSectionText = function () {
                var str = '';

                for (var i = 0; i < mSelectInfo.length; i++) {
                    a = mSelectInfo[i];

                    if (i > 0) str += ' ';
                    str += a.ch;
                }

                return str;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 다중 선택 정보의 인덱스로 문장 번호의 데이터를 리턴한다.
            // [ Param  ] direction : 방향 (true : next, false : prev)
            // [ Return ] boolean 형태로 결과를 리턴한다. (true : add, false : remove)
            this.GetMultiSelectActionType = function (direction) {
                var defaultIdx = -1;
                var endCfiOffset = -1;
                var startCfiOffset = -1;
                var baseCfiOffset = -1;

                // 기준 문장번호 찾기
                for (var i = 0; i < mSelectInfo.length; i++) {
                    a = mSelectInfo[i];

                    if (mSelectIndex == a.index) {
                        defaultIdx = i;
                        break;
                    }
                }

                //direction true : next
                if (direction) {
                    if (mSelectStartCfi.indexOf(':') > 0) {
                        baseCfiOffset = parseInt(mSelectStartCfi.split(':')[1]);
                    }
                    else {
                        baseCfiOffset = 0;
                    }

                    if (defaultIdx == 0) {
                        if (mSelectInfo.length == 1) {
                            if (mSelectInfo[defaultIdx].startCfi.indexOf(':') > 0) {
                                startCfiOffset = parseInt(mSelectInfo[defaultIdx].startCfi.split(':')[1]);
                            }
                            else {
                                startCfiOffset = 0;
                            }

                            if (baseCfiOffset == startCfiOffset) {
                                //add
                                return true;
                            }
                            else {
                                //remove
                                return false;
                            }
                        }
                        else {
                            //add
                            return true;
                        }
                    }
                    else {
                        // remove
                        return false;
                    }
                }
                else {
                    //direction false : prev
                    if (mSelectEndCfi.indexOf(':') > 0) {
                        baseCfiOffset = parseInt(mSelectEndCfi.split(':')[1]);
                    }
                    else {
                        baseCfiOffset = 0;
                    }

                    if (defaultIdx == (mSelectInfo.length - 1)) {
                        if (mSelectInfo.length == 1) {
                            if (mSelectInfo[defaultIdx].endCfi.indexOf(':') > 0) {
                                endCfiOffset = parseInt(mSelectInfo[defaultIdx].endCfi.split(':')[1]);
                            }
                            else {
                                endCfiOffset = 0;
                            }

                            if (baseCfiOffset == endCfiOffset) {
                                //add
                                return true;
                            }
                            else {
                                //remove
                                return false;
                            }
                        }
                        else {
                            //add
                            return true;
                        }

                        return false;
                    }
                    else {
                        //remove
                        return false;
                    }
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 다중 선택 정보의 인덱스로 문장 번호의 데이터를 리턴한다.
            // [ Param  ] idx : 다중 선택 인덱스
            // [ Return ] Array 로 결과를 리턴한다.
            this.GetMultSelectionInfoByIndex = function (idx) {
                var val = { idx: -1, base: null, baseStart: -1, baseEnd: -1, sentence: null, sentenceStart: -1, sentenceEnd: -1, page: -1 };
                var index = -1;
                var startCfi = null;
                var endCfi = null;
                var baseStartCfi = null;
                var baseEndCfi = null;
                var sentenceCfi = null;
                var baseStartCfiOffset = -1;
                var baseEndCfiOffset = -1;
                var startCfiOffset = -1;
                var endCfiOffset = -1;

                index = mSelectInfo[idx].index;
                startCfi = mSelectInfo[idx].startCfi;
                endCfi = mSelectInfo[idx].endCfi;

                if (startCfi.indexOf(':') > 0) {
                    baseStartCfi = startCfi.split(':')[0]
                    baseStartCfiOffset = parseInt(startCfi.split(':')[1]);
                }
                else {
                    baseStartCfi = startCfi;
                    baseStartCfiOffset = 0;
                }

                if (endCfi.indexOf(':') > 0) {
                    baseEndCfi = endCfi.split(':')[0]
                    baseEndCfiOffset = parseInt(endCfi.split(':')[1]);
                }
                else {
                    baseEndCfi = endCfi;
                    baseEndCfiOffset = 0;
                }

                val.idx = index;
                val.base = baseStartCfi;
                val.baseStart = baseStartCfiOffset;
                val.baseEnd = baseEndCfiOffset;

                sentence = mSentenceList[index];

                if (sentence != null) {

                    if (sentence.start.indexOf(':') > 0) {
                        sentenceCfi = sentence.start.split(':')[0]
                        startCfiOffset = parseInt(sentence.start.split(':')[1]);
                    }
                    else {
                        sentenceCfi = sentence.start;
                        startCfiOffset = 0;
                    }

                    if (sentence.end.indexOf(':') > 0) {
                        //sentenceCfi = sentence.end.split(':')[0]
                        endCfiOffset = parseInt(sentence.end.split(':')[1]);
                    }
                    else {
                        endCfiOffset = 0;
                    }

                    val.sentence = sentenceCfi;
                    val.sentenceStart = startCfiOffset;
                    val.sentenceEnd = endCfiOffset;
                    val.page = sentence.sp;
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파라메터 정보의 텍스트를 구해준다.
            // [ Param  ] idx : 문장번호
            // [ Param  ] start : 시작위치
            // [ Param  ] end : 종료위치
            // [ Return ] Array 로 결과를 리턴한다.
            this.GetSelectionTextByCfi = function (idx, start, end) {
                var val = { res: 0, page: -1, ch: '' };
                var sentence = null;
                var sentenceCfi = null;
                var endCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var nPos = -1;

                sentence = mSentenceList[idx];

                if (sentence != null) {

                    if (sentence.start.indexOf(':') > 0) {
                        sentenceCfi = sentence.start.split(':')[0]
                        sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                    }
                    else {
                        sentenceCfi = sentence.start;
                        sentenceStartCfiOffset = 0;
                    }

                    if (end.indexOf(':') > 0) {
                        //sentenceCfi = sentence.end.split(':')[0]
                        endCfiOffset = parseInt(end.split(':')[1]);
                    }
                    else {
                        endCfiOffset = 0;
                    }

                    if (start.indexOf(':') > 0) {
                        baseCfi = start.split(':')[0]
                        baseCfiOffset = parseInt(start.split(':')[1]);
                    }
                    else {
                        baseCfi = start;
                        baseCfiOffset = 0;
                    }

                    nPos = baseCfiOffset == -1 ? 0 : baseCfiOffset - sentenceStartCfiOffset;

                    ch = sentence.sentence.substring(nPos, nPos + (endCfiOffset - baseCfiOffset));

                    val['page'] = sentence.sp;
                    val['ch'] = ch;
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파라메터 정보의 시작 위치의 첫 글자를 구해준다.
            // [ Param  ] idx : 문장번호
            // [ Param  ] start : 시작위치
            // [ Param  ] end : 종료위치
            // [ Return ] Array 로 결과를 리턴한다.
            this.GetSelectionFirstCharByCfi = function (idx, start, end) {
                var val = { res: 0, page: -1, start: '', end: '', ch: '' };
                var sentence = null;
                var sentenceCfi = null;
                var endCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var nPos = -1;

                sentence = mSentenceList[idx];

                if (sentence != null) {

                    if (sentence.start.indexOf(':') > 0) {
                        sentenceCfi = sentence.start.split(':')[0]
                        sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                    }
                    else {
                        sentenceCfi = sentence.start;
                        sentenceStartCfiOffset = 0;
                    }

                    if (end.indexOf(':') > 0) {
                        //sentenceCfi = sentence.end.split(':')[0]
                        endCfiOffset = parseInt(end.split(':')[1]);
                    }
                    else {
                        endCfiOffset = 0;
                    }

                    if (start.indexOf(':') > 0) {
                        baseCfi = start.split(':')[0]
                        baseCfiOffset = parseInt(start.split(':')[1]);
                    }
                    else {
                        baseCfi = start;
                        baseCfiOffset = 0;
                    }

                    if (baseCfiOffset < endCfiOffset) {
                        nPos = baseCfiOffset == -1 ? 0 : baseCfiOffset - sentenceStartCfiOffset;
                    }
                    else {
                        nPos = sentence.sentence - 2;
                    }

                    ch = sentence.sentence.substring(nPos, nPos + 1);

                    val['start'] = sentenceCfi + ":" + (sentenceStartCfiOffset + nPos);
                    val['end'] = sentenceCfi + ":" + (sentenceStartCfiOffset + nPos + 1);
                    val['page'] = sentence.sp;
                    val['ch'] = ch;
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파일의 글자 또는 문장을 추가하여 선택 하도록 한다.
            //            기준의 처음 선택되어 있는 영역을 기준으로 한다.
            // [ Param  ] type : 0(글자), 2(문장)
            // [ Param  ] movetype : 1(이전), 2(다음)
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(해당 문장이 없음)
            this.Navigate = function (type, movetype) {
                var sentence = null;
                var val = { res: 0, type: -1, ch: '', indexs: null };
                var sIndex = -1;
                var sEndCfi = '';
                var sStartCfi = '';
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var baseCfi = null;
                var baseCfiOffset = -1;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        val['res'] = -2;
                    }
                    else {
                        if (mSelectInfo == null || mSelectInfo.length == 0) {
                            //멀티 선택을 하지 않은 상태면 해당 페이지의 첫문장의 첫글자를 선택한다.
                            if (mSelectIndex == -1) {
                                //error
                                val['res'] = -3;

                                result = JSON.stringify(val);

                                return result;
                            }
                            else {
                                resultSel = this.GetSelectionFirstCharByCfi(mSelectIndex, mSelectStartCfi, mSelectEndCfi);
                                this.AddMultiSelectInfo(mSelectIndex, resultSel['start'], resultSel['end'], resultSel['ch'], parseInt(resultSel['page']));

                                mSelectEndCfi = resultSel['end'];

                                val['res'] = 0;
                                val['type'] = true;
                                val['ch'] = resultSel['ch'];
                                val['indexs'] = mSelectInfo;

                                result = JSON.stringify(val);

                                return result;
                            }
                        }

                        var action = this.GetMultiSelectActionType(movetype == 1 ? false : true);

                        if (action) {
                            //add
                            if (type == 0) {
                                // 글자 추가 , 선택 정보의 첫 또는 마지막 글자의 cfi를 준다.
                                var AddInfo = null;
                                var preSelInfo = null;

                                if (movetype == 1) {
                                    //prev, add
                                    preSelInfo = this.GetMultSelectionInfoByIndex(0);

                                    sIndex = preSelInfo.idx;
                                    sStartCfi = preSelInfo.base + ":" + preSelInfo.baseStart;
                                    sEndCfi = preSelInfo.base + ":" + (preSelInfo.baseStart + 1);

                                    AddInfo = this.PrevCharPreview(sIndex, sStartCfi, sEndCfi);
                                }
                                else if (movetype == 2) {
                                    //next, add
                                    preSelInfo = this.GetMultSelectionInfoByIndex(mSelectInfo.length - 1);

                                    sIndex = preSelInfo.idx;
                                    sEndCfi = preSelInfo.base + ":" + preSelInfo.baseEnd;
                                    sStartCfi = preSelInfo.base + ":" + (preSelInfo.baseEnd - 1);

                                    AddInfo = this.NextCharPreview(sIndex, sStartCfi, sEndCfi);
                                }

                                RunaEngine.sublog2('Navigate Add Char = [' + sIndex + ', ' + sStartCfi + ', ' + sEndCfi + '], [' + JSON.stringify(AddInfo) + ']');

                                if (parseInt(AddInfo['res']) == 0) {
                                    //미리 글자를 가져와서 같은 페이지인지 아닌지 같은 문장인지 아닌지 체크한다
                                    if (parseInt(AddInfo['index']) == sIndex) {
                                        //문장 번호가 같으면 시작 위치만 변경해서 준다.
                                        if (movetype == 1) {
                                            mSelectInfo[0].startCfi = AddInfo['startCfi'];
                                            resultSel = this.GetSelectionTextByCfi(mSelectInfo[0].index, mSelectInfo[0].startCfi, mSelectInfo[0].endCfi);
                                            mSelectInfo[0].ch = resultSel.ch;
                                        }
                                        else if (movetype == 2) {
                                            mSelectInfo[mSelectInfo.length - 1].endCfi = AddInfo['endCfi'];
                                            resultSel = this.GetSelectionTextByCfi(mSelectInfo[mSelectInfo.length - 1].index, mSelectInfo[mSelectInfo.length - 1].startCfi, mSelectInfo[mSelectInfo.length - 1].endCfi);
                                            mSelectInfo[mSelectInfo.length - 1].ch = resultSel.ch;
                                        }

                                        val['res'] = 0;
                                        val['type'] = action;
                                        val['ch'] = AddInfo['ch'];
                                        val['indexs'] = mSelectInfo;
                                    }
                                    else {
                                        //문장 번호가 다르면 페이지도 확인해서 같으면 추가.
                                        if (parseInt(AddInfo['page']) == preSelInfo.page) {
                                            this.AddMultiSelectInfo(parseInt(AddInfo['index']), AddInfo['startCfi'], AddInfo['endCfi'], AddInfo['ch'], parseInt(AddInfo['page']));

                                            val['res'] = 0;
                                            val['type'] = action;
                                            val['ch'] = AddInfo['ch'];
                                            val['indexs'] = mSelectInfo;
                                        }
                                        else {
                                            //다르면 더 이상 추가 하지 않는다.
                                            RunaEngine.sublog2('Navigate 1 = [ 글자 추가 선택 페이지 다름');
                                            val['res'] = -4;
                                        }
                                    }
                                }
                                else {
                                    // 오류
                                    RunaEngine.sublog2('Navigate 22 = [ CharPreview 오류');
                                    val['res'] = -5;
                                }
                            }
                            else if (type == 2) {
                                // 문장 추가 , 선택 정보의 첫 또는 마지막 글자의 cfi를 준다.
                                var AddInfo = null;
                                var preSelInfo = null;
                                var preNotSelSentence = null;
                                var chkSentenceCfi = null;
                                var isFirstLastinSentence = false;
                                var addTxt = '';

                                if (movetype == 1) {
                                    //prev, add
                                    preSelInfo = this.GetMultSelectionInfoByIndex(0);

                                    sIndex = preSelInfo.idx;
                                    sStartCfi = preSelInfo.base + ":" + preSelInfo.baseStart;
                                    sEndCfi = preSelInfo.base + ":" + (preSelInfo.baseStart + 1);

                                    chkSentenceCfi = preSelInfo.sentence + ":" + preSelInfo.sentenceStart;

                                    if (chkSentenceCfi != sStartCfi) {
                                        isFirstLastinSentence = true;
                                        mSelectInfo[0].startCfi = chkSentenceCfi;

                                        preNotSelSentence = this.GetSelectionTextByCfi(sIndex, chkSentenceCfi, sStartCfi);

                                        addTxt = preNotSelSentence.ch;
                                    }
                                    else {
                                        AddInfo = this.PrevSentencePreview(sIndex, sStartCfi, sEndCfi);
                                    }
                                }
                                else if (movetype == 2) {
                                    //next, add
                                    preSelInfo = this.GetMultSelectionInfoByIndex(mSelectInfo.length - 1);

                                    sIndex = preSelInfo.idx;
                                    sEndCfi = preSelInfo.base + ":" + preSelInfo.baseEnd;
                                    sStartCfi = preSelInfo.base + ":" + (preSelInfo.baseEnd - 1);

                                    chkSentenceCfi = preSelInfo.sentence + ":" + preSelInfo.sentenceEnd;

                                    if (chkSentenceCfi != sEndCfi) {
                                        isFirstLastinSentence = true;
                                        mSelectInfo[mSelectInfo.length - 1].endCfi = chkSentenceCfi;

                                        preNotSelSentence = this.GetSelectionTextByCfi(sIndex, sEndCfi, chkSentenceCfi);

                                        addTxt = preNotSelSentence.ch;
                                    }
                                    else {
                                        AddInfo = this.NextSentencePreview(sIndex, sStartCfi, sEndCfi);
                                    }
                                }

                                RunaEngine.sublog2('Navigate Add Sentence = [' + sIndex + ', ' + sStartCfi + ', ' + sEndCfi + '], [' + JSON.stringify(AddInfo) + ']');

                                //현재 선택되어 있는 내용이 해당 문장의 처음, 마지막 인지 체크 한다.
                                if (isFirstLastinSentence) {
                                    val['res'] = 0;
                                    val['type'] = action;
                                    val['ch'] = addTxt;
                                    val['indexs'] = mSelectInfo;
                                }
                                else {
                                    if (parseInt(AddInfo['res']) == 0) {
                                        //미리 문장을 가져와서 같은 페이지인지 아닌지 같은 문장인지 아닌지 체크한다
                                        if (parseInt(AddInfo['page']) == preSelInfo.page) {
                                            //문장 페이지가 같으면 추가한다.
                                            this.AddMultiSelectInfo(parseInt(AddInfo['index']), AddInfo['startCfi'], AddInfo['endCfi'], AddInfo['ch'], parseInt(AddInfo['page']));

                                            val['res'] = 0;
                                            val['type'] = action;
                                            val['ch'] = AddInfo['ch'];
                                            val['indexs'] = mSelectInfo;
                                        }
                                        else {
                                            //다르면 더 이상 추가 하지 않는다.
                                            RunaEngine.sublog2('Navigate 1 = [ 이전 글자 선택 페이지 다름');
                                            val['res'] = -4;
                                        }
                                    }
                                    else {
                                        // 오류
                                        RunaEngine.sublog2('Navigate 22 = [ SentencePreview 오류');
                                        val['res'] = -5;
                                    }
                                }
                            }
                        }
                        else {
                            //remove
                            if (type == 0) {
                                // 글자 제거 , 선택 정보의 첫 또는 마지막 글자의 cfi를 준다.
                                var RemoveInfo = null;
                                var preSelInfo = null;
                                var removeTxt = '';

                                if (movetype == 2) {
                                    //next, remove
                                    preSelInfo = this.GetMultSelectionInfoByIndex(0);

                                    sIndex = preSelInfo.idx;
                                    sStartCfi = preSelInfo.base + ":" + preSelInfo.baseStart;
                                    sEndCfi = preSelInfo.base + ":" + (preSelInfo.baseStart + 1);

                                    RemoveInfo = this.NextCharPreview(sIndex, sStartCfi, sEndCfi);
                                }
                                else if (movetype == 1) {
                                    //prev, remove
                                    preSelInfo = this.GetMultSelectionInfoByIndex(mSelectInfo.length - 1);

                                    sIndex = preSelInfo.idx;
                                    sEndCfi = preSelInfo.base + ":" + preSelInfo.baseEnd;
                                    sStartCfi = preSelInfo.base + ":" + (preSelInfo.baseEnd - 1);

                                    RemoveInfo = this.PrevCharPreview(sIndex, sStartCfi, sEndCfi);
                                }

                                preNotSelSentence = this.GetSelectionTextByCfi(sIndex, sStartCfi, sEndCfi);

                                RunaEngine.sublog2('Navigate Remove Char = [' + sIndex + ', ' + sStartCfi + ', ' + sEndCfi + '], [' + JSON.stringify(RemoveInfo) + ']');

                                if (parseInt(RemoveInfo['res']) == 0) {
                                    //미리 글자를 가져와서 같은 페이지인지 아닌지 같은 문장인지 아닌지 체크한다
                                    if (parseInt(RemoveInfo['index']) == sIndex) {
                                        //문장 번호가 같으면 시작 위치만 변경해서 준다.
                                        if (movetype == 2) {
                                            mSelectInfo[0].startCfi = RemoveInfo['startCfi'];
                                            resultSel = this.GetSelectionTextByCfi(mSelectInfo[0].index, mSelectInfo[0].startCfi, mSelectInfo[0].endCfi);
                                            mSelectInfo[0].ch = resultSel.ch;
                                        }
                                        else if (movetype == 1) {
                                            mSelectInfo[mSelectInfo.length - 1].endCfi = RemoveInfo['endCfi'];
                                            resultSel = this.GetSelectionTextByCfi(mSelectInfo[mSelectInfo.length - 1].index, mSelectInfo[mSelectInfo.length - 1].startCfi, mSelectInfo[mSelectInfo.length - 1].endCfi);
                                            mSelectInfo[mSelectInfo.length - 1].ch = resultSel.ch;
                                        }

                                        val['res'] = 0;
                                        val['type'] = action;
                                        val['ch'] = preNotSelSentence.ch;
                                        val['indexs'] = mSelectInfo;
                                    }
                                    else {
                                        if (movetype == 2) {
                                            this.RemoveMultiSelectInfo(0);
                                        }
                                        else if (movetype == 1) {
                                            this.RemoveMultiSelectInfo(mSelectInfo.length - 1);
                                        }

                                        val['res'] = 0;
                                        val['type'] = action;
                                        val['ch'] = preNotSelSentence.ch;
                                        val['indexs'] = mSelectInfo;
                                    }
                                }
                                else {
                                    // 오류
                                    RunaEngine.sublog2('Navigate 22 = [ CharPreview 오류');
                                    val['res'] = -5;
                                }
                            }
                            else if (type == 2) {
                                // 문장 제거 , 선택 정보의 첫 또는 마지막 글자의 cfi를 준다.
                                var RemoveInfo = null;
                                var preSelInfo = null;
                                var removeTxt = '';

                                if (movetype == 2) {
                                    //next, remove
                                    preSelInfo = this.GetMultSelectionInfoByIndex(0);

                                    sIndex = preSelInfo.idx;
                                    sStartCfi = preSelInfo.base + ":" + preSelInfo.baseStart;
                                    sEndCfi = preSelInfo.base + ":" + (preSelInfo.baseStart + 1);

                                    removeTxt = mSelectInfo[0].ch;
                                    this.RemoveMultiSelectInfo(0);
                                }
                                else if (movetype == 1) {
                                    //prev, add
                                    preSelInfo = this.GetMultSelectionInfoByIndex(mSelectInfo.length - 1);

                                    sIndex = preSelInfo.idx;
                                    sEndCfi = preSelInfo.base + ":" + preSelInfo.baseEnd;
                                    sStartCfi = preSelInfo.base + ":" + (preSelInfo.baseEnd - 1);

                                    removeTxt = mSelectInfo[mSelectInfo.length - 1].ch;
                                    this.RemoveMultiSelectInfo(mSelectInfo.length - 1);
                                }

                                val['res'] = 0;
                                val['type'] = action;
                                val['ch'] = removeTxt;
                                val['indexs'] = mSelectInfo;

                                RunaEngine.sublog2('Navigate Add Sentence = [' + sIndex + ', ' + sStartCfi + ', ' + sEndCfi + '], [' + removeTxt + ']');

                            }
                        }
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파일의 처음 글자를 선택 하도록 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(해당 문장이 없음)
            this.FirstChar = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var baseCfiOffset = 0, startCfiOffset = 0;
                var baseCfi = '';
                var page = 0;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                    else {

                        this.ClearMultiSelectInfo();

                        mSelectIndex = 0;

                        while (true) {

                            if (mSelectIndex >= mSentenceList.length) {
                                mSelectIndex = -1;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (arrIgnoreType != null) {
                                var bSkip = false;
                                for (var i = 0; i < arrIgnoreType.length; i++) {
                                    if (sentence.type == arrIgnoreType[i]) {
                                        mSelectIndex++;
                                        bSkip = true;
                                        break;
                                    }
                                }

                                if (bSkip)
                                    continue;
                            }

                            if (sentence.draw == 'N') {
                                mSelectIndex++;
                                continue;
                            }
                            else {
                                break;
                            }
                        }
                    }

                    if (sentence != null) {
                        mSelectStartCfi = sentence.start;
                        mSelectEndCfi = sentence.end;

                        if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                            ch = sentence.sentence;
                            page = sentence.sp;
                        }
                        else {
                            if (sentence.sentence.length > 0) {
                                if (mSelectStartCfi.indexOf(':') > 0) {
                                    baseCfi = mSelectStartCfi.split(':')[0]
                                    startCfiOffset = parseInt(mSelectStartCfi.split(':')[1]);
                                    baseCfiOffset = 0;
                                    baseCfiOffset += 1;
                                    ch = sentence.sentence.substring(baseCfiOffset - 1, baseCfiOffset);
                                }

                                mSelectEndCfi = baseCfi + ':' + (baseCfiOffset + startCfiOffset);
                            }

                            page = sentence.sp;
                        }

                        val['res'] = 0;
                        val['index'] = mSelectIndex;
                        val['startCfi'] = mSelectStartCfi;
                        val['endCfi'] = mSelectEndCfi;
                        val['ch'] = ch;
                        val['page'] = page;
                    }
                    else {
                        val['res'] = -2;
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파일의 처음 단어를 선택 하도록 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(해당 문장이 없음)
            this.FirstWord = function (ignoreTypes) {
                var sentence = null;
                var arrSentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var baseCfiOffset = 0, startCfiOffset = 0;
                var baseCfi = '';
                var page = 0;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    val['res'] = -1;
                }
                else {
                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                    else {

                        this.ClearMultiSelectInfo();

                        mSelectIndex = 0;

                        while (true) {

                            if (mSelectIndex >= mSentenceList.length) {
                                mSelectIndex = -1;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (arrIgnoreType != null) {
                                var bSkip = false;
                                for (var i = 0; i < arrIgnoreType.length; i++) {
                                    if (sentence.type == arrIgnoreType[i]) {
                                        mSelectIndex++;
                                        bSkip = true;
                                        break;
                                    }
                                }

                                if (bSkip)
                                    continue;
                            }

                            if (sentence.draw == 'N') {
                                mSelectIndex++;
                                continue;
                            }
                            else {
                                break;
                            }
                        }
                    }

                    if (sentence != null) {
                        mSelectStartCfi = sentence.start;
                        mSelectEndCfi = sentence.end;
                        page = sentence.sp;

                        if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                            ch = sentence.sentence;
                        }
                        else {
                            if (sentence.sentence.length > 0) {
                                if (mSelectStartCfi.indexOf(':') > 0) {
                                    baseCfi = mSelectStartCfi.split(':')[0]
                                    startCfiOffset = parseInt(mSelectStartCfi.split(':')[1]);
                                    baseCfiOffset = 0;
                                }

                                arrSentence = sentence.sentence.split(' ');

                                if (arrSentence != null && arrSentence.length > 0) {
                                    baseCfiOffset += arrSentence[0].length;
                                    ch = arrSentence[0];
                                }

                                mSelectEndCfi = baseCfi + ':' + (baseCfiOffset + startCfiOffset);
                            }
                        }

                        val['res'] = 0;
                        val['index'] = mSelectIndex;
                        val['startCfi'] = mSelectStartCfi;
                        val['endCfi'] = mSelectEndCfi;
                        val['ch'] = ch;
                        val['page'] = page;
                    }
                    else {
                        val['res'] = -2;
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파일의 처음 문장을 선택 하도록 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(해당 문장이 없음)
            this.FirstSentence = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    val['res'] = -1;
                }
                else {
                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                    else {

                        this.ClearMultiSelectInfo();

                        mSelectIndex = 0;

                        while (true) {

                            if (mSelectIndex >= mSentenceList.length) {
                                mSelectIndex = -1;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (arrIgnoreType != null) {
                                var bSkip = false;
                                for (var i = 0; i < arrIgnoreType.length; i++) {
                                    if (sentence.type == arrIgnoreType[i]) {
                                        mSelectIndex++;
                                        bSkip = true;
                                        break;
                                    }
                                }

                                if (bSkip)
                                    continue;
                            }

                            if (sentence.draw == 'N') {
                                mSelectIndex++;
                                continue;
                            }
                            else {
                                break;
                            }
                        }
                    }

                    if (sentence != null) {
                        mSelectStartCfi = sentence.start;
                        mSelectEndCfi = sentence.end;

                        val['res'] = 0;
                        val['index'] = mSelectIndex;
                        val['startCfi'] = mSelectStartCfi;
                        val['endCfi'] = mSelectEndCfi;
                        val['ch'] = sentence.sentence;
                        val['page'] = sentence.sp;
                    }
                    else {
                        val['res'] = -2;
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파일의 마지막 글자를 선택 하도록 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(해당 문장이 없음)
            this.LastChar = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var baseCfiOffset = 0, startCfiOffset = 0, endCfiOffset = 0;
                var baseCfi = '';
                var page = 0;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                    else {

                        this.ClearMultiSelectInfo();

                        mSelectIndex = mSentenceList.length - 1;

                        while (true) {

                            if (mSelectIndex < 0) {
                                mSelectIndex--;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (sentence.draw == 'N') {
                                mSelectIndex--;
                                continue;
                            }
                            else {
                                break;
                            }
                        }
                    }

                    if (sentence != null) {
                        mSelectStartCfi = sentence.start;
                        mSelectEndCfi = sentence.end;
                        page = sentence.sp;

                        if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                            ch = sentence.sentence;
                        }
                        else {
                            if (sentence.sentence.length > 0) {
                                if (mSelectEndCfi.indexOf(':') > 0) {
                                    startCfiOffset = parseInt(mSelectStartCfi.split(':')[1]);
                                    baseCfi = mSelectEndCfi.split(':')[0];
                                    endCfiOffset = parseInt(mSelectEndCfi.split(':')[1]);
                                    baseCfiOffset = endCfiOffset - startCfiOffset;
                                    baseCfiOffset -= 1;
                                    ch = sentence.sentence.substring(baseCfiOffset, baseCfiOffset + 1);
                                }

                                mSelectStartCfi = baseCfi + ':' + (baseCfiOffset + startCfiOffset);
                            }
                        }

                        val['res'] = 0;
                        val['index'] = mSelectIndex;
                        val['startCfi'] = mSelectStartCfi;
                        val['endCfi'] = mSelectEndCfi;
                        val['ch'] = ch;
                        val['page'] = page;
                    }
                    else {
                        val['res'] = -2;
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파일의 마지막 단어를 선택 하도록 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(해당 문장이 없음)
            this.LastWord = function (ignoreTypes) {
                var sentence = null;
                var arrSentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var baseCfiOffset = 0;
                var baseCfi = '';
                var page = 0;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    val['res'] = -1;
                }
                else {
                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                    else {

                        this.ClearMultiSelectInfo();

                        mSelectIndex = mSentenceList.length - 1;

                        while (true) {

                            if (mSelectIndex < 0) {
                                mSelectIndex--;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (sentence.draw == 'N') {
                                mSelectIndex--;
                                continue;
                            }
                            else {
                                break;
                            }
                        }
                    }

                    if (sentence != null) {

                        mSelectStartCfi = sentence.start;
                        mSelectEndCfi = sentence.end;

                        page = sentence.sp;
                        if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                            ch = sentence.sentence;
                        }
                        else {
                            if (sentence.sentence.length > 0) {
                                if (mSelectEndCfi.indexOf(':') > 0) {
                                    baseCfi = mSelectEndCfi.split(':')[0]
                                    baseCfiOffset = parseInt(mSelectEndCfi.split(':')[1]);
                                }

                                arrSentence = sentence.sentence.split(' ');

                                if (arrSentence != null && arrSentence.length > 0) {
                                    baseCfiOffset -= arrSentence[arrSentence.length - 1].length;
                                    ch = arrSentence[arrSentence.length - 1];
                                }

                                mSelectStartCfi = baseCfi + ':' + baseCfiOffset;
                            }

                        }

                        val['res'] = 0;
                        val['index'] = mSelectIndex;
                        val['startCfi'] = mSelectStartCfi;
                        val['endCfi'] = mSelectEndCfi;
                        val['ch'] = ch;
                        val['page'] = page;
                    }
                    else {
                        val['res'] = -2;
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 파일의 마지막 문장을 선택 하도록 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(해당 문장이 없음)
            this.LastSentence = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    val['res'] = -1;
                }
                else {
                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                    else {

                        this.ClearMultiSelectInfo();

                        mSelectIndex = mSentenceList.length - 1;

                        while (true) {

                            if (mSelectIndex < 0) {
                                mSelectIndex--;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (sentence.draw == 'N') {
                                mSelectIndex--;
                                continue;
                            }
                            else {
                                break;
                            }
                        }
                    }

                    if (sentence != null) {

                        mSelectStartCfi = sentence.start;
                        mSelectEndCfi = sentence.end;
                        ch = sentence.sentence;

                        val['res'] = 0;
                        val['index'] = mSelectIndex;
                        val['startCfi'] = mSelectStartCfi;
                        val['endCfi'] = mSelectEndCfi;
                        val['ch'] = ch;
                        val['page'] = sentence.ed;
                    }
                    else {
                        val['res'] = -2;
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 이전 글자를 선택 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (이전 문자열이 없음)
            this.PrevChar = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            mSelectIndex = mSelectInfo[0].index;
                            mSelectStartCfi = mSelectInfo[0].startCfi;
                            mSelectEndCfi = mSelectInfo[0].endCfi;

                            this.ClearMultiSelectInfo();
                        }

                        while (true) {
                            if ((sentence = mSentenceList[mSelectIndex]) == null) {
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                                sentence = null;
                                val['res'] = -3;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    mSelectStartCfi = null;
                                    mSelectEndCfi = null;
                                    mSelectIndex--;
                                    continue;
                                }

                                if (arrIgnoreType != null) {
                                    var bSkip = false;
                                    for (var i = 0; i < arrIgnoreType.length; i++) {
                                        if (sentence.type == arrIgnoreType[i]) {
                                            mSelectIndex--;
                                            bSkip = true;
                                            break;
                                        }
                                    }

                                    if (bSkip)
                                        continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (mSelectStartCfi != null) {

                                    if (mSelectStartCfi.indexOf(':') > 0) {
                                        baseCfi = mSelectStartCfi.split(':')[0]
                                        baseCfiOffset = parseInt(mSelectStartCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = mSelectStartCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset == 0 || baseCfiOffset <= sentenceStartCfiOffset) {
                                        // 이전 문장의 마지막 글자를 선택 해야 함.
                                        mSelectStartCfi = null;
                                        mSelectEndCfi = null;
                                        mSelectIndex--;
                                        continue;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceStartCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset) - 1, (baseCfiOffset - sentenceStartCfiOffset)) != '') {
                                                baseCfiOffset--;
                                                break;
                                            }
                                            baseCfiOffset--;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 이전 글자를 찾을수가 없음.
                                            mSelectStartCfi = null;
                                            mSelectEndCfi = null;
                                            mSelectIndex--;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            mSelectStartCfi = baseCfi + ':' + baseCfiOffset;
                                            mSelectEndCfi = baseCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = mSelectIndex;
                                            val['startCfi'] = mSelectStartCfi;
                                            val['endCfi'] = mSelectEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                                else {
                                    //
                                    baseCfiOffset = sentenceEndCfiOffset;

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        mSelectStartCfi = sentence.start;
                                        mSelectEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = mSelectIndex;
                                        val['startCfi'] = mSelectStartCfi;
                                        val['endCfi'] = mSelectEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceStartCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset) - 1, (baseCfiOffset - sentenceStartCfiOffset)) != '') {
                                                baseCfiOffset--;
                                                break;
                                            }
                                            baseCfiOffset--;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 이전 글자를 찾을수가 없음.
                                            mSelectStartCfi = null;
                                            mSelectEndCfi = null;
                                            mSelectIndex--;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            mSelectStartCfi = sentenceCfi + ':' + baseCfiOffset;
                                            mSelectEndCfi = sentenceCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = mSelectIndex;
                                            val['startCfi'] = mSelectStartCfi;
                                            val['endCfi'] = mSelectEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 이전 글자 정보를 가져오며 선택은 하지 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (이전 문자열이 없음)
            this.PrevCharPreview = function (idx, startcfi, endcfi) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var sIndex = idx;
                var sEndCfi = endcfi;
                var sStartCfi = startcfi;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sIndex = -1;
                        sStartCfi = null;
                        sEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        while (true) {
                            if ((sentence = mSentenceList[sIndex]) == null) {
                                sIndex = -1;
                                sStartCfi = null;
                                sEndCfi = null;
                                sentence = null;
                                val['res'] = -3;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    sStartCfi = null;
                                    sEndCfi = null;
                                    sIndex--;
                                    continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (sStartCfi != null) {

                                    if (sStartCfi.indexOf(':') > 0) {
                                        baseCfi = sStartCfi.split(':')[0]
                                        baseCfiOffset = parseInt(sStartCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = sStartCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset == 0 || baseCfiOffset <= sentenceStartCfiOffset) {
                                        // 이전 문장의 마지막 글자를 선택 해야 함.
                                        sStartCfi = null;
                                        sEndCfi = null;
                                        sIndex--;
                                        continue;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceStartCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset) - 1, (baseCfiOffset - sentenceStartCfiOffset)) != '') {
                                                baseCfiOffset--;
                                                break;
                                            }
                                            baseCfiOffset--;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 이전 글자를 찾을수가 없음.
                                            sStartCfi = null;
                                            sEndCfi = null;
                                            sIndex--;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            sStartCfi = baseCfi + ':' + baseCfiOffset;
                                            sEndCfi = baseCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = sIndex;
                                            val['startCfi'] = sStartCfi;
                                            val['endCfi'] = sEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                                else {
                                    //
                                    baseCfiOffset = sentenceEndCfiOffset;

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        sStartCfi = sentence.start;
                                        sEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = sIndex;
                                        val['startCfi'] = sStartCfi;
                                        val['endCfi'] = sEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceStartCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset) - 1, (baseCfiOffset - sentenceStartCfiOffset)) != '') {
                                                baseCfiOffset--;
                                                break;
                                            }
                                            baseCfiOffset--;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 이전 글자를 찾을수가 없음.
                                            sStartCfi = null;
                                            sEndCfi = null;
                                            sIndex--;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            sStartCfi = sentenceCfi + ':' + baseCfiOffset;
                                            sEndCfi = sentenceCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = sIndex;
                                            val['startCfi'] = sStartCfi;
                                            val['endCfi'] = sEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 이전 단어를 선택 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (이전 문자열이 없음)
            this.PrevWord = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            mSelectIndex = mSelectInfo[0].index;
                            mSelectStartCfi = mSelectInfo[0].startCfi;
                            mSelectEndCfi = mSelectInfo[0].endCfi;

                            this.ClearMultiSelectInfo();
                        }

                        while (true) {
                            if ((sentence = mSentenceList[mSelectIndex]) == null) {
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                                sentence = null;
                                val['res'] = -3;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    mSelectStartCfi = null;
                                    mSelectEndCfi = null;
                                    mSelectIndex--;
                                    continue;
                                }

                                if (arrIgnoreType != null) {
                                    var bSkip = false;
                                    for (var i = 0; i < arrIgnoreType.length; i++) {
                                        if (sentence.type == arrIgnoreType[i]) {
                                            mSelectIndex--;
                                            bSkip = true;
                                            break;
                                        }
                                    }

                                    if (bSkip)
                                        continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (mSelectStartCfi != null) {

                                    if (mSelectStartCfi.indexOf(':') > 0) {
                                        baseCfi = mSelectStartCfi.split(':')[0]
                                        baseCfiOffset = parseInt(mSelectStartCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = mSelectStartCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset == 0 || baseCfiOffset <= sentenceStartCfiOffset) {
                                        // 이전 문장의 마지막 글자를 선택 해야 함.
                                        mSelectStartCfi = null;
                                        mSelectEndCfi = null;
                                        mSelectIndex--;
                                        continue;
                                    }
                                    else {
                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.lastIndexOf(' ', nOffset);

                                        if (nLastSpacePos <= 0) {
                                            mSelectStartCfi = null;
                                            mSelectEndCfi = null;
                                            mSelectIndex--;
                                            continue;
                                        }
                                        else {
                                            nLastSpacePos2 = sentence.sentence.lastIndexOf(' ', nLastSpacePos - 1);

                                            if (nLastSpacePos2 != -1) {

                                                mSelectEndCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                mSelectStartCfi = baseCfi + ':' + (nLastSpacePos2 + 1 + sentenceStartCfiOffset);

                                                ch = sentence.sentence.substring(nLastSpacePos2 + 1, nLastSpacePos);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = mSelectIndex;
                                                val['startCfi'] = mSelectStartCfi;
                                                val['endCfi'] = mSelectEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                            else {
                                                mSelectEndCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                mSelectStartCfi = baseCfi + ':' + sentenceStartCfiOffset;

                                                ch = sentence.sentence.substring(0, nLastSpacePos);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = mSelectIndex;
                                                val['startCfi'] = mSelectStartCfi;
                                                val['endCfi'] = mSelectEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else {

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        mSelectStartCfi = sentence.start;
                                        mSelectEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = mSelectIndex;
                                        val['startCfi'] = mSelectStartCfi;
                                        val['endCfi'] = mSelectEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {

                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.lastIndexOf(' ');

                                        if (nLastSpacePos <= 0) {
                                            ch = sentence.sentence;
                                            mSelectStartCfi = sentence.start;
                                            mSelectEndCfi = sentence.end;

                                            val['res'] = 0;
                                            val['index'] = mSelectIndex;
                                            val['startCfi'] = mSelectStartCfi;
                                            val['endCfi'] = mSelectEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = sentence.ed;
                                            break;
                                        }
                                        else {

                                            mSelectEndCfi = sentence.end;
                                            mSelectStartCfi = sentenceCfi + ':' + (nLastSpacePos + 1 + sentenceStartCfiOffset);

                                            ch = sentence.sentence.substring(nLastSpacePos + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = mSelectIndex;
                                            val['startCfi'] = mSelectStartCfi;
                                            val['endCfi'] = mSelectEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 이전 단어 정보를 가져오며 선택은 하지 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (이전 문자열이 없음)
            this.PrevWordPreview = function (idx, startcfi, endcfi) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var sIndex = idx;
                var sEndCfi = endcfi;
                var sStartCfi = startcfi;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sIndex = -1;
                        sStartCfi = null;
                        sEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        while (true) {
                            if ((sentence = mSentenceList[sIndex]) == null) {
                                sIndex = -1;
                                sStartCfi = null;
                                sEndCfi = null;
                                sentence = null;
                                val['res'] = -3;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    sStartCfi = null;
                                    sEndCfi = null;
                                    sIndex--;
                                    continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (sStartCfi != null) {

                                    if (sStartCfi.indexOf(':') > 0) {
                                        baseCfi = sStartCfi.split(':')[0]
                                        baseCfiOffset = parseInt(sStartCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = sStartCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset == 0 || baseCfiOffset <= sentenceStartCfiOffset) {
                                        // 이전 문장의 마지막 글자를 선택 해야 함.
                                        sStartCfi = null;
                                        sEndCfi = null;
                                        sIndex--;
                                        continue;
                                    }
                                    else {
                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.lastIndexOf(' ', nOffset);

                                        if (nLastSpacePos <= 0) {
                                            sStartCfi = null;
                                            sEndCfi = null;
                                            sIndex--;
                                            continue;
                                        }
                                        else {
                                            nLastSpacePos2 = sentence.sentence.lastIndexOf(' ', nLastSpacePos - 1);

                                            if (nLastSpacePos2 != -1) {

                                                sEndCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                sStartCfi = baseCfi + ':' + (nLastSpacePos2 + 1 + sentenceStartCfiOffset);

                                                ch = sentence.sentence.substring(nLastSpacePos2 + 1, nLastSpacePos);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = sIndex;
                                                val['startCfi'] = sStartCfi;
                                                val['endCfi'] = sEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                            else {
                                                sEndCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                sStartCfi = baseCfi + ':' + sentenceStartCfiOffset;

                                                ch = sentence.sentence.substring(0, nLastSpacePos);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = sIndex;
                                                val['startCfi'] = sStartCfi;
                                                val['endCfi'] = sEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else {

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        sStartCfi = sentence.start;
                                        sEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = sIndex;
                                        val['startCfi'] = sStartCfi;
                                        val['endCfi'] = sEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {

                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.lastIndexOf(' ');

                                        if (nLastSpacePos <= 0) {
                                            ch = sentence.sentence;
                                            sStartCfi = sentence.start;
                                            sEndCfi = sentence.end;

                                            val['res'] = 0;
                                            val['index'] = sIndex;
                                            val['startCfi'] = sStartCfi;
                                            val['endCfi'] = sEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = sentence.ed;
                                            break;
                                        }
                                        else {

                                            sEndCfi = sentence.end;
                                            sStartCfi = sentenceCfi + ':' + (nLastSpacePos + 1 + sentenceStartCfiOffset);

                                            ch = sentence.sentence.substring(nLastSpacePos + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = sIndex;
                                            val['startCfi'] = sStartCfi;
                                            val['endCfi'] = sEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 이전 문장을 선택 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (이전 문자열이 없음)
            this.PrevSentence = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            mSelectIndex = mSelectInfo[0].index;
                            mSelectStartCfi = mSelectInfo[0].startCfi;
                            mSelectEndCfi = mSelectInfo[0].endCfi;

                            this.ClearMultiSelectInfo();
                        }

                        mSelectIndex--;

                        while (true) {

                            if (mSelectIndex < 0) {
                                mSelectIndex = -1;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (arrIgnoreType != null) {
                                var bSkip = false;
                                for (var i = 0; i < arrIgnoreType.length; i++) {
                                    if (sentence.type == arrIgnoreType[i]) {
                                        mSelectIndex--;
                                        bSkip = true;
                                        break;
                                    }
                                }

                                if (bSkip)
                                    continue;
                            }

                            if (sentence.draw == 'N') {
                                mSelectIndex--;
                                continue;
                            }
                            else {
                                break;
                            }
                        }

                        if (sentence != null) {

                            mSelectStartCfi = sentence.start;
                            mSelectEndCfi = sentence.end;
                            ch = sentence.sentence;

                            val['res'] = 0;
                            val['index'] = mSelectIndex;
                            val['startCfi'] = mSelectStartCfi;
                            val['endCfi'] = mSelectEndCfi;
                            val['ch'] = ch;
                            val['page'] = sentence.ed;
                        }
                        else {
                            val['res'] = -3;
                            mSelectIndex = -1;
                            mSelectStartCfi = null;
                            mSelectEndCfi = null;
                            sentence = null;
                        }
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 이전 문장 정보를 가져오며 선택은 하지 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (이전 문자열이 없음)
            this.PrevSentencePreview = function (idx, startcfi, endcfi) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var ch = '';
                var sindex = idx;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sindex = -1;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {
                        sindex--;

                        while (true) {

                            if (sindex < 0) {
                                sindex = -1;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[sindex];

                            if (sentence.draw == 'N') {
                                sindex--;
                                continue;
                            }
                            else {
                                break;
                            }
                        }

                        if (sentence != null) {

                            ch = sentence.sentence;

                            val['res'] = 0;
                            val['index'] = sindex;
                            val['startCfi'] = sentence.start;
                            val['endCfi'] = sentence.end;
                            val['ch'] = ch;
                            val['page'] = sentence.ed;
                        }
                        else {
                            val['res'] = -3;
                            sindex = -1;
                            sentence = null;
                        }
                    }
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 다음 글자를 선택 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (다음 문자열이 없음)
            this.NextChar = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            //원래 마지막 선택 문장을 기준으로 변경 했다가,
                            //처음 문장으로 변경함.
                            mSelectIndex = mSelectInfo[0].index;
                            mSelectStartCfi = mSelectInfo[0].startCfi;
                            mSelectEndCfi = mSelectInfo[0].endCfi;

                            this.ClearMultiSelectInfo();
                        }

                        while (true) {
                            if ((sentence = mSentenceList[mSelectIndex]) == null) {
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                                sentence = null;
                                val['res'] = -3;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    mSelectStartCfi = null;
                                    mSelectEndCfi = null;
                                    mSelectIndex++;
                                    continue;
                                }

                                if (arrIgnoreType != null) {
                                    var bSkip = false;
                                    for (var i = 0; i < arrIgnoreType.length; i++) {
                                        if (sentence.type == arrIgnoreType[i]) {
                                            mSelectIndex++;
                                            bSkip = true;
                                            break;
                                        }
                                    }

                                    if (bSkip)
                                        continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (mSelectEndCfi != null) {

                                    if (mSelectEndCfi.indexOf(':') > 0) {
                                        baseCfi = mSelectEndCfi.split(':')[0]
                                        baseCfiOffset = parseInt(mSelectEndCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = mSelectEndCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset < 0 || baseCfiOffset >= sentenceEndCfiOffset) {
                                        // 다음 문장의 첫 글자를 선택 해야 함.
                                        mSelectStartCfi = null;
                                        mSelectEndCfi = null;
                                        mSelectIndex++;
                                        continue;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceEndCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1) != '') {
                                                break;
                                            }

                                            baseCfiOffset++;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 다음 글자를 찾을수가 없음.
                                            mSelectStartCfi = null;
                                            mSelectEndCfi = null;
                                            mSelectIndex++;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            mSelectStartCfi = baseCfi + ':' + baseCfiOffset;
                                            mSelectEndCfi = baseCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = mSelectIndex;
                                            val['startCfi'] = mSelectStartCfi;
                                            val['endCfi'] = mSelectEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                                else {
                                    //
                                    baseCfiOffset = sentenceStartCfiOffset;

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        mSelectStartCfi = sentence.start;
                                        mSelectEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = mSelectIndex;
                                        val['startCfi'] = mSelectStartCfi;
                                        val['endCfi'] = mSelectEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceEndCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1) != '') {
                                                break;
                                            }

                                            baseCfiOffset++;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 다음 글자를 찾을수가 없음.
                                            mSelectStartCfi = null;
                                            mSelectEndCfi = null;
                                            mSelectIndex++;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            mSelectStartCfi = sentenceCfi + ':' + baseCfiOffset;
                                            mSelectEndCfi = sentenceCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = mSelectIndex;
                                            val['startCfi'] = mSelectStartCfi;
                                            val['endCfi'] = mSelectEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 다음 글자 정보를 가져오며 선택은 하지 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (다음 문자열이 없음)
            this.NextCharPreview = function (idx, startcfi, endcfi) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var sIndex = idx;
                var sEndCfi = endcfi;
                var sStartCfi = startcfi;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sIndex = -1;
                        sStartCfi = null;
                        sEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        while (true) {
                            if ((sentence = mSentenceList[sIndex]) == null) {
                                sIndex = -1;
                                sStartCfi = null;
                                sEndCfi = null;
                                sentence = null;
                                val['res'] = -3;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    sStartCfi = null;
                                    sEndCfi = null;
                                    sIndex++;
                                    continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (sEndCfi != null) {

                                    if (sEndCfi.indexOf(':') > 0) {
                                        baseCfi = sEndCfi.split(':')[0]
                                        baseCfiOffset = parseInt(sEndCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = sEndCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset < 0 || baseCfiOffset >= sentenceEndCfiOffset) {
                                        // 이전 문장의 마지막 글자를 선택 해야 함.
                                        sStartCfi = null;
                                        sEndCfi = null;
                                        sIndex++;
                                        continue;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceEndCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1) != '') {
                                                break;
                                            }

                                            baseCfiOffset++;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 이전 글자를 찾을수가 없음.
                                            sStartCfi = null;
                                            sEndCfi = null;
                                            sIndex++;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            sStartCfi = baseCfi + ':' + baseCfiOffset;
                                            sEndCfi = baseCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = sIndex;
                                            val['startCfi'] = sStartCfi;
                                            val['endCfi'] = sEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                                else {
                                    //
                                    baseCfiOffset = sentenceStartCfiOffset;

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        sStartCfi = sentence.start;
                                        sEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = sIndex;
                                        val['startCfi'] = sStartCfi;
                                        val['endCfi'] = sEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {
                                        while (true) {
                                            if (sentenceEndCfiOffset == baseCfiOffset) {
                                                baseCfiOffset = -1;
                                                break;
                                            }

                                            if (sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1) != '') {
                                                break;
                                            }

                                            baseCfiOffset++;
                                        }

                                        if (baseCfiOffset < 0) {
                                            // 문장에서 이전 글자를 찾을수가 없음.
                                            sStartCfi = null;
                                            sEndCfi = null;
                                            sIndex++;
                                            continue;
                                        }
                                        else {
                                            ch = sentence.sentence.substring((baseCfiOffset - sentenceStartCfiOffset), (baseCfiOffset - sentenceStartCfiOffset) + 1);
                                            sStartCfi = sentenceCfi + ':' + baseCfiOffset;
                                            sEndCfi = sentenceCfi + ':' + (baseCfiOffset + 1);
                                            page = sentence.sp;

                                            val['res'] = 0;
                                            val['index'] = sIndex;
                                            val['startCfi'] = sStartCfi;
                                            val['endCfi'] = sEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = page;
                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 다음 단어를 선택 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (다음 문자열이 없음)
            this.NextWord = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            mSelectIndex = mSelectInfo[mSelectInfo.length - 1].index;
                            mSelectStartCfi = mSelectInfo[mSelectInfo.length - 1].startCfi;
                            mSelectEndCfi = mSelectInfo[mSelectInfo.length - 1].endCfi;

                            this.ClearMultiSelectInfo();
                        }

                        while (true) {
                            if ((sentence = mSentenceList[mSelectIndex]) == null) {
                                val['res'] = -3;
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                                sentence = null;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    mSelectStartCfi = null;
                                    mSelectEndCfi = null;
                                    mSelectIndex++;
                                    continue;
                                }

                                if (arrIgnoreType != null) {
                                    var bSkip = false;
                                    for (var i = 0; i < arrIgnoreType.length; i++) {
                                        if (sentence.type == arrIgnoreType[i]) {
                                            mSelectIndex++;
                                            bSkip = true;
                                            break;
                                        }
                                    }

                                    if (bSkip)
                                        continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (mSelectEndCfi != null) {

                                    if (mSelectEndCfi.indexOf(':') > 0) {
                                        baseCfi = mSelectEndCfi.split(':')[0]
                                        baseCfiOffset = parseInt(mSelectEndCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = mSelectEndCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset < 0 || baseCfiOffset >= sentenceEndCfiOffset) {
                                        // 이전 문장의 마지막 글자를 선택 해야 함.
                                        mSelectStartCfi = null;
                                        mSelectEndCfi = null;
                                        mSelectIndex++;
                                        continue;
                                    }
                                    else {

                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.indexOf(' ', nOffset);

                                        if (nLastSpacePos == -1) {
                                            mSelectStartCfi = null;
                                            mSelectEndCfi = null;
                                            mSelectIndex++;
                                            continue;
                                        }
                                        else {
                                            nLastSpacePos2 = sentence.sentence.indexOf(' ', nLastSpacePos + 1);

                                            if (nLastSpacePos2 != -1) {

                                                mSelectStartCfi = baseCfi + ':' + (nLastSpacePos + 1 + sentenceStartCfiOffset);
                                                mSelectEndCfi = baseCfi + ':' + (nLastSpacePos2 + sentenceStartCfiOffset);

                                                ch = sentence.sentence.substring(nLastSpacePos + 1, nLastSpacePos2);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = mSelectIndex;
                                                val['startCfi'] = mSelectStartCfi;
                                                val['endCfi'] = mSelectEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                            else {
                                                mSelectStartCfi = baseCfi + ':' + (nLastSpacePos + 1 + sentenceStartCfiOffset);
                                                mSelectEndCfi = sentence.end;

                                                ch = sentence.sentence.substring(nLastSpacePos + 1);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = mSelectIndex;
                                                val['startCfi'] = mSelectStartCfi;
                                                val['endCfi'] = mSelectEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else {
                                    //
                                    baseCfiOffset = sentenceEndCfiOffset;

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        mSelectStartCfi = sentence.start;
                                        mSelectEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = mSelectIndex;
                                        val['startCfi'] = mSelectStartCfi;
                                        val['endCfi'] = mSelectEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {
                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.indexOf(' ');

                                        if (nLastSpacePos == -1) {
                                            ch = sentence.sentence;
                                            mSelectStartCfi = sentence.start;
                                            mSelectEndCfi = sentence.end;

                                            val['res'] = 0;
                                            val['index'] = mSelectIndex;
                                            val['startCfi'] = mSelectStartCfi;
                                            val['endCfi'] = mSelectEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = sentence.ed;
                                            break;
                                        }
                                        else {
                                            if (nLastSpacePos == 0) {
                                                while (true) {
                                                    if (nLastSpacePos == sentence.sentence.length) {
                                                        baseCfiOffset = -1;
                                                        break;
                                                    }

                                                    if (sentence.sentence.substring(nLastSpacePos, nLastSpacePos + 1) != ' ') {
                                                        break;
                                                    }

                                                    nLastSpacePos++;
                                                }

                                                nLastSpacePos2 = sentence.sentence.indexOf(' ', nLastSpacePos + 1);

                                                if (nLastSpacePos2 != -1) {

                                                    mSelectStartCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                    mSelectEndCfi = baseCfi + ':' + (nLastSpacePos2 + sentenceStartCfiOffset);

                                                    ch = sentence.sentence.substring(nLastSpacePos, nLastSpacePos2);
                                                    page = sentence.sp;

                                                    val['res'] = 0;
                                                    val['index'] = mSelectIndex;
                                                    val['startCfi'] = mSelectStartCfi;
                                                    val['endCfi'] = mSelectEndCfi;
                                                    val['ch'] = ch;
                                                    val['page'] = page;
                                                    break;
                                                }
                                                else {
                                                    mSelectStartCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                    mSelectEndCfi = sentence.end;

                                                    ch = sentence.sentence.substring(nLastSpacePos);
                                                    page = sentence.sp;

                                                    val['res'] = 0;
                                                    val['index'] = mSelectIndex;
                                                    val['startCfi'] = mSelectStartCfi;
                                                    val['endCfi'] = mSelectEndCfi;
                                                    val['ch'] = ch;
                                                    val['page'] = page;
                                                    break;
                                                }
                                            }
                                            else {
                                                mSelectStartCfi = sentence.start;
                                                mSelectEndCfi = sentenceCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);

                                                ch = sentence.sentence.substring(0, nLastSpacePos);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = mSelectIndex;
                                                val['startCfi'] = mSelectStartCfi;
                                                val['endCfi'] = mSelectEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                            }

                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 다음 단어 정보를 가져오며 선택은 하지 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (다음 문자열이 없음)
            this.NextWordPreview = function (idx, startcfi, endcfi) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var sIndex = idx;
                var sEndCfi = endcfi;
                var sStartCfi = startcfi;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sIndex = -1;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        while (true) {
                            if ((sentence = mSentenceList[sIndex]) == null) {
                                val['res'] = -3;
                                sIndex = -1;
                                sentence = null;
                                break;
                            }
                            else {

                                if (sentence.draw == 'N') {
                                    sIndex++;
                                    continue;
                                }

                                if (sentence.start.indexOf(':') > 0) {
                                    sentenceCfi = sentence.start.split(':')[0]
                                    sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                }
                                else {
                                    sentenceCfi = sentence.start;
                                    sentenceStartCfiOffset = 0;
                                }

                                if (sentence.end.indexOf(':') > 0) {
                                    sentenceCfi = sentence.end.split(':')[0]
                                    sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                }
                                else {
                                    sentenceEndCfiOffset = 0;
                                }

                                if (sEndCfi != null) {

                                    if (sEndCfi.indexOf(':') > 0) {
                                        baseCfi = sEndCfi.split(':')[0]
                                        baseCfiOffset = parseInt(sEndCfi.split(':')[1]);
                                    }
                                    else {
                                        baseCfi = sEndCfi;
                                        baseCfiOffset = 0;
                                    }

                                    if (baseCfiOffset < 0 || baseCfiOffset >= sentenceEndCfiOffset) {
                                        // 이전 문장의 마지막 글자를 선택 해야 함.
                                        sStartCfi = null;
                                        sEndCfi = null;
                                        sIndex++;
                                        continue;
                                    }
                                    else {

                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.indexOf(' ', nOffset);

                                        if (nLastSpacePos == -1) {
                                            sStartCfi = null;
                                            sEndCfi = null;
                                            sIndex++;
                                            continue;
                                        }
                                        else {
                                            nLastSpacePos2 = sentence.sentence.indexOf(' ', nLastSpacePos + 1);

                                            if (nLastSpacePos2 != -1) {

                                                sStartCfi = baseCfi + ':' + (nLastSpacePos + 1 + sentenceStartCfiOffset);
                                                sEndCfi = baseCfi + ':' + (nLastSpacePos2 + sentenceStartCfiOffset);

                                                ch = sentence.sentence.substring(nLastSpacePos + 1, nLastSpacePos2);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = sIndex;
                                                val['startCfi'] = sStartCfi;
                                                val['endCfi'] = sEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                            else {
                                                sStartCfi = baseCfi + ':' + (nLastSpacePos + 1 + sentenceStartCfiOffset);
                                                sEndCfi = sentence.end;

                                                ch = sentence.sentence.substring(nLastSpacePos + 1);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = sIndex;
                                                val['startCfi'] = sStartCfi;
                                                val['endCfi'] = sEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else {
                                    //
                                    baseCfiOffset = sentenceEndCfiOffset;

                                    if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                        sStartCfi = sentence.start;
                                        sEndCfi = sentence.end;

                                        val['res'] = 0;
                                        val['index'] = sIndex;
                                        val['startCfi'] = sStartCfi;
                                        val['endCfi'] = sEndCfi;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.sp;
                                        break;
                                    }
                                    else {
                                        nOffset = baseCfiOffset - sentenceStartCfiOffset;
                                        nLastSpacePos = sentence.sentence.indexOf(' ');

                                        if (nLastSpacePos == -1) {
                                            ch = sentence.sentence;
                                            sStartCfi = sentence.start;
                                            sEndCfi = sentence.end;

                                            val['res'] = 0;
                                            val['index'] = sIndex;
                                            val['startCfi'] = sStartCfi;
                                            val['endCfi'] = sEndCfi;
                                            val['ch'] = ch;
                                            val['page'] = sentence.ed;
                                            break;
                                        }
                                        else {
                                            if (nLastSpacePos == 0) {
                                                while (true) {
                                                    if (nLastSpacePos == sentence.sentence.length) {
                                                        baseCfiOffset = -1;
                                                        break;
                                                    }

                                                    if (sentence.sentence.substring(nLastSpacePos, nLastSpacePos + 1) != ' ') {
                                                        break;
                                                    }

                                                    nLastSpacePos++;
                                                }

                                                nLastSpacePos2 = sentence.sentence.indexOf(' ', nLastSpacePos + 1);

                                                if (nLastSpacePos2 != -1) {

                                                    sStartCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                    sEndCfi = baseCfi + ':' + (nLastSpacePos2 + sentenceStartCfiOffset);

                                                    ch = sentence.sentence.substring(nLastSpacePos, nLastSpacePos2);
                                                    page = sentence.sp;

                                                    val['res'] = 0;
                                                    val['index'] = sIndex;
                                                    val['startCfi'] = sStartCfi;
                                                    val['endCfi'] = sEndCfi;
                                                    val['ch'] = ch;
                                                    val['page'] = page;
                                                    break;
                                                }
                                                else {
                                                    sStartCfi = baseCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);
                                                    sEndCfi = sentence.end;

                                                    ch = sentence.sentence.substring(nLastSpacePos);
                                                    page = sentence.sp;

                                                    val['res'] = 0;
                                                    val['index'] = sIndex;
                                                    val['startCfi'] = sStartCfi;
                                                    val['endCfi'] = sEndCfi;
                                                    val['ch'] = ch;
                                                    val['page'] = page;
                                                    break;
                                                }
                                            }
                                            else {
                                                sStartCfi = sentence.start;
                                                sEndCfi = sentenceCfi + ':' + (nLastSpacePos + sentenceStartCfiOffset);

                                                ch = sentence.sentence.substring(0, nLastSpacePos);
                                                page = sentence.sp;

                                                val['res'] = 0;
                                                val['index'] = sIndex;
                                                val['startCfi'] = sStartCfi;
                                                val['endCfi'] = sEndCfi;
                                                val['ch'] = ch;
                                                val['page'] = page;
                                            }

                                            break;
                                        }
                                    }
                                }
                            }
                        }// while
                    }
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 다음 문장을 선택 한다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (다음 문자열이 없음)
            this.NextSentence = function (ignoreTypes) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var ch = '';
                var arrIgnoreType = null;

                if (ignoreTypes != null && ignoreTypes != "") {
                    if (ignoreTypes.indexOf(',') > 0) {
                        arrIgnoreType = ignoreTypes.split(',');
                    }
                    else {
                        arrIgnoreType = new Array();
                        arrIgnoreType.push(ignoreTypes);
                    }
                }

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            mSelectIndex = mSelectInfo[mSelectInfo.length - 1].index;
                            mSelectStartCfi = mSelectInfo[mSelectInfo.length - 1].startCfi;
                            mSelectEndCfi = mSelectInfo[mSelectInfo.length - 1].endCfi;

                            this.ClearMultiSelectInfo();
                        }

                        mSelectIndex++;

                        while (true) {

                            if (mSelectIndex < 0 || mSelectIndex >= mSentenceList.length) {
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[mSelectIndex];

                            if (arrIgnoreType != null) {
                                var bSkip = false;
                                for (var i = 0; i < arrIgnoreType.length; i++) {
                                    if (sentence.type == arrIgnoreType[i]) {
                                        mSelectIndex++;
                                        bSkip = true;
                                        break;
                                    }
                                }

                                if (bSkip)
                                    continue;
                            }

                            if (sentence.draw == 'N') {
                                mSelectIndex++;
                                continue;
                            }
                            else {
                                break;
                            }
                        }

                        if (sentence != null) {

                            mSelectStartCfi = sentence.start;
                            mSelectEndCfi = sentence.end;
                            ch = sentence.sentence;

                            val['res'] = 0;
                            val['index'] = mSelectIndex;
                            val['startCfi'] = mSelectStartCfi;
                            val['endCfi'] = mSelectEndCfi;
                            val['ch'] = ch;
                            val['page'] = sentence.ed;
                        }
                        else {
                            val['res'] = -3;
                            mSelectIndex = -1;
                            mSelectStartCfi = null;
                            mSelectEndCfi = null;
                            sentence = null;
                        }
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택 되어 있는 글자의 다음 문장만 가져온다. 선택은 하지 않는다.
            // [ Param  ] none
            // [ Return ] JSON Array 로 결과를 리턴한다.
            //            res : 0(정상), -1 (초기화 안됨), -2(현재 파일의 TTS 데이터가 없음), -3 (다음 문자열이 없음)
            this.NextSentencePreview = function (idx, startcfi, endcfi) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var sindex = idx;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sentence = null;
                        sindex = -1;
                        val['res'] = -2;
                    }
                    else {
                        sindex++;

                        while (true) {

                            if (sindex < 0 || sindex >= mSentenceList.length) {
                                sindex = -1;
                                sentence = null;
                                break;
                            }

                            sentence = mSentenceList[sindex];

                            if (sentence.draw == 'N') {
                                sindex++;
                                continue;
                            }
                            else {
                                break;
                            }
                        }

                        if (sentence != null) {

                            val['res'] = 0;
                            val['index'] = sindex;
                            val['startCfi'] = sentence.start;
                            val['endCfi'] = sentence.end;
                            val['ch'] = sentence.sentence;
                            val['page'] = sentence.ed;
                        }
                        else {
                            val['res'] = -3;
                            sindex = -1;
                            sentence = null;
                        }
                    }
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] CFI로 문장 인덱스를 리턴한다.
            // [ Param  ] none
            // [ Return ] 정수형 숫자로 리턴
            this.FirstSentenceOnlyCfi = function (cfi, text) {
                var val = RunaEngine.HtmlCtrl.AVTTSEngine.FindSentence(cfi, text, false);

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] CFI로 문장 인덱스를 리턴한다.
            // [ Param  ] none
            // [ Return ] 정수형 숫자로 리턴
            this.FindSentence = function (cfi, text, isset) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var senteceIdx = 0;
                var textData = '';
                var startOffset = 0, endOffset = 0;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {
                        if (cfi.indexOf(':') > 0) {
                            baseCfi = cfi.split(':')[0];
                            baseCfiOffset = parseInt(cfi.split(':')[1]);
                        }
                        else {
                            baseCfi = cfi;
                            baseCfiOffset = 0;
                        }

                        textData = AVDRemoveWhiteSpace(text);
                        startOffset = text.indexOf(textData);
                        endOffset = text.lastIndexOf(textData);

                        if (startOffset == endOffset) endOffset = endOffset + textData.length;

                        while (true) {

                            if (senteceIdx >= mSentenceList.length) {
                                val['res'] = -3;
                                if (isset) {
                                    mSelectIndex = -1;
                                    mSelectStartCfi = null;
                                    mSelectEndCfi = null;
                                }
                                sentence = null;
                                senteceIdx = -1;
                                break;
                            }

                            sentence = mSentenceList[senteceIdx];

                            if (sentence.start.indexOf(':') > 0) {
                                sentenceCfi = sentence.start.split(':')[0];
                                sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                            }
                            else {
                                sentenceCfi = sentence.start;
                                sentenceStartCfiOffset = 0;
                            }

                            if (sentence.end.indexOf(':') > 0) {
                                //sentenceCfi = sentence.end.split(':')[0];
                                sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                            }
                            else {
                                sentenceEndCfiOffset = 0;
                            }

                            if (baseCfi == sentenceCfi) {
                                if (sentence.sentence == textData) {
                                    if (isset) {
                                        mSelectStartCfi = sentence.start;
                                        mSelectEndCfi = sentence.end;
                                        mSelectIndex = senteceIdx;

                                        this.HighlightOnByCfi(mSelectIndex, mSelectStartCfi, mSelectEndCfi, hlAvdTTSColor);
                                    }

                                    ch = sentence.sentence;

                                    val['res'] = 0;
                                    val['index'] = senteceIdx;
                                    val['startCfi'] = sentence.start;
                                    val['endCfi'] = sentence.end;
                                    val['ch'] = ch;
                                    val['page'] = sentence.ed;

                                    RunaEngine.sublog2('FindSentence = [' + cfi + ', ' + text + '], [' + mSelectIndex + ', ' + ch + ']');
                                    break;
                                }
                                else {
                                    if (sentence.sentence.length > textData.length) {
                                        if ((baseCfiOffset + startOffset) >= sentenceStartCfiOffset && (baseCfiOffset + endOffset) <= sentenceEndCfiOffset) {

                                            if (isset) {
                                                mSelectStartCfi = cfi;
                                                mSelectEndCfi = sentence.end;
                                                mSelectIndex = senteceIdx;
                                                this.HighlightOnByCfi(mSelectIndex, mSelectStartCfi, mSelectEndCfi, hlAvdTTSColor);
                                            }

                                            ch = this.GetSelectionTextByCfi(senteceIdx, cfi, sentence.end).ch;

                                            val['res'] = 0;
                                            val['index'] = senteceIdx;
                                            val['startCfi'] = cfi;
                                            val['endCfi'] = sentence.end;
                                            val['ch'] = ch;
                                            val['page'] = sentence.ed;
                                            //RunaEngine.sublog2('FindSentence = [' + cfi + ', ' + text + '], [' + mSelectIndex + ', ' + ch + ']');
                                            break;
                                        }
                                    }
                                    else {
                                        if (sentence.type == 'MATH_TEXT') {
                                            if (isset) {
                                                mSelectStartCfi = sentence.start;
                                                mSelectEndCfi = sentence.end;
                                                mSelectIndex = senteceIdx;
                                                this.HighlightOnByCfi(mSelectIndex, mSelectStartCfi, mSelectEndCfi, hlAvdTTSColor);
                                            }

                                            ch = sentence.sentence;

                                            val['res'] = 0;
                                            val['index'] = senteceIdx;
                                            val['startCfi'] = sentence.start;
                                            val['endCfi'] = sentence.end;
                                            val['ch'] = ch;
                                            val['page'] = sentence.ed;
                                            //RunaEngine.sublog2('FindSentence = [' + cfi + ', ' + text + '], [' + mSelectIndex + ', ' + ch + ']');
                                            break;
                                        }
                                        else if (sentence.type == 'TABLE_START') {
                                            sentence = mSentenceList[senteceIdx + 1];

                                            if (sentence != null) {
                                                senteceIdx++;

                                                if (isset) {
                                                    mSelectStartCfi = sentence.start;
                                                    mSelectEndCfi = sentence.end;
                                                    mSelectIndex = senteceIdx;
                                                    this.HighlightOnByCfi(mSelectIndex, mSelectStartCfi, mSelectEndCfi, hlAvdTTSColor);
                                                }

                                                ch = sentence.sentence;

                                                val['res'] = 0;
                                                val['index'] = senteceIdx;
                                                val['startCfi'] = sentence.start;
                                                val['endCfi'] = sentence.end;
                                                val['ch'] = ch;
                                                val['page'] = sentence.ed;

                                                //RunaEngine.sublog2('FindSentence = [' + cfi + ', ' + text + '], [' + mSelectIndex + ', ' + ch + ']');
                                                break;
                                            }
                                        }
                                        else {
                                            //cfi는 같으나 텍스트 데이터가 다를때
                                            if (baseCfiOffset >= sentenceStartCfiOffset && baseCfiOffset <= sentenceEndCfiOffset) {

                                                if (isset) {
                                                    mSelectStartCfi = cfi;
                                                    mSelectEndCfi = sentence.end;
                                                    mSelectIndex = senteceIdx;
                                                    this.HighlightOnByCfi(mSelectIndex, mSelectStartCfi, mSelectEndCfi, hlAvdTTSColor);
                                                }

                                                ch = this.GetSelectionTextByCfi(senteceIdx, cfi, sentence.end).ch;

                                                val['res'] = 0;
                                                val['index'] = senteceIdx;
                                                val['startCfi'] = cfi;
                                                val['endCfi'] = sentence.end;
                                                val['ch'] = ch;
                                                val['page'] = sentence.ed;

                                                RunaEngine.sublog2('FindSentence 55555555 = [' + sentenceCfi + ', ' + baseCfi + ']');
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                //cfi가 다를때
                                if (sentenceCfi != null && sentenceCfi != undefined && sentenceCfi != '') {
                                    if (Math.abs(sentenceCfi.length - baseCfi.len) < 2) {
                                        if (baseCfi.indexOf(sentenceCfi) >= 0) {
                                            RunaEngine.sublog2('FindSentence 55555555 = [' + sentenceCfi + ', ' + baseCfi + ']');
                                        }
                                        else if (sentenceCfi.indexOf(baseCfi) >= 0) {
                                            RunaEngine.sublog2('FindSentence 66666666 = [' + sentenceCfi + ', ' + baseCfi + ']');
                                        }
                                    }
                                }
                            }

                            senteceIdx++;
                        }

                        if (senteceIdx == -1) {
                            val['res'] = -3;
                            if (isset) {
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                            }
                            sentence = null;
                            senteceIdx = -1;
                        }
                    }
                }

                result = JSON.stringify(val);

                RunaEngine.sublog2('FindSentence, res : ' + result);

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] id로 문장 인덱스를 리턴한다.
            // [ Param  ] none
            // [ Return ] 정수형 숫자로 리턴
            this.FindSentenceById = function (id, text) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var senteceIdx = 0;
                var textData = '';
                var startOffset = 0, endOffset = 0;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mIdMap.get(id) == null) {
                            val['res'] = -3;
                            senteceIdx = -1;
                        }
                        else {
                            senteceIdx = mIdMap.get(id);
                        }

                        textData = AVDRemoveWhiteSpace(text);
                        startOffset = text.indexOf(textData);
                        endOffset = text.lastIndexOf(textData);

                        if (startOffset == endOffset) endOffset = endOffset + textData.length;

                        sentence = mSentenceList[senteceIdx];

                        if (sentence == null) {
                            val['res'] = -4;
                            mSelectIndex = -1;
                            mSelectStartCfi = null;
                            mSelectEndCfi = null;
                            sentence = null;
                        }
                        else {
                            mSelectStartCfi = sentence.start;
                            mSelectEndCfi = sentence.end;
                            ch = sentence.sentence;
                            mSelectIndex = senteceIdx;

                            val['res'] = 0;
                            val['index'] = mSelectIndex;
                            val['startCfi'] = mSelectStartCfi;
                            val['endCfi'] = mSelectEndCfi;
                            val['ch'] = ch;
                            val['page'] = sentence.ed;
                        }
                    }
                }

                result = JSON.stringify(val);

                RunaEngine.sublog2('FindSentenceById, res : ' + result);

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] id 와 현재 셀렉션으로 로 문장 인덱스를 리턴한다.
            // [ Param  ] none
            // [ Return ] 정수형 숫자로 리턴
            this.FindSentenceByIdNSelection = function (id) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';
                var senteceIdx = 0;
                var textData = '';
                var startOffset = 0, endOffset = 0;

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mIdMap.get(id) == null) {
                            val['res'] = -3;
                            senteceIdx = -1;
                        }
                        else {
                            senteceIdx = mIdMap.get(id);
                        }

                        sentence = mSentenceList[senteceIdx];

                        if (sentence == null) {
                            val['res'] = -4;
                        }
                        else {
                            //해당 아이디와 연결된 문장을 리턴한다.
                            val['res'] = 0;
                            val['index'] = senteceIdx;
                            val['startCfi'] = sentence.start;
                            val['endCfi'] = sentence.end;
                            val['ch'] = sentence.sentence;
                            val['page'] = sentence.ed;
                        }
                    }
                }

                result = JSON.stringify(val);

                RunaEngine.sublog2('FindSentenceByIdNSelection, res : ' + result);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 파일의 단락 수를 가져온다.
            // [ Param  ] none
            // [ Return ] 정수형 숫자로 리턴
            this.getParagraphCount = function () {
                var val = 0;
                if (mParagraphMap != null) {
                    val = mParagraphMap.size();
                }

                return val;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 이동 할려는 단락의 문장 번호를 리턴한다. 반드시 호출 후에는 NextSentence 함수를 호출한다.
            // [ Param  ] none
            // [ Return ] 정수형 숫자로 리턴
            this.moveToParagraph = function (index) {
                var val = { res: 0, index: -1 };
                var idx = 0;

                if (mParagraphMap.get(index) == null) {
                    val['res'] = -1;
                }
                else {
                    idx = mParagraphMap.get(index);

                    sentence = mSentenceList[idx];

                    if (sentence.type != 'PARAGRAPH') {
                        val['res'] = -2;
                    }
                    else {
                        val['index'] = idx;
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 사용된 메모리 들을 해제 한다.
            // [ Param  ] none
            this.close = function () {

                if (mSentenceList != null) {
                    while (mSentenceList.length > 0) {
                        a = mSentenceList.splice(0, 1);
                        a = null;
                    }
                }

                if (mSelectInfo != null) {
                    while (mSelectInfo.length > 0) {
                        a = mSelectInfo.splice(0, 1);
                        a = null;
                    }
                }

                if (mParagraphMap != null) {
                    mParagraphMap.clear();
                    mParagraphMap = null;
                }
                if (mTableMap != null) {
                    mTableMap.clear();
                    mTableMap = null;
                }
                mSentenceList = null;
                mSentenceSeq = 0;
                mInitPage = false;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 문장 단위로 정리한다.
            // [ Param  ] 문장 데이터
            // [ Return ] 단위 단위로 분리된 문자열 배열을 리턴
            this.BuildSentence = function (sentence) {
                var sentenceList = new Array();
                var match = new Array();
                //var regExp = /[.,!?](?![^\(]*\))(?=(.,?!)*)/gi;
                var regExp = /[.!?](?! *\b\d{1,4})/gi;
                var result;
                while ((result = regExp.exec(sentence)) !== null) {
                    match.push(result.index);
                }

                if (match.length == 0 || sentence.length == 0) {
                    if (sentence.length > 0) {
                        var item = { s: sentence, start: 0, end: sentence.length };
                        RunaEngine.log('BuildSentence = [' + item.s + ']');
                        sentenceList.push(item);
                    }
                    return sentenceList;
                }

                if (match.length > 0) {
                    match.push(-1);
                }

                if (match.length >= 1) {
                    var i = 0;
                    var lastIndex1 = sentence.length;
                    var lastIndex2 = 0;
                    var skipFlag = false;
                    var breakLoop = false;

                    while (true) {
                        if (skipFlag) {
                            lastIndex2 = match[i++];
                            skipFlag = false;
                        }
                        else {
                            lastIndex1 = lastIndex2;
                            lastIndex2 = match[i++];
                        }

                        //if (sentence.substring(lastIndex1+1, lastIndex1 + 2) == '\n') {
                        //    lastIndex1++;
                        //}

                        if (lastIndex2 == -1) {
                            // 종료.
                            lastIndex2 = sentence.length - 1;
                            breakLoop = true;
                        }
                        else {
                            // 열고 닫는 특수 문자 (), {}, [], "", '', <>
                            // 전부 체크 하자.
                            if (sentence.substring(lastIndex2 + 1, lastIndex2 + 2) == "”" || sentence.substring(lastIndex2 + 1, lastIndex2 + 2) == "’"
                                 || sentence.substring(lastIndex2 + 1, lastIndex2 + 2) == ")" || sentence.substring(lastIndex2 + 1, lastIndex2 + 2) == "]"
                                  || sentence.substring(lastIndex2 + 1, lastIndex2 + 2) == "}" || sentence.substring(lastIndex2 + 1, lastIndex2 + 2) == ">") {
                                lastIndex2 = lastIndex2 + 1;
                            }
                            else {
                                //.이 오는 경우에는 다음 문자를 보자.
                                if (sentence.substring(lastIndex2 + 1, lastIndex2 + 2) == ".") {
                                    skipFlag = true;
                                    continue;
                                }
                                // 구분자 전에 오는 글자가 숫자이면 스킵하자.
                                else if (!isNaN(sentence.substring(lastIndex2 - 1, lastIndex2))) {
                                    skipFlag = true;
                                    continue;
                                }
                                else if (lastIndex2 - lastIndex1 == 0 || lastIndex2 - lastIndex1 == 1) {
                                    skipFlag = true;
                                    continue;
                                }
                            }

                        }

                        // ios - jdm
                        if (RunaEngine.DeviceInfo.IsIOS) {
                            if (sentence.substring(0, 1) == "\""){ // || sentence.substring(0, 1) == "」" || sentence.substring(0, 1) == "』") {
                                RunaEngine.log('BuildSentence lastIndex1, lastIndex2 = [' + lastIndex1 + ', ' + lastIndex2 + ']');
                                lastIndex2 = lastIndex2 + 1;
                            }
                        }

                        str = sentence.substring(lastIndex1 == 0 ? 0 : (lastIndex1 + 1), lastIndex2 + 1);
                        // ios - jdm
                        if (RunaEngine.DeviceInfo.IsIOS) {
                            AVDRemoveWhiteSpace(str);
                        }
                        var item = { s: str, start: lastIndex1 == 0 ? 0 : (lastIndex1 + 1), end: lastIndex2 + 1 };
                        sentenceList.push(item);
                        RunaEngine.log('BuildSentence = [' + item.s + ', ' + item.start + ', ' + item.end + ']');

                        if (breakLoop) {
                            break;
                        }
                    }
                }

                return sentenceList;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 하이라이트 제거
            // [ Param  ] none
            this.HighlightOff = function () {
                var rtnVal = { res: -1, page: -1 };

                if (mInitPage) {

                    try {
                        RunaEngine.HtmlCtrl.Selection.DeleteSelection();
                        RunaEngine.HtmlCtrl.Highlight.Remove(hlCreateId);
                    }
                    catch (ex) {
                        RunaEngine.log(ex);
                    }
                }

                result = JSON.stringify(rtnVal);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 문단 단위로 하이라이트를 한다.
            //            하이라이트를 하게 되면 해당 문장 인덱스와 문장의 위치 정보를 선택된 위치 정보로 설정한다.
            // [ Param  ] color - #33aa11 식의 컬러 값을 넣는다.
            this.HighlightOn = function (index, color) {
                var rtnVal = { res: -1, page: -1 };
                var nPage = -1;

                if (mInitPage) {
                    hlAvdTTSColor = color;

                    try {
                        RunaEngine.HtmlCtrl.Selection.DeleteSelection();
                        RunaEngine.HtmlCtrl.Highlight.Remove(hlCreateId);

                        if (mSentenceList[index].start != mSentenceList[index].end) {
                            val = RunaEngine.HtmlCtrl.Highlight.CreateHighLight(hlCreateId, mSentenceList[index].start, mSentenceList[index].end, color);

                            if (val != null && !(val.propertyIsEnumerable('length')) &&
                                typeof val === 'object' &&
                                typeof val.length === 'number') {
                                nPage = val['page'];
                            }

                            if (parseInt(val['res']) == 0) {
                                mSelectIndex = index;
                                mSelectStartCfi = mSentenceList[index].start;
                                mSelectEndCfi = mSentenceList[index].end;
                            }
                            /*전 버전에 있던 부분*/
                            if(mSentenceList[index].ed != nPage) {
                            	nPage = mSentenceList[index].ed
                            }

                            rtnVal['res'] = val['res'];
                            rtnVal['page'] = nPage;
                        }
                        else {
                            if (mSentenceList[index].type == 'IMG' || mSentenceList[index].type == 'TABLE_NO_DATA') {
                                mSelectIndex = index;
                                mSelectStartCfi = mSentenceList[index].start;
                                mSelectEndCfi = mSentenceList[index].end;
                                rtnVal['res'] = 0;
                                rtnVal['page'] = mSentenceList[index].sp;
                            }
                        }

                        RunaEngine.sublog2('[res = ' + val['res'] + '] , HighlightOn, start=' + mSentenceList[index].start + ', end = ' + mSentenceList[index].end + ', page : ' + val['page']);
                    }
                    catch (ex) {
                        RunaEngine.sublog2(ex);
                    }
                }

                result = JSON.stringify(rtnVal);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] cfi 단위로 하이라이트를 한다.
            //            하이라이트를 하게 되면 해당 문장 인덱스와 문장의 위치 정보를 선택된 위치 정보로 설정한다.
            // [ Param  ] color - #33aa11 식의 컬러 값을 넣는다.
            this.HighlightOnByCfi = function (index, start, end, color) {
                var rtnVal = { res: -1, page: -1 };
                var nPage = -1;
                var sentence = null;

                if (mInitPage) {
                    hlAvdTTSColor = color;

                    try {
                        RunaEngine.HtmlCtrl.Selection.DeleteSelection();
                        RunaEngine.HtmlCtrl.Highlight.Remove(hlCreateId);

                        if (start != end) {
                            val = RunaEngine.HtmlCtrl.Highlight.CreateHighLight(hlCreateId, start, end, color);

                            if (val != null && !(val.propertyIsEnumerable('length')) &&
                                typeof val === 'object' &&
                                typeof val.length === 'number') {
                                nPage = val['page'];
                            }

                            if (parseInt(val['res']) == 0) {
                                mSelectIndex = index;
                                mSelectStartCfi = start;
                                mSelectEndCfi = end;
                            }

                            //검증을 하자.
                            /*전 버전에 있던 부분*/
                            if(mSentenceList[index].ed != nPage) {
                            	nPage = mSentenceList[index].ed
                            }

                            rtnVal['res'] = val['res'];
                            rtnVal['page'] = nPage;
                        }
                        else {
                            if ((sentence = mSentenceList[index]) != null) {
                                if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                    mSelectIndex = index;
                                    mSelectStartCfi = start;
                                    mSelectEndCfi = end;
                                    rtnVal['res'] = 0;
                                    rtnVal['page'] = sentence.sp;
                                }
                            }
                        }

                        RunaEngine.sublog('[res = ' + val['res'] + '] , HighlightOnByCfi, start=' + start + ', end = ' + end + ', page : ' + val['page']);
                    }
                    catch (ex) {
                        RunaEngine.sublog(ex);
                    }
                }

                result = JSON.stringify(rtnVal);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 문단 단위로 하이라이트를 한다.
            //            하이라이트를 하게 되면 해당 문장 인덱스와 문장의 위치 정보를 선택된 위치 정보로 설정한다.
            // [ Param  ] color - #33aa11 식의 컬러 값을 넣는다.
            this.MultiHighlightOn = function (color) {
                var rtnVal = { res: -1, page: -1 };
                var nPage = -1;
                var sentence = null;

                if (mInitPage) {
                    hlAvdTTSColor = color;

                    try {
                        RunaEngine.HtmlCtrl.Selection.DeleteSelection();
                        this.RemoveMultiSelectInfoHighlight();
                        RunaEngine.HtmlCtrl.Highlight.Remove(hlCreateId);

                        for (var i = 0; i < mSelectInfo.length; i++) {
                            a = mSelectInfo[i];

                            if ((sentence = mSentenceList[a.index]) != null) {
                                if (sentence.type == 'IMG' || sentence.type == 'TABLE_NO_DATA') {
                                    rtnVal['res'] = 0;
                                    rtnVal['page'] = sentence.sp;
                                }
                                else {
                                    val = RunaEngine.HtmlCtrl.Highlight.CreateHighLight(a.hlId, a.startCfi, a.endCfi, color);

                                    if (val != null && !(val.propertyIsEnumerable('length')) &&
                                        typeof val === 'object' &&
                                        typeof val.length === 'number') {
                                        nPage = val['page'];
                                    }

                                    RunaEngine.log('[res = ' + val['res'] + '] , HighlightOnOnlyDraw, start=' + a.startCfi + ', end = ' + a.endCfi + ', page : ' + val['page']);

                                    rtnVal['res'] = val['res'];
                                    rtnVal['page'] = nPage;
                                }
                            }
                        }
                    }
                    catch (ex) {
                        RunaEngine.log(ex);
                    }
                }

                result = JSON.stringify(rtnVal);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 문장을 선택한다.
            // [ Param  ] index : 문장 인덱스 번호, start : 시작 위치, end : 마지막 위치
            // [ Return ] 찾은 문장의 정보과 결과를 JSON으로 리턴한다.
            //            0 : 정상, -1 : 초기화 안됨, -2 : 문장 리스트가 없음, -3 : 해당 인덱스의 문장이 없음,
            //            -4 : 오프셋 위치가 부적절함 , -5 : 문장 cfi 가 맞지 않음
            this.SetCurrentSelectionInfo = function (idx, start, end) {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var StartCfi = null;
                var EndCfi = null;
                var StartCfiOffset = -1;
                var EndCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {
                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            this.ClearMultiSelectInfo();
                        }

                        if (idx == -1) {
                            sentence = mSentenceList[0];
                            mSelectIndex = 0;

                            mSelectStartCfi = sentence.start;
                            mSelectEndCfi = sentence.end;
                            ch = sentence.sentence;

                            val['res'] = 0;
                            val['index'] = 0;
                            val['startCfi'] = sentence.start;
                            val['endCfi'] = sentence.end;
                            val['ch'] = sentence.sentence;
                            val['page'] = sentence.ed;
                        }
                        else {

                            if ((sentence = mSentenceList[idx]) == null) {
                                val['res'] = -3;
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                                sentence = null;
                            }
                            else {
                                if (start == sentence.start && end == sentence.end) {
                                    mSelectIndex = idx;

                                    mSelectStartCfi = sentence.start;
                                    mSelectEndCfi = sentence.end;
                                    ch = sentence.sentence;

                                    val['res'] = 0;
                                    val['index'] = mSelectIndex;
                                    val['startCfi'] = sentence.start;
                                    val['endCfi'] = sentence.end;
                                    val['ch'] = sentence.sentence;
                                    val['page'] = sentence.ed;
                                }
                                else {

                                    if (start == '' || end == '') {
                                        mSelectIndex = idx;

                                        mSelectStartCfi = sentence.start;
                                        mSelectEndCfi = sentence.end;
                                        ch = sentence.sentence;

                                        val['res'] = 0;
                                        val['index'] = mSelectIndex;
                                        val['startCfi'] = sentence.start;
                                        val['endCfi'] = sentence.end;
                                        val['ch'] = sentence.sentence;
                                        val['page'] = sentence.ed;
                                    }
                                    else {
                                        // 특정 위치가 있을때는 특정 위치를 리턴한다.
                                        if (sentence.start.indexOf(':') > 0) {
                                            sentenceCfi = sentence.start.split(':')[0]
                                            sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                        }

                                        if (sentence.end.indexOf(':') > 0) {
                                            sentenceCfi = sentence.end.split(':')[0]
                                            sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                        }

                                        if (start != null && start != '' && start.indexOf(':') > 0) {
                                            StartCfi = start.split(':')[0]
                                            StartCfiOffset = parseInt(start.split(':')[1]);
                                        }

                                        if (end != null && end != '' && end.indexOf(':') > 0) {
                                            EndCfi = end.split(':')[0]
                                            EndCfiOffset = parseInt(end.split(':')[1]);
                                        }

                                        if (StartCfi == EndCfi && StartCfi == sentenceCfi) {
                                            if (StartCfiOffset >= sentenceStartCfiOffset && EndCfiOffset <= sentenceEndCfiOffset) {
                                                mSelectIndex = idx;

                                                mSelectStartCfi = sentence.start;
                                                mSelectEndCfi = sentence.end;
                                                ch = sentence.sentence;

                                                ch = sentence.sentence.substring((StartCfiOffset - sentenceStartCfiOffset), (EndCfiOffset - sentenceStartCfiOffset));

                                                val['res'] = 0;
                                                val['index'] = mSelectIndex;
                                                val['startCfi'] = sentence.start;
                                                val['endCfi'] = sentence.end;
                                                val['ch'] = ch;
                                                val['page'] = sentence.ed;
                                            }
                                            else {
                                                val['res'] = -4;
                                                mSelectIndex = -1;
                                                mSelectStartCfi = null;
                                                mSelectEndCfi = null;
                                                sentence = null;
                                            }
                                        }
                                        else {
                                            val['res'] = -5;
                                            mSelectIndex = -1;
                                            mSelectStartCfi = null;
                                            mSelectEndCfi = null;
                                            sentence = null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 다중 선택 값을 가져온다.
            // [ Param  ] none
            // [ Return ] 문장의 정보과 결과를 JSON으로 리턴한다.
            this.GetCurrentMultiSelectionInfo = function () {
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };

                if (mSelectInfo != null && mSelectInfo.length > 0) {
                    val.index = mSelectInfo[0].index;
                    val.startCfi = mSelectInfo[0].startCfi;
                    val.endCfi = mSelectInfo[mSelectInfo.length - 1].endCfi;
                    val.page = mSelectInfo[0].page;
                    val.ch = this.GetMultiSectionText();
                }
                else {
                    val.res = -1;
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 선택된 문장 정보를 리턴한다.
            // [ Param  ] none
            // [ Return ] 찾은 문장의 정보과 결과를 JSON으로 리턴한다.
            this.GetCurrentSelectionInfo = function () {
                var sentence = null;
                var val = { res: 0, index: -1, startCfi: '', endCfi: '', ch: '', page: -1 };
                var page = 0;
                var baseCfi = null;
                var baseCfiOffset = -1;
                var sentenceCfi = null;
                var sentenceEndCfiOffset = -1;
                var sentenceStartCfiOffset = -1;
                var ch = '';

                if (mInitPage == false) {
                    // 초기화 안됨.
                    val['res'] = -1;
                }
                else {

                    if (mSentenceList.length == 0) {
                        mSelectIndex = -1;
                        mSelectStartCfi = null;
                        mSelectEndCfi = null;
                        sentence = null;
                        val['res'] = -2;
                    }
                    else {

                        if (mSelectInfo != null && mSelectInfo.length > 0) {
                            mSelectIndex = mSelectInfo[0].index;
                            mSelectStartCfi = mSelectInfo[0].startCfi;
                            mSelectEndCfi = mSelectInfo[0].endCfi;

                            this.ClearMultiSelectInfo();
                        }

                        if (mSelectIndex == -1) {
                            sentence = mSentenceList[0];

                            val['res'] = 0;
                            val['index'] = 0;
                            val['startCfi'] = sentence.start;
                            val['endCfi'] = sentence.end;
                            val['ch'] = sentence.sentence;
                            val['page'] = sentence.ed;
                        }
                        else {

                            if ((sentence = mSentenceList[mSelectIndex]) == null) {
                                val['res'] = -3;
                                mSelectIndex = -1;
                                mSelectStartCfi = null;
                                mSelectEndCfi = null;
                                sentence = null;
                            }
                            else {

                                if (mSelectStartCfi == null) {
                                    //전체를 리턴한다.
                                    val['res'] = 0;
                                    val['index'] = 0;
                                    val['startCfi'] = sentence.start;
                                    val['endCfi'] = sentence.end;
                                    val['ch'] = sentence.sentence;
                                    val['page'] = sentence.ed;
                                }
                                else {
                                    // 특정 위치가 있을때는 특정 위치를 리턴한다.
                                    if (sentence.start.indexOf(':') > 0) {
                                        sentenceCfi = sentence.start.split(':')[0]
                                        sentenceStartCfiOffset = parseInt(sentence.start.split(':')[1]);
                                    }

                                    if (sentence.end.indexOf(':') > 0) {
                                        sentenceCfi = sentence.end.split(':')[0]
                                        sentenceEndCfiOffset = parseInt(sentence.end.split(':')[1]);
                                    }

                                    if (mSelectStartCfi.indexOf(':') > 0) {
                                        baseCfi = mSelectStartCfi.split(':')[0]
                                        baseCfiOffset = parseInt(mSelectStartCfi.split(':')[1]);
                                    }

                                    nPos = baseCfiOffset == -1 ? 0 : baseCfiOffset - sentenceStartCfiOffset;

                                    ch = sentence.sentence.substring(nPos, nPos + (sentenceEndCfiOffset - sentenceStartCfiOffset));

                                    val['res'] = 0;
                                    val['index'] = mSelectIndex;
                                    val['startCfi'] = mSelectStartCfi;
                                    val['endCfi'] = mSelectEndCfi;
                                    val['ch'] = ch;
                                    val['page'] = sentence.ed;
                                }
                            }
                        }
                    }
                }

                result = JSON.stringify(val);

                return result;
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 노드의 위치 정보로 페이지 정보를 리턴한다.
            // [ Param  ] node : 현재 노드
            // [ Return ] 분리된 페이지정보를 리턴한다.
            this.GetNodeStartEndPageInfo = function (node) {
                var rect = null;
                var pageinfo = { start: -1, end: -1 };
                var fstartDate = new Date();
                try {

                    rect = node.getBoundingClientRect();

                    if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                        pageinfo.start = parseInt(rect.top / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                        pageinfo.end = parseInt((rect.top + rect.height) / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                    }
                    else {
                        pageinfo.start = parseInt(rect.left / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                        pageinfo.end = parseInt((rect.left + rect.width) / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                    }

                    return pageinfo;
                }
                catch (ex) {
                    RunaEngine.log(ex);
                }
                finally {
                    fendDate = new Date();
                    fcalDate = fendDate - fstartDate;
                    RunaEngine.sublog2('GetNodeStartEndPageInfo, process time = ' + (fcalDate));
                    rect = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 현재 노드의 위치 정보로 페이지 정보를 오프셋과 함께 배열로 리턴한다.
            // [ Param  ] node : 현재 노드, startOffset : 시작 위치, endOffset : 마지막 위치
            // [ Return ] 분리된 페이지와 오프셋 정보를 배열로 리턴한다.
            this.GetStartEndPageInfo = function (node, startOffset, endOffset) {
                var range = null;
                var rect = null;
                var startPage = 0, endPage = 0;
                var tStartPage = 0, tEndPage = 0;
                var tStartOffset = startOffset;
                var rtnVal = new Array();
                var incOffset = startOffset;
                var fstartDate = new Date();
                try {
                    range = document.createRange();
                    range.setStart(node, startOffset);
                    range.setEnd(node, endOffset);

                    fsubstartdate = new Date();

                    rect = range.getBoundingClientRect();
                    range.detach();

                    fsubendDate = new Date();
                    fcalDate = fsubendDate - fsubstartdate;
                    // RunaEngine.sublog2('GetStartEndPageInfo, getBoundingClientRect, process time = ' + (fcalDate));

                    if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                        startPage = parseInt(rect.top / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                        endPage = parseInt((rect.top + rect.height) / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                    }
                    else {
                        startPage = parseInt(rect.left / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                        endPage = parseInt((rect.left + rect.width) / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                    }

                    if(RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount() == 1) {
                        startPage = 1;
                        endPage = startPage;
                    }

                    //RunaEngine.sublog2('[GetStartEndPageInfo] , PAGE : [' + startPage + ', ' + endPage + '], [' + tStartOffset + ', ' + endOffset + ']');
                    RunaEngine.log('[GetStartEndPageInfo] , PAGE : [' + startPage + ', ' + endPage + '], [' + tStartOffset + ', ' + endOffset + ']');

                    if (startPage != endPage) {

                        //incOffset++;

                        while (incOffset < endOffset) {

                            range.setStart(node, incOffset);
                            range.setEnd(node, incOffset + 1);

                            fsubstartdate = new Date();

                            rect = range.getBoundingClientRect();
                            range.detach();

                            fsubendDate = new Date();
                            fcalDate = fsubendDate - fsubstartdate;
                            // RunaEngine.sublog2('GetStartEndPageInfo, 22 getBoundingClientRect, process time = ' + (fcalDate));
                            // RunaEngine.log('GetStartEndPageInfo, 22 getBoundingClientRect, process time = ' + (fcalDate));

                            if (rect.width == 0) {
                                incOffset++;
                                continue;
                            }

                            if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                                tStartPage = parseInt(rect.top / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                                tEndPage = parseInt((rect.top + rect.height) / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                            }
                            else {
                                tStartPage = parseInt(rect.left / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                                tEndPage = parseInt((rect.left + rect.width) / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                            }

                            if (startPage != tStartPage) {

                                var pageinfo = { start: startPage, end: startPage, sp: tStartOffset, ed: incOffset };
                                rtnVal.push(pageinfo);

                                //RunaEngine.sublog2('[GetStartEndPageInfo] 11, PAGE : [' + startPage + ', ' + endPage + '], [' + tStartOffset + ', ' + incOffset + ']');
                                RunaEngine.log('[GetStartEndPageInfo] 11, PAGE : [' + startPage + ', ' + endPage + '], [' + tStartOffset + ', ' + incOffset + ']');

                                tStartOffset = incOffset;
                                startPage = tStartPage;
                            }

                            incOffset++;
                        }

                        if (tStartOffset < endOffset) {
                            var pageinfo = { start: startPage, end: startPage, sp: tStartOffset, ed: endOffset };
                            rtnVal.push(pageinfo);

                            //RunaEngine.sublog2('[GetStartEndPageInfo] , PAGE 22 : [' + startPage + ', ' + endPage + '], [' + tStartOffset + ', ' + endOffset + ']');
                            RunaEngine.log('[GetStartEndPageInfo] , PAGE 22 : [' + startPage + ', ' + endPage + '], [' + tStartOffset + ', ' + endOffset + ']');
                        }


                    }
                    else {
                        var pageinfo = { start: startPage, end: endPage, sp: startOffset, ed: endOffset };
                        rtnVal.push(pageinfo);
                    }

                    return rtnVal;
                }
                catch (ex) {
                    RunaEngine.log(ex);
                }
                finally {
                    fendDate = new Date();
                    fcalDate = fendDate - fstartDate;
                    RunaEngine.sublog2('GetStartEndPageInfo, process time = ' + (fcalDate));
                    startPage = null;
                    endPage = null;
                    rect = null;
                    range = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 상위 노드의 하위 텍스트 노드의 문장 정보를 만들어 리턴한다.
            // [ Param  ] parentnode : 현재 상위 노드
            // [ Return ] 분리된 문장, 페이지, 오프셋 정보를 배열로 리턴한다.
            this.GetAllSentence = function (parentnode) {

                var node = null;
                var arrSentence = new Array();
                var pageInfo = null;
                var parentStyle = null;
                var sentenceStyleType = '0000000';

                try {
                    node = parentnode.firstChild;

                    while (node != null) {
                        if (node == parentnode) {
                            break;
                        }

                        if (node.childNodes.length > 0) {
                            node = node.firstChild;
                            continue;
                        }

                        if (node.nodeType == 3) {

                            sentenceStyleType = '0000000';

                            tempTextData = node.textContent;
                            compareVal = AVDSentenceCompare(tempTextData);

                            startOffset = compareVal.start;
                            endOffset = compareVal.end;
                            textData = compareVal.s;

                            //find style
                            if(node.parentNode != null) {
                            	if(node.parentNode.nodeType == 1) {

                            		if (node.nodeName.toUpperCase() == 'U') {
                            			//underline
                            			sentenceStyleType = '100000';
                            		}
                                    else if (node.nodeName.toUpperCase() == 'INS') {
                                        //underline
                                        sentenceStyleType = '200000';
                                    }
                            		else if (node.nodeName.toUpperCase() == 'DEL') {
                            			//line-through
                            			sentenceStyleType = '010000';
                            		}
                                    else if (node.nodeName.toUpperCase() == 'STRIKE') {
                                        //line-through
                                        sentenceStyleType = '020000';
                                    }
                                    else if (node.nodeName.toUpperCase() == 'S') {
                                        //line-through
                                        sentenceStyleType = '030000';
                                    }
                            		else if (node.nodeName.toUpperCase() == 'I') {
                            			//italic
                            			sentenceStyleType = '001000';
                            		}
                            		else if (node.nodeName.toUpperCase() == 'EM') {
                            			sentenceStyleType = '002000';
                            		}
                            		else if (node.nodeName.toUpperCase() == 'B') {
                            			sentenceStyleType = '000100';
                            		}
                            		else if (node.nodeName.toUpperCase() == 'STRONG') {
                            			sentenceStyleType = '000200';
                            		}
                            		else if (node.nodeName.toUpperCase() == 'MARK') {
                            			sentenceStyleType = '000001';
                            		}
                            		else if (node.nodeName.toUpperCase() == 'SMALL') {
                            			sentenceStyleType = '000002';
                            		}
                            		else if (node.nodeName.toUpperCase() == 'SUB') {
                            			sentenceStyleType = '000003';
                            		}
                            		else if (node.nodeName.toUpperCase() == 'SUP') {
                            			sentenceStyleType = '000004';
                            		}
                                    else if (node.nodeName.toUpperCase() == 'CITE') {
                                        sentenceStyleType = '000005';
                                    }
                            		else {
                            			parentStyle = window.getComputedStyle(node.parentNode);

	                            		if(parentStyle != null) {

                                            if(parentStyle.fontStyle != null && parentStyle.fontStyle == 'italic') {
                                                sentenceStyleType = '003000';
                                            }

                                            if(parentStyle.fontWeight != null) {
                                                if(isNaN(parentStyle.fontWeight)) {
                                                    if(parentStyle.fontWeight == 'bold' || parentStyle.fontWeight == 'bolder') {
                                                        //bold
                                                        sentenceStyleType = '000300';
                                                    }
                                                }
                                                else {
                                                    //숫자
                                                    if(parseInt(parentStyle.fontWeight) >= 700) {
                                                        //bold
                                                        sentenceStyleType = '000300';
                                                    }
                                                }
                                            }

                                            if(parentStyle.textDecoration != null) {
                                                if(parentStyle.textDecoration == 'underline') {
                                                    sentenceStyleType = '300000';
                                                }
                                                if(parentStyle.textDecoration == 'overline') {
                                                    sentenceStyleType = '000010';
                                                }
                                                if(parentStyle.textDecoration == 'line-through') {
                                                    sentenceStyleType = '040000';
                                                }
                                            }
                                        }
                            		}
                            	}
                            }

                            RunaEngine.log('textData, startOffset = ' + startOffset + ', endOffset = ' + endOffset + ', now = ' + textData.length);

                            if (textData != null && textData.length > 0) {

                                nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                if (nodeCfi.indexOf(':') > 0) {
                                    nodeCfi = nodeCfi.split(':')[0];
                                }

                                var list = RunaEngine.HtmlCtrl.AVTTSEngine.BuildSentence(textData);

                                if (list != null && list.length > 0) {
                                    for (var i = 0; i < list.length; i++) {
                                        if (list[i].s.length == 0) continue;
                                        pageInfo = RunaEngine.HtmlCtrl.AVTTSEngine.GetStartEndPageInfo(node, startOffset + list[i].start, startOffset + list[i].end);
                                        RunaEngine.log('GetAllSentence pageInfo : node = ' + list[i].s + ', startOffset = ' + (startOffset + list[i].start) + ', endOffset = ' + (startOffset + list[i].end));
                                        val = list[i].s; //RunaEngine.Util.encodeHtmlEntity(list[i].s);

                                        if (pageInfo.length > 1) {
                                            // RunaEngine.sublog2('GetStartEndPageInfo 333, str = ' + list[i].s + ', startOffset = ' + (startOffset + list[i].start) + ', endOffset = ' + (startOffset + list[i].end));
                                            RunaEngine.log('getAllSenetence() GetStartEndPageInfo 333, str = ' + list[i].s + ', startOffset = ' + (startOffset + list[i].start) + ', endOffset = ' + (startOffset + list[i].end));

                                            for (var j = 0; j < pageInfo.length; j++) {

                                                var str = val.substring(pageInfo[j].sp - (startOffset + list[i].start), pageInfo[j].ed - (startOffset + list[i].start));

                                                // RunaEngine.sublog2('GetStartEndPageInfo 444, str = ' + str + ', startOffset = ' + startOffset + ', endOffset = ' + endOffset + ', now = ' + pageInfo[j].sp + ', now = ' + pageInfo[j].ed);
                                                RunaEngine.log('GetStartEndPageInfo 444, str = ' + str + ', startOffset = ' + startOffset + ', endOffset = ' + endOffset + ', now = ' + pageInfo[j].sp + ', now = ' + pageInfo[j].ed);

                                                var pageSentence = { cfi: nodeCfi, s: str, startoffset: pageInfo[j].sp, endoffset: pageInfo[j].ed, sp: pageInfo[j].start, ed: pageInfo[j].end, style: sentenceStyleType };
                                                arrSentence.push(pageSentence);
                                            }
                                        }
                                        else {
                                            var pageSentence = { cfi: nodeCfi, s: list[i].s, startoffset: startOffset + list[i].start, endoffset: startOffset + list[i].end, sp: pageInfo[0].start, ed: pageInfo[0].end, style: sentenceStyleType };
                                            RunaEngine.log('pageSentence : ' + pageSentence);
                                            arrSentence.push(pageSentence);
                                        }
                                    }
                                }
                            }
                        }

                        if (node.nextSibling != null) {
                            node = node.nextSibling;
                        }
                        else {
                            node = node.parentNode;

                            while (true) {
                                if (node == parentnode) {
                                    break;
                                }
                                else {
                                    if (node.nextSibling != null) {
                                        node = node.nextSibling;
                                        break;
                                    }
                                    else {
                                        node = node.parentNode;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (ex) {
                    RunaEngine.log('[★Exception★][GetAllSentence] ' + ex);
                    arrSentence = null;
                }
                finally {
                    pageInfo = null;
                    node = null;
                    startPage = null;
                    endPage = null;
                    rect = null;
                    range = null;
                    sentenceStyleType = null;
                    parentStyle = null;

                    return arrSentence;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 노드의 문장 정보를 만들어 리턴한다.
            // [ Param  ] parentnode : 현재 상위 노드
            // [ Return ] 분리된 문장, 페이지, 오프셋 정보를 배열로 리턴한다.
            this.GetNodeAllSentence = function (node) {

                var arrSentence = new Array();
                var pageInfo = null;
                var parentStyle = null;
                var sentenceStyleType = '0000000';
                var fstartDate = new Date();

                try {
                    if (node.nodeType == 3) {

                        tempTextData = node.textContent;
                        compareVal = AVDSentenceCompare(tempTextData);

                        startOffset = compareVal.start;
                        endOffset = compareVal.end;
                        textData = compareVal.s;


                        //find style
                        if(node.parentNode != null) {
                        	if(node.parentNode.nodeType == 1) {

                        		if (node.nodeName.toUpperCase() == 'U') {
                                    //underline
                                    sentenceStyleType = '100000';
                                }
                                else if (node.nodeName.toUpperCase() == 'INS') {
                                    //underline
                                    sentenceStyleType = '200000';
                                }
                                else if (node.nodeName.toUpperCase() == 'DEL') {
                                    //line-through
                                    sentenceStyleType = '010000';
                                }
                                else if (node.nodeName.toUpperCase() == 'STRIKE') {
                                    //line-through
                                    sentenceStyleType = '020000';
                                }
                                else if (node.nodeName.toUpperCase() == 'S') {
                                    //line-through
                                    sentenceStyleType = '030000';
                                }
                                else if (node.nodeName.toUpperCase() == 'I') {
                                    //italic
                                    sentenceStyleType = '001000';
                                }
                                else if (node.nodeName.toUpperCase() == 'EM') {
                                    sentenceStyleType = '002000';
                                }
                                else if (node.nodeName.toUpperCase() == 'B') {
                                    sentenceStyleType = '000100';
                                }
                                else if (node.nodeName.toUpperCase() == 'STRONG') {
                                    sentenceStyleType = '000200';
                                }
                                else if (node.nodeName.toUpperCase() == 'MARK') {
                                    sentenceStyleType = '000001';
                                }
                                else if (node.nodeName.toUpperCase() == 'SMALL') {
                                    sentenceStyleType = '000002';
                                }
                                else if (node.nodeName.toUpperCase() == 'SUB') {
                                    sentenceStyleType = '000003';
                                }
                                else if (node.nodeName.toUpperCase() == 'SUP') {
                                    sentenceStyleType = '000004';
                                }
                                else if (node.nodeName.toUpperCase() == 'CITE') {
                                    sentenceStyleType = '000005';
                                }
                                else {
                                    parentStyle = window.getComputedStyle(node.parentNode);

                                    if(parentStyle != null) {

                                        if(parentStyle.fontStyle != null && parentStyle.fontStyle == 'italic') {
                                            sentenceStyleType = '003000';
                                        }

                                        if(parentStyle.fontWeight != null) {
                                            if(isNaN(parentStyle.fontWeight)) {
                                                if(parentStyle.fontWeight == 'bold' || parentStyle.fontWeight == 'bolder') {
                                                    //bold
                                                    sentenceStyleType = '000300';
                                                }
                                            }
                                            else {
                                                //숫자
                                                if(parseInt(parentStyle.fontWeight) >= 700) {
                                                    //bold
                                                    sentenceStyleType = '000300';
                                                }
                                            }
                                        }

                                        if(parentStyle.textDecoration != null) {
                                            if(parentStyle.textDecoration == 'underline') {
                                                sentenceStyleType = '300000';
                                            }
                                            if(parentStyle.textDecoration == 'overline') {
                                                sentenceStyleType = '000010';
                                            }
                                            if(parentStyle.textDecoration == 'line-through') {
                                                sentenceStyleType = '040000';
                                            }
                                        }
                                    }
                                }
                        	}
                        }

                        RunaEngine.log('textData, startOffset = ' + startOffset + ', endOffset = ' + endOffset + ', now = ' + textData.length);

                        if (textData != null && textData.length > 0) {

                            nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");
                            RunaEngine.log('nodeCfi before : ' + nodeCfi);
                            if (nodeCfi.indexOf(':') > 0) {
                                nodeCfi = nodeCfi.split(':')[0];
                            }
                            RunaEngine.log('nodeCfi after : ' + nodeCfi);
                            var list = RunaEngine.HtmlCtrl.AVTTSEngine.BuildSentence(textData);

                            if (list != null && list.length > 0) {
                                for (var i = 0; i < list.length; i++) {
                                    if (list[i].s.length == 0) continue;
                                    pageInfo = RunaEngine.HtmlCtrl.AVTTSEngine.GetStartEndPageInfo(node, startOffset + list[i].start, startOffset + list[i].end);
                                    RunaEngine.log('GetNodeAllSentence pageInfo : node = ' + list[i].s + ', startOffset = ' + (startOffset + list[i].start) + ', endOffset = ' + (startOffset + list[i].end));


                                    val = list[i].s; //RunaEngine.Util.encodeHtmlEntity(list[i].s);

                                    // ios - jdm
                                    // val = AVDRemoveWhiteSpace(list[i].s);
                                    // if (RunaEngine.DeviceInfo.IsIOS) {
                                    //     val = val.replace(/^\s/gm, '');
                                    // }

                                    if (pageInfo.length > 1) {
                                        //RunaEngine.sublog2('GetStartEndPageInfo 333, str = ' + list[i].s + ', startOffset = ' + (startOffset + list[i].start) + ', endOffset = ' + (startOffset + list[i].end));
                                        RunaEngine.log('getNodeAllSentence() GetStartEndPageInfo 333, str = ' + list[i].s + ', startOffset = ' + (startOffset + list[i].start) + ', endOffset = ' + (startOffset + list[i].end));

                                        for (var j = 0; j < pageInfo.length; j++) {
                                            RunaEngine.log('pageInfo[j].sp :' + pageInfo[j].sp + ', list[i].start : ' + list[i].start);
                                            RunaEngine.log('pageInfo[j].ed :' + pageInfo[j].ed + ', list[i].end : ' + list[i].end);

                                            // ios - jdm

                                            // if (RunaEngine.DeviceInfo.IsIOS) {
                                            //     var str = null;
                                            //     var pgsp = pageInfo[j].sp;
                                            //     var startOffsetListStart = startOffset + list[i].start;
                                            //     if ((pgsp - startOffsetListStart) == 0) {
                                            //         RunaEngine.log('jdm == 0 pgsp : ' + pgsp + ', startOffsetListStart : ' + startOffsetListStart);
                                            //         str = val.substring(pageInfo[j].sp - (startOffsetListStart), (pageInfo[j].ed - 1) - (startOffsetListStart));
                                            //     } else {
                                            //         RunaEngine.log('jdm pgsp ' + pgsp + 'startOffsetListStart' + startOffsetListStart);
                                            //         str = val.substring((pageInfo[j].sp - 1) - (startOffset + list[i].start), pageInfo[j].ed - (startOffset + list[i].start));
                                            //     }
                                            // } else {
                                                var str = val.substring(pageInfo[j].sp - (startOffset + list[i].start), pageInfo[j].ed - (startOffset + list[i].start));
                                            // }

                                            // var str = val.substring(pageInfo[j].sp - (startOffset + list[i].start), pageInfo[j].ed - (startOffset + list[i].start));

                                            //RunaEngine.sublog2('GetStartEndPageInfo 444, str = ' + str + ', startOffset = ' + startOffset + ', endOffset = ' + endOffset + ', now = ' + pageInfo[j].sp + ', now = ' + pageInfo[j].ed);
                                            RunaEngine.log('GetStartEndPageInfo 444, str = ' + str + ', startOffset = ' + startOffset + ', endOffset = ' + (startOffset + list[i].end) + ', now = ' + pageInfo[j].sp + ', now = ' + pageInfo[j].ed);


                                            var pageSentence = null;
                                            // ios - jdm
                                            // if (RunaEngine.DeviceInfo.IsIOS) {
                                            //     RunaEngine.log("jdm str.length " + str + ", str.length " + str.length);
                                            //
                                            //     pageSentence = {
                                            //         cfi: nodeCfi,
                                            //         s: str,
                                            //         startoffset: pageInfo[j].sp,
                                            //         endoffset: pageInfo[j].ed,
                                            //         sp: pageInfo[j].start,
                                            //         ed: pageInfo[j].end,
                                            //         style: sentenceStyleType
                                            //     };
                                            // } else {
                                                pageSentence = {
                                                    cfi: nodeCfi,
                                                    s: str,
                                                    startoffset: pageInfo[j].sp,
                                                    endoffset: pageInfo[j].ed,
                                                    sp: pageInfo[j].start,
                                                    ed: pageInfo[j].end,
                                                    style: sentenceStyleType
                                                };
                                            // }
                                            RunaEngine.log('pageSentence cfi : ' + pageSentence.cfi + ', s : ' + pageSentence.s + ', startoffset : ' + pageSentence.startoffset + ', endoffset : ' + pageSentence.endoffset + ', sp : ' + pageSentence.sp + ', ed : ' + pageSentence.ed);
                                            arrSentence.push(pageSentence);
                                        }
                                    }
                                    else {
                                        var pageSentence = { cfi: nodeCfi, s: list[i].s, startoffset: startOffset + list[i].start, endoffset: startOffset + list[i].end, sp: pageInfo[0].start, ed: pageInfo[0].end, style: sentenceStyleType };
                                        RunaEngine.log('pageSentence cfi : ' + pageSentence.cfi + ', s : ' + pageSentence.s + ', startoffset : ' + pageSentence.startoffset + ', endoffset : ' + pageSentence.endoffset + ', sp : ' + pageSentence.sp + ', ed : ' + pageSentence.ed);
                                        arrSentence.push(pageSentence);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (ex) {
                    RunaEngine.log('[★Exception★][GetNodeAllSentence] ' + ex);
                    arrSentence = null;
                }
                finally {
                    pageInfo = null;
                    node = null;
                    startPage = null;
                    endPage = null;
                    rect = null;
                    range = null;
                    sentenceStyleType = null;
                    parentStyle = null;

                    fendDate = new Date();
                    fcalDate = fendDate - fstartDate;
                    RunaEngine.sublog2('GetNodeAllSentence, process time = ' + (fcalDate));

                    return arrSentence;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 특정 노드의  html 소스 코드를 리턴한다.
            // [ Param  ] idx : 문장 번호
            // [ Return ] 문자열 , html 소스 코드
            this.GetHtmlData = function(idx) {
                var sentence = null;
                var rtnVal = '';

                try {

                    if (mSentenceList.length == 0) {
                        return '';
                    }

                    if ((sentence = mSentenceList[idx]) != null) {
                        if(sentence.ele != null) {
                            if(sentence.ele.nodeType == 1)
                                rtnVal = sentence.ele.outerHTML;
                        }
                    }
                    return rtnVal;
                }
                catch (ex) {
                    RunaEngine.sublog2('[★Exception★][GetHtmlData] ' + ex);

                    rtnVal = null;
                    return rtnVal;
                }
                finally {
                    sentence = null;
                    rtnVal = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 문장 데이터를 배열에 넣는다.
            // [ Param  ] 0
            // [ Return ] none
            this.AddSentence = function (sentenceType, isDraw, startPosition, endPosition, sentenceValue, idValue, hrefValue, startPage, endPage, idx, textStyle) {

                try {
                    var pageSentence = { type: sentenceType, draw: isDraw, start: startPosition, end: endPosition, sentence: sentenceValue, id: idValue, href: hrefValue, sp: startPage, ed: endPage, index: idx, style: textStyle };
                    mSentenceList.push(pageSentence);
                }
                catch (ex) {
                    RunaEngine.sublog2('[★Exception★][AddSentence] ' + ex);
                }

            }
            this.AddTableSentenceWithNode = function (sentenceType, isDraw, startPosition, endPosition, sentenceValue, idValue, hrefValue, tableidValue, startPage, endPage, idx, textStyle, node) {

                try {
                    var pageSentence = { type: sentenceType, draw: isDraw, start: startPosition, end: endPosition, sentence: sentenceValue, id: idValue, href: hrefValue, tableid: tableidValue, sp: startPage, ed: endPage, index: idx, style: textStyle, ele: node };
                    mSentenceList.push(pageSentence);
                }
                catch (ex) {
                    RunaEngine.sublog2('[★Exception★][AddTableSentenceWithNode] ' + ex);
                }

            }
            this.AddTableSentence = function (sentenceType, isDraw, startPosition, endPosition, sentenceValue, idValue, hrefValue, tableidValue, startPage, endPage, cellNumber, rowNumber, cellExtraNumber, rowExtraNumber, textStyle) {

                try {
                    var pageSentence = { type: sentenceType, draw: isDraw, start: startPosition, end: endPosition, sentence: sentenceValue, id: idValue, href: hrefValue, tableid: tableidValue, sp: startPage, ed: endPage, index: -1, col: cellNumber, row: rowNumber, colex: cellExtraNumber, rowex: rowExtraNumber, style: textStyle };
                    mSentenceList.push(pageSentence);
                }
                catch (ex) {
                    RunaEngine.sublog2('[★Exception★][AddTableSentence] ' + ex);
                }

            }

            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 상위 테이블 노드의 하위 텍스트 노드의 문장 정보를 만들어 리턴한다.
            // [ Param  ] parentnode : 현재 상위 노드
            // [ Return ] 분리된 문장, 페이지, 오프셋 정보를 배열로 리턴한다.
            this.MakeTableSentence = function (parentnode, tableId) {
                var iterator = null;
                var node = null;
                var rowIdx = -1;
                var cellIdx = 0;
                var cellExIdx = 0;
                var rowExIdx = -1;

                var node = null;
                var tableMap = new RunaEngineMap();
                var tabledataKey = '';
                var startPage = 0, endPage = 0;
                var range = null;
                var rect = null;
                var aria = '';
                var paraNodeNext = null;
                var headlineNodeNext = null;
                var figcaptionNodeNext = null;
                var tablecaptionNodeNext = null;
                var asideNodeNext = null;
                var thtdNodeNext = null;

                var ttsTagType = AVTTSTagType.NONE;
                var ttsTableTagType = AVTTSTableTagType.NONE;


                iterator = document.createTreeWalker(parentnode, NodeFilter.SHOW_ALL, AVDEleCheck, false);
                node = iterator.firstChild();

                while (node) {

                    nodeCfiOffset = 0;

                    if (node.nodeType == 1) {

                        id = AVDGetAttrValue(node, 'id');
                        if (id == null || id == undefined)
                            id = '';

                        range = document.createRange();
                        range.selectNode(node);
                        rect = range.getBoundingClientRect();
                        range.detach();


                        if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                            startPage = parseInt(rect.top / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                            endPage = parseInt((rect.top + rect.height) / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                        }
                        else {
                            startPage = parseInt(rect.left / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                            endPage = parseInt((rect.left + rect.width) / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                        }

                        if(RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount() == 1) {
                            startPage = 1;
                            endPage = startPage;
                        }

                        if (paraNodeNext != null && node == paraNodeNext) {

                            paraNodeNext = null;

                            RunaEngine.log('P, End Paragraph');

                            this.AddSentence('RETURN', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                        }


                        if (figcaptionNodeNext != null && node == figcaptionNodeNext) {
                            figcaptionNodeNext = null;

                            this.AddTableSentence('TABLE_IMG_DESC_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            ttsTagType = AVTTSTagType.NONE;
                        }

                        if (asideNodeNext != null && node == asideNodeNext) {
                            asideNodeNext = null;

                            this.AddTableSentence('TABLE_ASIDE_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            ttsTagType = AVTTSTagType.NONE;
                        }

                        if (tablecaptionNodeNext != null && node == tablecaptionNodeNext) {
                            tablecaptionNodeNext = null;

                            this.AddTableSentence('TABLE_CAPTION_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            ttsTagType = AVTTSTagType.NONE;
                        }

                        if (headlineNodeNext != null && node == headlineNodeNext) {
                            headlineNodeNext = null;
                            ttsTagType = AVTTSTagType.NONE;
                        }

                        if (thtdNodeNext != null && node == thtdNodeNext) {
                            thtdNodeNext = null;
                            ttsTableTagType = AVTTSTagType.NONE;
                            cellIdx++;
                        }


                        //태그에서 처리는 앵커 태그만 처리하고 나머지는 텍스트 노드에서 결합해서 처리 한다.
                        if (node.nodeName.toUpperCase() == 'CAPTION') {

                            textData = node.textContent;
                            textData = AVDRemoveWhiteSpace(textData);

                            if (textData != null && textData.length > 0) {

                                this.AddTableSentence('TABLE_CAPTION_START', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                ttsTagType = AVTTSTagType.TABLECAPTION;

                                tablecaptionNodeNext = node.nextElementSibling;

                                if (tablecaptionNodeNext == null) {
                                    tablecaptionNodeNext = node.parentNode;
                                    if (tablecaptionNodeNext != null)
                                        tablecaptionNodeNext = tablecaptionNodeNext.nextElementSibling;
                                }
                                RunaEngine.sublog2('table caption = ' + (tablecaptionNodeNext != null ? tablecaptionNodeNext.nodeName : 'null'));
                            }
                        }
                        else if (node.nodeName.toUpperCase() == 'THEAD' || node.nodeName.toUpperCase() == 'TBODY') {
                        }
                        else if (node.nodeName.toUpperCase() == 'TR') {
                            rowIdx++;
                            cellIdx = 0;
                            //셀 번호를 초기화 한다.
                        }
                        else if (node.nodeName.toUpperCase() == 'TH') {
                            //rowspan, colspan
                            rowspanCnt = AVDGetAttrValue(node, 'rowspan');
                            colspanCnt = AVDGetAttrValue(node, 'colspan');

                            if (rowspanCnt != null && rowspanCnt != '')
                                rowspanCnt = parseInt(rowspanCnt);
                            else rowspanCnt = 0;
                            if (colspanCnt != null && colspanCnt != '')
                                colspanCnt = parseInt(colspanCnt);
                            else colspanCnt = 0;

                            while (true) {

                                tabledataKey = rowIdx + '..' + cellIdx;

                                if (!tableMap.containsKey(tabledataKey)) {
                                    tableMap.put(tabledataKey, 'aa');
                                    break;
                                }

                                cellIdx++;
                            }

                            cellExIdx = cellIdx;
                            rowExIdx = rowIdx;

                            if (rowspanCnt > 0) {
                                for (var i = 1; i < rowspanCnt; i++) {
                                    tabledataKey = (rowIdx + i) + '..' + cellIdx;
                                    tableMap.put(tabledataKey, 'aa');
                                    rowExIdx = (rowIdx + i);
                                }
                            }

                            if (colspanCnt > 0) {
                                for (var i = 1; i < colspanCnt; i++) {
                                    tabledataKey = rowIdx + '..' + (cellIdx + i);
                                    tableMap.put(tabledataKey, 'aa');
                                    cellExIdx = (cellIdx + i);
                                }
                            }

                            textData = node.textContent;
                            textData = AVDRemoveWhiteSpace(textData);

                            if (textData != null && textData.length > 0) {

                                ttsTableTagType = AVTTSTableTagType.TH;

                                thtdNodeNext = node.nextElementSibling;

                                if (thtdNodeNext == null) {
                                    thtdNodeNext = node.parentNode;
                                    if (thtdNodeNext != null)
                                        thtdNodeNext = thtdNodeNext.nextElementSibling;
                                }
                            }
                            else {
                                nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                if (nodeCfi.indexOf(':') > 0) {
                                    nodeCfi = nodeCfi.split(':')[0];
                                }

                                var pageInfo = this.GetNodeStartEndPageInfo(node);

                                this.AddTableSentence('TABLE_NO_DATA', 'Y', nodeCfi, nodeCfi, '빈 셀', id, '', tableId, pageInfo.start, pageInfo.end, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            }

                            //cellIdx++;
                        }
                        else if (node.nodeName.toUpperCase() == 'TD') {
                            //rowspan, colspan
                            rowspanCnt = AVDGetAttrValue(node, 'rowspan');
                            colspanCnt = AVDGetAttrValue(node, 'colspan');

                            if (rowspanCnt != null && rowspanCnt != '')
                                rowspanCnt = parseInt(rowspanCnt);
                            else rowspanCnt = 0;
                            if (colspanCnt != null && colspanCnt != '')
                                colspanCnt = parseInt(colspanCnt);
                            else colspanCnt = 0;

                            while (true) {

                                tabledataKey = rowIdx + '..' + cellIdx;

                                if (!tableMap.containsKey(tabledataKey)) {
                                    tableMap.put(tabledataKey, 'aa');
                                    break;
                                }

                                cellIdx++;
                            }

                            cellExIdx = cellIdx;
                            rowExIdx = rowIdx;

                            if (rowspanCnt > 0) {
                                for (var i = 1; i < rowspanCnt; i++) {
                                    tabledataKey = (rowIdx + i) + '..' + cellIdx;
                                    tableMap.put(tabledataKey, 'aa');
                                    rowExIdx = (rowIdx + i);
                                }
                            }

                            if (colspanCnt > 0) {
                                for (var i = 1; i < colspanCnt; i++) {
                                    tabledataKey = rowIdx + '..' + (cellIdx + i);
                                    tableMap.put(tabledataKey, 'aa');
                                    cellExIdx = (cellIdx + i);
                                }
                            }

                            textData = node.textContent;
                            textData = AVDRemoveWhiteSpace(textData);

                            if (textData != null && textData.length > 0) {
                                //
                                ttsTableTagType = AVTTSTableTagType.TD;

                                thtdNodeNext = node.nextElementSibling;

                                if (thtdNodeNext == null) {
                                    thtdNodeNext = node.parentNode;
                                    if (thtdNodeNext != null)
                                        thtdNodeNext = thtdNodeNext.nextElementSibling;
                                }
                            }
                            else {
                                nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                if (nodeCfi.indexOf(':') > 0) {
                                    nodeCfi = nodeCfi.split(':')[0];
                                }

                                var pageInfo = this.GetNodeStartEndPageInfo(node);

                                this.AddTableSentence('TABLE_NO_DATA', 'Y', nodeCfi, nodeCfi, '빈 셀', id, '', tableId, pageInfo.start, pageInfo.end, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            }

                            //cellIdx++;
                        }
                        else if (node.nodeName.toUpperCase() == 'P') {
                            RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);

                            textData = node.textContent;
                            textData = AVDRemoveWhiteSpace(textData);

                            this.AddSentence('PARAGRAPH', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');
                            mParagraphMap.put(mParagraphIdx++, mSentenceList.length - 1);

                            if (textData == null || textData == '' || textData.length == 0) {
                                RunaEngine.log('P, Blank Line');
                                this.AddSentence('BLANK_LINE', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');
                            }

                            if (id != '') {
                                mIdMap.put(id, mSentenceList.length - 1);
                            }

                            paraNodeNext = node.nextElementSibling;

                            if (paraNodeNext == null) {
                                paraNodeNext = node.parentNode;
                                if (paraNodeNext != null)
                                    paraNodeNext = paraNodeNext.nextElementSibling;
                            }
                            //RunaEngine.sublog2('paragraph = ' + (paraNodeNext != null ? paraNodeNext.nodeName : 'null'));
                        }
                        else if (node.nodeName.toUpperCase() == 'IMG') {
                            aria = AVDGetAttrValue(node, 'aria-describedby');
                            val = AVDGetAttrValue(node, 'alt');

                            if ((aria != null && aria != '') || (val != null && val.length > 0)) {

                                //val = RunaEngine.Util.encodeHtmlEntity(val);
                                val = decodeHTMLEntities(val);

                                this.AddTableSentence('TABLE_IMG_START', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                RunaEngine.log('TABLE_IMG - ALT, name = ' + node.nodeName + ', type = ' + node.nodeType + ', text = ' + val);

                                this.AddTableSentence('TABLE_IMG', 'Y', nodeCfi, nodeCfi, val, id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                                this.AddTableSentence('TABLE_IMG_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            }
                        }
                        else if (node.nodeName.toUpperCase() == 'FIGURE') {
                            if (id != '') {
                                mIdMap.put(id, mSentenceList.length - 1);
                            }
                        }
                        else if (node.nodeName.toUpperCase() == 'FIGCAPTION') {
                            if (id != null && id != undefined && id == aria) {

                                this.AddTableSentence('TABLE_IMG_DESC_START', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                ttsTagType = AVTTSTagType.FIGCAPTION;

                                figcaptionNodeNext = node.nextElementSibling;

                                if (figcaptionNodeNext == null) {
                                    figcaptionNodeNext = node.parentNode;
                                    if (figcaptionNodeNext != null)
                                        figcaptionNodeNext = figcaptionNodeNext.nextElementSibling;
                                }
                                RunaEngine.sublog2('table figcaption = ' + (figcaptionNodeNext != null ? figcaptionNodeNext.nodeName : 'null'));
                            }

                            RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);
                        }
                        else if (node.nodeName.toUpperCase() == 'ASIDE') {
                            this.AddTableSentence('TABLE_ASIDE_START', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                            if (id != '') {
                                mIdMap.put(id, mSentenceList.length - 1);
                            }

                            ttsTagType = AVTTSTagType.ASIDE;

                            asideNodeNext = node.nextElementSibling;

                            if (asideNodeNext == null) {
                                asideNodeNext = node.parentNode;
                                if (asideNodeNext != null)
                                    asideNodeNext = asideNodeNext.nextElementSibling;
                            }
                            RunaEngine.sublog2('table aside = ' + (asideNodeNext != null ? asideNodeNext.nodeName : 'null'));
                        }
                        else if (node.nodeName.toUpperCase() == 'BR') {
                            this.AddSentence('RETURN', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                        }
                        else if (node.nodeName.toUpperCase() == 'A') {
                            href = AVDGetAttrValue(node, 'href');
                            //RunaEngine.log('--- A , id = ' + id + ', href = ' + href + ', text = ' + node.textContent);

                            this.AddTableSentence('TABLE_ANCHOR_START', 'N', '', '', '', id, href, tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                            if (id != '') {
                                mIdMap.put(id, mSentenceList.length - 1);
                            }

                            nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                            if (nodeCfi.indexOf(':') > 0) {
                                nodeCfi = nodeCfi.split(':')[0];
                            }

                            // RunaEngine.Util.encodeHtmlEntity(node.textContent);

                            textData = node.textContent;
                            textData = AVDRemoveWhiteSpace(textData);

                            if (textData != null && textData.length > 0) {
                                var list = RunaEngine.HtmlCtrl.AVTTSEngine.GetAllSentence(node);

                                if (list != null && list.length > 0) {
                                    for (var i = 0; i < list.length; i++) {
                                        if (list[i].s.length == 0) continue;
                                        //val = RunaEngine.Util.encodeHtmlEntity(list[i].s);
                                        this.AddTableSentence('TABLE_ANCHOR_TEXT', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, id, href, tableId, list[i].sp, list[i].ed, cellIdx, rowIdx, cellExIdx, rowExIdx, list[i].style);
                                    }
                                }
                            }

                            this.AddTableSentence('TABLE_ANCHOR_END', 'N', '', '', '', id, href, tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            this.AddTableSentence('TABLE_ANCHOR_DESC_START', 'N', '', '', '', id, href, tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            this.AddTableSentence('TABLE_ANCHOR_DESC_TEXT', 'N', '', '', '', id, href, tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                            this.AddTableSentence('TABLE_ANCHOR_DESC_END', 'N', '', '', '', id, href, tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                            node = iterator.nextSibling();
                            while (node == null) {
                                node = iterator.parentNode();

                                //sjhan , 210617 , 다음 노드가 parent일 때 while문 탈출
                                if (node == parentnode) {
                                    node = null;
                                    break;
                                }

                                if (node != null)
                                    node = iterator.nextSibling();
                            }
                            continue;
                        }
                        else if (node.nodeName.toUpperCase() == 'SPAN') {
                            epub_type = AVDGetAttrValue(node, 'epub:type');
                            RunaEngine.log('name = ' + node.nodeName + ', epub:type = ' + epub_type + ', text = ' + node.textContent);

                            if (epub_type == "pagebreak") {

                                textData = node.textContent;
                                textData = AVDRemoveWhiteSpace(textData);

                                if (textData != null && textData.length > 0) {
                                    var list = RunaEngine.HtmlCtrl.AVTTSEngine.GetAllSentence(node);

                                    if (list != null && list.length > 0) {
                                        for (var i = 0; i < list.length; i++) {
                                            if (list[i].s.length == 0) continue;
                                            //val = RunaEngine.Util.encodeHtmlEntity(list[i].s);
                                            this.AddTableSentence('TABLE_PAPER_PAGE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, id, '', tableId, list[i].sp, list[i].ed, cellIdx, rowIdx, cellExIdx, rowExIdx, list[i].style);
                                        }
                                    }
                                }

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                node = iterator.nextSibling();
                                while (node == null) {
                                    node = iterator.parentNode();

                                    //sjhan , 210617 , 다음 노드가 parent일 때 while문 탈출
		                            if (node == parentnode) {
		                                node = null;
		                                break;
		                            }

                                    if (node != null)
                                        node = iterator.nextSibling();
                                }
                                continue;
                            }
                            else {
                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }
                            }
                        }
                        else if (node.nodeName.toUpperCase() == 'H1' || node.nodeName.toUpperCase() == 'H2' || node.nodeName.toUpperCase() == 'H3' || node.nodeName.toUpperCase() == 'H4' || node.nodeName.toUpperCase() == 'H5' || node.nodeName.toUpperCase() == 'H6') {
                            RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);
                            this.AddSentence('HEADLINE', 'N', '', '', node.nodeName.toUpperCase(), id, '', startPage, endPage, mParagraphIdx, '000000');

                            if (id != '') {
                                mIdMap.put(id, mSentenceList.length - 1);
                            }

                            ttsTagType = AVTTSTagType.HEADLINE;

                            headlineNodeNext = node.nextElementSibling;

                            if (headlineNodeNext == null) {
                                headlineNodeNext = node.parentNode;
                                if (headlineNodeNext != null)
                                    headlineNodeNext = headlineNodeNext.nextElementSibling;
                            }
                            RunaEngine.sublog2('table HEADLINE = ' + (headlineNodeNext != null ? headlineNodeNext.nodeName : 'null'));

                            this.AddSentence('PARAGRAPH', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');

                            mParagraphMap.put(mParagraphIdx++, mSentenceList.length - 1);
                        }
                        else if (node.nodeName.toUpperCase() == 'MATH') {
                            alttext = AVDGetAttrValue(node, 'alttext');

                            nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                            textData = node.textContent;
                            textData = AVDRemoveWhiteSpace(textData);

                            if (nodeCfi.indexOf(':') > 0) {
                                nodeCfi = nodeCfi.split(':')[0];
                            }
                            //alttext = RunaEngine.Util.encodeHtmlEntity(alttext);
                            alttext = decodeHTMLEntities(alttext);

                            this.AddTableSentence('TABLE_MATH_START', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                            if (id != '') {
                                mIdMap.put(id, mSentenceList.length - 1);
                            }

                            this.AddTableSentence('TABLE_MATH_TEXT', 'Y', nodeCfi + ':0', nodeCfi + ':' + textData.length, alttext, id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                            this.AddTableSentence('TABLE_MATH_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');

                            node = iterator.nextSibling();
                            while (node == null) {
                                node = iterator.parentNode();

                                //sjhan , 210617 , 다음 노드가 parent일 때 while문 탈출
                                if (node == parentnode) {
                                    node = null;
                                    break;
                                }

                                if (node != null)
                                    node = iterator.nextSibling();
                            }
                            continue;
                        }
                        else if (node.nodeName.toUpperCase() == 'TABLE') {
                            RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);

                            nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                            this.AddTableSentenceWithNode('TABLE_START', 'N', nodeCfi, nodeCfi, '', id, '', id, startPage, endPage, -1, '000000', node);

                            if (id != '') {
                                mIdMap.put(id, mSentenceList.length - 1);
                            }

                            this.AddSentence('PARAGRAPH', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');

                            mParagraphMap.put(mParagraphIdx++, mSentenceList.length - 1);
                            mTableMap.put(mTableIdx++, mSentenceList.length - 1);

                            this.MakeTableSentence(node, id);

                            this.AddSentence('TABLE_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');

                            node = iterator.nextSibling();
                            while (node == null) {
                                node = iterator.parentNode();

                                //sjhan , 210617 , 다음 노드가 parent일 때 while문 탈출
                                if (node == parentnode) {
                                    node = null;
                                    break;
                                }

                                if (node != null)
                                    node = iterator.nextSibling();
                            }
                            continue;
                        }
                    }
                    else if (node.nodeType == 3) {
                        //text node
                        textData = node.textContent;
                        textData = AVDRemoveWhiteSpace(textData);

                        if (textData != null && textData.length > 0) {

                            var list = RunaEngine.HtmlCtrl.AVTTSEngine.GetNodeAllSentence(node);

                            if (list != null && list.length > 0) {
                                for (var i = 0; i < list.length; i++) {
                                    if (list[i].s.length == 0) continue;
                                    //val = RunaEngine.Util.encodeHtmlEntity(list[i].s);
                                    if (ttsTagType == AVTTSTagType.HEADLINE) {
                                        this.AddTableSentence('TABLE_HEADLINE_TEXT', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, tableId, '', tableId, list[i].sp, list[i].ed, cellIdx, rowIdx, cellExIdx, rowExIdx, list[i].style);
                                    }
                                    else if (ttsTagType == AVTTSTagType.FIGCAPTION) {
                                        this.AddTableSentence('TABLE_IMG_DESC_TEXT_LINE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, tableId, '', tableId, list[i].sp, list[i].ed, cellIdx, rowIdx, cellExIdx, rowExIdx, list[i].style);
                                    }
                                    else if (ttsTagType == AVTTSTagType.ASIDE) {
                                        this.AddTableSentence('TABLE_ASIDE_TEXT_LINE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, tableId, '', tableId, list[i].sp, list[i].ed, cellIdx, rowIdx, cellExIdx, rowExIdx, list[i].style);
                                    }
                                    else {
                                        this.AddTableSentence('TABLE_TEXT_LINE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, tableId, '', tableId, list[i].sp, list[i].ed, cellIdx, rowIdx, cellExIdx, rowExIdx, list[i].style);
                                    }
                                }
                            }
                        }
                    }

                    node = iterator.nextNode();
                }//while

                if (ttsTagType == AVTTSTagType.FIGCAPTION) {
                    this.AddTableSentence('TABLE_IMG_DESC_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                }
                else if (ttsTagType == AVTTSTagType.ASIDE) {
                    this.AddTableSentence('TABLE_ASIDE_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                }
                else if (ttsTagType == AVTTSTagType.TABLECAPTION) {
                    this.AddTableSentence('TABLE_CAPTION_END', 'N', '', '', '', id, '', tableId, startPage, endPage, cellIdx, rowIdx, cellExIdx, rowExIdx, '000000');
                }

                if (tableMap != null) {
                    tableMap.clear();
                    tableMap = null;
                }
            }
            ///////////////////////////////////////////////////////////////////////
            // [ Desc   ] 해당 파일의 TTS 데이터를 생성한다.
            // [ Param  ] 0
            // [ Return ] none
            this.initialize = function (side) {
                var iterator = null;
                var node = null;
                var paraNodeNext = null;
                var headlineNodeNext = null;
                var figcaptionNodeNext = null;
                var asideNodeNext = null;
                var ttsTagType = AVTTSTagType.NONE;
                var fstartDate = new Date();
                var isContentDiv = false;
                var willPageEnd = false;
                var aria = '';
                var startPage = 0, endPage = 0;
                var range = null;
                var rect = null;


                this.close();

                mSentenceList = null;
                mInitPage = false;
                mSentenceList = new Array();
                mParagraphMap = new RunaEngineMap();
                mTableMap = new RunaEngineMap();
                mIdMap = new RunaEngineMap();
                mParagraphIdx = 0;
                mTableIdx = 0;

                try {

                    iterator = document.createTreeWalker(m_divContentLayout, NodeFilter.SHOW_ALL, AVDEleCheck, false);
                    node = iterator.firstChild();

                    while (node) {
                        nodeCfiOffset = 0;

                        if (node.nodeType == 1) {

                            if (node == m_divContentLayout) {
                                node = iterator.nextNode();
                                isContentDiv = true;
                                continue;
                            }

                            range = document.createRange();
                            range.selectNode(node);
                            rect = range.getBoundingClientRect();
                            range.detach();


                            if (RunaEngine.DeviceInfo.IsVScrollMode()) {
                                startPage = parseInt(rect.top / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                                endPage = parseInt((rect.top + rect.height) / RunaEngine.HtmlCtrl.getPageHeight()) + 1;
                            }
                            else {
                                startPage = parseInt(rect.left / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                                endPage = parseInt((rect.left + rect.width) / RunaEngine.HtmlCtrl.getPageWidth()) + 1;
                            }

                            if(RunaEngine.HtmlCtrl.PageInfo.GetTotalPageCount() == 1) {
                                startPage = 1;
                                endPage = startPage;
                            }


                            isContentDiv = false;

                            //NONE: 0, HEADLINE: 1, PARAGRAPH: 2, IMG: 3, ANCHOR: 4, TABLE: 5, TEXT: 6, SPAN: 8, FIGCAPTION: 9, CAPTION: 10, PAPERSPAN: 11, MATH: 12, FIGURE: 13, ASIDE: 14
                            id = AVDGetAttrValue(node, 'id');

                            if (id == null || id == undefined) id = '';

                            if (paraNodeNext != null && node == paraNodeNext) {
                                paraNodeNext = null;

                                //RunaEngine.log('P, End Paragraph');

                                this.AddSentence('RETURN', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                            }

                            if (figcaptionNodeNext != null && node == figcaptionNodeNext) {
                                figcaptionNodeNext = null;

                                this.AddSentence('IMG_DESC_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                                ttsTagType = AVTTSTagType.NONE;
                            }

                            if (asideNodeNext != null && node == asideNodeNext) {
                                asideNodeNext = null;

                                this.AddSentence('ASIDE_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                                ttsTagType = AVTTSTagType.NONE;
                            }

                            if (headlineNodeNext != null && node == headlineNodeNext) {
                                headlineNodeNext = null;
                                ttsTagType = AVTTSTagType.NONE;
                            }

                            if (node.nodeName.toUpperCase() == 'SECTION') {
                                node = iterator.nextNode();
                                continue;
                            }

                            if (node.nodeName.toUpperCase() == 'P') {
                                RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);

                                textData = node.textContent;
                                textData = AVDRemoveWhiteSpace(textData);

                                this.AddSentence('PARAGRAPH', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');

                                mParagraphMap.put(mParagraphIdx++, mSentenceList.length - 1);

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                if (textData == null || textData == '' || textData.length == 0) {
                                    RunaEngine.log('P, Blank Line');

                                    this.AddSentence('BLANK_LINE', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');
                                }


                                paraNodeNext = node.nextElementSibling;

                                if (paraNodeNext == null) {
                                    paraNodeNext = node.parentNode;
                                    //RunaEngine.sublog2('paragraph parent = ' + (paraNodeNext != null ? paraNodeNext.nodeName : 'null'));
                                    if (paraNodeNext != null)
                                        paraNodeNext = paraNodeNext.nextElementSibling;
                                    //RunaEngine.sublog2('paragraph parent = ' + (paraNodeNext != null ? paraNodeNext.nodeName : 'null'));

                                    //if (paraNodeNext == m_divContentLayout)
                                        //RunaEngine.sublog2('paragraph parent root div = ' + (paraNodeNext != null ? paraNodeNext.nodeName : 'null'));
                                }
                                //RunaEngine.sublog2('paragraph = ' + (paraNodeNext != null ? paraNodeNext.nodeName : 'null'));

                            }
                            else if (node.nodeName.toUpperCase() == 'IMG') {

                                aria = AVDGetAttrValue(node, 'aria-describedby');
                                val = AVDGetAttrValue(node, 'alt');

                                if ((aria != null && aria != '') || (val != null && val.length > 0)) {

                                    //val = RunaEngine.Util.encodeHtmlEntity(val);
                                    val = decodeHTMLEntities(val);

                                    this.AddSentence('IMG_START', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');

                                    if (id != '') {
                                        mIdMap.put(id, mSentenceList.length - 1);
                                    }

                                    nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                    RunaEngine.log('IMG - ALT, name = ' + node.nodeName + ', type = ' + node.nodeType + ', text = ' + val);

                                    this.AddSentence('IMG', 'Y', nodeCfi, nodeCfi, val, id, '', startPage, endPage, -1, '000000');

                                    this.AddSentence('IMG_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                                }
                            }
                            else if (node.nodeName.toUpperCase() == 'FIGURE') {
                                RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);
                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }
                            }
                            else if (node.nodeName.toUpperCase() == 'FIGCAPTION') {

                                if (id != null && id != undefined && id == aria) {

                                    this.AddSentence('IMG_DESC_START', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');

                                    ttsTagType = AVTTSTagType.FIGCAPTION;

                                    figcaptionNodeNext = node.nextElementSibling;

                                    if (figcaptionNodeNext == null) {
                                        figcaptionNodeNext = node.parentNode;
                                        RunaEngine.sublog2('figcaption parent = ' + (figcaptionNodeNext != null ? figcaptionNodeNext.nodeName : 'null'));
                                        if (figcaptionNodeNext != null)
                                            figcaptionNodeNext = figcaptionNodeNext.nextElementSibling;
                                        RunaEngine.sublog2('figcaption parent = ' + (figcaptionNodeNext != null ? figcaptionNodeNext.nodeName : 'null'));

                                        if (figcaptionNodeNext == m_divContentLayout)
                                            RunaEngine.sublog2('figcaption parent root div = ' + (figcaptionNodeNext != null ? figcaptionNodeNext.nodeName : 'null'));
                                    }
                                    RunaEngine.sublog2('figcaption = ' + (figcaptionNodeNext != null ? figcaptionNodeNext.nodeName : 'null'));
                                }

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);

                            }
                            else if (node.nodeName.toUpperCase() == 'ASIDE') {

                                this.AddSentence('ASIDE_START', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                ttsTagType = AVTTSTagType.ASIDE;

                                asideNodeNext = node.nextElementSibling;

                                if (asideNodeNext == null) {
                                    asideNodeNext = node.parentNode;
                                    RunaEngine.sublog2('aside parent = ' + (asideNodeNext != null ? asideNodeNext.nodeName : 'null'));
                                    if (asideNodeNext != null)
                                        asideNodeNext = asideNodeNext.nextElementSibling;
                                    RunaEngine.sublog2('aside parent = ' + (asideNodeNext != null ? asideNodeNext.nodeName : 'null'));

                                    if (asideNodeNext == m_divContentLayout)
                                        RunaEngine.sublog2('aside parent root div = ' + (asideNodeNext != null ? asideNodeNext.nodeName : 'null'));
                                }
                                RunaEngine.sublog2('aside = ' + (asideNodeNext != null ? asideNodeNext.nodeName : 'null'));
                            }
                            else if (node.nodeName.toUpperCase() == 'TABLE') {
                                RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);

                                nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                this.AddTableSentenceWithNode('TABLE_START', 'N', nodeCfi, nodeCfi, '', id, '', id, startPage, endPage, -1, '000000', node);

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                this.AddSentence('PARAGRAPH', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');

                                mParagraphMap.put(mParagraphIdx++, mSentenceList.length - 1);
                                mTableMap.put(mTableIdx++, mSentenceList.length - 1);

                                this.MakeTableSentence(node, id);

                                this.AddSentence('TABLE_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');

                                node = iterator.nextSibling();
                                while (node == null) {
                                    node = iterator.parentNode();

                                    if (node == m_divContentLayout) {
                                    	//sjhan,210617,나의 parent node가 root node일때 반복문 종료
                                    	node = null
                                    	break;
									}
									
                                    if (node != null)
                                        node = iterator.nextSibling();
                                }
                                continue;
                            }
                            else if (node.nodeName.toUpperCase() == 'BR') {
                            	this.AddSentence('RETURN', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                            }
                            else if (node.nodeName.toUpperCase() == 'A') {
                                href = AVDGetAttrValue(node, 'href');
                                RunaEngine.log('--- A , id = ' + id + ', href = ' + href + ', text = ' + node.textContent);

                                this.AddSentence('ANCHOR_START', 'N', '', '', '', id, href, startPage, endPage, -1, '000000');

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                if (nodeCfi.indexOf(':') > 0) {
                                    nodeCfi = nodeCfi.split(':')[0];
                                }

                                // RunaEngine.Util.encodeHtmlEntity(node.textContent);

                                textData = node.textContent;
                                textData = AVDRemoveWhiteSpace(textData);

                                if (textData != null && textData.length > 0) {
                                    var list = RunaEngine.HtmlCtrl.AVTTSEngine.GetAllSentence(node);

                                    if (list != null && list.length > 0) {
                                        for (var i = 0; i < list.length; i++) {
                                            if (list[i].s.length == 0) continue;
                                            //val = RunaEngine.Util.encodeHtmlEntity(list[i].s);
                                            this.AddSentence('ANCHOR_TEXT', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, id, href, list[i].sp, list[i].ed, -1, list[i].style);
                                        }
                                    }
                                }

                                this.AddSentence('ANCHOR_END', 'N', '', '', '', id, href, startPage, endPage, -1, '000000');
                                this.AddSentence('ANCHOR_DESC_START', 'N', '', '', '', id, href, startPage, endPage, -1, '000000');
                                this.AddSentence('ANCHOR_DESC_TEXT', 'N', '', '', '', id, href, startPage, endPage, -1, '000000');
                                this.AddSentence('ANCHOR_DESC_END', 'N', '', '', '', id, href, startPage, endPage, -1, '000000');

                                node = iterator.nextSibling();
                                while (node == null) {
                                    node = iterator.parentNode();

                                    if (node == m_divContentLayout) {
                                    	//sjhan,210617,나의 parent node가 root node일때 반복문 종료
                                    	node = null
                                    	break;
									}

                                    if (node != null)
                                        node = iterator.nextSibling();
                                }
                                continue;
                            }
                            else if (node.nodeName.toUpperCase() == 'SPAN') {

                                epub_type = AVDGetAttrValue(node, 'epub:type');
                                RunaEngine.log('name = ' + node.nodeName + ', epub:type = ' + epub_type + ', text = ' + node.textContent);

                                if (epub_type == "pagebreak") {

                                    textData = node.textContent;
                                    textData = AVDRemoveWhiteSpace(textData);

                                    if (textData != null && textData.length > 0) {
                                        var list = RunaEngine.HtmlCtrl.AVTTSEngine.GetAllSentence(node);

                                        if (list != null && list.length > 0) {
                                            for (var i = 0; i < list.length; i++) {
                                                if (list[i].s.length == 0) continue;
                                                //val = RunaEngine.Util.encodeHtmlEntity(list[i].s);
                                                this.AddSentence('PAPER_PAGE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, id, '', list[i].sp, list[i].ed, -1, list[i].style);
                                            }
                                        }
                                    }

                                    if (id != '') {
                                        mIdMap.put(id, mSentenceList.length - 1);
                                    }

                                    node = iterator.nextSibling();
                                    while (node == null) {
                                        node = iterator.parentNode();

                                        if (node == m_divContentLayout) {
	                                    	//sjhan,210617,나의 parent node가 root node일때 반복문 종료
	                                    	node = null
	                                    	break;
										}

                                        if (node != null)
                                            node = iterator.nextSibling();
                                    }
                                    continue;
                                }
                                else {
                                    if (id != '') {
                                        mIdMap.put(id, mSentenceList.length - 1);
                                    }
                                }
                            }
                            else if (node.nodeName.toUpperCase() == 'H1' || node.nodeName.toUpperCase() == 'H2' || node.nodeName.toUpperCase() == 'H3' || node.nodeName.toUpperCase() == 'H4' || node.nodeName.toUpperCase() == 'H5' || node.nodeName.toUpperCase() == 'H6') {
                                RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);
                                this.AddSentence('HEADLINE', 'N', '', '', node.nodeName.toUpperCase(), id, '', startPage, endPage, mParagraphIdx, '000000');

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                ttsTagType = AVTTSTagType.HEADLINE;

                                headlineNodeNext = node.nextElementSibling;

                                if (headlineNodeNext == null) {
                                    headlineNodeNext = node.parentNode;
                                    RunaEngine.sublog2('HEADLINE parent = ' + (headlineNodeNext != null ? headlineNodeNext.nodeName : 'null'));
                                    if (headlineNodeNext != null)
                                        headlineNodeNext = headlineNodeNext.nextElementSibling;
                                    RunaEngine.sublog2('HEADLINE parent = ' + (headlineNodeNext != null ? headlineNodeNext.nodeName : 'null'));

                                    if (headlineNodeNext == m_divContentLayout)
                                        RunaEngine.sublog2('HEADLINE parent root div = ' + (headlineNodeNext != null ? headlineNodeNext.nodeName : 'null'));
                                }
                                RunaEngine.sublog2('HEADLINE = ' + (headlineNodeNext != null ? headlineNodeNext.nodeName : 'null'));

                                this.AddSentence('PARAGRAPH', 'N', '', '', '', id, '', startPage, endPage, mParagraphIdx, '000000');

                                mParagraphMap.put(mParagraphIdx++, mSentenceList.length - 1);
                            }
                            else if (node.nodeName.toUpperCase() == 'MATH') {
                                RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);
                                alttext = AVDGetAttrValue(node, 'alttext');

                                nodeCfi = RunaEngine.HtmlCtrl.DPControl._encodeCFI(document, node, 0, "");

                                textData = node.textContent;
                                textData = AVDRemoveWhiteSpace(textData);

                                if (nodeCfi.indexOf(':') > 0) {
                                    nodeCfi = nodeCfi.split(':')[0];
                                }
                                //alttext = RunaEngine.Util.encodeHtmlEntity(alttext);
                                alttext = decodeHTMLEntities(alttext);

                                this.AddSentence('MATH_START', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');

                                if (id != '') {
                                    mIdMap.put(id, mSentenceList.length - 1);
                                }

                                this.AddSentence('MATH_TEXT', 'Y', nodeCfi + ':0', nodeCfi + ':' + textData.length, alttext, id, '', startPage, endPage, -1, '000000');

                                this.AddSentence('MATH_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');

                                node = iterator.nextSibling();
                                while (node == null) {
                                    node = iterator.parentNode();

                                        if (node == m_divContentLayout) {
                                    	//sjhan,210617,나의 parent node가 root node일때 반복문 종료
                                    	node = null
                                    	break;
									}

                                    if (node != null)
                                        node = iterator.nextSibling();
                                }
                                continue;
                            }
                            else {
                                RunaEngine.log('name = ' + node.nodeName + ', text = ' + node.textContent);
                            }
                            //RunaEngine.log('node, name = '+node.nodeName+', type = '+ node.nodeType+', text = '+node.textContent+', attr = '+node.hasAttributes() +', left =  '+ node.offsetLeft);
                        }
                        else if (node.nodeType == 3) {
                            //RunaEngine.log('node, name = '+node.nodeName+', type = '+ node.nodeType+', text = '+node.textContent);

                            if (isContentDiv) {
                                //구성 div후에 바로 text만 있는 형태
                                textData = node.textContent;
                                textData = AVDRemoveWhiteSpace(textData);
                                RunaEngine.log('----------- AVTTS Error : ' + textData);

                                var list = RunaEngine.HtmlCtrl.AVTTSEngine.GetNodeAllSentence(node);

                                if (list != null && list.length > 0) {
                                    for (var i = 0; i < list.length; i++) {
                                        if (list[i].s.length == 0) continue;
                                        //val = RunaEngine.Util.encodeHtmlEntity(list[i].s);
                                        this.AddSentence('TEXT_LINE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, '', '', list[i].sp, list[i].ed, -1, list[i].style);
                                    }
                                }
                            }
                            else {
                                textData = node.textContent;
                                textData = AVDRemoveWhiteSpace(textData);

                                if (textData != null && textData.length > 0) {

                                    var list = RunaEngine.HtmlCtrl.AVTTSEngine.GetNodeAllSentence(node);

                                    // NONE : 0, HEADLINE : 1, PARAGRAPH : 2, IMG : 3, ANCHOR : 4, TABLE : 5, TEXT : 6, SPAN : 8
                                    if (list != null && list.length > 0) {
                                        for (var i = 0; i < list.length; i++) {
                                            if (list[i].s.length == 0) continue;
                                            //val = RunaEngine.Util.encodeHtmlEntity(list[i].s);
                                            if (ttsTagType == AVTTSTagType.HEADLINE) {
                                                this.AddSentence('HEADLINE_TEXT', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, '', '', list[i].sp, list[i].ed, -1, list[i].style);
                                            }
                                            else if (ttsTagType == AVTTSTagType.FIGCAPTION) {
                                                this.AddSentence('IMG_DESC_TEXT_LINE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, '', '', list[i].sp, list[i].ed, -1, list[i].style);
                                            }
                                            else if (ttsTagType == AVTTSTagType.ASIDE) {
                                                this.AddSentence('ASIDE_TEXT_LINE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, '', '', list[i].sp, list[i].ed, -1, list[i].style);
                                            }
                                            else {
                                                this.AddSentence('TEXT_LINE', 'Y', list[i].cfi + ':' + list[i].startoffset, list[i].cfi + ':' + list[i].endoffset, list[i].s, '', '', list[i].sp, list[i].ed, -1, list[i].style);
                                            }
                                        }
                                    }
                                }
                                else {
                                    // no text
                                }
                            }
                        }
                        node = iterator.nextNode();
                    } // while

                    if (ttsTagType == AVTTSTagType.FIGCAPTION) {
                        this.AddSentence('IMG_DESC_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                    }
                    else if (ttsTagType == AVTTSTagType.ASIDE) {
                        this.AddSentence('ASIDE_END', 'N', '', '', '', id, '', startPage, endPage, -1, '000000');
                    }

                    mInitPage = true;
                }
                catch (ex) {
                    RunaEngine.sublog2('[★Exception★][AVDTTS : initialize] ' + ex.stack);
                    mInitPage = false;
                }
                finally {
                    iterator = null;
                    node = null;

                    fendDate = new Date();
                    fcalDate = fendDate - fstartDate;
                    RunaEngine.sublog2('node, iterate date = ' + (fcalDate / 1000));
                }
            }
        };
    };

    ///////////////////////////////////////////////////////////////////
    // 웹에서 이벤트를 주는 경우의 Interface들 모음
    ///////////////////////////////////////////////////////////////////
    this.Interface = new function __Interface() {

        ///////////////////////////////////////////////////////////////////////
        // [ Desc   ] 하이라이팅 클릭 이벤트 발생
        // [ Param  ] (string) strHLID: 하이라이팅 ID
        this.OnHLDivClick = function (strHLID) {
            if (RunaEngine.DeviceInfo.IsAndroid()) {
                window.android.selectHighlight(strHLID);
            }
            else if (RunaEngine.DeviceInfo.isPC()) {
                RunaPC.selectHighlight(strHLID);
            }
            else {
                //IOS
                document.location = "runaScheme:" + strHLID;
            }
        }
    };

    this.Util = new function __Util() {

        this.Trim = function (str) {
            return str.replace(/^\s+|\s+$/g, "")
        }

        this.replaceAt = function(str, index, chr) {
        	if(index > str.length - 1 || index < 0) return str;

        	return str.substr(0, index) + chr + str.substr(index + 1);
        }

        this.GetElementInsideContainer = function(containerID, attrName) {
            var resultArray = new Array();
            var elms = document.getElementById(containerID).getElementsByTagName("*");
            for (var i = 0; i < elms.length; i++) {
                if(this.GetAttrValue(elms[i], attrName) != null ) {
                    resultArray.push(elms[i]);
                }
            }
            return resultArray;
        }

        this.GetElementInsideContainerWithValue = function(containerID, attrName, value) {
            var resultArray = new Array();
            var elms = document.getElementById(containerID).getElementsByTagName("*");
            for (var i = 0; i < elms.length; i++) {
                if(this.GetAttrValue(elms[i], attrName) != null && this.GetAttrValue(elms[i], attrName) == value) {
                    resultArray.push(elms[i]);
                }
            }
            return resultArray;
        }

        this.GetAttrValue = function (node, name) {
            var val = null;
            var attrs = null;
            var len = 0;
            var attr = null;

            try {
                attrs = node.attributes;

                if (attrs != null) {
                    len = attrs.length;

                    if (len > 0) {
                        while (len--) {
                            attr = attrs[len];

                            if (attr.name == name) {
                                val = attr.value;
                                break;
                            }
                        }
                    }
                }
            }
            catch (ex) {
                RunaEngine.log(ex);
            }
            finally {
                attrs = null;
                len = 0;
                attr = null;
            }

            return val;
        }

        this.CheckWhitespace = function (ch) {
            var rg1 = /^\s/gm;
            var rg2 = /\r\n$/g;
            var rg3 = /(^\s*)|(\s*$)/g;

            if (rg1.test(ch))
                return true;

            if (rg2.test(ch))
                return true;

            //            if(rg3.test(ch))
            //                return true;

            return false;
        }

        this.getParentTagName = function (node, tagname) {
            var parent;
            if (node === null || tagname === '') return;
            parent = node.parentNode;
            tagname = tagname.toUpperCase();

            while (parent.tagName.toUpperCase() !== "HTML") {
                if (parent.tagName.toUpperCase() == tagname) {
                    return parent;
                }
                parent = parent.parentNode;
            }

            return parent;
        }

        this.decodeHtmlEntity = function (str) {
            return str.replace(/&#(\d+);/g, function (match, dec) {
                               return String.fromCharCode(dec);
                               });
        };

        this.encodeHtmlEntity = function (str) {
            var buf = [];
            for (var i = str.length - 1; i >= 0; i--) {
                buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
            }
            return buf.join('');
        };

        /**
         * 인자로 받은 태그 이름아닌 최초의 부모 노드를 가져온다.
         * @param  {Object} node    DOM node, element
         * @param  {String} tagname HTML tagName
         * @return {Object}         Parent node
         */
        this.getFirstParentNotEqualTagName = function (node, tagname) {
            var parent;
            if (node === null || tagname === '') return;
            parent = node.parentNode;
            tagname = tagname.toUpperCase();

            while (parent.tagName.toUpperCase() !== "HTML") {
                if (parent.tagName.toUpperCase() !== tagname) {
                    return parent;
                }
                parent = parent.parentNode;
            }

            return parent;
        }

        this.caretRangeFromPoint = function (doc, x, y) {
            var position, range = null;

            if (typeof doc.caretPositionFromPoint != "undefined") {
                position = doc.caretPositionFromPoint(x, y);
                range = doc.createRange();
                range.setStart(position.offsetNode, position.offset);
                range.collapse(true);
                //RunaEngine.sublog(' RunaEngine.Util.caretRangeFromPoint 1: '+range);
            } else if (typeof doc.caretRangeFromPoint != "undefined") {
                range = doc.caretRangeFromPoint(x, y);
                //RunaEngine.sublog(' RunaEngine.Util.caretRangeFromPoint 2 : '+range);
            } else if (typeof doc.body.createTextRange != "undefined") {
                range = doc.body.createTextRange();
                range.moveToPoint(x, y);
                //RunaEngine.sublog(' RunaEngine.Util.caretRangeFromPoint 3 : '+range);
            }

            return range;
        }

        this.rangeFromOffset = function (x, y) {
            var log = "";



            var element = document.elementFromPoint(x, y);
            var nodes = getTextNodes(element, x, y);
            if (!nodes.length)
                return null;
            var node = nodes[0];

            var range = document.createRange();
            range.setStart(node, 0);
            range.setEnd(node, 1);

            for (var i = nodes.length; i--;) {
                var node = nodes[i],
                text = node.nodeValue;


                range = document.createRange();
                range.setStart(node, 0);
                range.setEnd(node, text.length);

                if (!inObject(x, y, range))
                    continue;

                for (var j = text.length; j--;) {
                    if (text.charCodeAt(j) <= 32)
                        continue;

                    range = document.createRange();
                    range.setStart(node, j);
                    range.setEnd(node, j + 1);

                    if (inObject(x, y, range)) {
                        range.setEnd(node, j);
                        return range;
                    }
                }
            }

            return range;
        }

        this.getElementsByTagNames = function (list, obj) {
            if (!obj) var obj = document;
            var tagNames = list.split(',');
            var resultArray = new Array();
            for (var i = 0; i < tagNames.length; i++) {
                var tags = obj.getElementsByTagName(tagNames[i]);
                for (var j = 0; j < tags.length; j++) {
                    resultArray.push(tags[j]);
                }
            }
            var testNode = resultArray[0];
            if (!testNode) return [];
            if (testNode.sourceIndex) {
                resultArray.sort(function (a, b) { return a.sourceIndex - b.sourceIndex; });
            }
            else if (testNode.compareDocumentPosition) {
                resultArray.sort(function (a, b) { return 3 - (a.compareDocumentPosition(b) & 6); });
            }
            return resultArray;
        }

        function inRect(x, y, rect) {
            return x >= rect.left && x <= rect.right && y >= rect.top && y <= rect.bottom;
        }

        function inObject(x, y, object) {
            var rects = object.getClientRects();
            for (var i = rects.length; i--;)
                if (inRect(x, y, rects[i]))
                    return true;
            return false;
        }

        function getTextNodes(node, x, y) {
            if (!inObject(x, y, node))
                return [];

            var result = [];
            node = node.firstChild;
            while (node) {
                if (node.nodeType == 3)
                    result.push(node);
                if (node.nodeType == 1)
                    result = result.concat(getTextNodes(node, x, y));

                node = node.nextSibling;
            }

            return result;
        }
    };
};

/////////////////////////////////////////////////////////////////////////////////
if (!RunaEngine.DeviceInfo.IsAndroid() && !RunaEngine.DeviceInfo.isPC()) {
    document.addEventListener("touchstart", touchStartHandler, true);
    document.addEventListener("touchend", touchEndHandler, true);
    //document.location = 'bdbinit:true';
}
else {
    if (RunaEngine.DeviceInfo.IsOSX()) {
        document.addEventListener("mousewheel", mouseWheelEventHandler, false);
        document.addEventListener("DOMMouseScroll", mouseWheelEventHandler, false);
        window.addEventListener("keydown", keyboardPreventFunc, false);
    }
    else if (RunaEngine.DeviceInfo.isPC()) {
        document.addEventListener("mousewheel", mouseWheelEventHandler, false);
        document.addEventListener("DOMMouseScroll", mouseWheelEventHandler, false);
        window.addEventListener("keydown", keyboardPreventFunc, false);
    }
    else if (RunaEngine.DeviceInfo.IsAndroid()) {
        document.addEventListener("DOMMouseScroll", mouseWheelEventHandler, false);
        window.onwheel = mouseWheelEventHandler;
        window.onmousewheel = mouseWheelEventHandler;
        window.ontouchmove = mouseWheelEventHandler;
        document.ontouchmove = mouseWheelEventHandler;

        //android 2.3 이하를 위한 xpath 라이브러리 로딩
        if (RunaEngine.DeviceInfo.getAndroidVer() <= 230) {
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'file:///android_asset/xpath.min.js';
            head.appendChild(script);
        }

        window.addEventListener('load', function(){
            var arrImgs = document.getElementsByTagName('img');

            if( arrImgs == null || arrImgs == undefined ) return;

            arrImgsRect = new Array();

            for (i = 0, len = arrImgs.length ; i < len; i++) {
                var iW = arrImgs[i].width;
                var iH = arrImgs[i].height;

                arrImgsRect.push({width:iW, height: iH});
            }
        });
    }
}

function keyboardPreventFunc(e) {
    // space and arrow keys
    if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
        return false;
    }
}


/**
 * textFinder class
 */
const Finder = {
    /**
     * 뷰어의 양쪽 끝  textNode를 가저온다.
     * @param {*} target 뷰어 element
     * @returns 
     */
    scanAll(target){
        let rect = target.getBoundingClientRect()
        let x1 = rect.left -1
        let y1 = rect.top -1
        let x2 = rect.right-1
        let y2 = rect.bottom-2
        let mRange = null
        let mRange2 = null
        const getRange = function(x,y){
            let position = null, range = null;
            if (typeof document.caretPositionFromPoint != "undefined") {
                position = document.caretPositionFromPoint(x, y);
                range = document.createRange();
                range.setStart(position.offsetNode, position.offset);
                range.collapse(true);
            } 
            else if (typeof document.caretRangeFromPoint != "undefined") {
                range = document.caretRangeFromPoint(x, y);
            }
            else if (typeof document.body.createTextRange != "undefined") {
                range = fromPointIE(x, y, document);
            }
            return range
        }
        const isStrEmpty = function(str){ 
            if (typeof str === 'string') {
                if ( str.trim().length === 0){
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        }
        const getTextNodesAll = function(element) {
            var n, a=[], walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
            while(n=walk.nextNode()){
                if (n.parentNode  && n.parentNode instanceof HTMLUnknownElement){
                    continue;
                }
                a.push(n);
            }
            n = null;
            return a;
        }
        const rewind = function(startRange,endRange){
            let sc = startRange.startContainer, 
            so = startRange.startOffset,
            ec = endRange.endContainer,
            eo = endRange.endOffset;    

            let range = null
            let fg = null
            let list = null
            let front = null
            let back = null
            range = document.createRange()
            range.setStart(sc,so)
            range.setEnd(ec,eo)
            fg = range.cloneContents()
            list = getTextNodesAll(fg)
            front = null
            back = null
            for (let i = 0; i<list.length; i++){
                let node = list[i]
                if (!isStrEmpty(node.textContent)){
                    front = node
                    break;
                }
            }
            for (let i = list.length-1; i>=0; i--){
                let node = list[i]
                if (!isStrEmpty(node.textContent)){
                    back = node
                    break;
                }
            }
            if (!front || !back){
                return null
            }
            return {front,back}
        }   

        return new Promise(function(resolve , reject){
            mRange = getRange(x1,y1)
            mRange2 = getRange(x2,y2)
            if (!mRange || !mRange2){
                reject('not found range')
                return 
            }
            let rw = rewind(mRange,mRange2)
            if (rw){
                resolve(rw)
            } else {
                reject('not found text node')
            }
        })
    },
    /**
     * 전체를 스캔후 파라미터에 해당하는 애들이 front의 끝인지 back의 끝인지 판변하는 함수
     * @param {*} selector 샐랙터
     * @param {*} startNode 시작노드
     * @param {*} startOffset 노드offset
     * @param {*} endNode 끝노드
     * @param {*} endOffset 끝노드offset
     * @returns 
     */
    checkEndPoint(selector,startNode,startOffset,endNode,endOffset){
        let tg = null
        if (typeof selector === 'string' ){
            if (selector.startsWith('#')){
                tg = document.getElementById(selector.replace('#',''))
            } else if (selector.startsWith('.')){
                tg = document.getElementsByClassName(selector.replace('.',''))[0]
            } 
        } else if (selector instanceof HTMLElement){
            tg = selector
        }
        if (!tg){
            return Promise.reject('not found target element')
        }

        return this.scanAll(tg).then(rs=>{
            let cloneS = rs.front.textContent 
            let cloneE = rs.back.textContent

            let s = startNode.textContent.slice(startOffset)
            let e = endNode.textContent.substring(0,endOffset)
            let front = s === cloneS
            let back = e === cloneE
            return { front,back }
        })
    },
}