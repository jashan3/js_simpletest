

function SomeTask(){
    this.loading = false
}

SomeTask.prototype = {
    
    run: function(){

    },
    //task 정지
    stop: function(){

    },
    //메모리 클리어
    clear: function(){

    },
    _loadImage: function(){
   
    },
    _onLoadImageComplete: function(){

    },
    _onLoadImageFail: function(){

    },
    _render: function(){

    },
    _onRenderFail: function(){

    },
    _onRenderComplete: function(){
        this.loading = true
    },
}


const App = {

    makeTask: function(){
       return new SomeTask()
    }
}