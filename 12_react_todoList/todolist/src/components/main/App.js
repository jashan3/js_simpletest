import {
  BrowserRouter,
  Routes,
  Route,
  Link
} from "react-router-dom";
import React from 'react';


import '../main/App.css';

function App() {
  return (
    <BrowserRouter>
      <div>
        <header>
          <h1>title</h1>
          <ul className="header-horitontal">
            <li><Link to="/">홈</Link></li>
            <li><Link to="/users">유저</Link></li>
            <li><Link to="/list">리스트</Link></li>
          </ul>
        </header>
        <div className="container">
          <Routes>
            <Route path="/" element={ <Main/> } exact/>
            <Route path="users/*" element={ <Login/> } />
            <Route path="/list" element={ <MainList/> } />
          </Routes>
        </div>
      </div>

    </BrowserRouter>
  );
}

function Main(){
 return (
    <div className='Main'>
      메인
    </div>
  )
}

function Login(){
  return (
    <div className='Login'>
      로그인
    </div>
  )
}

const MainList = ()=>{
  return(
    <div>
      <ul
        data={[
          {key: 'Devin'},
          {key: 'Dan'},
          {key: 'Dominic'},
          {key: 'Jackson'},
          {key: 'James'},
          {key: 'Joel'},
          {key: 'John'},
          {key: 'Jillian'},
          {key: 'Jimmy'},
          {key: 'Julie'},
        ]}
        renderItem={({item}) => <li >{item.key}</li>}
      />
    </div>
  )
}

export default App;
