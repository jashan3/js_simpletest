class Model {
    constructor() {
      books = [
        {id: 'RCB-123',name: "React Cook Book", isFavorite: false},
        {id: 'VCB-123',name: "Vue Cook Book", isFavorite: false},
        {id: 'ACB-123',name: "Angular Cook Book", isFavorite: false}
      ];
    }
    
    getBooks() {
        return this.books
    }

    toggleFavorite(bookId) {
      const target = this.books.filter(item => item.id === bookId)[0];
      target.isFavorite = !target.isFavorite
    }
}