import DefineMap from 'can-define/map/map';

export const ViewModel = DefineMap.extend({
  count: 'number',
  increment: function() {
    return this.count++;
  },
});