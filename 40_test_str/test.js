
// const path1 = "/HTML/BODY/DIV/MAIN/SECTION/DIV/UL/LI[16]/P[19]/text()[2]";
// const path2 = "/HTML/BODY/DIV/MAIN/SECTION/DIV/UL/LI[16]/P[19]/text()[3]";

// var path1 = "/HTML/BODY/DIV/MAIN/SECTION/DIV/UL/LI[16]/P[22]/text()";
// var path2 = "/HTML/BODY/DIV/MAIN/SECTION/DIV/UL/LI[16]/P[19]/text()[2]";



var path1 = "/HTML/BODY/DIV/MAIN/SECTION/DIV/UL/LI[17]/P[12]/text()" 
,path2 ="/HTML/BODY/DIV/MAIN/SECTION/DIV/UL/LI[17]/H2/text()[2]";

const compareXpath = (startPath,endPath) =>{
    if (typeof startPath != 'string'){
        return null;
    }
    if (typeof endPath != 'string'){
        return null;
    }
    if (startPath.length == 0){
        return null;
    }
    if (endPath.length == 0){
        return null;
    }
    function extractNumbersFromString(str) {
        let num = str.replace(/\D/g, "");
        num = parseInt(num)
        if (!isFinite(num)||isNaN(num)){
            num = 1;
        }
        return num;
    }
    const startlist = startPath.split('/');
    const endlist = endPath.split('/');

    for (let index = 8; index < startlist.length; index++) {
        const item = startlist[index];
        const num = extractNumbersFromString(item);

        console.log(item,num);
    }

    for (let index = 8; index < endlist.length; index++) {
        const item = endlist[index];
        const num = extractNumbersFromString(item);
        console.log(item,num);
        
    }
}
compareXpath(path1,path2);