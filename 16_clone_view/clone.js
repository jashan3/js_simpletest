function CloneKit(clLayer) {
    this.clLayer = document.createElement('div')
    this.clLayer.style.width = '100vw'
    this.clLayer.style.height = '100vh'
    this.clLayer.style.position = 'fixed'
    this.clLayer.style.left = 0
    this.clLayer.style.top = 0
    this.clLayer.style.zIndex = 10
    this.clLayer.style.display = 'none'
    this.inAnimation = false
    document.body.appendChild(this.clLayer)
    this.clLayer.addEventListener('click',this.dismiss.bind(this))
}

CloneKit.prototype = {
    /**
     * 복사한 element를 지정한 parent 안쪽에 넣는다.
     */ 
    addElement(element){
        this.clLayer.innerHTML = ''
        this.clLayer.style.display = 'block'
        let copy = this.copyElement(element)
        this.clLayer.appendChild(copy.element)
        this.onStartTransition(copy.element,copy.x,copy.y)
    },
    /**
     * element를 복사시킨다.
     */ 
    copyElement(element){
        let childStyle = this.getStyle(element)
        let childRect = element.getBoundingClientRect()
        let nd = element.cloneNode(true)
        let afterLeft = window.innerWidth/2 - (childRect.width - childStyle.margin.left)/2
        //size
        nd.style.position = 'absolute'
        nd.style.top = childRect.top-childStyle.margin.top +'px'
        nd.style.left = childRect.left-childStyle.margin.left + 'px'
        nd.style.width = childRect.width - childStyle.padding.left - childStyle.padding.right +'px'
        nd.style.height = childRect.height - childStyle.padding.top - childStyle.padding.bottom+'px'

        nd.style.paddingTop = childStyle.padding.top + 'px'
        nd.style.paddingLeft = childStyle.padding.left + 'px'
        nd.style.paddingBottom = childStyle.padding.bottom + 'px'
        nd.style.paddingRight = childStyle.padding.right + 'px'

        //style
        nd.style.opacity = 1;
        nd.style.boxShadow = "2px 2px 2px 1px rgba(0, 0, 0, 0.2)";

        var transform3d = 'scale3d('     + 0 + ', '  + 0 + ',1)'
        var transform2d = 'scale('       + 0 + ', '  + 0 + ')'
        nd.style.webkitTransform = transform3d;
        nd.style.mozTransform = transform2d;
        nd.style.msTransform = transform2d;
        nd.style.oTransform = transform2d;
        nd.style.transform = transform3d;

        return {
            'element':nd,
            'x':afterLeft,
            'y':0,
        }
    },
    setStyle({
        element , 
        progress ,
        zoom ,
        x ,
        y ,
        border
    }){
        let zoomFactor = zoom * progress
        let offsetX = x * progress
        let offsetY = y * progress
        let _border = border
        var transform3d = 'scale3d('     + zoomFactor + ', '  + zoomFactor + ',1) ' +'translate3d(' + offsetX    + 'px,' + offsetY    + 'px,0px)'
        var transform2d = 'scale('       + zoomFactor + ', '  + zoomFactor + ') ' +'translate('   + offsetX    + 'px,' + offsetY    + 'px)'
        
        element.style.webkitTransform = transform3d;
        element.style.mozTransform = transform2d;
        element.style.msTransform = transform2d;
        element.style.oTransform = transform2d;
        element.style.transform = transform3d;

        element.style.borderRadius = _border +'px'
    },
    /**
     * padding이나 margin 값을 추출
     */ 
    getStyle(element){
        let result = {
            margin: {
                top:0,
                left:0,
                right:0,
                bottom:0,
            },
            padding: {
                top:0,
                left:0,
                right:0,
                bottom:0,
            }
        }
        let ct = getComputedStyle(element)
        for (k1 of Object.keys(result)){
            for (k2 of Object.keys(result[k1])){
                result[k1][k2] = parseInt(ct.getPropertyValue(k1+'-'+k2))
            }
        }
        return result
    },
    animate: function (duration, framefn, timefn, callback) {
        var startTime = new Date().getTime(),
            renderFrame = (function () {
                if (!this.inAnimation) { return; }
                var frameTime = new Date().getTime() - startTime,
                    progress = frameTime / duration;
                if (frameTime >= duration) {
                    framefn(1);
                    if (callback) {
                        callback();
                    }
                    this.stopAnimation();
                } else {
                    if (timefn) {
                        progress = timefn(progress);
                    }
                    framefn(progress);
          
                    requestAnimationFrame(renderFrame);
                }
            }).bind(this);
        this.inAnimation = true;
        requestAnimationFrame(renderFrame);
    },
    stopAnimation: function(){
        this.inAnimation = false
    },
    swing(p) {
        return -Math.cos(p * Math.PI) / 2  + 0.5;
    },
    dismiss: function(){
        this.stopAnimation()
        let self = this
        let startOpacity = self.clLayer.firstChild.style.opacity
        this.animate(300,function(progress){
            self.clLayer.firstChild.style.opacity = startOpacity-startOpacity*progress
        },this.swing,function(){
            self.clLayer.firstChild.remove()
            self.clLayer.style.display = 'none'
        })
        
    },
    onStartTransition: function(element, toX, toY){
        let self = this
        this.stopAnimation()
        let clLayerRect = element.getBoundingClientRect()
        let originX = clLayerRect.left
        let originY = clLayerRect.top
        
        let diffX = toX - originX;
        let diffY = (window.innerHeight/2) - clLayerRect.bottom

        let border = 10

        this.animate(300,function(progress){
            self.setStyle({
                'element': element, 
                'progress': progress,
                'zoom': 1.5,
                'x': diffX ,
                'y':diffY  ,
                'border':border
            })
        },this.swing,function(){
    
        })
    }
}