


class SearchData {
    constructor(){

    }
}


class TocData {
    constructor(){

    }
}


/**
 * 북마크 객체.
 */
 class Annotation {

    /**
     * @param {*} chapterName 목차 제목
     * @param {*} text 텍스트
     * @param {*} fileNo 파일 번호
     * @param {*} percent 현재 퍼센트 (epub only)
     * @param {*} cfi 현재 cfi (epub only)
     * @param {*} selector 현재 selector (epub only)
     */
    constructor( chapterName, text, fileNo, percent, cfi, selector) {
        this.id = this.uuidv4(); 
        this.chapterName = chapterName; 
        this.text = text; 
        this.fileNo = fileNo;
        this.date =  new Date().current();
        this.percent = percent;  
        this.cfi = cfi;
        this.serverId = null;
        this.selector = selector;
        this.type = 0;
        this.memo = null
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
          (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }

    static responseToAnnotation(res){
        console.log('## responseToAnnotation',res)
        let mChapterName = res['chapterTitle']
        let mText = res['memo']
        let mPage = res['page']
        let mPercent = res['pagePercent']
        let mCfi= res['startPath']
        let mSelecter = res['startPath']
        mText = BDBParse.decode(mText)
        mChapterName = BDBParse.decode(mChapterName)
        let anno = new Annotation(mChapterName,mText,mPage,mPercent,mCfi,mSelecter)
        anno.serverId = res['seq']
        anno.fileNo = res['chapterNo']
        anno.date = res['regDt']
        return anno 
    }

    static responseToAnnotationList(res){
        let result = []

        res.forEach(e=>{
            
            result.push(Annotation.responseToAnnotation(e))
        })

        return result
    }
}