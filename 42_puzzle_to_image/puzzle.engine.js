/**
 * Puzzle engine
 */
const PuzzleEngine = (function () {
  const INSTANCE = {};

  /**
   * init constructor
   * @param {*} options
   */
  INSTANCE.init = function (options) {
    if (options){
        if (options.previewZone instanceof HTMLElement) {
            Puzzle.previewZone = options.previewZone;
          }
          if (options.gridContainer instanceof HTMLElement) {
            Puzzle.gridContainer = options.gridContainer;
          }
          if (typeof options.colSize == 'number' && options.colSize > 1) {
              Puzzle.colSize = options.gridContainer;
          }
          if (typeof options.rowSize == 'number' && options.rowSize > 1) {
              Puzzle.rowSize = options.gridContainer;
          }
    }

    let switchingResult = null;
    let selectedElement = null;
    let onStart = function (e) {
      let ele = null;
      if (
        e.target.classList &&
        e.target.classList.contains("grid-container-item")
      ) {
        ele = e.target;
      } else {
        ele = e.target.closest(".grid-container-item");
      }
      selectedElement = ele;
    };
    let onMove = function (e) {
      e.preventDefault();
      if (!selectedElement) {
        return;
      }
      let ele = e.target.closest(".grid-container-item");
      if (ele) {
        let prect = ele.parentElement.getBoundingClientRect();
        let mx = e.clientX - prect.x;
        let my = e.clientY - prect.y;
        let matched = Util.isMatched(selectedElement, ele, mx, my);
        if (matched) {
          ele.style.border = "1px solid red";
          let changeItem = Puzzle.puzzleList.find((it) => it.item == ele);
          if (changeItem) {
            switchingResult = {
              from: matched,
              to: changeItem,
            };
            return;
          }
        } else {
          for (let child of Puzzle.gridContainer.children) {
            child.style.border = "";
          }
        }
        switchingResult = null;
      }
    };

    let onEnd = function (e) {
      selectedElement = null;
      if (switchingResult) {
        const temp = Puzzle.animateMap.get(switchingResult.from.item.id);

        console.log('temp',temp.destRect)
        Puzzle.animateMap.set(
            switchingResult.from.item.id,
            Object.assign({},{
                originRect: switchingResult.to.rect,
                destRect: switchingResult.from.rect,
            })
        );
        
        Puzzle.animateMap.set(
            switchingResult.to.item.id,
            Object.assign({},{
                originRect: switchingResult.from.rect,
                destRect: temp.destRect,
            })
        );

        switchingResult = null;

        Puzzle.startAnimate().then(rs=>{
            console.log('err',rs);
        }).catch(err=>{
            console.error('err',err);
        })
      }
    };

    window.addEventListener("mousedown", onStart);
    window.addEventListener("mousemove", onMove);
    window.addEventListener("mouseup", onEnd);
    window.addEventListener("touchstart", onStart);
    window.addEventListener("touchmove", onMove);
    window.addEventListener("touchend", onEnd);
  };

  INSTANCE.setImage = function (image) {
    if (image) {
      Puzzle.originalImage = image;
    }
    return Puzzle.build()
      .then((rs) => Puzzle.suffle())
      .then((rs) => Puzzle.draw())
      .then((rs) => Puzzle.visible(true))
      .then((rs) => Puzzle.startAnimate());
  };
  INSTANCE.animate = function () {
    return Puzzle.visible(true)
    .then((rs) => Puzzle.startAnimate());
  };
  INSTANCE.draw = function () {
    return Puzzle.draw()
      .then((rs) => Puzzle.visible(true))
      .then((rs) => Puzzle.startAnimate());
  };
  INSTANCE.suffle = function () {
    return Puzzle.suffle()
      .then((rs) => Puzzle.visible(true))
      .then((rs) => Puzzle.startAnimate());
  };

  INSTANCE.reset = function () {
    return Puzzle.reset();
  };

  const Puzzle = {
    colSize: 3,
    rowSize: 3,
    puzzleList: new Array(),
    animateMap: new Map(),
    state: -1,
    originalImage: new Image(),
    /**@type {HTMLElement} */
    previewZone: null,
    /**@type {HTMLElement} */
    gridContainer: null,
    reset: function () {
      Puzzle.puzzleList = null;
      Puzzle.puzzleList = new Array();
      Puzzle.animateMap = null;
      Puzzle.animateMap = new Map();
      Puzzle.originalImage.innerHTML = "";
      Puzzle.gridContainer.innerHTML = "";
      this.visible(false);
    },
    visible: function (isActive) {
      return new Promise(function (resolve, reject) {
        if (isActive) {
          Puzzle.previewZone.classList.add("gone");
          Puzzle.gridContainer.classList.remove("gone");
        } else {
          Puzzle.previewZone.classList.remove("gone");
          Puzzle.gridContainer.classList.add("gone");
        }
        resolve(isActive);
      });
    },
    /**
     * build base data
     * @returns
     */
    build: function () {
      return new Promise(function (resolve, reject) {
        if (!Puzzle.originalImage) {
          return reject(new Error("setImage Function must be new Image()"));
        }

        Puzzle.reset();

        const rect = Puzzle.originalImage.getBoundingClientRect();
        const maxWidth = rect.width / Puzzle.colSize;
        const maxHeight = rect.height / Puzzle.rowSize;
        const originalWidth = Puzzle.originalImage.naturalWidth;
        const originalHeight = Puzzle.originalImage.naturalHeight;
        for (var i = 1; i <= Puzzle.colSize * Puzzle.rowSize; i++) {
          let div = document.createElement("div");
          let canvas = document.createElement("canvas");
          div.style.position = "absolute";
          div.appendChild(canvas);
          div.id = "puzzle_piece_" + i;
          div.setAttribute("puzzle", i);
          div.classList.add("grid-container-item");
          let context = canvas.getContext("2d");
          let pieceWidth = originalWidth / Puzzle.colSize;
          let pieceHeight = originalHeight / Puzzle.rowSize;
          let offsetX = ((i - 1) % Puzzle.colSize) * pieceWidth;
          let offsetY = Math.floor((i - 1) / Puzzle.colSize) * pieceHeight;

          canvas.width = Math.floor(originalWidth);
          canvas.height = Math.floor(originalHeight);
          canvas.draggable = true;
          context.drawImage(
            Puzzle.originalImage,
            offsetX,
            offsetY,
            pieceWidth,
            pieceHeight,
            0,
            0,
            canvas.width,
            canvas.height
          );

          let newWidth = pieceWidth;
          let newHeight = pieceHeight;
          if (pieceWidth > maxWidth) {
            newWidth = maxWidth;
            newHeight = (maxWidth / pieceWidth) * pieceHeight;
          }
          if (newHeight > maxHeight) {
            newHeight = maxHeight;
            newWidth = (maxHeight / pieceHeight) * pieceWidth;
          }

          canvas.style.width = newWidth + "px";
          canvas.style.height = newHeight + "px";
          div.style.width = newWidth + "px";
          div.style.height = newHeight + "px";
          let mleft = newWidth * ((i - 1) % Puzzle.colSize);
          let mtop = newHeight * Math.floor((i - 1) / Puzzle.colSize);

          Puzzle.puzzleList.push({
            item: div,
            rect: {
              id: div.id,
              left: mleft,
              top: mtop,
              right: mleft + newWidth,
              bottom: mtop + newHeight,
              width: newWidth,
              height: newHeight,
            },
          });
        }
        resolve(true);
      });
    },
    /**
     * 패섞기
     * @returns
     */
    suffle: function () {
      return new Promise(function (resolve, reject) {
        let index = 0;
        let randomArray = Util.generateSuffledArray(
          Puzzle.colSize * Puzzle.rowSize
        );
        while (Puzzle.colSize * Puzzle.rowSize > index) {
          let removedItem = randomArray.shift();
          let div = Puzzle.puzzleList[index].item;
          let originRect = Puzzle.puzzleList[index].rect;
          let destRect = Puzzle.puzzleList[removedItem].rect;
          let animateData = {
            originRect: originRect,
            destRect: destRect,
          };
          Puzzle.animateMap.set(div.id, animateData);
          index++;
        }
        resolve(Puzzle.animateMap);
      });
    },
    /**
     * 그리기
     * @returns
     */
    draw: function () {
      return new Promise(function (resolve, reject) {
        let i = 0;
        Puzzle.animateMap.forEach((value, key) => {
          const div = Puzzle.puzzleList[i].item;
          div.id = key;
          div.style.left = value.originRect.left + "px";
          div.style.top = value.originRect.top + "px";
          Puzzle.gridContainer.appendChild(div);
          i++;
          if (Puzzle.animateMap.size == i) {
            resolve(true);
          }
        });
      });
    },
    /**
     * animation start
     * @returns
     */
    startAnimate: function () {
      return new Promise(function (resolve, reject) {
        let i = 0;
        Puzzle.animateMap.forEach((value, key) => {
          const target = document.getElementById(key);
          if (target) {
            anime({
              targets: target,
              left: `${value.destRect.left}px`,
              top: `${value.destRect.top}px`,
              duration: 1200,
              easing: "easeInOutExpo",
              complete: function(anim) {
                i++;
                if (Puzzle.animateMap.size == i) {
                  resolve(true);
                }
              }
            });
          }
        });
      });
    },
    /**
     * validate Anim
     */
    validate: function(){
        Puzzle
        for (let index = 0; index < array.length; index++) {
            Puzzle
            const element = array[index];
            
        }
    }
  };

  const Util = {
    getRandomArbitrary: function (min, max) {
      return Math.random() * (max - min) + min;
    },
    shuffleArray: function (array) {
      for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
      }
    },
    generateSuffledArray: function (length = 64) {
      const numbersArray = Array.from({ length: length }, (_, index) => index);
      Util.shuffleArray(numbersArray);
      return numbersArray;
    },
    randomColor: function () {
      return `rgb(${Math.floor(Math.random() * 256)}, ${Math.floor(
        Math.random() * 256
      )}, ${Math.floor(Math.random() * 256)})`;
    },
    isMatched: function (startTarget, findTarget, mx, my) {
      if (!startTarget || !findTarget) {
        return false;
      }
      if (!(startTarget instanceof HTMLElement)) {
        return false;
      }
      if (!(findTarget instanceof HTMLElement)) {
        return false;
      }

      let fnd = Puzzle.puzzleList.find((it) => it.item == startTarget);
      let targetLeft = fnd.rect.left;
      let targetTop = fnd.rect.top;
      let targetRight = fnd.rect.right;
      let targetBottom = fnd.rect.bottom;
      if (
        mx > targetLeft &&
        mx < targetRight &&
        my > targetTop &&
        my < targetBottom
      ) {
        return fnd;
      }
      return false;
    },
  };

  return INSTANCE;
})();
