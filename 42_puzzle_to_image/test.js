

function dropHandler(ev) {
  ev.preventDefault();
  console.log(ev);
  if (ev.dataTransfer.items) {
    [...ev.dataTransfer.items].forEach((item, i) => {
      if (item.kind === "file") {
        const file = item.getAsFile();
        setImage(file);
      }
    });
  } else {
    [...ev.dataTransfer.files].forEach((file, i) => {
        setImage(file);
    });
  }
}
function dragOverHandler(ev) {
  ev.preventDefault();
}
function setImage(file){
    if (FileReader && file) {
        var fr = new FileReader();
        fr.onload = function () {
          document.getElementById('preview-image-tag').src = fr.result;
        }
        fr.readAsDataURL(file);
    }
}




const UI = {
  previewZone :document.getElementById('preview-zone'), 
  previewImageTag :document.getElementById('preview-image-tag'), 
  dragAndDropZone :document.getElementById('drag-and-drop-zone'), 
  gridContainer :document.getElementById('grid-container'), 
  gridConatianerAnimate :document.getElementById('grid-conatianer-animate'), 
  puzzleResetButton :document.getElementById('puzzle-reset-button'), 
  puzzleSuffleButton :document.getElementById('puzzle-suffle-button'), 
  puzzleAnimateButton :document.getElementById('puzzle-animate-button'), 
  puzzleDrawButton :document.getElementById('puzzle-draw-button'), 
}
UI.puzzleResetButton.addEventListener('click', function(e){
  e.preventDefault();
  PuzzleEngine.reset();
})
UI.puzzleSuffleButton.addEventListener('click',function(e){
  e.preventDefault();
  PuzzleEngine.setImage(UI.previewImageTag)
    .then(function (rs) {
      console.log("setImage", rs);
    })
    .catch(function (err) {
      console.error("setImage", err);
    });
})
UI.puzzleAnimateButton.addEventListener('click' , function(e){
  e.preventDefault();
  PuzzleEngine.suffle()
    .then(function (rs) {
      console.log("setImage", rs);
    })
    .catch(function (err) {
      console.error("setImage", err);
    });
})

if (document.readyState == 'complete'){
  PuzzleEngine.init({
    previewZone: UI.previewZone,
    gridContainer: UI.gridContainer
  })
} else {
  document.addEventListener('DOMContentLoaded',function(){
    PuzzleEngine.init({
      previewZone: UI.previewZone,
      gridContainer: UI.gridContainer
    })
  })
}




