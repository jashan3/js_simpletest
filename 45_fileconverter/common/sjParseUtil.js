const msgToName = (msg) =>{
    const mlast = msg.split('/')
    const last = mlast.pop();
    if (last){
        const list = last.split('.');
        if (list && Array.isArray(list)){
            return { name: list[0],type: list[1].toLowerCase() }
        }
    }
    return { name: null, type: null };
}


module.exports = {
    msgToName
}