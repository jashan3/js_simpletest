const fs = require('fs');
const { parseStringPromise } = require('xml2js');
const path = require('path');
const { unzipFile } = require('./sjFileUtil');
const { createTempThumbnail } = require('./sjImageUtils');

/**
 * rootfile이 안찾아질때까지 돌면 찾는 기능
 * @param {*} files 
 * @returns 
 */
const findRecursiveData = ( files) =>{
  let find = null;
  if (files && Array.isArray(files)){
    for (let index = 0; index < files.length; index++) {
      const { $ , rootfile } = files[index];
      if (files && Array.isArray(rootfile)){
        return findRecursiveData(rootfile)
      }
      find = $;
      break;
    }
  }
  return find;
}
/**
 * manifest 파일 읽는 기능
 * @param {*} fullOpfPath opf 파일에 전체경로
 * @param {*} mimeType 
 * @returns 
 */
const readOpfManifestFile = async (fullOpfPath, mimeType = 'image') =>{
  if (fs.existsSync(fullOpfPath)){
    const opfData = fs.readFileSync(fullOpfPath,'utf-8');
    const opfXMLData = await parseStringPromise(opfData);
    if (opfXMLData){
      const { item } = opfXMLData?.package?.['manifest'][0];
      if (item && Array.isArray(item)){
          const filteredItems = item.filter((it) =>{
          const mediaType = it.$['media-type'];
          return mediaType && mediaType.includes(mimeType);
        }).map((it) => {
          const { id, href } = it.$;
          const mediaType = it.$['media-type'];
          return { id, href , mediaType };
        })
        return filteredItems;
      }
    }
  }
  return null;
}

/**
 * metadata 읽는 기능
 * @param {*} fullOpfPath opf 파일에 전체경로
 * @returns {[] | null } metadata array형식으로 반환
 */
const readOpfMetadataFile = async ( fullOpfPath ) =>{
  if (fs.existsSync(fullOpfPath)){
    const opfData = fs.readFileSync(fullOpfPath,'utf-8');
    const opfXMLData = await parseStringPromise(opfData);
    if (opfXMLData){
      const metadata = opfXMLData?.package?.['metadata'][0];
      const keysList = Object.keys(metadata);
      const resultList = []
      for (let index = 0; index < keysList.length; index++) {
        const key = keysList[index];
        if (key.startsWith('dc:')){
          const parsedKey = key.replace('dc:','');
          resultList.push({ key: parsedKey, value: metadata[key] })
        }
      }
      return resultList;
    }
  }
  return null;
}
/**
 * 메타데이터 중 key로 찾아 읽는 기능
 * @param {*} fullOpfPath opf 파일에 전체경로
 * @returns 
 */
const readOpfMetadataFileFindByKey = async ( fullOpfPath , value = '') =>{
  let epubTitle = 'TEST BOOK';
  const metadata = await readOpfMetadataFile(fullOpfPath);
  if (metadata){
    if (metadata && Array.isArray(metadata)){
      const { value } = metadata.find(it=>it.key === 'title');
      epubTitle =  value.join('');
    }
  }
  return epubTitle
}
/**
 * epub 커버이미지의 상대 경로를 찾는기능
 * @param {*} fullOpfPath 
 * @returns 
 */
const readImageRelativePathFromOpf = async(fullOpfPath) =>{
  const opfFileList = await readOpfManifestFile(fullOpfPath,'image');
  const findCoverImage = opfFileList.find(({ id, href, mediaType })=> id.includes('cover'));
  if (findCoverImage){
    const { href } = findCoverImage;
    return href;
  }
  return null;
}

async function generateEpubThumbnail( epubFilePath, outputPath, rename ) {
  try {
    if (!fs.existsSync(outputPath)){
      fs.mkdirSync(outputPath, { recursive: true })
    }
    /**
     * 압축 해제를 위한 임시 파일 생성
     */
    const tempDir = path.join(outputPath, 'temp_epub_extraction' + '_' + Date.now().valueOf());
    fs.mkdirSync(tempDir, { recursive: true });

    /**
     * 압축해제
     */
    await unzipFile(epubFilePath,tempDir);

    /**
     * opf파일 경로를 위해 container.xml을 뒤진다.
     */
    const containXML = `${tempDir}/META-INF/container.xml`;
    const data = fs.readFileSync(containXML);
    const containerXMLData = await parseStringPromise(data);
    const { $ , rootfiles } = containerXMLData.container;
    const opfPath = findRecursiveData(rootfiles);
    const opfPathList = opfPath['full-path'].split('/');
    opfPathList.pop();

    //epub 파일에 root path
    const relativeEpubRootPath = opfPathList.join('/');
    //컨텐츠가 정보가 있는 opf 파일 경로
    const fullOpfPath = path.join(tempDir,opfPath['full-path']);
    // 원본 상대이미지 경로
    const relativeCoverHref = await readImageRelativePathFromOpf(fullOpfPath);
    
    // 복사될 커버이미지 경로
    const outputImagePath = path.join(outputPath,rename);
    if (relativeCoverHref) {
      // 원본 커버이미지 경로
      const coverImagePath = path.join( tempDir, relativeEpubRootPath, relativeCoverHref );
      if (fs.existsSync(coverImagePath)) {
        fs.copyFileSync(coverImagePath, outputImagePath);
        console.log(`SUCCESS ===> ${outputImagePath}`);
      } else {
        const epubTitle = await readOpfMetadataFileFindByKey(fullOpfPath);
        await createTempThumbnail(outputImagePath, epubTitle);
        console.error(`NOT FOUND, MAKE TEMP FILE ===> ${outputImagePath}`);
      }
    } else {
      const epubTitle = await readOpfMetadataFileFindByKey(fullOpfPath);
      await createTempThumbnail(outputImagePath, epubTitle);
      console.error(`NOT FOUND, MAKE TEMP FILE ===> ${outputImagePath}`);
    }
    
    fs.rmSync(tempDir, { recursive: true });
    return outputImagePath;
  } catch (error) {
    console.error('Error generating thumbnail' + `[${rename}] => `, error);
    return false;
  }
}


module.exports = {
    generateEpubThumbnail
}