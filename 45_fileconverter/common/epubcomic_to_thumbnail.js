const fs = require("fs");
const sharp = require("sharp");
const path = require("path");
const { unzipFile, zipFolder } = require("./sjFileUtil");

const generateEpubcomicThumbnail = (input, outputPath, rename) => {
  return new Promise((resolve, reject) => {
    const tempDir = path.join(
      outputPath,
      "temp_epub_extraction" + "_" + Date.now().valueOf()
    );
    if (fs.existsSync(tempDir)) {
      fs.mkdirSync(tempDir, { recursive: true });
    }
    unzipFile(input, tempDir)
      .then((rs) => {
        fs.readdir(tempDir, function (err, files) {
          if (err) {
            return reject(err);
          }
          if (files.length == 0) {
            return reject(new Error("empty folder"));
          }
          let fileName = "";
          for (let index = 0; index < files.length; index++) {
            const file = files[index];
            if (file.includes("thum")) {
              const imgPath = path.join(tempDir, file);
              if (!fileName) {
                fileName = file;
                const copyImgPath = path.join(outputPath, rename);
                if (fs.existsSync(imgPath)){
                    fs.copyFileSync(imgPath, copyImgPath);
                }
              }
              fs.rmSync(imgPath);
            }
          }

          const name = rename.split("-")[0] + ".ZIP";
          const copyImgPath = path.join(outputPath, name);
          zipFolder(tempDir, copyImgPath)
            .then((rs) => {
              if (fs.existsSync(tempDir)) {
                fs.rmSync(tempDir, { recursive: true });
              }
              resolve();
            })
            .catch((err) => {
              if (fs.existsSync(tempDir)) {
                fs.rmSync(tempDir, { recursive: true });
              }
              reject(err);
            });
        });
      })
      .catch((err) => {
        if (fs.existsSync(tempDir)) {
          fs.rmSync(tempDir, { recursive: true });
        }
        reject(err);
      });
  });
};

module.exports = {
  generateEpubcomicThumbnail,
};
