const fs = require('fs');
const sharp = require('sharp');
const unzipper = require('unzipper');
const path = require('path');
const zlib = require('zlib');
const tar = require('tar'); 
const AdmZip = require('adm-zip');
const { listFiles } = require('./sjFileUtil');

const generateComicThumbnail = (input, outputPath, rename) =>{
  return new Promise((resolve,reject)=>{
    let inputDir = input.split('/')
    let inputName = inputDir.pop();
    let inputNameList = inputName.split('.');
    inputNameList.pop();
    inputName = 'temp_' + inputNameList.join('');
    inputDir = inputDir.join('/')
    const extractionPath = path.join(inputDir,inputName)
    if (!fs.existsSync(extractionPath)) {
      fs.mkdirSync(extractionPath, { recursive: true });
    }
    const zip = new AdmZip(input);
    // 압축 해제
    zip.extractAllTo(extractionPath, true);
    listFiles(extractionPath).then(list=>{
      const thumb = list.find(it=>it.includes('.jpg'));
      const coverImagePath = path.join(extractionPath,thumb);
      if (!fs.existsSync(outputPath)) {
        fs.mkdirSync(outputPath, { recursive: true });
      }

      const copyImagePath = path.join(outputPath,'temp_'+rename);
      const copyImagePath2 = path.join(outputPath,rename);
      fs.copyFileSync(coverImagePath,copyImagePath);
      fs.rmSync(extractionPath, { recursive: true });
      const resizeOptions = {
        width: 300,
        height: 350,
        fit: sharp.fit.inside,
      };
  
      sharp(copyImagePath)
      .resize(resizeOptions)
      .toFile(copyImagePath2, (err, info) => {
        if (err) {
          reject(err)
        } else {
          fs.rmSync(copyImagePath);
          resolve(info);
        }
      });
    }).catch(err=>{
      reject(err)
    })
  })

}


 module.exports = {
    generateComicThumbnail
 }