const fs = require("fs");
const JSZip = require("jszip");
const path = require("path");
const archiver = require("archiver");

async function listFiles(directoryPath) {
  try {
    const files = await fs.readdirSync(directoryPath);
    return files;
  } catch (err) {
    console.error("Error reading directory:", err);
  }
}
/**
 *
 * @param {*} from 압축해제하려는 파일 경로
 * @param {*} to 압축해제위치
 * @returns
 */
const unzipFile = (from, to) => {
  return new Promise((resolve, reject) => {
    fs.readFile(from, (err, data) => {
      if (err) {
        console.error("Error reading zip file:", err);
        return;
      }
      const zip = new JSZip();
      zip
        .loadAsync(data)
        .then((zipContents) => {
          if (!fs.existsSync(to)) {
            fs.mkdirSync(to, { recursive: true });
          }
          const promises = [];
          zipContents.forEach((relativePath, file) => {
            const filePath = `${to}/${relativePath}`;
            if (file.dir) {
              fs.mkdirSync(filePath, { recursive: true });
            } else {
              const promise = file.async("nodebuffer").then((content) => {
                const directory = path.dirname(filePath);
                if (!fs.existsSync(directory)) {
                  fs.mkdirSync(directory, { recursive: true });
                }
                fs.writeFileSync(filePath, content);
              });
              promises.push(promise);
            }
          });
          Promise.all(promises)
            .then(() => {
              resolve();
            })
            .catch((unzipErr) => {
              reject(unzipErr);
            });
        })
        .catch((loadErr) => {
          reject(loadErr);
        });
    });
  });
};

const zipFolder = (folder, zip) => {
  return new Promise((resolve, reject) => {
    const outputZipStream = fs.createWriteStream(zip);
    const archive = archiver("zip", {
      zlib: { level: 9 }, // Compression level (0-9)
    });
    outputZipStream.on("close", () => {
      resolve(folder, zip);
    });
    archive.on("error", (err) => {
      reject(err);
    });
    archive.pipe(outputZipStream);
    archive.directory(folder, false);
    archive.finalize();
  });
};

module.exports = {
  listFiles,
  unzipFile,
  zipFolder
};
