const fs = require("fs");
const path = require("path");
const sharp = require("sharp");

/**
 * 임시 썸네일 생성
 * @param {*} destination
 * @param {*} title
 * @param {*} width
 * @param {*} height
 * @returns
 */
const createTempThumbnail = (destination, title, width = 300, height = 400) => {
  const textColor = "#ffffff"; // Text color
  const fontSize = 24;
  const text = title; // Text to write
  return new Promise((resolve, reject) => {
    sharp({
      create: {
        width: width,
        height: height,
        channels: 4, // 4 channels for RGBA color
        background: { r: 52, g: 152, b: 219, alpha: 1 }, // Color in RGBA format
      },
    })
      .composite([
        {
          input: Buffer.from(
            `<svg width="${width}" height="${height}" xmlns="http://www.w3.org/2000/svg"><text x="50%" y="50%" text-anchor="middle" alignment-baseline="middle" font-family="Arial" font-size="${fontSize}" fill="${textColor}">${text}</text></svg>`
          ),
          left: 0,
          top: 0,
        },
      ])
      .toFile(destination, (err, info) => {
        if (err) {
          reject(err);
        } else {
          resolve(info);
        }
      });
  });
};

module.exports = {
    createTempThumbnail
}
