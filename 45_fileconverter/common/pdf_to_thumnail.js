
const fs = require('fs');
const sharp = require('sharp');
const path = require('path');
const { pdftobuffer } = require('pdftopic');

const generatePdfThumbnail = (input, outputPath, rename) => {
  return new Promise((resolve,reject)=>{
    const resizeOptions = {
      width: 300, 
      height: 200, 
      fit: 'inside', 
    };
    const output = path.join(outputPath,'temp_'+rename);
    const output2 = path.join(outputPath,rename);
    const pdf = fs.readFileSync(input, null);
    
    if (!fs.existsSync(outputPath)) {
      fs.mkdirSync(outputPath, { recursive: true });
    }
    pdftobuffer(pdf, 0).then(buffer=>{
      fs.writeFileSync(output, buffer, null);
      sharp(output)
      .resize(resizeOptions)
      .toFile(output2, (err, info) => {
        if (err) {
          reject(err)
        } else {
          fs.rmSync(output);
          resolve(info);
        }
      });
    }).catch(err=>{
      reject(err)
    })
  })
};

module.exports = {
  generatePdfThumbnail,
};
