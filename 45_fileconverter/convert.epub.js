const path = require('path');
const { listFiles } = require('./common/sjFileUtil');
const { msgToName } = require('./common/sjParseUtil');

const { generateEpubThumbnail } = require('./common/epub_to_thumnail');
const { generatePdfThumbnail } = require('./common/pdf_to_thumnail');
const { generateComicThumbnail } = require('./common/zip_to_thumnail');
const { generateEpubcomicThumbnail } = require('./common/epubcomic_to_thumbnail');


// const ROOT_PATH = '/Users/hansangjun/Desktop/test'
// const ROOT_PATH = "/Users/hansangjun/Downloads/0913";
const ROOT_PATH = "/Users/hansangjun/Desktop/testepub";

const INPUT = {
    EPUB: ROOT_PATH + "/epub",
    PDF: ROOT_PATH + "/pdf",
    COMIC: ROOT_PATH + "/comic",
    EPUB_COMIC: ROOT_PATH + "/epubcomic",
}

const OUTPUT = {
    EPUB: ROOT_PATH + "/epub_out",
    PDF: ROOT_PATH + "/pdf_out",
    COMIC: ROOT_PATH + "/comic_out",
    EPUB_COMIC: ROOT_PATH + "/epubcomic_out",
}

const runFiles = async (input,output) => {
    /**
     * read file List
     */
    const list = await listFiles(input);
    const listCount = list.length;
    for (let index = 0; index < listCount; index++) {
      const item = list[index];
      if (!item){
        continue;
      }
      if (item.toLowerCase().includes(".ds_store")) {
        continue;
      }
      const { name, type } = msgToName(item);
      const rename = name + "-" + type + ".jpg";
      const inputItem = input + "/" + item;
      if (type == 'epub'){
        try {
          const result = await generateEpubThumbnail( inputItem, output, rename );
        } catch(err){
          console.error('fail generate epub thumbnail in generateEpubThumbnail',err);
        }
      } else if (type == 'pdf'){
        try {
          generatePdfThumbnail( inputItem, output, rename ).then(rs=>{
            console.log('generate thumbnail ==> ',rs);
          }).catch(err=>{
            console.error('fail generate pdf thumbnail in generatePdfThumbnail',err);
          })
        } catch(err){
          console.error('fail generate pdf thumbnail in generatePdfThumbnail',err);
        }
      } else if (type == 'comic'||type == 'zip'){
          generateComicThumbnail(inputItem, output, rename).then(rs=>{
            console.log('generate thumbnail ==> ',rs);
          }).catch(err=>{
            console.error('fail generate comic thumbnail in generateComicThumbnail',err);
          })
      } else if (type == 'epub_comic'){
        const rename = name + "-zip.jpg";
        generateEpubcomicThumbnail(inputItem, output, rename).then(rs=>{
          console.log('generate thumbnail ==> ',rs);
        }).catch(err=>{
          console.error('fail generate Epubcomic Thumbnail in generateEpubcomicThumbnail',err);
        })
      }
    }


    return true;
} 

const run = async () => {
  return epubRunFiles()
    .then((rs) => pdfRunFiles())
    .then((rs) => comicRunFiles())
};
const epubRunFiles = async () => {
    const epubInputDir = INPUT.EPUB;
    const epubOutputDir = path.join(OUTPUT.EPUB,"output_" + Math.floor(Date.now() / 1000));
    const epubRunFiles = await runFiles(epubInputDir,epubOutputDir);
    return epubRunFiles;
}

const pdfRunFiles = async () =>{
    const pdfInputDir = INPUT.PDF;
    const pdfOutputDir = path.join(OUTPUT.PDF,"output_" + Math.floor(Date.now() / 1000));
    const pdfRunFiles = await runFiles(pdfInputDir,pdfOutputDir);
    return pdfRunFiles;
}
const comicRunFiles = async () =>{
  const pdfInputDir = INPUT.COMIC;
  const pdfOutputDir = path.join(OUTPUT.COMIC,"output_" + Math.floor(Date.now() / 1000));
  const pdfRunFiles = await runFiles(pdfInputDir,pdfOutputDir);
  return pdfRunFiles;
}

const epubcomicRunFiles = async () =>{
  const pdfInputDir = INPUT.EPUB_COMIC;
  const pdfOutputDir = path.join(OUTPUT.EPUB_COMIC,"output_" + Math.floor(Date.now() / 1000));
  const pdfRunFiles = await runFiles(pdfInputDir,pdfOutputDir);
  return pdfRunFiles;
}

// run().then(rs=>{
//   console.log('rs',rs);
// }).catch(err=>{
//   console.error(err);
// })
// pdfRunFiles().then(rs=>{
//     console.log('rs',rs);
// }).catch(err=>{
//     console.error(err);
// })
run().then(rs=>{
  console.log('rs',rs);
}).catch(err=>{
  console.error(err);
})
// epubcomicRunFiles()
//   .then((rs) => {
//     console.log("rs", rs);
//   })
//   .catch((err) => {
//     console.error(err);
//   });