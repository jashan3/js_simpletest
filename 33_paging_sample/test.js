/**
 * DataSource Class
 */
const PagingRepo = {
    total: 234,   // 전체 갯수
    perPage: 10, // 10개씩 보기
    pagingCount: 0, // 페이징 갯수
    pagingCurrent: 0, // 현재 페이징
    perPagingGroup: 10, // 보여질 페이징 버튼 갯수
    pageGroupTotal: 0, //  페이징 그룹 전체 갯수
    pageGroupIndex: 0, //  페이징 그룹 인덱스
    startOfPagingGroup: 0, // 페이징 그룹 시작 인덱스
    endOfPagingGroup: 0,
    offset: 0,
    limit: 10,
    init: function(){
        this.update();
    },
    setPage: function(page){
        this.pagingCurrent = page;
        this.update();
    },
    update : function(){
        this.pagingCount = Math.ceil(this.total / this.perPage);
        this.pageGroupTotal = Math.ceil(this.pagingCount / this.perPagingGroup);
        this.startOfPagingGroup = this.pageGroupIndex * this.perPagingGroup;
        this.endOfPagingGroup = Math.min( this.pagingCount,this.startOfPagingGroup + this.perPagingGroup)
        this.offset = this.pagingCurrent * this.perPage;
        this.limit = this.offset + this.perPage;
    },
    next: function(){
        PagingRepo.pageGroupIndex+=1;
        let c = PagingRepo.pageGroupIndex*PagingRepo.perPagingGroup
        PagingRepo.setPage(c);
    },
    prev: function(){
        PagingRepo.pageGroupIndex-=1;
        let c = (PagingRepo.pageGroupIndex+1)*PagingRepo.perPagingGroup -1;
        PagingRepo.setPage(c);
    },
    hasNext: function(){
        if (PagingRepo.pagingCount > PagingRepo.perPagingGroup){
            //last
            if (PagingRepo.pagingCount == PagingRepo.endOfPagingGroup){

            } else {
                return true;
            }
        }
        return false;
    },
    hasPrev: function(){
        return PagingRepo.startOfPagingGroup > 0
    },
    // log: function(){
    //     console.table({
    //         total: this.total,
    //         // perPage: this.perPage,
    //         // pagingCount: this.pagingCount,
    //         // pageCurrent: this.pagingCurrent,
    //         // perPagingGroup: this.perPagingGroup,
    //         // pageGroupTotal: this.pageGroupTotal,
    //         // GroupIndex: this.pageGroupIndex,
    //         offset: this.offset,
    //         limit: this.limit,
    //         // group_start: this.startOfPagingGroup,
    //         // group_end: this.endOfPagingGroup
    //     })
    // },
}

/**
 * UI Builder class
 */
const PagingBuilder = {
    PagingIndexAttr: "page-index-btn",
    callbackDelegate: {
        onClickNext: null,
        onClickPrev: null,
        onClickPage: null,
    },
    pagingWrap: function(){
        const div = document.createElement('div');
        div.style.minWidth = "500px";
        return div;
    },
    nextButton: function(buttonText = "다음"){
        const btn2 = document.createElement('button');
        btn2.textContent = buttonText
        btn2.addEventListener('click', (e)=>{
            e.preventDefault();
            if (typeof PagingBuilder.callbackDelegate.onClickNext == "function" ){
                PagingBuilder.callbackDelegate.onClickNext();
            }
        })
        return btn2;
    },
    prevButton: function(buttonText = "이전"){
        const btn3 = document.createElement('button');
        btn3.textContent = buttonText
        btn3.addEventListener('click', (e)=>{
            e.preventDefault();
            if (typeof PagingBuilder.callbackDelegate.onClickPrev == "function" ){
                PagingBuilder.callbackDelegate.onClickPrev();
            }
        })
        return btn3
    },
    pageButtons: function(){
        const docf = document.createDocumentFragment();
        for (let start = PagingRepo.startOfPagingGroup; start < PagingRepo.endOfPagingGroup; start++) {
            const btn = document.createElement('button');
            btn.setAttribute(PagingBuilder.PagingIndexAttr,start);
            btn.textContent = start+1;
            btn.addEventListener('click', (e)=>{
                e.preventDefault();
                const page = parseInt(btn.getAttribute(PagingBuilder.PagingIndexAttr));
                if (typeof PagingBuilder.callbackDelegate.onClickPage == "function" ){
                    PagingBuilder.callbackDelegate.onClickPage(page);
                }
            })
            docf.appendChild(btn);
        }
        return docf;
    },
}

/**
 * merge Data & UI
 */
const PagingViewHolder = {
    parentElement: null,
    init: function(parent){
        if (!parent){
            throw new Error("parent must be defined!")
        }
        if (parent instanceof HTMLElement){
            PagingViewHolder.parentElement = parent
        } else if (typeof parent == "string"){
            PagingViewHolder.parentElement = document.getElementById(parent)
        }
        PagingRepo.init();
        PagingBuilder.callbackDelegate = {
            onClickNext: function(){
                PagingRepo.next();
                PagingViewHolder.reload();
            },
            onClickPrev: function(){
                PagingRepo.prev();
                PagingViewHolder.reload();
            },
            onClickPage: function(page){
                PagingRepo.setPage(page);
                PagingViewHolder.reload();
                console.log(page);
            },
        }
        PagingViewHolder.reload();
    },
    reload: function(){
        if (! PagingViewHolder.parentElement){
            throw new Error("parent must be defined!")
        }
        PagingViewHolder.parentElement.innerHTML = ""
        const w = PagingBuilder.pagingWrap();
        //paging
        w.appendChild(PagingBuilder.pageButtons());
        //next
        if (PagingRepo.hasNext()){
            w.appendChild(PagingBuilder.nextButton());
        }
        //prev
        if (PagingRepo.startOfPagingGroup > 0){
            w.prepend(PagingBuilder.prevButton());
        }
        PagingViewHolder.parentElement.appendChild(w);
    }
}

document.addEventListener('DOMContentLoaded' , ()=>{
    PagingViewHolder.init("btn_wrapper");
})