
const express = require('express');
const app = express();
const indexRouter = require('./routes/index');
const port = 10000;

const webpack = require("webpack");
const webpackConfig = require("./webpack.config.js");
const compiler = webpack(webpackConfig);
const webpackDevMiddleware = require("webpack-dev-middleware");

// view 경로 설정
app.set('views', __dirname + '/public');

// 화면 engine을 ejs로 설정
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(
    webpackDevMiddleware(compiler, {
      publicPath: webpackConfig.output.publicPath,
      stats: { colors: true },
    })
  );
// asset 경로 지정
app.use(express.static(__dirname));

//router 경로 지정
app.use('/', indexRouter);


app.listen(port, function () {
    console.log('Example app listening on port : ' + port,__dirname);
});


module.exports = app;