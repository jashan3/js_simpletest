// import * as THREE from 'three';
import * as THREE from 'three';

import{
    OrbitControls
} from 'three/examples/jsm/controls/OrbitControls.js'

/**
 * MAIN 랜더링
 */
const renderer = new THREE.WebGLRenderer({
    alpha: true,
    antialias: true
});
renderer.setSize(window.innerWidth,window.innerHeight);
renderer.shadowMap.enabled = true;

/**
 * Scene
 */
const scene = new THREE.Scene();

/**
 * 카메라 값 세팅
 * fov 시야각
 * aspect 비율
 * near 최소거리
 * far  최대거리
 */
const camera = new THREE.PerspectiveCamera(
    120,
    window.innerWidth/window.innerHeight,
    0.1,
    1000)
camera.position.set(0,2,5);

/**
 * 대상의 움직임
 */
const orbit = new OrbitControls(camera,renderer.domElement)

/**
 * 디버깅용
 * 좌표를 이미지화 해서 보여줌
 */
const axesHelper = new THREE.AxesHelper(5)
const gridHelper = new THREE.GridHelper(30)
scene.add(axesHelper);
scene.add(gridHelper);

/**
 * create component
 */
const box = CreateBox();
const plane = CreatePlane();
const icosahedron = CreateIcosahedron();
plane.rotation.x = -0.5 * Math.PI;

function CreatePlane(){
    const geo = new THREE.PlaneGeometry(30,30);
    const meterial = new THREE.MeshBasicMaterial({color: 0xffffff});
    return new THREE.Mesh(geo,meterial);
}
function CreateBox(){
    const boxGeometry = new THREE.BoxGeometry(1,1,1);
    const boxMaterial = new THREE.MeshBasicMaterial({color: 0x0FF00});
    let obj = new THREE.Mesh(boxGeometry,boxMaterial);
    return obj
}
function CreateIcosahedron(){
    const boxGeometry = new THREE.IcosahedronGeometry(0.5,0);
    const boxMaterial = new THREE.MeshStandardMaterial({color:  0x004fff})
    let obj = new THREE.Mesh(boxGeometry,boxMaterial);
    obj.position.set(1.5,1.5,0);
    obj.castShadow = true;
    obj.receiveShadow = true;
    return obj
}


/**
 * Add componet
 */
scene.add(box);
scene.add(plane);
scene.add(icosahedron);

/**
 * validate ui
 */
orbit.update();
function animate(time){
    box.rotateX.x = time / 1000;
    box.rotateX.y = time / 1000;
    renderer.render(scene,camera);
    orbit.update();
}

renderer.setAnimationLoop(animate);
document.body.appendChild(renderer.domElement);