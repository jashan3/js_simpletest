 

const path = require("path");
//HTML 플러그인
const HtmlWebpackPlugin = require("html-webpack-plugin");
/**
 * webpack
 */
 const webpack = require("webpack");
 const {  CleanWebpackPlugin } = require('clean-webpack-plugin');
 const CopyWebpackPlugin = require('copy-webpack-plugin')
 const HtmlInlineScriptPlugin = require('html-inline-script-webpack-plugin');
 const HtmlMinimizerPlugin = require("html-minimizer-webpack-plugin");
 const TerserPlugin = require("terser-webpack-plugin");
//  const InjectHtmlPlugin = require('inject-html-webpack-plugin')
 const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
 
 
 //css
 const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
 const MiniCssExtractPlugin = require('mini-css-extract-plugin');
 const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
 var HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
 const InlineChunkHtmlPlugin = require("inline-chunk-html-plugin")
 const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default;
 const CopyPlugin = require("copy-webpack-plugin");
 
 // svg
//  const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');

module.exports = {
  mode: "development",
  devtool: "source-map", //크롬에서 디버깅 가능하도록 원본코드같이 bundle
  entry: "./asset/js/three.view.js", //진입점
  output: {
    path: path.resolve(__dirname, "public"), // bundle만들어질 장소
    filename: "./asset/js/index.bundle.js", // bundle 될 파일 이름
  },
  module: {
    rules: [
      //es5 Loader
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env'],
          },
        },
      },
    //   {
    //     test: /\.(sa|sc|c)ss$/, //scss,sass,css templating
    //     use: [MiniCssExtractPlugin.loader,"css-loader", "sass-loader"],
    //   },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./views/index.html", //html  webpack플러그인을 통해 html 파일도 함께 bundle
    }),
    // new MiniCssExtractPlugin({ // style 태그 대신 css 파일도 함께 bundle
    //   filename: 'style.css',
    //   chunkFilename: 'style.css',
    // }),
  ],
};