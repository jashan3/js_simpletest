// Initialize Three.js scene
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Initialize XR
// const xrButton = new XRButton({ text: 'Start XR' });
// document.body.appendChild(xrButton.domElement);

let xrSession = null;
let xrFaceTracker = null;

// Function to start XR session
// async function startXR() {
//     if (xrSession === null) {
//         xrSession = await navigator.xr.requestSession('immersive-vr');
//         xrButton.setSession(xrSession);
//         xrFaceTracker = new XRFaceTracker(xrSession);
//         xrFaceTracker.addEventListener('facetracking', onFaceTracking);
//         xrFaceTracker.start();
//     }
// }

// Function to handle face tracking data
function onFaceTracking(event) {
    const faces = event.faces;

    if (faces.length > 0) {
        const face = faces[0]; // Assuming only one tracked face

        // Extract face position and rotation
        const position = face.transform.position;
        const rotation = face.transform.rotation;

        // Update the position and rotation of a 3D object (e.g., a cube)
        // Replace this with your 3D model and manipulation
        const cube = new THREE.Mesh(new THREE.BoxGeometry(0.2, 0.2, 0.2), new THREE.MeshBasicMaterial({ color: 0xff0000 }));
        cube.position.copy(position);
        cube.rotation.copy(rotation);
        scene.add(cube);
    }
}

// Handle window resize
window.addEventListener('resize', () => {
    const { innerWidth, innerHeight } = window;
    camera.aspect = innerWidth / innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(innerWidth, innerHeight);
});

// Main animation loop
function animate() {
    renderer.setAnimationLoop(render);
}

function render() {
    renderer.render(scene, camera);
}

// Start the XR session when the button is clicked
// xrButton.addEventListener('click', startXR);

// Set up the camera position
camera.position.z = 2;

// Start the animation loop
animate();
