const express = require('express'); 
const router = express.Router(); 


//file path
const FileService = require('../service/fileService')
const { dirname } = require('path');
const appDir = dirname(require.main.filename);
const fileLisPath = appDir + '/public/static/file/'
const musicLisPath = appDir + '/public/static/music/'
const mm = require('music-metadata');
const util = require('util');

router.get('/v1', (req, res) => { 
    res.send('Hello, User'); 
}); 

router.get('/fileList',async (req, res) => { 
    let fs = new FileService()
    let list = await fs.getfiles(fileLisPath)
    return res.json(list)
}); 

router.get('/musicanal',async (req,res)=>{
    let fs = new FileService()
    let list = await fs.getfiles(musicLisPath)

    for (file of list){
        const metadata = await mm.parseFile(musicLisPath+file.name);
        // console.log('######music file.name=> ',file.name)
        // console.log('######music list=> ',metadata)
        // const metadata2 = await mm.parseStream(someReadStream, {mimeType: 'audio/mpeg', size: 26838});

        // audio/wav
        const info = util.inspect(metadata, { showHidden: false, depth: null })

        
        console.log('@@file.name',file.name);
        console.log('@@metadata',metadata);
        console.log('@@exif',info);
    }

    return res.json(list)
})




module.exports = router;

