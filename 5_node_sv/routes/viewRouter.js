const express = require('express'); 
const path = require('path');
const fs =require('fs')
const router = express.Router(); 
const pdfCore = require('../public/script/pdfcore/pdfCore')



router.get('/', (req, res) => { 
    res.redirect('/main')
}); 

router.get('/main', (req, res , next) => { 
    res.render('main/index', { title: 'Express' })
}); 
router.get('/zoom', (req, res , next) => { 
    res.render('viewer/zoom', { title: 'Express' })
}); 

router.get('/music', (req, res , next) => { 
    res.render('viewer/music', { title: 'Express' })
}); 




router.get('/viewer/:name', (req, res , next) => { 

    let mName = req.params.name ?? ''
    let mUrl = '/static/file/'+mName
    let rending = 'viewer/pdf_ver6'
    let type = 0
    //pdf 
    if (mName.includes('.pdf') || mName.includes('.PDF')){
        type = 1
    } 
    //그외
    else {
        rending = 'viewer/zip'
    }
    const baseProjectPath = process.cwd();

    const pdfpath = path.join(baseProjectPath,'public','static','file',mName);

    fs.readFile(pdfpath,(err,pdfBuffer)=>{
        if (err){
            return err
        }
        pdfCore.getOutlines(pdfBuffer).then(data=>{
            data
            console.log(data);
        }).catch(err=>{
            console.log(err);
        })
    })
    console.log('####',baseProjectPath);
    let data = { name: mName, url: mUrl , type: type}
    res.render(rending, {data: data})
}); 

module.exports = router;

