// node_modules 에 있는 express 관련 파일을 가져온다.
var express = require('express')
var path = require('path')

/**
 * route
 */
//resource route
const resource = express.static(__dirname + '/public')

//api
const apiRouter = require('./routes/apiRouter')

//views
const viewRouter = require('./routes/viewRouter')


/**
 * app set
 */
// express 는 함수이므로, 반환값을 변수에 저장한다.
var app = express()

//ejs형식이 아닌 html 자체로 쓰고 싶을때 
app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');

app.use(resource)
app.use('/api', apiRouter)
app.use('/', viewRouter)


app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'content-type');
    next();
});

// 3000 포트로 서버 오픈
app.listen(3000, function() {
    console.log("start! express server on port 3000")
})