

/**
 * view base class
 */
export class BaseUIView {
    constructor(){
        if (this.constructor == BaseUIView) {
            throw new Error("Abstract classes can't be instantiated.");
        }
        this._ul = null
        this._li = null
        this._a = null
    }
    ul = (()=> {
        if (!this._ul) {
            this._ul = document.createElement('ul');
        }
        return this._ul.cloneNode(true);
    })()

    li = (()=> {
        console.log(this._li)
        if (!this._li){
            this._li = document.createElement('li');
        }
        return this._li.cloneNode(true);
    })()

    a = (()=> {
        if (!this._a) {
            this._a = document.createElement('a');
        }
        return this._a.cloneNode(true);
    })()
}

