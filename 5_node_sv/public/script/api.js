
/**
 * api를 위한 class
 */
export class API {

    static fetchFileList = async ()=> {
       return fetch('/api/fileList')
        .then(res=>res.json())
    }
}