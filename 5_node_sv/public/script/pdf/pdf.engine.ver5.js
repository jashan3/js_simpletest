
const RenderingStatus = Object.freeze({
    none:   Symbol(0),
    inRendering:  Symbol(2),
    complete: Symbol(3),
    error: Symbol(4),
    stop: Symbol(5),
    destory:  Symbol(6),
});


function RenderingTask(pageNumber, data, desireWidth, callback){
    this.pageNumber = pageNumber
    this.data = data
    this.desireWidth = desireWidth
    this.status = RenderingStatus.none
    this.callback = callback
    window.innerWidth * 0.7
}

RenderingTask.prototype = {
    start(){
        console.log('## RenderingTask',this.pageNumber,this.status )
        if (this.pageNumber == null){
            return;
        }
        if (this.status === RenderingStatus.inRendering){
            return;
        }

        if (!this.data){
            return;
        }

        
        this.status = RenderingStatus.inRendering

        let self = this
        let mStatus = self.status
        let mPage =  self.pageNumber



        this.data.then(function(pageTask){
            pageTask.cleanupAfterRender = true
            if (mStatus === RenderingStatus.inRendering){

            }

            let scale = 1 
            let ratio = window.devicePixelRatio;
            let zoom = scale *ratio;
            let vp = pageTask.getViewport({scale: zoom});
            
            let computedRatio = self.desireWidth / vp.width
            let vp2 = pageTask.getViewport({
                scale: computedRatio * ratio
            });

            let canvas = document.createElement('canvas')
            let renderContext = {
                canvasContext: canvas.getContext('2d'),
                viewport: vp2,
                transform: [1, 0, 0, 1, 0, 0],
            }


            let cWidth = Math.floor(vp2.width)
            let cHeight = Math.floor(vp2.height)
            canvas.width =  cWidth;
            canvas.height = cHeight;

            /**
             * style
             */
            let resizedHeight  = cHeight * self.desireWidth / cWidth
            canvas.style.width = self.desireWidth + 'px'
            canvas.style.height = resizedHeight + 'px'
   
            console.log('pageTask',pageTask)
            
            pageTask.getTextContent().then(rs=>{
                console.log(rs)
            })
            // getXfa
            // getTextContent
            // getJSActions
            pageTask.render(renderContext).promise.then(function(){
                console.log('## complete Rendering',mPage)
                pageTask.cleanup()
                // if (self.callback){
                //     self.callback(canvas , mPage , desireWidth , resizedHeight)
                // }
                let width = self.desireWidth
                let height = resizedHeight
                let item = document.querySelector('[data-page=\"'+mPage+'\"]')
                if (item){
                    let wrapper = item.querySelector('.canvasWrapper')
                    if (wrapper){
                        while (wrapper.firstChild) {
                            wrapper.removeChild(wrapper.firstChild);
                        }
                        item.style.width = width + 'px'
                        item.style.height = height + 'px'
                        wrapper.style.width = width + 'px'
                        wrapper.style.height = height + 'px'
                        wrapper.appendChild(canvas)                    
                    }
                }
            })
        })
    },
    stop(){
        this.status = RenderingStatus.stop
    },
    destory(){
        this.status = RenderingStatus.destory

        this.pageNumber = null
        this.data = null
        this.status = null
        this.callback = null
    },
    getItem(){
        let div = document.createElement('div')
        div.classList.add('canvasWrapper')
        return div
    }
}

/**
 * pdf module
 */
 var PdfEngine = PdfEngine || (function(){
    //instance
    var SHARED = {};

    var Config = SHARED.Config = {

        element: {
            viewerWrapper: document.querySelector('.viewer-wrapper'),
            scrollLayer: document.getElementById('viewer-scroll-mode-wrapper'),

            currentPage: document.getElementById('current-page')
        },

        /**
         * 워커 경로
         */
        workerSrc: 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.13.216/pdf.worker.min.js',

        /*
            뷰어상태
        */
        status: {
            scrollInfo: null,
            pdf: null,
        },

        useChunked: false,
        init(url , token){
            var ChunkedParams = null
            if (Config.useChunked){
                ChunkedParams = {
                    "url": url,
                    "cMapPacked": true,
                    "cMapUrl": "../lib/cmaps/",
                    "disableAutoFetch": true,
                    "disableFontFace": false,
                    "disableRange": false,
                    "disableStream": true,
                    "docBaseUrl": "",
                    "enableXfa": true,
                    "fontExtraProperties": false,
                    "isEvalSupported": true,
                    "maxImageSize": -1,
                    "pdfBug": false,
                    "standardFontDataUrl": "../pdf/lib/standard_fonts/",
                    "verbosity": 1,
                    "httpHeaders": { "authorization": token },
                    "withCredentials": true
                }
            } 
            //pdf 워커 위치
            pdfjsLib.GlobalWorkerOptions.workerSrc = Config.workerSrc
            if (ChunkedParams){
                pdfjsLib.getDocument(ChunkedParams).promise.then(Config.onLoadPdf)
            } else {
                pdfjsLib.getDocument({
                    url: url,
                    maxImageSize: -1,
                    disableAutoFetch: true,
                    disableFontFace: false,
                    disableRange: false,
                    disableStream: true
                }).promise.then(Config.onLoadPdf)
            }
        },

        onLoadPdf(pdf){
            Config.status.pdf = pdf

            Scroll.initScroll()
        }

    }

    

    /**
     * 
     * 
     */
    var Scroll = SHARED.Scroll = {
        /**
         *  작업들.
         */ 
        tasks: new Array(),

        beforeInit: function(){
          let canvas = document.createElement('canvas') 
          Config.status.pdf.getPage(pageNum).then(function(pageTask){
            let scale = 1 
            let ratio = window.devicePixelRatio;
            let zoom = scale *ratio;
            let vp = pageTask.getViewport({scale: zoom});
            let renderContext = {
                canvasContext: canvas.getContext('2d'),
                viewport: vp,
                transform: [1, 0, 0, 1, 0, 0],
            }
            pageTask.render(renderContext).promise.then(function(){
                
            })
          })
        },
        /**
         * scroll
         */ 
        initScroll: function(){
            while (Config.element.scrollLayer.firstChild) {
				Config.element.scrollLayer.removeChild(Config.element.scrollLayer.firstChild);
			}

            Scroll.setSize( window.innerWidth * 0.7 )
            
            
            const docf = document.createDocumentFragment()
            for (let i=0; i<Config.status.pdf._pdfInfo.numPages; i++){
                let item = document.createElement('div')
                item.classList.add('recyler-inner-item')
                item.setAttribute('data-page',i+1)
                item.style.width = Scroll.getSize() + 'px'
                item.style.height = 500 + 'px'
                docf.appendChild(item)
            }

            Config.element.scrollLayer.appendChild(docf)
            Config.element.viewerWrapper.removeEventListener('scroll',Scroll.onScrollChangeListener)
            Config.element.viewerWrapper.addEventListener('scroll',Scroll.onScrollChangeListener)
            Scroll.loadMore()
        },

        size: 0,
        setSize: function(size){
            Scroll.size = size
        },
        getSize: function(){
            return Scroll.size
        },
        
        updatePage: function(pageNum,isCreate){
            let idx = Scroll.tasks.findIndex(function(e){
                if (e.pageNumber === pageNum){
                    return true
                } else {
                    return false
                }
            })

            //생성 이며 
            if (isCreate){
                //아직 안들어간 애들, 넣어줘야함.
                if (idx === -1){
                    Scroll.tasks.push(new RenderingTask(pageNum, Config.status.pdf.getPage(pageNum), Scroll.getSize(), Scroll.onRenderCallback))
                } 
            }
            //삭제 이며
            else {
                // 랜더링 큐에 없음
                if (idx === -1){
                    
                } 
                // 랜더링 큐에 있음, 제거해야됨
                else {
                    // let findItemm = Scroll.tasks[idx]
                    Scroll.tasks[idx].destory()
                    Scroll.tasks.splice(idx, 1);
                }
            }

            // console.log('##updatePage',idx, pageNum,isCreate,typeof pageNum )
        },

        _cacheTask: null,
        onScrollChangeListener: function(){
            Scroll.loadMore()
            setTimeout(() => {
                Config.element.currentPage.textContent = Scroll.getCurrentPage() + 'P'
            }, 0);
        },
        loadMore: function(){
            Scroll.updateScroll(Config.element.viewerWrapper)
            Scroll.updateScrollUI()

            if (Scroll._cacheTask){
                clearTimeout(Scroll._cacheTask)
                Scroll._cacheTask = null
            }

            //on idle time
            // Scroll._cacheTask = setTimeout(() => {
            //     Scroll.tasks.forEach(function(task){
            //         // console.log(task)
            //         if (task.status === RenderingStatus.none){
            //             task.start()
            //         }
            //     })
            // }, 100);
            Scroll.tasks.forEach(function(task){
                Scroll._cacheTask = setTimeout(() => {
                    if (task.status === RenderingStatus.none){
                        task.start()
                    }
                }, 100);
            })

        },
        getCurrentPage: function(){
            let scroll = Config.element.viewerWrapper
            let list = scroll.getElementsByClassName('recyler-inner-item')
            let result = null
            for (let item of list){
                if (scroll.scrollTop <= item.offsetTop && item.offsetTop < scroll.scrollTop + scroll.clientHeight ){
                    result = item.getAttribute('data-page')
                    break;
                }
            }
            return result
        },
        /**
         * 현재 스크롤상 보이는 item에 Loaded라는 속성을 부여해
         * 로드됨을 표현한다.
         * @param {*} scroll 
         */
        updateScroll: function(scroll){
            let list = scroll.getElementsByClassName('recyler-inner-item')
            for (let item of list){
                if (scroll.scrollTop-((scroll.clientHeight * 5)) <= item.offsetTop && item.offsetTop < scroll.scrollTop+(scroll.clientHeight * 5) ){
                    if(!item.getAttribute('Loaded')){
                        item.setAttribute('Loaded', true)
                        Scroll.updatePage( parseInt(item.getAttribute('data-page')), true)
                    }
                } else {
                    item.removeAttribute('Loaded')
                    Scroll.updatePage( parseInt( item.getAttribute('data-page') ), false)
                }   
            }
            list = null
        },
        /**
         * ui에 canvas wrapper를 추가한다.
         */
        updateScrollUI: function(){
            let list = Config.element.scrollLayer.getElementsByClassName('recyler-inner-item')
            for (let item of list){
                if(item.getAttribute('Loaded')){
                    if (Array.from(item.children).filter(e=>e.classList.contains('canvasWrapper')).length === 0){
                        item.appendChild(Scroll.getCanvasWrapper())
                    }
                } else {
                    while (item.firstChild) {
                        item.removeChild(item.firstChild);
                    }
                }
            }
            list = null
        },
        onRenderCallback: function(canvas, page , width , height){
            let item = document.querySelector('[data-page=\"'+page+'\"]')
            if (item){
                let wrapper = item.querySelector('.canvasWrapper')
                if (wrapper){
                    item.style.width = width + 'px'
                    item.style.height = height + 'px'
                    wrapper.style.width = width + 'px'
                    wrapper.style.height = height + 'px'
                    wrapper.appendChild(canvas)                    
                    item.appendChild(Scroll.getTextLayer())
                    item.appendChild(Scroll.getAnnotationLayer())
                }
            }
        },
        getCanvasWrapper: function(){
            let div = document.createElement('div')
            div.classList.add('canvasWrapper')
            return div
        },
        getTextLayer: function(){
            let div = document.createElement('div')
            div.style.position = 'absolute'
            div.style.width = '100%'
            div.style.height = '0'
            div.style.left = '0'
            div.style.top = '0'
            div.style.right = '0'
            div.style.bottom = '0'
            div.style.overflow = 'hidden'
            div.style.opacity = 0.2
            div.style.lineHeight = 1.0
            return div
        },
        getAnnotationLayer: function(){
            let div = document.createElement('div')
            div.style.position = 'absolute'
            div.style.width = '100%'
            div.style.height = '0'
            div.style.left = '0'
            div.style.top = '0'
            div.style.right = '0'
            div.style.bottom = '0'
            div.style.overflow = 'hidden'
            div.style.opacity = 0.2
            div.style.lineHeight = 1.0
            return div
        },


        moveTo: function(page){

            document.querySelector('[data-page=\"'+page+'\"]').scrollIntoView()
        }
    }


    // var View = SHARED.View = {

    //     updateScrollInfo: function(){

    //     },
    // }


    // var Util = SHARED.Util = {

    // }
    return SHARED
}());


function watchScroll(viewAreaElement, callback) {
    const debounceScroll = function (evt) {
      if (rAF) {
        return;
      }
      // schedule an invocation of scroll for next animation frame.
      rAF = window.requestAnimationFrame(function viewAreaElementScrolled() {
        rAF = null;
  
        const currentX = viewAreaElement.scrollLeft;
        const lastX = state.lastX;
        if (currentX !== lastX) {
          state.right = currentX > lastX;
        }
        state.lastX = currentX;
        const currentY = viewAreaElement.scrollTop;
        const lastY = state.lastY;
        if (currentY !== lastY) {
          state.down = currentY > lastY;
        }
        state.lastY = currentY;
        callback(state);
      });
    };
  
    const state = {
      right: true,
      down: true,
      lastX: viewAreaElement.scrollLeft,
      lastY: viewAreaElement.scrollTop,
      _eventHandler: debounceScroll,
    };
  
    let rAF = null;
    viewAreaElement.addEventListener("scroll", debounceScroll, true);
    return state;
  }