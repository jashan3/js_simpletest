/**
 * 
 */
const HANDLER_MSG_INIT = 0
var AnnotationType = {
    TEXT: 1,
    LINK: 2,
    FREETEXT: 3,
    LINE: 4,
    SQUARE: 5,
    CIRCLE: 6,
    POLYGON: 7,
    POLYLINE: 8,
    HIGHLIGHT: 9,
    UNDERLINE: 10,
    SQUIGGLY: 11,
    STRIKEOUT: 12,
    STAMP: 13,
    CARET: 14,
    INK: 15,
    POPUP: 16,
    FILEATTACHMENT: 17,
    SOUND: 18,
    MOVIE: 19,
    WIDGET: 20,
    SCREEN: 21,
    PRINTERMARK: 22,
    TRAPNET: 23,
    WATERMARK: 24,
    THREED: 25,
    REDACT: 26
};
/**
 * pdf module
 */
 var PdfEngine = PdfEngine || (function(){
    //instance
    var SHARED = {};

    /**
     * 정보
     */
    var Config = SHARED.Config = {

        url: null,

        workerSrc: 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.13.216/pdf.worker.min.js',
        //전체 페이지
        total: 0,
        //현재 페이지
        current: 100,
        //목차 리스트
        tocList: [],
        //pdf 객체
        pdf: null,
        // canvas 넓이
        width: 0,
        // canvas 높이
        height: 0,
        //스크롤 위아래 공간
        spacer: 100,

        vs_items: new Map,

        vs_items_map: new Map,

        /*
            ui element 
        */
        element: {
            canvas : document.getElementById('render-container'),
            currentPage: document.getElementById('current_page'),
            totalPage: document.getElementById('total_page'),
            inputRange: document.getElementById('input_range'),
            textLayer: document.getElementById('textlayer'),

            // render layer
            viewerWrapper: document.getElementById('viewer-wrapper'),
            singleLayer: document.getElementById('viewer-page-mode-wrapper'),
            dualLayer: document.getElementById('viewer-page-dual-mode-wrapper'),
            scrolLayer: document.getElementById('viewer-scroll-mode-wrapper'),
            hScrollLayer: document.getElementById('viewer-horizontal-scroll-mode-wrapper'),
        },

        init: function(url){
            this.url = url
            pdfjsLib.GlobalWorkerOptions.workerSrc = Config.workerSrc
            Config.getDocumentTask(url).then(function(pdfDocument){
                console.log(pdfDocument)
                Config.pdf = pdfDocument
                Config.total = pdfDocument._transport._numPages
                
                View.initScroll()
            })
        },
        getDocumentTask: function(url) {
            let task = pdfjsLib.getDocument({
                cMapPacked: true,
                cMapUrl: "../lib/cmaps/",
                disableAutoFetch: true,
                disableFontFace: false,
                disableRange: false,
                disableStream: true,
                docBaseUrl: "",
                enableXfa: true,
                fontExtraProperties: false,
                httpHeaders: {authorization: 'M2RlNjNiZTYyMmM1NzRmN2M2YjYzZGZjNWMyOGM0NTY5ZmZiMjhiYmU3YmNkYzdjZTAzZjZjMTY5NjAxZjEzOQ=='},
                isEvalSupported: true,
                maxImageSize: -1,
                pdfBug: false,
                standardFontDataUrl: "../pdf/lib/standard_fonts/",
                url: url,
                verbosity: 1,
                withCredentials: true,
            })

            return task.promise
        }
    }


    var View = SHARED.View = {
        //
        scrollItemCount: 8,
        //로딩 중인지
        _isLoading: false,
        //마지막 업데이트된 시간
        _last_update_time: null,
        //마지막 업데이트된 시간으로 부터 얼마나 지나야 하는지
        _on_Idle_time: 2000,

        clearScrollLayer: function(){
            while (Config.element.scrolLayer.firstChild) {
                Config.element.scrolLayer.removeChild(Config.element.scrolLayer.firstChild);
            }
        },
        // _update_vscroll: function(page,wrapper){
           
        // },
        initScroll: function(){
            
            while (Config.element.scrolLayer.firstChild) {
                Config.element.scrolLayer.removeChild(Config.element.scrolLayer.firstChild);
            }
         
            let tempDiv = document.createElement('div')
            tempDiv.classList.add('pinch-zoom-wrapper-layer')
            for (let i =0; i<View.scrollItemCount+1; i++){
                let sItem = null
                if (i ===0){
                    sItem = View.getEmptyScrollItems()
                } else if (i === View.scrollItemCount){
                    sItem = View.getEmptyScrollItems()
                } else {
                    sItem = View.getScrollItems()
                }
                tempDiv.appendChild(sItem)
            }
            
            Config.element.scrolLayer.appendChild(tempDiv)
            
            
            View._update_vscroll(Config.current)
            Config.element.viewerWrapper.removeEventListener('scroll',View.onScrollChangeListener)
            Config.element.viewerWrapper.addEventListener('scroll',View.onScrollChangeListener, {passive: false})

            tempDiv = null
        },
        /**
         * map에 페이지 위치에 대한 정보가 들어있으나 없는 경우 업데이트 한다.
         * @param {*} page 페이지
         * @param {*} width 넓이
         * @param {*} height 높이
         */
        _update_size: function(page, width, height){
            if (Config.vs_items.has(page)){
                let item = Config.vs_items.get(page)
                if (item.width != width || item.height != height){
                    Config.vs_items.set(page,{ width:width, height:height })
                }
            } else {
                Config.vs_items.set(page,{ width:width, height:height })
            }
            
            Config.width = width
            Config.height = height
        },
        

        /**
         * vetical 세로 스크롤 페이지 업데이트
         * @param {*} page 업데이트할 페이지
         * @param {*} direction 방향 true 위쪽 동작, false 아래쪽 동작
         */
        _taskCounting: 0,
        _tasks: null,
        _update_vscroll: function(page , direction){
            
            Config.element.scrolLayer.querySelectorAll('canvas').forEach(function(element,idx){
                element.style.visibility = 'hidden'
                let index = idx+page
                element.parentNode.parentNode.setAttribute('page',index)

                Config.pdf.getPage(index).then(function(pageTask){
                    console.log(index,pageTask)
                    let scale = 1
                    let ratio = window.devicePixelRatio
                    let zoom = scale * ratio
                    let vp = pageTask.getViewport({scale: zoom})
                    
                    let ctx = element.getContext('2d',{ alpha: false })
                    ctx.clearRect(0 ,0 ,element.width, element.height);

                    let renderContext = {
                        canvasContext: ctx,
                        viewport: vp,
                        transform: [scale, 0, 0, scale, 0, 0],
                    }
                    
                    //viewport에서 추출한 사이즈
                    let m_width = Math.floor(vp.width)
                    let m_height = Math.floor(vp.height)

                    let d_width = ( window.innerWidth > window.innerHeight ? window.innerWidth: window.innerHeight ) * 0.5
                    let d_height = m_height * d_width / m_width

                    console.log(ratio,m_width , m_height , m_width*m_height)
                    //사이즈 업데이트
                    View._update_size(index,Math.floor(d_width),Math.floor(d_height))

                    //canvas 랜더링 사이즈
                    element.width = m_width
                    element.height = m_height

                    let renewScale =  Config.width / m_width
                    let renewViewport = pageTask.getViewport({scale: 1})
                    //각 css 사이즈
                    element.style.width = element.parentNode.style.width = element.parentNode.parentNode.style.width  = Config.width +'px'
                    element.style.height = element.parentNode.style.height =  element.parentNode.parentNode.style.height  = Config.height  +'px'
                    

                    if (View._tasks){
                        View._tasks.push({'renderContext': renderContext, 'task':pageTask ,'renewViewport':renewViewport, 'pdfview': element.parentNode.parentNode, 'size': {width: Config.width, height:Config.height} })
                    } else {
                        View._tasks = []
                        View._tasks.push({'renderContext': renderContext, 'task':pageTask ,'renewViewport':renewViewport, 'pdfview': element.parentNode.parentNode, 'size': {width: Config.width, height:Config.height}})
                    }

                    //마지막일때
                    if (View._tasks && View._tasks.length >= View.scrollItemCount-1){
                        //true인경우 마지막 페이지 부터 보여야기 때문에 array를 뒤집어준다.
                        if (direction){
                            View._tasks.reverse()
                        }
                        View._tasks.forEach(function(mItem,idx){
                            View._taskCounting++;
                            let isLast = false
                            if (View.scrollItemCount-1 ===View._taskCounting){
                                isLast = true
                            }
                            mItem.task.render(mItem.renderContext).promise.then(View.onCompleteRendering.bind(this,mItem,idx,direction,isLast))
                        })
                        View._tasks = undefined
                        count = undefined
                    }

                    scale = vp = ctx = m_width = m_height = d_width = d_height = undefined
                    index = undefined
                })
            })
        },
        _pz: null,
        onCompleteRendering: function(mItem, index, direction, isLast){
            
            const pdfview =  mItem.pdfview
            const size = mItem.size
            const renewViewport = mItem.renewViewport
            const readableStream = mItem.task.streamTextContent({
                normalizeWhitespace: true,
                includeMarkedContent: true
            });
            //annotation layer
            mItem.task.getAnnotations().then(function(annotation){
                if (annotation && annotation.length > 0){
                    let annotationLayer = pdfview.querySelector('.annotationLayer')
                    if (annotationLayer){
                        annotationLayer.style.width = size.width + 'px'
                        annotationLayer.style.height = size.height + 'px'
                        while (annotationLayer.firstChild) {
                            annotationLayer.removeChild(annotationLayer.firstChild);
                        }
                        //annotation type에 따라 분기
                        if (annotation.annotationType === AnnotationType.LINK){

                        }
                        
                    } else {
               
                    }
                    annotationLayer = null
                }
            })

            //text layer
            mItem.task.getTextContent().then(function(textContent){
                if (textContent && textContent.items.length>0){
                    let textLayer = pdfview.querySelector('.textLayer')
                    
                    if (textLayer){
                        while (textLayer.firstChild) {
                            textLayer.removeChild(textLayer.firstChild);
                        }
                        textLayer.remove()
                        textLayer = View.getTextLayer(size.width, size.height)

                        pdfjsLib.renderTextLayer({
                            textContent: textContent,
                            container: textLayer,
                            textContentStream: readableStream,
                            viewport: renewViewport,
                            textDivs: [],
                            transform: null
                        }).promise.then(function(){
                            pdfview.appendChild(textLayer)
                            console.log('## textrender')
                        })
                    }
                    textContent = null
                    // textLayer = null
                }
            })

            
            mItem.task.cleanup()
            mItem.pdfview.querySelector('canvas').style.visibility = 'visible'
            if (isLast){
                if (direction){
                    Config.element.viewerWrapper.scrollTo(0, Config.element.viewerWrapper.scrollHeight-Config.spacer);
                } else {
                    Config.element.viewerWrapper.scrollTo(0, Config.spacer);  
                }
                setTimeout(() => {
                    console.log('load complete')
                    //완료 시점
                    View._isLoading = false   
                    View._taskCounting = 0   
                }, 500);
            }
            mItem.pdfview = null
            mItem.task = null
            mItem.renderContext = null
            mItem = null
            index = null
            counting = null
            direction = null
        },

        rederingAnnotation:function(){

        },
        rederingTextLayer: function(){

        },

        getTextLayer: function(width, height){
            let textLayer = document.createElement('div')
            textLayer.classList.add('textLayer')
            textLayer.style.width = width + 'px'
            textLayer.style.height = height + 'px'
            return textLayer
        },
        getEmptyScrollItems: function(){
            let item = document.createElement('div')
            // item.classList.add('recyler-inner-item')
            item.style.height = Config.spacer+'px'
            // item.style.background = 'white'
            return item
        },
        getScrollItems: function(){
            let item = document.createElement('div')
            item.classList.add('recyler-inner-item')

            let canvasWrapper = document.createElement('div')
            canvasWrapper.classList.add('canvasWarpper')

            //text
            let textLayer = document.createElement('div')
            textLayer.classList.add('textLayer')
            textLayer.style.position = 'absolute'
            textLayer.style.width = '100%'
            textLayer.style.height = '0'
            textLayer.style.left = '0'
            textLayer.style.top = '0'
            textLayer.style.right = '0'
            textLayer.style.bottom = '0'
            textLayer.style.overflow = 'hidden'
            textLayer.style.opacity = 0.2
            textLayer.style.lineHeight = 1.0

            //annotation
            let annotationLayer = document.createElement('div')
            annotationLayer.classList.add('annotationLayer')
            annotationLayer.style.zIndex = 50
            // div.style.background = 'green'
            // annotationLayer.style.cursor = 'pointer'
            annotationLayer.style.position = 'absolute';
            annotationLayer.style.left = 0
            annotationLayer.style.top = 0

            //canvas
            let canvas = document.createElement('canvas')
            canvasWrapper.appendChild(canvas)
            item.appendChild(canvasWrapper)
            item.appendChild(textLayer)
            item.appendChild(annotationLayer)
            return item
        },

        onScrollChangeListener: async function(event){


            // event.preventDefault()
            if (View._isLoading){
                console.log('###')
                await new Promise(resolve => window.requestAnimationFrame(resolve))
                const {
                  scrollTop,
                  scrollLeft,
                  scrollHeight,
                  clientHeight
                } = this
                const atTop = scrollTop <= Config.spacer
                const beforeTop = 1
                const atBottom = scrollTop === scrollHeight - clientHeight
                const beforeBottom = scrollHeight - clientHeight - 1
              
                if (atTop) {
                    Config.element.viewerWrapper.scrollTo(scrollLeft, beforeTop) 
                } else if (atBottom) {
                    Config.element.viewerWrapper.scrollTo(scrollLeft, beforeBottom)
                  }
                // Config.element.viewerWrapper.style.overflow = ''
                // Config.element.viewerWrapper.scrollTop = Config.spacer
                return false
            }
            let diff = this.scrollHeight - this.scrollTop
            let cHeight = this.clientHeight
            let currentTime = new Date()

            
            let mitem = Config.vs_items.get(Config.current)

            let checkpage =  (this.scrollTop+this.clientHeight)/mitem.height
            checkpage = Math.floor(checkpage)
            // if (Config.current != checkpage){
            //     Config.current = checkpage
            // }
            // console.log(checkpage, Config.current)
            //time distrupt
            if (currentTime - View._last_update_time < View._on_Idle_time){
                return
            }

            //top
            if (this.scrollTop <= 0){
                if (Config.current > 1){
                    View._isLoading = true
                    let eleItems = Config.element.scrolLayer.querySelectorAll('[page]')
                    let eleItem = eleItems[0]
                    let page = eleItem.getAttribute('page')
                    let tmp = parseInt(page) - View.scrollItemCount -1
                    if (tmp <= 0 ){
                        tmp = 1
                    }
                    Config.current = tmp
                    View._update_vscroll(Config.current, true)
                    return;
                }
            }

            //bottom
            if (diff <= cHeight){
                View._isLoading = true
                let eleItems = Config.element.scrolLayer.querySelectorAll('[page]')
                let eleItem = eleItems[eleItems.length-1]
                let page = eleItem.getAttribute('page')
                let tmp = parseInt(page)+1
                if (tmp > Config.total){
                    tmp = Config.total
                }
                
                Config.current = tmp
                View._update_vscroll(Config.current, false)
                return;
            }
        }
    }

    var Search = SHARED.Search = {
        //검색 리스트
        searchList: [],
        //검색 데이터 파싱중인지
        onSearchProgress: true,
        //파싱 시작
        init:function(){
            for (var i = 1; i <= Config.total; ++i) {
                Config.pdf.getPage(i).then(function(mPage){
                    mPage.getTextContent().then(function(textContent){
                        let info = { 'page': mPage._pageIndex ,'obj': textContent }
                        Search.searchList.push(info)
                        if (Search.searchList.length === Config.total){
                            Search.onSearchProgress = false
                        }
                        mPage = null
                        textContent = null
                        info = null
                    })
                })
            }
        }
    }


    /**
     * loader
     * not use
     */
    var Loader = SHARED.Loader = {
        /**
         * vetical scroll item parsing
         */
        estimate_size: ( window.innerWidth > window.innerHeight ? window.innerWidth: window.innerHeight ) * 0.5,
        vsscrollParseCallback: null,
        vscrollParse: function(index){
            console.log('##vscroll_parse')
            if (index === 1){
                Config.vs_items_map.clear()
            }
            if (index > Config.total){
                if (Loader.vsscrollParseCallback){
                    Loader.vsscrollParseCallback()
                }
            } else {
                Config.pdf.getPage(index).then(function(pageTask){
                    let scale = 1
                    let ratio = window.devicePixelRatio
                    let zoom = scale * ratio
                    let vp = pageTask.getViewport({scale: zoom})
                    //viewport에서 추출한 사이즈
                    let m_width = Math.floor(vp.width)
                    let m_height = Math.floor(vp.height)
                    let d_width = Loader.estimate_size
                    let d_height = m_height * d_width / m_width
                    Loader.updateScrollItem(index,d_width,d_height)
                    Loader.vscrollParse(index+1)

                    //deloc
                    pageTask = null
                    scale = null
                    ratio = null
                    zoom = null
                    vp = null
                    m_width = null
                    m_height = null
                    d_width = null
                    d_height = null
                })
            }
        },
        updateScrollItem: function(index , width, height){
            Config.vs_items_map.set(index, {'width':width, 'height':height})
        },
        getScrollItem: function(index){
            if (index < 0 ){
                return null
            }
            if (Config.vs_items_map.has(index)){
                return Config.vs_items_map.get(index)
            } else{
                return null
            }
        }
    }


    /**
     * background task
     */
    var Job = SHARED.Worker = {
        ui_worker:'/script/pdf.engine.worker.js',

        task: null,

        init: function(task){
            let msg = Job.workerMsg(HANDLER_MSG_INIT,task)
            Job.post(msg)
        },
        onWorkerMessageListener: function(event){
            if (event.type === 0){

            } 
            
            else if (event.type === 1){

            }
            Job.destory()
        },
        workerMsg: function(type, data){
            return {'type': type, 'obj': data }
        },
        post: function(msg){
            Job.destory()
            Job.task = new Worker(Job.ui_worker)
            Job.task.onmessage = Job.onWorkerMessageListener
            Job.task.postMessage(msg)
        },
        destory: function(){
            if (Job.task){
                Job.task.terminate();
                Job.task = null
            }
        }
    }

    return SHARED
}());