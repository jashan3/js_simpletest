CanvasRenderingContext2D.prototype.clear = 
  CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
    if (preserveTransform) {
      this.save();
      this.setTransform(1, 0, 0, 1, 0, 0);
    }

    this.clearRect(0, 0, this.canvas.width, this.canvas.height);

    if (preserveTransform) {
      this.restore();
    }           
};

/**
 * pdf module
 */
 var PdfEngine = PdfEngine || (function(){
    //instance
    var SHARED = {};

    var Util = SHARED.Util = {
        isMobile:function(){
            const userAgent = typeof navigator !== "undefined" && navigator.userAgent || "";
            const platform = typeof navigator !== "undefined" && navigator.platform || "";
            const maxTouchPoints = typeof navigator !== "undefined" && navigator.maxTouchPoints || 1;
            const isAndroid = /Android/.test(userAgent);
            const isIOS = /\b(iPad|iPhone|iPod)(?=;)/.test(userAgent) || platform === "MacIntel" && maxTouchPoints > 1;
            return isAndroid || isIOS
        }
    }
    /*
    * config
    */
    var Config = SHARED.Config = {
        /**
         * 청크방식일 경우 단순 url 호출로 load 시킬수 없다.
         */ 
        useChunked: false,
        /**
         * 워커 경로
         */
        workerSrc: 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.13.216/pdf.worker.min.js',
        /*
            ui element 
        */
        element: {
            canvas : document.getElementById('render-container'),
            currentPage: document.getElementById('current_page'),
            totalPage: document.getElementById('total_page'),
            inputRange: document.getElementById('input_range'),
            textLayer: document.getElementById('textlayer'),

            // render layer
            viewerWrapper: document.getElementById('viewer-wrapper'),
            singleLayer: document.getElementById('viewer-page-mode-wrapper'),
            dualLayer: document.getElementById('viewer-page-dual-mode-wrapper'),
            scrolLayer: document.getElementById('viewer-scroll-mode-wrapper'),
            hScrollLayer: document.getElementById('viewer-horizontal-scroll-mode-wrapper'),
        },
        /*
            뷰어상태
        */
        status: {
            /**
             * 0: 첫페이지 부터 양면
             * 1: 그냥 양면
             * 2: 단면
             * 3: 스크롤
             * 4: 가로 스크롤
             */
            mode: 3,

            /**
             * 가로 , 세로 상관없음 유저가 설정한 값을 따라감
             * 0: 첫페이지 부터 양면
             * 1: 그냥 양면
             * 2: 단면
             * 3: 스크롤
             */
            uiMode: 0,

            //pdf object
            pdf: null,

            //현재페이지
            currentPage: 1,

            //전체페이지
            total: 0,

            //현재 pdf 줌 비율
            zoom: 1.0,

            //canvas의 x,y,z 좌표
            // transform: [1,0,0,1,0,0]
            transform: null,
        },

        /**
         * 현재 ui 정보
         */
        info: {

            items: [],

            width: 0,

            height: 0 ,

        },
        /*
            뷰어 메타 데이터
        */
        meta: {
            toc: null,
            title: null,
            subject: null,
            author: null,
            creater: null,
        },
        init(url , token){
            console.log('## PdfEngine init')
            var ChunkedParams = null
            if (Config.useChunked){
                ChunkedParams = {
                    "url": url,
                    "cMapPacked": true,
                    "cMapUrl": "../lib/cmaps/",
                    "disableAutoFetch": true,
                    "disableFontFace": false,
                    "disableRange": false,
                    "disableStream": true,
                    "docBaseUrl": "",
                    "enableXfa": true,
                    "fontExtraProperties": false,
                    "isEvalSupported": true,
                    "maxImageSize": -1,
                    "pdfBug": false,
                    "standardFontDataUrl": "../pdf/lib/standard_fonts/",
                    "verbosity": 1,
                    "httpHeaders": { "authorization": token },
                    "withCredentials": true
                }
            } 
            //pdf 워커 위치
            pdfjsLib.GlobalWorkerOptions.workerSrc = Config.workerSrc
            if (ChunkedParams){
                pdfjsLib.getDocument(ChunkedParams).promise.then(Config.onLoadPdf)
            } else {
                
                // pdfjsLib.getDocument(url).promise.then(Config.onLoadPdf)
                pdfjsLib.getDocument({
                    url: url,
                    maxImageSize: -1,
                    disableAutoFetch: true,
                    disableFontFace: false,
                    disableRange: false,
                    disableStream: true
                }).promise.then(Config.onLoadPdf)
            }
        },
        /**
         * pdf load callback
         * @param {*} pdf 
         */
        onLoadPdf: async function(pdf){
            console.log('## PdfEngine onLoadPdf',pdf)
            Config.status.pdf = pdf
            if (pdf._pdfInfo){
                Config.status.total = pdf._pdfInfo.numPages
            }
            const pageLayoutPromise = pdf.getPageLayout().catch(function () {});
            const pageModePromise = pdf.getPageMode().catch(function () {});
            const openActionPromise = pdf.getOpenAction().catch(function () {});
            const pdfViewerPreferencesPromise = pdf.getViewerPreferences().catch(function() {});
            //목차 
            //parse toc
            console.log('### parse toc init',Config.meta.toc)
            let tt = await pdf.getOutline()
            Config.meta.toc = tt
            // await Toc.makeToc(tt , 0)
            // Toc.createTocFragment()
            // Config.element.tocList.appendChild(Toc.tocDocFragment)
            
            Loader.load()

            Config.element.viewerWrapper.addEventListener('scroll',Observer.onVScrollChangeObserver.bind(this))

            Config.status.pdf.getPageView(1)
        }
    }

    /**
     * ui 관련
     * view
     */ 
    var View = SHARED.View = {


        _update_vscroll: async function(wrapper , renderingPage ){
            
            if (wrapper.getAttribute('loaded') === null){
                while (wrapper.firstChild) {
                    wrapper.removeChild(wrapper.firstChild);
                }
                return;
            }

            let canvas = wrapper.querySelector('canvas')
            if (!canvas){
                canvas = document.createElement('canvas')
            } else {
                canvas.clearRect(0,0,canvas.width, canvas.height)
            }

            while (wrapper.firstChild) {
                wrapper.removeChild(wrapper.firstChild);
            }
            canvas.style.display = 'none'
            canvas.hidden = true
            
            let mPage = renderingPage
            let zoom = Config.status.zoom
            let transform = [1,0,0,1,0,0]
            let page = await Config.status.pdf.getPage(mPage)
            let ratio = window.devicePixelRatio;
            let scale = zoom*ratio
            // let scale = 1
            var viewport = page.getViewport({
                scale: scale
            });
            var pixelsInViewport = viewport.width * viewport.height;
            var maxScale = Math.sqrt(Loader.MAX_CANVAS_PIXEL / pixelsInViewport);

            //viewport를 원하는 픽셀로 리사이즈
            let desireViewport  = null
            let renderContext = null
            let desireScale  = null
            desireScale = Config.info.width / viewport.width;
            desireViewport = page.getViewport({
                scale: desireScale * ratio 
            });

            let cWidth = Math.floor(desireViewport.width)
            let cHeight = Math.floor(desireViewport.height)
            
            canvas.width =  cWidth;
            canvas.height = cHeight;
            // canvas.style.width = cWidth + 'px'
            // canvas.style.height = cHeight + 'px'
            canvas.style.width = Config.info.width + 'px'
            canvas.style.height = Config.info.height + 'px'
            vp = desireViewport;



            /**
             * text layer
             */
            let textWrapper = wrapper.querySelector('.textLayer')
            if (!textWrapper){
                textWrapper = ViewHolder.getTextDiv()
                textWrapper.style.width = canvas.style.width
                textWrapper.style.height = canvas.style.height
                
            }
            let textContent = await page.getTextContent()
            pdfjsLib.renderTextLayer({
                textContent: textContent,
                container: textWrapper,
                viewport: desireViewport,
                textDivs: []
            })

            let ctx = canvas.getContext('2d',{alpha: false})
            renderContext = {
                canvasContext: ctx,
                viewport: vp,
                transform: transform,
            }
            
            page.cleanupAfterRender = true;
            let task = page.render(renderContext);
            

            let loaded = wrapper.getAttribute('loaded')
            if (loaded === null){

                page.cleanup()
                if (canvas){
                    let img = new Image()
                    ctx.drawImage(img,0 ,0)
                    ctx.clear()
                    canvas.width = 0
                    canvas.height = 0
                    canvas.remove()
                    canvas = null
                    img = null
                }
                while (wrapper.firstChild) {
                    wrapper.removeChild(wrapper.firstChild);
                }
                task = null
            } else {
                task.promise.then(()=>{
                    page.cleanup()
                    page = null

                    let mcv = wrapper.querySelector('canvas')
                    if (mcv){
                        mcv.clear()
                    }
                    while (wrapper.firstChild) {
                        wrapper.removeChild(wrapper.firstChild);
                    }

                    wrapper.appendChild(canvas)
                    wrapper.appendChild(textWrapper)
                    canvas.style.display = 'block'
                    canvas = null

                }).catch(e=>{
                    console.error('## render error',e)
                    if (canvas){
                        let img = new Image()
                        ctx.drawImage(img,0 ,0)
                        ctx.clear()
                        canvas.width = 0
                        canvas.height = 0
                        canvas.remove()
                        canvas = null
                        img = null
                    }
                    while (wrapper.firstChild) {
                        wrapper.removeChild(wrapper.firstChild);
                    }
                })
            }
        },
        handleDrawTask: function(){

        },

        handleAnnotationTask: function(wrapper , page ){

            page.getAnnotations().then(function(anno){
                let annotationWrapper = null
                if(anno && anno.length>0){
                    annotationWrapper = wrapper.querySelector('.annotationLayer')
                    if (!annotationWrapper){
                        annotationWrapper = ViewHolder.getAnnotationDiv()
                        wrapper.appendChild(annotationWrapper)
                    }
                    let existCount = 0
                    anno.forEach((annoItem,annoIndex)=>{
                        // subtype
                        let annoRect = annoItem.rect
                        let annoView = page.view
                        let type = annoItem.subtype
                        //annotation 존재할때
                        if (type && type.length>0){
                            let div = document.createElement('div')
                            div.style.zIndex = 50
                            div.style.background = 'green'
                            div.style.cursor = 'pointer'
                            div.style.position = 'absolute';
                            div.style.left = 0
                            div.style.top = 0
                            div.style.width = 30+'px'
                            div.style.height = 30+'px'
                            if (type === 'Link'){
                                let mDest = annoItem.dest
                                if (mDest){
                                    let [destRef] = mDest
                                    Config.status.pdf.getPageIndex(destRef).then(position=>{
                                        div.style.left = 0+'px'
                                        div.style.top = 0+'px'
                                        div.setAttribute('type','link')
                                        div.setAttribute('position',position)
                                        div.addEventListener('click', function(){
                                            // alert(position)
                                        })
                                    })
                                }
                            }
                            annotationWrapper.appendChild(div)
                          
                        }
                    })
                }
            })
           
        },

        handleTextTask: function(){

        }

    }


    /**
     * 
     */
    var Observer = SHARED.Observer = {
        //item 속성 옵저버
        itemAttrChangeObserver: function(mutations){
            let tg = mutations[0].target;
            if (tg.id ){
                let temp = tg.id
                temp = temp.replace('recycler-item-','')
                if (temp){
                    temp = parseInt(temp)
                    setTimeout(() => {
                        let element = document.getElementById(tg.id)
                        element.style.visibility = ""
                        let attr = element.getAttribute('loaded')
                        // if (attr != null){
                            
                        // } else {

                        // }
                        View._update_vscroll(tg, temp)  
                    }, 700);
                }
            }
        },
        //세로 스크롤 옵저버
        onVScrollChangeObserver: function(e){
            Validate.invalidateVscroll(e.target)
        }
    }

    /**
     * 
     */
    var Validate = SHARED.Validate = {
        /**
         * 스크롤이 위치에 따라 로드가 됫는지 확인
         * @param {*} scroll 
         * @returns 
         */
        invalidateVscroll: function(scroll){
            let under = Config.info.items.filter(e=> e.position >= scroll.scrollTop)
            if (under && under.length>0){
                // d는 현재 보고있는 아이템
                let d = null
                if (scroll.scrollHeight - scroll.scrollTop === scroll.clientHeight){
                    d = under[under.length-1]
                } else {
                    d = under[0]
                }
                if (Config.status.currentPage === d.seq){
                    return
                }
                Config.status.currentPage = d.seq
                Loader.pageUpdate(d.seq-1,Config.element.scrolLayer)
                d = null
            }
        }
    }


    

    var Loader = SHARED.Loader = {

        MAX_CANVAS_PIXEL: Util.isMobile()?5242880:16777216,
        
        /**
         * 최초 1회 업데이트
         */
        load: function(){
            //pre cal
            Loader._preCalc().then(e=>{
                Loader.calculate(Config.info.width,Config.info.height)
                Config.element.scrolLayer.innerHTML = ''
                Config.info.items.forEach((item)=>{
                    let template = ViewHolder.getItemTemplate()
                    template.id = 'recycler-item-'+item.seq
                    // let observer = new MutationObserver(Observer.itemAttrChangeObserver);
                    // observer.observe(template, {  attributes: true, attributeFilter: ['loaded'] });
                    Loader.updateSize(template,item)
                    Config.element.scrolLayer.appendChild(template)
                })

                Loader.pageUpdate(Config.status.currentPage,Config.element.scrolLayer)
            })
         
        },

        calculate: function(width, height){
            //clear items
            Config.info.items = []
            for(var i =1; i<=Config.status.total; i++){
                let position = 0
                if (i > 1){
                    position = Config.info.items[i-2].position + height
                }  else {
                    position = (height/2)
                }
                Config.info.items.push({
                    seq: i , 
                    width: width, 
                    height: height,
                    position: position,
                    ratio: window.devicePixelRatio 
                })
            }
        },
        updateSize: function(element,item){
            element.style.width = item.width + 'px'
            element.style.height = item.height + 'px'
        },

        loadElement: function(element){
            if (!element){
                return;
            }
            let attr = element.getAttribute('loaded')
            //already load
            if (attr){
                
            } 
            //need update
            else {
                //some action after update
                element.setAttribute('loaded',1)
                let temp = element.id 
                temp = temp.replace('recycler-item-','')
                if (temp){
                    temp = parseInt(temp)
                    //빠르게 스크롤할 경우
                    setTimeout(() => {
                        let ee = document.getElementById(element.id)
                        ee.style.visibility = ""
                        let attr = ee.getAttribute('loaded')
                        View._update_vscroll(ee, temp)  
                        if (attr != null){
                          
                        }
                    }, 700);
                }
            }
        },

        unLoadElement: function(element){
            if (!element){
                return
            }
            
            let attr = element.getAttribute('loaded')
            if (attr != null ){
                element.removeAttribute('loaded')
            } 
           
            while (element.firstChild) {
                element.removeChild(element.firstChild);
            }

            element.innerHTML = ''
            element.style.visibility = "hidden"
        },

        /**
        * 
        * @param {*} refNumber 
        * @param {*} innerScrollWrapper 
        */
        pageUpdate: function(refNumber,innerScrollWrapper){
            //찾아올 대상의 그룹
            let findGroupCount = 7
            let listViewElement = innerScrollWrapper
            let targetQuerySelector = '.'+ViewHolder.itemClassName
            let ulRvItems = null
            try {
                ulRvItems = listViewElement.querySelectorAll(targetQuerySelector)
                ulRvItems.forEach((e,idx)=>{
                    if (refNumber === idx || 
                        refNumber === idx+1 || 
                        refNumber === idx-1 || 
                        refNumber === idx+2 || 
                        refNumber === idx-2 ||
                        refNumber === idx+3 || 
                        refNumber === idx-3 
                    ){
                        Loader.loadElement(e)
                    } else {
                        Loader.unLoadElement(e)
                    }
                })
            } catch(e){
                console.error('Error findTarget('+refNumber+')',e)
            } finally{
                listViewElement = null
                findGroupCount = null
                needClear = null
                needInvalidate = null
                targetQuerySelector = null
                ulRvItems = null
            }
        },
        _preCalc: async function(){
            let page = await Config.status.pdf.getPage(1)
            let ratio = window.devicePixelRatio;
            let zoom = Config.status.zoom
            let scale = zoom*ratio
            var viewport = page.getViewport({
                scale: scale
            });
            //viewport를 원하는 픽셀로 리사이즈
            let desireViewport  = null
            let desireScale  = null
            // let size = window.innerHeight > window.innerWidth ? window.innerHeight:window.innerWidth
            let size = window.innerHeight > window.innerWidth ? window.innerHeight : window.innerWidth
            let desireSize = size * 0.5
            desireScale = desireSize / viewport.height;
            desireViewport = page.getViewport({
                scale: desireScale * ratio 
            });

            let cWidth = Math.floor(desireViewport.width )
            let cHeight = Math.floor(desireViewport.height)
            Config.info.width = cWidth
            Config.info.height = cHeight

            return new Promise((resolve, reject) =>{
                resolve()
            })
        },
    }


    /**
     * event bind
     */ 
    var Event = SHARED.Event = {
        init: function(){
            Config.element.inputRange.addEventListener('change',Event.onChangeInput)
        },
        onChangeInput: function(){
            Navi.movePage( parseInt(this.value) )
        }
    }

    /**
     * 목차
     */
    var Toc = SHARED.Toc = {
        _tocCount: 0,
        tocList: [],
        tocDocFragment: null,
        makeToc: async function( tocList , depth){
            if (!tocList){
                return;
            }
            let pdf = PdfEngine.Config.status.pdf

            for (e of tocList){
                let mDest = e.dest;
                let items = e['items'];
                let title = e['title'];
                if (mDest){
                    // string type
                    if (typeof mDest === 'string'){
                        let obj = null
                        if (items && items.length>0){
                            obj = {'title':title, 'page': 0,  'items':items, 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                            //한번더 깊이를 탐색할때마다 기존 depth에 0
                            Toc.makeToc( items ,depth+1)
                        } else {
                            obj = {'title':title, 'page': 0, 'items':items , 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                        }
                    } 
                    //object type
                    else if (typeof mDest === 'object'){
                        let [destRef] = mDest
                        let position = await pdf.getPageIndex(destRef);
                        let obj = null
                        position+=1
                        if (items && items.length>0){
                            obj = {'title':title, 'page': position, 'items':items, 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                            Toc.makeToc(items,depth+1)
                        }  
                        
                        else {
                            obj = {'title':title, 'page': position, 'items':items , 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                        }

                        destRef = null
                        position = null
                        obj = null
                    } 
                    
                } 
                // null
                else {
                    // console.log('### no dest')
                }
                mDest = null
                items = null
                title = null
            }

            if (Toc.tocList.length>0){
                Toc.tocList.sort((a,b)=>a.page-b.page)
            }
        },

        createTocFragment: function(){
            let docf = document.createDocumentFragment()
            Toc.tocList.forEach(e=>{
                let li = document.createElement('li')
                let ptag = document.createElement('p')
                ptag.textContent = e.title
                ptag.style.marginLeft = (e.depth+1) * 10 + 'px'
                li.appendChild(ptag)
                docf.appendChild(li)
            })
            Toc.tocDocFragment = docf
        },
        toggleToc:function(){
            if (Config.element.tocWrapper.style.display === 'none'){
                Config.element.tocWrapper.style.display = 'block'
            } else {
                Config.element.tocWrapper.style.display = 'none'
            }
           
        }
    }

    var Parser = SHARED.Parser = {
        searchList:[],
        annotaions:[],

        init: async function(){
            for (var i = 1; i <= Config.status.total; ++i) {
                let mPage = await Config.status.pdf.getPage(i)
                let textContent = await mPage.getTextContent()
                let info = { 'page': i ,'obj': textContent }
                Parser.searchList.push(info)

                let annos = await mPage.getAnnotations()
                if (!annos){
                    annos = null
                }
                if (annos&&annos.length<=0){
                    annos = null
                }
                let anno = {'key':i, 'anno':annos}
                Parser.annotaions.push(anno)

                mPage = null
                textContent = null
                info = null
                anno = null
            }
        }
    }
    /**
     * 검색 관련 클래스
     */
    var Search = SHARED.Search = {
        /**
         * 검색 관련 정보 
         */
        searchInfo: [],
        /**
         * 검색 정보 파싱
         */
        init: async function(){
            for (var i = 1; i <= Config.status.total; ++i) {
                let mPage = await Config.status.pdf.getPage(i)
                let textContent = await mPage.getTextContent()
                let info = { 'page': i ,'obj': textContent }
                Search.searchInfo.push(info)
                mPage = null
                textContent = null
                info = null
            }
        },
        /**
         * 검색 결과를 array로 return
         * @param {*} keyword 
         * @returns 
         */
        _search: function(keyword){
            let result = []
            if (!Search.searchInfo){
                throw 'search info parse error' 
            }
            if (Search.searchInfo.length == 0){
                throw 'search Info Empty'
            }
            Search.searchInfo.forEach((e,idx)=>{
                let mPage = e.page
                e.obj.items.forEach((e2,idx2)=>{
                    if (e2.str.includes(keyword)){
                        result.push({text:e2.str , page: mPage})
                    }
                })
            })
            return result
        },
        /**
         * 키워드에 따른 ui 결과값
         * @param {*} keyword 
         * @returns 
         */
        getSearchUI: function(keyword){
            let result = null
            let searchResult = null
            try{
                let searchResult = Search._search(keyword)
                let searchDocf = document.createDocumentFragment()

                if (searchResult && searchResult.length > 0){
                    let ul = document.createElement('ul')
                    searchResult.forEach(rst=>{
                        let li = document.createElement('li')
                        let ptag = document.createElement('p')
                        ptag.textContent = rst.text
                        li.appendChild(ptag)
                        ul.appendChild(li)
                    })
                    searchDocf.appendChild(ul)
                } else {
                    let div = document.createElement('div')
                    let ptag = document.createElement('p')
                    ptag.textContent = '검색 결과가 없습니다.'
                    div.appendChild(ptag) 
                    searchDocf.appendChild(div)
                }
                result = searchDocf
            } catch(e){
                console.error('getSearchUI error',e)
            } finally{
                searchResult = null
            }
            return result
        },
        /**
         * 검색 function
         * @returns 
         */
        onSearch: function(){
            let keyword = Config.element.searchIP.value
            if (!keyword){
                return
            }
            //2자 이상
            if (keyword.length < 2){
                return
            }
            //20자 이내
            if (keyword.length > 20){
                return
            }

            let search_ui = Search.getSearchUI(keyword)
            if (search_ui){
                Config.element.searchContents.innerHTML = '';
                Config.element.searchContents.appendChild(search_ui)
            }
        },
        /**
         * 검색 layer toggle
         */
        onToggleSearch: function(){
            if (Config.element.searchWrapper.style.display === 'none'){
                Config.element.searchWrapper.style.display = 'block'
            } else {
                Config.element.searchWrapper.style.display = 'none'
            }
        }
    }

    /**
     * 이동
     * navigation
     * 단면, 스크롤
        전부 가능

        첫페이지 부터 양면 
        12 34 56 78
        current값 : 홀수 유지

        그냥 양면 ()
        1 23 45 67 89
        current값 : 1 제외 , 짝수 유지
     */ 
    var Navi = SHARED.Navi = {
        movePage(page){
            //같은경우 ui 업데이트 할 필요가 없다.
            if (Config.status.currentPage === page){
                console.log('## movePage same')
                return
            }
            //처음페이지
            if (page <= 0 ) {
                console.log('## movePage first')
                return
            }
            //마지막 페이지
            if (Config.status.total < page){
                console.log('## movePage last')
                return
            }
            Config.status.currentPage = page
            View.update()
        },
        /**
         * 가로 스크롤일때 무브
         * @param {*} page 
         */
        moveToPage_horizontal: function(page){
            let scrollDiv2 = Config.element.hScrollLayer.querySelector('div') 
            let scrollul2 = scrollDiv2.querySelector('ul')
            let scrollli2 = scrollul2.querySelectorAll('li')
            scrollli2[page-1].scrollIntoView()
        },
        moveToPage_vertical: function(page){
            let scrollDiv2 = Config.element.scrolLayer.querySelector('div') 
            let scrollul2 = scrollDiv2.querySelector('ul')
            let scrollli2 = scrollul2.querySelectorAll('li')
            scrollli2[page-1].scrollIntoView()
        },
        prev(){
            if (Config.status.mode === 4){
                let mv = Config.status.currentPage-1
                mv-=1

                Config.status.currentPage = Config.status.currentPage-1
                Config.element.hScrollLayer.querySelector('ul').querySelectorAll('li')[mv].scrollIntoView()
                return
            } 
            //모드에 따라 변경되어야할 값
            let dynamicValue = 1
            let temp = Config.status.currentPage - dynamicValue 
            Navi.movePage(temp)
        },
        next(){
            if (Config.status.mode === 4){
                let mv = Config.status.currentPage-1
                mv+=1
                Config.status.currentPage = Config.status.currentPage+1
                Config.element.hScrollLayer.querySelector('ul').querySelectorAll('li')[mv].scrollIntoView()
                return
            } 
            //모드에 따라 변경되어야할 값
            let dynamicValue = 1
            let temp = Config.status.currentPage + dynamicValue 
            Navi.movePage(temp)
        }
    }


    /**
     * 줌
     * zoom
     */  
    var Zoom = SHARED.Zoom = {
        setZoom(zoom){
            if (Config.status.zoom === zoom){
                console.log('## setZoom same')
                return
            }
            Config.status.zoom = zoom
            View.update()
        },
        zoomOrigin(){
            let temp = 1.0
            Zoom.setZoom(temp)
        },
        zoomPlus(){
            let temp = Config.status.zoom + 0.2
            Zoom.setZoom(temp)
        },
        zoomMinus(){
            let temp = Config.status.zoom - 0.2
            Zoom.setZoom(temp)
        },
    }

    /**
     * view holder
     */
    var ViewHolder = SHARED.ViewHolder = {
        itemClassName : "recyler-inner-item",
        itemTemplate: null,
        canvasDiv: null,
        textDiv: null,
        annotaionDiv: null,
        getItemTemplate: function(){
            if (!ViewHolder.itemTemplate){
                let temp = document.createElement('div')
                temp.style.background = 'white'
                temp.classList.add(ViewHolder.itemClassName)
                ViewHolder.itemTemplate = temp
                temp = null
            }
            return ViewHolder.itemTemplate.cloneNode(true)
        },
        getCanvasDiv: function(){
            if (!ViewHolder.canvasDiv){
                let temp = document.createElement('div')
                let canvas = document.createElement('canvas')
                temp.appendChild(canvas)
                ViewHolder.canvasDiv = temp
                temp = null
            }
            return ViewHolder.canvasDiv.cloneNode(true)
        },
        getTextDiv: function(){
            if (!ViewHolder.textDiv){
                let temp = document.createElement('div')
                temp.classList.add('textLayer')
                temp.style.width = '100%'
                temp.style.height = '0'
                temp.style.left = '0'
                temp.style.top = '0'
                temp.style.right = '0'
                temp.style.bottom = '0'
                temp.style.overflow = 'hidden'
                // temp.style.opacity = 0.2
                temp.style.color = 'transparent'
                temp.style.lineHeight = 1.0
                ViewHolder.textDiv = temp
                temp = null
            }
            return ViewHolder.textDiv.cloneNode(true)
        },
        getAnnotationDiv: function(){
            if (!ViewHolder.annotaionDiv){
                let temp = document.createElement('div')
                temp.classList.add('annotationLayer')
                temp.style.width = '100%'
                temp.style.height = '0'
                temp.style.left = '0'
                temp.style.top = '0'
                temp.style.right = '0'
                temp.style.bottom = '0'
                temp.style.overflow = 'hidden'
                temp.style.opacity = 0.2
                temp.style.lineHeight = 1.0
                ViewHolder.annotaionDiv = temp
                temp = null
            }
            return ViewHolder.annotaionDiv.cloneNode(true)
        },
    }



    return SHARED
}());


function fullscreen(element){
    if (element.requestFullscreen) 
        element.requestFullscreen();
    else if (element.webkitRequestFullscreen) 
        element.webkitRequestFullscreen();
    else if (element.msRequestFullScreen) 
        element.msRequestFullScreen();
}

function reqFullscreen(){
    // fullscreen(document.querySelector('.viewer-wrapper'))
    fullscreen(document.body)
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e.preventDefault();
}

function preventDefaultForScrollKeys(e) {
  if (keys[e.keyCode]) {
    preventDefault(e);
    return false;
  }
}

// modern Chrome requires { passive: false } when adding event
var supportsPassive = false;
try {
  window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
    get: function () { supportsPassive = true; } 
  }));
} catch(e) {}

var wheelOpt = supportsPassive ? { passive: false } : false;
var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

// call this to Disable
function disableScroll() {
  window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
  window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
  window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
  window.addEventListener('keydown', preventDefaultForScrollKeys, false);
}

function activeScroll(){

}

document.documentElement.addEventListener('touchstart', function (event) { if (event.touches.length > 1) { event.preventDefault(); } }, false);

