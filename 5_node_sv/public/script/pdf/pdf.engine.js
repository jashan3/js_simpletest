

/**
 * pdf module
 */
var PdfEngine = PdfEngine || (function(){
    //instance
    var SHARED = {};

    /*
    * config
    */
    var Config = SHARED.Config = {
        /**
         * 청크방식일 경우 단순 url 호출로 load 시킬수 없다.
         */ 
        useChunked: false,
        /**
         * 워커 경로
         */
        workerSrc: 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.13.216/pdf.worker.min.js',
        /*
            ui element 
        */
        element: {
            canvas : document.getElementById('render-container'),
            currentPage: document.getElementById('current_page'),
            totalPage: document.getElementById('total_page'),
            inputRange: document.getElementById('input_range'),
            textLayer: document.getElementById('textlayer'),
            // render layer
            viewerWrapper: document.getElementById('viewer-wrapper'),
            singleLayer: document.getElementById('viewer-page-mode-wrapper'),
            dualLayer: document.getElementById('viewer-page-dual-mode-wrapper'),
            scrolLayer: document.getElementById('viewer-scroll-mode-wrapper'),
            hScrollLayer: document.getElementById('viewer-horizontal-scroll-mode-wrapper'),
            //toc
            tocWrapper: document.getElementById('viewer-toc-wrapper'),
            tocList: document.getElementById('viewer-toc-list'),
            //search
            searchWrapper: document.getElementById('viewer-search-wrapper'),
            searchContents: document.getElementById('viewer-search-contents-wrapper'),
            searchIP: document.getElementById('viewer-search-input'),
            searchBtn: document.getElementById('viewer-search-btn'),
        },
        /*
            뷰어상태
        */
        status: {
            /**
             * 0: 첫페이지 부터 양면
             * 1: 그냥 양면
             * 2: 단면
             * 3: 스크롤
             * 4: 가로 스크롤
             */
            mode: 3,

            /**
             * 가로 , 세로 상관없음 유저가 설정한 값을 따라감
             * 0: 첫페이지 부터 양면
             * 1: 그냥 양면
             * 2: 단면
             * 3: 스크롤
             */
            uiMode: 0,

            //pdf object
            pdf: null,

            //현재페이지
            currentPage: 1,

            //전체페이지
            total: 0,

            //현재 pdf 줌 비율
            zoom: 1.0,

            //canvas의 x,y,z 좌표
            // transform: [1,0,0,1,0,0]
            transform: null
        },
        /*
            뷰어 메타 데이터
        */
        meta: {
            toc: null,
            title: null,
            subject: null,
            author: null,
            creater: null,
        },
        init(url , token){
            console.log('## PdfEngine init')
            var ChunkedParams = null
            if (Config.useChunked){
                ChunkedParams = {
                    "url": url,
                    "cMapPacked": true,
                    "cMapUrl": "../lib/cmaps/",
                    "disableAutoFetch": true,
                    "disableFontFace": false,
                    "disableRange": false,
                    "disableStream": true,
                    "docBaseUrl": "",
                    "enableXfa": true,
                    "fontExtraProperties": false,
                    "isEvalSupported": true,
                    "maxImageSize": -1,
                    "pdfBug": false,
                    "standardFontDataUrl": "../pdf/lib/standard_fonts/",
                    "verbosity": 1,
                    "httpHeaders": { "authorization": token },
                    "withCredentials": true
                }
            } 
            //pdf 워커 위치
            pdfjsLib.GlobalWorkerOptions.workerSrc = Config.workerSrc
            if (ChunkedParams){
                pdfjsLib.getDocument(ChunkedParams).promise.then(Config.onLoadPdf)
            } else {
                pdfjsLib.getDocument(url).promise.then(Config.onLoadPdf)
            }
        },
      
        onLoadPdf: async function(pdf){
            console.log('## PdfEngine onLoadPdf',pdf)
            Config.status.pdf = pdf
            if (pdf._pdfInfo){
                Config.status.total = pdf._pdfInfo.numPages
                Config.element.totalPage.textContent = Config.status.total
                Config.element.inputRange.min = 1
                Config.element.inputRange.max = Config.status.total
            }

            //목차 
            //parse toc
            console.log('### parse toc init',Config.meta.toc)
            let tt = await pdf.getOutline()
            Config.meta.toc = tt
            await Toc.makeToc(tt , 0)
            Toc.createTocFragment()
            Config.element.tocList.appendChild(Toc.tocDocFragment)
            console.log('### parse toc end',Toc.tocList)

            console.log('### search data parse init')
            // Search.init()
            console.log('### search data parse end')

            //not use
            // pdf.getAttachments().then(data=>{
            //     console.log('## attach',data)
            // })
            
            //pdf byteArray
            // pdf.getData().then(data=>{
            //     console.log('## data',data)
            // })

            //pdf 다운로드 정보
            // pdf.getDownloadInfo().then(data=>{
                // console.log('## download info',data)
            // })

            //pdf 적용중인 페이지 모드
            // pdf.getPageMode().then(data=>{
                // console.log('## pagemode',data)
            // })
            //pdf 제목, 저자등 .. 메타데이터
            // pdf.getMetadata().then(data=>{
                // console.log('## metadata',data.info)
                // Config.meta.toc = data.info
            // })

            View.setMode(Config.status.mode)
            Event.init()
            View.update()
            

        }
    }

    /**
     * ui 관련
     * view
     */ 
    var View = SHARED.View = {
        setMode: function(mode , updateUI){
            Config.status.mode = mode
            switch(mode){
                /**
                 * 첫페이지부터양면
                 */
                case 0: 
                    Config.element.dualLayer.style.display = ''
                    Config.element.singleLayer.style.display = 'none'
                    Config.element.scrolLayer.style.display = 'none'
                    Config.element.hScrollLayer.style.display = 'none'

                    let dualWrapper = Config.element.dualLayer.querySelector('div')
                    let canvasElements = dualWrapper.querySelectorAll('canvas')
                    if (canvasElements.length === 0){
                        dualWrapper.appendChild(document.createElement('canvas'))
                        dualWrapper.appendChild(document.createElement('canvas'))
                    } else if (canvasElements.length === 1){
                        dualWrapper.appendChild(document.createElement('canvas'))
                    } else if (canvasElements.length === 2){

                    }

                    // console.log(canvasElements.length)
                break;
                /**
                 * 그냥 양면
                 */
                case 1: 
                    Config.element.dualLayer.style.display = ''
                    Config.element.singleLayer.style.display = 'none'
                    Config.element.scrolLayer.style.display = 'none'
                    Config.element.hScrollLayer.style.display = 'none'
                break;
                /**
                 * 단면
                 */
                case 2: 
                    Config.element.dualLayer.style.display = 'none'
                    Config.element.singleLayer.style.display = ''
                    Config.element.scrolLayer.style.display = 'none'
                    Config.element.hScrollLayer.style.display = 'none'
                break;

                /**
                 * 스크롤
                 */
                case 3: 
                    Config.element.dualLayer.style.display = 'none'
                    Config.element.singleLayer.style.display = 'none'
                    Config.element.scrolLayer.style.display = ''
                    Config.element.hScrollLayer.style.display = 'none'

                    let _scrollWrapper = Config.element.scrolLayer.querySelector('div')
                    let mUl = _scrollWrapper.querySelector('ul')
                    if (!mUl){
                        let vUl = document.createElement('ul')
                        vUl.addEventListener('scroll',View.onVScrollChangeListener) 
                        for (var i=0; i<Config.status.total; i++){
                            let li = document.createElement('li')
                            let cvs = document.createElement('canvas')
                            li.appendChild(cvs)
                            vUl.appendChild(li)
                            li = null;
                            cvs = null;
                        }
                        _scrollWrapper.appendChild(vUl)
                    }
                break;

                /**
                 * 가로 스크롤 모드
                 */
                case 4:
                    Config.element.dualLayer.style.display = 'none'
                    Config.element.singleLayer.style.display = 'none'
                    Config.element.scrolLayer.style.display = 'none'
                    Config.element.hScrollLayer.style.display = ''

                    let hscrollWrapper = Config.element.hScrollLayer.querySelector('div')
                    let mUl2 = hscrollWrapper.querySelector('ul')
                    if (!mUl2){
                        let ul2 = document.createElement('ul')
                        ul2.addEventListener('scroll',View.onHScrollChangeListener) 
                        for (var i2 =0; i2<Config.status.total; i2++){
                            let li2 = document.createElement('li')
                            let cvs2 = document.createElement('canvas')
                            li2.appendChild(cvs2)
                            ul2.appendChild(li2)
                        }
                        hscrollWrapper.appendChild(ul2)
                    }
                break;
            }

            if (updateUI){
                View.update()
            }
        },
        
        update(){
            switch(Config.status.mode){
                /**
                 * 첫페이지 양면
                 */
                case 0 :
                    let dualWrapper = Config.element.dualLayer.querySelector('div').querySelectorAll('canvas')
                    if (dualWrapper){
                        //첫 페이지 부터 양면보기일시 
                        if (Config.status.currentPage === 1 ){
                            let mCanvas0 = dualWrapper[0]
                            let mCanvas1 = dualWrapper[1]
                            mCanvas0.style.display = ''
                            mCanvas1.style.display = 'none'
                            View._update(mCanvas0,Config.status.currentPage)
                        } else {
                            dualWrapper.forEach((canvas , idx) =>{
                                canvas.style.display = ''
                                View._update(canvas,Config.status.currentPage + idx)
                            })
                        }
                    }
                break;

                /**
                 * 그냥 양면
                 */
                case 1 : 
                    let dualWrapper2 = Config.element.dualLayer.querySelector('div').querySelectorAll('canvas')
                    if (dualWrapper2){
                        dualWrapper2.forEach((canvas , idx) =>{
                            canvas.style.display = ''
                            View._update(canvas,Config.status.currentPage + idx)
                        })
                    }
                break;

                /**
                 * 단면
                 */
                case 2 :
                    let singleDiv = Config.element.singleLayer.querySelector('div') 
                    let singleCanvas = Config.element.singleLayer.querySelector('div').querySelector('canvas')
        
                    console.log(singleCanvas)
                    if (singleCanvas){
                        View._update(singleCanvas,Config.status.currentPage )
                    }
                break;

                /**
                 * 스크롤
                 */
                case 3 : 
                    let scrollDiv = Config.element.scrolLayer.querySelector('div') 
                    let scrollul = scrollDiv.querySelector('ul') 
                    if (scrollul){
                        let scrollli = scrollul.querySelectorAll('li')
                        let total = Config.status.currentPage+5
                        let current = Config.status.currentPage
                        let tasks = []
                        for (let i = current-1; i<total; i++){
                            let scrollCanvas = scrollli[i].querySelector('canvas')
                            scrollCanvas.style.display = 'none'
                            if (scrollCanvas){
                                tasks.push(View._updateTask(scrollCanvas,i+1))
                            }
                        }
                        //전부 로드가 끝낫으면 이동
                        Promise.all(tasks).then(values=>{
                            // Navi.moveToPage_vertical(Config.status.currentPage)
                        })
                        scrollli = null
                        total = null
                        current = null
                        tasks = null
                    }
                break;

                /**
                 * 가로 스크롤
                 */
                case 4:
                    Loader.loadMoreScrollItem(Config.status.currentPage)
                    Navi.moveToPage_horizontal(Config.status.currentPage)
                break;
            }
        },
        /**
         * 
         * @param {*} canvas canvas element
         * @param {*} renderingPage 보여줄 페이지 번호
         */ 
        _update(canvas , renderingPage ){
            let mPage = renderingPage
            let zoom = Config.status.zoom
            let transform = null
            Config.status.pdf.getPage(mPage).then((page) => {
                
                let ratio = window.devicePixelRatio;
                let scale = zoom*ratio

                var viewport = page.getViewport({
                    scale: scale
                  });

                //viewport를 원하는 픽셀로 리사이즈
                let desireViewport  = null
                let renderContext = null
                let desireScale  = null
                let vp = null
                //스크롤
                if (Config.status.mode === 3){
                    let size = window.innerWidth >= window.innerHeight?window.innerHeight:window.innerWidth
                     desireScale = size / viewport.width;
                    desireViewport = page.getViewport({
                        scale: desireScale * ratio *0.7
                      });
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    canvas.style.width = desireViewport.width + 'px'
                    canvas.style.height = desireViewport.height + 'px'
                    // Config.element.scrolLayer.querySelector('div').style.width = desireViewport.width + 'px'
                    vp = viewport;
                } 
                //가로 스크롤
                else if (Config.status.mode === 4){
                    let desireHeight = window.innerHeight - Config.element.viewerWrapper.getBoundingClientRect().top
                    desireScale = desireHeight  / viewport.height;
                    desireViewport = page.getViewport({
                        scale: desireScale * ratio *0.7
                    });
                    canvas.width = viewport.width;
                    canvas.height = viewport.height;
                    canvas.style.width = desireViewport.width + 'px'
                    canvas.style.height = desireViewport.height + 'px'
                    Config.element.hScrollLayer.querySelector('div').style.height = desireViewport.height + 'px'
                    vp = viewport;
                }
                //페이지 모드
                else {
                    desireScale = window.innerHeight / viewport.height;
                    desireViewport = page.getViewport({
                        scale: desireScale
                    });
                    canvas.width = desireViewport.width;
                    canvas.height = desireViewport.height;
                    vp = desireViewport;
                }

                renderContext = {
                    canvasContext: canvas.getContext('2d',{alpha: false}),
                    viewport: vp,
                    transform: transform,
                }

                let task = page.render(renderContext);
                task.promise.then(()=>{
                    View.onPageRendered(mPage,page,canvas)
                })
                .catch((e)=>{
                    View.onPageRenderFail(e)
                })
            })
        },
        /**
         * 
         * @param {*} canvas canvas element
         * @param {*} renderingPage 보여줄 페이지 번호
         */ 
         _updateTask: async function (canvas , renderingPage ){
            let mPage = renderingPage
            let zoom = Config.status.zoom
            let transform = null
            let page = await Config.status.pdf.getPage(mPage)

            let ratio = window.devicePixelRatio;
            let scale = zoom*ratio

            var viewport = page.getViewport({
                scale: scale
              });

            //viewport를 원하는 픽셀로 리사이즈
            let desireViewport  = null
            let renderContext = null
            let desireScale  = null
            let vp = null
            //스크롤
            if (Config.status.mode === 3){
                let size = window.innerWidth >= window.innerHeight?window.innerHeight:window.innerWidth
                desireScale = size / viewport.width;
                desireViewport = page.getViewport({
                    scale: desireScale * ratio *0.7
                });
                canvas.width = viewport.width;
                canvas.height = viewport.height;
                canvas.style.width = desireViewport.width + 'px'
                canvas.style.height = desireViewport.height + 'px'
                vp = viewport;
            } 
            //가로 스크롤
            else if (Config.status.mode === 4){
                let desireHeight = window.innerHeight - Config.element.viewerWrapper.getBoundingClientRect().top
                desireScale = desireHeight  / viewport.height;
                desireViewport = page.getViewport({
                    scale: desireScale * ratio *0.7
                });
                canvas.width = viewport.width;
                canvas.height = viewport.height;
                canvas.style.width = desireViewport.width + 'px'
                canvas.style.height = desireViewport.height + 'px'
                Config.element.hScrollLayer.querySelector('div').style.height = desireViewport.height + 'px'
                vp = viewport;
            }
            //페이지 모드
            else {
                desireScale = 500 / viewport.height;
                desireViewport = page.getViewport({
                    scale: desireScale
                });
                canvas.width = desireViewport.width;
                canvas.height = desireViewport.height;
                vp = desireViewport;
            }

            renderContext = {
                canvasContext: canvas.getContext('2d',{alpha: false}),
                viewport: vp,
                transform: transform,
            }
            let task = await page.render(renderContext);

            page.getAnnotations().then(anno=>{
                console.log('## annotation',anno)
            })
            
            canvas.style.display = ''
            return task.promise
        },
        /**
         * page render callback
         * @param {*} page 페이지 번호
         * @param {*} pageInfo 페이지 정보
         * @param {*} canvas 캔바스
         */
        onPageRendered: function(page, pageInfo , canvas){
            console.log('##onPageRendered!' ,page, pageInfo , canvas)
            if (Config.status.mode === 3 || Config.status.mode === 4){
                canvas.style.display = ''
                console.log(canvas.closest())
            }
        },
        /**
         * page render fail callback
         * @param {*} exception 
         */
        onPageRenderFail: function(exception){

        },
        onHScrollChangeListener: function(e){
            // e.preventDefault()
            console.log('## onHScrollChangeListener',e,this.scrollLeft)
        },
        onVScrollChangeListener: function(e){
            // e.preventDefault()
            console.log('## onVScrollChangeListener',e,this.scrollTop);

            // Config.element.scrolLayer.querySelector(ul)
        },
 
    }


    var Loader = SHARED.Loader = {
        isLoadingProccess: false,
        /**
         * 가로모드 컨텐츠
         * 페이지를 더 불러온다
         * @param {*} pageNumberToLoad 불러올 기준될 페이지
         * @param {*} pageNumberToLoad 불러올 기준될 페이지
         */
        loadMoreScrollItem: function(pageNumberToLoad){
            if (Loader.isLoadingProccess){
                return false
            }
            Loader.isLoadingProccess = true

            let scrollDiv2 = null
            if (Config.status.mode == 3){
                scrollDiv2 = Config.element.scrolLayer.querySelector('div') 
            } else if (Config.status.mode == 4){
                scrollDiv2 =  Config.element.hScrollLayer.querySelector('div') 
            }

            if (!scrollDiv2){
                Loader.isLoadingProccess = false
                return false
            }
            
            let scrollul2 = scrollDiv2.querySelector('ul')
            if (!scrollul2){
                Loader.isLoadingProccess = false
                return false
            }


            let scrollli2 = scrollul2.querySelectorAll('li')
            let total2 = pageNumberToLoad+5
            let current2 = pageNumberToLoad
            let cPage = current2-1;
            let updateCount = 0;
            for (let i2 = cPage; i2<total2; i2++){
                let li_tag_2 = scrollli2[i2]
                let scrollCanvas2 = li_tag_2.querySelector('canvas')
                scrollCanvas2.style.display = 'none'
                if (scrollCanvas2){
                    updateCount++;
                    View._update(scrollCanvas2,i2+1)
                }
            }
            Loader.isLoadingProccess = false
            return updateCount>0
        },
        loadSingle:function(page){
            let scrollDiv2 = null
            if (Config.status.mode == 3){
                scrollDiv2 = Config.element.scrolLayer.querySelector('div') 
            } else if (Config.status.mode == 4){
                scrollDiv2 =  Config.element.hScrollLayer.querySelector('div') 
            }

            if (!scrollDiv2){
                return;
            }

            let li = scrollDiv2.querySelector('ul').querySelectorAll('li')[page-1]
            let canvas = li.querySelector('canvas')
            let task = View._updateTask(canvas,page)
            return task
        },
        /**
         * 전체 로드
         * 
         */
        loadAll:async function(){
            console.log('##start load')
            let tasks = []
            for(let i =1; i<Config.status.total; i++){
                tasks.push(Loader.loadSingle(i))
            }
            Promise.all(tasks).then(values=>{
                console.log('##load all end')
            })
        },
        load: async function(wrapper , renderingPage){
            let canvas = wrapper.querySelector('canvas')
            if (!canvas){
                canvas = Loader.getCanvas()
                wrapper.querySelector('.pdf-canvas-wrapper').appendChild(canvas)
            }
            
            let textWrapper = wrapper.querySelector('.textLayer')
            let annotationWrapper = wrapper.querySelector('.annotationLayer')

            let mPage = renderingPage
            let zoom = Config.status.zoom
            let transform = null
            let page = await Config.status.pdf.getPage(mPage)
            let ratio = window.devicePixelRatio;
            let scale = zoom*ratio
            var viewport = page.getViewport({
                scale: scale
            });

            //viewport를 원하는 픽셀로 리사이즈
            let desireViewport  = null
            let renderContext = null
            let desireScale  = null
            desireScale = window.innerWidth / viewport.width;
            desireViewport = page.getViewport({
                scale: desireScale * ratio *0.5
            });

            let cWidth = Math.floor(desireViewport.width)
            let cHeight = Math.floor(desireViewport.height)
            canvas.width =  Math.floor(viewport.width);
            canvas.height = Math.floor(viewport.height);
            canvas.style.width = cWidth + 'px'
            canvas.style.height = cHeight + 'px'
            vp = viewport;

   
            //parst annotaion
            let anno = await page.getAnnotations()
            if(anno && anno.length>0){
                // console.log(renderingPage,anno,page,page.view)
                wrapper.style.background = 'red'
                anno.forEach((annoItem,annoIndex)=>{
                    let div = document.createElement('div')
                    div.style.zIndex = 50
                    div.style.background = 'green'
                    div.style.cursor = 'pointer'
                    div.style.position = 'absolute';
                    div.style.left = 0
                    div.style.top = 0
                    div.style.width = 30+'px'
                    div.style.height = 30+'px'

                    // subtype
                    let annoRect = annoItem.rect
                    let annoView = page.view
                    let type = annoItem.subtype
                    if (type === 'Link'){
                        let mDest = annoItem.dest
                        if (mDest){
                            let [destRef] = mDest
                            Config.status.pdf.getPageIndex(destRef).then(position=>{
                                div.style.left = 0+'px'
                                div.style.top = 0+'px'
                                div.setAttribute('type','link')
                                div.setAttribute('position',position)
                                div.addEventListener('click', function(){
                                    alert(position)
                                })
                            })
                        }
                    }
            
                    annotationWrapper.appendChild(div)
                })  
            }
            /**
             * text layer
             */
            let textContent = await page.getTextContent()
            pdfjsLib.renderTextLayer({
                textContent: textContent,
                container: textWrapper,
                viewport: desireViewport,
                textDivs: []
            })
            // console.log(text)

            renderContext = {
                canvasContext: canvas.getContext('2d',{alpha: false}),
                viewport: vp,
                transform: transform,
            }
            let task = await page.render(renderContext);
            console.log('#####################',mPage, Config.status.total)
            canvas = null
            return task.promise
        },
        check: async function(){
            // let svc = document.createElement('cavas')
            let docf = document.createDocumentFragment()
            for(var i =1; i<Config.status.total; i++){
                let svc = document.createElement('canvas')
                svc.style.display = 'none'
                docf.appendChild(svc);
            }


            docf.querySelectorAll('canvas').forEach((e,i)=>{
                Loader.load(e,i+1)
            })
        },

        /**
         * create element
         * @returns 
         */
        getCanvas:function(){
            let svc = document.createElement('canvas')
            svc.style.margin = 0
            svc.style.margin = 0
            svc.style.display = 'none'
            return svc
        },
        getTextLayer:function(){
            let textLayer = document.createElement('div')
            textLayer.classList.add('textLayer')
            textLayer.style.width = '100%'
            textLayer.style.height = '0'
            textLayer.style.left = '0'
            textLayer.style.top = '0'
            textLayer.style.right = '0'
            textLayer.style.bottom = '0'
            textLayer.style.overflow = 'hidden'
            textLayer.style.opacity = 0.2
            textLayer.style.lineHeight = 1.0
            return textLayer
        },
        getAnnotaionLayer: function(){
            let annotationLayer = document.createElement('div')
            annotationLayer.classList.add('annotationLayer')
            annotationLayer.style.width = '100%'
            annotationLayer.style.height = '0'
            annotationLayer.style.left = '0'
            annotationLayer.style.top = '0'
            annotationLayer.style.right = '0'
            annotationLayer.style.bottom = '0'
            annotationLayer.style.overflow = 'hidden'
            annotationLayer.style.opacity = 0.2
            annotationLayer.style.lineHeight = 1.0
            return annotationLayer
        },
        createPdfItemElement: function(idx){
            //text
            let textLayer = Loader.getTextLayer()

            //annotaion
            let annotationLayer = Loader.getAnnotaionLayer();

            //li
            let li = document.createElement('li')
            li.style.position = 'relative'

            let scvDiv = document.createElement('div')
            scvDiv.classList.add('pdf-canvas-wrapper')
            let svc = Loader.getCanvas()
            scvDiv.appendChild(svc)

            let inner = document.createElement('div')
            inner.appendChild(scvDiv)
            inner.appendChild(textLayer)
            inner.appendChild(annotationLayer)
            inner.setAttribute('idx',idx)

            li.appendChild(inner)
            return li
        },
        docfragment: document.createDocumentFragment(),
        loadChaining: function(idx){
            let li = Loader.createPdfItemElement(idx)
            // let svc = li.querySelector('canvas')
            Loader.load(li,idx).then(value=>{
                if(idx >= Config.status.total ){
                    console.log('## end of chain')
                    let ulList = Config.element.scrolLayer.querySelector('ul')
                    ulList.innerHTML = ''
                    ulList.appendChild(Loader.docfragment)
                    ulList.querySelectorAll('canvas').forEach(e=>{
                        e.style.display = ''
                    })
                    ulList = null
                } else {
                    Loader.docfragment.appendChild(li)
                    
                    Loader.loadChaining(idx+1)
                }   
            })
            svc = null
        }
    }

    /**
     * event bind
     */ 
    var Event = SHARED.Event = {
        init: function(){
            Config.element.inputRange.addEventListener('change',Event.onChangeInput)
        },
        onChangeInput: function(){
            Navi.movePage( parseInt(this.value) )
        }
    }

    /**
     * 목차
     */
    var Toc = SHARED.Toc = {
        _tocCount: 0,
        tocList: [],
        tocDocFragment: null,
        makeToc: async function( tocList , depth){
            if (!tocList){
                return;
            }
            let pdf = PdfEngine.Config.status.pdf

            for (e of tocList){
                console.log('toc',e)
                let mDest = e.dest;
                let items = e['items'];
                let title = e['title'];
                if (mDest){
                    // string type
                    if (typeof mDest === 'string'){
                        console.log('## getPageIndex-> ',mDest , title,e.dest)
                        let obj = null
                        if (items && items.length>0){
                            obj = {'title':title, 'page': 0,  'items':items, 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                            //한번더 깊이를 탐색할때마다 기존 depth에 0
                            Toc.makeToc( items ,depth+1)
                        } else {
                            obj = {'title':title, 'page': 0, 'items':items , 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                        }
                    } 
                    //object type
                    else if (typeof mDest === 'object'){
                        let [destRef] = mDest
                        let position = await pdf.getPageIndex(destRef);
                        let obj = null
                        position+=1
                        if (items && items.length>0){
                            obj = {'title':title, 'page': position, 'items':items, 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                            Toc.makeToc(items,depth+1)
                        }  
                        
                        else {
                            obj = {'title':title, 'page': position, 'items':items , 'depth':depth , 'dest': mDest}
                            Toc.tocList.push(obj)
                        }

                        destRef = null
                        position = null
                        obj = null
                    } 
                    
                } 
                // null
                else {
                    // console.log('### no dest')
                }
                mDest = null
                items = null
                title = null
            }

            if (Toc.tocList.length>0){
                Toc.tocList.sort((a,b)=>a.page-b.page)
            }
        },

        createTocFragment: function(){
            let docf = document.createDocumentFragment()
            Toc.tocList.forEach(e=>{
                console.log(e)
                let li = document.createElement('li')
                let ptag = document.createElement('p')
                ptag.textContent = e.title
                ptag.style.marginLeft = (e.depth+1) * 10 + 'px'
                li.appendChild(ptag)
                docf.appendChild(li)
            })
            Toc.tocDocFragment = docf
        },
        toggleToc:function(){
            if (Config.element.tocWrapper.style.display === 'none'){
                Config.element.tocWrapper.style.display = 'block'
            } else {
                Config.element.tocWrapper.style.display = 'none'
            }
           
        }
    }

    /**
     * 검색 관련 클래스
     */
    var Search = SHARED.Search = {
        /**
         * 검색 관련 정보 
         */
        searchInfo: [],
        /**
         * 검색 정보 파싱
         */
        init: async function(){
            for (var i = 1; i <= Config.status.total; ++i) {
                let mPage = await Config.status.pdf.getPage(i)
                let textContent = await mPage.getTextContent()
                let info = { 'page': i ,'obj': textContent }
                Search.searchInfo.push(info)
                mPage = null
                textContent = null
                info = null
            }
        },
        /**
         * 검색 결과를 array로 return
         * @param {*} keyword 
         * @returns 
         */
        _search: function(keyword){
            let result = []
            if (!Search.searchInfo){
                throw 'search info parse error' 
            }
            if (Search.searchInfo.length == 0){
                throw 'search Info Empty'
            }
            Search.searchInfo.forEach((e,idx)=>{
                let mPage = e.page
                e.obj.items.forEach((e2,idx2)=>{
                    if (e2.str.includes(keyword)){
                        result.push({text:e2.str , page: mPage})
                    }
                })
            })
            return result
        },
        /**
         * 키워드에 따른 ui 결과값
         * @param {*} keyword 
         * @returns 
         */
        getSearchUI: function(keyword){
            let result = null
            let searchResult = null
            try{
                let searchResult = Search._search(keyword)
                let searchDocf = document.createDocumentFragment()

                if (searchResult && searchResult.length > 0){
                    let ul = document.createElement('ul')
                    searchResult.forEach(rst=>{
                        let li = document.createElement('li')
                        let ptag = document.createElement('p')
                        ptag.textContent = rst.text
                        li.appendChild(ptag)
                        ul.appendChild(li)
                    })
                    searchDocf.appendChild(ul)
                } else {
                    let div = document.createElement('div')
                    let ptag = document.createElement('p')
                    ptag.textContent = '검색 결과가 없습니다.'
                    div.appendChild(ptag) 
                    searchDocf.appendChild(div)
                }
                result = searchDocf
            } catch(e){
                console.error('getSearchUI error',e)
            } finally{
                searchResult = null
            }
            return result
        },
        /**
         * 검색 function
         * @returns 
         */
        onSearch: function(){
            let keyword = Config.element.searchIP.value
            if (!keyword){
                return
            }
            //2자 이상
            if (keyword.length < 2){
                return
            }
            //20자 이내
            if (keyword.length > 20){
                return
            }

            let search_ui = Search.getSearchUI(keyword)
            if (search_ui){
                Config.element.searchContents.innerHTML = '';
                Config.element.searchContents.appendChild(search_ui)
            }
        },
        /**
         * 검색 layer toggle
         */
        onToggleSearch: function(){
            if (Config.element.searchWrapper.style.display === 'none'){
                Config.element.searchWrapper.style.display = 'block'
            } else {
                Config.element.searchWrapper.style.display = 'none'
            }
        }
    }

    /**
     * 이동
     * navigation
     * 단면, 스크롤
        전부 가능

        첫페이지 부터 양면 
        12 34 56 78
        current값 : 홀수 유지

        그냥 양면 ()
        1 23 45 67 89
        current값 : 1 제외 , 짝수 유지
     */ 
    var Navi = SHARED.Navi = {
        movePage(page){
            //같은경우 ui 업데이트 할 필요가 없다.
            if (Config.status.currentPage === page){
                console.log('## movePage same')
                return
            }
            //처음페이지
            if (page <= 0 ) {
                console.log('## movePage first')
                return
            }
            //마지막 페이지
            if (Config.status.total < page){
                console.log('## movePage last')
                return
            }
            Config.status.currentPage = page
            View.update()
        },
        /**
         * 가로 스크롤일때 무브
         * @param {*} page 
         */
        moveToPage_horizontal: function(page){
            let scrollDiv2 = Config.element.hScrollLayer.querySelector('div') 
            let scrollul2 = scrollDiv2.querySelector('ul')
            let scrollli2 = scrollul2.querySelectorAll('li')
            scrollli2[page-1].scrollIntoView()
        },
        moveToPage_vertical: function(page){
            let scrollDiv2 = Config.element.scrolLayer.querySelector('div') 
            let scrollul2 = scrollDiv2.querySelector('ul')
            let scrollli2 = scrollul2.querySelectorAll('li')
            scrollli2[page-1].scrollIntoView()
        },
        prev(){
            if (Config.status.mode === 4){
                let mv = Config.status.currentPage-1
                mv-=1

                Config.status.currentPage = Config.status.currentPage-1
                Config.element.hScrollLayer.querySelector('ul').querySelectorAll('li')[mv].scrollIntoView()
                return
            } 
            //모드에 따라 변경되어야할 값
            let dynamicValue = 1
            let temp = Config.status.currentPage - dynamicValue 
            Navi.movePage(temp)
        },
        next(){
            if (Config.status.mode === 4){
                let mv = Config.status.currentPage-1
                mv+=1
                Config.status.currentPage = Config.status.currentPage+1
                Config.element.hScrollLayer.querySelector('ul').querySelectorAll('li')[mv].scrollIntoView()
                return
            } 
            //모드에 따라 변경되어야할 값
            let dynamicValue = 1
            let temp = Config.status.currentPage + dynamicValue 
            Navi.movePage(temp)
        }
    }


    /**
     * 줌
     * zoom
     */  
    var Zoom = SHARED.Zoom = {
        setZoom(zoom){
            if (Config.status.zoom === zoom){
                console.log('## setZoom same')
                return
            }
            Config.status.zoom = zoom
            View.update()
        },
        zoomOrigin(){
            let temp = 1.0
            Zoom.setZoom(temp)
        },
        zoomPlus(){
            let temp = Config.status.zoom + 0.2
            Zoom.setZoom(temp)
        },
        zoomMinus(){
            let temp = Config.status.zoom - 0.2
            Zoom.setZoom(temp)
        },
    }
    return SHARED
}());


function fullscreen(element){
    if (element.requestFullscreen) 
        element.requestFullscreen();
    else if (element.webkitRequestFullscreen) 
        element.webkitRequestFullscreen();
    else if (element.msRequestFullScreen) 
        element.msRequestFullScreen();
}

function reqFullscreen(){
    // fullscreen(document.querySelector('.viewer-wrapper'))
    fullscreen(document.body)
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e.preventDefault();
}

function preventDefaultForScrollKeys(e) {
  if (keys[e.keyCode]) {
    preventDefault(e);
    return false;
  }
}

// modern Chrome requires { passive: false } when adding event
var supportsPassive = false;
try {
  window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
    get: function () { supportsPassive = true; } 
  }));
} catch(e) {}

var wheelOpt = supportsPassive ? { passive: false } : false;
var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

// call this to Disable
function disableScroll() {
  window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
  window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
  window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
  window.addEventListener('keydown', preventDefaultForScrollKeys, false);
}

function activeScroll(){

}

document.documentElement.addEventListener('touchstart', function (event) { if (event.touches.length > 1) { event.preventDefault(); } }, false);

