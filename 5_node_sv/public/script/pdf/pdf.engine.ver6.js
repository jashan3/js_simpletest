class PixelsPerInch {
  static CSS = 96.0;

  static PDF = 72.0;

//   static PDF_TO_CSS_UNITS = this.CSS / this.PDF;
  static PDF_TO_CSS_UNITS = this.CSS / this.PDF;
}

/**
 * Scale factors for the canvas, necessary with HiDPI displays.
 */
class OutputScale {
    constructor() {
      const pixelRatio = window.devicePixelRatio || 1;
  
      /**
       * @type {number} Horizontal scale.
       */
      this.sx = pixelRatio;
  
      /**
       * @type {number} Vertical scale.
       */
      this.sy = pixelRatio;
    }
  
    /**
     * @type {boolean} Returns `true` when scaling is required, `false` otherwise.
     */
    get scaled() {
      return this.sx !== 1 || this.sy !== 1;
    }
}

const MAX_CANVAS_PIXELS =  16777216;

/**
 * pdf module
 */
var PdfEngine =
  PdfEngine ||
  (function () {
    //instance
    var SHARED = {};

    var Config = (SHARED.Config = {
      element: {
        viewerWrapper: document.querySelector(".viewer-wrapper"),
        scrollLayer: document.getElementById("viewer-scroll-mode-wrapper"),
        currentPage: document.getElementById("current-page"),
      },
      /**
       * 워커 경로
       */
      workerSrc:
        "https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.13.216/pdf.worker.min.js",
      /*
            뷰어상태
        */
      status: {
        scrollInfo: null,
        pdf: null,
        currentPage: 0,
      },
      viewport: null,
      scale: 1,
      useChunked: false,
      url: "",
      token: "",
      init(url, token) {
        Config.url = url;
        Config.token = token;
        var ChunkedParams = null;
        if (Config.useChunked) {
          ChunkedParams = {
            url: url,
            cMapPacked: true,
            cMapUrl: "/script/pdflib/cmaps/",
            disableAutoFetch: true,
            disableFontFace: false,
            disableRange: false,
            disableStream: true,
            docBaseUrl: "",
            enableXfa: true,
            fontExtraProperties: false,
            isEvalSupported: true,
            // maxImageSize: -1,
            pdfBug: false,
            standardFontDataUrl: "/script/pdflib/standard_fonts/",
            verbosity: 1,
            httpHeaders: { authorization: token },
            withCredentials: true,
            maxImageSize: 1024 * 1024,
          };
        }
        //pdf 워커 위치
        pdfjsLib.GlobalWorkerOptions.workerSrc = Config.workerSrc;
        
        if (ChunkedParams) {
          pdfjsLib.getDocument(ChunkedParams).promise.then(Config.onLoadPdf);
        } else {
          pdfjsLib
            .getDocument({
              url: url,
              maxImageSize: -1,
              disableAutoFetch: true,
              disableFontFace: false,
              disableRange: false,
              disableStream: true,
            })
            .promise.then(Config.onLoadPdf);
        }
      },
      outlineList: [],
      onLoadPdf: (pdf) => {
        Config.status.pdf = pdf;
        for (let index = 0; index < pdf.numPages; index++) {
          const div = document.createElement("div");
          div.classList.add("recyler-inner-item");
          div.id = 'recyler-inner-item_'+ index;
          Config.element.viewerWrapper.appendChild(div)
          View.rendering(index+1);
        }

        // pdfjsLib.getDocument({
        //   url: Config.url,
        //   cMapPacked: true,
        //   cMapUrl: "/script/pdflib/cmaps/",
        //   disableAutoFetch: true,
        //   disableFontFace: false,
        //   disableRange: false,
        //   disableStream: true,
        //   docBaseUrl: "",
        //   enableXfa: true,
        //   fontExtraProperties: false,
        //   isEvalSupported: true,
        //   // maxImageSize: -1,
        //   pdfBug: false,
        //   standardFontDataUrl: "/script/pdflib/standard_fonts/",
        //   verbosity: 1,
        //   httpHeaders: { authorization: Config.token },
        //   withCredentials: true,
        //   maxImageSize: 1024 * 1024,
        // }).promise.then((pdf2)=>{
        //   Config.getTocs(pdf2).then(list=>{
        //     console.log('목차리스트',list);
        //     console.timeEnd('TOC');
        //   }).catch(err=>{
        //     console.error('에러처리',err);
        //     console.timeEnd('TOC');
        //   })
        // })
        // Config.getTocs(pdf).then(list=>{
        //   console.log('목차리스트',list);
        //   console.timeEnd('TOC');
        // }).catch(err=>{
        //   console.error('에러처리',err);
        // })
      },
      /**
       * 목차 리스트 생성 및 dest 페이지 파싱
       * @param {*} pdf 
       * @returns 
       */
      getTocs(pdf){
        return Config._getTocs(pdf).then((list) =>{
          if (!list){
            return list;
          }
          return Promise.all(
            list.map((item) =>
              Config._parseDestToPage(pdf, item.dest).then((page) => {
                delete item.dest;
                item.page = page;
                return item;
              })
            )
          )
        })
      },
      /**
       * 목차 리스트 생성
       * @param {*} pdf 
       * @returns 
       */
      _getTocs(pdf){
        return pdf.getOutline().then((tocs) =>{
          console.log('TOCS')
          if (!tocs){
            return null;
          }
          const outlineList = []
          for (let index = 0; index < tocs.length; index++) {
            Config._parseToc(outlineList,tocs[index]);
          }
          return outlineList;
        })
      },
      /**
       * 목차 파싱
       * @param {*} outlineList 
       * @param {*} toc 
       * @param {*} depth 
       */
      _parseToc(outlineList,toc,depth = 0){
        if (toc){
          const { bold , color, count, dest, italic, items, newWindow, title, unsafeUrl, url } = toc;
          outlineList.push({ title, depth, dest, bold, italic, color });
          if (items && Array.isArray(items) && items.length >0){
            for (let index = 0; index < items.length; index++) {
              Config._parseToc(outlineList,items[index],depth+1);
            }
          }
        }
      },
      /**
       * dest를 page로 파싱
       * @param {*} pdfDocument 
       * @param {*} dest 
       * @returns 
       */
      _parseDestToPage(pdfDocument,dest){
        if (!dest) {
          return Promise.resolve(null);
        }
        if (typeof dest === "string") {
          return pdfDocument.getDestination(dest);
        } else {
          const [destRef] = dest;
          if (destRef){
            return pdfDocument.getPageIndex(destRef);
          } else {
            return Promise.resolve(null);
          }
        }
      }
    });

    const View = SHARED.View = {
      rendering(page = 1){
        Config.status.pdf
          .getPage(page)
          .then((pageTask) => {
            const pdfview = new PDFView(
              Config.element.viewerWrapper.children[page - 1]
            );
            pdfview.setPdf(pageTask);
            Config.element.viewerWrapper.children[page - 1].appendChild(
              pdfview.div
            );
          })
          .catch((err) => {
            console.log("pageTask err", err);
          });
      }
    }

    return SHARED;
  })();


class PDFView {
  constructor(canvasWrapper) {
    this.id = null;
    this.canvas = null;
    this.canvasWrapper = canvasWrapper;
    this.pdfPage = null;
    this.viewport = null;
    this.scale = 1;
    this.outputScale = null;
    this.maxCanvasPixels = MAX_CANVAS_PIXELS;
    this.useOnlyCssZoom = false;
    this.div = document.createElement('div');
    this.div.classList.add('page');
    this.l10n = null;
    this.pageColors = {
        background: '#00FF00',
        foreground: '#FF0000'
    };
  }

  setPdf(pdfPage) {
    this.pdfPage = pdfPage;
    this.viewport = this.pdfPage.getViewport({
      scale: this.scale * PixelsPerInch.PDF_TO_CSS_UNITS,
    });
    this.draw();
  }

  draw() {
    this.cancel();
    const { div, l10n, pageColors, pdfPage, viewport } = this;
    const { width, height } = viewport;
    let canvas = document.createElement("canvas");
    this.div.append(canvas);
    this.canvas = canvas;

    const ctx = canvas.getContext("2d", { alpha: false });

    const outputScale = (this.outputScale = new OutputScale());
    if (this.useOnlyCssZoom) {
      const actualSizeViewport = viewport.clone({
        scale: PixelsPerInch.PDF_TO_CSS_UNITS,
      });
      outputScale.sx *= actualSizeViewport.width / width;
      outputScale.sy *= actualSizeViewport.height / height;
    }
    if (this.maxCanvasPixels > 0) {
      const pixelsInViewport = width * height;
      const maxScale = Math.sqrt(this.maxCanvasPixels / pixelsInViewport);
      if (outputScale.sx > maxScale || outputScale.sy > maxScale) {
        outputScale.sx = maxScale;
        outputScale.sy = maxScale;
        this.hasRestrictedScaling = true;
      } else {
        this.hasRestrictedScaling = false;
      }
    }
    const sfx = this.approximateFraction(outputScale.sx);
    const sfy = this.approximateFraction(outputScale.sy);
    canvas.width = Math.floor( this.roundToDivide(width * outputScale.sx, sfx[0]))
    canvas.height = Math.floor( this.roundToDivide(height * outputScale.sy, sfy[0]));
    const { style } = canvas;
    style.width = this.roundToDivide(width, sfx[1]) + "px";
    style.height = this.roundToDivide(height, sfy[1]) + "px";
    this.div.style.width =  this.roundToDivide(width, sfx[1]) + "px";
    this.div.style.height =  this.roundToDivide(height, sfy[1]) + "px";

    const transform = outputScale.scaled? [outputScale.sx, 0, 0, outputScale.sy, 0, 0]: null;
    const renderContext = {
      canvasContext: ctx,
      transform,
      viewport,
      // annotationMode: this.#annotationMode,
      // optionalContentConfigPromise: this._optionalContentConfigPromise,
      // annotationCanvasMap: this._annotationCanvasMap,
      pageColors,
    };

    const renderTask = (this.renderTask = this.pdfPage.render(renderContext));
    renderTask.promise.then(() => {
        canvas.style.transform = `scale(${1}, ${1})`;
        canvas.style.transformOrigin = "0 0";
        document.querySelector('#recyler-inner-item_'+this.pdfPage._pageIndex).setAttribute('pageTask','c');
    });
  }

  cancel({
    keepAnnotationLayer = false,
    keepAnnotationEditorLayer = false,
    keepXfaLayer = false,
    keepTextLayer = false,
    cancelExtraDelay = 0,
  } = {}) {
    if (this.renderTask) {
      this.renderTask.cancel(cancelExtraDelay);
      this.renderTask = null;
    }
  }

  roundToDivide(x, div) {
    const r = x % div;
    return r === 0 ? x : Math.round(x - r + div);
  }

  approximateFraction(x) {
    // Fast paths for int numbers or their inversions.
    if (Math.floor(x) === x) {
      return [x, 1];
    }
    const xinv = 1 / x;
    const limit = 8;
    if (xinv > limit) {
      return [1, limit];
    } else if (Math.floor(xinv) === xinv) {
      return [1, xinv];
    }
    const x_ = x > 1 ? xinv : x;
    // a/b and c/d are neighbours in Farey sequence.
    let a = 0,
      b = 1,
      c = 1,
      d = 1;
    // Limiting search to order 8.
    while (true) {
      // Generating next term in sequence (order of q).
      const p = a + c,
        q = b + d;
      if (q > limit) {
        break;
      }
      if (x_ <= p / q) {
        c = p;
        d = q;
      } else {
        a = p;
        b = q;
      }
    }
    let result;
    // Select closest of the neighbours to x.
    if (x_ - a / b < c / d - x_) {
      result = x_ === x ? [a, b] : [b, a];
    } else {
      result = x_ === x ? [c, d] : [d, c];
    }
    return result;
  }
}