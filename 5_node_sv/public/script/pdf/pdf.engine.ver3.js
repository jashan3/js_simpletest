CanvasRenderingContext2D.prototype.clear = 
  CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
    if (preserveTransform) {
      this.save();
      this.setTransform(1, 0, 0, 1, 0, 0);
    }

    this.clearRect(0, 0, this.canvas.width, this.canvas.height);

    if (preserveTransform) {
      this.restore();
    }           
};
               // const USE_ONLY_CSS_ZOOM = true;
                // const TEXT_LAYER_MODE = 0; // DISABLE
                // const MAX_IMAGE_SIZE = 1024 * 1024;
                // const CMAP_URL = "../../node_modules/pdfjs-dist/cmaps/";
                // const CMAP_PACKED = true;

                // const DEFAULT_SCALE_DELTA = 1.1;
                // const MIN_SCALE = 0.25;
                // const MAX_SCALE = 10.0;
                // const DEFAULT_SCALE_VALUE = "auto";
                // const self = this;
/**
 * pdf module
 */
 var PdfEngine = PdfEngine || (function(){
    //instance
    var SHARED = {};

    var Util = SHARED.Util = {
        isMobile:function(){
            const userAgent = typeof navigator !== "undefined" && navigator.userAgent || "";
            const platform = typeof navigator !== "undefined" && navigator.platform || "";
            const maxTouchPoints = typeof navigator !== "undefined" && navigator.maxTouchPoints || 1;
            const isAndroid = /Android/.test(userAgent);
            const isIOS = /\b(iPad|iPhone|iPod)(?=;)/.test(userAgent) || platform === "MacIntel" && maxTouchPoints > 1;
            return isAndroid || isIOS
        }
    }
    /*
    * config
    */
    var Config = SHARED.Config = {
        /**
         * 청크방식일 경우 단순 url 호출로 load 시킬수 없다.
         */ 
        useChunked: false,
        /**
         * 워커 경로
         */
        workerSrc: 'https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.13.216/pdf.worker.min.js',
        /*
            ui element 
        */
        element: {
            canvas : document.getElementById('render-container'),
            currentPage: document.getElementById('current_page'),
            totalPage: document.getElementById('total_page'),
            inputRange: document.getElementById('input_range'),
            textLayer: document.getElementById('textlayer'),

            // render layer
            viewerWrapper: document.getElementById('viewer-wrapper'),
            singleLayer: document.getElementById('viewer-page-mode-wrapper'),
            dualLayer: document.getElementById('viewer-page-dual-mode-wrapper'),
            scrolLayer: document.getElementById('viewer-scroll-mode-wrapper'),
            hScrollLayer: document.getElementById('viewer-horizontal-scroll-mode-wrapper'),
        },
        /*
            뷰어상태
        */
        status: {
            /**
             * 0: 첫페이지 부터 양면
             * 1: 그냥 양면
             * 2: 단면
             * 3: 스크롤
             * 4: 가로 스크롤
             */
            mode: 3,

            /**
             * 가로 , 세로 상관없음 유저가 설정한 값을 따라감
             * 0: 첫페이지 부터 양면
             * 1: 그냥 양면
             * 2: 단면
             * 3: 스크롤
             */
            uiMode: 0,

            //pdf object
            pdf: null,

            //현재페이지
            currentPage: 1,

            //전체페이지
            total: 0,

            //현재 pdf 줌 비율
            zoom: 1.0,

            //canvas의 x,y,z 좌표
            // transform: [1,0,0,1,0,0]
            transform: null,

            initRender: false,
        },

        /**
         * 현재 ui 정보
         */
        info: {

            items: [],

            width: 0,

            height: 0 ,

        },
        /*
            뷰어 메타 데이터
        */
        meta: {
            toc: null,
            title: null,
            subject: null,
            author: null,
            creater: null,
        },
        pdfLoadingTask: null,
        pdfDocument: null,
        pdfViewer: null,
        pdfHistory: null,
        pdfLinkService: null,
        eventBus: null,
        url: null,

        init(url , token){
            Config.url = url
            console.log('## PdfEngine init', url)
            var ChunkedParams = null
            if (Config.useChunked){
                ChunkedParams = {
                    "url": url,
                    "cMapPacked": true,
                    "cMapUrl": "../lib/cmaps/",
                    "disableAutoFetch": true,
                    "disableFontFace": false,
                    "disableRange": false,
                    "disableStream": true,
                    "docBaseUrl": "",
                    "enableXfa": true,
                    "fontExtraProperties": false,
                    "isEvalSupported": true,
                    "maxImageSize": -1,
                    "pdfBug": false,
                    "standardFontDataUrl": "../pdf/lib/standard_fonts/",
                    "verbosity": 1,
                    "httpHeaders": { "authorization": token },
                    "withCredentials": true
                }
            } 
            //pdf 워커 위치
            pdfjsLib.GlobalWorkerOptions.workerSrc = Config.workerSrc
            if (ChunkedParams){
                pdfjsLib.getDocument(ChunkedParams).promise.then(Config.onLoadPdf)
            } else {
                Loader.getDocumentTask(Config.url).then(function(pdfDocument){
                    if (!Config.pdfDocument){
                        Config.pdfDocument = pdfDocument
                        Config.status.total = pdfDocument._pdfInfo.numPages
                        console.log('##',Config.status.total)
                    }
                    // Loader.renderingSinglePage(Config.element.singleLayer,Config.status.currentPage)
                    Loader.loadScrollPage()
                })
            }
        },
     
        /**
         * pdf load callback
         * @param {*} pdf 
         */
        onLoadPdf: async function(pdf){
            console.log('## PdfEngine onLoadPdf',pdf)
            Config.status.pdf = pdf
            if (pdf._pdfInfo){
                Config.status.total = pdf._pdfInfo.numPages
            }

            //목차 
            //parse toc
            console.log('### parse toc init',Config.meta.toc)
            let tt = await pdf.getOutline()
            Config.meta.toc = tt
            // await Toc.makeToc(tt , 0)
            // Toc.createTocFragment()
            // Config.element.tocList.appendChild(Toc.tocDocFragment)
            
            Loader.load()

            Config.element.viewerWrapper.addEventListener('scroll',Observer.onVScrollChangeObserver.bind(this))
        },

    }

    var Loader = SHARED.Loader = {

        MAX_CANVAS_PIXEL: Util.isMobile()?5242880:16777216,

        loadScrollPage: function(){
            //
            let div = document.createElement('div')
            div.classList.add('recyler-inner-item')
            for (let i=1; i<Config.status.total; i++){
                let item = div.cloneNode(true)
                item.id = 'recycler-item-'+i
                Config.element.scrolLayer.appendChild(item)
            }

            for (let j =0; j<6; j++){
                let c = Config.element.scrolLayer.children[j]
                Loader.renderingScrollPage(c,j+1)
                // console.log(c)
            }
        },

        /**
         * 
         * @param {*} wrapper recyler-inner-item class를 가진 element
         */
        renderingScrollPage: function(wrapper, page){
            // while (wrapper.firstChild) {
            //     wrapper.removeChild(wrapper.firstChild);
            // }
         
            //
            let canvasWrapper = document.createElement('div')
            canvasWrapper.classList.add('canvasWrapper')

            //
            let canvas = document.createElement('canvas')
            canvas.style.position = 'absolute'
            canvas.style.display = 'none'
            canvasWrapper.appendChild(canvas)
            wrapper.appendChild(canvasWrapper)

            let pageTask = Loader.getPageTask({
                page:page,
                canvas: canvas
            })
            pageTask.then(()=>{
                canvas.style.display = ''
                wrapper.style.background = 'red'
            })
        },

        /**
         * page 모드경우 wrapper로 지정되는것은 바깥 
         * @param {*} wrapper 
         * @param {*} page 
         */
        renderingSinglePage: function(wrapper , page){
            while (wrapper.firstChild) {
                wrapper.removeChild(wrapper.firstChild);
            }

            //
            let div = document.createElement('div')
            div.classList.add('recyler-inner-item')

            //
            let canvasWrapper = document.createElement('div')
            canvasWrapper.classList.add('canvasWrapper')

            //
            let canvas = document.createElement('canvas')
            canvas.style.position = 'absolute'
            canvas.style.display = 'none'
            canvasWrapper.appendChild(canvas)
            div.appendChild(canvasWrapper)

            let pageTask = Loader.getPageTask({
                page:page,
                canvas: canvas
            })
            pageTask.then(()=>{
                canvas.style.display = ''
                wrapper.appendChild(div)
            })
        },
        getDocumentTask:function(url){
            let task = pdfjsLib.getDocument({
                url: url,
                maxImageSize: -1,
                disableAutoFetch: true,
                disableFontFace: false,
                disableRange: false,
                disableStream: true
            })
            return task.promise
        },
        /**
         * canvas drawing을 위한 task
         * @param {*} param0 
         * @returns 
         */
        getPageTask:function({
            page,
            canvas = null,
            transform = null,
        }){
            return Loader.getDocumentTask(Config.url).then(function(pdfDocument){
                return pdfDocument.getPage(page)
            }).then(function(pageTask){
                let zoom = Config.status.zoom
                let ratio = window.devicePixelRatio;
                let mscale = zoom*ratio
                let vp = pageTask.getViewport({scale: mscale})
                let size = window.innerWidth>window.innerHeight?window.innerWidth:window.innerHeight
                let desire_width = Math.floor(size *0.5)
                
                //max pixel
                let currentPx = Math.floor(vp.width * vp.height )
                let needResize = false
                //하락시킬 픽샐 품질
                let downScaleRatio = 1
                //모바일일 경우 최대 픽샐이 넘어가버리면 캔버스가 죽기 때문에 한번더 체크를 해준다.
                if (currentPx >= Loader.MAX_CANVAS_PIXEL){
                    needResize = true
                    downScaleRatio =  Loader.MAX_CANVAS_PIXEL / currentPx
                }

                
                // 원하는 넓이 사이즈를 가저오기위해 재 계산한다.
                let desire_ratio = ratio
                if (desire_width < vp.width){
                    desire_ratio = desire_width / vp.width
                } 

                console.log('downScaleRatio',downScaleRatio)
                console.log('desire_ratio',desire_ratio)
                console.log('desire_ratio*downScaleRatio',desire_ratio*downScaleRatio)
                
                
                let desire_viewport = pageTask.getViewport({scale: desire_ratio*downScaleRatio})

                console.log('orgin',vp)
                console.log('desire',desire_viewport``)
                let desire_height = desire_width * desire_viewport.height / desire_viewport.width
                let renderContext  = null
                if (canvas){
                    let mWidth = null
                    let mHeight = null
                    let resultViewport = null
                    if (needResize){
                         mWidth = Math.floor(desire_viewport.width)
                         mHeight = Math.floor(desire_viewport.height) 
                         resultViewport = desire_viewport
                    } else {
                         mWidth = Math.floor(vp.width)
                         mHeight = Math.floor(vp.height)
                         resultViewport = vp
                    }
                    canvas.width = mWidth
                    canvas.height = mHeight


                    canvas.style.width = desire_width + 'px'
                    canvas.style.height = desire_height + 'px'
                    canvas.parentNode.style.width = desire_width + 'px'
                    canvas.parentNode.style.height = desire_height + 'px'
                    canvas.parentNode.parentNode.style.width = desire_width + 'px'
                    canvas.parentNode.parentNode.style.height = desire_height + 'px'
    
                    renderContext = {
                        canvasContext: canvas.getContext('2d',{ alpha: false }),
                        viewport: resultViewport,
                        transform: transform,
                    }
                    return pageTask.render(renderContext)
                } else {
                    return null
                }
            })
        },


        /**
         * load되어야 하는지 체크
         * @param {*} element 
         * @returns 
         */
        loadElement: function(element){
            if (!element){
                return;
            }
            let attr = element.getAttribute('loaded')
            //already load
            if (attr){
                
            } 
            //need update
            else {
                //some action after update
                element.setAttribute('loaded',1)

            }
        },
        /**
         * deload되어야 하는지 체크
         * @param {*} element 
         * @returns 
         */
        unLoadElement: function(element){
            if (!element){
                return
            }
            
            let attr = element.getAttribute('loaded')
            if (attr){
                element.removeAttribute('loaded')
            } 
            
            let cv = element.querySelector('canvas')
            if (cv){
                let ctx = cv.getContext('2d')
                cv.width = 0
                cv.height = 0
                let img = new Image()
                ctx.drawImage(img,0 ,0)
                ctx.clear()
                cv.parentNode.removeChild(cv);
                cv = null
                cc = null
            }

            element.innerHTML = ''
        },

        /**
        * 
        * @param {*} refNumber 
        * @param {*} innerScrollWrapper 
        */
        pageUpdate: function(refNumber,innerScrollWrapper){
            //찾아올 대상의 그룹
            let findGroupCount = 7
            let needInvalidate = []
            let needClear = []
            let listViewElement = innerScrollWrapper
            let targetQuerySelector = '.'+ViewHolder.itemClassName
            let ulRvItems = null
            try {
                ulRvItems = listViewElement.querySelectorAll(targetQuerySelector)
                ulRvItems.forEach((e,idx)=>{
                    if (refNumber === idx || 
                        refNumber === idx+1 || 
                        refNumber === idx-1 || 
                        refNumber === idx+2 || 
                        refNumber === idx-2 ||
                        refNumber === idx+3 || 
                        refNumber === idx-3 ||
                        refNumber === idx+4 || 
                        refNumber === idx-4
                    ){
                        needInvalidate.push(e)
                    } else {
                        needClear.push(e)
                    }
                })

                //찾은 대상들을 업데이트한다.
                //update
                needInvalidate.forEach((e,idx)=>{
                    Loader.loadElement(e)
                })
                
                //clear
                needClear.forEach((e2,idx)=>{
                    Loader.unLoadElement(e2)
                })
            } catch(e){
                console.error('Error findTarget('+refNumber+')',e)
            } finally{
                listViewElement = null
                findGroupCount = null
                needClear = null
                needInvalidate = null
                targetQuerySelector = null
                ulRvItems = null
            }
        },
    }

    return SHARED
}());


function fullscreen(element){
    if (element.requestFullscreen) 
        element.requestFullscreen();
    else if (element.webkitRequestFullscreen) 
        element.webkitRequestFullscreen();
    else if (element.msRequestFullScreen) 
        element.msRequestFullScreen();
}

function reqFullscreen(){
    // fullscreen(document.querySelector('.viewer-wrapper'))
    fullscreen(document.body)
}

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e.preventDefault();
}

function preventDefaultForScrollKeys(e) {
  if (keys[e.keyCode]) {
    preventDefault(e);
    return false;
  }
}

// modern Chrome requires { passive: false } when adding event
var supportsPassive = false;
try {
  window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
    get: function () { supportsPassive = true; } 
  }));
} catch(e) {}

var wheelOpt = supportsPassive ? { passive: false } : false;
var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

// call this to Disable
function disableScroll() {
  window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
  window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
  window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
  window.addEventListener('keydown', preventDefaultForScrollKeys, false);
}

function activeScroll(){

}

document.documentElement.addEventListener('touchstart', function (event) { if (event.touches.length > 1) { event.preventDefault(); } }, false);

