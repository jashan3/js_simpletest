import * as BaseModule from './base.js';


/**
 * main.html 뷰 클래스
 */
export class MainView extends BaseModule.BaseUIView {
    constructor(config){
        super()
        if (config){
            this.mainlist = config.element.mainlist
        }
        
        this.element = {
            mainlist: document.getElementById('main-list-view')
        }
    }

    reload = (list)=>{  
        // console.log('list', list , this.li())
        let df = document.createDocumentFragment()
        let ul = this.ul
        ul.classList.add('list-view-holder-section')
        list.forEach((e)=>{
            let mli = document.createElement('li')
            let a =  document.createElement('a')
            a.href = '/viewer/'+e.name
            a.textContent = e.name 
            mli.appendChild(a) 
            ul.appendChild(mli)
            
            mli = null
            a = null
        })
        
        df.appendChild(ul)

        this.element.mainlist.innerHTML = ''
        this.element.mainlist.appendChild(df)
        console.log(this)
    }
}

export class MainGridView extends BaseModule.BaseUIView {
    constructor(config){
        super()
        
        this.element = {
            gridview: document.getElementById('grid-view-wrapper')
        }
    }

    reload = function(list){
        let df = document.createDocumentFragment()
        list.forEach(function(e){
            let div = document.createElement('div')
            let ptag = document.createElement('p')
            div.classList.add('grid-item')
            div.style.cursor = 'pointer'
            ptag.textContent = e.name 
            div.addEventListener('click',function(){
                location.href = '/viewer/'+e.name
            })
            div.appendChild(ptag)
            df.appendChild(div)
        })

        this.element.gridview.innerHTML = ''
        this.element.gridview.appendChild(df)
    }
}