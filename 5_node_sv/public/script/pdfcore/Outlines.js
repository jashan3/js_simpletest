/**
 * 
 */
class PdfOutlines {
  getTocs(pdf) {
    return this._getTocs(pdf).then((list) => {
      if (!list) {
        return list;
      }
      return Promise.all(
        list.map((item) =>
          this._parseDestToPage(pdf, item.dest).then((page) => {
            delete item.dest;
            item.page = page;
            return item;
          })
        )
      );
    });
  }
  /**
   * 목차 리스트 생성
   * @param {*} pdf
   * @returns
   */
  _getTocs(pdf) {
    return pdf.getOutline().then((tocs) => {
      console.log("TOCS");
      if (!tocs) {
        return null;
      }
      const outlineList = [];
      for (let index = 0; index < tocs.length; index++) {
        this._parseToc(outlineList, tocs[index]);
      }
      return outlineList;
    });
  }
  /**
   * 목차 파싱
   * @param {*} outlineList
   * @param {*} toc
   * @param {*} depth
   */
  _parseToc(outlineList, toc, depth = 0) {
    if (toc) {
      const {
        bold,
        color,
        count,
        dest,
        italic,
        items,
        newWindow,
        title,
        unsafeUrl,
        url,
      } = toc;
      outlineList.push({ title, depth, dest, bold, italic, color });
      if (items && Array.isArray(items) && items.length > 0) {
        for (let index = 0; index < items.length; index++) {
          this._parseToc(outlineList, items[index], depth + 1);
        }
      }
    }
  }
  /**
   * dest를 page로 파싱
   * @param {*} pdfDocument
   * @param {*} dest
   * @returns
   */
  _parseDestToPage(pdfDocument, dest) {
    if (!dest) {
      return Promise.resolve(null);
    }
    if (typeof dest === "string") {
      return pdfDocument.getDestination(dest);
    } else {
      const [destRef] = dest;
      if (destRef) {
        return pdfDocument.getPageIndex(destRef);
      } else {
        return Promise.resolve(null);
      }
    }
  }
}


module.exports = PdfOutlines
