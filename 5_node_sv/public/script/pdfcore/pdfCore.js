const pdfjs = require("pdfjs-dist");
const Outlines = require("./Outlines");


/**
 * 
 * @param {*} pdfBuffer 
 * @returns 
 */
const getOutlines = (pdfBuffer) => {
  const loadingTask = pdfjs.getDocument({ data: new Uint8Array(pdfBuffer) });
  return loadingTask.promise.then((pdf) => {
    const outlines = new Outlines();
    return outlines.getTocs(pdf).then((result) => {
      return result;
    });
  });
};


module.exports = {
    getOutlines
}