/**
 * zip type
 */
const ZipType = {
    //viewing mode
    ViewMode : Object.freeze({
        //양면
        DUAL:   Symbol(0),
        //첫페이지부터 양면
        DUAL_IN_FIRST:   Symbol(1),
        //단면
        SINGLE:  Symbol(2),
        //스크롤
        SCROLL: Symbol(3)
    }),

    //task type
    Task: Object.freeze({
        //최초상태
        Empty:     Symbol(0),
        //작동중
        Running:   Symbol(1),
        AreadyRunning:   Symbol(2),
        //완료
        Complete:  Symbol(10),
        //에러발생
        Error:     Symbol(20),
        //지연
        Pending:   Symbol(30),
        //지우기요청
        Destroy:    Symbol(40),
    }),
}

/**
 * zip task each
 */
const ZipTask = {
    self: null,
    taskState: ZipType.Task.Empty ,
    index: -1,
    url: null,
    viewMode: null,
    init: function(idx, url, viewMode){
        self = this
        this.index = idx
        this.url = url
        this.viewMode = viewMode
    },
    run: function(){

        if (self.taskState === ZipType.Task.Running ){
            return;
        }
        self.taskState = ZipType.Task.Running 
       
    },
    destory: function(){
        if (self){
            self.taskState = null
            self.index = null
            self.url = null
            self = null
        }
    },
    commit:function(){

    },


    _onWorkerMessage: function(msg){
        //
        self.taskState = ZipType.Task.Complete

    },
    _onWorkerError: function(err){
        //
        self.taskState =  ZipType.Task.Error
 
    },

}


/**
 * zip engine
 * 
 */
const ZipEngine = {
    /**
     * 단면
     * 양면
     * 첫페이지 양면
     * 스크롤 
     */
    zip: {
        //실행 여부
        initailized: false,
        //현재 페이지
        currentPage: 0,
        //현재 뷰모드
        currentViewMode: ZipType.ViewMode.DUAL,
        //현재 확대율
        magnifyRatio: 1,
        //목차 썸네일 리스트
        tocList: null,
        //레이아웃 정보
        layout:{ },
        //앨리먼트
        element:{ },
        taskQueue: [ ]
    },
    init: function(){
        if (this.zip.initailized){
            return;
        }
        this.zip.initailized = true
    },
    setConfiguration: function(){
        
    },
    _setViewMode: function(){
        
    },
    setViewMode: function(viewMode){
        switch(viewMode){

        }
    },
    excuteTaskQueue: function(){
        if (!this.zip.taskQueue){
            return
        }
        if (this.zip.taskQueue.length == 0){
            return
        }

    },

    validate: function(){
      
    }

}

