

import * as View from './mainView.js';
import * as Api from './api.js';

window.onload = async function(){

    let list  = await Api.API.fetchFileList()    
    //main view
    const mainView = new View.MainView()
    //ui update
    mainView.reload(list)

    // const mainGridView = new View.MainGridView()

    // mainGridView.reload(list)
}


