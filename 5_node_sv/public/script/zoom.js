




/**
	 * zoom class
	 * @param {*} el 
	 * @param {*} options 
	 */
 var BDBZoom = function (el, zoomTargetID) {
    this.el = el;
    this.zoomTargetID = zoomTargetID
    
    this.zoomFactor = 1;
    this.lastScale = 1
    this.maxZoom = 3
    this.minZoom = 0.5
    this.offset = {
        x: 0,
        y: 0
    };
    this.inAnimation = false

    this.enable()
    this.bindEvents()
}
BDBZoom.prototype = {
    bindEvents: function(){
        detectGestures(this.el,this)
    },
    /**
     * DRAG
     * @param {*} event 
     */
    handleDragStart: function (event) {
        console.log('[ViewerEvent][handleDragStart]',event)
    },
    handleDrag: function (event) {
        console.log('[ViewerEvent][handleDrag]',event)
    },
    handleDragEnd: function () {
      console.log('[ViewerEvent][handleDragEnd]',event)
    },
    /**
     * Zoom
     * @param {*} event 
     */
    _zoomCache: null,
    lastPosition: -1,
    scheduledAnimationFrame: false,
    handleZoomStart: function(event){
        this.lastPosition = this.el.scrollTop
        Event.scrollLock = true
        this.lastScale = 1;
    },
    handleZoomEnd: function(event){

    },
    handleZoom: function(event, newScale){
        // var touchCenter = this.getTouchCenter(this.getTouches(event))
        this.lastScale = newScale;
        let scale = newScale / this.lastScale;
        this.lastScale = newScale;
        console.log(scale/10)
        // let self = this
        // // let zoomTarget = document.getElementById(self.zoomTargetID)
        // self.scaleZoomFactor(scale)

        // console.log('this',this.zoomFactor)
        // this.inAnimation = false
        // var startZoomFactor = this.zoomFactor,
        //     originZoomFactor = 1
        // let updateProgress = (function(progress){
        //     let nSC = startZoomFactor + progress * (originZoomFactor - startZoomFactor)

        
            
          
        // }).bind(this)

        // this.animate(300,updateProgress,self.swing(),function(){
        //     console.log('## end')
        // })
    },
    /**
     * Doouble Tab
     * @param {*} event 
     */
    handleDoubleTap: function(event){
        console.log('[ViewerEvent][handleDoubleTap]',event)
        this.handleZoom(event,1)
    },
    update: function(event){
        console.log('[ViewerEvent][update]',event)
    },

    setStyle: function(element ,zoomFactor,offsetX,offsetY){
        var transform3d = 'scale3d('     + zoomFactor + ', '  + zoomFactor + ',1) ' +'translate3d(' + offsetX    + 'px,' + offsetY    + 'px,0px)'
        var transform2d = 'scale('       + zoomFactor + ', '  + zoomFactor + ') ' +'translate('   + offsetX    + 'px,' + offsetY    + 'px)'
        
        element.style.webkitTransform = transform3d;
        element.style.mozTransform = transform2d;
        element.style.msTransform = transform2d;
        element.style.oTransform = transform2d;
        element.style.transform = transform3d;
    },
    /**
     * Scale to a specific zoom factor (not relative)
     * @param zoomFactor
     * @param center
     */
    scaleTo: function (zoomFactor, center) {
        this.scale(zoomFactor / this.zoomFactor, center);
    },
    scale: function (scale, center) {
        scale = this.scaleZoomFactor(scale);
        this.addOffset({
            x: (scale - 1) * (center.x + this.offset.x),
            y: (scale - 1) * (center.y + this.offset.y)
        });
        // triggerEvent(this.el, this.options.zoomUpdateEventName);
        // if(typeof this.options.onZoomUpdate == "function"){
        // 	this.options.onZoomUpdate(this, event)
        // }
    },
    scaleZoomFactor: function (scale) {
        var originalZoomFactor = this.zoomFactor;
        this.zoomFactor *= scale;
        this.zoomFactor = Math.min(this.maxZoom, Math.max(this.zoomFactor, this.minZoom));
        return this.zoomFactor / originalZoomFactor;
    },
    addOffset: function (offset) {
        this.offset = {
            x: this.offset.x + offset.x,
            y: this.offset.y + offset.y
        };
    },
    enable: function() {
        this.enabled = true;
    },
    disable: function() {
        this.enabled = false;
    },
    canDrag: function(){
        return !this.isCloseTo(this.zoomFactor, 1);
    },
    isCloseTo: function (value, expected) {
        return value > expected - 0.01 && value < expected + 0.01;
    }
            /**
     * Calculates the touch center of multiple touches
     * @param touches
     * @return {Object}
     */
    ,getTouchCenter: function (touches) {
        return this.getVectorAvg(touches);
    },

    /**
     * Calculates the average of multiple vectors (x, y values)
     */
    getVectorAvg: function (vectors) {
        return {
            x: vectors.map(function (v) { return v.x; }).reduce(sum) / vectors.length,
            y: vectors.map(function (v) { return v.y; }).reduce(sum) / vectors.length
        };
    },
    swing(p) {
        return -Math.cos(p * Math.PI) / 2  + 0.5;
    },
    animate(duration, framefn, timefn, callback){
        let self = this
        var startTime = new Date().getTime(),
            renderFrame = (function () {
                if (!self.inAnimation) { return; }
                var frameTime = new Date().getTime() - startTime,
                    progress = frameTime / duration;
                if (frameTime >= duration) {
                    framefn(1);
                    if (callback) {
                        callback();
                    }
                    // resizeContainer()
                    self.inAnimation = false;
                    // resizeContainer()
                } else {
                    if (timefn) {
                        progress = timefn(progress);
                    }
                    framefn(progress);
                    // resizeContainer()
                    requestAnimationFrame(renderFrame);
                }
            }).bind(this);
            self.inAnimation = true;
        requestAnimationFrame(renderFrame);
    }
}






var detectGestures = function (el, target) {
    var interaction = null,
        fingers = 0,
        lastTouchStart = null,
        startTouches = null,

        setInteraction = function (newInteraction, event) {
            if (interaction !== newInteraction) {

                if (interaction && !newInteraction) {
                    switch (interaction) {
                        case "zoom":
                            target.handleZoomEnd(event);
                            break;
                        case 'drag':
                            target.handleDragEnd(event);
                            break;
                    }
                }

                switch (newInteraction) {
                    case 'zoom':
                        target.handleZoomStart(event);
                        break;
                    case 'drag':
                        target.handleDragStart(event);
                        break;
                }
            }
            interaction = newInteraction;
        },

        updateInteraction = function (event) {
            if (fingers === 2) {
                setInteraction('zoom');
            } else if (fingers === 1 && target.canDrag()) {
                setInteraction('drag', event);
            } else {
                setInteraction(null, event);
            }
        },

        targetTouches = function (touches) {
            return Array.from(touches).map(function (touch) {
                return {
                    x: touch.pageX,
                    y: touch.pageY
                };
            });
        },

        getDistance = function (a, b) {
            var x, y;
            x = a.x - b.x;
            y = a.y - b.y;
            return Math.sqrt(x * x + y * y);
        },

        calculateScale = function (startTouches, endTouches) {
            var startDistance = getDistance(startTouches[0], startTouches[1]),
                endDistance = getDistance(endTouches[0], endTouches[1]);
            return endDistance / startDistance;
        },

        cancelEvent = function (event) {
            event.stopPropagation();
            event.preventDefault();
        },

        detectDoubleTap = function (event) {
            var time = (new Date()).getTime();

            if (fingers > 1) {
                lastTouchStart = null;
            }

            if (time - lastTouchStart < 300) {
                cancelEvent(event);

                target.handleDoubleTap(event);
                switch (interaction) {
                    case "zoom":
                        target.handleZoomEnd(event);
                        break;
                    case 'drag':
                        target.handleDragEnd(event);
                        break;
                }
            } else {
                target.isDoubleTap = false;
            }

            if (fingers === 1) {
                lastTouchStart = time;
            }
        },
        firstMove = true;

    el.addEventListener('touchstart', function (event) {
        if(target.enabled) {
            firstMove = true;
            fingers = event.touches.length;
            detectDoubleTap(event);
        }
    });

    el.addEventListener('touchmove', function (event) {
        if(target.enabled && !target.isDoubleTap) {
            if (firstMove) {
                updateInteraction(event);
                if (interaction) {
                    cancelEvent(event);
                }
                startTouches = targetTouches(event.touches);
            } else {
                switch (interaction) {
                    case 'zoom':
                        if (startTouches.length == 2 && event.touches.length == 2) {
                            target.handleZoom(event, calculateScale(startTouches, targetTouches(event.touches)));
                        }
                        break;
                    case 'drag':
                        target.handleDrag(event);
                        break;
                }
                if (interaction) {
                    cancelEvent(event);
                    target.update();
                }
            }

            firstMove = false;
        }
    });

    el.addEventListener('touchend', function (event) {
        if(target.enabled) {
            fingers = event.touches.length;
            updateInteraction(event);
        }
    });
};