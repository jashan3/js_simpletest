const { app, BrowserWindow } = require('electron')

const createWindow = () => {
    const win = new BrowserWindow({
      width: 800,
      height: 600
    })
  
    win.loadURL('http://10.200.10.21:3000')
}


app.whenReady().then(() => {
    console.log('## whenReady')
    createWindow()
})