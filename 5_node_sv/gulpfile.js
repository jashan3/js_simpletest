const { src, dest, series , parallel ,task} = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const clean = require('gulp-clean');


const SRC = {
    FROM: 'public/',
    TO: 'output/',
}

/**
 * clear all
 * @param {*} cb 
 * @returns 
 */
function cleanTask(cb){
    return src(SRC.TO+'**')
    .pipe(clean({force: true}))
}

/**
 * js task 
 * @returns 
 */
function scriptTask(){
    return src(SRC.FROM+'script/**')
    .pipe(uglify())
    .pipe(dest(SRC.TO+'script'))
}
/**
 * css task
 * @returns 
 */
function cssTask(){
    return src(SRC.FROM+'css/**')
    .pipe(dest(SRC.TO+'css'))
}

/**
 * static task
 * image,media file ...
 * @returns 
 */
function staticTask(){
    return src(SRC.FROM+'static/**')
    .pipe(dest(SRC.TO+'static'))
}

// exports.build = parallel(scriptTask)
exports.default = series(cleanTask, parallel(cssTask,staticTask,scriptTask))