
let toggle = false

window.addEventListener('DOMContentLoaded', function(){
    const dropdown = document.getElementById('dropdown')
    const ul = dropdown.querySelector('ul')
    const li = dropdown.querySelector('li')
    const lirect = li.getBoundingClientRect();
    ul.style.height = `${lirect.height * 4}px;`
    dropdown.querySelectorAll('ul li').forEach(li=>{
        li.addEventListener('click',function(e){
            e.stopPropagation();
        })
    })
    document.body.addEventListener('click' ,function(){
        if (toggle){
            dropdown.classList.add('active');
        } else {
            dropdown.classList.remove('active');
        }
        toggle = !toggle
    })
})
