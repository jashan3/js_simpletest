window.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');

    
    Builder.Build({midOutter:'mid-outter',topContainer:'top-container'})



});




const Settings = (function(){
    const Setting = {}

    const Keys = {
        TOP_NAV_HEIGHT: '--top-nav-height',
        BTM_NAV_HEIGHT: '--bottom-nav-height',
        BDB_BTM_OFFSET:  '--bdb-bottom-offset',
        BDB_TOP_OFFSET: '--bdb-top-offset',

        VIEWER_FONT_SIZE: '--viewer-font-size',
        VIEWER_INDENT: '--viewer-indent',
        VIEWER_LINE_HEIGHT: '--viewer-line-height',
        VIEWER_TOP_MARGIN: '--viewer-top-margin',
        VIEWER_SIDE_MARGIN: '--viewer-side-margin',
    }

    function _set(attr,value){
        document.documentElement.style.setProperty(attr,value);
    }
    function _get(attr){
        document.documentElement.style.getPropertyValue(attr);
    }

    /**
     * UNIT
     */
    const UNIT_PX = 'px';
    const UNIT_EM = 'em';
    const UNIT_PERCENT = '%';


    /**
     * DEFAULT VALUE
     */

    const defaultFontSize = 1.5;
    const defaultIndent = 0;
    const defaultLineHeight = 150;
    const defaultTopMargin = 10;
    const defaultSideMargin = 10;

    /**
     * VALUE
     */
    let fontSize = defaultFontSize;
    let indent = defaultIndent;
    let lineHeight = defaultLineHeight;
    let topMargin = defaultTopMargin;
    let sideMargin = defaultSideMargin;


    /**
     * STEP
     */
    let topMarginStep = 5;
    let sideMarginStep = 5;

    /**
     * FUNC
     */
     Setting.setFontSize = function(value){
        if (typeof value ==='number'){
            fontSize = value
        } else if (typeof value === 'string'){
            fontSize = parseInt(value);
        }
        if (fontSize < 0){
            fontSize = 0;
        }
        _set(Keys.VIEWER_FONT_SIZE, fontSize + UNIT_EM);
    }
    Setting.getFontSize = function(){
        return fontSize
    }
    Setting.resetFontSize = function(){
        this.setFontSize(defaultFontSize)
    }

    Setting.setIndent = function(value){
        if (typeof value ==='number'){
            indent = value
        } else if (typeof value === 'string'){
            indent = parseInt(value);
        }
        if (indent < 0){
            indent = 0;
        }
        _set(Keys.VIEWER_INDENT, indent + UNIT_PERCENT);
    }
    Setting.getIndent = function(){
        return indent
    }
    Setting.resetIndent = function(){
        this.setIndent(defaultIndent)
    }

    Setting.setLineHeight = function(value){
        if (typeof value ==='number'){
            lineHeight = value
        } else if (typeof value === 'string'){
            lineHeight = parseInt(value);
        }
        if (lineHeight < 0){
            lineHeight = 0;
        }
        _set(Keys.VIEWER_LINE_HEIGHT, lineHeight + UNIT_PERCENT);
    }
    Setting.getLineHeight = function(){
        return lineHeight
    }
    Setting.resetLineHeight = function(){
        this.setLineHeight(defaultLineHeight)
    }
    
    Setting.setTopMargin = function(value){
        if (typeof value ==='number'){
            topMargin = value
        } else if (typeof value === 'string'){
            topMargin = parseInt(value);
        }
        if (topMargin < 0){
            topMargin = 0;
        }
        _set(Keys.VIEWER_TOP_MARGIN, topMargin + UNIT_PX);
    }
    Setting.getTopMargin = function(){
        return topMargin
    }
    Setting.resetTopMargin = function(){
        this.setTopMargin(defaultTopMargin)
    }


    Setting.setSideMargin = function(value){
        if (typeof value ==='number'){
            sideMargin = value
        } else if (typeof value === 'string'){
            sideMargin = parseInt(value);
        }
        if (sideMargin < 0){
            sideMargin = 0;
        }
        _set(Keys.VIEWER_SIDE_MARGIN, sideMargin + UNIT_PX);
    }
    Setting.getSideMargin = function(){
        return sideMargin
    }
    Setting.resetSideMargin = function(){
        this.setSideMargin(defaultSideMargin)
    }



    Setting.reset = function(){
        this.resetFontSize();
        this.resetIndent();
        this.resetLineHeight();
        this.resetTopMargin();
        this.resetSideMargin();
    }


    return Setting;
})();


const Builder = (function(Settings){
    const Engine = {}

    const Ele = {
        topContainer: null,
        midOutter: null,
        viewer: null,
    }

    Engine.Build = function(config){
        Ele.viewer = Build();
        Ele.midOutter = document.getElementById(config.midOutter);
        Ele.topContainer = document.getElementById(config.topContainer);

        Ele.topContainer.appendChild(BuildRangeInput(0,5.0,0.1,Settings.getFontSize(),'fontSize','글자크기'));
        Ele.topContainer.appendChild(BuildRangeInput(0,30,1,Settings.getIndent(),'indent','들여쓰기'));
        Ele.topContainer.appendChild(BuildRangeInput(0,300,10,Settings.getLineHeight(),'lineHeight','문단 간격'));
        Ele.topContainer.appendChild(BuildRangeInput(0,100,1,Settings.getTopMargin(),'topMargin','상하단 여백'));
        Ele.topContainer.appendChild(BuildRangeInput(0,100,1,Settings.getSideMargin(),'sideMargin','좌우 여백'));

        const rect = Ele.midOutter.getBoundingClientRect();
        Ele.viewer.style.width = rect.width + 'px';
        Ele.viewer.style.height = rect.height + 'px';
        Ele.midOutter.appendChild(Ele.viewer);

        Settings.reset();
    }

    function Build(){
        const div = document.createElement('div');
        div.style.position = 'absolute';

        return div
    }

    function BuildRangeInput(min,max,step,cur,key,labelText){
        const div = document.createElement('div');
        const label = document.createElement('label');
        const input = document.createElement('input');

        label.textContent = labelText;

        input.min = min;
        input.max = max;
        input.value = cur;
        input.step = step;
        input.type = 'range';
        input.onchange = onChanged;
        input.oninput = onChanged;
        input.setAttribute('setting-key',key);

        div.appendChild(label);
        div.appendChild(input);
        return div
    }

    function onChanged(e){
        let attr = e.target.getAttribute('setting-key');
        let val = e.target.value;
        if (attr === 'topMargin'){
            Settings.setTopMargin(val)
        } else if (attr === 'sideMargin'){
            Settings.setSideMargin(val)
        } else if (attr === 'fontSize'){
            Settings.setFontSize(val)
        } else if (attr === 'indent'){
            Settings.setIndent(val)
        } else if (attr === 'lineHeight'){
            Settings.setLineHeight(val)
        }
    }



    

    return Engine
})(Settings);