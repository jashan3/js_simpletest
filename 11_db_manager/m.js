



class SearchStorage {

    constructor( callback ){
        this.searchSaveKey = 'yes24#search://'
        this.list = []
        this.saveCallback = callback
    }

    /**
     * 검색 키워드를 받으면 저장시키거나 삭제시킨다.
     * @param {*} keyword 
     */
    save(keyword){
        let key = this._getKey()
        let loadValue = this.load()
        
        //없는 경우 새로 넣음
        if (!loadValue){
            loadValue = new Map()
        } 
        
        loadValue.set(keyword,{
            'date': new Date(),
            'keyword': keyword
        })

        let v = JSON.stringify(Object.fromEntries(loadValue))
        let value = encodeURIComponent(v);
        window.localStorage.setItem(key , value);

        if (this.saveCallback){
            this.saveCallback('save', this.load())
        }


        key = null
        loadValue = null
        v = null
        value = null
    }

    /**
     * 현재 저장된 검색 데이터
     * @param {*} keyword 
     */
    load(){
		let matches = this._get()
        if (matches){
            return new Map(Object.entries(JSON.parse(decodeURIComponent(matches))));
        } else {
            return null
        }
    }

    /**
     * 검색 키워드를 받으면 삭제시킨다.
     * @param {*} keyword 
     */
    delete(keyword){
        let key = this._getKey()
        let loadValue = this.load()
        if (loadValue){
            if (loadValue.has(keyword)){
                loadValue.delete(keyword)
                let v = JSON.stringify(Object.fromEntries(loadValue))
                let value = encodeURIComponent(v);
                window.localStorage.setItem(key , value);
                if (this.saveCallback){
                    this.saveCallback('delete', this.load())
                }
            }
        }
    }

    /**
     * 검색 키워드관련 데이터를 초기화
     * @param {*} keyword 
     */
    deleteAll(){
        let key = this._getKey()
        let loadValue = this.load()
        if (loadValue){
           window.localStorage.clear(key)
            if (this.saveCallback){
                this.saveCallback('deleteAll', this.load())
            }
        }
    }
    _get(){
        return window.localStorage.getItem(this.searchSaveKey);
    }
    _getKey(){
        return this.searchSaveKey
    }
}