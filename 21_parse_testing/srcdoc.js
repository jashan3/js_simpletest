
let data = `<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<link href="https://wviewer.ndev.kyobobook.co.kr/content/web_ebook/web_epub/970/480D181135970/480D181135970/styles/3e61e104-f601-4686-abfd-4fc8e319bbca.css" type="text/css" rel="stylesheet"></link>
<meta name="viewport" content="width=720, height=1098"></meta>
</head>
<body>
<p class="txt"><br/></p>
 <r1l class='bdb' style='display:none;'>슟</r1l>   <h1 class="txtt" id="c01h01"><span style="color: rgb(0, 0, 128);">이사</span></h1>
<i7tc93l class='bdb' style='display:none;'>흘듏캄뮭쬂</i7tc93l> <ckbd class='bdb' style='display:none;'>쁧낃</ckbd>   <p class="txts"><br/></p>
<drr80pvx class='bdb' style='display:none;'>엔늾</drr80pvx> <p class="txts"><br/></p>
 <p class="txts"><br/></p>
<p class="txt" id="c01p0001">마음이<c7it class='bdb' style='display:none;'>뭓쐴떙촢</c7it> 떠나니</p>
<p class="txt" id="c01p0002">정든 땅도<b113ko class='bdb' style='display:none;'>촅</b113ko> 낯설다</p>
<p class="txt" id="c01p0003">보금자리이던 집도 서먹한 시간</p>
<p class="txt" id="c01p0004">등<q5k class='bdb' style='display:none;'>쭟뾶어콧톫</q5k> 돌린<jpa6i class='bdb' style='display:none;'>쨷</jpa6i> 곳에</p>
<p class="txt" id="c01p0005">겨울바람이 차갑게 문을 닫는다</p>
<p class="txt"><br/></p>
<p class="txt" id="c01p0006">높은 아파트 창으로</p>
<p class="txt" id="c01p0007">한 점 두 점 짐이 빠져나가고</p>
<p class="txt" id="c01p0008">아련히<ydx class='bdb' style='display:none;'>냷쭜퓑</ydx> 추억만 텅 빈<raoev41i class='bdb' style='display:none;'>쩂턻뎙륟</raoev41i> 공간을 채운다</p>
<p class="txt"><br/></p>
<p class="txt" id="c01p0009">뭐라 말하고<tojhxdm class='bdb' style='display:none;'>폁썱</tojhxdm> 싶은데</p>
<p class="txt" id="c01p0010">그 무슨<rgbre class='bdb' style='display:none;'>뇗쿏뭤젆첄</rgbre> 몸짓이라도 나눴으면 하는데</p>
<p class="txt" id="c01p0011">벌써 이별이 눈치를<ng0t class='bdb' style='display:none;'>콒캌땡몟</ng0t> 챈다</p>
</body>
</html> `
// data.replaceAll('<',"")

window.addEventListener('DOMContentLoaded', (event) => {
    const iframe = document.getElementById('iframe')

    iframe.srcdoc = data
    iframe.addEventListener('load',function(){
        console.log('load')
    })
    var $iframe = $(iframe);
    onIframeReady($iframe, function($contents) {
        console.log("Ready to got");
        console.log($contents.find("*"));
    }, function() {
        console.log("Can not access iframe");
    });
});







    /**
     * Will wait for an iframe to be ready
     * for DOM manipulation. Just listening for
     * the load event will only work if the iframe
     * is not already loaded. If so, it is necessary
     * to observe the readyState. The issue here is
     * that Chrome will initialize iframes with
     * "about:blank" and set its readyState to complete.
     * So it is furthermore necessary to check if it's
     * the readyState of the target document property.
     * Errors that may occur when trying to access the iframe
     * (Same-Origin-Policy) will be catched and the error
     * function will be called.
     * @param  $i - The jQuery iframe element
     * @param  successFn - The callback on success. Will 
     * receive the jQuery contents of the iframe as a parameter
     * @param  errorFn - The callback on error
     */
    var onIframeReady = function($i, successFn, errorFn) {
        try {
            const iCon = $i.first()[0].contentWindow,
                bl = "about:blank",
                compl = "complete";
            const callCallback = () => {
                try {
                    const $con = $i.contents();
                    if($con.length === 0) { // https://git.io/vV8yU
                        throw new Error("iframe inaccessible");
                    }
                    successFn($con);
                } catch(e) { // accessing contents failed
                    errorFn();
                }
            };
            const observeOnload = () => {
                $i.on("load.jqueryMark", () => {
                    try {
                        const src = $i.attr("src").trim(),
                            href = iCon.location.href;
                        if(href !== bl || src === bl || src === "") {
                            $i.off("load.jqueryMark");
                            callCallback();
                        }
                    } catch(e) {
                        errorFn();
                    }
                });
            };
            if(iCon.document.readyState === compl) {
                const src = $i.attr("src").trim(),
                    href = iCon.location.href;
                if(href === bl && src !== bl && src !== "") {
                    observeOnload();
                } else {
                    callCallback();
                }
            } else {
                observeOnload();
            }
        } catch(e) { // accessing contentWindow failed
            errorFn();
        }
    };

    // var $iframe = $("iframe");
    // onIframeReady($iframe, function($contents) {
    //     console.log("Ready to got");
    //     console.log($contents.find("*"));
    // }, function() {
    //     console.log("Can not access iframe");
    // });

let d = ".chapter .box4 { border-radius: 1em; background-color: rgb(117, 117, 117);  padding: 0.5em; }"


function getCssAttr(attr = "background"){
    let back_index = d.indexOf(attr)
    if (back_index === -1){
        return null
    }
    let back_str = d.substring(back_index)
    let back_colon = back_str.indexOf(':')
    if (back_colon === -1){
        return null
    }
    let colon_str = back_str.substring(back_colon+1)
    let attrKey = back_str.substring(0,back_colon)
    let back_end = colon_str.indexOf(';')
    if (back_colon === -1){
        return null
    }
    let backgroundAttr = colon_str.substring(-1,back_end)
    if (backgroundAttr.length > 0){
        backgroundAttr = backgroundAttr.trim()
    }
    return {key:attrKey,value:backgroundAttr} 
}

console.log(getCssAttr())