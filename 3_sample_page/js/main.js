/**
 * application instance
 */



/**
 * 메인에서 사용될 아이템들
 * @returns 
 */
async function getMainData(){
    let req = API.request;
    let result = await req.category();
    // console.log(result);
    let array = []
    for (const item of result) {
        const contents = await req.posts(item.pk);
        contents.ctgrNm = item.ctgrNm
        array.push(contents)
    }
    return await array
}



function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();
    return div.firstChild; 
}


/**
 * app 구성요소
 */
var App = App || (function(){
   const Share = {};

   const element = {
       main: null
   }

   var bind = Share.bind = {

   };

   var Loader = Share.Loader = {
       index: function(){
            element.main = document.querySelector('#main-section');
            getMainData().then(result=>{
                console.log(result)
                var docFragment = document.createDocumentFragment();
                result.forEach((item)=>{
                    let ele = App.viewHolder.getMainList(item)
                    docFragment.appendChild(ele)
                });
                element.main.innerHTML = ''
                element.main.appendChild(docFragment)
            });
       }
   }

   var viewHolder = Share.viewHolder = {
        main:null,
        getMainList:function(item){
            if (!this.main){
                let item = `
                <div class="listViewHolder">
                    <label class="listViewHolderTitle"> sub title1</label> 
                    <div class="wrapper">
                        <amp-live-list
                            id="live-list-1"
                            data-poll-interval="20000"
                            data-max-items-per-page="4">
                            <button update id="fixed-button" class="button" on="tap:live-list-1.update">
                                new updates on live list 1
                                </button>
                                <div items class="liveListWrap"></div>
                        </amp-live-list>
                    </div>
                </div>`
                this.main = createElementFromHTML(item);
            } 

            let tempMain = this.main.cloneNode(true)
            tempMain.querySelector('.listViewHolderTitle').textContent = item.ctgrNm
            if (item.content){
                let holder = tempMain.querySelector('.liveListWrap')
                item.content.forEach((e)=>{
                    let tempEle = viewHolder.getMainListItem(e);
                    holder.appendChild(tempEle);
                })
            }
            return tempMain
        },
        mainListItem:null,
        getMainListItem: function(data){
            if (!this.mainListItem){
                let temp = `
                <div class="liveListItem">
                    <div>
                        <amp-img 
                            width="1" 
                            height="1"
                            layout="responsive">
                    </div>
                   
                    <div>
                        <label for="" class="liveListItemTitle">asdasdasd</label> 
                    </div>
                </div>
                `
                this.mainListItem = createElementFromHTML(temp);
            } 
            let tempItem = this.mainListItem.cloneNode(true)
            if (data.postTitle){
                tempItem.querySelector('.liveListItemTitle').textContent = data.postTitle
            } else {
                tempItem.querySelector('.liveListItemTitle').textContent = ''
            }
            if (data.postUrl){
                tempItem.addEventListener('click',function(){
                    location.href = data.postUrl;
                });
            }

            return tempItem
        },
        mainAdapter: function(){
            
        }
   };

   return Share;
}());

/**
 * api 부분
 */
var API = API || (function(){
    const Share = {};
    
    const domain = "http://www.semochuree.com:11111"

    const header = {
            headers: {
                'Content-Type': 'application/json'
            }
        }


    /**
     * 통신 객체
     */
    var request = Share.request = {

        DOMAIN: "https://www.semochuree.com:11111",
        HEADER: {
            headers: {
                'Content-Type': 'application/json'
            }
        },
        /**
         * 카테고리를 불러온다.
         * @returns 
         */
        category: async function(){
            let data = await fetch(this.DOMAIN+"/api/ctgrs",this.HEADER).then(r=>r.json())
            var mData = data.content
            mData.forEach(element => {
                let tempKey = element.links[0].href
                let seperator = API.request.DOMAIN+"/api/ctgrs/"
                let key = tempKey.replace(seperator, "")
                if (key){
                    element.pk = key
                }
            });
            return await mData
        },

        /**
         * 게시물을 불러온다.
         * @param {*} key 게시물의 pk
         * @returns 
         */
        contents: async function(key){
            return await fetch(this.DOMAIN+"/api/contents/search/postNo?postNo="+key,this.HEADER).then(r=>r.json())
        },

        /**
         * 최신 게시물
         * @param {*} key 게시물의 pk
         * @returns 
         */
        recently: async function(){
            var mSize = 4
            return await fetch(this.DOMAIN+"/api/posts?sort=postNo,desc&size="+mSize,this.HEADER).then(r=>r.json())
        },

        /**
         * 게시물
         * @param {*} category 카테고리 구분자
         * @param {*} size 가저올 갯수 
         * @param {*} sort 정렬 조건
         * @returns 
         */
        posts: async function(category,size,sort){
            var mSize = 4
            var mSort = "desc"
            if (size && size>0){
                mSize = size
            }

            if (sort){
                mSort = sort
            }

            return await fetch( this.DOMAIN+"/api/posts/search/ctgrNo?ctgrNo="+category +"&size="+mSize+"&sort=postNo,"+mSort,this.HEADER).then(r=>r.json())
        },
    };
    return Share;
 }());

