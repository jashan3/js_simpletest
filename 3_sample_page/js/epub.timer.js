


/**
 * 뷰어 타이머 컨트롤을 위한 manager
 */
function ViewerTimerManager(callback){
    this.originTime = 10000
    this.decreaseTime = 1000;
    this.remainTime = this.originTime;
    this.callback = callback
    this.interval = null
}

ViewerTimerManager.prototype = {

    start: function(){
        if (this.callback){
            this.callback('start',this.remainTime);
        }
        if (this.interval){
            this.pause();
        }
        this.interval = setInterval(this.tictoc.bind(this), 1000);
    },

    pause: function(){
        if (this.interval){
            clearInterval(this.interval)
            this.interval = null;
        }
        this.remainTime = this.originTime
        if (this.callback){
            this.callback('pause',this.remainTime)
        }
    },

    tictoc: function(){
        this.remainTime = this.remainTime - this.decreaseTime
        if (this.remainTime <= 0){
            this.pause();
            if (this.callback){
                this.callback('end',this.remainTime)
            }
            return;
        }
        if (this.callback){
            this.callback('tictoc',this.remainTime)
        }
    }
}