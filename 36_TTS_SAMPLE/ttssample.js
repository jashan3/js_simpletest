/********************************************
 * Element build Class
 *******************************************/
const EpubBuilder = (function () {
    const annotationClassName = "bdb-annotation-draw-layer";
    const ttsClassName = "bdb-tts-draw-layer";
    const selectionClassName = "bdb-selection-draw-layer";
    const searchClassName = "bdb-search-highlight-draw-layer";
    const drawingClassName = "bdb-pallet-draw-layer";
    function EpubBuilder({ } = {}) {

    }
    /**
     * @getter
     */
    EpubBuilder.prototype.annotationClassName = (function () {
        return annotationClassName
    })();
    EpubBuilder.prototype.ttsClassName = (function () {
        return ttsClassName
    })();
    EpubBuilder.prototype.selectionClassName = (function () {
        return selectionClassName
    })();
    EpubBuilder.prototype.searchClassName = (function () {
        return searchClassName
    })();
    EpubBuilder.prototype.annotationClassName = (function () {
        return drawingClassName
    })();

    /**
     * @public
     * @returns Element
     */
    EpubBuilder.prototype.CreateUnionLayer = function () {
        const unionLayer = document.createDocumentFragment();
        unionLayer.appendChild(CreateAnnotationLayer());
        unionLayer.appendChild(CreateSelectionLayer());
        unionLayer.appendChild(CreateTTSLayer());
        unionLayer.appendChild(CreateSearchHighlightLayer());
        unionLayer.appendChild(CreateDrawingLayer());
        return unionLayer
    }

    /**
     * Highligh div block
     * @param {*} x, y, w, h, color, opacity 
     * @returns 
     */
    EpubBuilder.prototype.CreateRectDiv = function ({
        x = 0,
        y = 0,
        w = 0,
        h = 0,
        color = "red",
        opacity = 0.5
    } = {}) {
        const div = CreateDefaultLayer();
        div.style.backgroundColor = color
        div.style.opacity = opacity
        div.style.width = w + "px";
        div.style.height = h + "px";
        div.style.left = x + "px";
        div.style.top = y + "px";
        return div
    }
    /**
     * Highligh svg block
     * @param {*} x, y, w, h, color, opacity 
     * @returns 
     */
    EpubBuilder.prototype.CreatSVGDrawingLayer = function (rects,options) {
        return CreatSVGDrawingLayer(rects,options);
    }
    

    /**
     * @private
     * @returns Default Element Layer 
     */
    const CreateDefaultLayer = function () {
        const div = document.createElement('div');
        div.style.position = 'absolute'
        return div;
    }
    const CreateAnnotationLayer = function () {
        let div = CreateDefaultLayer();
        div.classList.add(annotationClassName);
        return div;
    }
    const CreateTTSLayer = function () {
        let div = CreateDefaultLayer();
        div.classList.add(ttsClassName);
        div.style['transition'] = 'opacity 251ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
        div.style['transform'] = '167ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
        div.style['translate'] = '167ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
        div.style['transform-origin'] = 'top center'
        div.style['-o-transform-origin'] = 'top center'
        div.style['-ms-transform-origin'] = 'top center'
        div.style['-webkit-transform-origin'] = 'top center'
        return div
    }
    const CreateSelectionLayer = function () {
        let div = CreateDefaultLayer();
        div.classList.add(selectionClassName);
        return div
    }
    const CreateSearchHighlightLayer = function () {
        let div = CreateDefaultLayer();
        div.classList.add(searchClassName);
        return div
    }
    const CreateDrawingLayer = function () {
        let div = CreateDefaultLayer();
        div.classList.add(drawingClassName);
        return div
    }
    const isNumberCheck = (value) =>{
        return `${value}`.match(/^\d+([\.,]\d+)?$/)
    } 
    /**
     * 
     * @param {DOMRectList} rects 
     * @returns 
     */
    const CreatSVGDrawingLayer = function(rects,options){

        let parentWidth = 0;
        let parentHeight = 0;
        let parentX = 0;
        let parentY = 0;
        let rect = null;
        let iconPath = null;
        let color = "#1780c41e"
        let displayOption = true;

        if (options){
            if (isNumberCheck(options.width)){
                parentWidth = options.width;
            }
            if (isNumberCheck(options.height)){
                parentHeight = options.height
            }
            if (isNumberCheck(options.x)){
                parentX = options.x
            }
            if (isNumberCheck(options.y)){
                parentY = options.y
            }
            if (options.color){
                color = options.color
            }
            if (options.displayOption === true || options.displayOption === false){
                displayOption = options.displayOption;
            }
        }

        //create Element
        let iconSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        iconSvg.setAttribute('fill', 'none');
        iconSvg.setAttribute('viewBox', `0 0 ${parentWidth} ${parentHeight}`);
        for (let j = 0; j < rects.length; j++) {
            rect = rects[j];
            iconPath = document.createElementNS('http://www.w3.org/2000/svg','rect');
            iconPath.setAttribute('width',rect.width);
            iconPath.setAttribute('height',rect.height);
            iconPath.setAttribute('x',rect.x - parentX);
            iconPath.setAttribute('y',rect.y - parentY);
            iconPath.setAttribute('fill',color);
            if (!displayOption){
                iconPath.style.visibility = 'hidden';
            }
            iconSvg.appendChild(iconPath);
            rect = null;
            iconPath = null;
        }

        return iconSvg;
    }
    return EpubBuilder
})();

const Builder = new EpubBuilder();
/******************************************
 * TTS 모듈
 * @description 의존성을 전부 제거한 tts 모듈
 *******************************************/
const EpubTTS = (function (epubBuilder) {
    function EpubTTS() { }

    const PLAY_STATUS = {
        STOP: -1,
        PAUSE: 0,
        PLAY: 1,
        PENDING: 2
    }

    const callbackOpt = {
        onChanged: null,
    }
    /**
     * 
     * -1: stop
     *  0: pause
     *  1: play
     *  2: pending
     */
    let playStatus = PLAY_STATUS.STOP;
    const setPlayStatus = function(status){
        playStatus = status;
    }

    /**
     * text 정보
     */
    const textMap = new Map()

    const drawMap = new Map();
    /**
     * 음성엔진
     */
    let currentVoiceEngine = null
    /**
     * 속도
     */
    let currentVoiceSpeed = 1
    /**
     * 음 높낮이
     */
    let currentVoicePitch = 1
    /**
     * 현재 페이지의 텍스트
     */
    let currentItem = null;
    /**
     * 현재 챕터 번호
     */
    let currentChapterIndex = 2;
    /**
     * 현재 페이지 아이템의 index
     */
    let currentPlayingIndex = 0;

    /**
     * status Getter
     */
    EpubTTS.PLAY_STATUS = PLAY_STATUS;
    /**
     * getter
     */
    EpubTTS.prototype.getPlayStatus = function(){
        return playStatus;
    }
    /**
     * setter
     * @param {*} options 
     */
    EpubTTS.prototype.setOnChaned = function(callback){
        if (!callback){
            return;
        }
        if (!callback){
            return;
        }
        if (typeof callback !== 'function'){
            return;
        }
        callbackOpt.onChanged = callback;
    }
    /**
     * getter
     */
    EpubTTS.prototype.textMap = (function () {
        return textMap;
    })()


    /**
     * chapter의 text를 추출한다.
     * @param {*} file 
     * @returns 
     */
    EpubTTS.prototype.loadFile = function (key, targetElement , isReset) {
        if (!targetElement) {
            return Promise.reject("targetElement must be defined");
        }

        let range = document.createRange();
        let rects = null;
        let list = _getWalkerList(targetElement)
        let mlist = []
        let total = 0;
        for (let i = 0; i < list.length; i++) {
            let item = list[i];
            let group = []
            range.selectNode(item)
            rects = range.getClientRects()
            for(var j = total; j < total+rects.length; j++){
                group.push(j)
            }
            total += rects.length;
            mlist.push(Object.assign({
                group: group,
                text: item.textContent.replaceAll("{CONTENT_OBFUCATE}", " "),
                rects: rects
            }))
            item = null;
        }
        textMap.set(key, { data: [...mlist] })
        list = null;
        Build(key);
        return Promise.resolve(textMap);
    }
    EpubTTS.prototype.setEngine = function () {
        // let voices = synth.getVoices();
        // console.log(voices);
    }
    /**
     * 재생 or 정지
     */
    EpubTTS.prototype.playOrPause = function () {
        if (playStatus == PLAY_STATUS.PLAY){
            speakCancel();
        } else if (playStatus == PLAY_STATUS.PAUSE){
            currentItemSpeaking();
        } else if (playStatus == PLAY_STATUS.STOP){
            currentItemSpeaking();
        } else if (playStatus == PLAY_STATUS.PENDING){
            console.log('TTS Hanging..')
        } else {
            throw new Error("not define status")
        }
    }
    /**
     * 취소
     */
    EpubTTS.prototype.cancel = function () {
        speakCancel();
    }
    /**
     * 다음
     */
    EpubTTS.prototype.next = function () {
        _next();
    }
    /**
     * 이전
     */
    EpubTTS.prototype.prev = function () {
        _prev();
    }
    const getSelector = function(key,suffix){
        if (suffix){
            return "#bdb-chapter" + key + " ." + epubBuilder.ttsClassName + ' ' + suffix;
        } else {
            return "#bdb-chapter" + key + " ." + epubBuilder.ttsClassName;
        }
    }
    /**
     * 해당 파일을 파싱
     * @param {*} fileno 파일 번호
     * @returns 
     */
    const Build = function (fileno) {
        if (textMap.size === 0) {
            return Promise.reject("textMap Empty")
        }
        let item = null;
        let seletor = null;
        let selector = null;
        let rect = null;
        let list = []
        let ancestorRect = null;
        let parentRect = null;
        let options = null;
        let eleSVG = null;
        try {
            item = textMap.get(fileno)
            seletor = getSelector(fileno);
            selector = document.querySelector(seletor)
            for (let it of item.data) {
                for (let j = 0; j < it.rects.length; j++) {
                    rect = it.rects[j];
                    list.push(rect);
                    rect = null;
                }
            }
            ancestorRect = selector.parentElement.parentElement.getBoundingClientRect();
            parentRect = selector.parentElement.getBoundingClientRect();
            options = { width: parentRect.width, height: parentRect.height, x: ancestorRect.x, y: ancestorRect.y, displayOption: false }
            eleSVG = epubBuilder.CreatSVGDrawingLayer(list,options);
            selector.appendChild(eleSVG);
            return Promise.resolve(true);
        } catch(err){
            console.error("TTS Build Fail",err);
        } finally {
            item = null;
            seletor = null;
            selector = null;
            rect = null;
            list = null;
            ancestorRect = null;
            parentRect = null;
            options = null;
            eleSVG = null;
        }
    }
    /**
     * 하이라이트 그리기
     * @returns 
     */
    const drawHighlight = function(){
        let seletor = getSelector(currentChapterIndex,'svg')
        let targetele = document.querySelector(seletor)
        let elechild = null;
        let fileitem = null;
        let item = null;
        let groupchild = null;
        let svgele = null;
        try {
            if (!targetele){
                return false;
            }
            //clearAll
            for (let index = 0; index < targetele.children.length; index++) {
                elechild = targetele.children[index];
                if (elechild.style.visibility === 'visible'){
                    elechild.style.visibility = 'hidden'
                }
                elechild = null;
            }
            elechild = null;
    
            fileitem = getCurentFileItem();
            if (fileitem && fileitem.length > 0){
                item = fileitem[currentPlayingIndex];
                if (item){
                    for (let index = 0; index < item.group.length; index++) {
                        groupchild = item.group[index];
                        svgele = targetele.children[groupchild];
                        if (svgele){
                            svgele.style.visibility = 'visible';
                        }
                    }
                }
            }
            return true;
        } catch(err){

        }finally {
            seletor = null;
            targetele = null;
            elechild = null;
            fileitem = null;
            item = null;
            groupchild = null;
            svgele = null;
        }
    }
    /**
     * 하이라이트 지우기
     * @returns 
     */
    const clearHighlight = function(){
        let seletor = getSelector(currentChapterIndex,'svg')
        let targetele = document.querySelector(seletor)
        if (!targetele){
            return;
        }
        for (let index = 0; index < targetele.children.length; index++) {
            let element = targetele.children[index];
            if (element.style.visibility === 'visible'){
                element.style.visibility = 'hidden'
            }
        }
    }
    /**
     * 다음 재생
     */
    const _next = function(){
        if (playStatus == PLAY_STATUS.PENDING){
            return;
        }
        setPlayStatus(PLAY_STATUS.PENDING);
        speakCancel();
        if (playStatus == PLAY_STATUS.STOP){

        } else {
            moveNextIndex();
        }
        currentItemSpeaking();
    }
    /**
     * 이전 재생
     */
    const _prev = function(){
        if (playStatus == PLAY_STATUS.PENDING){
            return;
        }
        setPlayStatus(PLAY_STATUS.PENDING);
        speakCancel();
        if (playStatus == PLAY_STATUS.STOP){

        } else {
            movePrevIndex();
        }
        currentItemSpeaking();
    }
    /**
     * 현재 재생중인 파일 객체
     * @returns 
     */
    const getCurentFileItem = function(){
        let textObj = textMap.get(currentChapterIndex)
        if (textObj && textObj.data && textObj.data.length > 0){
            return textObj.data;
        }
        return null;
    }
    /**
     * 현재 재생할 음성데이터 객체
     * @returns 
     */
    const getCurrentPlayItem = function(){
        let data = getCurentFileItem();
        let current = data[currentPlayingIndex];
        if (current && current.text){
            return current;
        }
        return null;
    }
    /**
     * [이동] text 어절을 파라미터로 받은 index로 이동시킨다.
     * @param {*} index 이동할 index
     * @returns 
     */
    const moveIndex = function(index){
        let textObj = getCurentFileItem();
        if (textObj){
            let current = textObj[index]
            if (current){
                currentPlayingIndex = index;
                return currentPlayingIndex;
            }
        }
        throw new Error("moveIndex, not found index");
    }

    /**
     * [다음] text 어절을 다음 index로 이동시킨다.
     */
    const moveNextIndex = function(){
        // let file = currentChapterIndex;
        if (currentPlayingIndex >= 0 ){
            let textObj = getCurentFileItem();
            if (textObj){
                let current = textObj[currentPlayingIndex+1]
                if (current){
                    currentPlayingIndex++;
                    return currentPlayingIndex;
                } else {
                    //다음 파일 케이스
                }
            } else {

            }
        } else {

        }
    }
    /**
     * [이전] text 어절을 이전 index로 이동시킨다.
     */
    const movePrevIndex = function(){
        if (currentPlayingIndex >= 0 ){
            let textObj = getCurentFileItem();
            if (textObj){
                let current = textObj[currentPlayingIndex-1]
                if (current){
                    currentPlayingIndex--;
                } else {
                    //이전 파일케이스
                }
            } else {

            }
        } else {

        }
    }
    /**
     * 현재 아이템 재생
     * @returns 
     */
    const currentItemSpeaking = function(){
        setPlayStatus(PLAY_STATUS.PLAY);
        let current = getCurrentPlayItem();
        if (current){
            if (callbackOpt.onChanged && typeof callbackOpt.onChanged == 'function'){
                callbackOpt.onChanged("speaked",currentChapterIndex,currentPlayingIndex,current);
            }
            drawHighlight();
            speek(current).then(rs=>{
                _next();
            }).catch(err=>{
                console.log(err);
            })
            return;
        }
        throw new Error("currentItemSpeaking, not found index");
    }
    /**
     * 말하기 취소
     */
    const speakCancel = function(){
        if (window.speechSynthesis) {
            playStatus = PLAY_STATUS.PAUSE;
            window.speechSynthesis.cancel();
        }
    }
    /**
     * 
     * @param {*} param0 
     * @returns 
     */
    const speek = function ({
        text = null
    } = {}) {
        if (!window.speechSynthesis) {
            return Promise.reject("cannot supported 'speechSynthesis' function");
        }
        if (!text) {
            return Promise.reject("empty text");
        }
        const synth = window.speechSynthesis;
        const utterThis = new SpeechSynthesisUtterance(text);
        utterThis.pitch = currentVoicePitch;
        utterThis.rate = currentVoiceSpeed;
        return new Promise(function (resolve, reject) {
            utterThis.addEventListener('end', function (event) {
                resolve(event)
            }, { once: true })
            utterThis.addEventListener('error', function (event) {
                reject(event)
            }, { once: true })
            synth.speak(utterThis);
        })
    }
    /**
     * Private function
     * @param {*} node 
     * @returns 
     */
    const _acceptNode = (node) => {
        if (!(node.parentElement instanceof HTMLUnknownElement)) {
            if (! /^\s*$/.test(node.data)) {
                return NodeFilter.FILTER_ACCEPT;
            }
        }
    }
    const _acceptNodeEmpty = (node) => {
        if (!(node.parentElement instanceof HTMLUnknownElement)) {
            return NodeFilter.FILTER_ACCEPT;
        }
    }
    const _Build = (ele, acceptEmpty) => {
        if (!ele) {
            return null;
        }
        if (acceptEmpty) {
            return document.createTreeWalker(ele, NodeFilter.SHOW_TEXT, _acceptNodeEmpty, false);
        } else {
            return document.createTreeWalker(ele, NodeFilter.SHOW_TEXT, _acceptNode, false);
        }
    }
    const _getWalkerList = (ele) => {
        let walk = _Build(ele);
        if (!walk) return null;
        var nodeList = [];
        while (walk.nextNode()) {
            nodeList.push(walk.currentNode);
        }
        return nodeList;
    }

    return EpubTTS;
})(Builder);



window.addEventListener('DOMContentLoaded', function(){
    const prevsvg = `<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet"
            focusable="false" class="style-scope tp-yt-iron-icon"
            style="pointer-events: none; display: block; width: 100%; height: 100%;">
            <g class="style-scope tp-yt-iron-icon">
                <path d="M19,6L9,12l10,6V6L19,6z M7,6H5v12h2V6z" class="style-scope tp-yt-iron-icon">
                </path>
            </g>
        </svg>`
    const nextsvg = `<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet"
        focusable="false" class="style-scope tp-yt-iron-icon"
        style="pointer-events: none; display: block; width: 100%; height: 100%;">
        <g class="style-scope tp-yt-iron-icon">
            <path d="M5,18l10-6L5,6V18L5,18z M19,6h-2v12h2V6z" class="style-scope tp-yt-iron-icon">
            </path>
        </g>
        </svg>`
    const pausesvg = `<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" class="style-scope tp-yt-iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g height="24" viewBox="0 0 24 24" width="24" class="style-scope tp-yt-iron-icon"><path d="M9,19H7V5H9ZM17,5H15V19h2Z" class="style-scope tp-yt-iron-icon"></path></g></svg>`
    const playsvg = `<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" class="style-scope tp-yt-iron-icon" style="pointer-events: none; display: block; width: 100%; height: 100%;"><g class="style-scope tp-yt-iron-icon"><path d="M6,4l12,8L6,20V4z" class="style-scope tp-yt-iron-icon"></path></g></svg>`
    const viewer = this.document.getElementById('viewer')
    const chapter2 = this.document.getElementById('bdb-chapter2');

    const eleplayOrPause = this.document.getElementById('playOrPause');
    const elePlayerPlayOrPause = this.document.getElementById('player-play-or-pause');
    const elePlayerNext = this.document.getElementById('player-next');
    const elePlayerPrev = this.document.getElementById('player-prev');

    const epubtts = new EpubTTS();
    epubtts.setOnChaned(function(action,file,idx,item){
        console.log(action,file,idx,item)
    })
    epubtts.loadFile(2,chapter2);

    const changePlayBtnUI = (status)=>{
        if (status == EpubTTS.PLAY_STATUS.PLAY){
            eleplayOrPause.innerHTML = pausesvg;
        } else if (status == EpubTTS.PLAY_STATUS.PAUSE){
            eleplayOrPause.innerHTML = playsvg;
        } else if (status == EpubTTS.PLAY_STATUS.STOP){
            eleplayOrPause.innerHTML = playsvg;
        } else if (status == EpubTTS.PLAY_STATUS.PENDING){
            eleplayOrPause.innerHTML = '....'
        } else {
            //unknown
        }
    }
    this.window.addEventListener('resize',function(){
        epubtts.loadFile(2,chapter2);
    })

    elePlayerPlayOrPause.addEventListener('click', function(){
        epubtts.playOrPause();
        changePlayBtnUI(epubtts.getPlayStatus())
    })
    elePlayerNext.addEventListener('click', function(){
        epubtts.next();
    })
    elePlayerPrev.addEventListener('click', function(){
        epubtts.prev();
    })
})