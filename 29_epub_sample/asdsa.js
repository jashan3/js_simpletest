



/*************************************
 *********** EPUB STORAGE ************
 *************************************/
const EpubLocalStorage = (function () {
    const WEVICL_EPUB_DATABASE_VER = 1;
    const WEVICL_EPUB_DATABASE = "WEVICL_EPUB_DATABASE";
    const TABLE = {
        CONTENTS: "TB_CONTENTS"
    }


    function EpubLocalStorage() {
        this.idb = this.init();
    }

    EpubLocalStorage.prototype.init = function () {
        if (!window.indexedDB) return Promise.reject("window.indexedDB not supported");
        return new Promise(function (resolve, reject) {
            let openRequest = window.indexedDB.open(WEVICL_EPUB_DATABASE, WEVICL_EPUB_DATABASE_VER);
            openRequest.onupgradeneeded = function (evt) {
                ;
                var store = evt.currentTarget.result.createObjectStore(TABLE.CONTENTS, { keyPath: 'index' });
                // store.createIndex('index', 'index', { unique: true });
                // store.createIndex('item', 'item', { unique: false });
                // store.createIndex('date', 'date', { unique: false });
            };
            openRequest.onerror = function (e) {
                console.warn("onerror", e);
                reject(err);
            };
            openRequest.onblocked = function (e) {
                console.warn("onblocked", e);
            }
            openRequest.onsuccess = function (evt) {
                db = this.result;
                console.log("##onsuccess", db, evt)
                resolve(db);
            };
        })
    }

    EpubLocalStorage.prototype.getAll = function (type) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.idb && self.idb.then(idb => {
                const store = _getObjectStore(idb, TABLE.CONTENTS, "readonly")
                const request = store.getAll()
                request.onsuccess = e => resolve(e.target.result);
                request.onerror = e => reject(e.target.result);
            }).catch(err => {
                reject(err);
            })
        })
    }


    EpubLocalStorage.prototype.get = function (type) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.idb && self.idb.then(idb => {
                const store = _getObjectStore(idb, TABLE.CONTENTS, "readonly")
                const request = store.get(type)
                request.onsuccess = e => resolve(e.target.result);
                request.onerror = e => reject(e.target.result);
            }).catch(err => {
                reject(err);
            })
        })
    }

    EpubLocalStorage.prototype.set = function (key, item) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.idb && self.idb.then(idb => {
                const store = _getObjectStore(idb, TABLE.CONTENTS, "readwrite")
                const request = store.put(item, key)

                request.onsuccess = e => resolve(e.target.result);
                request.onerror = e => reject(e.target.result);
            }).catch(err => {
                reject(err);
            })
        })
    }

    EpubLocalStorage.prototype.remove = function (type) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.idb && self.idb.then(idb => {
                const store = _getObjectStore(idb, TABLE.CONTENTS, "readwrite")
                const request = store.delete(type)
                request.onsuccess = e => resolve(e.target.result);
                request.onerror = e => reject(e.target.result);
            }).catch(err => {
                reject(err);
            })
        })
    }

    function _getObjectStore(idb, store_name, mode) {
        if (!idb) return;
        var tx = idb.transaction(store_name, mode);
        let store = tx.objectStore(store_name);
        return store
    }
    return EpubLocalStorage
})();

