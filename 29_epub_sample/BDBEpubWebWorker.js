
// importScripts("/script/common/lib/crypto-js.min.js");

/**
 * Listener worker post message
 */
self.addEventListener('message', async function( e ) {
    const mauthToken = self.location.href.split('/').pop()
    const obj = JSON.parse(e.data);
    const { bcd, sbcd , finger , data , url , key , version, path, index } = obj

    console.log("[MESSAGE]",obj,mauthToken,this.self.location.hostname);
    // contentBaseUrl, 
    // item.path,
    // authToken, 
    // idx

    try {
        const repo = new Repo(key,version);
        await repo.init();
        const response = await repo.getRepository({
            baseUrl: url, 
            url: path, 
            token: mauthToken,
            index,
        })
        const result = getResult({ bcd,sbcd,finger,response,url })
        self.postMessage(result)
    } catch(error) {
        self.postMessage(error)
    }
});




function getResult({bcd, sbcd , finger, data , url  }){
    let iv = null;
    let key = null;
    let decrypted = null;
    iv = makeIV(bcd,sbcd);
    key = makeKey(finger)
    if (typeof data === 'string'){
        decrypted = decryptStrToStr(data,key,iv);
        decrypted = replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", url);
        decrypted = replaceAll(decrypted, "{CONTENT_DOMAIN}", url);
        return decrypted
    }
}
function makeIV(bcd,sbcd){
    if (!CryptoJS){
        return null;
    }
    return CryptoJS.enc.Utf8.parse(bcd.substring(bcd.length-8) + sbcd.substring(sbcd.length - 8))
}
function makeKey(finger){
    if (!CryptoJS){
        return;
    }
    let key = null;
    for (let i=0; i<finger.length; i++) finger[i] = finger[i] >> 2
    key = finger;
    return CryptoJS.lib.WordArray.create(new Uint8Array(key))
}
function decryptBufferToBuffer(d,key,iv){
    if (!CryptoJS){
        return null;
    }
    var u = CryptoJS.AES.decrypt(
        CryptoJS.lib.CipherParams.create({ ciphertext: CryptoJS.enc.Base64.parse(d) }),
        key, { iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }
    );
    return u;
}
function decryptStrToStr(d,key,iv) {
    if (!CryptoJS){
        return null;
    }
    var u = CryptoJS.AES.decrypt(
        CryptoJS.lib.CipherParams.create({ ciphertext: CryptoJS.enc.Base64.parse(d) }),
        key, { iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }
    );
    return u.toString(CryptoJS.enc.Utf8);
}
function replaceAll(str, searchString, replaceString){
    return str.split(searchString).join(replaceString);
}

/**
 * Repo class
 */
const Repo = (function () {
    function Repo(key,drmVersion){
       this.uKEY = key;
       this.drmVersion = drmVersion;
       this.epubLocalStorage = new EpubLocalStorage()
    }

    Repo.prototype.init = function(){
        return this.epubLocalStorage.lazyInit()
    }
    /**
     * {
     * baseUrl: contentBaseUrl, 
     * url: item.path,
     * token: authToken, 
     * index: idx
     * }
     * @param { baseUrl , url , token ,index} param0 
     * @returns 
     */
    Repo.prototype.getRepository = function({
        baseUrl = null, 
        url = null, 
        token = null,
        index = -1,
    }){
        return new Promise((resolve,reject) => {
            this.epubLocalStorage.get(this.uKEY).then(obj=>{
                let nArray = []
                let mFind = null
                if (obj){
                    //버전이 같은 경우 그대로 진행
                    if (obj.ver && obj.ver === drmVersion){
                        nArray = [...obj.data];
                        mFind = nArray.find(e=>e.index === index)
                    }
                }
                
                //이미 있음 로컬데이터 반환
                if (mFind){
                    resolve(mFind.data)
                }
                //없음 없는경우 통신을 통해 가저온다.
                else {
                    //empty , not found index case
                    fetchUrl({baseUrl, url}).then(newData =>{
                        nArray.push({index, data: newData })
                        this.epubLocalStorage.set({ token: this.uKEY, data: nArray, ver: this.drmVersion  }).then(r=>{
                            resolve(newData)
                        }).catch(e=>{
                            resolve(newData)
                        })
                    }).catch(err2=>{
                        reject(err2);
                    })
                }
            }).catch(err=>{
                console.error("3",err);
                fetchUrl({baseUrl, url}).then(newData =>{
                    resolve(newData)
                }).catch(err2=>{
                    reject(err2);
                })
            })
        })
    
    }
    const fetchUrl = function({
        baseUrl = null, 
        url = null, 
    }){
        if (!baseUrl || !url ){
            return Promise.reject("not empty")
        }
        const mUrl = url.replace('{CONTENT_DOMAIN}', baseUrl);
        return self.fetch(mUrl , { method: 'GET'})
            .then(rs=>rs.text())
            .then(data=> JSON.parse(data.replace(/^DataCallback\(|\)\;/g, '')).value)
    }
    return Repo
})();

/*************************************
 *********** EPUB STORAGE ************
*************************************/
const EpubLocalStorage = (function () {
    const WEVICL_EPUB_DATABASE_VER = 1;
    const WEVICL_DB_PREFIX = "DB"


    var DEFAULT_SCHEMA = self.location.hostname
    const WEVICL_EPUB_DATABASE = "EPUB_DATABASE";
    const TABLE = {
        CONTENTS: "TB_CONTENTS"
    }
    function EpubLocalStorage() { 
        this.idb = null;
    }

    function getDataBaseName() {
        return WEVICL_DB_PREFIX + "_" + DEFAULT_SCHEMA + "_" + WEVICL_EPUB_DATABASE;
    }

    EpubLocalStorage.prototype.setDataBaseName = function (name) {
        DEFAULT_SCHEMA = name
    }

    EpubLocalStorage.prototype.lazyInit = function () {
        this.idb = this.init();
        return this.idb
    }

    EpubLocalStorage.prototype.init = function () {
        if (!self.indexedDB) return Promise.reject("window.indexedDB not supported");
        return new Promise(function (resolve, reject) {
            let openRequest = self.indexedDB.open(getDataBaseName(), WEVICL_EPUB_DATABASE_VER);
            openRequest.onupgradeneeded = function (evt) {
                var store = evt.currentTarget.result.createObjectStore(TABLE.CONTENTS, { keyPath: 'token' });
                store.createIndex('token', 'token', { unique: true });
            };
            openRequest.onerror = function (e) {
                console.warn("onerror", e);
                reject(err);
            };
            openRequest.onblocked = function (e) {
                console.warn("onblocked", e);
            }
            openRequest.onsuccess = function (evt) {
                resolve(this.result);
            };
        })
    }

    /**
     * selectAll
     * @param {*} type 
     * @returns 
     */
    EpubLocalStorage.prototype.getAll = function (type) {
        return new Promise((resolve, reject) => {
            this.idb && this.idb.then(idb => {
                const store = _getObjectStore(idb, TABLE.CONTENTS, "readonly")
                const request = store.getAll()
                request.addEventListener('success', function (e) {
                    resolve(e.target.result);
                }, { once: true })
                request.addEventListener('error', function (e) {
                    reject(e.target.result);
                }, { once: true })
            }).catch(err => {
                reject(err);
            })
        })
    }
    /**
     * select
     * @param {*} type 
     * @returns 
     */
    EpubLocalStorage.prototype.get = function (type) {
        return new Promise((resolve, reject) => {
            if (type) {
                this.idb && this.idb.then(idb => {
                    const store = _getObjectStore(idb, TABLE.CONTENTS, "readonly")
                    const request = store.get(type)
                    request.addEventListener('success', function (e) {
                        resolve(e.target.result);
                    }, { once: true })
                    request.addEventListener('error', function (e) {
                        reject(e.target.result);
                    }, { once: true })
                }).catch(err => {
                    reject(err);
                })
            } else {
                reject(new Error("type must be filled!!"))
            }
        })
    }
    /**
     * insert or update
     * @param {*} item 
     * @returns 
     */
    EpubLocalStorage.prototype.set = function (item) {
        return new Promise((resolve, reject) => {
            this.idb && this.idb.then(idb => {
                const store = _getObjectStore(idb, TABLE.CONTENTS, "readwrite")
                const request = store.put(item)
                request.addEventListener('success', function (e) {
                    resolve(e.target.result);
                }, { once: true })
                request.addEventListener('error', function (e) {
                    reject(e.target.result);
                }, { once: true })
            }).catch(err => {
                reject(err);
            })
        })
    }
    /**
     * Delete
     * @param {*} type 
     * @returns 
     */
    EpubLocalStorage.prototype.remove = function (type) {
        return new Promise((resolve, reject) => {
            this.idb && this.idb.then(idb => {
                const store = _getObjectStore(idb, TABLE.CONTENTS, "readwrite")
                const request = store.delete(type)
                request.addEventListener('success', function (e) {
                    resolve(e.target.result);
                }, { once: true })
                request.addEventListener('error', function (e) {
                    reject(e.target.result);
                }, { once: true })
            }).catch(err => {
                reject(err);
            })
        })
    }
    /**
     * get transaction
     * @param {*} idb 
     * @param {*} store_name 
     * @param {*} mode 
     * @returns 
     */
    function _getObjectStore(idb, store_name, mode) {
        if (!idb) return;
        var tx = idb.transaction(store_name, mode);
        let store = tx.objectStore(store_name);
        return store
    }
    return EpubLocalStorage
})();



