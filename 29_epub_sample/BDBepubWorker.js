
/**
 * epub contents parse 및 decrypt 모듈
 */
const EpubWebWorker = (function(){
	var murl = null
	var mbcd = null
	var msbcd = null
	var mfinger = null
    var webworkerSrc = "/script/common/bdb.web.worker.js";
	var gloablWorker = null;

	function EpubWebWorker(){ }

    /**
     * worker 초기화
     * @param {*} contentBaseUrl 
     * @param {*} bcd 
     * @param {*} sbcd 
     * @param {*} finger 
     * @returns 
     */
	EpubWebWorker.prototype.init = function(contentBaseUrl,bcd,sbcd,finger,workerSrc){
		if (workerSrc){
			webworkerSrc = workerSrc
		}
		return new Promise(function(resolve,recject){
			murl = contentBaseUrl;
			mbcd = bcd;
			msbcd = sbcd;
			mfinger = finger
			if( window.Worker ) {
				gloablWorker = new Worker(webworkerSrc);
				resolve(gloablWorker);
			} else {
				recject(new Error("cannot support Worker Class"));
			}
		})

	}
    /**
     * worker를 지원하는지 여부
     * @returns 
     */
	EpubWebWorker.prototype.isSupport = function(){
		if( window.Worker ) {
			return true;
		}
		return false;
	}
    /**
     * 생성되어있는 worker로 컨텐츠 암호화를 해제한다.
     * @param {*} param0 
     * @returns 
     */
	EpubWebWorker.prototype.postMessage = function({ key = null, data = null } = {}){
		return new Promise(function(resolve,reject){
			if( window.Worker ) {
				gloablWorker.addEventListener('message',function(e){
					resolve(e.data)
				},{ once: true })
				gloablWorker.addEventListener('messageerror',function(e){
					reject(e);
				},{ once: true })
				gloablWorker.addEventListener('error',function(e){
					reject(e);
				},{ once: true })
				gloablWorker.postMessage(JSON.stringify({key,data,url: murl,bcd: mbcd,sbcd: msbcd, finger: mfinger}))
			} else {
				reject(new Error("window.Worker cannot supported"))
			}
		})
	}
    /**
     * 일회성으로 worker로 컨텐츠 암호화를 해제한다.
     * @param {*} param0 
     * @returns 
     */
	EpubWebWorker.prototype.postOncePromise = function({key = null, data = null, idx = -1, path = null, src = webworkerSrc} = {}){
		return new Promise((resolve,reject) => {
			if( window.Worker ) {
				var worker = new Worker(src);
				worker.addEventListener('message',function(e){
					console.log("message")
					worker.terminate();
					worker = null;
					resolve(e.data)
				},{ once: true })
				worker.addEventListener('messageerror',function(e){
					console.log("messageerror")
					worker.terminate();
					worker = null;
					reject(e);
				},{ once: true })
				worker.addEventListener('error',function(e){
					console.log("error")
					worker.terminate();
					worker = null;
					reject(e);
				},{ once: true })
				worker.postMessage(JSON.stringify({
					key,data,
					url: murl,
					bcd: mbcd,
					sbcd: msbcd, 
					finger: mfinger,
					index: idx,
					path
				}))
			} else {
				reject(new Error("window.Worker cannot supported"))
			}
		})
	}

	return EpubWebWorker
})();

export default EpubWebWorker;