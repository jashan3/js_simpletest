const express = require('express');
const app = express();
const fs = require('fs')
const https = require('https');
const http = require('http');
const serverInfo = require("./config/svr-config.json");
const options = {
    ca: fs.readFileSync(require.resolve(serverInfo.HTTPS_CERT_BUNDLE)),
    key: fs.readFileSync(require.resolve(serverInfo.HTTPS_CERT_PRIVATE), 'utf8').toString(),
    cert: fs.readFileSync(require.resolve(serverInfo.HTTPS_CERT_CERT), 'utf8').toString(),
}
const Port = 8081 | process.env.PORT || parseInt(serverInfo.PORT);
const SSLPort = 4430 | process.env.SSLPORT || parseInt(serverInfo.SSLPORT);
const path = require('path');
http.createServer(app).listen(Port);
https.createServer(options, app).listen(SSLPort);

app.use(express.static(__dirname));

// const mainRouter = express();
// mainRouter.get("/" , async (req, res, next) => {
//     res.sendFile(path.join(__dirname+'/index.html'));
// })

// app.use(express.static('src'));
// app.use(mainRouter);