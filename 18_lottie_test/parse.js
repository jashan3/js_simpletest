



const KyoboObject = {
  KEY: {
    ECASH: {
      /**
       * 회원번호
       */
      mmbrNum: 'mmbrNum',
      /**
       * 사용처코드
       */
      ecsUsplCode:   "ecsUsplCode", // "001",
      /**
       * e캐시총사용금액
       */
      ecsTtlUseAmnt:   "ecsTtlUseAmnt", // 50000,
      /**
       * 현금영수증 신청유형코드
       */
      csrcAplnPatrCode: "csrcAplnPatrCode", // "001",
      /**
       * 주 결제방법코드
       */
      mainStlmMthdCode:   "mainStlmMthdCode", // 119,
      /**
       * 통합주문채널코드
       */
      unfyOrdrChnlCode:  "unfyOrdrChnlCode", // "001"
      /**
       * 통합주문상세채널코드
       */
      unfyOrdrDtlChnlCode: "unfyOrdrDtlChnlCode", //"008"
      /**
       * e캐시 상품리스트
       */
      ecashCmdtList:  "ecashCmdtList", // [] 
      /**
       * e캐시 결제리스트
       */
      ecashStlmList: "ecashStlmList", // []
      /**
       * e캐시 상품리스트의 하위
       */
      /**
       * 기본할인여부
       */
      bscsDscnYsno: "bscsDscnYsno", //: "N",
      /**
       * 상품명
       */
      cmdtHnglName: "cmdtHnglName",//: "백팔번뇌. 1 신무학",
      /**
       * 쿠폰할인여부
       */
      cpnDscnYsno: "cpnDscnYsno",//: "N",
      /**
       * 쿠폰아이디 
       */
      cpnId: "cpnId",//: 2112010001,
      /**
       * 쿠폰발급번호
       */
      cpnIsncNum: "cpnIsncNum",//: 21120100010000868,
      /**
       * 할인여부 
       */ 
      dscnYsno: "dscnYsno",//: "N",
      // /**
      //  *  대여여부
      //  * */ 
      // ecsRentCmdtExusYsno: "ecsRentCmdtExusYsno",//: "N",
      /**
       * e캐시사용금액
       */
      ecsUseAmnt:  "ecsUseAmnt",//: 5000,
      /**
       * 이벤트할인 여부
       */
      evntDscnYsno: "evntDscnYsno",//: "N",
      /**
       * 상품순번 */ 
      ordrCmdtSrmb:  "ordrCmdtSrmb",//: 1,
      /**
       * 대여여부
       */
      rentYsno: "rentYsno",//: "N",
      /**
       * 판매상품ID
       */
      saleCmdtid: "saleCmdtid",// //: "KD01166259419",
      /**
       * 판매가
       */
      sapr: "sapr",// //: 5000,
      /**
       * e캐시 결제리스트
       */
      /**
       * 결제금액
       */
      stlmAmnt: "stlmAmnt",//: 0,
      /**
       * 결제방법코드
       */
      stlmMthdCode: "stlmMthdCode",//: "string"
    },
    PRODUCT: {
      /**
       * 19금제한여부(Y/N) : Y이면 19금상품
       */
      ageLimitYsno: "ageLimitYsno", //: "Y"
      /**
       * 기본 할인 적용 여부
       */
      bscDscnYsno: "bscDscnYsno", //: "N"
      /**
       * 구매가능상세메시지
       */
      buyPsblDtlMsg: "buyPsblDtlMsg", //: "성인인증이 필요한 상품입니다."
      /**
       * 구매가능여부(Y/N) : N이면 구매 불가
       */
      buyPsblYsno: "buyPsblYsno", //: "N"
      /**
       * 구매여부
       */
      buyYsno: "buyYsno", //: "Y"
      /**
       * 쿠폰발급여부
       */
      cpnIsncYsno: "cpnIsncYsno", //: "N"
      /**
       * 기본할인금액 대상 상품 구매 시 적용되는 기본 할인금액
       */
      dscnAmnt:  "dscnAmnt", //: 0
      /**
       * 할인율적용대상유무(Y/N)
       */
      dscnSaleCmdtYsno: "dscnSaleCmdtYsno", //: "N"
      /**
       * 대상 상품 구매 시 적용되는 이벤트 할인금액
       */
      evntDscnAmnt: "evntDscnAmnt", //: 0
      /**
       * 이벤트 할인 적용 여부
       */
      evntDscnYsno: "evntDscnYsno", //: "N"
      /**
       * 무료판매여부(Y/N)
       */
      freeSaleYsno: "freeSaleYsno", //: "N"
      /**
       * 쿠폰 적용 할인금액
       */
      saleCmdtCpnAmnt: "saleCmdtCpnAmnt", //: 0
      /**
       * 회원이 상품상세에서 다운로드 받은 상품할인쿠폰
       */
      saleCmdtCpnNum: "saleCmdtCpnNum", //: ""
      /**
       * 판매금지여부(Y/N) : 예약/판금이면 Y, 정상이면 
       */
      salePrhbYsno: "salePrhbYsno", //: "N"
      /**
       * 대여일수, 소장의 경우 0
       */
      stnrDyCont:  "stnrDyCont", //: 0
      /**
       * 영구구매 : U, 대여 : T 
       */
      order_type: "order_type", //: "U"
      /**
       * 주문번호 : 구매된 도서에 대해서 존재함, 없으면 null
       */
      ordercd: "ordercd", //: "T22070002953"
      /**
       * 보조 주문번호, 없을 경우 주문번호와 같은 값, 없으면 null
       */
      ordercdextra: "ordercdextra", //: "1"
      /**
       * 정가 대상 상품의 판매정가(연재 상품은 회당 정가)
       */
      prce: "prce", //: 500

      curHgrnItemId: "curHgrnItemId", //: "E000000112288"
      curItemId: "curItemId", //: "E000000349998"
      curRprsItemId:  "curRprsItemId", //: "E000000112288"
      nextHgrnItemId: "nextHgrnItemId", //: "E000000112288"
      nextItemId: "nextItemId", //: "E000000349999"
      nextRprsItemId: "nextRprsItemId", //: "E000000112288"
      prevHgrnItemId: "prevHgrnItemId", //: null
      prevItemId: "prevItemId", //: null
      prevRprsItemId: "prevRprsItemId", //: null
      rent_enddate: "rent_enddate", //: null
      rent_startdate: "rent_startdate", //: null
    }
  },

  make(memberID,price){
    let obj = new Object()
    obj[this.KEY.ECASH.mmbrNum] = memberID
    obj[this.KEY.ECASH.ecsUsplCode] = '001'
    obj[this.KEY.ECASH.ecsTtlUseAmnt] = ''
    obj[this.KEY.ECASH.csrcAplnPatrCode] = '001'
    obj[this.KEY.ECASH.mainStlmMthdCode] = 'K03'
    obj[this.KEY.ECASH.unfyOrdrChnlCode] = '001'
    obj[this.KEY.ECASH.unfyOrdrDtlChnlCode] = '001'
    obj[this.KEY.ECASH.ecashCmdtList] = [this.makeEcashCmdt()]
    obj[this.KEY.ECASH.ecashStlmList] = [this.makeEcashStlm(price)]
    return obj
  },
  /**
   * e캐시 상품리스트
   * @returns 
   */
  makeEcashCmdt(){
    let obj = new Object()
    obj[this.KEY.ECASH.bscsDscnYsno] = ''
    obj[this.KEY.ECASH.cmdtHnglName] = ''
    obj[this.KEY.ECASH.cpnDscnYsno] = ''
    obj[this.KEY.ECASH.cpnId] = ''
    obj[this.KEY.ECASH.cpnIsncNum] = ''
    obj[this.KEY.ECASH.ecsUseAmnt] = ''
    obj[this.KEY.ECASH.evntDscnYsno] = ''
    obj[this.KEY.ECASH.ordrCmdtSrmb] = 1
    obj[this.KEY.ECASH.rentYsno] = ''
    obj[this.KEY.ECASH.saleCmdtid] = ''
    obj[this.KEY.ECASH.sapr] = ''
    return obj
  },
  /**
   * e캐시 결제리스트 객체
   * @param {*} price 
   * @returns 
   */
  makeEcashStlm(price){
    let obj = new Object()
    obj[this.KEY.ECASH.stlmAmnt] = price
    obj[this.KEY.ECASH.stlmMthdCode] = 'K03'
    return obj
  },

  /**
   * 상품파라미터를 e캐시 상품리스트로 변환
   * @param {*} product 
   * @returns 
   */
  productToEcashCmdt(product,title){
    if (!product){
      return;
    }
    let obj = new Object()
    // o 기본할인여부
    obj[this.KEY.ECASH.bscsDscnYsno] = product[this.KEY.PRODUCT.bscDscnYsno]
    // 상품명
    obj[this.KEY.ECASH.cmdtHnglName] = title
    // 쿠폰할인여부
    obj[this.KEY.ECASH.cpnDscnYsno] = product[this.KEY.PRODUCT.cpnDscnYsno]
    // 쿠폰아이디
    obj[this.KEY.ECASH.cpnId] = product[this.KEY.PRODUCT.cpnId]
    // 쿠폰발급번호
    obj[this.KEY.ECASH.cpnIsncNum] = product[this.KEY.PRODUCT.cpnIsncNum]
    // e캐시사용금액
    obj[this.KEY.ECASH.ecsUseAmnt] = product[this.KEY.PRODUCT.prce]
    // o 이벤트 할인 적용 여부
    obj[this.KEY.ECASH.evntDscnYsno] = product[this.KEY.PRODUCT.evntDscnYsno]
    // o 상품순번
    obj[this.KEY.ECASH.ordrCmdtSrmb] = 1
    // o 대여여부
    obj[this.KEY.ECASH.rentYsno] = (this.KEY.PRODUCT.order_type === 'T')?"Y":"N"
    // o 판매상품id
    obj[this.KEY.ECASH.saleCmdtid] = product[this.KEY.PRODUCT.curItemId]
    // o 판매가(정가)
    obj[this.KEY.ECASH.sapr] = product[this.KEY.PRODUCT.prce]
    return obj
  }
  
}