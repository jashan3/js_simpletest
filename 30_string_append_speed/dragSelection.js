let selectionStart = null;
let selectionEnd = null;
let dragging = false;

const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');

document.addEventListener('DOMContentLoaded',function(){
    document.body.appendChild(svg);
})


document.addEventListener('mousedown', event => {
  selectionStart = event.clientX;
  selectionEnd = selectionStart;
  dragging = true;
  requestAnimationFrame(drawSelection);
});

document.addEventListener('mousemove', event => {
  if (dragging) {
    selectionEnd = event.clientX;
    requestAnimationFrame(drawSelection);
  }
});

document.addEventListener('mouseup', event => {
  dragging = false;
});

function drawSelection(timestamp) {
  // clear the previous selection
  while (svg.firstChild) {
    svg.removeChild(svg.firstChild);
  }

  // calculate the new selection range
  const start = Math.min(selectionStart, selectionEnd);
  const end = Math.max(selectionStart, selectionEnd);

  // draw the new selection using SVG
  const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
  rect.setAttribute('x', start);
  rect.setAttribute('y', 0);
  rect.setAttribute('width', end - start);
  rect.setAttribute('height', 100);
  rect.setAttribute('fill', '#ccc');
  rect.setAttribute('position','absolute')
  svg.appendChild(rect);

  // schedule the next frame
  if (dragging) {
    requestAnimationFrame(drawSelection);
  }
}