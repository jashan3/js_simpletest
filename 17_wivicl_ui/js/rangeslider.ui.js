/* RANGE SLIDER SCRIPT */
var $progress = $('.current-layer .progress .progress-bar');
$progress.rangeslider({
  polyfill: false,
  // Callback function
  onInit: function () {},
  // Callback function
  onSlide: function (position, value) {
    // console.log('onSlide', value);
  },
  // Callback function
  onSlideEnd: function (position, value) {
    // console.log('onSlideEnd');
  },
});
