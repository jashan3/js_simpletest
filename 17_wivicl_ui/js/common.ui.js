// TOAST MODAL
function ToastModal(option) {
  console.log(option);
  if (option.message !== undefined) {
    this.message = option.message;
  }

  this.duration = option.duration !== undefined ? option.duration : 500;
  this.continuance = option.continuance !== undefined ? option.continuance : 5000;
  this.setTimeNum = 0;
  var innerHTML = "";
  innerHTML += "<div class='toast-modal'><div class='toast-wrap'>";

  if (option.message !== undefined) {
    if (option.type !== undefined) {
      if (option.type == "type1") {
        innerHTML += "<p class='message type1'>" + this.message + "</p>";
      } else if (option.type === "single") {
        innerHTML += "<p class='message type2'><svg width='40' height='40' viewBox='0 0 40 40'><use href='#svg-modal-icon01'></use></svg><span class='guide'>" + this.message + "</span></p>";
      } else if (option.type === "both1") {
        innerHTML += "<p class='message type2'><svg width='40' height='40' viewBox='0 0 40 40'><use href='#svg-modal-icon02'></use></svg><span class='guide'>" + this.message + "</span></p>";
      } else if (option.type === "both2") {
        innerHTML += "<p class='message type2 multi'><svg width='40' height='40' viewBox='0 0 40 40'><use href='#svg-modal-icon02'></use></svg><span class='guide'>" + this.message + "</span></p>";
      } else if (option.type === "both3") {
        innerHTML += "<p class='message type2 multi'><svg width='40' height='40' viewBox='0 0 40 40'><use href='#svg-modal-icon03'></use></svg><span class='guide'>" + this.message + "</span></p>";
      } else if (option.type === "scroll") {
        innerHTML += "<p class='message type2'><svg width='40' height='40' viewBox='0 0 40 40'><use href='#svg-modal-icon04'></use></svg><span class='guide'>" + this.message + "</span></p>";
      }
    }
  }
  innerHTML += "</div></div>";

  var objThis = this;

  $(function () {
    $("body").append(innerHTML);
    $(".toast-modal").fadeIn(this.duration, function () {
      var $this = $(this);
      setTimeout(function () {
        $this.fadeOut(objThis.duration, function () {
          $this.remove();
        });
      }, objThis.continuance);
    });
  });
}

$(window).on("load", function () {
  // COMMON UI SCRIPT
  // LAYER UI SCRIPT
  var $wrap = $("#wrap");
  var $header = $("#wrap #header");
  var $currentLayer = $("#wrap .current-layer");
  var $sideMenu = $("#wrap .side-menu");
  var $tableLayer = $("#wrap .table-layer");
  var $searchLayer = $("#wrap .search-layer");
  var $noteLayer = $("#wrap .note-layer");
  var $totalLayer = $("#wrap .table-layer, #wrap .search-layer, #wrap .note-layer");
  var $font = $(".side-menu .util-layer .setting .font-setting .row.font button");

  $(document).on("click", function (e) {
    console.log(e.target);

    if ($font.hasClass("active")) {
      if (e.target.closest(".row01.font") === null) {
        $(".side-menu .util-layer .setting .font-setting .row01.font button").removeClass("active");
        $(".side-menu .util-layer .setting .font-setting .row01.font .font-list").slideUp("fast");
        return true;
      }
    }

    if ($(e.target).is(".side-menu") || $(e.target).is(".side-menu .container")) {
      if ($tableLayer.hasClass("active") || $searchLayer.hasClass("active") || $noteLayer.hasClass("active")) {
        $totalLayer.removeClass("active");
        return true;
      }

      $(".side-menu").removeClass("active");
    }

    if ($(e.target).is(".viewer-section") || $(e.target).is("#main-section") || $(e.target).is("#header") || $(e.target).is("#header .container") || $(e.target).is(".current-layer .container")) {
      if ($wrap.hasClass("readmode")) {
        $wrap.removeClass("readmode");
      } else {
        $wrap.addClass("readmode");
      }
    }
  });

  // BOOKMARK SCRIPT
  $("#header .bookmark").on("click", function () {
    $(this).toggleClass("active");
  });

  // CURRENT LAYER SCRIPT
  $(".current-layer .full-btn").click(function () {
    var img = $(".current-layer .full-btn img");

    img.attr("src", function (index, attr) {
      if (attr.match("icon01")) {
        return attr.replace("icon01", "icon02");
      } else {
        return attr.replace("icon02", "icon01");
      }
    });
  });

  // UTIL LAYER SCRIPT
  $(".current-layer .util-btn").on("click", function () {
    $(".side-menu").addClass("active");
  });

  $(".side-menu .menu .table-btn").on("click", function () {
    $(".side-menu .table-layer").addClass("active");
  });

  $(".side-menu .menu .search-btn").on("click", function () {
    $(".side-menu .search-layer").addClass("active");
  });

  $(".side-menu .menu .note-btn").on("click", function () {
    $(".side-menu .note-layer").addClass("active");
  });

  $(".side-menu .util-layer .view-type button").on("click", function () {
    $(".side-menu .util-layer .view-type button").removeClass("active");
    $(this).addClass("active");
  });

  $(".side-menu .util-layer .setting .theme .row01 button").on("click", function () {
    $(".side-menu .util-layer .setting .theme .row01 button").removeClass("active");
    $(this).addClass("active");
  });

  $(".side-menu .util-layer .setting .font-setting .row.font button").on("click", function () {
    $(this).toggleClass("active");
    $(".side-menu .util-layer .setting .font-setting .row.font .font-list").slideToggle("fast");
  });

  $(".side-menu .util-layer .setting .font-setting .row.font .font-list li").on("click", function () {
    var txt = $(this).text();

    $(".side-menu .util-layer .setting .font-setting .row.font .font-list li").removeClass("active");
    $(this).addClass("active");

    $(".side-menu .util-layer .setting .font-setting .row.font .font-list").slideToggle("fast");
    $(".side-menu .util-layer .setting .font-setting .row.font button").toggleClass("active");
    $(".side-menu .util-layer .setting .font-setting .row.font button .type").text(txt);
  });

  $(".side-menu .util-layer .setting .font-setting:not(.font, .row07) button:last-child").on("click", function () {
    $(this).toggleClass("active");
  });

  $(".side-menu .util-layer .setting .font-setting .row07 button").on("click", function () {
    $(".side-menu .util-layer .setting .font-setting .row07 button").removeClass("active");
    $(this).addClass("active");
  });

  $(".side-menu .util-layer .setting > .mode label").on("click", function () {
    $(".side-menu .util-layer .setting > .mode label .checkbox").removeClass("active");
    $(this).find(".checkbox").addClass("active");
  });

  $(".side-menu .common-layer .e-tit .close-btn").on("click", function () {
    $(this).parent().parent().removeClass("active");
  });

  // TABLE LAYER SCRIPT
  $(".side-menu .table-layer.pdf-comics .content .list li").on("click", function () {
    $(".side-menu .table-layer.pdf-comics .content .list li").removeClass("active");
    $(this).addClass("active");
  });

  // SEARCH LAYER SCRIPT
  $(".side-menu .search-layer .search-type button").on("click", function () {
    $(".side-menu .search-layer .search-type button").removeClass("active");
    $(this).addClass("active");
  });

  // NOTE LAYER SCRIPT
  $(".side-menu .note-layer .type button").on("click", function () {
    $(".side-menu .note-layer .type button").removeClass("active");
    $(this).addClass("active");
  });

  $(".side-menu .note-layer .note-content .util button").on("click", function () {
    $(".side-menu .note-layer .note-content .util button").removeClass("active");
    $(this).addClass("active");
  });

  $(".side-menu .note-layer .note-content .color-type button").on("click", function () {
    $(".side-menu .note-layer .note-content .color-type button").removeClass("active");
    $(this).addClass("active");
  });

  // CONTEXTMENU SCRIPT
  $(".contextmenu button.color").on("click", function () {
    $(this).toggleClass("active");
  });

  // COMMENT POPUP SCRIPT
  $(".comment-popup .move").on("click", function () {
    $(".comment-popup").hide();
  });

  // IMG ZOOM SCRIPT
  $(".img-zoom .close-btn").on("click", function () {
    $(".img-zoom").fadeOut("fast");
  });

  // MEMO POPUP SCRIPT
  $(".memo-popup .popup .row02 button.color").on("click", function () {
    const rgba2hex = (rgba) =>
      `#${rgba
        .match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+\.{0,1}\d*))?\)$/)
        .slice(1)
        .map((n, i) => (i === 3 ? Math.round(parseFloat(n) * 255) : parseFloat(n)).toString(16).padStart(2, "0").replace("NaN", ""))
        .join("")}`;
    var bgColor = rgba2hex($(this).css("backgroundColor"));

    $(".memo-popup .popup .row02 button.color").removeClass("active");
    $(this).addClass("active");

    $(".memo-popup .row03 .para-wrapper").css("background-color", bgColor + "66");
  });

  $(document).on("click", function (e) {
    if ($(e.target).is(".memo-popup")) {
      $(".memo-popup").fadeOut();
    }
  });

  $(".memo-popup .popup .close-btn").on("click", function () {
    $(".memo-popup").fadeOut();
  });

  // FINISHI POPUP SCRIPT
  $(document).on("click", function (e) {
    if ($(e.target).is(".finish-popup")) {
      $(".finish-popup").fadeOut();
    }
  });
  $(".finish-popup .close-btn").on("click", function () {
    $(".finish-popup").fadeOut();
  });
});
