
/**
 * 
 * wivicl UI Provider
 */
var UIProvider = UIProvider || (function(){
    //instance
    var SHARED = {};

    var Adapter = SHARED.Adapter = {
        tocParent: null,
        readnoteParent: null,
        searchParent: null,

        tocCallback: null,
        readnoteCallback: null,
        searchCallback: null,

        init(viewerType){
            ViewHolder.init(viewerType)
        },

        setToc: function(tocParentID,callback){
            Adapter.tocParent = document.getElementById(tocParentID)
            Adapter.tocCallback = callback
        },
        setSearch: function(searchParentID,callback){
            Adapter.searchParent = document.getElementById(searchParentID)
            Adapter.searchCallback = callback
        },

        setReadnote: function(readnoteID,callback){
            Adapter.readnoteParent = document.getElementById(readnoteID)
            Adapter.readnoteCallback = callback
        },

        updateToc: function(list){
            if (!Adapter.tocParent){
                throw 'Adapter.tocParent must not be null'
            }
            if (!list){
                Adapter.tocParent.innerHTML = ''
                Adapter.tocParent.appendChild(ViewHolder.getEmpty('toc'))
                return;
            }
            if (list.length == 0){
                Adapter.tocParent.innerHTML = ''
                Adapter.tocParent.appendChild(ViewHolder.getEmpty('toc'))
                return;
            }
            
            let mList = ViewHolder.getList('toc',list,Adapter.tocCallback)
            Adapter.tocParent.innerHTML = ''
            Adapter.tocParent.appendChild(mList)
            mList = null;
        },
        updateReadNote: function(list,orderSort,typeSort){
            if (!Adapter.readnoteParent){
                throw 'Adapter.readnoteParent must not be null'
            }
            if (!list){
                Adapter.readnoteParent.innerHTML = ''
                Adapter.readnoteParent.appendChild(ViewHolder.getEmpty('readnote'))
                return;
            }
            if (list.length == 0){
                Adapter.readnoteParent.innerHTML = ''
                Adapter.readnoteParent.appendChild(ViewHolder.getEmpty('readnote'))
                return;
            }
            let mList = ViewHolder.getList('readnote',list,Adapter.readnoteCallback)
            Adapter.readnoteParent.innerHTML = ''
            Adapter.readnoteParent.appendChild(mList)
            mList = null
        },
        updateSearch: function(list){
            if (!Adapter.searchParent){
               throw 'Adapter.searchParent must not be null'
            }
            if (!list){
                Adapter.searchParent.innerHTML = ''
                Adapter.searchParent.appendChild(ViewHolder.getEmpty('search'))
                return;
            }
            if (list.length == 0){
                Adapter.searchParent.innerHTML = ''
                Adapter.searchParent.appendChild(ViewHolder.getNotfound('search'))
                return;
            }
            
            let mList = ViewHolder.getList('search',list,Adapter.searchCallback)
            Adapter.searchParent.innerHTML = ''
            Adapter.searchParent.appendChild(mList)
            mList = null
        }
    }

    var MSG = SHARED.MSG = {
        lang: 'ko-kr',

        
    }
    
    var ViewHolder = SHARED.ViewHolder = {
        viewerType: 'epub',
        toc: {
            list : `<ul class="list"></ul>`,
            cell : `<li>
                        <span bdb-cell-attr="text" class="chapter"></span>
                        <span bdb-cell-attr="page" class="percent"></span>
                    </li>`,
            empty : `<div class="empty">
                        <svg width="73.123" height="65.883" viewBox="0 0 73.123 65.883">
                            <use href="#svg-speech-icon"></use>
                        </svg>
                        <span class="guide">목차가 없는 책입니다.</span>
                    </div>`,
        },
        readnote: {
            list : `<div>
                        <div class="util">
                            <button type="button" class="active">등록순</button>
                            <button type="button">목차순</button>
                        </div>
                        <div class="color-type">
                            <button type="button" class="all active">전체</button>
                            <button type="button" class="color lemon"></button>
                            <button type="button" class="color palegreen"></button>
                            <button type="button" class="color lavendar"></button>
                            <button type="button" class="color skyblue"></button>
                            <button type="button" class="color palepink"></button>
                            <button type="button"><img src="images/static/memo-popup-icon02.svg" alt="" /></button>
                        </div>
                        <div class="content">
                            <ul></ul>
                        </div>
                    </div>`,
            cell : `<li>
                        <img bdb-cell-attr="annotaion-icon" alt="" />
                        <div class="info">
                            <span bdb-cell-attr="chapter" class="chapter">제 1장 구체에서 추상으로 생각의 보폭을 키운다</span>
                            <p bdb-cell-attr="text">실제로는 제각기 다른 사과를 마치 동일한 것인 양 이미지화 하는 것과 같다. 그런 ‘가정하는’ 발상이 없다</p>
                            <p bdb-cell-attr="memo" class="memo"><span>가정하는 발상이란 것에 대한 글을 생각하게 보게된다. 이미지가 아니란 무엇인가. 실제로는 제각기 다른 사과를 마치 동일한 것인 양</span></p>
                            <div class="info-wrap">
                                <div class="per-date">
                                <span bdb-cell-attr="page" class="percent">12%</span>
                                <span bdb-cell-attr="date" class="date">2022. 03. 07 오전 08시 14분</span>
                                </div>
                                <div class="button">
                                    <button bdb-cell-attr="btn-edit" type="button"><img src="images/static/note-icon06.svg" alt="" /></button>
                                    <button bdb-cell-attr="btn-delete" type="button"><img src="images/static/note-icon05.svg" alt="" /></button>
                                </div>
                            </div>
                        </div>
                    </li>`,
            empty : `<div>
                        <svg width="73.123" height="65.883" viewBox="0 0 73.123 65.883">
                            <use href="#svg-speech-icon"></use>
                        </svg>
                        <span class="guide01">아직 작성하신 독서노트가 없습니다.</span>
                        <span class="guide02">자유롭게 기록을 남겨보세요.</span>
                    </div>`,
        },
        search: {
            list : `<div>
                        <div class="search-count">
                            <span bdb-cell-attr="search-count" class="count">검색결과 : <b>68</b>건</span>
                        </div>
                        <div class="search-result">
                            <ul></ul>
                        </div>
                    </div>`,
            cell : `<li>
                        <button type="button">
                            <span bdb-cell-attr="chapter" class="chapter">제 1장 구체에서 추상으로 생각의 보폭을 키운다</span>
                            <p bdb-cell-attr="text">장기화 국면에 접어들고 <b>백신</b> 접종률이 가파르게 상승하면서 일상으로의 복귀를 시도하는 나라가 조금씩 늘어났지만 아직</p>
                            <span bdb-cell-attr="page" class="percent">12%</span>
                        </button>
                    </li>`,
            empty : `<span class="guide">최소 2자 이상 입력해 주세요.</span>`,
            notfound: `<div>
                        <img src="images/static/search-icon02.svg" alt="" />
                        <span class="guide">검색결과가 없습니다.</span>
                    </div>`,
        },
        init: function(viewerType){
            if (viewerType){
                ViewHolder.viewerType = viewerType
            }
        },
        /**
         * 
         * @param {*} type ui type
         * @param {*} data 목차,리딩노트,검색 아이템
         * @param {*} callback 클릭콜백
         * @returns 
         */
        getCell: function(type,data,callback){
            if (typeof type != 'string'){
                return;
            }
            if (!ViewHolder[type].cell){
                return;
            }
            if (typeof ViewHolder[type].cell === 'string'){
                let template = document.createElement('template');
                template.innerHTML = ViewHolder[type].cell;
                ViewHolder[type].cell = template.content
                template = null
            }
            let result = ViewHolder[type].cell.cloneNode(true)

            //readnote
            if (type === 'readnote'){
                 //북마크
                if (data.type === 0){
                    // result.querySelector('[bdb-cell-attr="annotaion-icon"]').src = ""
                    if (result.querySelector('[bdb-cell-attr="chapter"]')){
                        result.querySelector('[bdb-cell-attr="chapter"]').textContext = data['title'] || data['chapter'] || ""
                    }
                    result.querySelector('[bdb-cell-attr="memo"]').remove()
                }
                //하이라이트
                else if (data.type === 1){
                    // result.querySelector('[bdb-cell-attr="annotaion-icon"]').src = ""
                    result.querySelector('[bdb-cell-attr="chapter"]').remove()
                    result.querySelector('[bdb-cell-attr="memo"]').remove()
                    
                }
                //메모
                else if (data.type === 2){
                    // result.querySelector('[bdb-cell-attr="annotaion-icon"]').src = ""
                    result.querySelector('[bdb-cell-attr="chapter"]').remove()
                    if (result.querySelector('[bdb-cell-attr="memo"]')){
                        result.querySelector('[bdb-cell-attr="memo"]').querySelector('span').textContext = ""
                    }
                }
            }
           

            if (result.querySelector('[bdb-cell-attr="text"]')){
                result.querySelector('[bdb-cell-attr="text"]').textContext = data['text'] || data['content']|| data['chapter']  || ""
            }
            if (result.querySelector('[bdb-cell-attr="page"]')){
                result.querySelector('[bdb-cell-attr="page"]').textContext = data['page'] || data['percent'] || ""
            }
            if (result.querySelector('[bdb-cell-attr="date"]')){
                result.querySelector('[bdb-cell-attr="date"]').textContext = data['date'] || ""
            }


            if (callback){
                result.firstChild.addEventListener('click',callback.bind(this,'select',type,data))

                if (result.querySelector('[bdb-cell-attr="btn-edit"]')){
                    result.querySelector('[bdb-cell-attr="btn-edit"]').addEventListener('click', function(event){
                        event.preventDefault()
                        callback(type,'edit',data,event)
                        event.stopPropagation()
                    })
                }
                if (result.querySelector('[bdb-cell-attr="btn-delete"]')){
                    result.querySelector('[bdb-cell-attr="btn-delete"]').addEventListener('click', function(event){
                        event.preventDefault()
                        callback(type,'delete',data,event)
                        event.stopPropagation()
                    })
                }
            }
            
            return result
        },
        getList: function(type,list,callback){
            if (typeof type != 'string'){
                return;
            }
            if (!ViewHolder[type].list){
                return;
            }
            if (typeof ViewHolder[type].list === 'string'){
                let template = document.createElement('template');
                template.innerHTML = ViewHolder[type].list;
                ViewHolder[type].list = template.content.firstChild
                template = null
            }
            let result = ViewHolder[type].list.cloneNode(true)
            if (result.querySelector('[bdb-cell-attr="search-count"]')){
                result.querySelector('[bdb-cell-attr="search-count"]').innerHTML = '검색결과 : <b>'+list.length+'</b>건'
            }
            result.style.height = "100%"
            for (let e of list){
                if (type === 'toc'){
                    result.appendChild(ViewHolder.getCell(type,e,callback))
                } else {
                    result.querySelector('ul').appendChild(ViewHolder.getCell(type,e,callback))
                }
            }

            return result
        },
        getEmpty: function(type){
            if (typeof type != 'string'){
                return;
            }
            if (!ViewHolder[type].empty){
                return;
            }

            if (typeof ViewHolder[type].empty === 'string'){
                let template = document.createElement('template');
                template.innerHTML = ViewHolder[type].empty;
                ViewHolder[type].empty = template.content.firstChild
                template = null
            }
            let result = ViewHolder[type].empty.cloneNode(true)
            return result
        },
        getNotfound: function(){
            if (typeof ViewHolder.search.notfound === 'string'){
                let template = document.createElement('template');
                template.innerHTML = ViewHolder.search.notfound
                ViewHolder.search.notfound = template.content.firstChild
                template = null
            }
            let result = ViewHolder.search.notfound.cloneNode(true)
            return result
        }
    }
    return SHARED;
})();