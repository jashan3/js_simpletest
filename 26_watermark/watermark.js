

/**
 * @author sjhan 221122
 * 워터마크 기능을 위한 class
 */
class WMResultData {
    constructor(id, bcd, sbcd, lcode, lucode, date, origin) {
        this.id = id;
        this.bcd = bcd;
        this.sbcd = sbcd;
        this.lcode = lcode;
        this.lucode = lucode;
        this.date = date;
        this.origin = origin;
    }
}
/**
 * 워터마크 파싱
 */
class WMManager {
    static BARCODE = "B";
    static SUB_BARCODE = "S";
    static DATE = "D";
    static LC = "LC";
    static LU = "LU";
    constructor() {

    }
    readFileData(file) {
        let self = this;
        let id = new Date().valueOf();
        return this.readFile(file)
            .then(data => Jimp.read(data))
            .then(image => {
                let text = self.extractText(image)
                return { text, image }
            })
            .then(result => {
                let textMap = self.parseData(result.text)
                if (textMap) {
                    let bcd = textMap.get(WMManager.BARCODE)
                    let sbcd = textMap.get(WMManager.SUB_BARCODE)
                    let date = textMap.get(WMManager.DATE)
                    let lcode = textMap.get(WMManager.LC)
                    let lucode = textMap.get(WMManager.LU)
                    let data = new WMResultData(id, bcd, sbcd, lcode, lucode, date, result);
                    return data;
                } else {
                    return null;
                }
            })
    }
    extractText(image) {
        var colorUnitIndex = 0;
        var charValue = 0;
        var w = image.getWidth();
        var h = image.getHeight();
        var idx = 0;
        var extractedText = '';
        var isAlpha = image._rgba;
        let self = this;
        for (var y = 0; y < h; y++) {
            for (var x = 0; x < w; x++) {
                var idx = image.bitmap.width * y + x << 2;
                var color = {
                    r: image.bitmap.data[idx + 0],
                    g: image.bitmap.data[idx + 1],
                    b: image.bitmap.data[idx + 2],
                    a: image.bitmap.data[idx + 3]
                }
                for (var n = 0; n < 3; n++) {
                    switch (colorUnitIndex % 3) {
                        case 0:
                            {
                                charValue = charValue * 2 + color.r % 2;
                            }
                            break;
                        case 1:
                            {
                                charValue = charValue * 2 + color.g % 2;
                            }
                            break;
                        case 2:
                            {
                                charValue = charValue * 2 + color.b % 2;
                            }
                            break;
                    }
                    colorUnitIndex++;
                    if (colorUnitIndex % 8 == 0) {
                        charValue = self.reverseBits(charValue);
                        if (charValue == 0) {
                            return extractedText;
                        }
                        extractedText += String.fromCharCode(charValue);
                    }
                }
            }
        }
        return extractedText;
    }
    reverseBits(n) {
        var result = 0;
        for (var i = 0; i < 8; i++) {
            result = result * 2 + n % 2;
            n = parseInt(n / 2);
        }
        return result;
    }
    readFile(file) {
        return new Promise((resolve, reject) => {
            let fr = new FileReader();
            fr.onload = x => resolve(fr.result);
            fr.onerrror = reject;
            fr.readAsDataURL(file) // or readAsText(file) to get raw content
        })
    }
    parseData(extractText) {
        if (typeof extractText !== 'string') {
            return null
        }
        if (extractText.length === 0) {
            return null
        }
        let eText = extractText.split("|")
        let result = new Map();
        for (let txt of eText) {
            let dText = txt.split("=")
            if (dText && dText.length == 2) {
                result.set(dText[0], dText[1])
            }
        }
        if (result.size > 0) {
            return result
        }
        return null;
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const wmManager = new WMManager();
    const dropzone = document.getElementById('drop_zone');
    const watermarkResult = document.getElementById('watermark-result');
    const fileINP = document.createElement('input')
    fileINP.type = 'file';
    fileINP.style.display = 'none'


    function CreateItem(data) {
        if (!(data instanceof WMResultData)) {
            return null
        }

        let wrap = document.createElement('div');
        wrap.id = "WMResultData_" + data.id;
        wrap.classList.add('watermark-result-item')

        let imTag = new Image();
        let span1 = document.createElement('span');
        let span2 = document.createElement('span');
        let span3 = document.createElement('span');
        let span4 = document.createElement('span');
        let span5 = document.createElement('span');
        data.origin.image.getBase64(Jimp.AUTO, (err, src) => {
            imTag.src = src
        })
        span1.textContent = data.bcd;
        span2.textContent = data.sbcd;
        span3.textContent = data.lcode;
        span4.textContent = data.lucode;
        span5.textContent = data.date;
        wrap.appendChild(imTag);
        wrap.appendChild(span1);
        wrap.appendChild(span2);
        wrap.appendChild(span3);
        wrap.appendChild(span4);
        wrap.appendChild(span5);
        return wrap
    }

    function CreateErrorItem() {
        let wrap = document.createElement('div');
        wrap.classList.add('watermark-result-item')
        let span1 = document.createElement('span');
        span1.textContent = "data 추출 실패";
        wrap.appendChild(span1);
        return wrap
    }

    const OnResult = function (data) {
        let wrap = CreateItem(data);
        if (!wrap) {
            wrap = CreateErrorItem();
        }
        watermarkResult.appendChild(wrap);
    }
    const OnError = function (err) {
        let wrap = CreateErrorItem();
        watermarkResult.appendChild(wrap);
    }

    dropzone.ondrop = function (ev) {
        ev.preventDefault();
        if (ev.dataTransfer.items) {
            [...ev.dataTransfer.items].forEach((item, i) => {
                if (item.kind === 'file') {
                    const file = item.getAsFile();
                    wmManager.readFileData(file).then(OnResult).catch(OnError)
                }
            });
        } else {
            [...ev.dataTransfer.files].forEach((file, i) => {
                wmManager.readFileData(file).then(OnResult).catch(OnError)
            });
        }
    }
    dropzone.ondragover = function (ev) {
        ev.target.classList.add('enter');
        ev.preventDefault();
    }
    dropzone.ondragleave = function (ev) {
        ev.target.classList.remove('enter');
        ev.preventDefault();
    }
    dropzone.onclick = function (ev) {
        fileINP.click();
        ev.preventDefault();
    }


    fileINP.onchange = function (e) {
        [...fileINP.files].forEach((file, i) => {
            wmManager.readFileData(file).then(OnResult).catch(OnError)
        });
    }
})