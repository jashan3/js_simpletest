

/**
 * string parse
 * last update 22051 20:00 sjhan
 */
const BDBParse = {
    encode(strText){
        return btoa(encodeURIComponent(strText))
    },
    decode(encodedText){
        if (BDBParse.isBase64(encodedText)){
            return decodeURIComponent(atob(encodedText))
        }
        return encodedText
    },
    isBase64(str){
        if (str ==='' || str.trim() ===''){ return false; }
        try {
            return btoa(atob(str)) == str;
        } catch (err) {
            return false;
        }
    }
}
/**
 * epub.vo.js
 * epub 기본 정보 데이터 
 * 
 */
class BDBEpubBaseData {

    constructor(bookInfo , info, baseInfo){
        if (info){
            /**
             * @property {Array} anchor 앵커 파일 정보
             */
            this.anchor = info.anchor;

            /**
             * @property {*} direction 뷰어 방향
             */
            this.direction = info.direction;

            /**
             * @property {*} icon 아이콘 url
             */
            this.icon = info.icon;

            /**
             * @property {*} items html epub 형식 정보
             */
            this.items = info.items;

            /**
             * @property {*} layout 레이아웃
             */
            this.layout = info.layout;

            /**
             * @property {*} orientation 회전여부
             */
            this.orientation = info.orientation;

            /**
             * @property {Array} smil 스마일(미디어) 정보
             */
            this.smil = info.smil;

            /**
             * @property {*} spread 스프레드 파일 여부
             */
            this.spread = info.spread;

            /**
             * @property {*} title 제목
             */
            this.title = info.title;

            /**
             * @property {Array} toc 목차 파일 정보
             */
            this.toc = info.toc;
            this.toc.forEach(e=>{
                e.pos = parseFloat(e.pos);
            })

            /**
             * @property {Array} type 타입
             */
            this.type = info.type;

            /**
             * @property {*} anchor 버전
             */
            this.ver = info.ver;
        }


        if (bookInfo){
            this.bcd = bookInfo.bcd;

            this.bcode = bookInfo.bcode;
            
            this.coverUrl = bookInfo.coverUrl;
            
            this.forcevscroll = bookInfo.forcevscroll;
            
            this.lcode = bookInfo.lcode;
            
            this.lucode = bookInfo.lucode;
            
            this.recApi = bookInfo.recApi;
            
            this.sbcd = bookInfo.sbcd;
            
            this.sharelink = bookInfo.sharelink;
            
            this.st = bookInfo.st;
            
            this.syncApi = bookInfo.syncApi;
            
            
        }

        if (baseInfo){
            this.baseUrl = baseInfo.baseUrl;
            this.token = baseInfo.token;
            this.writer = baseInfo.writer;
            this.publisher = baseInfo.publisher;
        }
    }
}


/**
 * 테마 객체
 */
 class ThemeColor {
    /**
     * @param {*} name 이름
     * @param {*} backColor 배경색
     * @param {*} textColor 텍스트 색깔.
     */
    constructor(name, backColor, textColor){
        this.name = name;
        this.backColor = backColor;
        this.textColor = textColor;

    }
    

    /**
     * 테마 리스트
     * @param {*} params 
     * @returns 
     */
    //  '','','','green','darkgray','black','origin'
    static getThemeList(params) {
        return [
            new ThemeColor("light","#FFFFFF","#000000"),
            new ThemeColor("smoke","#F0F0F0","#000000"),
            new ThemeColor("lightgray","#DDDDDD",'#000000'),
            new ThemeColor("mistyrose","#F6EEEA",'#1F1C1B'),
            new ThemeColor("lightyellow","#F9F5DA","#1F1C0C"),
            new ThemeColor("palegreen","#E4F2E1",'#171E16'),
            new ThemeColor("green","#477256",'#FFFFFF'),
            new ThemeColor("darkgray","#555555","#FFFFFF"),
            new ThemeColor("black","#000000","#FFFFFF"),
            new ThemeColor("origin",'#FFFFFF','inherit'),
            new ThemeColor("dark","#000000","#FFFFFF"),
        ]
    }

    /**
     * 색변경
     * @param {*} theme 
     * @param {*} themeList 
     * @param {*} currentStyleColor 
     */
     static changeColor( theme , themeList, currentStyleColor){
        let mTheme = theme
        let attrEle = document.querySelector(":root");
        let backgroundKey = '--epub-contents-background-color';
        let colorKey = '--epub-contents-text-color';
        if (currentStyleColor){
            mTheme = currentStyleColor
        }
        let find = themeList.find(e=>e.name === mTheme);
        if (find){
            attrEle.style.setProperty(backgroundKey, find.backColor);
            attrEle.style.setProperty(colorKey, find.textColor);
        }
    }
}

/**
 * 스타일 객체.
 */
class EpubStyle {
    /**
     * @param {*} key css 변수 키값 or attr 키값
     * @param {*} name 이름
     * @param {*} type ui type
     * @param {*} useOringinal 원본값을 기본값으로 하는지? 
     * @param {*} defaultValue 기본값 
     * @param {*} currentValue 현재값
     * @param {*} styleList 값리스트
     */
    constructor(key,name,type,useOringinal,className,defaultValue,currentValue,styleList, styleListName){
        this.key = key;
        this.name = name;
        this.type = type;
        this.useOringinal = useOringinal
        this.className = className
        this.defaultValue = defaultValue;
        this.currentValue = currentValue;
        this.styleList = styleList;
        this.styleListName = styleListName;
    }
    /**
     * 스타일 list 
     * @returns 
     */
    static getStyleData(isMobile){
        return {
            //폰트
            fontFamily: new EpubStyle('--epub-contents-font-family','글꼴','select',false,
                null,
                'KoPubWorldDotum WF',
                'KoPubWorldDotum WF',
                [
                    'KoPubWorldDotum WF','KoPubWorldBatang WF','NanumMyeongjo','NanumGothic','NanumSquare'
                ],
                [
                    'KOPUB 돋움','KOPUB 바탕','나눔명조','나눔고딕','나눔 스퀘어'
                ]),
            //글자크기
            fontSize: new EpubStyle('--epub-contents-font-size','글자크기','count',false,
                null,
                (isMobile)?'130%':'120%',
                (isMobile)?'130%':'120%',
                [
                    '90%','100%','110%','120%','130%','140%','150%','160%','170%','180%',
                    '190%','200%','210%','220%','230%','240%','250%','260%','270%','280%'
                ]),
            //줄간격
            lineHeight: new EpubStyle('--epub-contents-line-height','줄간격','count',false,
                null,
                (isMobile)?'160%':'160%',
                (isMobile)?'160%':'160%',
                [
                    '110%','120%','130%','140%','150%','160%','170%','180%','190%','200%'
                ]),
            //좌우 여백설정
            lrMargin: new EpubStyle('--epub-contents-left-right-margin','여백설정','count',false,
                null,
                '0px',
                '0px',
                [
                    '0px','5%','10%','15%','20%'
                ]),
            // //상하 여백
            // tbMargin: new EpubStyle('--epub-contents-top-bottom-margin','여백설정','count',false,
            //     null,
            //     '0px',
            //     '0px',
            //     [
            //         '0px','5%','10%','15%','20%'
            //     ]),
            //들여쓰기
            indent: new EpubStyle('--epub-contents-text-indent','들여쓰기','count',true,
                'idt',
                'default',
                'default',
                [
                    'default','0em','1em','2em','3em','4em','5em'
                ]),
            //문단간격
            bottomMargin: new EpubStyle('--epub-contents-margin-bottom','문단간격','count',true,
                'pm',
                'default',
                'default',
                [
                    'default','0%','2%','4%','6%','8%','10%'
                ]),
            //문단정렬
            textAlign: new EpubStyle('--epub-contents-text-align','문단정렬','select',true,
                'txtAlign',
                'default',
                'default',
                [
                    'default','left','center','right','jutify',
                ]),
            //컬러 스키마
            scheme: new EpubStyle('color-scheme','컬러 테마','select',false,
                null,
                0,
                0,
                [
                    0,1,2
                ]),
            //테마
            theme: new EpubStyle('data-theme','테마','select',false,
                null,
                'origin',
                'origin',
                [
                    'origin','light','smoke','lightgray','mistyrose','lightyellow','palegreen','green','darkgray','black',
                ]),
        }
    }

    /**
     * comic, pdf용 데이터
     * @returns 
     */
    static getStyleData2(){
        return {
            //컬러 스키마
            scheme: new EpubStyle('color-scheme','컬러 테마','select',false,
                null,
                0,
                0,
                [
                    0,1,2
                ]),
            //테마
            theme: new EpubStyle('data-theme','테마','select',false,
                null,
                'origin',
                'origin',
                [
                    'origin','light','smoke','lightgray','mistyrose','lightyellow','palegreen','green','darkgray','black',
                ]),
        }
    }

    /**
     * 기본값의 index를 리턴
     * @returns 
     */
    getDefaultValueIndex() {
        return this.styleList.indexOf(this.defaultValue);
    }
    /**
     * 현재값의 index를 리턴
     * @returns 
     */
    getCurrentIndex(){
        return this.styleList.indexOf(this.currentValue);
    }
    /**
     * 현재값을 세팅
     * @returns 
     */
    setCurrentValue(value){
        this.currentValue = value;
    }

    /**
     * 현재값을 list에서 index로 찾아 세팅
     * @returns 
     */
    setCurrentValueByIndex(value){
        this.currentValue = this.styleList[value];
    }

    resetDefault(){
        this.currentValue = this.defaultValue
    }
    
    /**
     * 현재값을 다음 아이템으로 세팅
     * @returns 
     */
    nextItem(){
        let currentIdx = this.getCurrentIndex();
        if (currentIdx >= 0 && this.styleList.length-1>currentIdx){
            let item = this.styleList[currentIdx+1];
            if (item){
                this.currentValue = item;
                return true;
            }
        }
        return false;
    }

    /**
     * 현재값을 이전 아이템으로 세팅
     * @returns 
     */
    prevItem(){
        let currentIdx = this.getCurrentIndex();
        if (currentIdx >= 0 ){
            let item = this.styleList[currentIdx-1];
            if (item){
                this.currentValue = item;
                return true;
            }
        }
        return false;
    }


}

/**
 * 저장관련 class
 */
class BookStorage {
    constructor(){
        this.clientKey = 'wivicle'
        this.codeKey = null
        this.gloablKey = null
        this.bcd = null
        this.sbcd = null
        this.adaptGlobal = false
        this.isMobile = false
    }

    init(bcd,sbcd,sellerCode,userCode,filetype,isMobile){
        this.codeKey = this.clientKey;
        if (sellerCode){
            this.codeKey += '-'+sellerCode;
        }  
        if (userCode){
            this.codeKey += '-'+userCode;
        }
        if (bcd){
            this.codeKey += '-'+bcd;
        }  
        if (sbcd){
            this.codeKey += '-'+sbcd;
        }
        if (filetype){
            this.codeKey += '-'+filetype
        }
        if (isMobile){
            this.codeKey += '-mobile'
        }
        this.gloablKey = this.codeKey += '-adaptall'
    }

    load(){
		let matches = window.localStorage.getItem(this.adaptGlobal?this.gloablKey:this.codeKey);
        if (matches){
            return JSON.parse(decodeURIComponent(matches));
        }
        return null;
    }
    /**
     * 저장정책
     
        키생성방법
            기관구별자 - sellerCode - userCode - 바코드 - 서브바코드-파일타입(epub,pdf,zip)-모바일여부(옵셔널)
            {cpcode}-{lcode}-{lucode}-{bcd}-{sbcd}-{fileType}-{optional mobile}

        테마 tm
            ['origin','light','smoke','lightgray','mistyrose','lightyellow','palegreen','green','darkgray','black', ]

        색 스키마 sm
            [ 0,1,2 ]

        뷰모드 vm

        터치영역모드 ta

        key, object를 -> JSON.stringify -> encodeURIComponent -> window.localStorage에 저장,불러오기
     * @param {*} obj 
     * @param {*} isReset 
     */
    save( obj , isReset){
        let saveGlobalKey = null
        if (this.isMobile){
            saveGlobalKey = this.clientKey+'-mobile-adaptall'
        } else {
            saveGlobalKey = this.clientKey+'-adaptall'
        }

        let matches = window.localStorage.getItem(this.gloablKey)
        let item = null
        let findObj = null
        if (matches){
            item = JSON.parse(decodeURIComponent(matches))
            for (let d of Object.keys(item)){
                findObj = obj[d]
                if (findObj){
                    
                } else {
                    obj[d] = findObj
                }
            }
        }

        
        
        window.localStorage.setItem(saveGlobalKey , this.adaptGlobal);

        if (isReset){
            let key = encodeURIComponent(this.codeKey);
            let value = encodeURIComponent(JSON.stringify(obj));
            if (key && value){
                window.localStorage.setItem(key , value);
            }
            let key2 = encodeURIComponent(this.gloablKey);
            let value2 = encodeURIComponent(JSON.stringify(obj));
            if (key2 && value2){
                window.localStorage.setItem(key2 , value2);
            }
        } else {
            let key = encodeURIComponent(this.adaptGlobal?this.gloablKey:this.codeKey);
            let value = encodeURIComponent(JSON.stringify(obj));
            if (key && value){
                window.localStorage.setItem(key , value);
            }
        }

        matches = null
        item = null
        findObj = null
    }
}

/**
 * 북마크 객체.
 */
class Annotation {

    /**
     * @param {*} chapterName 목차 제목
     * @param {*} text 텍스트
     * @param {*} fileNo 파일 번호
     * @param {*} percent 현재 퍼센트 (epub only)
     * @param {*} cfi 현재 cfi (epub only)
     * @param {*} selector 현재 selector (epub only)
     */
    constructor( chapterName, text, fileNo, percent, cfi, selector) {
        this.id = this.uuidv4(); 
        this.chapterName = chapterName; 
        this.text = text; 
        this.fileNo = fileNo;
        this.date =  new Date().current();
        this.percent = percent;  
        this.cfi = cfi;
        this.serverId = null;
        this.selector = selector;
        this.type = 0;
        this.memo = null
    }

    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
          (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }

    /**
     * 알라딘
     * chapterName: "김금희_우리는 페퍼로니에서 왔어"
     * createDate: "2022-03-17T07:11:16.000Z"
     * ebook_id: "p:eq(106)"
     * file: ""
     * id: 131
     * last_time: ""
     * page: 23
     * percent: 25.357142857142847
     * text: "지지하거나 누구를 밀려는 것이 아니라 정책 수립에 청년들 목소리가 반영되어야 한다는 입장이"
     * version: "1.0"
     * @param {*} res 
    */
    // chapterNo: 23
    // chapterTitle: "타이틀"
    // ebookSeq: 13
    // memo: "그들로선 유럽에서 품질이 더 좋고 더 싼 제조업 생산물들을 수입할 수 있는데, 이와 비교할 수 없이 열등한 ‘양키’ 물건을 사야 할 이"
    // pagePercent: "13.536363636363655"
    // regDt: "2022-05-04 14:50:04"
    // seq: 20
    // startPath: "p:eq(67)"
    // statusCd: "S"
    static responseToAnnotation(res){
        console.log('## responseToAnnotation',res)
        let mChapterName = res['chapterTitle']
        let mText = res['memo']
        let mPage = res['page']
        let mPercent = res['pagePercent']
        let mCfi= res['startPath']
        let mSelecter = res['startPath']
        mText = BDBParse.decode(mText)
        mChapterName = BDBParse.decode(mChapterName)
        let anno = new Annotation(mChapterName,mText,mPage,mPercent,mCfi,mSelecter)
        anno.serverId = res['seq']
        anno.fileNo = res['chapterNo']
        anno.date = res['regDt']
        return anno 
    }

    static responseToAnnotationList(res){
        let result = []

        res.forEach(e=>{
            
            result.push(Annotation.responseToAnnotation(e))
        })

        return result
    }


}


/**
 * 최근 검색어 저장소 객체
 */
class SearchStorage {

    /*
        crud callback
    */
    constructor( callback ){
        this.searchSaveKey = 'wevicl#search://'
        this.saveCallback = callback
    }

    /**
     * 검색 키워드를 받으면 저장시키거나 삭제시킨다.
     * @param {*} keyword 
     */
    save(keyword){
        let key = this._getKey()
        let loadValue = this.load()
        
        //없는 경우 새로 넣음
        if (!loadValue){
            loadValue = new Map()
        } 
        
        loadValue.set(keyword,{
            'date': new Date(),
            'keyword': keyword
        })

        let v = JSON.stringify(Object.fromEntries(loadValue))
        let value = encodeURIComponent(v);
        window.localStorage.setItem(key , value);

        if (this.saveCallback){
            this.saveCallback('save', this.load())
        }


        key = null
        loadValue = null
        v = null
        value = null
    }

    /**
     * 현재 저장된 검색 데이터
     * @param {*} keyword 
     */
    load(){
		let matches = this._get()
        if (matches){
            return new Map(Object.entries(JSON.parse(decodeURIComponent(matches))));
        } else {
            return null
        }
    }

    /**
     * 검색 키워드를 받으면 삭제시킨다.
     * @param {*} keyword 
     */
    delete(keyword){
        let key = this._getKey()
        let loadValue = this.load()
        let v = null
        let value = null
        if (loadValue){
            if (loadValue.has(keyword)){
                loadValue.delete(keyword)
                v = JSON.stringify(Object.fromEntries(loadValue))
                value = encodeURIComponent(v);
                window.localStorage.setItem(key , value);
                if (this.saveCallback){
                    this.saveCallback('delete', this.load())
                }
            }
        }
        key = null
        loadValue = null
        v = null
        value = null
    }

    /**
     * 검색 키워드관련 데이터를 초기화
     * @param {*} keyword 
     */
    deleteAll(){
        let key = this._getKey()
        let loadValue = this.load()
        if (loadValue){
           window.localStorage.removeItem(key)
            if (this.saveCallback){
                this.saveCallback('deleteAll', this.load())
            }
        }

        key = null
        loadValue = null
    }
    _get(){
        return window.localStorage.getItem(this.searchSaveKey);
    }
    _getKey(){
        return this.searchSaveKey
    }
}
