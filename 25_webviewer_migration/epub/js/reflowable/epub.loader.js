/*
Runa Web ePub engine loader V1.0.0.1001
www.bookswage.com
(c) 2004-2021 by Crazy Eric. All rights reserved.
last update 221103 10:12 sjhan
*/

String.prototype.insertAt = function(index,str){
	return this.slice(0,index) + str + this.slice(index)
}

/// <reference path="./epub.log.js" />
/// <reference path="../contentHelper.js" />
/// <reference path="./jquery.waitforimages.min.js" />

/**
 * ePub Engine Loader
 */
var ePubEngineLoaderJS = ePubEngineLoaderJS || (function(ContentHelperJS, undefined) {
	var ePubEngineLoaderJs;

    // Native ePubEngineLoaderJs from window (Browser)
    if (typeof window !== 'undefined' && window.ePubEngineLoaderJs) {
        ePubEngineLoaderJs = window.ePubEngineLoaderJs;
    }

	if (typeof self !== 'undefined' && self.ePubEngineLoaderJs) {
        ePubEngineLoaderJs = self.ePubEngineLoaderJs;
    }

	// Native ePubEngineLoaderJs from worker
    if (typeof globalThis !== 'undefined' && globalThis.ePubEngineLoaderJs) {
        ePubEngineLoaderJs = globalThis.ePubEngineLoaderJs;
    }

    // Native (experimental IE 11) ePubEngineLoaderJs from window (Browser)
    if (!ePubEngineLoaderJs && typeof window !== 'undefined' && window.msePubEngineLoaderJs) {
        ePubEngineLoaderJs = window.msePubEngineLoaderJs;
    }

	var Loader = {};
	/**
	 * 로딩할 최대 챕터 수
	 */
	var _maxSpine = 3;
	/**
	 * 로딩 완료 여부
	 */
    var readyContent = false;
	/**
	 * 콘텐츠 내용이 들어갈 상위 태그의 아이디.
	 */
	var contentConainerId = '';
	var spineAllItems = new Array();
	/**
	 * 순서대로 로딩할 챕터
	 */
	var spineItems = new Array();
	/**
	 * 기타 다른 챕터
	 */
	var spineNoItems = new Array();
	
	/**
	 * 파일번호가 추가된 spine data
	 */
	var spineWithFileItems = new Array();

	/**
     * 현재 보고 있는 spine
     */
	var currentSpine = -1;
	/**
	 * 전체 파일 수
	 */
	var totalSpine = 0;
 
	var totalPage = 0;

	/**
	 * 콘텐츠 기본 경로
	 */
	var contentBaseUrl = '';
	 /**
	  * 인증 토큰
	  */
	var authToken = '';

	/**
	 * 오디오 콜백 함수.
	 */
	 var callbackAudioFunc = null;
	 /**
	  * 오디오, 비디오 element 
	  */
	 var mediaItems = new Array();

	 /**
	  * 마지막 chopter 정보
	  */
	 var lastChaptorItem = null;

	/**
	 * Style background color Map
	*/
	const styleSheetRuleBackColorMap = new Map();

	/**
	 * Style margin map
	 */
	const styleSheetRuleMarginMap = new Map();

	/**
	* Style List
	*/
	const styleSheetBackColorList = new Array();

	 var tocs = null

	var Data = Loader.Data = {
		/**
		 * 모든 spine item
		 * @param {} idx 
		 * @returns 
		 */
		getOriginSpineItem(idx) {
			if(idx < spineAllItems.length) {
				return spineAllItems[idx];
			}
			return null;
		},
		/**
		 * 렌더링이 될 전체 파일 수
		 * @returns 렌더링이 될 전체 파일 수
		 */
		getTotalSpine: function() {
			return totalSpine;
		},
		getSpineItems:function(){
			return spineItems;
		},
		/**
		 * 해당 파일의 정보를 리턴한다.
		 * @param {number} idx 파일번호
		 * @returns 
		 */
		getSpineItem: function(idx) {
			if(idx < spineItems.length) {
				return spineItems[idx];
			}
			return null;
		},
		/**
		 * 왼쪽 기준으로 현재 로딩된 파일 번호.
		 * @returns 현재 로딩된 파일 번호
		 */
		getCurrentSpine: function() {
			return currentSpine;
		},
		/**
		 * 오디오, 비디오 리스트
		 * @returns Array
		 */
		getMediaList: function() {
			return mediaItems;
		},
				/**
		 * 왼쪽 기준으로 현재 로딩된 파일 번호.
		 * @returns 현재 로딩된 파일 번호
		 */
		// getLeftSpine: function() {
		// 	return leftSpine;
		// },
		// getLefFrame: function() {
		// 	return _ele_left_Frame;
		// },
		// getRightFrame: function() {
		// 	return _ele_right_Frame;
		// },
		getBaseUrl: function() {
			return contentBaseUrl;
		},
		/**
		 * 마지막 챕터 아이템.
		 * @returns 
		 */
		getLastChaptorItem:function(){
			return lastChaptorItem;
		},
		/**
		 * 파일을 로딩 하기 위한 기본 데이터 준비.
		 * @param {object} items path, fileno, option, linear, bgcolor, percent
		 * @param {string} baseUrl content's base url
		 * @param {string} token 인증을 위한 토큰
		 * @param {string} containerId 콘텐츠를 넣을 부모 엘리먼트 아이디
		 * @param {function(tpe, element)} audioCallback 오디오 콜백 함수, type: timeupdate, onplaying, onended, onpause
		 */
		init: function(items, baseUrl, token, toc, containerId, audioCallback) {
			return new Promise(function(resolve,reject){
				contentBaseUrl = baseUrl;
				authToken = token;
				contentConainerId = containerId;
				totalSpine = 0;
				callbackAudioFunc = audioCallback;
				tocs = toc
				
				for(var item of items) {
					/**
					 * 암호화된 콘텐츠
					 */
					item.contents = '';
					/**
					 * 데이터를 원격지에서 가져왔는지 여부.
					 */
					item.isLoaded = false;
	
					if(item.linear == 1) {
						item.subIndex = spineNoItems.length;
						spineNoItems.push(item);
					}
					else {
						item.subIndex = spineItems.length;
						spineItems.push(item);
						totalSpine++;
					}
	
					spineAllItems.push(item);
				}
				//마지막 아이템 
				var chapItems = Data.getSpineItems();
				if (chapItems&&chapItems.length>0){
					lastChaptorItem = {
						index:chapItems.length-1 , 
						chaptor:chapItems[chapItems.length-1]
					};
				}
				Obfucator.init();

				Data.loadCss2().then(rs=>{
					StyleSheet.extractAttrInStyleSheet()
					styleSheetRuleBackColorMap.forEach(e=>{
						let attr = e.cssText
						let cs = e.selectorText
						let ca = StyleSheet.getCssAttrBackground(attr)
						if (ca){
							let d = Object.assign({'class': cs},ca)
							if (d){
								styleSheetBackColorList.push(d)
							}
						}
					})
					resolve();
				}).catch(err=>{
					reject(err)
				})
			})
		},
		reset: function() {
			spineItems = undefined;
			contentBaseUrl = '';
		},
		/**
		 * 콘텐츠 기본 스타일을 로딩한다.
		 */
		loadCss: function() {
			var cssUrl = spineItems[0].path;
	
			cssUrl = cssUrl.substr(0, cssUrl.lastIndexOf('/'));
			cssUrl = cssUrl.replace('{CONTENT_DOMAIN}', contentBaseUrl);
	
			if(document.createStyleSheet) {
				document.createStyleSheet(cssUrl+"/style.css");
			}
			else {
				$("head").append($("<link rel=\"stylesheet\" href=\"" + cssUrl+"/style.css\" />"));
			}
		},
		loadCss2: function(){
			var cssUrl = spineItems[0].path;
			cssUrl = cssUrl.substr(0, cssUrl.lastIndexOf('/'));
			cssUrl = cssUrl.replace('{CONTENT_DOMAIN}', contentBaseUrl);
			return Util.LoadCssFile(cssUrl+"/style.css")
		},
		HlsObj: [],
		HlsConfig: {
			debug: true,
			autoStartLoad: true,
			maxBufferLength: 30,
			maxBufferSize: 7 * 1000 * 1000,
			liveSyncDurationCount: 1,
			liveMaxLatencyDurationCount: 5,
			enableWorker: true,
			lowLatencyMode: true,
			enableSoftwareAES: true,
			manifestLoadingTimeOut: 4000,
			manifestLoadingMaxRetryTimeout: 100,
			manifestLoadingMaxRetry: 10,
			manifestLoadingRetryDelay: 100,
			levelLoadingTimeOut: 4000,
			levelLoadingMaxRetry: 10,
			levelLoadingRetryDelay: 100,
			fragLoadingTimeOut: 4000,
			fragLoadingMaxRetry: 10,
			fragLoadingRetryDelay: 100,
			fpsDroppedMonitoringPeriod: 100,
			fpsDroppedMonitoringThreshold: 0.2,
			maxFragLookUpTolerance: 0.04,
			forceKeyFrameOnDiscontinuity: true,
			appendErrorMaxRetry: 3,
			maxStarvationDelay: 3,
			capLevelToPlayerSize: true,
			enableWebVTT: false,
			maxMaxBufferLength: 60},
		getNewHlsObjects: function() {
			return this.HlsObj;
		},
		removeNewHlsObjects: function() {
			this.HlsObj.splice(0, this.HlsObj.length);
		},
		loadVideoHls: function(ele) {
			return new Promise(function(resolve, reject) {
				$(ele).attr("preload", 'none');
				$(ele).removeAttr("controls");
				$(ele).removeAttr("autoplay");
				$(ele).attr('controls','');
				$(ele).attr('allowfullscreen','');
				$(ele).attr('muted','');

				$(ele).attr('ruaIdx', mediaItems.length);

				ele.onloadedmetadata = function() {
					this.muted = true;
				};
					
				mediaItems.push(ele);

				if (Hls.isSupported()) {
					var src = ele.src;

					if(src == undefined) {
						src = ele.currentSrc;
					}
					else if(src == '') {
						src = $(ele).find('Source:first').attr('src');
						$(ele).find('Source:first').attr('type', 'application/x-mpegURL');
					}

					var self = ele;
					
					if(src && src.indexOf('blob:') == -1) {

						var hls = new Hls(Data.HlsConfig);
					
						this.src = '';

						hls.attachMedia(ele);

						hls.on(Hls.Events.MEDIA_ATTACHED, function () {
							ePubEngineLogJS.L.log("hls.js are now bound together !");
							hls.loadSource(src);

							hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {
								ePubEngineLogJS.L.log('video MANIFEST_PARSED manifest loaded, found ' + data.levels.length + ' quality level');

								$(ele).hide();
								$(ele).show();

								self.onplaying = function () {
					
									var idx = ele.getAttribute('ruaIdx');
				
									// 재생을 누른 녀석 말고는 전부 정지 한다.
									for(var a of mediaItems ) {
										var cIdx = a.getAttribute('ruaIdx');
				
										if(cIdx !== idx) {
											a.pause();
										}
									}
								};


							});
						});

						hls.once(Hls.Events.FRAG_LOADED, function(event, data) {
							ePubEngineLogJS.L.log('video FRAG_LOADED ,' + event + ', ' + data);
						});
						
						hls.on(Hls.Events.ERROR, function (event, data) {
							if (data.fatal) {
								switch (data.type) {
								case Hls.ErrorTypes.NETWORK_ERROR:
									// try to recover network error
									ePubEngineLogJS.W.log('fatal network error encountered, try to recover');
									hls.startLoad();
									break;
								case Hls.ErrorTypes.MEDIA_ERROR:
									ePubEngineLogJS.W.log('fatal media error encountered, try to recover');
									hls.recoverMediaError();
									break;
								default:
									// cannot recover
									ePubEngineLogJS.W.log('fatal media error encountered, cant recover');
									hls.destroy();
									reject(0);
									break;
								}
							}
						});
						resolve(0);

						Data.HlsObj.push(hls);
					}
				}
				else if (ele.canPlayType('application/vnd.apple.mpegurl')) {
					//alert(' not support');
					ele.onplaying = function () {
					
						var idx = ele.getAttribute('ruaIdx');
	
						// 재생을 누른 녀석 말고는 전부 정지 한다.
						for(var a of mediaItems ) {
							var cIdx = a.getAttribute('ruaIdx');
	
							if(cIdx !== idx) {
								a.pause();
							}
						}
					};

					resolve(0);
				}
				
			});
		},
		// bindHlsEvent:function(ele,metaData){
		// 	// console.log(ele.src);
		// 	var hls = new Hls();
		// 	hls.attachMedia(ele);
		// 	// console.log('bindHlsEvent',ele.src);
		// 	hls.on(Hls.Events.MEDIA_ATTACHED, function () {
		// 		console.log('bindHlsEvent','MEDIA_ATTACHED');
		// 		hls.loadSource(metaData.src);
		// 		hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {
		// 			console.log('bindHlsEvent','MANIFEST_PARSED');
		// 			ele.pause();

		// 		})
		// 	})
		// },
		$audioTag: null,
		loadAudioHls: function(ele) {
			Data.$audioTag = ele;
			return new Promise(function(resolve, reject) {
				$('.bdb_custom_audio_wrapper').on('click',function(e){
					console.log(e);
				});
				$(ele).attr("preload", 'none');
				$(ele).removeAttr("autoplay");

				$(ele).attr('ruaIdx', mediaItems.length);
					
				mediaItems.push(ele);
				
				if (Hls.isSupported()) {
					var src = ele.src;

					if(src == undefined) {
						src = ele.currentSrc;
					}
					else if(src == '') {
						src = $(ele).find('Source:first').attr('src');
						$(ele).find('Source:first').attr('type', 'application/x-mpegURL');
					}

					var self = ele;

					if(src && src.indexOf('blob:') == -1) {
						// {debug: true, enableWorker: true, liveBackBufferLength: 900, maxMaxBufferLength: 10}
						var hls = new Hls();
					
						this.src = '';

						hls.attachMedia(ele);

						hls.on(Hls.Events.MEDIA_ATTACHED, function () {
							ePubEngineLogJS.L.log("hls.js are now bound together !");
							hls.loadSource(src);

							hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {
								ePubEngineLogJS.L.log('audio MANIFEST_PARSED manifest loaded, found ' + data.levels.length + ' quality level');

								self.pause();
								self.ontimeupdate = function () {
									var idx = self.getAttribute('ruaIdx');
									
									// ePubEngineLogJS.L.log('audio ['+idx+'], timeupdate : '+ this.currentTime +' / ' + this.duration);
			
									if (callbackAudioFunc) {
										callbackAudioFunc('timeupdate', self);
									}
								};
								self.onplaying = function () {
									
									var idx = self.getAttribute('ruaIdx');
			
									// ePubEngineLogJS.L.log('audio ['+idx+'], onplaying : '+ this.currentTime +' / ' + this.duration);
			
									if (callbackAudioFunc) {
										callbackAudioFunc('onplaying', self);
									}
								};
								self.onended = function () {
									var idx = self.getAttribute('ruaIdx');
									
									// ePubEngineLogJS.L.log('audio ['+idx+'], onended : '+ this.currentTime +' / ' + this.duration);
			
									if (self.currentTime >= self.duration && self.paused){
										self.currentTime = 0;
									}
										
			
									if (callbackAudioFunc) {
										callbackAudioFunc('onended', self);
									}
								};
								self.onpause = function () {
									var idx = self.getAttribute('ruaIdx');
									
									// ePubEngineLogJS.L.log('audio ['+idx+'], onpause : '+ this.currentTime +' / ' + this.duration);
			
									if (callbackAudioFunc) {
										callbackAudioFunc('onpause', self);
									}
								};
							});
						});

						hls.once(Hls.Events.FRAG_LOADED, function(event, data) {
							ePubEngineLogJS.L.log('audio FRAG_LOADED ,' + event + ', ' + data);
						});

						hls.on(Hls.Events.ERROR, function (event, data) {
							if (data.fatal) {
								switch (data.type) {
								case Hls.ErrorTypes.NETWORK_ERROR:
									// try to recover network error
									ePubEngineLogJS.W.log('fatal network error encountered, try to recover');
									hls.startLoad();
									break;
								case Hls.ErrorTypes.MEDIA_ERROR:
									ePubEngineLogJS.W.log('fatal media error encountered, try to recover');
									hls.recoverMediaError();
									break;
								default:
									// cannot recover
									ePubEngineLogJS.W.log('fatal media error encountered, cant recover');
									hls.destroy();
									reject(0);
									break;
								}
							}
							
						});

						resolve(0);

						Data.HlsObj.push(hls);
					}
				}
				else if (ele.canPlayType('application/vnd.apple.mpegurl')) {
					//alert(' not support');
					ele.pause();
					ele.ontimeupdate = function () {
						var idx = ele.getAttribute('ruaIdx');
						
						// ePubEngineLogJS.L.log('audio ['+idx+'], timeupdate : '+ ele.currentTime +' / ' + ele.duration);

						if (callbackAudioFunc) {
							callbackAudioFunc('timeupdate', ele);
						}
					};
					ele.onplaying = function () {
						
						var idx = ele.getAttribute('ruaIdx');

						// ePubEngineLogJS.L.log('audio ['+idx+'], onplaying : '+ ele.currentTime +' / ' + ele.duration);

						if (callbackAudioFunc) {
							callbackAudioFunc('onplaying', ele);
						}
					};
					ele.onended = function () {
						var idx = ele.getAttribute('ruaIdx');
						
						// ePubEngineLogJS.L.log('audio ['+idx+'], onended : '+ ele.currentTime +' / ' + ele.duration);

						if (ele.currentTime >= ele.duration && ele.paused){
							ele.currentTime = 0;
						}
							

						if (callbackAudioFunc) {
							callbackAudioFunc('onended', ele);
						}
					};
					ele.onpause = function () {
						var idx = ele.getAttribute('ruaIdx');
						
						// ePubEngineLogJS.L.log('audio ['+idx+'], onpause : '+ ele.currentTime +' / ' + ele.duration);

						if (callbackAudioFunc) {
							callbackAudioFunc('onpause', ele);
						}
					};

					resolve(0);
				}
			});
		},
		/**
		 * 난독화전 element tag에 cfi를 넣는다.
		 * @param {*} data 
		 */
		addCfiAttribute: function(data){
			let de_temp = '<li>'+data +'</li>'
			let de_temp_element = de_temp.createElementFromHTML()
			let eleList = de_temp_element.getElementsByTagName("*");
			let _path = null
		
			let count = 0
			for (let nd of eleList){
				count ++;
				_path = ePubEngineCoreJS.Position._encodeCFI( de_temp_element, nd, nd.textContent.length, "")
				nd.setAttribute('cfi',_path)
				nd.setAttribute('tag-total',eleList.length)
				nd.setAttribute('tag-current',count)
			}
			_path = null
			_path2 = null
			de_temp = null
			eleList = null

			return de_temp_element.innerHTML
		},
		/**
		 * 콘텐츠를 추가한다.
		 * @param {number} curChapterNum 챕터 순번
		 * @param {string} data 챕터 데이터
		 * @param {string} bgolor 챕터 배경색
		 * @returns 
		 */
		addedContents: function(curChapterNum, data, bgolor) {
			return new Promise( function(resolve, reject) {
				const chapter = $("#bdb-chapter"+curChapterNum);
				

				chapter.removeAttr('style');
				chapter.empty();

				//sjhan 완벽하게 체크를못해서 일단 주석
				const parsedData = Data.addCfiAttribute(data)
				const de = Obfucator.obfucator(parsedData);
			
				// const de = Obfucator.obfucator(data);

				const findVideoIndex = Util.findStrIndex('<video' , '</video>' , de);
				// var findAudioCount = Util.occurrences(de , '\<audio ');
				
				//video
				if (findVideoIndex){
					var videoItem = Util.videoObjMake(de);
					var begin = findVideoIndex.begin;
					var end = findVideoIndex.end;
					var findVideoElement = de.slice(begin , end).replaceAll('{CONTENT_OBFUCATE}',' ');
					var videoNode = Util.createElementFromHTML(findVideoElement);
					var ff2 = Util.replaceRange(de , begin , end , '');
					var ff3 = ff2.insertAt(findVideoIndex.begin,'\<iframe class="bdb_custom_iframe" scrolling="no" id=\"bdb_pending_video_player\"\>');
					chapter.get(0).innerHTML = ff3;
					var ifr = chapter.get(0).querySelector('#bdb_pending_video_player');
					var doc = ifr.contentDocument || ifr.contentWindow.document;
					doc.body.appendChild(videoNode);

					Data.loadVideoHls(videoNode , videoItem);
				} 
				//audio
				// else if (findAudioCount > 0){
				// 	var replaceHtml = Util.replaceStringElement(de,'\<audio ','\<\/audio\>');
				// 	chapter.get(0).innerHTML = replaceHtml;
				// } 
				else {
					// const docf = document.createDocumentFragment()
					// docf.appendChild($(de).get(0))
					// chapter.get(0).appendChild(docf)
					chapter.get(0).innerHTML = de;

				}

				/**
				 * todo sjhan
				 * background color를 가저와 어두운 값이면 clear 해버려야함 
				 */
				// let list = Parser.getElementChildList(chapter.get(0))
				// list.forEach(function(e){
				// 	e.style.backgroundColor = "transparent";
				// })
				// list = null

				chapter.waitForImages({
					finished:function(){
						var promises = [];
						chapter.find("audio").filter(function () {
							// this.style.visibility = 'hidden';
							promises.push(Data.loadAudioHls(this));
							return true;
						});
						if(promises.length > 0) {
							Promise.all(promises)
							.then((_) => {
								Data.setLoadChapter(curChapterNum, true);
								Data.ResetChapter();
								resolve();
							});
						}
						else {
							Data.setLoadChapter(curChapterNum, true);
							Data.ResetChapter();
							resolve();
						}
					},
					each: function() {
					
					},
					waitForAll: true
				});

				// chapter.html(Obfucator.obfucator(data));
				// if(bgolor != null && bgolor != undefined && bgolor != '') {
				// 	chapter.css('background-color', bgolor);
				// }

				// chapter.waitForImages({
				// 	finished: function() {
				// 		var promises = [];
		
				// 		chapter.find("video").filter(function () {
				// 			this.style.visibility = 'hidden';
				// 			// Data.HlsObj.push({type:'v', ele: this, isloaded: false});
				// 			promises.push(Data.loadVideoHls(this));
				// 			return true;
				// 		});
				
				// 		chapter.find("audio").filter(function () {
				// 			this.style.visibility = 'hidden';
				// 			// Data.HlsObj.push({type:'a', ele: this, isloaded: false});
				// 			promises.push(Data.loadAudioHls(this));
				// 			return true;
				// 		});

				// 		if(promises.length > 0) {

				// 			Promise.all(promises)
				// 			.then((_) => {
				// 				Data.setLoadChapter(curChapterNum, true);
				// 				Data.ResetChapter();
				// 				resolve();
				// 			});
				// 		}
				// 		else {
				// 			Data.setLoadChapter(curChapterNum, true);
				// 			Data.ResetChapter();
				// 			resolve();
				// 		}
				// 	},
				// 	each: function() {
				// 		// ePubEngineLogJS.L.log('loader wait image : ['+curChapterNum+'] :: ');
				// 	},
				// 	waitForAll: true
				// });


				
			});
		},
		onAudioCallback:function(type , obj){
			if (callbackAudioFunc) {
				callbackAudioFunc(type, obj);
			}
		},
		/**
		 * 해당 챕터의 로딩 상태를 로딩으로 처리 한다.
		 * @param {number} idx 
		 * @param {boolean} isLoaded 
		 */
		setLoadChapter: function(idx, isLoaded) {
			var chapter = $("#bdb-chapter"+idx);
	
			if(isLoaded) {
				chapter.attr("loaded", 1);
			}
			else {
				chapter.removeAttr("loaded");
			}
		},
		/**
		 * 해당 챕터의 내용을 안보이게 처리 한다.
		 * @param {number} idx 
		 */
		removeChapter: function(idx) {
			var chapter = $("#bdb-chapter"+idx);
	
			var isLoaded = chapter.attr("loaded");
			if(isLoaded != null && isLoaded != undefined && isLoaded == 1) {
				chapter.children().css('visibility', 'hidden');
			}
		},
		/**
		 * 해당 챕터가 로딩 되어 있는지 확인
		 * @param {number} idx 
		 * @returns true or false 
		 */
		checkLoadChapter: function(idx) {

			if(idx < 0 || idx >= totalSpine) {
				return true;
			}

			var chapter = $("#bdb-chapter"+idx);
	
			var isLoaded = chapter.attr("loaded");
			if(isLoaded != null && isLoaded != undefined && isLoaded == 1) {
				chapter.children().css('visibility', 'visible');
				return true;
			}
			return false;
		},
		setCurrentChapter: function(idx) {
			currentSpine = idx;
			this.ResetChapter();
		},
		loadSpines: function(index) {
			currentSpine = index;
			this.makeContentTag();
			
			return this.loadSpine(index);
		},
		/**
		 * 기본 영역을 만들기 위해 태그를 생성한다.
		 */
		makeContentTag: function() {
			var contents = $("#"+contentConainerId);
	
			contents.empty();
	
			for(var i = 0; i < totalSpine; i++) {
				var chapter = document.createElement("li");
				chapter.setAttribute("class", "chapter" + i + " chapter bdb-page-chapter" + (0 == i ? " first" : ""));
				chapter.setAttribute("id", "bdb-chapter"+i);
				contents.append(chapter);
			}
		},
		loadSpineAt: function(index) {
			currentSpine = index;

			return Data.loadSpine(index);
		},
		ResetChapter: function() {
			if(currentSpine == 0 || currentSpine - 1 == totalSpine) {
				for(var i = 0; i < totalSpine; i++) {
					if(Math.abs(currentSpine - i) > 2) {
						this.removeChapter(i);
					}
					else {
						this.checkLoadChapter(i);
					}
				}
			}
			else {
				for(var i = 0; i < totalSpine; i++) {
					if(Math.abs(currentSpine - i) > 1) {
						this.removeChapter(i);
					}
					else {
						this.checkLoadChapter(i);
					}
				}
			}
	
			return true;
		},
		/**
		 * 지정한 챕터의 내용을 다운로드 및 추가 한다.
		 * @param {number} idx 
		 * @returns resolve, reject 으로 로딩할 챕터 번호를 리턴한다.
		 */
		loadSpine: function(idx) {
			// console.log('@@@@@ ');
			return new Promise( function(resolve, reject) {
				
				if(idx < 0 || idx >= totalSpine) {
					resolve(idx);
					return;
				}

				var item = spineItems[idx];

				if(item == undefined) {
					console.log('invalid index : '+idx);
					return reject(idx);
				}

				if(Data.checkLoadChapter(idx)) {
					// 로딩 할려고 하는 챕터가 이미 로딩 되어 있을 경우
					// 이전, 다음 챕터의 로딩 여부를 체크해 로딩한다.
					Data.ResetChapter();

					if(currentSpine > 0) {
						if(!Data.checkLoadChapter(currentSpine - 1)) {
							Data.loadSpine(currentSpine - 1)
								.then(function(data) {
									if(!Data.checkLoadChapter(currentSpine +1)) {
										Data.loadSpine(currentSpine + 1)
											.then(function(data) {
												resolve(data);
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine + 1);
											});
									}
									else {
										resolve(data);
									}
								})
								.catch(function(err) {
									ePubEngineLogJS.L.log(err);
									reject(currentSpine - 1);
								});
						}
						else {
							if(!Data.checkLoadChapter(currentSpine + 1)) {
								Data.loadSpine(currentSpine + 1)
									.then(function(data) {
										resolve(data);
									})
									.catch(function(err) {
										ePubEngineLogJS.L.log(err);
										reject(currentSpine + 1);
									});
							}
							else {
								resolve(currentSpine +1);
							}
						}
					}
					else if(currentSpine == (totalSpine - 1)) {
						if(!Data.checkLoadChapter(currentSpine - 1)) {
							Data.loadSpine(currentSpine - 1)
								.then(function(data) {
									if(!Data.checkLoadChapter(currentSpine - 2)) {
										Data.loadSpine(currentSpine - 2)
											.then(function(data) {
												resolve(data);
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine - 2);
											});
									}
									else {
										resolve(data);
									}
								})
								.catch(function(err) {
									ePubEngineLogJS.L.log(err);
									reject(currentSpine - 1);
								});
						}
						else {
							if(!Data.checkLoadChapter(currentSpine - 2)) {
								Data.loadSpine(currentSpine - 2)
									.then(function(data) {
										resolve(data);
									})
									.catch(function(err) {
										ePubEngineLogJS.L.log(err);
										reject(currentSpine - 2);
									});
							}
							else {
								resolve(currentSpine - 2);
							}
						}
					}
					else {
						if(currentSpine + 2 >= totalSpine) {

							resolve(currentSpine + 1);
							return;
						}
						
						if(!Data.checkLoadChapter(currentSpine + 1)) {
							Data.loadSpine(currentSpine + 1)
								.then(function(data) {
									if(!Data.checkLoadChapter(currentSpine + 2)) {
										Data.loadSpine(currentSpine + 2)
											.then(function(data) {
												resolve(data);
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine + 2);
											});
									}
									else {
										resolve(data);
									}
								})
								.catch(function(err) {
									ePubEngineLogJS.L.log(err);
									reject(currentSpine + 1);
								});
						}
						else {
							if(currentSpine + 2 >= totalSpine) {

								resolve(currentSpine + 1);
								return;
							}
							
							if(!Data.checkLoadChapter(currentSpine + 2)) {
								Data.loadSpine(currentSpine + 2)
									.then(function(data) {
										resolve(data);
									})
									.catch(function(err) {
										ePubEngineLogJS.L.log(err);
										reject(currentSpine + 2);
									});
							}
							else {
								resolve(currentSpine + 2);
							}
						}
					}
				}
				else {
					// 해당 챕터를 로딩
					if(item.isLoaded) {
						var decrypted = Util.decryptContents(item.contents);

						// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
						decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
						decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

						Data.addedContents(idx, decrypted, item.bgcolor).then(function() {
							Data.checkLoadChapter(idx);
							Data.ResetChapter();

							if(currentSpine > 0) {
								if(!Data.checkLoadChapter(currentSpine - 1)) {
									Data.loadSpine(currentSpine - 1)
										.then(function(data) {
											if(!Data.checkLoadChapter(currentSpine +1)) {
												Data.loadSpine(currentSpine + 1)
													.then(function(data) {
														resolve(data);
													})
													.catch(function(err) {
														ePubEngineLogJS.L.log(err);
														reject(currentSpine + 1);
													});
											}
											else {
												resolve(data);
											}
										})
										.catch(function(err) {
											ePubEngineLogJS.L.log(err);
											reject(currentSpine - 1);
										});
								}
								else {
									if(!Data.checkLoadChapter(currentSpine + 1)) {
										Data.loadSpine(currentSpine + 1)
											.then(function(data) {
												resolve(data);
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine + 1);
											});
									}
									else {
										resolve(currentSpine +1);
									}
								}
							}
							else if(currentSpine == (totalSpine - 1)) {
								if(!Data.checkLoadChapter(currentSpine - 1)) {
									Data.loadSpine(currentSpine - 1)
										.then(function(data) {
											if(!Data.checkLoadChapter(currentSpine - 2)) {
												Data.loadSpine(currentSpine - 2)
													.then(function(data) {
														resolve(data);
													})
													.catch(function(err) {
														ePubEngineLogJS.L.log(err);
														reject(currentSpine - 2);
													});
											}
											else {
												resolve(data);
											}
										})
										.catch(function(err) {
											ePubEngineLogJS.L.log(err);
											reject(currentSpine - 1);
										});
								}
								else {
									if(!Data.checkLoadChapter(currentSpine - 2)) {
										Data.loadSpine(currentSpine - 2)
											.then(function(data) {
												resolve(data);
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine - 2);
											});
									}
									else {
										resolve(currentSpine - 2);
									}
								}
							}
							else {
								if(!Data.checkLoadChapter(currentSpine + 1)) {
									Data.loadSpine(currentSpine + 1)
										.then(function(data) {
											if(!Data.checkLoadChapter(currentSpine + 2)) {
												Data.loadSpine(currentSpine + 2)
													.then(function(data) {
														resolve(data);
													})
													.catch(function(err) {
														ePubEngineLogJS.L.log(err);
														reject(currentSpine + 2);
													});
											}
											else {
												resolve(data);
											}
										})
										.catch(function(err) {
											ePubEngineLogJS.L.log(err);
											reject(currentSpine + 1);
										});
								}
								else {
									if(!Data.checkLoadChapter(currentSpine + 2)) {
										Data.loadSpine(currentSpine + 2)
											.then(function(data) {
												resolve(data);
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine + 2);
											});
									}
									else {
										resolve(currentSpine + 2);
									}
								}
							}
						});
					}
					else {
						// Util.getUrlAxios(contentBaseUrl, item.path, authToken)
						Util.getUrl(contentBaseUrl, item.path, authToken)
						.then(function(data) {

							item.contents = data.value;
							item.isLoaded = true;

							var decrypted = Util.decryptContents(data.value);

							// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
							decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
							decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

							Data.addedContents(idx, decrypted, item.bgcolor).then(function() {
								Data.checkLoadChapter(idx);
								Data.ResetChapter();

								if(currentSpine > 0) {
									if(!Data.checkLoadChapter(currentSpine - 1)) {
										Data.loadSpine(currentSpine - 1)
											.then(function(data) {
												if(!Data.checkLoadChapter(currentSpine +1)) {
													Data.loadSpine(currentSpine + 1)
														.then(function(data) {
															resolve(data);
														})
														.catch(function(err) {
															ePubEngineLogJS.L.log(err);
															reject(currentSpine + 1);
														});
												}
												else {
													resolve(data);
												}
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine - 1);
											});
									}
									else {
										if(!Data.checkLoadChapter(currentSpine + 1)) {
											Data.loadSpine(currentSpine + 1)
												.then(function(data) {
													resolve(data);
												})
												.catch(function(err) {
													ePubEngineLogJS.L.log(err);
													reject(currentSpine + 1);
												});
										}
										else {
											resolve(currentSpine +1);
										}
									}
								}
								else {
									if(!Data.checkLoadChapter(currentSpine + 1)) {
										Data.loadSpine(currentSpine + 1)
											.then(function(data) {
												if(!Data.checkLoadChapter(currentSpine + 2)) {
													Data.loadSpine(currentSpine + 2)
														.then(function(data) {
															resolve(data);
														})
														.catch(function(err) {
															ePubEngineLogJS.L.log(err);
															reject(currentSpine + 2);
														});
												}
												else {
													resolve(data);
												}
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(currentSpine + 1);
											});
									}
									else {
										if(!Data.checkLoadChapter(currentSpine + 2)) {
											Data.loadSpine(currentSpine + 2)
												.then(function(data) {
													resolve(data);
												})
												.catch(function(err) {
													ePubEngineLogJS.L.log(err);
													reject(currentSpine + 2);
												});
										}
										else {
											resolve(currentSpine + 2);
										}
									}
								}
							});
						})
						.catch(function(err) {
							console.log(err);
							reject(-1);
						});
					}
				}
			});
		},
		/**
		 * 해당 데이터의 복호화된 데이터만 가져온다.
		 * @param {number} idx 파일 번호
		 * @returns null or string
		 */
		getSpineData: function(idx) {

			return new Promise( function(resolve, reject) {				
				var item = spineItems[idx];

				if(item == undefined) {
					console.log('invalid index : '+idx);
					return reject(null);
				}

				if(item.isLoaded) {
					var decrypted = Util.decryptContents(item.contents);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

					resolve(decrypted);
				}
				else {
					// 해당 챕터를 로딩
					// Util.getUrlAxios(contentBaseUrl, item.path, authToken)
					Util.getUrl(contentBaseUrl, item.path, authToken)
						.then(function(data) {
							item.contents = data.value;
							item.isLoaded = true;

							var decrypted = Util.decryptContents(data.value);

							// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
							decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
							decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

							resolve(decrypted);
						})
						.catch(function(err) {
							console.log(err);
							reject(null);
						});
				}
			});
		},
		/**
		 * 해당 데이터의 복호화된 데이터만 가져온다.
		 * @param {number} idx 파일 번호
		 * @returns null or string
		 */
		 getSpineDataNested: function(idx) {
			var item = spineItems[idx];

			if(item == undefined) {
				console.log('invalid index : '+idx);
				return Promise.reject(null)
			}
			if(item.isLoaded) {
				var decrypted = Util.decryptContents(item.contents);
				decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
				decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);
				decrypted = Util.replaceAll(decrypted, "{CONTENT_OBFUCATE}", ' ');
				return Promise.resolve(decrypted)
			} else {
				return Util.getUrl(contentBaseUrl, item.path, authToken)
				.then(function(data) {
					item.contents = data.value;
					item.isLoaded = true;
					var decrypted = Util.decryptContents(data.value);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_OBFUCATE}", ' ');
					return decrypted
				})
			}

			return new Promise( function(resolve, reject) {				
				var item = spineItems[idx];

				if(item == undefined) {
					console.log('invalid index : '+idx);
					return reject(null);
				}

				if(item.isLoaded) {
					var decrypted = Util.decryptContents(item.contents);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

					resolve(decrypted);
				}
				else {
					// 해당 챕터를 로딩
					// Util.getUrlAxios(contentBaseUrl, item.path, authToken)
					Util.getUrl(contentBaseUrl, item.path, authToken)
						.then(function(data) {
							item.contents = data.value;
							item.isLoaded = true;

							var decrypted = Util.decryptContents(data.value);

							// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
							decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
							decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

							resolve(decrypted);
						})
						.catch(function(err) {
							console.log(err);
							reject(null);
						});
				}
			});
		},

		/**
		 * 퍼센트 안쪽에 속하는 목차정보
		 * @param {*} percent 
		 * @returns 
		 */
		getTocByPercent: function(percent){
			if (typeof percent !== 'number'){
				return null
			}
			let t = null
			for (let i=0;i<tocs.length;i++){
				let item = tocs[i]
				let	nItem = null
				if (i === tocs.length-1){
					if (percent >= item.pos){
						t = item
						break;
					}
				} else {
					nItem = tocs[i+1]
					if (percent >= item.pos && percent < nItem.pos){
						t = item
						break;
					}
				}
			}

			return t
		}
	};

	/**
	 * StyleSheet관련 파싱
	 */
	const StyleSheet = Loader.StyleSheet = {
		/**
		 * 배경색 초기화
		 * @returns 
		 */
		resetBackground(){
			console.log('###resetBackground ')
			if (styleSheetBackColorList.length === 0 ){
				return
			}
			for (let item of styleSheetBackColorList){
				let ele = document.querySelectorAll(item.class)
				if (ele){
					ele.forEach(e=>{
						$(e).css(item.key,"");
					})
				}
			}
		},
		/**
		 * 배경색 투명색으로 변경
		 * @returns 
		 */
		transparentBackground(){
			if (styleSheetBackColorList.length === 0 ){
				return
			}
			for (let item of styleSheetBackColorList){
				let ele = document.querySelectorAll(item.class)
				if (ele){
					ele.forEach(e=>{
						let mkey = item.key
						console.log(e,$(e).css(mkey))
						$(e).css(mkey,'transparent')
					})
				}
			}
		},
		/**
		 * styleSheet Attr 추출
		 */
		extractAttrInStyleSheet(){
			let stylesheets = StyleSheet.getEpubStyleSheet()
			StyleSheet.clearSideMargin(stylesheets)
			if (stylesheets && stylesheets.cssRules && stylesheets.cssRules.length>0){
				for (let i = 0; i<stylesheets.cssRules.length; i++){
					let rule = stylesheets.cssRules[i];
					
					if (typeof rule.cssText === 'string' && rule.cssText.length >0 ){
						if (rule.cssText.includes('background')){
							if (rule.cssText.includes('url')){

							} else {
								styleSheetRuleBackColorMap.set(i,rule)
							}
						}
					}
				}
			}
		},
		/**
		 * 
		 * @param {*} element 
		 */
		extractMarginInStyleSheet(element){
			let stylesheets = StyleSheet.getEpubStyleSheet()
			if (stylesheets && stylesheets.cssRules && stylesheets.cssRules.length>0){
				for (let i = 0; i<stylesheets.cssRules.length; i++){
					let rule = stylesheets.cssRules[i];
					let target = element.querySelector(rule.selectorText);
					if (target){
						if (target.id.includes('bdb-chapter') || target.parentElement.id.includes('bdb-chapter')){
							if (typeof rule.cssText === 'string' && rule.cssText.length >0 ){
								let attr = rule.cssText
								console.log('##margin',target,attr)
								// stylesheets.deleteRule(i);
								// if (rule.cssText.includes('margin-right')){
								// 	rule.style.marginRight = '0px';
									
								// }
								// if (rule.cssText.includes('margin-left')){
								// 	rule.style.marginLeft = '0px';
								// }
		
								// if (rule.style.margin.length > 0){
								// 	// console.log('##margin',rule.style.margin)
								// }	
							}
						}
					}
				}
			}
		},
		getColors(){
			return styleSheetRuleBackColorMap
		},
		getMargins(){
			return styleSheetRuleMarginMap
		},
		/**
		 * epub style sheet 가저오기
		 * @returns 
		 */
		getEpubStyleSheet(){
			return StyleSheet.getStyleSheet("bdb-epub-css-file")
		},
		/**
		 * style sheet 가저오기
		 * @param {*} unique_title 
		 * @returns 
		 */
		getStyleSheet(unique_title) {
			for (const sheet of document.styleSheets) {
				if (sheet.title === unique_title) {
					return sheet;
				}
			}
		},
		/**
		 * 배경색을 추출한다.
		 * @param {*} originalStr 원본 string
		 * @returns 
		 */
		getCssAttrBackground(originalStr){
			return StyleSheet.getCssAttr(originalStr,'background')
		},
		getCssAttrMargin(originalStr){
			return StyleSheet.getCssAttr(originalStr,'margin')
		},
		getCssAttrMarginLeft(originalStr){
			return StyleSheet.getCssAttr(originalStr,'margin-left')
		},
		getCssAttrMarginRight(originalStr){
			return StyleSheet.getCssAttr(originalStr,'margin-right')
		},
		/**
		 * 
		 * @param { CSSStyleSheet } sheet를 넣어주면 left,right 속성의 sheet값을 제거해준다.
		 * @returns 
		 */
		clearSideMargin(sheet){
            if (!(sheet instanceof CSSStyleSheet)){
                return;
            }
			let result = []

			try {
				for (const rule of sheet.cssRules){
					let origin = rule.cssText
					console.log('ORIGIN',origin);
					if (!origin){
						continue;
					}
					let marginLeft = StyleSheet.getCssAttr(origin,"margin-left")
					let marginRight = StyleSheet.getCssAttr(origin,"margin-right")
					let margin = StyleSheet.getCssAttr(origin,"margin")
					let replacement = null
					if (marginLeft && marginRight){
						replacement = origin.replace(marginLeft.key+": "+marginLeft.value+"; ","");
						replacement = replacement.replace(marginRight.key+": "+marginRight.value+"; ","")
					} else if (marginLeft){
						replacement = origin.replace(marginLeft.key+": "+marginLeft.value+"; ","");
					} else if (marginRight){
						replacement = origin.replace(marginRight.key+": "+marginRight.value+"; ","")
					} else if (margin){
						if (typeof margin.value === 'string'){
							let mlist = margin.value.split(" ")
							if (mlist.length === 4){ 
								replacement = origin.replace(margin.key+": "+margin.value+"; ","margin-top: "+mlist[0]+"; margin-bottom: "+mlist[2]+";");
							} else if (mlist.length === 3){
								replacement = origin.replace(margin.key+": "+margin.value+"; ","margin-top: "+mlist[0]+"; margin-bottom: "+mlist[2]+";");
							} else if (mlist.length === 2){
								replacement = origin.replace(margin.key+": "+margin.value+"; ","margin-top: "+mlist[0]+"; margin-bottom: "+mlist[0]+";");
							} else if (mlist.length === 1){
								replacement = origin.replace(margin.key+": "+margin.value+"; ","margin-top: "+mlist[0]+"; margin-bottom: "+mlist[0]+";");
							} else {
	
							}
						}
					}
					if (replacement){
						let idx = [...sheet.cssRules].findIndex(e=>e.cssText === origin)
						console.log(idx);
						result.push({ origin , replacement})
						sheet.deleteRule(idx)
					}
				}
				result.forEach(item=>{
					console.log(item.replacement)
					sheet.insertRule(item.replacement)    
				})
			} catch(e){
				console.error(e);
			}

			// result = null;
		},
		/**
		 * string에서 속성을 추출한다.
		 * @param {*} originalStr 원본 string
		 * @param {*} attr 추출하려는 속성
		 * @returns 
		 */
		getCssAttr(originalStr,attr){
			let back_index = originalStr.indexOf(attr)
			if (back_index === -1){
				return null
			}
			let back_str = originalStr.substring(back_index)
			let back_colon = back_str.indexOf(':')
			if (back_colon === -1){
				return null
			}
			let colon_str = back_str.substring(back_colon+1)
			let attrKey = back_str.substring(0,back_colon)
			let back_end = colon_str.indexOf(';')
			if (back_colon === -1){
				return null
			}
			let backgroundAttr = colon_str.substring(-1,back_end)
			if (backgroundAttr.length > 0){
				backgroundAttr = backgroundAttr.trim()
			}
			return {key:attrKey,value:backgroundAttr} 
		}
	}
	/**
	 * 난독화 관련
	 */
	var Obfucator = Loader.Obfucator = {
		/**
		 * 초기화
		 */
		init: function() {
			this.arrObfucator = new Array();
			this.arrEngChar = "abcdefghi0123456789jklmnopqrstuvwxyz";

			for(var i = 45032; i < 55204; i++) {
				this.arrObfucator.push(String.fromCharCode(i));
			}
		},
		/**
		 * 난독화 태그 생성
		 * @returns 난독화를 위한 태그 생성
		 */
		makeTag: function() {
			var result = '';
			var tagLen = parseInt(Math.random() * 6) + 3;    
	
			for (var i = 0; i < tagLen; i++) {
				idx = parseInt(Math.random() * this.arrEngChar.length);
				while(parseInt(this.arrEngChar.charAt(idx)) >= 0 && i == 0) {
					idx = parseInt(Math.random() * this.arrEngChar.length);
				}
				result += this.arrEngChar.charAt(idx);
			}
			tagLen = null
			return result;
		},
		/**
		 * 난독화
		 * @param {string} data 서버로부터 받은 콘텐츠 데이터
		 * @returns 난독화 된 콘텐츠 데이터
		 */
		obfucator: function(data) {
			let strResult = "";

			var arr = data.split('{CONTENT_OBFUCATE}');
	
			for (var i=0; i<arr.length; i++){
				isEnable = Math.floor(Math.random() * 10);
				// isEnable = 0;

				if(isEnable > 5) {
					charNum = parseInt(Math.random() * 5) + 1;
					d = "";
		
					for(var j = 0; j < charNum; j++) {
						num = parseInt(Math.random() * this.arrObfucator.length);
						d += this.arrObfucator[num];
					}
					tag = this.makeTag();
		
					obs = "<"+tag+" style='display:none !important; visibility: hidden !important;'>"+d+"</"+tag+">";
		
					strResult += arr[i];
					strResult += obs;
					strResult += ' ';
				}
				else {
					strResult += arr[i];
					strResult += ' ';					
				}
			}
			arr = null
			return strResult;
		}
	};

	var Util = Loader.Util = {
		replaceAll: function(str, searchString, replaceString) {
			return str.split(searchString).join(replaceString);
		},
		/**
		 * 콘텐츠 복호화
		 * @param {string} data 암호화 데이터
		 * @returns 복호화된 문자열
		 */
		decryptContents: function(data) {
			return ContentHelperJS.D.ps(data);
		},
		getUrl: function(baseUrl, url, token) {
			return new Promise( function(resolve, reject) {
				$.ajax( {
					_retrycnt: 0,
					_retrymax: 4,
					_retryinterval: 100,
					crossOrigin : true,
					type:'get',
					async: true,
					url: url.replace('{CONTENT_DOMAIN}', baseUrl),
					dataType: "jsonp",
					jsonpCallback:'DataCallback',
					headers: {
						'authorization': token
					},
					beforeSend: function( xhr, settings ) {
						// console.log(settings.url);
					},
					success: function(data) {
						if(data != undefined) {
							resolve(data);
						}
						else {
							this._retrycnt++;
							if((this._retrycnt <= this._retrymax)) {
								var rtnThis = this;

								return void setTimeout(function() {
									$.ajax(rtnThis);
								}, this._retryinterval);
							}
						}
					},
					error: function(request,status,error) {
						// console.log('error Get json');
						// console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
						this._retrycnt++;
						if((this._retrycnt <= this._retrymax)) {
							var rtnThis = this;

							return void setTimeout(function() {
								$.ajax(rtnThis);
							}, this._retryinterval);
						}
						else {
							reject(error);
						}
					}
				});
			});
		},
		getUrlAxios:function(baseUrl, url, token){
			var parseUrl = url.replace('{CONTENT_DOMAIN}', baseUrl);
			var headers = {
				'Content-type': 'application/json; charset=UTF-8',
				'Accept': '*/*',
				'Pragma':'no-cache',
				'authorization': token
			  }
	
			return new Promise(function(resolve , reject ){
				axios.get(parseUrl, { headers : headers})
					.then( function (res) {
						var m = res.data.replace(/^DataCallback\(|\)\;/g, '');
						var data = JSON.parse(m);
						if (res.status === 200){
							resolve(data);
						} else {
							reject(-1);
						}
					})
					.catch(function (error) {
						console.log(error);
						reject(error);
					})
			});
		},
		/**
		 * 
		 * @param {*} htmlString string element
		 * @returns $()와 유사.
		 */
		createElementFromHTML: function(htmlString) {
			var div = document.createElement('div');
			div.innerHTML = htmlString.trim();
			return div.firstChild; 
		},
		/**
		 * 
		 */
		convertTagData:function(data){
			var tag = '<video';
			var videoIdx = data.indexOf(tag);
			if (videoIdx>0){
				return data.insertAt(videoIdx+tag.length    ,' preload=\"none\"');
			}
			return data;
		},

		findBetween:function(beginString, endString, originalString){
			var beginIndex = originalString.indexOf(beginString);
			if (beginIndex === -1) {
				return null;
			}
			var beginStringLength = beginString.length;
			var substringBeginIndex = beginIndex + beginStringLength;
			var substringEndIndex = originalString.indexOf(endString, substringBeginIndex);
			if (substringEndIndex === -1) {
				return null;
			}
			return originalString.substring(substringBeginIndex, substringEndIndex);
		},

		findStrIndex : function(beginString, endString, originalString){
			var beginIndex = originalString.indexOf(beginString);
			if (beginIndex === -1) {
				return null;
			}
			var beginStringLength = beginString.length;
			var substringBeginIndex = beginIndex + beginStringLength;
			var substringEndIndex = originalString.indexOf(endString, substringBeginIndex);
			return {'begin':substringBeginIndex-beginString.length , 'end':substringEndIndex+endString.length};
		},
		replaceRange:function(s, start, end, substitute) {
			return s.substring(0, start) + substitute + s.substring(end);
		},
		videoObjMake:function(data){
			var pst = Util.findBetween('poster\=\"','\"' , data);
			var src = Util.findBetween('src\=\"','\"' , data);
			return {
				poster:pst,
				src:src
			}
		},
		audioObjMake:function(data){
			var src = Util.findBetween('src\=\"','\"' , data);
			return {
				src:src
			}
		},
		makeVideoNode:function(obj){
			let video = document.createElement('video');
			video.src = obj.src;
			video.poster = obj.poster;
			video.controls = true;
			return video;
		},
		/**
		 * 찾으려는 단어의 갯수가 몇개인지 찾는 기능
		 * @param {*} string 문장
		 * @param {*} subString  단어
		 * @param {*} allowOverlapping 중복가능 
		 * @returns 
		 */
		occurrences:function(string, subString, allowOverlapping) {
            string += "";
            subString += "";
            if (subString.length <= 0) return (string.length + 1);

            var n = 0,
                pos = 0,
                step = allowOverlapping ? 1 : subString.length;

            while (true) {
                pos = string.indexOf(subString, pos);
                if (pos >= 0) {
                    ++n;
                    pos += step;
                } else break;
            }
            return n;
        },
		/**
		 * 대체하려는 앞쪽,뒷쪽 단어사이의 문장을 찾아
		 * 오디오를 wrap하는 div를추가한다.
		 * @param {*} originalStringHtml 원본 string html
		 * @param {*} front 대체하려는 앞쪽 단어
		 * @param {*} end 대체하려는 뒷쪽단어
		 * @returns 
		 */
		 replaceStringElement:function(originalStringHtml, front, end ){
            var mMaster = originalStringHtml;
            var mFront = front;
            var mEnd = end;
            var mEndLen = mEnd.length;
            var count = Util.occurrences(mMaster, front);
            try{
                for (let i = 0; i<count; i++){
                    //계속 string갯수가 바뀔꺼라서 다시 계산한다.
                    var left = Util.getIndicesOf(mMaster,mFront)[i];
                    var right = Util.getIndicesOf(mMaster,mEnd)[i];
                    //audio string tag
                    var stringElement = mMaster.substring(left, right + mEndLen);
                    //audio wrapping
                    var wrap = Util.getAudioWrapStringElement(stringElement,i);
                    //replace
                    mMaster = Util.replaceRange(mMaster ,left ,right+mEndLen ,wrap);
                }
            }catch(e) {
				console.error(e);
                mMaster = originalStringHtml;
            }finally{
                navigation = null;
                mFront = null;
                mEnd = null;
                mEndLen = null;
                count = null;
            }
            return mMaster;
        },
		/**
		 * 
		 * @param {*} audio string 형태의 audio
		 * @returns 
		 */
		getAudioWrapStringElement :function(audio,index){
            return '\<div id\=\"bdb_custom_audio_wrapper_'+index+'\" class\=\"bdb_custom_audio_wrapper\"\>'+audio+'\<\/div\>';
        },
		/**
		 * custom iframe string element.
		 * @returns 
		 */
		getIframeStringElement:function(){
			return '\<iframe class=\"bdb_custom_iframe\" scrolling=\"no\" id=\"bdb_pending_video_player\"\>';
		},
		getIframeStringAudioElement:function(){
			return '\<iframe class=\"bdb_custom_iframe\" scrolling=\"no\" id=\"bdb_pending_audio_player\"\>';
		},
		/**
		 * 찾는 단어의 string index를 배열로 return
		 * @param {*} sentence 문장
		 * @param {*} searchStr 찾을 단어
		 * @param {*} caseSensitive 
		 * @returns 
		 */
		getIndicesOf:function(sentence , searchStr, caseSensitive) {
            var searchStrLen = searchStr.length;
            if (searchStrLen == 0) {
                return [];
            }
            var startIndex = 0, index, indices = [];
            if (!caseSensitive) {
                sentence = sentence.toLowerCase();
                searchStr = searchStr.toLowerCase();
            }
            while ((index = sentence.indexOf(searchStr, startIndex)) > -1) {
                indices.push(index);
                startIndex = index + searchStrLen;
            }
            return indices;
        },
		LoadCssFile(url){
			return new Promise(function(resolve,reject){
				let callback = function(e){
					resolve()
				} 
				var styles = document.createElement('link');
				styles.rel = 'stylesheet';
				styles.type = 'text/css';
				styles.media = 'screen';
				styles.href = url
				styles.onreadystatechange = callback;
				styles.onload = callback;
				styles.title = "bdb-epub-css-file"
				document.head.appendChild(styles);
			})
		},
	};

	var Parser = Loader.Parser = {

		getElementChildList: function(element){
			try{
				var childNodes = element.childNodes,
				children = [],
				i = childNodes.length;
		
				while (i--) {
					if (childNodes[i].nodeType == 1) {
						children.unshift(childNodes[i]);
					}
				}
				i = null
				childNodes = null
				return children
			} catch(e){
				return null
			}
		},
		getPropertyValue: function(element , styleType){
			let styles = getComputedStyle(element);
			let bg = styles.getPropertyValue(styleType);
			styles = null
			return bg
		},
		getBackground: function(element){
			let styles = getComputedStyle(element);
			let bg = styles.getPropertyValue('background-color');
			styles = null
			return bg
		},
		getBackgroundColor: function(element){
			let styles = getComputedStyle(element);
			let bg = styles.getPropertyValue('background-color');
			styles = null
			return bg
		}
	}

	return Loader;

}(ContentHelperJS));

export { ePubEngineLoaderJS as default };