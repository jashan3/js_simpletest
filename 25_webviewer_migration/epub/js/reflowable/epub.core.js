/*
Runa Web ePub engine core V1.0.0.1001
www.bookswage.com
(c) 2004-2021 by Crazy Eric. All rights reserved.
last update 220316 20:00 sjhan
*/


/// <reference path="./epub.extension.js" />
/// <reference path="./epub.loader.js" />
/// <reference path="./epub.log.js" />
/// <reference path="../common/lib/hls.min.js" />

/**
 * ePub Engine core
 */
 var ePubEngineCoreJS = ePubEngineCoreJS || (function(ePubEngineLogJS, ePubEngineLoaderJS, undefined) {
	var ePubEngineCoreJs;

    // Native ePubEngineCoreJs from window (Browser)
    if (typeof window !== 'undefined' && window.ePubEngineCoreJs) {
        ePubEngineCoreJs = window.ePubEngineCoreJs;
    }

	if (typeof self !== 'undefined' && self.ePubEngineCoreJs) {
        ePubEngineCoreJs = self.ePubEngineCoreJs;
    }

	// Native ePubEngineCoreJs from worker
    if (typeof globalThis !== 'undefined' && globalThis.ePubEngineCoreJs) {
        ePubEngineCoreJs = globalThis.ePubEngineCoreJs;
    }

    // Native (experimental IE 11) ePubEngineCoreJs from window (Browser)
    if (!ePubEngineCoreJs && typeof window !== 'undefined' && window.msePubEngineCoreJs) {
        ePubEngineCoreJs = window.msePubEngineCoreJs;
    }

	var Core = {};
	/**
	 * viewer conainer element
	 */
	var ele_view_wrapper;

	/**
	 * 
	 */
	var ele_pure_view_wrapper;

	/**
	 * 뷰어 로딩 시 보여줄 엘리먼트
	 */
	var ele_loading_bar;

	/**
	 * 상단 메뉴 컨테이너
	 */
	var ele_topmenu_container;
	/**
	 * 하단 메뉴 컨테이너
	 */
	var ele_bottommenu_container;

	/**
	 * 뷰어 가로 크기
	 */
	var _viewer_width = 0;
	/**
	 * 뷰어 세로 높이
	 */
	var _viewer_height = 0;

	/**
	 * 뷰어 상하 margin
	 */
	var _viewer_height_margin = 0;

	/**
	 * 높이 공간
	 */
	var _viewer_height_space = 0;

	var _initialized = false;

	/**
	 * 가로 스크롤 모드 시 페이지를 나눌 넓이
	 */
	var _columnWidth = 0;
	/**
	 * 가로 스크롤 모드 시 페이지를 나눌때 중간의 여백 넓이
	 */
	var _columnGapWidth = "0";

	/*
	* 가로 모드 중간 여백 기본값.
	*/
	var _columnGapDefault = 60;

	/**
	 * 가로 모드 중간 여백에 따른 보정값.
	 */
	var _columnGapCorrect = 30;

	/**
	 * 세로 스크롤 시 각 파일별 시작 높이
	 */
	var _arrSpineHeight = [];
	/**
	 * 가로 스크롤 시 각 파일별 시작 왼쪽 위치
	 */
	var _arrSpineWidth = [];
	/**
	 * 각 파일별
	 */
	var _mapSpineHeight = new Map();
	var _mapSpineWidth = new Map();

	/**
	 * 페이지 모드 (true : 앙면, false : 단면)
	 */
	var _pageMode = true;
	/**
	 * 세로 스크롤 모드 ( true : 세로 스크롤 모드, false : 가로 스크롤 모드)
	 */
	var _scrollMode = false;

	/**
	 * 현재 폰트 크기
	 */
	var _curFontSize = null;

	/**
	 * 왼쪽 margin
	 */
	var _leftMargin = 0;

	/**
	 * 마지막 위치 (내부 사용용)
	 */
	var lastReadPos;
	var lastReadPosSelector;
	var lastReadPosOffset;

	/**
	 * 현재 위치정보 callback
	 */
	var callbackPosition;


	/**
	 * error발생햇을경우 callback
	 */
	var callbackError = null;
	/**
	 * 로딩여부
	 */
	var callbackOnLoading = null;

	/**
	 * selection picker 사용 여부
	 */
	var _use_selection_picker = false;

	var Config = Core.Config = {
		imgRoot:"/images/static/",
		/**
		 * 세팅값 저장
		 */
		Save: function(){
			
		},
		/**
		 * 세팅값 불러오기
		 */
		Load: function(){

		}
	}


	var Layout = Core.Layout = {
		/**
		 * 환경 설정
		 * @param {JQuery} viewerContainer 뷰어 컨테이너
		 * @param {JQuery} loadingbar 로딩 컨테이너
		 * @param {string} topMenu상단 메뉴 클래스
		 * @param {string} bottomMenu 하단 메뉴 클래스
		 * @param {boolean} pageMode 페이지 모드
		 * @param {boolean} scrollMode 스크롤 모드
		 */
		config: function(viewerContainer, loadingbar, topMenu, bottomMenu, smil, positionCallback,smilCallback) {
			ele_view_wrapper = viewerContainer;
			ele_pure_view_wrapper = document.getElementById(viewerContainer.attr('id'));
			ele_loading_bar = loadingbar;
			ele_topmenu_container = topMenu;
			ele_bottommenu_container = bottomMenu;

			callbackPosition = positionCallback;

			// _pageMode = pageMode;
			// _scrollMode = scrollMode;

			_viewer_height = ele_view_wrapper.parent().height();

			/**
			 * annotation 초기화.
			 */
			ePubEngineCoreJS.Annotation.init()

			if (smil){
				var smilItems = smil['items'];
				if (smilItems){
					smilItems.forEach(item=>{
						if (item.seq){
							item.seq.forEach(seqItem =>{
								seqItem.begin = parseFloat(seqItem.begin);
								seqItem.end = parseFloat(seqItem.end);
							})
						}
					})
					var smilInfo = { activeclass: smil['active-class'], items: smilItems };
					SMIL.setup(smilInfo ,smilCallback);
				}
			}
		},
		/**
		 * 
		 * @param {boolean} pageMode 페이지 모드 (true : 앙면, false : 단면)
		 * @param {boolean} scrollMode 세로 스크롤 모드 ( true : 세로 스크롤 모드, false : 가로 스크롤 모드)
		 */
		changeViewModel(pageMode, scrollMode) {
			_pageMode = pageMode;
			_scrollMode = scrollMode;
		},
		/**
		 * 페이지 모드 정보를 리턴한다.
		 * @returns 페이지 모드 (true : 앙면, false : 단면) 
		 */
		getPageMode: function() {
			return _pageMode;
		},
		/**
		 * 현재모드가 스크롤모드인지 return 한다.
		 * @returns {Boolean}
		 */
		getScrollMode: function() {
			return _scrollMode;
		},
		/**
		 * 현재 뷰어 높이
		 * @returns {Number}
		 */
		getViewerHeight: function() {
			return _viewer_height;
		},
		getEachColoumWidth: function(){
			return _viewer_width - _columnGapCorrect
		},
		/**
		 * 왼쪽 여백값 -> 오른쪽여백을주기위해 넓이를 빼기위함.
		 * @param {*} value 
		 */
		setLeftMargin:function( value ){
			_leftMargin = value
		},
		/**
		 * 상하 여백
		 * @param {*} value 
		 */
		setHeightMargin: function(value){
			_viewer_height_margin = value
		},
		/**
		 * 높이 보정값
		 * @param {} value 
		 */
		setHeightSpace:function(value){
			_viewer_height_space = value;
		},
		/**
		 * 양면모드일때 사이 갭
		 * @param {*} value 
		 */
		setColumnGap:function(value){
			_columnGapDefault = value;
			_columnGapCorrect = _columnGapDefault/2
		},
		/**
		 * 에러발생 callback interface
		 * @param {*} callback 
		 */
		setErrorCallback:function(callback) {
			callbackError = callback	
		},
		/**
		 * 뷰어가 로딩되는 callback interface
		 * @param {*} callback 
		 */
		setLoadingCallback:function(callback){
			callbackOnLoading = callback
		},
		/**
		 * 화면 재 배열
		 * @param {boolean} excuteResize spine data 재계산할지 여부
		 * @returns Promise
		 */
		init: function(excuteResize) {
			
			if (excuteResize){
				for(var i = 0; i < ePubEngineLoaderJS.Data.getTotalSpine(); i++) {
					_arrSpineWidth[i] = 0;
					_arrSpineHeight[i] = 0;
					this.resetChapterStyle(i);
				}
			}


			return new Promise( function(resolve, reject) {
				try {
					_viewer_width = parseInt(ele_view_wrapper.parent().width());

					//좌우 여백
					if (_leftMargin){
						_viewer_width = _viewer_width - (_leftMargin*2);//right margin
					}
					
					//짝수인지 체크.
					if (_pageMode && (_viewer_width%2 != 0)){
						_viewer_width = _viewer_width-1;
					}
					_viewer_width = parseInt(_viewer_width)
					ele_view_wrapper.width(_viewer_width);
					
					
					_viewer_height = (window.innerHeight) - _viewer_height_space;
					//상하여백
					_viewer_height = _viewer_height - _viewer_height*parseFloat(_viewer_height_margin)/100
	
					ele_view_wrapper.height(_viewer_height);

					_columnWidth = parseInt(_pageMode ? parseFloat((_viewer_width / 2) - _columnGapCorrect) : _viewer_width);
					_columnGapWidth = _pageMode ? _columnGapDefault.toString() : "0";

			
					if(!_scrollMode) {
						ele_view_wrapper.css({
							overflow: "hidden",
							"column-width": _columnWidth + "px",
							"-webkit-column-width": _columnWidth + "px",
							"-moz-column-width": _columnWidth + "px",
							"-ms-column-width": _columnWidth + "px",
							"column-gap": _columnGapWidth + "px",
							"-webkit-column-gap":_columnGapWidth + "px",
							"-ms-column-gap":_columnGapWidth + "px",
							"-moz-column-gap":_columnGapWidth + "px"
						});

						ele_view_wrapper.css("overflow-x", "hidden");
						ele_view_wrapper.css("overflow-y", "hidden");
					}
					else {
						// 세로 스크롤 모드
						ele_view_wrapper.css({
							overflow: "visible",
							"column-width": '',
							"-webkit-column-width": '',
							"-moz-column-width": '',
							"-ms-column-width": '',
							"column-gap": '',
							"-webkit-column-gap":'',
							"-ms-column-gap":'',
							"-moz-column-gap":''
						});
			
						ele_view_wrapper.css("overflow-x", "");
						ele_view_wrapper.css("overflow-y", "scroll");
					}
			
					this.maxImageWidth = _scrollMode ? _viewer_width : _columnWidth;
					this.maxImgW = (this.maxImageWidth * 0.95);
					this.maxImgH = (_viewer_height * 0.95);
					this.imgMaxRatio = 1.0;
					this.imgHeight = 0;

					var obj = this;
		
					// var mW = this.maxImgW ;
					// var mH = this.maxImgH ;
					if(!_scrollMode) {
						ele_view_wrapper.find("img,audio,video,svg,iframe").filter(function () {
							if (this.tagName === 'IFRAME'){
								var doc = this.contentDocument || this.contentWindow.document;
								var video = doc.body.querySelector('video');
								var audio = doc.body.querySelector('audio');
								if (video){
									var vRect = video.getBoundingClientRect();
									var _ratio = obj.maxImgW / vRect.width;
									var _vHeight = vRect.height * _ratio;
									if (_vHeight > obj.maxImgH){
										_vHeight = obj.maxImgH;
									}
									$(this).css("width", obj.maxImgW);
									$(this).css("height", _vHeight);
									$(video).css("max-width", obj.maxImgW);
									$(video).css("max-height", _vHeight);
								} 
								// if (audio){
								// 	var vRect = audio.getBoundingClientRect();
								// 	var _ratio = obj.maxImgW / vRect.width;
								// 	var _vHeight = vRect.height * _ratio;
								// 	if (_vHeight > obj.maxImgH){
								// 		_vHeight = obj.maxImgH;
								// 	}
								// 	$(this).css("width", obj.maxImgW);
								// 	$(this).css("height", _vHeight);
								// 	$(audio).css("max-width", obj.maxImgW);
								// 	$(audio).css("max-height", _vHeight);
								// }
								return true;
							}
							
							//sjhan ghost img prevent
							if (this.tagName === 'IMG'){
								var self = this;
								new Promise(function(res,rej){
									self.classList.add('non_ghost_img');
									res(0);
								});
								this.style.margin = '0 auto 0'
							}
							
							//contents가 뷰어사이즈 보다클때.
							if( $(this).width() > obj.maxImgW || $(this).height() > obj.maxImgH) {			
								if($(this).is('video') || $(this).is('audio')) {
									$(this).css("max-width", obj.maxImgW);
									$(this).css("max-height", obj.maxImgH);
								}
								else if($(this).is('svg')) {
									$(this).css("max-width", obj.maxImgW);
									$(this).css("max-height", obj.maxImgH);
								}
								else {
									
									
									orgWidth = $(this).attr("ow");
									orgHeight = $(this).attr("oh");
									iW = $(this).width();
									iH = $(this).height();
									nW = $(this).prop("naturalWidth");
									nH = $(this).prop("naturalHeight");

				
									if (orgWidth == undefined || orgHeight == undefined) {
										$(this).attr("ow", nW);
										$(this).attr("oh", nH);

										if (nW > obj.maxImgW) {
											obj.imgRatio = obj.maxImgW / nW;
				
											$(this).width(obj.maxImgW);
											$(this).height(nH * obj.imgRatio);

											iW = $(this).width();
											iH = $(this).height();

											if(iH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / iH;
				
												$(this).height(obj.maxImgH);
												$(this).width(iW * obj.imgRatio);
											}
										}
										else {
											if(nH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / nH;
				
												$(this).height(obj.maxImgH);
												$(this).width(nW * obj.imgRatio);

												iW = $(this).width();
												iH = $(this).height();

												if (iW > obj.maxImgW) {
													obj.imgRatio = obj.maxImgW / iW;
						
													$(this).width(obj.maxImgW);
													$(this).height(iH * obj.imgRatio);

												}
											}
										}

										iW = $(this).width();
										iH = $(this).height();

										if (iW > obj.maxImgW) {
											obj.imgRatio = obj.maxImgW / iW;
				
											$(this).width(obj.maxImgW);
											$(this).height(iH * obj.imgRatio);

											nW = $(this).width();
											nH = $(this).height();

											if(nH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / nH;
				
												$(this).height(obj.maxImgH);
												$(this).width(nW * obj.imgRatio);
											}
										}
										else {
											if(iH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / iH;
				
												$(this).height(obj.maxImgH);
												$(this).width(iW * obj.imgRatio);

												nW = $(this).width();
												nH = $(this).height();

												if (nW > obj.maxImgW) {
													this.imgRatio = obj.maxImgW / nW;
						
													$(this).width(obj.maxImgW);
													$(this).height(nH * obj.imgRatio);

												}
											}
										}
									}
									else {
										iW = orgWidth;
										iH = orgHeight;

										if (iW > obj.maxImgW) {
											obj.imgRatio = obj.maxImgW / iW;
				
											$(this).width(obj.maxImgW);
											$(this).height(iH * obj.imgRatio);

											nW = $(this).width();
											nH = $(this).height();

											if(nH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / nH;
				
												$(this).height(obj.maxImgH);
												$(this).width(nW * obj.imgRatio);
											}
										}
										else {
											if(iH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / iH;
				
												$(this).height(obj.maxImgH);
												$(this).width(iW * obj.imgRatio);

												nW = $(this).width();
												nH = $(this).height();

												if (nW > obj.maxImgW) {
													this.imgRatio = obj.maxImgW / nW;
						
													$(this).width(obj.maxImgW);
													$(this).height(nH * obj.imgRatio);

												}
											}
										}

										iW = $(this).width();
										iH = $(this).height();

										if (iW > obj.maxImgW) {
											obj.imgRatio = obj.maxImgW / iW;
				
											$(this).width(obj.maxImgW);
											$(this).height(iH * obj.imgRatio);

											nW = $(this).width();
											nH = $(this).height();

											if(nH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / nH;
				
												$(this).height(obj.maxImgH);
												$(this).width(nW * obj.imgRatio);
											}
										}
										else {
											if(iH > obj.maxImgH) {
												obj.imgRatio = obj.maxImgH / iH;
				
												$(this).height(obj.maxImgH);
												$(this).width(iW * obj.imgRatio);

												nW = $(this).width();
												nH = $(this).height();

												if (nW > obj.maxImgW) {
													this.imgRatio = obj.maxImgW / nW;
						
													$(this).width(obj.maxImgW);
													$(this).height(nH * obj.imgRatio);

												}
											}
										}
									}
									
								}
							}
							return $(this).width() > _columnWidth || $(this).height() > _viewer_height;
						})
					}
					else {
						// 책표지 이미지 크기 조정 
						ele_view_wrapper.find("img,audio,video,svg").filter(function() {
							this.style.maxWidth = _viewer_width+'px';
							this.style.maxHeight = 'auto';
							this.style.objectFit = 'scale-down';	
							return $(this).width() > _columnWidth || $(this).height() > _viewer_height;
						})
						.hide()
						.css("max-width", (_columnWidth * 0.8) + "px")
						.css("max-height", (_viewer_height * 0.8) + "px").show();

						//iframe 안쪽에 video를 넣은경우.
						ele_view_wrapper.find('iframe').filter(function(){
							var ifr = this;
							var doc = this.contentDocument || this.contentWindow.document;
							var video = doc.body.querySelector('video');
							if (video){
								video.onloadedmetadata = function(){
									
									var vWidth = this.videoWidth;
									var vHeight = this.videoHeight;
									var _ratio = (_viewer_width / vWidth)*0.8;
									var _vWidth = vWidth*_ratio;
									var _vHeight = vHeight *_ratio;

									$(ifr).css("width", _vWidth);
									$(ifr).css("height", _vHeight);
				
									$(this).css("max-width", _vWidth);
									$(this).css("max-height", _vHeight);
								}
							}
							doc = null;
							video = null;
							return true;
						})
						.hide()
						.show();
					}
		
					_initialized = true;

					if(_scrollMode) {
						Layout.recalChapterHeight();
					}
					else {
						Layout.recalChapterWidth();
					}
					resolve();
				}
				catch(ex) {
					reject(ex);
				}
			});
		},
		syncSteg : function(token , send_item) {
			var rdata;
			$.ajax({
				url : "/Api/GetWatermarkImage",
				crossOrigin : true,
				type:'get',
				async: false,
				headers: {
					'authorization': token
				},
				data : {
					url:send_item,
					token:baseInfo.token
				},
				success : function(receive_data) {
					rdata = receive_data;
				},
				error:function () {
						console.log("복호화 실패");
				}
			});
		
			return rdata;
		},

		/**
		 * 해당 파일의 높이를 지정하고 해당 높이를 리턴한다.
		 * @param {number} idx 파일 번호
		 * @returns 해당 파일의 세로 길이
		 */
		setChapterHeight: function(idx) {

			var chapter = $("#bdb-chapter"+idx);
			var h = parseInt(chapter.height());
			var isLoaded = chapter.attr("loaded");
	
			if(isLoaded != null && isLoaded != undefined && isLoaded == 1) {

				if((h % _viewer_height) > 0) {
					var restHeight = _viewer_height - (h % _viewer_height);
					h += restHeight;
				}

				chapter.attr('style', 'min-height:'+ parseInt(h) + 'px !important;');
				
				h = parseInt(chapter.height());

				return h;
			}
			else {
				chapter.attr('style', 'width:'+ parseInt(_viewer_width) + 'px !important; height:'+ parseInt(_viewer_height) + 'px !important');
				return parseInt(_viewer_height);
			}
		},
		/**
		 * 기존의 style을 모두 지운다.
		 * @param {*} idx 
		 */
		resetChapterStyle: function(idx) {

			var chapter = $("#bdb-chapter"+idx);
			chapter.removeAttr('style');
			chapter.children('.bdb_para_empty').remove();
		},
		/**
		 * 해당 파일의 넓이를 지정하고 해당 넓이를 리턴한다.
		 * @param {number} idx 파일번호
		 * @returns 해당 파일의 넓이
		 */
		setChapterWidth: function(idx) {

			var chapter = $("#bdb-chapter"+idx);
			var isLoaded = chapter.attr("loaded");
			var isExtraFill = true;
			var restWidth = 0;
			var pageWidth = _scrollMode ? _viewer_width : _pageMode ? (_viewer_width / 2 + _columnGapCorrect) : _viewer_width;
			var emptyParaLeft = 0;
			var emptyParaHeight = 0;
			var emptyParaBottom = 0;

			if(isLoaded != null && isLoaded != undefined && isLoaded == 1) {
				var w  = this.getChapterWidth(idx);
				var page = 0;

				if(_pageMode) {

					
					if( w < pageWidth) {

						while(w <= pageWidth || (emptyParaHeight + emptyParaBottom) < _viewer_height) {
							var emptyPara = $('<p class=\"bdb_para_empty\">&nbsp;</p>');
							chapter.append(emptyPara);
							w  = this.getChapterWidth(idx);

							emptyParaLeft = emptyPara.get(0).offsetLeft;
							emptyParaHeight = emptyPara.get(0).offsetHeight;
							emptyParaBottom = emptyPara.get(0).offsetTop + emptyParaHeight;

						}

						page = 2;
					}
					else {
						//한페이지 보다 크면
						restWidth = w % pageWidth;
						page = parseInt(w / pageWidth);


						if(page % 2 > 0) {

							var tempPage = page;

							if(restWidth > 0) {
								page += 1;
							}
							
							while(page % 2 > 0 && restWidth == 0) {
								var emptyPara = $('<p class=\"bdb_para_empty\">&nbsp;</p>');
								chapter.append('<p class=\"bdb_para_empty\">&nbsp;</p>');
								w  = this.getChapterWidth(idx);
								page = parseInt(w / pageWidth);
								restWidth = w % pageWidth;
								ePubEngineLogJS.L.log('setChapterWidth, [2] ['+ idx +'], w : '+ w +', page : '+page + ', pageWidth : ' + pageWidth + ', restWidth : ' + restWidth);

							}
						}
					}
				}
				else {
					/**
					 * sjhan 220323
					 * chapter가 끝나는 지점은 다른 chapter가 침범하면 안된다.
					 * 해당챕터의 마지막 아이템의 마지막 위치의 높이가 현재페이지보다 낮을때 
					 * 현재페이지 - 마지막위치 공간만큼 빈공간을 채워준다.
					 */
					Layout._fillChapterBottom(chapter.get(0))
					
					if( w < _viewer_width) {
						w = _viewer_width;
						page = 1;
					}
					else {
						//한페이지 보다 크면
						page = parseInt(w /_viewer_width);

						restWidth = w % _viewer_width;

						if( restWidth > 0) {
							w += (_viewer_width - restWidth);
							page += 1;
							//isFixedWidth = true;
						}
					}
				}

				chapter.removeAttr('style');
				return {width : w, page: page};
			}
			else {
				chapter.attr('style', 'min-width:'+ parseInt(_viewer_width) + 'px !important; height:'+ parseInt(_viewer_height * 2) + 'px !important');
				return {width : parseInt(_viewer_width), page: _pageMode ? 2 : 1};
			}
		},
		
		/**
		 * 해당 파일의 로딩 완료 여부를 리턴
		 * @param {number} idx 파일번호
		 * @returns true면 로딩이 완료, false면 로딩이 안됨
		 */
		getChapterLoaded: function(idx) {
			var chapter = $("#bdb-chapter"+idx);
			var isLoaded = chapter.attr("loaded");

			return (isLoaded != null && isLoaded != undefined && isLoaded == 1);
		},
		/**
		 * 특정 위치의 해당하는 파일 번호를 리턴
		 * @param {number} pos 위치( 페이지 모드의 경우에는 왼쪽 위치, 세로 스크롤은 scrollTop + 콘텐츠를 넣는 컨테이너의 높이)
		 * @returns 
		 */
		getChapterByPos: function(pos) {
			var idx = 0;
			if(pos <= 0) return idx;

			var total = ePubEngineLoaderJS.Data.getTotalSpine();
			// TODO -- 가장 마지막 위치는 처리 해야 한다.

			for(var i = 0; i < total; i++) {

				if(_scrollMode) {
					if(_arrSpineHeight[i].pos  > pos) {
						break;
					}
				}
				else {
					if(_arrSpineWidth[i].pos > pos) {
						break;
					}
				}

				idx = i;
			}

			return idx;
		},
		/**
		 * 계산된 범위내에서 pos를 돌려준다.
		 * @param {*} pos 
		 * @returns 
		 */
		 getPosByPos: function(pos) {
			let result = null
			let result2 = null
			let m = null

			if(pos < 0) return result;
			
			if (Layout.getScrollMode()){
				m = _mapSpineHeight
			} else {
				m = _mapSpineWidth
			}
			if (m instanceof Map){
				let values = Array.from(m.values()).flatMap(e=>e.pageInfo).sort((a, b)=> a.percent - b.percent)
				for (let i=0; i<values.length; i++){
					let item = values[i]
					let next_item = null
					if (i+1 === values.length){
						if (item.pos < pos){
							result = item
							result2 = null
							break;
						}
					} else {
						if (pos === 0 && item.pos === pos){
							result = item
							result2 = values[1]
							break;
						}
						next_item = values[i+1]
						if (item.pos < pos && next_item.pos>=pos){
							result = item
							result2 = next_item
							break;
						}
					}
					
				}
			}
			return {current: result, next: result2};
		},
		/**
		 * 
		 * @param {number} idx 파일 번호
		 * @param {boolean} diretion true 이면 마지막 위치, false 이면 첫 위치
		 * @returns 
		 */
		getChapterByDiection: function(idx, diretion) {
			var total = ePubEngineLoaderJS.Data.getTotalSpine();

			if(idx < 0 ) return 0;
			
			if(idx >= total) {

				if(_scrollMode) {
					if(_arrSpineHeight[i].pos > pos) {
						return idx;
					}
				}
				else {
					if(_arrSpineWidth[i].pos > pos) {
						return idx;
					}
				}
				return;
			}
			
			if(_scrollMode) {
				if(_arrSpineHeight[i].pos > pos) {
					return idx;
				}
			}
			else {
				if(_arrSpineWidth[i].pos > pos) {
					return idx;
				}
			}

			return total - 1;
		},
		/**
		 * 특정 위치가 포함된 파일을 리턴
		 * @param {number} percent 위치
		 * @returns -1 또는 정상 파일 번호를 리턴
		 */
		getChapterByPercent: function(percent) {
			var findedSpine = null;
			var findedSpineIdx = -1;

			try {
				if((parseFloat(percent)).toFixed(1) == 0.0) return 0;
				if((parseFloat(percent)).toFixed(1) == 100.0) return ePubEngineLoaderJS.Data.getTotalSpine() - 1;

				for(var i = 0; i < ePubEngineLoaderJS.Data.getTotalSpine(); i++) {
					var spine = ePubEngineLoaderJS.Data.getSpineItem(i);

					if(spine != null && spine != undefined) {
						if(parseFloat(spine.percent) > parseFloat(percent)) {
							break;
						}
					}

					findedSpine = spine;
					findedSpineIdx = i;
				}

				if(findedSpine != null && findedSpine != undefined) {
					
				}

				return findedSpineIdx;
			}
			catch(ex) {
				ePubEngineLogJS.e.log(ex);
			}
		},
		/**
		 * 특정 위치에서 이동 가능한 position 정보를 리턴
		 * @param {number} idx 파일번호
		 * @param {number} percent 위치
		 * @returns 0 또는 위치를 리턴한다.
		 */
		getChapterInPosByPercent: function(idx, percent) {
			var fileInfo = null;
			var pageInfo = null;

			if(_scrollMode) {
				if(_mapSpineHeight.has(idx)) {
					fileInfo = _mapSpineHeight.get(idx);

					if(fileInfo != null && fileInfo != undefined) {
						total = fileInfo.pageInfo.length;
		
						for(var i = 0; i < total; i++) {
							if(fileInfo.pageInfo[i].percent > percent) {
								if(pageInfo == null) {
									pageInfo = fileInfo.pageInfo[i];
								}
								break;
							}
							pageInfo = fileInfo.pageInfo[i];
						}
					}
				}
			}
			else {
				if(_mapSpineWidth.has(idx)) {
					fileInfo = _mapSpineWidth.get(idx);

					if(fileInfo != null && fileInfo != undefined) {
						total = fileInfo.pageInfo.length;
		
						if(_pageMode) {
							for(var i = 0; i < total; i += 2) {
								if(fileInfo.pageInfo[i].percent > percent) {
									if(pageInfo == null) {
										pageInfo = fileInfo.pageInfo[i];
									}
									break;
								}
								pageInfo = fileInfo.pageInfo[i];
							}
						}
						else {
							for(var i = 0; i < total; i++) {
								if(fileInfo.pageInfo[i].percent > percent) {
									if(pageInfo == null) {
										pageInfo = fileInfo.pageInfo[i];
									}
									break;
								}
								pageInfo = fileInfo.pageInfo[i];
							}
						}
					}
				}
			}
			
			if(pageInfo != null) {
				ePubEngineLogJS.W.log('getChapterInPosByPercent, found [' + idx +'], percent = '+pageInfo.pos);
				return pageInfo.pos;
			}
			else {
				ePubEngineLogJS.W.log('getChapterInPosByPercent, not found [' + idx +'], percent = '+percent);
				return 0;
			}
		},
		/**
		 * 엘리먼트 위치가 포함된 페이지의 이동 위치를 구한다.
		 * @param {number} idx 파일번호
		 * @param {number} basePos 해당 파일의 기준 위치
		 * @param {number} pos 엘리먼트의 offset top or left postion
		 * @returns 0 or specific position
		 */
		getChapterInPosByElementPos: function(idx, basePos, pos) {
			var fileInfo = null;
			var pageInfo = null;
			var chapter = $("#bdb-chapter"+idx);

			if(_scrollMode) {
				var parentPos = chapter.get(0).offsetTop;
				var elePos = basePos + (pos - parentPos);

				if(_mapSpineHeight.has(idx)) {
					
					fileInfo = _mapSpineHeight.get(idx);

					if(fileInfo != null && fileInfo != undefined) {
						total = fileInfo.pageInfo.length;
		
						for(var i = 0; i < total; i++) {
							if(fileInfo.pageInfo[i].pos > elePos) {
								if(pageInfo == null) {
									pageInfo = fileInfo.pageInfo[i];
								}
								break;
							}
							pageInfo = fileInfo.pageInfo[i];
						}
					}
				}
			}
			else {
				var parentPos = chapter.get(0).offsetLeft;
				var elePos = basePos + (pos - parentPos);

				if(_mapSpineWidth.has(idx)) {
					fileInfo = _mapSpineWidth.get(idx);

					if(fileInfo != null && fileInfo != undefined) {
						total = fileInfo.pageInfo.length;
		
						if(_pageMode) {
							for(var i = 0; i < total; i += 2) {
								if(fileInfo.pageInfo[i].pos > elePos) {
									if(pageInfo == null) {
										pageInfo = fileInfo.pageInfo[i];
									}
									break;
								}
								pageInfo = fileInfo.pageInfo[i];
							}
						}
						else {
							for(var i = 0; i < total; i++) {
								if(fileInfo.pageInfo[i].pos > elePos) {
									if(pageInfo == null) {
										pageInfo = fileInfo.pageInfo[i];
									}
									break;
								}
								pageInfo = fileInfo.pageInfo[i];
							}
						}
					}
				}
			}

			if(pageInfo != null) {
				ePubEngineLogJS.W.log('getChapterInPosByElementPos, found [' + idx +'], pos = '+pageInfo.pos);
				return pageInfo.pos;
			}
			else {
				ePubEngineLogJS.W.log('getChapterInPosByElementPos, not found [' + idx +'], pos = '+pos);
				return 0;
			}
		},
		/**
		 * 해당 파일 번호 챕터의 실제 차지하는 넓이를 구한다.
		 * @param {number} idx 파일번호
		 * @returns 넓이
		 */
		getChapterWidth: function(idx) {
			var total = ePubEngineLoaderJS.Data.getTotalSpine();
			var scrollWidth = ele_view_wrapper.get(0).scrollWidth;
			
			var chapter = $("#bdb-chapter"+idx);
			var nextChapter = null;
			var isLoaded = chapter.attr("loaded");
			var pageWidth = _scrollMode ? _viewer_width : _pageMode ? (_viewer_width / 2 + _columnGapCorrect) * 2 : _viewer_width;

			if(isLoaded != null && isLoaded != undefined && isLoaded == 1) {
				
				if(idx == (total - 1)) {
					pageWidth = scrollWidth - chapter.get(0).offsetLeft;
				}
				else {
					nextChapter = $("#bdb-chapter"+(idx + 1));

					pageWidth = nextChapter.get(0).offsetLeft - chapter.get(0).offsetLeft;
				}
			}
			return pageWidth;
		},
		/**
		 * 페이지 모드에서의 각 파일별 넓이로 해당 파일들의 크기 및 페이지, 
		 * 위치 정보 등을 재 계산한다.
		 */
		recalChapterHeight: function() {

			var pos = 0;
			var i = 0;
			var prePercent = parseFloat(100);
			var total = ePubEngineLoaderJS.Data.getTotalSpine();
			var chapter = null;
			_arrSpineHeight = Array.from({length : total}, () => 0);

			for(i = 0; i < total; i++) {
				var h = this.setChapterHeight(i);
				_arrSpineHeight[i] = {pos : pos , height: h};
				pos += h;
			}
	
			for(i = total - 1; i >= 0; i--) {

				var spine = ePubEngineLoaderJS.Data.getSpineItem(i);
				var w = _arrSpineHeight[i];
				var page = parseInt(w.height / _viewer_height);
		
				var resetPercent = prePercent - parseFloat(spine.percent);
				var pagePerPercent = (resetPercent / page);


				var arrPage = Array.from({length : page}, ()=> 0);
				var pagePercent = parseFloat(spine.percent);
				var pagePos = w.pos;

				for(var j = 0; j < page; j++) {
					arrPage[j] = {percent: pagePercent, pos : pagePos};
					pagePercent += pagePerPercent;
					pagePos += _viewer_height;
				}

				if(!_mapSpineHeight.has(i)) {
					var spineHeightInfo = {height : h, page : page, start: parseFloat(spine.percent), pageInfo: arrPage ,file:i};
					_mapSpineHeight.set(i, spineHeightInfo);
				}
				else {
					var spineHeightInfo = _mapSpineHeight.get(i);

					spineHeightInfo.height = h;
					spineHeightInfo.page = page;
					spineHeightInfo.start = parseFloat(spine.percent);
					spineHeightInfo.pageInfo = arrPage;
				}

				prePercent = parseFloat(spine.percent);

				//컨텐츠가 배경색을 가지고있을경우 li chapter에 색을 넣어준다..
				if (spine&&spine.bgcolor&&spine.bgcolor.length>0){
					chapter = document.getElementById('bdb-chapter'+i);
					if (chapter){
						// chapter.style.backgroundColor = spine.bgcolor;
						chapter.style.setProperty('background-color', spine.bgcolor,'important');
					}
				}
			}
			pos = null;
			i = null;
			prePercent = null;
			total = null;
			chapter = null;
		},
		/**
		 * 페이지 모드에서의 각 파일별 넓이로 해당 파일들의 크기 및 페이지, 
		 * 위치 정보 등을 재 계산한다.
		 */
		recalChapterWidth: function() {
	
			var pos = 0;
			var i = 0;
			var prePercent = parseFloat(100);
			var total = ePubEngineLoaderJS.Data.getTotalSpine();
			var scrollWidth = ele_view_wrapper.get(0).scrollWidth;

			var chapter = $("#bdb-chapter0");
			var baseLeft = chapter.get(0).offsetLeft;
			var prevOffset = 0;
			var pageWidth = _scrollMode ? _viewer_width : _pageMode ? (_viewer_width / 2 + _columnGapCorrect) : _viewer_width;

			_arrSpineWidth = Array.from({length : total}, () => 0);

			// ePubEngineLogJS.L.log('recalChapterWidth, [0], left = ' + baseLeft );

			for(i = 0; i < total; i++) {
				chapter = $("#bdb-chapter"+i);
				var isLoaded = chapter.attr("loaded");

				if(!isLoaded) {
					chapter.attr('style', 'min-width:'+ parseInt(_viewer_width) + 'px !important; height:'+ parseInt(_viewer_height * 2) + 'px !important');
				}
				else {
					chapter.removeAttr('style');
				}
			}

			scrollWidth = ele_view_wrapper.get(0).scrollWidth;

			for(i = 0; i < total; i++) {
				chapter = $("#bdb-chapter"+i);

				var left = chapter.get(0).offsetLeft;
				var width = left - prevOffset;
				var isLoaded = chapter.attr("loaded");

				prevOffset = left;

				_arrSpineWidth[i] = {pos : left , width: 0, page: 0, isLoaded: isLoaded};

				if( i > 0 && i < (total - 1)) {
					_arrSpineWidth[i - 1].width = width;
				}
				else {
					if( i == (total - 1)) {
						_arrSpineWidth[i - 1].width = width;
						width = scrollWidth - left;
						_arrSpineWidth[i].width = width;
					}
				}
			}


			for(i = 0; i < total; i++) {
				chapter = $("#bdb-chapter"+i);
				var w = this.setChapterWidth(i);
				var left = chapter.get(0).offsetLeft - baseLeft;

				_arrSpineWidth[i].pos = left;
				_arrSpineWidth[i].page = w.page;
				_arrSpineWidth[i].width = w.width;
			}

			for(i = total - 1; i >= 0; i--) {
				var spine = ePubEngineLoaderJS.Data.getSpineItem(i);
				var w = _arrSpineWidth[i];
				var resetPercent = prePercent - parseFloat(spine.percent);
				var pagePerPercent = (resetPercent / parseFloat( w.page));

				// ePubEngineLogJS.L.log('recalChapterWidth2, ['+ i +'], page = '+ page + ', start = ' + spine.percent + ', file Percent = '+resetPercent+ ', page Percent = '+(resetPercent / page));

				var arrPage = Array.from({length : w.page}, ()=> 0);
				var pagePercent = parseFloat(spine.percent);
				var pagePos = w.pos;
				
				for(var j = 0; j < w.page; j++) {
					arrPage[j] = {percent: pagePercent, pos : pagePos};
					pagePercent += pagePerPercent;
					pagePos += pageWidth;
				}

				if(!_mapSpineWidth.has(i)) {
					var spineWidthInfo = {width : w.width, page : w.page, start: parseFloat(spine.percent), pageInfo: arrPage};
					_mapSpineWidth.set(i, spineWidthInfo);
				}
				else {
					var spineWidthInfo = _mapSpineWidth.get(i);

					spineWidthInfo.width = w.width;
					spineWidthInfo.page = w.page;
					spineWidthInfo.start = parseFloat(spine.percent);
					spineWidthInfo.pageInfo = arrPage;
				}

				prePercent = parseFloat(spine.percent);
				
				//컨텐츠가 배경색을 가지고있을경우.
				if (spine&&spine.bgcolor&&spine.bgcolor.length>0){
					chapter = document.getElementById('bdb-chapter'+i);
					if (chapter){
						chapter.style.setProperty('background-color', spine.bgcolor,'important');
					}
				}

			}

			pos = null;
			i = null;
			prePercent = null;
			total = null;
			scrollWidth = null;
			chapter = null;
			baseLeft = null;
			prevOffset = null;
		    pageWidth = null;
		},
		/**
		 * 특정 위치에 해당하는 파일 정보를 리턴
		 * @param {number} pos 특정 위치
		 * @returns 
		 */
		getCurrentPageInfo: function(pos) {
			return Layout.getCurrentPageInfoOrigin(pos);
		},
		/**
		 * 파일과 scroll position기준으로 현재 페이지정보를 추출
		 * @param {*} fileNo  파일번호
		 * @param {*} percent 페이지 퍼센트
		 * @returns 
		 */
		getPageInfoPos: function(fileNo , pos){
			var _mapSpineData = null;
			let _mData  = null;
			let _pageInfo = null;
			let _pageIndex = null;
			let _nextItem = null;
			try{
				if (Layout.getScrollMode()){
					_mapSpineData = _mapSpineHeight;
				} else {
					_mapSpineData = _mapSpineWidth;
				}
	
				if (_mapSpineData.has(fileNo)){
					_mData = _mapSpineData.get(fileNo)
					_pageInfo = _mData.pageInfo
					
					for (let i=0; i<_pageInfo.length; i++){
						let _cur = _pageInfo[i]
						let _nex = _pageInfo[i+1]
						if (_cur&&_nex&&_cur.pos<=pos && _nex.pos> pos){
							return _cur
						}
					}
				}
				return null
			} catch(e){
				console.error('getNextPageInfo2',e)
				return null
			} finally {
				_mapSpineData = null;
				_mData  = null;
				_pageInfo = null;
				_pageIndex = null;
				_nextItem = null;
			}
		},
		/**
		 * 파일과 퍼센트기준으로 다음 페이지정보를 추출
		 * @param {*} fileNo  파일번호
		 * @param {*} percent 페이지 퍼센트
		 * @returns 
		 */
		getNextPageInfoByFilePercent: function(fileNo, percent){
			var _mapSpineData = null;
			let _mData  = null;
			let _pageInfo = null;
			let _pageIndex = null;
			let _nextItem = null;
			try{
				if (Layout.getScrollMode()){
					_mapSpineData = _mapSpineHeight;
				} else {
					_mapSpineData = _mapSpineWidth;
				}
	
				if (_mapSpineData.has(fileNo)){
					_mData = _mapSpineData.get(fileNo)
					_pageInfo = _mData.pageInfo
					_pageIndex = _pageInfo.findIndex(e=> e.percent === percent)
					if (_pageIndex >= 0){
						 _nextItem = _pageInfo[_pageIndex+1]
						if (_nextItem){
							return _nextItem
						} else {
							//다음 spine 탐색
							if (_mapSpineData.has(fileNo+1)){
								_mData = _mapSpineData.get(fileNo+1)
								_nextItem = _mData.pageInfo[0]
								return _nextItem
							} 
						}
					} else {
						//다음 spine 탐색
						if (_mapSpineData.has(fileNo+1)){
							_mData = _mapSpineData.get(fileNo+1)
							_nextItem = _mData.pageInfo[0]
							return _nextItem
						}
					}
				}
				return null
			} catch(e){
				console.error('getNextPageInfo2',e)
				return null
			} finally {
				_mapSpineData = null;
				_mData  = null;
				_pageInfo = null;
				_pageIndex = null;
				_nextItem = null;
			}
		},
		/**
		 * 양면일때 다음페이지의 인포.
		 * @param {*} info 
		 * @returns 
		 */
		getNextPageInfo: function(info) {
			var result = null;
			var pageInfo = null;
			var _mapSpineData = null;
			if (info){
				//scroll
				if (Layout.getScrollMode()){
					_mapSpineData = _mapSpineHeight;
					if (_mapSpineData.has(info.fileno)){
						pageInfo = _mapSpineData.get(info.fileno).pageInfo[info.pageIndex+1];
						result = {info : pageInfo, fileno: info.fileno, pageIndex:info.pageIndex+1 };
					}
				} else {
					_mapSpineData = _mapSpineWidth;
					if (Layout.getPageMode()){
						if (info.fileno === (ePubEngineLoaderJS.Data.getTotalSpine() - 1)){
							pageInfo = _mapSpineData.get(info.fileno).pageInfo[_mapSpineData.get(info.fileno).pageInfo.length-1];
							result = {info : pageInfo, fileno: info.fileno, pageIndex:info.pageIndex+1 };
						} else {
							pageInfo = _mapSpineData.get(info.fileno).pageInfo[info.pageIndex+1];
							result = {info : pageInfo, fileno: info.fileno, pageIndex:info.pageIndex+1 };
						}
					} 
					
					else {
						if (info.fileno === (ePubEngineLoaderJS.Data.getTotalSpine() - 1)){
							pageInfo = _mapSpineData.get(info.fileno).pageInfo[_mapSpineData.get(info.fileno).pageInfo.length-1];
							result = {info : pageInfo, fileno: info.fileno, pageIndex:info.pageIndex+1 };
						} else {
							if (info.pageIndex === _mapSpineData.get(info.fileno).pageInfo.length-1){
								pageInfo = _mapSpineData.get(info.fileno+1).pageInfo[0];
							} else {
								pageInfo = _mapSpineData.get(info.fileno).pageInfo[info.pageIndex+1];
							}
							result = {info : pageInfo, fileno: info.fileno, pageIndex:info.pageIndex+1 };
						}

					}

				}
			}
			return result;
		},


		getCurrentPageInfoOrigin:function(pos){
			var total = ePubEngineLoaderJS.Data.getTotalSpine();
			var fileInfo = null;
			var pageInfo = null;
			var idx = -1;

			for(var i = 0; i < total; i++) {
				var spineInfo = null;

				if(_scrollMode) {
					spineInfo = _arrSpineHeight[i];
				}
				else {
					spineInfo = _arrSpineWidth[i];
				}

				if(spineInfo.pos > pos) {
					break;
				}

				if(_scrollMode) {
					if(_mapSpineHeight.has(i)) {
						idx = i;
						fileInfo = _mapSpineHeight.get(i);
					}
				}
				else {
					if(_mapSpineWidth.has(i)) {
						idx = i;
						fileInfo = _mapSpineWidth.get(i);
					}
				}
			}

			var pageIndex = -1;
			if(fileInfo != null && fileInfo != undefined) {
				total = fileInfo.pageInfo.length;

				for(var i = 0; i < total; i++) {
					if(fileInfo.pageInfo[i].pos > pos) {
						break;
					}
					pageInfo = fileInfo.pageInfo[i];
					pageIndex = i;
				}
			}

			return {info : pageInfo, fileno: idx, pageIndex:pageIndex };
		},
		getCurrentOffset: function(left, right, direction) {
			var chapter = $("#bdb-chapter0");
			var baseLeft = chapter.get(0).offsetLeft;
			var total = 0;
			var fileInfo = null;
			var rightFileInfo = null;
			var mapInfo = null;

			if(_scrollMode) {
				if(_mapSpineHeight.has(left)) {
					fileInfo = _arrSpineHeight[left];
				}
				if(_mapSpineHeight.has(right)) {
					rightFileInfo = _arrSpineHeight[right];
				}
			}
			else {
				if(_mapSpineWidth.has(left)) {
					fileInfo = _arrSpineWidth[left];
				}
				if(_mapSpineWidth.has(right)) {
					rightFileInfo = _arrSpineWidth[right];
				}
			}

			if(fileInfo != null && fileInfo != undefined) {

				if(_scrollMode) {
					if(_mapSpineHeight.has(left)) {
						mapInfo = _mapSpineHeight.get(left);
					}
				}
				else {
					if(_mapSpineWidth.has(left)) {
						mapInfo = _mapSpineWidth.get(left);
					}
				}

				total = mapInfo.pageInfo.length;

				if(_scrollMode) {
					if(direction) {
						return rightFileInfo.pos;
					}
					else {
						return rightFileInfo.pos;
					}
				}
				else {
					if(_pageMode) {
						if(left != right) {
							return mapInfo.pageInfo[total - 1].pos;
						}
						else {
							if(direction) {
								return fileInfo.pos;
							}
							else {
								return mapInfo.pageInfo[total - 2].pos;
							}
						}
					}
					else {
						if(direction) {
							return fileInfo.pos;
						}
						else {
							return mapInfo.pageInfo[total - 1].pos;
						}
					}
				}
			}

			return baseLeft;

		},
		invalidate: function() {
			lastReadPos = "";
			page = 1;
	
			try {
				lastReadPos = Position.getLastReadPos();
	
				this.init();
	
				if(lastReadPos != "" && lastReadPos != undefined) {
					page = Navi.GetCfiPageNumber(lastReadPos);
					Navi.GotoPage(page);
				}
				else {
					Navi.GotoPage(1);
				}
			}
			catch(ex) {
				ePubEngineLogJS.e.log(ex);
				Navi.GotoPage(1);
			}
			finally {
				page = null;
				lastReadPos = null;
			}
		},
		invalidatePage: function() {
			if(_scrollMode) {
				_currPage = Math.floor(ele_view_wrapper.prop("scrollTop") / _viewer_height) + 1;
			}
			else {
				_currPage = Math.floor(tele_view_wrapper.prop("scrollLeft") / _columnWidth) + 1;
			}
			
			invalidate();
		},
		getAllPageInfo:function(){
			return {
				array_h:_arrSpineHeight,
				array_w:_arrSpineWidth,
				map_h:_mapSpineHeight,
				map_w:_mapSpineWidth,
			}
		},
		_onScrollRequest: false,
		checkCurrentScrollInfo: function(){
			if(!Layout.getScrollMode()) {
				return;
			}

			if (Layout._onScrollRequest){
				return;
			}
			
			var curScrollTop = ele_view_wrapper.scrollTop();
			var topChapter = Layout.getChapterByPos(curScrollTop);
			var bottomChapter = Layout.getChapterByPos(curScrollTop + Layout.getViewerHeight());		
			var nextMovePos = curScrollTop + Layout.getViewerHeight();

			if(topChapter == bottomChapter) {
				var isLoaded = ePubEngineCoreJS.Layout.getChapterLoaded(topChapter);
				//로드된 애들
				if(isLoaded) {
					Layout.onSetInfo(topChapter, curScrollTop, nextMovePos);
				}
				//로드 안된애들
				else {
					//이동을 한 후에 로딩을 시킨다.
					ele_view_wrapper.disablescroll();
					ele_loading_bar.show();
					Layout._onScrollRequest = true
					ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
						.then(function(result) {
							ePubEngineCoreJS.Layout.init()
								.then(function() {
									Layout.onSetInfo(topChapter, curScrollTop, nextMovePos);
									Layout._onScrollRequest = false
								})
								.catch((err) => onErrorCall(-1, err));
						})
						.catch((err) => onErrorCall(-2, err));
				}
			}
			else {
				var isLoadedLeft = Layout.getChapterLoaded(topChapter);
				var isLoadedRight = Layout.getChapterLoaded(bottomChapter);
				
				if(isLoadedLeft && isLoadedRight) {
					Layout.onSetInfo(topChapter, curScrollTop, nextMovePos);
				}
				else {
					if(isLoadedLeft) {
						ele_loading_bar.show();
						ele_view_wrapper.disablescroll();
						Layout._onScrollRequest = true
						ePubEngineLoaderJS.Data.loadSpineAt(bottomChapter)
							.then(function(result) {
								ePubEngineCoreJS.Layout.init()
									.then(function() {
										Layout.onSetInfo(topChapter, curScrollTop, nextMovePos);
										Layout._onScrollRequest = false
									})
									.catch((err) => onErrorCall(-1, err));
							})
							.catch((err) => onErrorCall(-2, err));
					}
					else {
						ele_loading_bar.show();
						ele_view_wrapper.disablescroll();
						Layout._onScrollRequest = true
						ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
							.then(function(result) {
								ePubEngineCoreJS.Layout.init()
									.then(function() {
										Layout.onSetInfo(topChapter, curScrollTop, nextMovePos);
										Layout._onScrollRequest = false
									})
									.catch((err) => onErrorCall(-1, err));
							})
							.catch((err) => onErrorCall(-2, err));
					}
				}
			}
		},
		/**
		 * 에러 콜백
		 * @param {*} errorCode 
		 * @param {*} error 
		 */
		onErrorCall: function(errorCode, error){
			ePubEngineLogJS.E.log(errorCode , error);
			if (callbackError){
				callbackError(errorCode, error);
			}
			ele_view_wrapper.disablescroll('undo');
		},
		/**
		 * 스크롤 정보 콜백
		 * @param {*} chapter 
		 * @param {*} currentPosition 
		 * @param {*} nextPosition 
		 */
		onSetInfo: function(chapter , currentPosition, nextPosition){
			ePubEngineLoaderJS.Data.setCurrentChapter(chapter);
			var info = ePubEngineCoreJS.Layout.getCurrentPageInfo(currentPosition);
			var info_r = ePubEngineCoreJS.Layout.getCurrentPageInfo(nextPosition);

			if (callbackPosition){
				callbackPosition(info , info_r)
			}
			// View.setCurrentPosition(info , info_r);

			ele_view_wrapper.scrollTop(currentPosition);
			ele_loading_bar.hide();
			ele_view_wrapper.disablescroll('undo');
		},

		/**
		 * 배경색의 밝기찾아 기준치보다 밝으면 보기에 제일 편한 색으로 변경해준다.
		 * @param {*} spineNo 파일번호
		 * @param {*} desireBackground  배경색
		 * @param {*} desireTextcolor 택스트색
		 * * @param {*} isReset reset 여부
		 */
		updateBackgroundColor:function(spineNo ,isReset){
			if (!ele_view_wrapper){
				return
			}
			let main = ele_view_wrapper.get(0)
			let ch = main.querySelector('#bdb-chapter'+spineNo)	
			let chList = ePubEngineLoaderJS.Parser.getElementChildList(ch)
			if (chList == null){
				return;
			}
			let flist = chList.filter(e=> !e.classList.contains('bdb_para_empty'))
			let rex = 'rgb'
			let condition = null
			let regex = /[^0-9]/g;
			//변경되어야할 여부
			let checkerUpdate = false
			//이미 checker class가 되어있는지 여부
			let beforeHasBrightnessClass = false

			//case1: chapter li color
			if (isReset){
				if (ch.classList.contains('bdb-brightness-checker')){
					ch.classList.remove('bdb-brightness-checker')
				}
			} else {
				if (ch.classList.contains('bdb-brightness-checker')){
					beforeHasBrightnessClass = true
				}
				//배경색 값을 찾음
				let _chapterColor = ePubEngineLoaderJS.Parser.getPropertyValue(ch,'background-color')
				if (_chapterColor){
					condition = _chapterColor.startsWith(rex)
					if (condition){
						let result = _chapterColor.replace(regex, " ");
						let rgbList = result.split(" ").filter(e=>e.length>0)
						let bright = null
						if (rgbList && rgbList.length>0){
							//rgb
							if (rgbList.length === 3){
								bright = Layout.brightnessFromRGB(parseInt(rgbList[0]),parseInt(rgbList[1]),parseInt(rgbList[2]))
								if (bright > 70){
									checkerUpdate = true	
								}
							} 
							//rgba
							else if (rgbList.length === 4){
								bright = Layout.brightnessFromRGB(parseInt(rgbList[0]),parseInt(rgbList[1]),parseInt(rgbList[2]))
								if (bright > 70){
									checkerUpdate = true
								}
							}
						}
						result = null
						rgbList = null
						bright = null
					}

					//초기화
					if (!checkerUpdate){
						if (beforeHasBrightnessClass){

						} else {
							if (ch.classList.contains('bdb-brightness-checker')){
								ch.classList.remove('bdb-brightness-checker')
							}
						}
					}
					//밝은 색인경우
					else {
						if (!ch.classList.contains('bdb-brightness-checker')){
							ch.classList.add('bdb-brightness-checker')
						}
					}
				}
			}
		

			//case2: chapter li child color
			for (let i = 0; i<flist.length; i++){
				condition = null
				checkerUpdate = false
				beforeHasBrightnessClass = false
				let e = flist[i]
				let bgc = null
				
				//이전에 이미 클래스를 가지고잇는지 체크
				if (isReset){
					if (e.classList.contains('bdb-brightness-checker')){
						e.classList.remove('bdb-brightness-checker')
					}
				} else {
					if (e.classList.contains('bdb-brightness-checker')){
						beforeHasBrightnessClass = true
					}
					//배경색 값을 찾음
					bgc = ePubEngineLoaderJS.Parser.getPropertyValue(e,'background-color')
					if (bgc){
						condition = bgc.startsWith(rex)

						if (condition){
							let result = bgc.replace(regex, " ");
							let rgbList = result.split(" ").filter(e=>e.length>0)
							let bright = null
							if (rgbList && rgbList.length>0){
								//rgb
								if (rgbList.length === 3){
									bright = Layout.brightnessFromRGB(parseInt(rgbList[0]),parseInt(rgbList[1]),parseInt(rgbList[2]))
									if (bright > 70){
										checkerUpdate = true	
									}
								} 
								//rgba
								else if (rgbList.length === 4){
									bright = Layout.brightnessFromRGB(parseInt(rgbList[0]),parseInt(rgbList[1]),parseInt(rgbList[2]))
									if (bright > 70){
										checkerUpdate = true
									}
								}
							}
							result = null
							rgbList = null
						}
					}
	
					//초기화
					if (!checkerUpdate){
						if (beforeHasBrightnessClass){

						} else {
							if (e.classList.contains('bdb-brightness-checker')){
								e.classList.remove('bdb-brightness-checker')
							}
						}
					}
					//밝은 색인경우
					else {
						if (!e.classList.contains('bdb-brightness-checker')){
							e.classList.add('bdb-brightness-checker')
						}
					}
				}
				

				e = null
				bg = null
				bgc = null
				condition = null
			}

			ch = null
			chList = null
			flist = null
			rex = null
			condition = null
		},
		/**
		 * rgb에서 밝기정도로 추출 100%에 가까울수록 밝은색
		 * @param {*} Rint 
		 * @param {*} Gint 
		 * @param {*} Bint 
		 * @returns 
		 */
		brightnessFromRGB: function(Rint,Gint,Bint){
			var Rlin = (Rint / 255.0) ** 2.218;   // Convert int to decimal 0-1 and linearize
			var Glin = (Gint / 255.0) ** 2.218;   // ** is the exponentiation operator, older JS needs Math.pow() instead
			var Blin = (Bint / 255.0) ** 2.218;   // 2.218 Gamma for sRGB linearization. 2.218 sets unity with the piecewise sRGB at #777 .... 2.2 or 2.223 could be used instead
		
			var Ylum = Rlin * 0.2126 + Glin * 0.7156 + Blin * 0.0722;   // convert to Luminance Y
		
			return Math.pow(Ylum, 0.43) * 100;  // Convert to lightness (0 to 100)
		},
		/**
		 * single page mode only
		 * chapter 하단에 빈공간만큼 p tag를 채운다.
		 * @param { Element } chapterElement pure javascript element
		 */
		_fillChapterBottom: function(chapterElement){
			let _emptyClassName = 'bdb-empty-spacer'
			let clist = chapterElement.children;
			let _alreadyEmptyspaceCount = 0
			let _mainRect = null
			let _lastChild = null
			let _lrect2 = null
			let _lrect2Last = null
			let _lrect2LastHeight = null
			let _lrect2LastTopmargin = null
			let remain = null
			let ptg = null
			let m1  = 0

			try {
				//chapter의 childeren이 존재하지 않는경우 return
				if (!clist){
					return
				}
				//빈공간 채워주는 ptag가 존재하는지 조회
				for (let _mItem of clist){
					if (_mItem.classList.contains(_emptyClassName)){
						_alreadyEmptyspaceCount+=1
					}
				}
				//한번이라도 공간을 채워넣은 케이스면 return
				if (_alreadyEmptyspaceCount > 0){
					return
				}
				_mainRect = ele_view_wrapper.get(0).getBoundingClientRect()

				_lastChild = clist.item(clist.length-1)
				m1 = getComputedStyle(_lastChild).getPropertyValue("margin");
				if (m1){
					m1 = parseInt(m1)
				}
				_lrect2 = _lastChild.getClientRects()
				if (_lrect2.length === 0){
					return
				}
				_lrect2Last = _lrect2[_lrect2.length-1]
				_lrect2LastTopmargin = _lrect2Last.top - _mainRect.top
				_lrect2LastHeight = _lrect2Last.height + _lrect2LastTopmargin
				
				
				//마지막 엘리먼트의 사이즈가 뷰어높이보다 낮을 경우 빈공간을 넣는다.
				if (_lrect2LastHeight<_viewer_height){
					remain = _viewer_height - _lrect2LastHeight
					ptg = document.createElement('p')
					ptg.classList.add(_emptyClassName)
					ptg.style.height = (remain - m1) * 1 + 'px'
					ptg.style.width = _viewer_width * 1 + 'px'
					ptg.style.margin = 0
					ptg.style.padding = 0
					chapterElement.appendChild(ptg)
				}
			} catch(err){
				console.error('_fillChapterBottom error',err)
			} finally {
				_emptyClassName = null
				_mainRect = null
				clist = null
				_lastChild = null
				_lrect2 = null
				_lrect2Last = null
				_lrect2LastHeight = null
				remain = null
				ptg = null
				_alreadyEmptyspaceCount = null
				_correctSize = null
				m1 = null
			}
		},
	};

	
	/**
	 * error code
	 */
	 const ERROR_MSG_LOAD_FAIL = -1; // loader에서 spine load중 에러 발생시.
	 const ERROR_MSG_LAYOUT_INIT_FAIL = -2; // core에서 layout init 실패시
	 const ERROR_MSG_PERCENT_NOT_IN_RANGE = -3; //퍼센트 범위내에 존재하지 않는 경우
	 
	var Navi = Core.Navi = {
		GetNodePageNumber: function(node) {
			left = 0, top = 0;
			nMovePageNo = null;
			chapter_left = null, chapter_top = null;
	
			try {
				if(_scrollMode) {
					chapter_top = $(node).prop("offsetTop") - top;
					nMovePageNo = parseInt(parseInt(chapter_top, 10) / _viewer_height, 10);
				}
				else {
					chapter_left = $(node).prop("offsetLeft") - left;
					nMovePageNo = parseInt(parseInt(chapter_left, 10) / _columnWidth, 10);
				}
				
				return parseInt(nMovePageNo, 10) + 1;
			}
			catch (ex) {
				return -1;
			}
			finally {
				left = null;
				nMovePageNo = null;
				chapter_left = null;
				top = null;
				chapter_top = null;
			}
		},
		/**
		 * 이전 페이지 이동
		 */
		GotoPrevPage: function() {
			return this.GotoPage(false);			
		},
		/**
		 * 다음 페이지 이동
		 */
		GotoNextPage: function() {
			return this.GotoPage(true); 
			
		},
		/**
		 * 페이지 이동
		 * @param {boolean} direction 방향 false 이면 이전, true 이면 다음.
		 */
		GotoPage: function(direction) {
			//scroll 모드
			if (Layout.getScrollMode()){
				var curPos = ele_view_wrapper.scrollTop();
				if (direction){
					curPos += ePubEngineCoreJS.Layout.getViewerHeight();
				} else {
					curPos -= ePubEngineCoreJS.Layout.getViewerHeight();
				}
				if(curPos < 0) curPos = 0;
	
				ele_view_wrapper.scrollTop(curPos);
				let isbottom = (ele_view_wrapper.get(0).scrollHeight - ele_view_wrapper.get(0).scrollTop) === ele_view_wrapper.get(0).clientHeight;
				if (isbottom){
					return -2;
				}
				return;
				
			}


			var curLeft = ele_view_wrapper.scrollLeft();
			var scrollEndPos = ele_view_wrapper.get(0).scrollWidth;
			var extraPos = _pageMode ? _columnGapDefault : 0;
			var gotoPos = direction ? curLeft + ( _viewer_width + extraPos) : curLeft - (_viewer_width + extraPos);
			var gotoLeft = gotoPos;
			var gotoRight = gotoPos;

			if (ePubEngineCoreJS.Selection.IsSelected){
				ePubEngineCoreJS.Selection.DeleteSelection();
			}

			Highlight.deleteSearchHighlight();

			
			if(direction) {
				let rst = Math.ceil(curLeft) + _viewer_width
				if(rst >= scrollEndPos) {
					// 마지막 페이지 입니다.
					return -2;
				}
			}
			else {
				if(curLeft <= 0) {
					//첫 페이지 입니다.
					return -1;
				}
			}

			if(_pageMode) {
				gotoLeft = gotoPos ;
				gotoRight = gotoPos + (_viewer_width/ 2) + _columnGapCorrect ;
				// gotoRight = gotoPos + (_viewer_width/ 2) + _columnGapDefault ;
				
			}

			ePubEngineLogJS.W.log('GotoPage, curLeft = ' + curLeft);
			ePubEngineLogJS.W.log('GotoPage, scrollEndPos = ' + scrollEndPos);
			ePubEngineLogJS.W.log('GotoPage, move scroll = ' + gotoPos);

			// 이동할 위치의 챕터를 찾는다.
			var leftChapter = Layout.getChapterByPos(gotoLeft);
			var rightChapter = Layout.getChapterByPos(gotoRight);


			if(leftChapter == -1 && rightChapter == -1) {
				if(scrollEndPos < gotoPos) {
					ele_view_wrapper.scrollLeft(gotoPos);
				}
				return;
			}

			if(leftChapter == rightChapter) {
				var isLoaded = Layout.getChapterLoaded(leftChapter);

				if(isLoaded) {
					ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

					ele_view_wrapper.scrollLeft(gotoPos);

					var info = Layout.getCurrentPageInfo(gotoLeft);
					var info_r = Layout.getCurrentPageInfo(gotoRight);
					// if (_pageMode){
						// info_r =Layout.getNextPageInfo(info);
					// } 
					callbackPosition(info, info_r);
				}
				else {
					//이동을 한 후에 로딩을 시킨다.
					ele_loading_bar.show();

					ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
						.then(function(result) {
							Layout.init()
								.then(function() {
									ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

									gotoPos = Layout.getCurrentOffset(leftChapter, rightChapter, direction);

									if(_pageMode) {
										gotoLeft = gotoPos;
										gotoRight = gotoLeft + _columnWidth + extraPos + 1;
									}

									ele_view_wrapper.scrollLeft(gotoPos);
									ele_loading_bar.hide();

									var info = Layout.getCurrentPageInfo(gotoLeft);
									var info_r = Layout.getCurrentPageInfo(gotoRight);
									// if (_pageMode){
										info_r =Layout.getNextPageInfo(info);
									// } 
									callbackPosition(info, info_r);
								})
								.catch(function() {
									// ele_loading_bar.hide();
									if (callbackError){
										callbackError();
									}
								});
						})
						.catch(function(err) {
							ePubEngineLogJS.E.log(err);
							if (callbackError){
								callbackError();
							}
						});
				}
			}
			else {
				var isLoadedLeft = Layout.getChapterLoaded(leftChapter);
				var isLoadedRight = rightChapter >= 0 ? Layout.getChapterLoaded(rightChapter) : false;
				
				if(isLoadedLeft && isLoadedRight) {
					ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

					ele_view_wrapper.scrollLeft(gotoPos);

					var info = Layout.getCurrentPageInfo(gotoLeft);
					var info_r = Layout.getCurrentPageInfo(gotoRight);
					// if (_pageMode){
						info_r =Layout.getNextPageInfo(info);
					// } 
					callbackPosition(info, info_r);
				}
				else {
					if(isLoadedLeft) {
						ele_loading_bar.show();

						ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
							.then(function(result) {
								Layout.init()
									.then(function() {

										ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

										gotoPos = Layout.getCurrentOffset(leftChapter, rightChapter, direction);

										if(_pageMode) {
											gotoLeft = gotoPos;
											gotoRight = gotoLeft + _columnWidth + extraPos + 1;
										}

										ele_view_wrapper.scrollLeft(gotoPos);
										ele_loading_bar.hide();

										var info = Layout.getCurrentPageInfo(gotoLeft);
										var info_r = Layout.getCurrentPageInfo(gotoRight);
										// if (_pageMode){
											info_r =Layout.getNextPageInfo(info);
										// } 
										callbackPosition(info, info_r);
									})
									.catch(function() {
										// ele_loading_bar.hide();
										if (callbackError){
											callbackError();
										}
									});
							})
							.catch(function(err) {
								ePubEngineLogJS.E.log(err);
								if (callbackError){
									callbackError();
								}
							});
					}
					else {
						ele_loading_bar.show();

						ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
							.then(function(result) {
								Layout.init()
									.then(function() {

										ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

										gotoPos = Layout.getCurrentOffset(leftChapter, rightChapter, direction);

										if(_pageMode) {
											gotoLeft = gotoPos;
											gotoRight = gotoLeft + _columnWidth + extraPos + 1;
										}

										ele_view_wrapper.scrollLeft(gotoPos);
										ele_loading_bar.hide();

										var info = Layout.getCurrentPageInfo(gotoLeft);
										var info_r = Layout.getCurrentPageInfo(gotoRight);
										// if (_pageMode){
											info_r =Layout.getNextPageInfo(info);
										// } 
										callbackPosition(info, info_r);
									})
									.catch(function() {
										// ele_loading_bar.hide();
										if (callbackError){
											callbackError();
										}
									});
							})
							.catch(function(err) {
								ePubEngineLogJS.E.log(err);
								if (callbackError){
									callbackError();
								}
							});
					}
				}
			}
		},
		/**
		 * 페이지 이동 
		 * @param {boolean} direction 방향 false 이면 이전, true 이면 다음.
		 * @returns {Promise} number -1 첫페이지,-2: 마지막 페이지, object {left,right}
		 */
		 GotoPageAsync: function(direction) {
			var curLeft = ele_view_wrapper.scrollLeft();
			var scrollEndPos = ele_view_wrapper.get(0).scrollWidth;
			var extraPos = _pageMode ? _columnGapDefault : 0;
			var gotoPos = direction ? curLeft + ( _viewer_width + extraPos) : curLeft - (_viewer_width + extraPos);
			var gotoLeft = gotoPos;
			var gotoRight = gotoPos;

			if (ePubEngineCoreJS.Selection.IsSelected){
				ePubEngineCoreJS.Selection.DeleteSelection();
			}

			Highlight.deleteSearchHighlight();
			ePubEngineCoreJS.Annotation.clearDraw();

			if(direction) {
				if(curLeft + _viewer_width >= scrollEndPos) {
					// 마지막 페이지 입니다.
					return Promise.resolve(-2);
				}
			}
			else {
				if(curLeft <= 0) {
					//첫 페이지 입니다.
					return Promise.resolve(-1);
				}
			}

			if(_pageMode) {
				gotoLeft = gotoPos ;
				gotoRight = gotoPos + (_viewer_width/ 2) + _columnGapCorrect ;
			}
			// 이동할 위치의 챕터를 찾는다.
			var leftChapter = Layout.getChapterByPos(gotoLeft);
			var rightChapter = Layout.getChapterByPos(gotoRight);


			if(leftChapter == -1 && rightChapter == -1) {
				if(scrollEndPos < gotoPos) {
					ele_view_wrapper.scrollLeft(gotoPos);
				}
				return Promise.reject(`leftChapter: ${leftChapter}, rightChapter: ${rightChapter}`)
			}
			return new Promise(function(resolve,reject){
				if(leftChapter == rightChapter) {
					var isLoaded = Layout.getChapterLoaded(leftChapter);
					if(isLoaded) {
						ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
						ele_view_wrapper.scrollLeft(gotoPos);
						var info = Layout.getCurrentPageInfo(gotoLeft);
						var info_r = Layout.getCurrentPageInfo(gotoRight);
						info_r = Layout.getNextPageInfo(info);
						resolve({'left': info,'right':info_r})
					}
					else {
						ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
							.then(function(result) {
								Layout.init()
									.then(function() {
										ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
	
										gotoPos = Layout.getCurrentOffset(leftChapter, rightChapter, direction);
	
										if(_pageMode) {
											gotoLeft = gotoPos;
											gotoRight = gotoLeft + _columnWidth + extraPos + 1;
										}
	
										ele_view_wrapper.scrollLeft(gotoPos);
	
										var info = Layout.getCurrentPageInfo(gotoLeft);
										var info_r = Layout.getCurrentPageInfo(gotoRight);
											info_r =Layout.getNextPageInfo(info);
	
										resolve({'left': info,'right':info_r})
									})
									.catch(function(err) {
										reject(err)
									});
							})
							.catch(function(err) {
								reject(err)
							});
					}
				}
				else {
					var isLoadedLeft = Layout.getChapterLoaded(leftChapter);
					var isLoadedRight = rightChapter >= 0 ? Layout.getChapterLoaded(rightChapter) : false;
					if(isLoadedLeft && isLoadedRight) {
						ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
						ele_view_wrapper.scrollLeft(gotoPos);
						var info = Layout.getCurrentPageInfo(gotoLeft);
						var info_r = Layout.getCurrentPageInfo(gotoRight);
						info_r =Layout.getNextPageInfo(info);
						resolve({'left': info,'right':info_r})
					}
					else {
						if(isLoadedLeft) {
	
							ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
								.then(function(result) {
									Layout.init()
										.then(function() {
	
											ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
	
											gotoPos = Layout.getCurrentOffset(leftChapter, rightChapter, direction);
	
											if(_pageMode) {
												gotoLeft = gotoPos;
												gotoRight = gotoLeft + _columnWidth + extraPos + 1;
											}
	
											ele_view_wrapper.scrollLeft(gotoPos);
	
											var info = Layout.getCurrentPageInfo(gotoLeft);
											var info_r = Layout.getCurrentPageInfo(gotoRight);
												info_r =Layout.getNextPageInfo(info);
												resolve({'left': info,'right':info_r})
										})
										.catch(function(err) {
											reject(err)
										});
								})
								.catch(function(err) {
									reject(err)
								});
						}
						else {
	
							ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
								.then(function(result) {
									Layout.init()
										.then(function() {
	
											ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
	
											gotoPos = Layout.getCurrentOffset(leftChapter, rightChapter, direction);
	
											if(_pageMode) {
												gotoLeft = gotoPos;
												gotoRight = gotoLeft + _columnWidth + extraPos + 1;
											}
	
											ele_view_wrapper.scrollLeft(gotoPos);
	
											var info = Layout.getCurrentPageInfo(gotoLeft);
											var info_r = Layout.getCurrentPageInfo(gotoRight);
											// if (_pageMode){
												info_r =Layout.getNextPageInfo(info);
											// } 
											resolve({'left': info,'right':info_r})
										})
										.catch(function(err) {
											reject(err)
										});
								})
								.catch(function(err) {
									reject(err)
								});
						}
					}
				}
			})
		
		},
		/**
		 * 특정 위치로 이동
		 * @param {number} percent 위치
		 */
		GotoPageByPercent: function(percent) {
			if (!(0> percent || percent > 100)) {         
				
				var moveChapter = Layout.getChapterByPercent(percent);
				var movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;

				ePubEngineLogJS.W.log('GotoPageByPercent, move scroll = ' + movePos);

				
				if (ePubEngineCoreJS.Selection.IsSelected){
					ePubEngineCoreJS.Selection.DeleteSelection();
				}

				Highlight.deleteSearchHighlight();

				if(_scrollMode) {
					var nextMovePos = movePos + _viewer_height;
					var topChapter = Layout.getChapterByPos(movePos);
					var bottomChapter = Layout.getChapterByPos(nextMovePos);

					if(topChapter == bottomChapter) {
						var isLoaded = Layout.getChapterLoaded(topChapter);
		
						if(isLoaded) {
							ePubEngineLoaderJS.Data.setCurrentChapter(topChapter);
		
							ele_view_wrapper.scrollTop(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							callbackPosition(info, info_r);
						}
						else {
							//이동을 한 후에 로딩을 시킨다.
							ele_loading_bar.show();
		
							ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
								.then(function(result) {
									Layout.init()
										.then(function() {
											
											movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;

											ele_view_wrapper.scrollTop(movePos);		
											ele_loading_bar.hide();
		
											var info = Layout.getCurrentPageInfo(movePos);
											var info_r = Layout.getCurrentPageInfo(nextMovePos);
											callbackPosition(info, info_r);
										})
										.catch(function() {
											ele_loading_bar.hide();
										});
								})
								.catch(function(err) {
									ePubEngineLogJS.E.log(err);
								});
						}
					}
					else {
						var isLoadedLeft = Layout.getChapterLoaded(topChapter);
						var isLoadedRight = Layout.getChapterLoaded(bottomChapter);
						
						if(isLoadedLeft && isLoadedRight) {
							ePubEngineLoaderJS.Data.setCurrentChapter(topChapter);
		
							ele_view_wrapper.scrollTop(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							callbackPosition(info, info_r);
						}
						else {
							if(isLoadedLeft) {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(bottomChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
		
												ele_view_wrapper.scrollTop(movePos);
												ele_loading_bar.hide();
		
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);
												callbackPosition(info, info_r);
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
							else {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
		
												ele_view_wrapper.scrollTop(movePos);
												ele_loading_bar.hide();
		
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);
												callbackPosition(info, info_r);
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
						}
					}
				}
				else {
					var extraPos = parseInt(movePos / _viewer_width) * _columnGapWidth;
					var nextMovePos = movePos + _columnWidth;
					var leftChapter = Layout.getChapterByPos(movePos);
					var rightChapter = Layout.getChapterByPos(nextMovePos);
					//이동되어질 페이지의 넓이.
					var estimateNextPageWidth = _pageMode ? (_viewer_width / 2 + _columnGapCorrect) : _viewer_width;

					if(leftChapter == rightChapter) {
						var isLoaded = Layout.getChapterLoaded(leftChapter);
		
						if(isLoaded) {
							ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
		
							ele_view_wrapper.scrollLeft(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							callbackPosition(info, info_r);
						}
						else {
							//이동을 한 후에 로딩을 시킨다.
							ele_loading_bar.show();
		
							ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
								.then(function(result) {
									Layout.init()
										.then(function() {

											ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

											movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
											nextMovePos = movePos + _columnWidth;
											
											if(_pageMode) {
												nextMovePos = movePos + estimateNextPageWidth;
											}

											ele_view_wrapper.scrollLeft(movePos);
											ele_loading_bar.hide();

											var info = Layout.getCurrentPageInfo(movePos);
											var info_r = Layout.getCurrentPageInfo(nextMovePos);
											callbackPosition(info, info_r);
										})
										.catch(function() {
											ele_loading_bar.hide();
										});
								})
								.catch(function(err) {
									ePubEngineLogJS.E.log(err);
								});
						}
					}
					else {
						var isLoadedLeft = Layout.getChapterLoaded(leftChapter);
						var isLoadedRight = Layout.getChapterLoaded(rightChapter);
						
						if(isLoadedLeft && isLoadedRight) {
							ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
		
							ele_view_wrapper.scrollLeft(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							callbackPosition(info, info_r);
						}
						else {
							if(isLoadedLeft) {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {
		
												ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												nextMovePos = movePos + _columnWidth;
												if(_pageMode) {
													nextMovePos = movePos + estimateNextPageWidth;
												}
												ele_view_wrapper.scrollLeft(movePos);
												ele_loading_bar.hide();

												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);
												callbackPosition(info, info_r);
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
							else {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {

												ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												nextMovePos = movePos + _columnWidth;
					
												ele_view_wrapper.scrollLeft(movePos);
												ele_loading_bar.hide();
												if(_pageMode) {
													nextMovePos = movePos + estimateNextPageWidth;
												}
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);
												callbackPosition(info, info_r);
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
						}
					}
				}
			}
		},

		/**
		 * 특정 위치로 이동
		 * @param {number} percent 위치
		 */
		GotoPageByPercentAsync: function(percent,fileno) {


			return new Promise((rs , rj ) =>{
				if (!(0> percent || percent > 100)) {         
					var moveChapter = -1;
					if (typeof fileno === 'number'){
						moveChapter = fileno
					} else {
						moveChapter = Layout.getChapterByPercent(percent);
					}
					
					var movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
					var extraPos = _pageMode ? _columnGapDefault : 0;
					
					if (ePubEngineCoreJS.Selection.IsSelected){
						ePubEngineCoreJS.Selection.DeleteSelection();
					}
	
					Highlight.deleteSearchHighlight();
	
					//scroll mode
					if(_scrollMode) {
						var nextMovePos = movePos + _viewer_height;
						var topChapter = Layout.getChapterByPos(movePos);
						var bottomChapter = Layout.getChapterByPos(nextMovePos);
	
						//
						if(topChapter == bottomChapter) {
							var isLoaded = Layout.getChapterLoaded(topChapter);
							//이미 로딩됨 (chaptor element attribute에 loaded 1 로 표시된 경우) 
							if(isLoaded) {
								ePubEngineLoaderJS.Data.setCurrentChapter(topChapter);
								ele_view_wrapper.scrollTop(movePos);
								var info = Layout.getCurrentPageInfo(movePos);
								var info_r = Layout.getCurrentPageInfo(nextMovePos);
								// info_r =Layout.getNextPageInfo(info);
								
								var infoObj = {left: info , right: info_r}
								rs(infoObj);
							}
							//로딩안된경우 새 chaptor를 불러온다.
							else {
								ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {
												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												ele_view_wrapper.scrollTop(movePos);		
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);
												// info_r =Layout.getNextPageInfo(info);
												var infoObj = {left: info , right: info_r}
												rs(infoObj);
											})
											.catch(function(err) {
												rj(ERROR_MSG_LAYOUT_INIT_FAIL , err);
											});
									})
									.catch(function(err) {
										rj(ERROR_MSG_LOAD_FAIL , err);
									});
							}
						}
						else {
							var isLoadedLeft = Layout.getChapterLoaded(topChapter);
							var isLoadedRight = Layout.getChapterLoaded(bottomChapter);
							
							if(isLoadedLeft && isLoadedRight) {
								ePubEngineLoaderJS.Data.setCurrentChapter(topChapter);
			
								ele_view_wrapper.scrollTop(movePos);
			
								var info = Layout.getCurrentPageInfo(movePos);
								var info_r = Layout.getCurrentPageInfo(nextMovePos);
								// info_r =Layout.getNextPageInfo(info);
								var infoObj = {left: info , right: info_r}
								rs(infoObj);
							}
							else {
								if(isLoadedLeft) {
									ePubEngineLoaderJS.Data.loadSpineAt(bottomChapter)
										.then(function(result) {
											Layout.init()
												.then(function() {
													movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
													ele_view_wrapper.scrollTop(movePos);
													var info = Layout.getCurrentPageInfo(movePos);
													var info_r = Layout.getCurrentPageInfo(nextMovePos);
													// info_r =Layout.getNextPageInfo(info);
													var infoObj = {left: info , right: info_r}
													rs(infoObj);
												})
												.catch(function(err) {
													rj(ERROR_MSG_LAYOUT_INIT_FAIL , err);
												});
										})
										.catch(function(err) {
											rj(ERROR_MSG_LOAD_FAIL , err);
										});
								}
								else {
									ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
										.then(function(result) {
											Layout.init()
												.then(function() {
													movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
													ele_view_wrapper.scrollTop(movePos);
													var info = Layout.getCurrentPageInfo(movePos);
													var info_r = Layout.getCurrentPageInfo(nextMovePos);
													// info_r =Layout.getNextPageInfo(info);
													var infoObj = {left: info , right: info_r}
													rs(infoObj);
												})
												.catch(function(err) {
													rj(ERROR_MSG_LAYOUT_INIT_FAIL , err);
												});
										})
										.catch(function(err) {
											rj(ERROR_MSG_LOAD_FAIL , err);
										});
								}
							}
						}
					}
					//한페이지 , 양면
					else {
						var nextMovePos = movePos + _columnWidth;
						var leftChapter = Layout.getChapterByPos(movePos);
						var rightChapter = Layout.getChapterByPos(nextMovePos);
						//이동되어질 페이지의 넓이.
						var estimateNextPageWidth = Layout.getPageMode() ? (_viewer_width / 2) + _columnGapCorrect: _viewer_width;

						if(leftChapter == rightChapter) {
							var isLoaded = Layout.getChapterLoaded(leftChapter);
			
							if(isLoaded) {
								ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
								ele_view_wrapper.scrollLeft(movePos);
								var info = Layout.getCurrentPageInfo(movePos);
								var info_r =  Layout.getCurrentPageInfo(nextMovePos);
								// if (_pageMode){
									info_r =Layout.getNextPageInfo(info);
								// } 
								var infoObj = {left: info , right: info_r}
								rs(infoObj);
							}
							else {
								ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {
												ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												// nextMovePos = movePos + _columnWidth;
												if(_pageMode) {
													nextMovePos = movePos + estimateNextPageWidth;
												}
												ele_view_wrapper.scrollLeft(movePos);
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r =  Layout.getCurrentPageInfo(nextMovePos);
												// if (_pageMode){
													info_r =Layout.getNextPageInfo(info);
												// } 
												var infoObj = {left: info , right: info_r}
												rs(infoObj);
											})
											.catch(function(err) {
												rj(ERROR_MSG_LAYOUT_INIT_FAIL , err);
											});
									})
									.catch(function(err) {
										rj(ERROR_MSG_LOAD_FAIL , err);
									});
							}
						}
						else {
							var isLoadedLeft = Layout.getChapterLoaded(leftChapter);
							var isLoadedRight = Layout.getChapterLoaded(rightChapter);
							
							if(isLoadedLeft && isLoadedRight) {
								ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
								ele_view_wrapper.scrollLeft(movePos);
								var info = Layout.getCurrentPageInfo(movePos);
								var info_r =  Layout.getCurrentPageInfo(nextMovePos);
								// if (_pageMode){
									info_r =Layout.getNextPageInfo(info);
								// } 
								var info_r = Layout.getNextPageInfo(info);
								var infoObj = {left: info , right: info_r}
								rs(infoObj);
							}
							else {
								if(isLoadedLeft) {
									ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
										.then(function(result) {
											Layout.init()
												.then(function() {
													ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
													movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
													// nextMovePos = movePos + _columnWidth;
													if(_pageMode) {
														nextMovePos = movePos + estimateNextPageWidth;
													}
													ele_view_wrapper.scrollLeft(movePos);
													var info = Layout.getCurrentPageInfo(movePos);
													var info_r =  Layout.getCurrentPageInfo(nextMovePos);
													// if (_pageMode){
														info_r =Layout.getNextPageInfo(info);
													// } 
													var infoObj = {left: info , right: info_r}
													rs(infoObj);
												})
												.catch(function(err) {
													rj(ERROR_MSG_LAYOUT_INIT_FAIL , err);
												});
										})
										.catch(function(err) {
											rj(ERROR_MSG_LOAD_FAIL , err);
										});
								}
								else {
									ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
										.then(function(result) {
											Layout.init()
												.then(function() {
													ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
													movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
			
													if(_pageMode) {
														nextMovePos = movePos + estimateNextPageWidth;
													}
													ele_view_wrapper.scrollLeft(movePos);
													var info = Layout.getCurrentPageInfo(movePos);
													var info_r =  Layout.getCurrentPageInfo(nextMovePos);
													// if (_pageMode){
														info_r =Layout.getNextPageInfo(info);
													// } 
													var infoObj = {left: info , right: info_r}
													rs(infoObj);
												})
												.catch(function(err) {
													rj(ERROR_MSG_LAYOUT_INIT_FAIL , err);
												});
										})
										.catch(function(err) {
											rj(ERROR_MSG_LOAD_FAIL , err);
										});
								}
							}
						}
					}
				} else {
					rj(ERROR_MSG_PERCENT_NOT_IN_RANGE);
				}
			});//promise end
			
		},
	
		GotoPageByPercentWithHighlight: function(percent, selector, word, textNode) {
			
			// console.log('##GotoPageByPercentWithHighlight',percent, selector, word, $(selector),$(selector).offset().top)
			if (!(0> percent || percent > 100)) {         
				
				var moveChapter = Layout.getChapterByPercent(percent);
				var movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;

				ePubEngineLogJS.W.log('GotoPageByPercent, move scroll = ' + movePos);

				
				if (ePubEngineCoreJS.Selection.IsSelected){
					ePubEngineCoreJS.Selection.DeleteSelection();
				}

				Highlight.deleteSearchHighlight();

				if(_scrollMode) {
					Layout._onScrollRequest = true;
					let _select = $(selector)
					if (_select && _select.length > 0){
						_element_offset = _select.offset()
						movePos = document.getElementById('contents').scrollTop + (_element_offset.top/2)
					}
					
					var nextMovePos = movePos + _viewer_height;
					var topChapter = Layout.getChapterByPos(movePos);
					var bottomChapter = Layout.getChapterByPos(nextMovePos);

					if(topChapter == bottomChapter) {
						var isLoaded = Layout.getChapterLoaded(topChapter);
		
						if(isLoaded) {
							ePubEngineLoaderJS.Data.setCurrentChapter(topChapter);
							let selMovePos = $(selector).get(0).getBoundingClientRect()
							// movePos += selMovePos.top
							movePos = $(selector).get(0).offsetTop - selMovePos.height

							ele_view_wrapper.scrollTop(movePos);
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							// let c = $(selector).get(0).scrollIntoView()
							

							callbackPosition(info, info_r);
							
							
							
							let test = Search.findSearchDataBySpine(topChapter )
							let test2 = Search.findSearchDataBySpine(bottomChapter )

							var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

							// if(findWordInfo.constructor == Array)
							// {
		

							// 	var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
							// 	var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
								
							// 	if(infoTop < 0) {
							// 		Navi.GotoPrevPage();
							// 	}
							// 	else if(infoTop > _viewer_height) {
							// 		Navi.GotoNextPage();
							// 	}
							// }
							
							Highlight.createSearchHighlight(selector, word , textNode);
							Layout._onScrollRequest = false;	
						}
						else {
							//이동을 한 후에 로딩을 시킨다.
							ele_loading_bar.show();
		
							ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
								.then(function(result) {
									Layout.init()
										.then(function() {
											
											movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
											let selMovePos = $(selector).get(0).getBoundingClientRect()
											// movePos += selMovePos.top
											movePos = $(selector).get(0).offsetTop - selMovePos.height

											ele_view_wrapper.scrollTop(movePos);		
											ele_loading_bar.hide();
		
											var info = Layout.getCurrentPageInfo(movePos);
											var info_r = Layout.getCurrentPageInfo(nextMovePos);

											
											callbackPosition(info, info_r);
											// $(selector).get(0).scrollIntoView()
											var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

											
											// if(findWordInfo.constructor == Array)
											// {
								

											// 	var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
											// 	var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
												
											// 	if(infoTop < 0) {
											// 		Navi.GotoPrevPage();
											// 	}
											// 	else if(infoTop > _viewer_height) {
											// 		Navi.GotoNextPage();
											// 	}
											// }
											
											Highlight.createSearchHighlight(selector, word , textNode);
											Layout._onScrollRequest = false;	
										})
										.catch(function() {
											ele_loading_bar.hide();
										});
								})
								.catch(function(err) {
									ePubEngineLogJS.E.log(err);
								});
						}
					}
					else {
						var isLoadedLeft = Layout.getChapterLoaded(topChapter);
						var isLoadedRight = Layout.getChapterLoaded(bottomChapter);
						
						if(isLoadedLeft && isLoadedRight) {
							ePubEngineLoaderJS.Data.setCurrentChapter(topChapter);
							let selMovePos = $(selector).get(0).getBoundingClientRect()
							// movePos += selMovePos.top
							movePos = $(selector).get(0).offsetTop - selMovePos.height
							ele_view_wrapper.scrollTop(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);

							
							callbackPosition(info, info_r);
							
							var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

							// if(findWordInfo.constructor == Array)
							// {
			

							// 	var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
							// 	var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
								
							// 	if(infoTop < 0) {
							// 		Navi.GotoPrevPage();
							// 	}
							// 	else if(infoTop > _viewer_height) {
							// 		Navi.GotoNextPage();
							// 	}
							// }
							
							Highlight.createSearchHighlight(selector, word, textNode);
							Layout._onScrollRequest = false;	
						}
						else {
							if(isLoadedLeft) {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(bottomChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												let selMovePos = $(selector).get(0).getBoundingClientRect()
												// movePos += selMovePos.top
												movePos = $(selector).get(0).offsetTop - selMovePos.height
												ele_view_wrapper.scrollTop(movePos);
												ele_loading_bar.hide();
		
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);

												
												callbackPosition(info, info_r);
												// $(selector).get(0).scrollIntoView()
												var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

												// if(findWordInfo.constructor == Array)
												// {
									

												// 	var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
												// 	var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
													
												// 	if(infoTop < 0) {
												// 		Navi.GotoPrevPage();
												// 	}
												// 	else if(infoTop > _viewer_height) {
												// 		Navi.GotoNextPage();
												// 	}
												// }
												
												Highlight.createSearchHighlight(selector, word, textNode);
												Layout._onScrollRequest = false;	
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
							else {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(topChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												let selMovePos = $(selector).get(0).getBoundingClientRect()
												// movePos += selMovePos.top
												movePos = $(selector).get(0).offsetTop - selMovePos.height
												
												ele_view_wrapper.scrollTop(movePos);
												ele_loading_bar.hide();
		
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);

											
												callbackPosition(info, info_r);
												// $(selector).get(0).scrollIntoView()
												var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

												// if(findWordInfo.constructor == Array)
												// {
											

												// 	var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
												// 	var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
													
												// 	if(infoTop < 0) {
												// 		Navi.GotoPrevPage();
												// 	}
												// 	else if(infoTop > _viewer_height) {
												// 		Navi.GotoNextPage();
												// 	}
												// }
												
												Highlight.createSearchHighlight(selector, word , textNode);
												Layout._onScrollRequest = false;	
					
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
						}
					}
				}
				else {
					//sjhan
					// var extraPos = parseInt(movePos / _viewer_width) * _columnGapWidth;

					//이동할 다음페이지의 넓이 계산.
					var nextMovePos = 0;
					if (Layout.getPageMode()){
						nextMovePos = movePos +(_viewer_width / 2) + _columnGapCorrect;
					} else {
						nextMovePos = movePos + _viewer_width;
					}

					var leftChapter = Layout.getChapterByPos(movePos);
					var rightChapter = Layout.getChapterByPos(nextMovePos);

					if(leftChapter == rightChapter) {
						var isLoaded = Layout.getChapterLoaded(leftChapter);
		
						if(isLoaded) {
							ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
		
							ele_view_wrapper.scrollLeft(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							callbackPosition(info, info_r);

							var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

							if(findWordInfo.constructor == Array)
							{
						

								var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
								var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
								
								if(infoLeft < 0) {
									Navi.GotoPrevPage();
								}
								else if(infoLeft > _viewer_width) {
									Navi.GotoNextPage();
								}
							}
							
							Highlight.createSearchHighlight(selector, word, textNode);
						}
						else {
							//이동을 한 후에 로딩을 시킨다.
							ele_loading_bar.show();
		
							ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
								.then(function(result) {
									Layout.init()
										.then(function() {

											ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

											movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
											nextMovePos = movePos + _columnWidth;
											
											ele_view_wrapper.scrollLeft(movePos);
											ele_loading_bar.hide();

											var info = Layout.getCurrentPageInfo(movePos);
											var info_r = Layout.getCurrentPageInfo(nextMovePos);
											callbackPosition(info, info_r);

											var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

											if(findWordInfo.constructor == Array)
											{
							

												var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
												var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
												
												if(infoLeft < 0) {
													Navi.GotoPrevPage();
												}
												else if(infoLeft > _viewer_width) {
													Navi.GotoNextPage();
												}
											}
											
											Highlight.createSearchHighlight(selector, word, textNode);
										})
										.catch(function() {
											ele_loading_bar.hide();
										});
								})
								.catch(function(err) {
									ePubEngineLogJS.E.log(err);
								});
						}
					}
					else {
						var isLoadedLeft = Layout.getChapterLoaded(leftChapter);
						var isLoadedRight = Layout.getChapterLoaded(rightChapter);
						
						if(isLoadedLeft && isLoadedRight) {
							ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);
		
							ele_view_wrapper.scrollLeft(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							callbackPosition(info, info_r);

							var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

							if(findWordInfo.constructor == Array)
							{
					

								var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
								var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
								
								if(infoLeft < 0) {
									Navi.GotoPrevPage();
								}
								else if(infoLeft > _viewer_width) {
									Navi.GotoNextPage();
								}
							}
							
							Highlight.createSearchHighlight(selector, word, textNode);
						}
						else {
							if(isLoadedLeft) {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {
		
												ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												nextMovePos = movePos + _columnWidth;
		
												ele_view_wrapper.scrollLeft(movePos);
												ele_loading_bar.hide();

												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);
												callbackPosition(info, info_r);

												var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

												if(findWordInfo.constructor == Array)
												{
											

													var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
													var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
													
													if(infoLeft < 0) {
														Navi.GotoPrevPage();
													}
													else if(infoLeft > _viewer_width) {
														Navi.GotoNextPage();
													}
												}
												
												Highlight.createSearchHighlight(selector, word, textNode);
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
							else {
								ele_loading_bar.show();
		
								ePubEngineLoaderJS.Data.loadSpineAt(leftChapter)
									.then(function(result) {
										Layout.init()
											.then(function() {

												ePubEngineLoaderJS.Data.setCurrentChapter(leftChapter);

												movePos = moveChapter >= 0 ? Layout.getChapterInPosByPercent(moveChapter, percent) : 0;
												nextMovePos = movePos + _columnWidth;
		
												ele_view_wrapper.scrollLeft(movePos);
												ele_loading_bar.hide();
		
												var info = Layout.getCurrentPageInfo(movePos);
												var info_r = Layout.getCurrentPageInfo(nextMovePos);
												callbackPosition(info, info_r);

												var findWordInfo = Highlight.getSearchHighlightInfo(selector, word);

												if(findWordInfo.constructor == Array)
												{
										

													var infoTop = parseInt(findWordInfo[0]['top'].toString(), 10);
													var infoLeft = parseInt(findWordInfo[0]['left'].toString(), 10);
													
													if(infoLeft < 0) {
														Navi.GotoPrevPage();
													}
													else if(infoLeft > _viewer_width) {
														Navi.GotoNextPage();
													}
												}
												
												Highlight.createSearchHighlight(selector, word, textNode);
											})
											.catch(function() {
												ele_loading_bar.hide();
											});
									})
									.catch(function(err) {
										ePubEngineLogJS.E.log(err);
									});
							}
						}
					}
				}
			}
		},
		GotoAnchor: function(idx, id) {
			var totalSpine = ePubEngineLoaderJS.Data.getTotalSpine();

			if(idx < 0 || idx >= totalSpine) {
				return;
			}

			if (ePubEngineCoreJS.Selection.IsSelected){
				ePubEngineCoreJS.Selection.DeleteSelection();
			}

			Highlight.deleteSearchHighlight();

			var spineInfo;

			if(_scrollMode) {
				spineInfo = _arrSpineHeight[idx];
			}
			else {
				spineInfo = _arrSpineWidth[idx];
			}

			// 먼저 해당 챕터가 로딩 되어 있는지 확인
			// 해당 챕터의 처음 스크롤 위치를 찾자.
			var isLoaded = Layout.getChapterLoaded(idx);

			if(_scrollMode) {
				
				if (callbackOnLoading){
					callbackOnLoading(true);
				}
				if(isLoaded) {
					var movePos = spineInfo.pos;
					// var moveEle = $('#'+id);
					// var moveElePos = moveEle.offset().top;
					var targetEle = $('#'+id);
					var moveElePos = targetEle.position() ? targetEle.get(0).offsetTop : movePos;
					movePos = Layout.getChapterInPosByElementPos(idx, movePos, moveElePos);

					var nextMovePos = movePos + _viewer_height;
					var bottomChapter = Layout.getChapterByPos(nextMovePos);

					if(idx == bottomChapter) {
						ePubEngineLoaderJS.Data.setCurrentChapter(idx);
	
						ele_view_wrapper.scrollTop(movePos);
						var info = Layout.getCurrentPageInfo(movePos);
						var info_r = Layout.getCurrentPageInfo(nextMovePos);
						if (callbackOnLoading){
							callbackOnLoading(false);
						}
						callbackPosition(info, info_r);
					}
					else {
						var isLoadedBottom = Layout.getChapterLoaded(bottomChapter);

						if(isLoadedBottom) {
							ePubEngineLoaderJS.Data.setCurrentChapter(idx);
		
							ele_view_wrapper.scrollTop(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							if (callbackOnLoading){
								callbackOnLoading(false);
							}
							callbackPosition(info, info_r);
						}
						else {
							ele_loading_bar.show();
	
							ePubEngineLoaderJS.Data.loadSpineAt(idx)
								.then(function(result) {
									Layout.init()
										.then(function() {
	
											ele_view_wrapper.scrollTop(movePos);
											ele_loading_bar.hide();
	
											var info = Layout.getCurrentPageInfo(movePos);
											var info_r = Layout.getCurrentPageInfo(nextMovePos);
											if (callbackOnLoading){
												callbackOnLoading(false);
											}
											callbackPosition(info, info_r);
										})
										.catch(function() {
											ele_loading_bar.hide();
											if (callbackOnLoading){
												callbackOnLoading(false);
											}
										});
								})
								.catch(function(err) {
									ePubEngineLogJS.E.log(err);
									if (callbackOnLoading){
										callbackOnLoading(false);
									}
								});
						}
					}
				}
				else {
					ele_loading_bar.show();
					ePubEngineLoaderJS.Data.loadSpineAt(idx)
						.then(function(result) {
							Layout.init()
								.then(function() {

									spineInfo = _arrSpineHeight[idx];

									var movePos = spineInfo.pos;
									// var moveElePos = $('#'+id).offset().top;
									var targetEle = $('#'+id);
									var moveElePos = targetEle.position() ? targetEle.get(0).offsetTop : movePos;
									movePos = Layout.getChapterInPosByElementPos(idx, movePos, moveElePos);

									var nextMovePos = movePos + _viewer_height;

									ele_view_wrapper.scrollTop(movePos);		
									ele_loading_bar.hide();

									var info = Layout.getCurrentPageInfo(movePos);
									var info_r = Layout.getCurrentPageInfo(nextMovePos);
									if (callbackOnLoading){
										callbackOnLoading(false);
									}
									callbackPosition(info, info_r);
								})
								.catch(function() {
									ele_loading_bar.hide();
								});
						})
						.catch(function(err) {
							ePubEngineLogJS.E.log(err);
						});
				}
			}
			else {

				if(isLoaded) {
					var movePos = spineInfo.pos;
					var targetEle = $('#'+id);
					// 아이디가 없는 경우에는 챕터의 처음으로 이동 하면 된다.
					var moveElePos = targetEle.position() ? targetEle.get(0).offsetLeft : movePos;

					movePos = Layout.getChapterInPosByElementPos(idx, movePos, moveElePos);

					var nextMovePos = movePos + _columnWidth;
					var rightChapter = Layout.getChapterByPos(nextMovePos);

					if(idx == rightChapter) {
						ePubEngineLoaderJS.Data.setCurrentChapter(idx);
						ele_view_wrapper.scrollLeft(movePos);

						//단면인 케이스
						if (!Layout.getPageMode()){
							//[sjhan][220323]페이지이동후 이동하려는 anchor 위치 확인후 재이동한다.
							let refineElement = $('#'+id).get(0);
							let inf = Layout.getPageInfoPos(idx,refineElement.offsetLeft)
							let _mrect = refineElement.getBoundingClientRect()
							let _sum = _mrect.left + ele_view_wrapper.get(0).scrollLeft
							let inf2 = Layout.getPageInfoPos(idx,_sum)
							//현재위치가 아닌경우
							if (inf && inf2 && inf.pos != inf2.pos){
								movePos = inf2.pos
								nextMovePos = movePos + _columnWidth;
								ele_view_wrapper.scrollLeft(movePos);
							}
							refineElement = null
							inf = null
							_mrect = null
							_sum = null
							inf2 = null
						}


						var info = Layout.getCurrentPageInfo(movePos);
						var info_r = Layout.getCurrentPageInfo(nextMovePos);
						if (callbackOnLoading){
							callbackOnLoading(false);
						}
						callbackPosition(info, info_r);
					}
					else {
						var isLoadedRight = Layout.getChapterLoaded(rightChapter);

						if(isLoadedRight) {
							ePubEngineLoaderJS.Data.setCurrentChapter(idx);
	
							ele_view_wrapper.scrollLeft(movePos);
		
							var info = Layout.getCurrentPageInfo(movePos);
							var info_r = Layout.getCurrentPageInfo(nextMovePos);
							if (callbackOnLoading){
								callbackOnLoading(false);
							}
							callbackPosition(info, info_r);
						}
						else {
							ele_loading_bar.show();
	
							ePubEngineLoaderJS.Data.loadSpineAt(idx)
								.then(function(result) {
									Layout.init()
										.then(function() {
	
											ePubEngineLoaderJS.Data.setCurrentChapter(idx);
	
											ele_view_wrapper.scrollLeft(movePos);
											ele_loading_bar.hide();

											var info = Layout.getCurrentPageInfo(movePos);
											var info_r = Layout.getCurrentPageInfo(nextMovePos);
											if (callbackOnLoading){
												callbackOnLoading(false);
											}
											callbackPosition(info, info_r);
										})
										.catch(function() {
											ele_loading_bar.hide();
										});
								})
								.catch(function(err) {
									ePubEngineLogJS.E.log(err);
								});
						}
					}
				}
				else {
					ele_loading_bar.show();
	
					ePubEngineLoaderJS.Data.loadSpineAt(idx)
						.then(function(result) {
							Layout.init()
								.then(function() {

									ePubEngineLoaderJS.Data.setCurrentChapter(idx);
									
									spineInfo = _arrSpineWidth[idx];

									var movePos = spineInfo.pos;
									var targetEle = $('#'+id);
									var moveElePos = targetEle.position() ? targetEle.get(0).offsetLeft : movePos;

									movePos = Layout.getChapterInPosByElementPos(idx, movePos, moveElePos);

									var nextMovePos = movePos + _columnWidth;

									ele_view_wrapper.scrollLeft(movePos);
									ele_loading_bar.hide();

									var info = Layout.getCurrentPageInfo(movePos);
									var info_r = Layout.getCurrentPageInfo(nextMovePos);
									if (callbackOnLoading){
										callbackOnLoading(false);
									}
									callbackPosition(info, info_r);
								})
								.catch(function() {
									ele_loading_bar.hide();
								});
						})
						.catch(function(err) {
							ePubEngineLogJS.E.log(err);
						});
				}
			}
		},
		/**
		 * cfi 로이동.
		 */
		moveToCfi:function(value){
			return new Promise((result, reject)=>{
				if (!value|| value[0].length ===0){
					reject(-1);
					return;
				}
				var cfi = value[0][1];
				if (cfi&&cfi.length>0){
					var spine = ePubEngineLoaderJS.Data.getCurrentSpine();
					var spineInfo = null;
					var mSpine = null;
					var movePos = null;
					var moveElePos = null;
					var nextMovePos = null;

					if (Layout.getScrollMode()){
						spineInfo = _arrSpineHeight[spine];
						mSpine = _mapSpineHeight.get(spine);
					} else {
						spineInfo = _arrSpineWidth[spine];
						mSpine = _mapSpineWidth.get(spine);
					}
					movePos = spineInfo.pos;
					moveElePos = Position.GetCfiRects(cfi);

					//이동할 rects 정보가 array인지.
					if (moveElePos&&Array.isArray(moveElePos)){
						
						//이동할 거리.
						movePos = Layout.getChapterInPosByElementPos(spine, movePos, moveElePos);
						
						if (Layout.getScrollMode()){
							nextMovePos = movePos+_viewer_height;
							ele_view_wrapper.scrollTop(movePos);
						} else {
							nextMovePos = movePos+_columnWidth;
							ele_view_wrapper.scrollLeft(movePos);
						}
	
						//이동후 현재 페이지의 정보를 update한다.
						var info = Layout.getCurrentPageInfo(movePos);
						var info_r = Layout.getCurrentPageInfo(nextMovePos);
						if (Layout.getPageMode()){
							info_r = Layout.getNextPageInfo(info);
						}

						result({ left: info , right: info_r});
					} else {
						reject(-2);
					}
				} else {
					reject(-3);
				}
			});
		},

		/**
		 * {
		 * behavior: "smooth", 
		 * block: "end", 
		 * inline: "nearest"
		 * }
		 * 
		 * 
		 * @param {*} element 
		 */
		moveToElement:function(element, cfi){
			return new Promise((rs,rj)=>{
				if (Layout.getScrollMode()){
					var move = element.offsetTop;
		
					var nextMove = move+_viewer_height;
	
					ele_view_wrapper.scrollTop(move);
	
					var info = Layout.getCurrentPageInfo(move);
					var info_r = Layout.getCurrentPageInfo(nextMove);
					rs({left:info , right:info_r});
				} else {
					rj();
				}
			});
		},
		scrollMoveDelay:300,
		isScrollMoved:false,
		/**
		 * 
		 * @param {*} direction true 아랫쪽, false 윗쪽
		 * @returns 
		 */
		moveScroll:function(direction){
			if (Layout.getScrollMode()){
				//selection picker를 놓으면
				if (!Selection._isSelectPicker){
					Navi.isScrollMoved = false;
					return false;
				} 
				//scroll이 진행중
				if (Navi.isScrollMoved){
					return true;
				}

				Navi.isScrollMoved = true;
				let distance = 30;
				distance = (direction)?distance:-distance;
				let currentPosition = ele_view_wrapper.get(0).scrollTop;
				
				//상단이 0 이면 중지.
				if (currentPosition === 0){
					Navi.isScrollMoved = false;
					return false;
				}

				ele_view_wrapper.animate({ scrollTop: currentPosition + distance},Navi.scrollMoveDelay,function(){
					Navi.isScrollMoved = false;
				})
				return true
			}
			Navi.isScrollMoved = false;
			return false;
		},
		/**
		 * move pos
		 * @param {*} direction 
		 * @returns 
		 */
		getMovePos: function(direction){
			let curLeft = ele_view_wrapper.scrollLeft();
			let scrollEndPos = ele_view_wrapper.get(0).scrollWidth;
			let extraPos = _pageMode ? _columnGapDefault : 0;
			let gotoPos = direction ? curLeft + ( _viewer_width + extraPos) : curLeft - (_viewer_width + extraPos);
			let gotoLeft = gotoPos
			let gotoRight = gotoPos

			if(_pageMode) {
				gotoLeft = gotoPos ;
				gotoRight = gotoPos + (_viewer_width/ 2) + _columnGapCorrect;
			}

			return {
				left: gotoLeft, 
				right: gotoRight
			}	
		},
		/**
		 * anntation 데이터로 이동
		 * @param {*} data 
		 * @returns 
		 */
		moveToAnnotation:function(data){

			console.log('##moveToAnnotation',data)
			let originChapter = data[KEY_ANNOTATION_CHAPTER]
			let startChapter = data[KEY_ANNOTATION_START_CFI_CHAPTER]
			let startCfiAttr = data[KEY_ANNOTATION_STARTCFIATTR]
			let startIndexFromParent = data[KEY_ANNOTATION_STARTINDEXFROMPARENT]
			let endChapter = data[KEY_ANNOTATION_END_CFI_CHAPTER]
			let endCfiAttr = data[KEY_ANNOTATION_ENDCFIATTR]
			let endIndexFromParent = data[KEY_ANNOTATION_ENDINDEXFROMPARENT]

			let chapter = -1
			let parent = null
			let targetEle = null
			let chapterSelector = null
			let selector = null

			return new Promise(function(resolve,reject){
					parent = ele_view_wrapper.get(0)
					//check selector
					if (typeof startCfiAttr === 'string'){
						selector = `[cfi="${startCfiAttr}"]`
					}
					if (!selector){
						reject(`"${selector}" is empty`)
						return 
					}
					//check chapter number
					if (typeof startChapter === 'number'){
						chapter = startChapter
					} else {
						if (typeof originChapter === 'number'){
							chapter = originChapter
						}
					}
					if (chapter < 0){
						reject(`"${chapter}" is not found`)
						return 
					}
					chapterSelector = `#bdb-chapter${chapter}`
					//move
					Navi.moveToChapter(chapter).then(e=>{
						let encode = Annotation.encode(
							data[KEY_ANNOTATION_CHAPTER],
							data[KEY_ANNOTATION_STARTCFIATTR],
							data[KEY_ANNOTATION_STARTINDEXFROMPARENT],
							data[KEY_ANNOTATION_ENDCFIATTR],
							data[KEY_ANNOTATION_ENDINDEXFROMPARENT],
							data[KEY_ANNOTATION_START_CFI_CHAPTER],
							data[KEY_ANNOTATION_END_CFI_CHAPTER])
						if (encode){
							Navi.calcNodeOffsetFromParent(encode.start.node).then(pos=>{
								let result = Layout.getPosByPos(pos)
								if (!result){
									reject(`can not find pos in arrMap`)	
									return 
								}
								let estimateNextPage = 0
								let rePos = result.current.pos
								if (Layout.getScrollMode()){
									estimateNextPage = _viewer_height
									ele_view_wrapper.scrollTop(rePos)
								} else {
									estimateNextPage = Layout.getPageMode() ? (_viewer_width / 2) + _columnGapCorrect: _viewer_width;
									ele_view_wrapper.scrollLeft(rePos)
								}
								let rePos_next = rePos+estimateNextPage
								let left = Layout.getCurrentPageInfo(rePos);
								var right =  Layout.getCurrentPageInfo(rePos_next);
								return { left , right }
							}).then(rs=>{
								callbackPosition(rs.left,rs.right)
								resolve( { left , right})
							}).catch(err2=>{
								reject(`error "calcNodeOffsetFromParent" ${err2}`)
							})
						} else {
							return reject(`encode cfi failure`)
						}
					}).catch(err=>{
						return reject(`error "moveToChapter" ${err}`)
					})
			})
		},
		/**
		 * 해당 챕터로 이동
		 * @param {*} chapter 챕터 번호 
		 * @returns 
		 */
		 moveToChapter(chapter){
			console.log('moveToChapter=========',chapter)
			let parent = null
			let parentRect = null
			let chapterLi = null

			parent = ele_view_wrapper.get(0)
			parentRect = parent.getBoundingClientRect()
			if (typeof chapter != 'number'){
				return Promise.reject(`chapter type not a number`)	
			}
			if (chapter < 0){
				return Promise.reject(`chapter never be negative number`)
			}

			chapterLi = parent.querySelector(`#bdb-chapter${chapter}`)
			if (!chapterLi){
				return Promise.reject(`chapter element not found`)	
			}

			return new Promise(function(resolve,reject){
				chapterLi.scrollIntoView()
				window.scrollTo(0,0)
				document.body.scrollTop = 0
				Navi.callWhenScrollCompleted($(parent),200,()=>{
					//안해주면 위로 밀리는 현상이 있음.

					// if(ePubEngineLoaderJS.Data.checkLoadChapter(chapter)) {
					// 	resolve()
					// } else {
					// 	ePubEngineLoaderJS.Data.loadSpineAt(chapter)
					// 		.then(()=>Layout.init())
					// 		.then(()=>{
					// 			ePubEngineLoaderJS.Data.setCurrentChapter(chapter);
					// 			return Navi.calcChapterOffsetFromParent(chapter)
					// 		})
					// 		.then((pos)=>{
					// 			if (Layout.getScrollMode()){
					// 				parent.scrollTo(parentRect.left,pos)
					// 			} else {
					// 				parent.scrollTo(pos,parentRect.top)
					// 			}
					// 			resolve()
					// 		})
					// 		.catch(err=>{
					// 			reject(err)
					// 		})
					// }
					//load체크
					if (chapterLi && typeof chapterLi.getAttribute('loaded') === 'string' && chapterLi.getAttribute('loaded') === "1"){
						ePubEngineLoaderJS.Data.setCurrentChapter(chapter);
						resolve()
					}
					else {
						ePubEngineLoaderJS.Data.loadSpineAt(chapter)
							.then(()=>Layout.init())
							.then(()=>{
								ePubEngineLoaderJS.Data.setCurrentChapter(chapter);
								return Navi.calcChapterOffsetFromParent(chapter)
							})
							.then((pos)=>{
								if (Layout.getScrollMode()){
									parent.scrollTo(parentRect.left,pos)
								} else {
									parent.scrollTo(pos,parentRect.top)
								}
								resolve()
							})
							.catch(err=>{
								reject(err)
							})
					}
				})
			})
		},
		/**
		 * parent로 부터 현chapter간의 거리
		 * @param {*} chapter 
		 * @returns 
		 */
		 calcChapterOffsetFromParent(chapter){
			let parent = null
			let chapterLi = null

			parent = ele_view_wrapper.get(0)
			if (typeof chapter != 'number'){
				return Promise.reject(`chapter type not a number`)	
			}
			chapterLi = parent.querySelector(`#bdb-chapter${chapter}`)
			if (!chapterLi){
				return Promise.reject(`chapter element not found`)	
			}
			let pos = (Layout.getScrollMode())? chapterLi.offsetTop:chapterLi.offsetLeft
			let result = Layout.getPosByPos(pos)
			if (result){
				if (result['next']){
					return Promise.resolve(result.next.pos)
				} else {
					if (result['current']){
						return Promise.resolve(result.current.pos)
					} else {
						return Promise.reject('not fount result')
					}
				}
			} else {
				return Promise.reject('not fount pos')
			}
		},
		/**
		 * 현위치에서 노드까지 거리
		 * @param {*} node 
		 */
		calcNodeOffsetFromParent(node){
			let chapterLi = Util.findParent(node)
			if (!chapterLi){
				return Promise.reject('not fount chapter li')
			}
			let parent = ele_view_wrapper.get(0)
			let parentPos = (Layout.getScrollMode())?parent.scrollTop:parent.scrollLeft
			let range = document.createRange();
			range.selectNodeContents(node);
			let rect = range.getBoundingClientRect();
			let movePos = (Layout.getScrollMode())?rect.top:rect.left
			let result = parentPos + movePos
			return Promise.resolve(result)
		},
		/**
		 * 해당 엘리먼트로 이동
		 * @param {*} parentElement 
		 * @param {*} checkTimeout 
		 * @param {*} callback 
		 */
		callWhenScrollCompleted(parentElement = $(window), checkTimeout = 200, callback,  ) {
			const scrollTimeoutFunction = () => {
			  parentElement.off("scroll");
			  callback();
			};
			let scrollTimeout = setTimeout(scrollTimeoutFunction, checkTimeout);
		  
			parentElement.on("scroll", () => {
			  clearTimeout(scrollTimeout);
			  scrollTimeout = setTimeout(scrollTimeoutFunction, checkTimeout);
			});
		}
	};

	var Search = Core.Search = {
		_asideList: null,
		/**
		 * 검색된 전체 개수
		 */
		searchItemTotal: 0,
		/**
		 * 검색할 파일 개수
		 */
		searchFileTotal: 0,
		/**
		 * 현재 검색 중인 파일 번호
		 */
		searchFileStatus : -1,
		
		findTextHeaderTag: '<b>',
		findTextFooterTag: '</b>',
		// findTextHeaderTag: '<span class="lyres_tsym">',
		// findTextFooterTag: '</span>',
		/**
		 * 검색 내용에 aside를 포함할것인지.
		 */
		useAsideSearchContains: false,
		setAsideList(asideList){
			Search._asideList = asideList
		},
		/**
		 * 검색용 캐싱 map
		 */
		SearchMap: new Map(),
		/**
		 * spine번호에 따른 html 정보를 리턴한다.
		 * @param {*} idx 
		 * @param {*} keyword 
		 * @returns 
		 */
		getSpineHtml: function(idx){
			if (idx < 0){
				return Promise.reject(`file number greater than minus`) 
			}
			if (idx > ePubEngineLoaderJS.Data.getTotalSpine()-1){
				return Promise.reject(`${idx} cannot be greater than ${ePubEngineLoaderJS.Data.getTotalSpine()}`)
			}
			if (Search.SearchMap.has(idx)){
				return Promise.resolve(Search.SearchMap.get(idx))
			} else {
				return ePubEngineLoaderJS.Data.getSpineDataNested(idx).then(data=>{
					let parsedData = ePubEngineLoaderJS.Data.addCfiAttribute(data)
					let total = ePubEngineLoaderJS.Data.getTotalSpine();
					var spineItem = ePubEngineLoaderJS.Data.getSpineItem(idx);
					var item1 = parseFloat(spineItem.percent)
					var item2 = 0 ;
					if (idx == total -1){
						item2 = 100;
					} else {
						var nextspineItem = ePubEngineLoaderJS.Data.getSpineItem(idx+1);
						item2 = parseFloat(nextspineItem.percent);
					}
					//다음 퍼센트 - 현재 퍼센트
					var diff = item2 - item1;


					let parser = new DOMParser();
					let spineDoc = parser.parseFromString(parsedData, 'text/html');
					let walk = Walker.Build(spineDoc,true)
					
					let info = []
					let s = 0
					let e = 0
					
					while(walk.nextNode()) {
						if (walk.currentNode.nodeType === Node.TEXT_NODE){
							if (walk.currentNode.parentElement){
								let attrCfi = walk.currentNode.parentElement.getAttribute('cfi')
								let number1 = parseFloat(walk.currentNode.parentElement.getAttribute('tag-current'))
								let number2 = parseFloat(walk.currentNode.parentElement.getAttribute('tag-total'))
								let percent = (number1 / number2) * 100
								let ratio = parseFloat( percent)/100;
								let rPercent = item1 + (diff*ratio);
								let toc = ePubEngineLoaderJS.Data.getTocByPercent(rPercent)
								if (attrCfi && attrCfi.length >0){
									s = e
									e = s + walk.currentNode.textContent.length
									info.push({ cfi: attrCfi, text: walk.currentNode.textContent, start: s , end: e, percent: rPercent ,filePercent: percent, toc: toc })
								}
								attrCfi = null;
								number1 = null;
								number2 = null;
								percent = null;
								ratio = null;
								rPercent = null;
							}
						}
					}
			
					let text = info.map(e=>e.text).join('')
					let result = { idx , info , text}
					Search.SearchMap.set(idx, result)
					parsedData = null
					parser = null
					spineDoc = null
					walk = null
					info = null
					return result
				})
			}
		},
		findSearchInChapter: function(idx , keyword){
			Search.getSpineHtml(idx).then(rs=>{
				let indxs = rs.text.findAllIndex(keyword)
				let mStart = false
				let mEnd = false
				let tmp = []
				for (let ds of indxs){
					mStart = false;
					mEnd = false;
					
					for (let info of rs.info){
						if (info.start <= ds.s && info.end > ds.s){
							mStart = true
						}

						if (mStart){
							console.log("[DRAW]",ds,info)
							if (info.end >= ds.e){
								tmp.push({'start':ds.s,'end':ds.e,info})
								mEnd = true
								break;
							}
						}
					}
				}
				console.log('[SPINE]',idx)
				console.log('[CONTAIN STRING]',tmp)
			})
		},
		/**
		 * 검색 시작
		 * @param {string} keyword 검색어
		 * @param { any } toc 목차 리스트 검색하기위함
		 * @param { function( any ) } callback 검색 결과를 실시간으로 가저오기 위함.
		 */
		_onSearchProgress: false,
		_stopSearching: false,
		searchStart: async function(keyword , toc , callback) {
			if (Search._stopSearching){
				return null
			}

			if (Search._onSearchProgress){
				return
			}
			Search.searchFileTotal = ePubEngineLoaderJS.Data.getTotalSpine();
			Search.searchItemTotal = 0;
			Search.searchFileStatus = -1;

			
			var searchResult = []
			var parser = new DOMParser();

			var temp = []
			toc.forEach((r)=>{
				temp.push(parseFloat(r.pos)*10)
			})

			// console.time('start search')
			for(let i = 0; i < Search.searchFileTotal; i++) {
				if (Search._stopSearching){
					Search._stopSearching = false
					Search._onSearchProgress = false;
					return;
				}
				var spineData = await ePubEngineLoaderJS.Data.getSpineData(i);
				if (spineData){

				} else {
					continue;
				}
			
		
				//i 값이 중복되어 들어가는 경우가 있음..
				var ofData = ePubEngineLoaderJS.Util.replaceAll(spineData, "{CONTENT_OBFUCATE}", ' ');
				var spineDoc = parser.parseFromString(ofData, 'text/html');
				var result = this._search_fast_impl(keyword, spineDoc, 25, i , false);

				ofData = null
				spineDoc = null

				if (result){
					if (result.items && result.items.length > 0){
						result.items.forEach((rs)=>{
				
							var spineItem = ePubEngineLoaderJS.Data.getSpineItem(i);

							var item1 = parseFloat(spineItem.percent)
							var item2 = 0 ;
							if (i == Search.searchFileTotal -1){
								item2 = 100;
							} else {
								var nextspineItem = ePubEngineLoaderJS.Data.getSpineItem(i+1);
								item2 = parseFloat(nextspineItem.percent);
							}
							//다음 퍼센트 - 현재 퍼센트
							var diff = item2 - item1;
							var ratio = parseFloat( rs.percent)/100;
							
							//현재 퍼센트 + ( 다음 퍼센트와의 차이값 * 현재 검색 대상의 퍼센트)
							var rPercent = item1 + (diff*ratio);
							var findTocIndex = Core.Util.getRangeIndex(rPercent*10 , temp);
							
							
							//toc 범위내에 찾지 못하면 null 처리.
							if (findTocIndex === -1){
								rs.toc = null;
							} else {
								rs.toc = toc[findTocIndex]
							}
							rs.originPercent = rs.percent;
							rs.percent = rPercent;
							rs.curToc = item1;
							rs.nextToc = item2;
							
							spineItem = null
							item1 = null
							item2 = null
							diff = null
							ratio = null
							rPercent = null
							findTocIndex = null
							/**
							 * aside 포함
							 */
							if (Search.useAsideSearchContains){
								searchResult.push(rs);
								if (callback){
									setTimeout(() => {
										callback(rs);
									}, 0);
								}
							} 
							/**
							 * 미포함
							 */
							else {
								if (rs.selector&&rs.selector.includes('aside')){
									//skip contain aside
								} else {
									searchResult.push(rs);
									if (callback){
										setTimeout(() => {
											callback(rs);
										}, 0);
									}
								}
							}
						})
					}
				}
			}

	
			parser = null
			result = null
			temp = null
			
			if (searchResult.length>0){
				//sorting
				searchResult.sort((a,b)=>{
					return a.percent - b.percent
				})
			}
			Search._onSearchProgress = false;
			// console.timeEnd('start search')
			return searchResult
		},
		SetCfiAttribute: function (spineDocument) {
			
			try {
				// nextSibling만 처리
				var node = spineDocument;
				var cfi = '';
				var idxCfi = 0;
				var idxTag = 0;

				var temp = node.firstChild;
				while (temp != null) {
					idxCfi |= 1;
					if (temp.nodeType == 3) {
						idxCfi++;
						idxTag++;
					}
					
					try {
						temp['cfi'] = cfi + '/' + idxCfi;
						temp['tagPos'] = idxTag;
					}
					catch (ea) {
						ePubEngineLogJS.W.log(ea + ' :' + temp.nodeType + ',' + temp.nodeName);
					}

					idxTag = this._SetCfiAttributeImpl(temp, cfi + '/' + idxCfi, idxTag);

					temp = temp.nextSibling;
				}
				
			}
			catch (ex) {
				ePubEngineLogJS.W.log(ex);
			}

			return idxTag;
		},
		_SetCfiAttributeImpl: function (node, cfiHeader, tagPos) {
			try {
				// nextSibling만 처리
				var cfi = cfiHeader;
				var idxCfi = 0;
				var idxTag = tagPos;

				var temp = node.firstChild;
				while (temp != null) {
					idxCfi |= 1;
					//text node 갯수만 카운트.
					if (temp.nodeType == 3) {
						idxCfi++;
						idxTag++;
					}
					
					try {
						temp['cfi'] = cfi + '/' + idxCfi;
						temp['tagPos'] = idxTag;
					}
					catch (ea) {
						ePubEngineLogJS.W.log(ea + ' :' + temp.nodeType + ',' + temp.nodeName);
					}
					idxTag = this._SetCfiAttributeImpl(temp, cfi + '/' + idxCfi, idxTag);
					temp = temp.nextSibling;
				}
			}
			catch (ex) {
				ePubEngineLogJS.W.log(ex);
			}

			return idxTag;
		},
		_search_fast_impl: function(keyword, spineDocument, resultTextMaxCount, fileno, isContainAside) {
			var jsonReturn = {};
            jsonReturn['items'] = new Array();

			try {
				var strContent = spineDocument.body.textContent;
				var strRexSearchWord = '';
				var strRexGapString = '[ \n\r\t\f\v\x0b\xa0]{0,}';
				var objTextNode = null
				// check
				if (keyword.length > resultTextMaxCount) {
					jsonReturn['error_code'] = -1;
					jsonReturn['error_msg'] = 'search text too long';
					jsonReturn['total_cnt'] = 0;
					jsonReturn['curr_cnt'] = 0;
					return jsonReturn; //너무 길어서 사용할 수 없음.
				}
				if (keyword.length < 2) {
					jsonReturn['error_code'] = -2;
					jsonReturn['error_msg'] = 'search text too short';
					jsonReturn['total_cnt'] = 0;
					jsonReturn['curr_cnt'] = 0;
					return jsonReturn; //너무 길어서 사용할 수 없음.
				}


				// 0. make match word string
				for (var idxStr = 0; idxStr < keyword.length; idxStr++) {
					if (null != keyword.substr(idxStr, 1).match(new RegExp("[\$\(\)\*\+\.\[\?\\\^\{\|]"))) {
						strRexSearchWord += '\\' + keyword.substr(idxStr, 1);
					}
					else {
						strRexSearchWord += keyword.substr(idxStr, 1);
					}
					strRexSearchWord += strRexGapString;
				}
				strRexSearchWord = strRexSearchWord.substring(0, strRexSearchWord.length - strRexGapString.length);
				//Timer('0. make match word string','E',2);

				//ePubEngineLogJS.W.log('search Regex : ' + strRexSearchWord);

				// 1. make array find word list
				var reg = new RegExp(strRexSearchWord, "gim");
				var arrMatchString = strContent.match(reg);

				var tagTotal = this.SetCfiAttribute(spineDocument);

				// 2. make array find position list

				var nIndexOf_StartPos = 0;
				var nIndexOf_FindPos = 0;
				var nIdxPosition = 0;
				var arrSearchPosition = new Array(); // match count *2 ( start, end )
				
				if (arrMatchString != null) {
					for (var nIdxMatch = 0; nIdxMatch < arrMatchString.length; nIdxMatch++) {
						var strFindString = arrMatchString[nIdxMatch];
						nIndexOf_FindPos = strContent.indexOf(strFindString, nIndexOf_StartPos);

						arrSearchPosition[nIdxPosition] = new Array();
						arrSearchPosition[nIdxPosition]['pos'] = nIndexOf_FindPos;
						arrSearchPosition[nIdxPosition]['findpage'] = false;
						nIdxPosition++;

						arrSearchPosition[nIdxPosition] = new Array();
						arrSearchPosition[nIdxPosition]['pos'] = nIndexOf_FindPos + strFindString.length;
						arrSearchPosition[nIdxPosition]['findpage'] = false;
						nIdxPosition++;

						nIndexOf_StartPos = nIndexOf_FindPos + strFindString.length;
					}
				}
				

				// 3. position to cfi array
				//Timer('3. position to cfi array','S',2);
				if (arrMatchString != null) {
					//var iTextNode = document.evaluate("/html/body//text()", document.body, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
					var iTextNode = spineDocument.evaluate(".//text()", spineDocument.body, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
					
					var nSumTotalCount = 0;
					var nSumCurrentStartCount = 0;
					var objPosition = null;
					
					var nIdxArray = 0;
					var nIdxPosition = 0;
					var objTextNode_Idx = 0;
					var nCurrentNodeValueLen = 0;

					for (objTextNode_Idx = 0; objTextNode_Idx < iTextNode.snapshotLength; objTextNode_Idx++) {
						objTextNode = iTextNode.snapshotItem(objTextNode_Idx);
						nCurrentNodeValueLen = objTextNode.nodeValue.length
						nSumTotalCount += nCurrentNodeValueLen;

						while (arrSearchPosition.length > nIdxPosition) {

							if (arrSearchPosition[nIdxPosition]['pos'] <= nSumTotalCount) {


								// 여기서 기존 encodeCfi를 구하면 시간이 너무 오래 걸려서, cfi를 속성으로 추가한후에 구함 (이게 바로 FAST CFI)
								arrSearchPosition[nIdxPosition]['cfi'] = objTextNode['cfi'] + ':' + (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount);
								arrSearchPosition[nIdxPosition]['offset'] = (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount);
								arrSearchPosition[nIdxPosition]['selector'] = Position._getPath(objTextNode);
								arrSearchPosition[nIdxPosition]['select_textnode'] = objTextNode;

								//ePubEngineLogJS.L.log('FALSE [' + arrSearchPosition[nIdxPosition]['pos'] + '] ');


								// 위치 번호 구하기
								arrSearchPosition[nIdxPosition]['tagPos'] = parseFloat( parseFloat(objTextNode['tagPos']) / parseFloat(tagTotal) * 100);
								

								// 다음 검색된 텍스트 위치
								nIdxPosition++;
								continue;
							}
							else {
								// 범위벗어나면, 다음 텍스트 노드 가져옴
								nSumCurrentStartCount += nCurrentNodeValueLen;
								break;
							}
						}
					}
				}

				// 4. 결과셋만들기 및 텍스트 추출

				if (arrMatchString != null) {
					for (var i = 0; i < arrSearchPosition.length; i = i + 2) {
						jsonReturn['items'][i / 2] = {};
						jsonReturn['items'][i / 2]['cfi_start'] = arrSearchPosition[i]['cfi'];
						jsonReturn['items'][i / 2]['cfi_end'] = arrSearchPosition[i + 1]['cfi'];
						jsonReturn['items'][i / 2]['selector'] = arrSearchPosition[i + 1]['selector'];
						jsonReturn['items'][i / 2]['startOffset'] = arrSearchPosition[i]['offset'];
						jsonReturn['items'][i / 2]['endOffset'] = arrSearchPosition[i + 1]['offset'];
						jsonReturn['items'][i / 2]['percent'] = arrSearchPosition[i]['tagPos'];
						jsonReturn['items'][i / 2]['fileno'] = fileno;
						// if (arrSearchPosition[i]['selector'].contains('aside')){
						// 	jsonReturn['items'][i / 2]['aside'] = true
						// } else {
						// 	jsonReturn['items'][i / 2]['aside'] = false
						// }
						// jsonReturn['items'][i / 2]['aside'] = arrSearchPosition[i]['aside'];;


						var nTextLen = parseInt(arrSearchPosition[i + 1]['pos'] - arrSearchPosition[i]['pos']);
						var nTextGap = parseInt((resultTextMaxCount - nTextLen) / 2);

						var strHeaderLimit = '';
						if (arrSearchPosition[i]['pos'] - (nTextGap * 2) <= 0) {
							strHeaderLimit = strContent.substring(0, arrSearchPosition[i]['pos']).replace(/[\n\r\t\f\x0b\xa0]/gm, ""); // 유의 substring
						}
						else {
							strHeaderLimit = strContent.substr(arrSearchPosition[i]['pos'] - (nTextGap * 2), nTextGap * 2)// 유의 substring
						}
						strHeaderLimit = strHeaderLimit.replace(/[\n\r\t\f\x0b\xa0]/gm, "");
						var strFooterLimit = strContent.substr(arrSearchPosition[i + 1]['pos'], nTextGap * 2).replace(/[\n\r\t\f\x0b\xa0]/gm, ""); // gap 2배만큼 많은 텍스트를 얻어와서, 텍스트만 추출해서 Gap만큼만 가져옴
						// let mResulttext = strHeaderLimit + strContent.substr(arrSearchPosition[i]['pos'], nTextLen) + strFooterLimit;
						// console.log('##ㅇㅇ',strHeaderLimit)
						// console.log('##ㅇㅇ',Search.findTextFooterTag)
						// console.log('##ㅇㅇ',strFooterLimit)
		
						jsonReturn['items'][i / 2]['result_text'] = strHeaderLimit + Search.findTextHeaderTag + strContent.substr(arrSearchPosition[i]['pos'], nTextLen) + Search.findTextFooterTag + strFooterLimit;
						jsonReturn['items'][i / 2]['result_text_origin'] =  strHeaderLimit + strContent.substr(arrSearchPosition[i]['pos'], nTextLen) + strFooterLimit;
						jsonReturn['items'][i / 2]['matches_node'] =  arrSearchPosition[i]['select_textnode'];
						//ePubEngineLogJS.L.log('id : ' + i + ', ' + jsonReturn['items'][i / 2]['cfi_start'] + ', ' + jsonReturn['items'][i / 2]['cfi_end']);
					}
				}


				jsonReturn['error_code'] = 0;
				jsonReturn['error_msg'] = '';
				jsonReturn['total_cnt'] = (arrMatchString != null && arrMatchString.length > 0) ? arrMatchString.length : 0;
				jsonReturn['curr_cnt'] = (jsonReturn['items'] != null && jsonReturn['items'].length > 0) ? jsonReturn['items'].length : 0;
				return jsonReturn;
			}
			catch (ex) {
				ePubEngineLogJS.E.log(ex);
				jsonReturn['error_code'] = -1;
				jsonReturn['error_msg'] = '';
				return jsonReturn;
			}finally{
				objTextNode = null
			}
		},

		_searchCache: null,
		_loadSearchData: false,
		/**
		 * sjhan 
		 * 검색 버전2
		 * @param {*} searchKeyword 
		 * @param {*} callback 
		 * @returns 
		 */
		search2: async function(searchKeyword, toc, callback){
			console.time('search start')
			if (Search._loadSearchData){
				return
			}
			Search._loadSearchData = true
			Search.searchFileTotal = ePubEngineLoaderJS.Data.getTotalSpine();
			Search.searchItemTotal = 0;
			Search.searchFileStatus = -1;

			var temp = []
			toc.forEach((r)=>{
				temp.push(parseFloat(r.pos)*10)
			})
			Search._searchCache = []
			var parser = new DOMParser();
			//가저올 글자 길이
			let limitLen = 30

			
			for(var i = 0; i < Search.searchFileTotal; i++) {
				var spineData = await ePubEngineLoaderJS.Data.getSpineData(i);
				if (spineData){

				} else {
					continue;
				}
		
				var ofData = ePubEngineLoaderJS.Util.replaceAll(spineData, "{CONTENT_OBFUCATE}", ' ');
				let ofdata2 = ePubEngineLoaderJS.Data.addCfiAttribute(ofData)
				var spineDoc = parser.parseFromString(ofdata2, 'text/html');
				
				var textnodeList = Position.getTextNodesAllWithObject(spineDoc.body)

				//키워드가 존재하는 노드
				let filter1 = function(e) {
					return e.node.textContent.includes(searchKeyword)
				}


				//파일 내에 퍼센트계산 및 텍스트 위치찾기
				let filter2 = function(e){
					let nd = e.node.parentNode
					let number1 = parseFloat(nd.getAttribute('tag-current'))
					let number2 = parseFloat(nd.getAttribute('tag-total'))
					let percent = (number1 / number2) * 100
					let cfiAttr = nd.getAttribute('cfi')

					var spineItem = ePubEngineLoaderJS.Data.getSpineItem(i);
					var item1 = parseFloat(spineItem.percent)
					var item2 = 0 ;
					if (i == Search.searchFileTotal -1){
						item2 = 100;
					} else {
						var nextspineItem = ePubEngineLoaderJS.Data.getSpineItem(i+1);
						item2 = parseFloat(nextspineItem.percent);
					}
					//다음 퍼센트 - 현재 퍼센트
					var diff = item2 - item1;
					var ratio = parseFloat( percent)/100;
					
					//현재 퍼센트 + ( 다음 퍼센트와의 차이값 * 현재 검색 대상의 퍼센트)
					var rPercent = item1 + (diff*ratio);
					
					var findTocIndex = Core.Util.getRangeIndex(rPercent*10 , temp);
					
					let tc = toc[findTocIndex]

					
					nd = null
					number1 = null
					number2 = null
					return [{
						'spine':i,
						'item':e,
						'text':e.node.textContent ,
						'idxList': Util.getStringIndexList(e.node.textContent, searchKeyword), 
						'percentInFile': percent ,
						'percent': rPercent,
						'toc': tc,
						cfiAttr
					}]
				}
				// html형태로 검색리스트에 보여질 문자열 파싱
				let filter3 = function(e2){
					let ee = []
					for (let idx of e2.idxList){
						let _range = []
						let frontIndex = idx - 1 - limitLen
						let backIndex = idx + searchKeyword.length
						
						let backword = e2['text'].substring( backIndex, backIndex + limitLen)
						let parsedString = ''
		
						_range.push(backIndex + limitLen)
						//검색문자 + 뒷글자 
						parsedString = Search.findTextHeaderTag + searchKeyword + Search.findTextFooterTag + backword
						
						//앞쪽글자 더하기
						if ( frontIndex >=0 ){
							parsedString = e2['text'].substring( frontIndex , idx) + parsedString
							_range.push(frontIndex)
						} else {
							parsedString = e2['text'].substring( 0 , idx ) + parsedString
							_range.push(0)
						}
						_range.reverse()


						let beforeParsedString = e2['text'].substring(_range[0],_range[1])
						
						var temp = {
							'spine': e2['spine'],
							'item': e2['item'],
							'text': e2['text'],
							'idxList': e2['idxList'], 
							'percentInFile': e2['percentInFile'] ,
							'index': idx,
							'result_text': parsedString,
							'percent': e2['percent'] ,
							'toc': e2['toc'] ,
							'beforeParsedString': beforeParsedString,
							'offsetRange': _range
						}
						ee.push(temp)

						//
						frontIndex = null
						backIndex = null
						backword = null
						parsedString = null
						temp = null
					}
					return ee
				}
				let sText = textnodeList.filter(filter1)
										.flatMap(filter2)
										.flatMap(filter3)


				// console.log(sText)

				Search._searchCache.push.apply(Search._searchCache, sText);
				

				ofData = null
				ofdata2 = null
				spineDoc = null
				textnodeList = null
				sText = null
				filter1 = null
				filter2 = null
				filter3 = null
				if (callback){
					//update ui
					setTimeout(() => {
						if (Search._searchCache.length > 0){
							callback(Search._searchCache)
						}
					}, 0);
				}
			}
		
			Search._loadSearchData = false

			console.log(Search._searchCache)
			console.timeEnd('search start')
		},
		/**
		 * search2 함수로 파싱된 데이터 기준으로 이동
		 * @param {*} obj 
		 */
		goSearchPage: function(obj){
			console.log(obj)
			let spine = obj.spine
			let _cfi = obj.item.cfi

			let ele_spine = ele_view_wrapper.get(0).querySelector('#bdb-chapter'+spine)
			let parentsRect = ele_view_wrapper.get(0).getBoundingClientRect()
			let isLoaded = ele_spine.getAttribute('loaded')
			let _moveTargetElement = null
			let _move_pos = 0

			let rtnDraw = null
			let nRtnDraw_Cnt = null
			let dl_idx = null
			let re = null
			let docf = document.createDocumentFragment()
			
			Highlight.deleteSearchHighlight()

			// 찾은 element가 화면상에 실제로 보이는지 체크
			let checkVisibleEle = ele_view_wrapper.get(0).querySelector('#bdb-chapter'+spine).querySelector('[cfi=\"'+_cfi+'\"]')
			let _isHidden = false
			if (checkVisibleEle){
				let _style = window.getComputedStyle(checkVisibleEle)	
				if (_style){
					_isHidden = _style.display === 'none' || _style.visibility === 'hidden'
				}
			}
			
			//이미 로드됨 && 보이는 엘리먼트
			if (isLoaded === "1" && !_isHidden){
				console.log('##[goSearchPage][loaded]')
				_moveTargetElement = ele_view_wrapper.get(0).querySelector('#bdb-chapter'+spine).querySelector('[cfi=\"'+_cfi+'\"]')
				if (Layout.getScrollMode()){
					_moveTargetElement.scrollIntoView()
				} else {
					_move_pos = _moveTargetElement.offsetLeft
					let infoPos = Layout.getPageInfoPos(obj.spine ,_move_pos)
					if (infoPos){
						ele_view_wrapper.scrollLeft(infoPos.pos)
					}
				}
				// var info = Layout.getCurrentPageInfo(gotoLeft);
				// var info_r = Layout.getCurrentPageInfo(gotoRight);

				Highlight.clearSearchHighlight()
				let selMovePos = _moveTargetElement.getBoundingClientRect()
				let movePos = (Layout.getScrollMode())? _moveTargetElement.offsetTop - selMovePos.height: _moveTargetElement.offsetLeft
				let _chapter = Layout.getChapterByPos(movePos)
				let rangeResults = Search.findSearchDataBySpine(_chapter ,obj.beforeParsedString)

				//결과값이 존재하면 그려준다.
				if (rangeResults && rangeResults.length >0){
					rangeResults.forEach(e=>{
						let rects = Highlight.getRectsFromElementWithOffset(e.startContainer , e.startOffset, e.endContainer, e.endOffset)
						if (rects){
							if (parentsRect.left <= rects[0].left && rects[0].left <= parentsRect.right){
	
							} else {
								Navi.GotoNextPage()
							}
							setTimeout(() => {
								Highlight.highlightWithRects(Highlight.getRectsFromElementWithOffset(e.startContainer , e.startOffset, e.endContainer, e.endOffset))	
							}, 0);
						}
					})
				} else {
					console.log("ㄴㄴ")
					var info = Layout.getCurrentPageInfo(movePos);
					var info_r = Layout.getCurrentPageInfo(nextMovePos);
				}
			} 
			//로드 안됨
			else {
				console.log('##[goSearchPage][not loaded]')
				Navi.GotoPageByPercentAsync(obj.percent).then(function(re){

					_moveTargetElement = ele_view_wrapper.get(0).querySelector('#bdb-chapter'+ obj.spine).querySelector('[cfi=\"'+_cfi+'\"]')	

					if (Layout.getScrollMode()){
						_moveTargetElement.scrollIntoView()
					} else {
						_move_pos = _moveTargetElement.offsetLeft
						let infoPos = Layout.getPageInfoPos(obj.spine ,_move_pos)
						if (infoPos){
							ele_view_wrapper.scrollLeft(infoPos.pos)
						}
					}
					
					Highlight.clearSearchHighlight()
					let selMovePos = _moveTargetElement.getBoundingClientRect()
					let movePos = (Layout.getScrollMode())? _moveTargetElement.offsetTop - selMovePos.height: _moveTargetElement.offsetLeft
					let _chapter = Layout.getChapterByPos(movePos)
					let rangeResults = Search.findSearchDataBySpine(_chapter ,obj.beforeParsedString)
	
					//결과값이 존재하면 그려준다.
					if (rangeResults && rangeResults.length >0){
						rangeResults.forEach(e=>{
							let rects = Highlight.getRectsFromElementWithOffset(e.startContainer , e.startOffset, e.endContainer, e.endOffset)
							if (rects){
								if (parentsRect.left <= rects[0].left && rects[0].left <= parentsRect.right){
	
								} else {
									Navi.GotoNextPage()
								}
								setTimeout(() => {
									Highlight.highlightWithRects(Highlight.getRectsFromElementWithOffset(e.startContainer , e.startOffset, e.endContainer, e.endOffset))	
								}, 0);
							}
						})
					} else {
						console.log("ㄴㄴ",re)
					}
				}).catch(function(err){
					console.error('selection error', err)
				})
			}			
		},
		/**
		 * 검색한 페이지로 이동
		 * @param {*} obj 
		 * @returns 
		 */
		goSearchPage2: function(obj){
			console.log(obj)
			let spine = obj.spine
			let tagerEle = null
			let _cfi = null
			let li = null
			if (typeof spine !== 'number'){
				return Promise.reject('spine must be Number Type')
			}
			_cfi = obj.item.cfi
			if (typeof _cfi !== 'string'){
				return Promise.reject('_cfi must be String Type')
			}
			li = document.querySelector('#bdb-chapter'+spine)
			if (!li){
				return Promise.reject('not found Chapter')
			}
			return new Promise(function(resolve,reject){
				//로드
				// if (li && typeof li.getAttribute('loaded') === 'string' && li.getAttribute('loaded') === "1"){
				if (ePubEngineLoaderJS.Data.checkLoadChapter(spine)){
					tagerEle = li.querySelector(`[cfi="${_cfi}"]`)
					if (!tagerEle){
						return reject(`not found element [cfi="${_cfi}"]`)
					}
					Navi.calcNodeOffsetFromParent(tagerEle).then(pos=>{
						let result = Layout.getPosByPos(pos)
						if (!result){
							reject(`can not find pos in arrMap`)	
							return 
						}

						
						let estimateNextPage = 0
						let rePos = result.current.pos
						if (Layout.getScrollMode()){
							estimateNextPage = _viewer_height
							ele_view_wrapper.scrollTop(rePos)
						} else {
							estimateNextPage = Layout.getPageMode() ? (_viewer_width / 2) + _columnGapCorrect: _viewer_width;
							ele_view_wrapper.scrollLeft(rePos)
						}

						let pe = Util.findParent(tagerEle)
						console.log('PE',pe)
						// ePubEngineLoaderJS.Data.setCurrentChapter(spine);

						let rePos_next = rePos+estimateNextPage
						let left = Layout.getCurrentPageInfo(rePos);
						var right =  Layout.getCurrentPageInfo(rePos_next);
						return { left , right }
					}).then(rs=>{
						callbackPosition(rs.left,rs.right)
						resolve({ element: tagerEle,keyword: obj.beforeParsedString })
					}).catch(err=>{
						return reject(`fail to calc pos ${err}`)	
					})
				} 
				//로드해야됨
				else {
					Navi.moveToChapter(spine).then(e=>{
						tagerEle = li.querySelector(`[cfi="${_cfi}"]`)
						if (!tagerEle){
							return reject(`not found element [cfi="${_cfi}"]`)
						}
						Navi.calcNodeOffsetFromParent(tagerEle).then(pos=>{
							let result = Layout.getPosByPos(pos)
							if (!result){
								reject(`can not find pos in arrMap`)	
								return 
							}

							// ePubEngineLoaderJS.Data.setCurrentChapter(spine);

							let estimateNextPage = 0
							let rePos = result.current.pos
							if (Layout.getScrollMode()){
								estimateNextPage = _viewer_height
								ele_view_wrapper.scrollTop(rePos)
							} else {
								estimateNextPage = Layout.getPageMode() ? (_viewer_width / 2) + _columnGapCorrect: _viewer_width;
								ele_view_wrapper.scrollLeft(rePos)
							}

							let pe = Util.findParent(tagerEle)
							console.log('PE22',pe)
							let rePos_next = rePos+estimateNextPage
							let left = Layout.getCurrentPageInfo(rePos);
							var right =  Layout.getCurrentPageInfo(rePos_next);
							return { left , right }
						}).then(rs=>{
							callbackPosition(rs.left,rs.right)
							resolve({ element: tagerEle,keyword: obj.beforeParsedString })
						}).catch(err=>{
							return reject(`fail to calc pos ${err}`)	
						})
					}).catch(err=>{
						reject(err)
					})
				}
			})
		},
		/**
		 * 검색 이동 ver2
		 * 검색이동 후 해당 키워드에 하이라이트 한다.
		 * @param {*} obj 
		 * @returns 
		 */
		goSearchPage2WithDraw: function(obj){
			Highlight.deleteSearchHighlight()
			return Search.goSearchPage2(obj).then(rs=>{
				let offset = Position.ElementOffsetByKeyword(rs.element,rs.keyword)
				let rects = Position.ElementRectByOffset(rs.element, offset.start, offset.end)
				Highlight.highlightWithRects(rects)
				return true
			})
		},

		findNodeRange: function(element, targetString , startStringIndex, endStringIndex){
			console.log('## findNodeRange',element, targetString ,startStringIndex, endStringIndex)
			let list = null
			if (element && element.childNodes && element.childNodes.length >0 ){
				let count = 0
				let append = 0
				list = []
				for (let ele of element.childNodes){
					
					if (ele instanceof HTMLUnknownElement){

					} else {
						if (ele.nodeType === Node.TEXT_NODE){
							append += ele.textContent.length
							list.push({
								'index':count,
								'pos': append,
								'text':ele.textContent,
								'inRange': (startStringIndex < append && endStringIndex >= append)
							})

							console.log(ele.childNodes)
						} else {

						}
					}
		
					count++
				}
				console.log('## findNodeRange before', list)
				list = list.filter(e=> e.inRange)
			}

			let first = list[0]
			let last = list[ list.length -1 ]

			var sel = window.getSelection();
			sel.removeAllRanges();

			var range = document.createRange();
			range.setStart(element , first.index)
			range.setEnd(element , last.index)
			sel.addRange(range);

			console.log('## findNodeRange result', list)
			return list
		},

		/**
		 * 해당 spine에 해당되는 앨리먼트내에 모든 텍스트를 조회해서
		 * 텍스트 결과를 추출.
		 * @param {*} spineNumber 
		 */
		findSearchDataBySpine: function( spineNumber , keyword){

                
			let findParent = document.getElementById('bdb-chapter'+spineNumber)
			//chapter내 모든 text node를 가저온다.
			let searchCache = Position.getTextNodesAllWithObj( findParent )

			//full string
			let strFullScanSpine = searchCache.flatMap(e=>[e.node.textContent]).join("")
			let startIndexList = strFullScanSpine.indexOfAll(keyword)

			//범위내 텍스트가 존재하는지 filtering
			let resultList = Search.filterRangeTextExist(searchCache , startIndexList,findParent)

			//존재하는 텍스트 결과들에서 최종 샐랙션에 쓰일 element 및 offset을 추출
			let rangeResults = Search.getSelectionRangeFromResult( resultList )

		
			return rangeResults
		},
		/**
		 * 범위내 텍스트가 존재하는지 filtering
		 * @param { Array } searchCache 모든텍스트node 가 들어있는 list 
		 * @param { Array } startIndexList 해당단어의 시작 index list
		 * @param { Array } findParent 해당하는 chapter의 element
		 * @returns 
		 */
		filterRangeTextExist: function(searchCache , startIndexList , findParent){

			let resultList = []
			let group = []
			let isFInd = false
			let needAdd = false
			for (let e of startIndexList){
				isFInd = false
				needAdd = false
				group = []
				for (let j of searchCache){
					//첫인덱스 조회
					if (j.start <= e.start && j.end >= e.start){
						if (j.node.parentElement != findParent){
							group.push(j)
						}
						
						needAdd = true
					}

					//사이에 모든애들을 담는다.
					if (needAdd){
						if (group.indexOf(j) === -1){
							//찾은 노드의 parent가 챕터 앨리먼드 그자체면 잘못된 결과라 필터.
							if (j.node.parentElement != findParent){
								group.push(j)
							}
						}
					}
					//마지막 인덱스 조회
					if (j.start <= e.end && j.end >= e.end){
						isFInd = true
						break;
					}
				} 
				resultList.push({'range':e,'list':group})
			}
			group = null

			return resultList
		},


		/**
		 * 결과에서 selection할 범위의 아이템을 뽑아낸다.
		 * @param {*} resultList filterRangeTextExist의 결과 값을 넣으면 selection할 범위가 들은 List를 return
		 * @returns 
		 */
		getSelectionRangeFromResult: function(resultList){
                
			let result = []
			if (resultList&& resultList.length > 0){

				for(let item of resultList){


					let startContainer = null 
					let startOffset = -1

					let endContainer = null
					let endOffset = -1

					//한노드에 문자가 전부 들어있는경우
					if (item.list.length === 1){
						startContainer = item.list[0].node
						startOffset = item.range.start - item.list[0].start 
						endContainer = item.list[0].node
						endOffset = item.range.end - item.range.start + startOffset
					} 
					//여러 노드에 걸처 문자가 들어있는 경우
					else {
						startContainer = item.list[0].node
						startOffset = item.range.start - item.list[0].start 
						endContainer = item.list[item.list.length-1].node
						endOffset = item.range.end - item.list[item.list.length-1].start
					}

					if (startContainer && endContainer && startOffset != -1 && endOffset != -1){
						result.push({"startContainer":startContainer , "startOffset":startOffset, "endContainer":endContainer, "endOffset":endOffset})
					}
				}

			} 

			return result
		}
	};


	var Position = Core.Position = {
		/**
		 * 마지막 읽은 위치
		 * @returns object {path: lastReadPosSelector, offset: lastReadPosOffset}
		 */
		getLastReadPos: function() {
			cfi = null;
	
			try {
				cfi = this.getLastPageCfi();
	
				if(cfi != null && cfi[0][1] != undefined) {
					lastReadPos = cfi[0][1];
					lastReadPosSelector = cfi[0][0];
					lastReadPosOffset = cfi[0][2];
				}
				else {
					lastReadPos = '';
					lastReadPosSelector = '';
					lastReadPosOffset = 0;
				}
			}
			catch(ex) {
				ePubEngineLogJS.W.log(ex);
				lastReadPos = '';
				lastReadPosSelector = '';
				lastReadPosOffset = 0;
			}
			finally {
				cfi = null;
			}
	
			return {path: lastReadPosSelector, offset: lastReadPosOffset};
		},
		getTextNodeRect: function(textNode){
			if (document.createRange) {
				var range = document.createRange();
				range.selectNodeContents(textNode);
				if (range.getClientRects) {
					var rect = range.getClientRects();
					return rect;
				}
			}
			return null;
		},

		/**
		 * 화면상에 몇퍼센트로 스캔할지에대한 비율 ex) 0.2, 0.9
		 * @param {*} ratio 
		 */
		scanCfi: function(ratio){
			let scanRatio = 1;
			if (ratio){
				scanRatio = ratio;
			}
			
			//안쪽 보정값
			var scanCorrect = 0;
			var ele = ele_view_wrapper.get(0);
			var eleRect = ele.getBoundingClientRect();

			var findX = parseInt( scanCorrect + eleRect.left);
			var findY = parseInt(scanCorrect + eleRect.top);
			var caretRangeStart = Util.caretRangeFromPoint(document, findX, findY);
			let startElement = caretRangeStart.startContainer;
			let startOffset = caretRangeStart.startOffset;
			let encodeCfi_start = this._encodeCFI(document, startElement, startOffset, "");
	
			var findX1 = parseInt(eleRect.left + _viewer_width*scanRatio - scanCorrect);
			var findY1 = parseInt(eleRect.top + _viewer_height*scanRatio - scanCorrect);
			var caretRangeEnd = Util.caretRangeFromPoint(document, findX1, findY1);
			let endElement = caretRangeEnd.endContainer;
			let endOffset = caretRangeEnd.endOffset;
			let encodeCfi_end = this._encodeCFI(document, endElement, endOffset, "");

			// let find1 = Array.from(startElement.childNodes).some(child => child.nodeType === child.TEXT_NODE);
			// let find2 = Array.from(endElement.childNodes).some(child => child.nodeType === child.TEXT_NODE);
			let find1 = Position.getTextNodesAll(startElement)
			let find2 =Position.getTextNodesAll(endElement)

			console.log('startElement',typeof find1, find1,startElement,startElement.nodeType);
			console.log('endElement',typeof find2, find2,endElement,endElement.nodeType);
			// console.log(startElement, endElement, find1, find2);
			if (startElement.nodeType === 3){
				let s1 = Position.getTextNodeRect(startElement);
				let tl = startElement.textContent.length;
				let s1l = s1.length;
				console.log('## textnode rect',startElement,s1,startElement.textContent,tl,s1l);


			}	
			if (endElement.nodeType === 3){
				let s2 = Position.getTextNodeRect(endElement);
				let tl2 = endElement.textContent.length;
				let s2l = s2.length;
				console.log('## textnode rect',endElement,s2,endElement.textContent,tl2,s2l);
			}

			//해당 element가 text일경우만 text 가저오기.
			var _result = null;
			if (startElement.nodeType === 3 && endElement.nodeType === 3){
				_result = Position._GetText(encodeCfi_start, startElement, 0, encodeCfi_end, endElement, endOffset,true,true);
			}
			
			return _result;
		},

		
		/**
		 *  sjhan 220330
		 * spine 번호 기준 모든 textnode를 가저와서
		 * caretRangeFromPoint와 비교해 가장 적절한 텍스트 값을 뽑아낸다.
		 * @param {*} spine 
		 */
		scanCurrentVisiblePageCfi: function(spine, limitTextLength){
			let x = 0
			let y = 0
			let viewerRect = null
			let _start_x = null
			let _start_y = null
			var caretRangeStart = null
			let result = null
			let _spine = spine
			let spineElement = null
			ele_view_wrapper.get(0).classList.remove('none-selection')
			try {
				x = 0
				y = 0
				viewerRect = ele_view_wrapper.get(0).getBoundingClientRect()
				_start_x = x + viewerRect.left + 10
				_start_y = y + viewerRect.top + 10
				caretRangeStart = Util.caretRangeFromPoint(document, _start_x, _start_y);
				spineElement = document.getElementById('bdb-chapter'+spine)
				 
				//reSearch until find some element
				while(caretRangeStart.startContainer === spineElement){
					_start_y += 10
					caretRangeStart = Util.caretRangeFromPoint(document, _start_x, _start_y);
					if (_start_y === viewerRect.height){
						break
					}
				}

				 //다음 spine 탐색 , 스크롤모드일때 현재 spine에서 찾지 못하면 다음 spine으로 이동해 검색한다.
				//  console.log(caretRangeStart.startContainer)
				if (spineElement.lastChild === caretRangeStart.startContainer|| spineElement.lastChild.previousSibling === caretRangeStart.startContainer){
					_spine += 1

					//마지막페이지인경우는 탐색을 한번더 안해도됨
					if (_spine === ePubEngineLoaderJS.Data.getTotalSpine()){

					} else {
						let _next_chapter = document.getElementById('bdb-chapter'+_spine)
						let _next_chapter_rect = _next_chapter.getBoundingClientRect()
						_start_y = _next_chapter_rect.top
						caretRangeStart = Util.caretRangeFromPoint(document, _start_x, _start_y);
						_next_chapter = null
						_next_chapter_rect = null
					}
				}
				 
				result = Position.checkCaretRange(_spine , caretRangeStart, limitTextLength)

				// if (typeof result.data  === 'string'){
				// 	console.log(result,result.data.length)
				// } else {
				// 	console.log(result)
				// }
				ele_view_wrapper.get(0).classList.add('none-selection')
				 return result
			} catch(error){
				ele_view_wrapper.get(0).classList.add('none-selection')
			} finally {
				x = null
				y = null
				viewerRect = null
				_start_x = null
				_start_y = null
				caretRangeStart = null
				result = null
				_spine = null
				
			}
		},

		/**
		 * sjhan 220330
		 * 현재 보고있는 페이지의 상단 text를 확인해 반환한다.
		 * @param {*} spine 파일번호
		 * @param {*} caretRangeStart caretPoint
		 * @param {*} limitTextLength 제한하고 싶은 글자 길이
		 * @returns 
		 */
		checkCaretRange:function(spine , caretRangeStart, limitTextLength){

			// console.log('## checkCaretRange',caretRangeStart,spine)
			let limitText = 80;
			if (limitTextLength > 0){
				limitText = limitTextLength
			}
			let result = null
			let _cfi = null
			let _children = null
			let _spineElement = null
			let _selecter = null
			
			_selecter = Position._getPath(caretRangeStart.startContainer)
			_spineElement = document.getElementById('bdb-chapter'+spine)

			_cfi = this._encodeCFI(document, caretRangeStart.endContainer, caretRangeStart.endOffset, "");

			// TODO: chapter 그자체인경우는 return 별도의 처리하는 로직이 필요할것같음
			if (_spineElement === caretRangeStart.startContainer){
				// console.error('## chapter 그자체')
				return result
			}
			//첫 아이템이 element
			if (caretRangeStart.startContainer.nodeType === Node.ELEMENT_NODE){

				//다음 앨리먼트 탐색해서 text node & text길이가 빈값이 아닌 값을 조회.
				let _ele = caretRangeStart.startContainer.nextSibling
				let _mediaContents = null
				while(_ele){
					if (_ele instanceof HTMLUnknownElement){
						_ele = _ele.nextSibling;
						continue
					}
					if (_ele && _ele.nodeType === Node.TEXT_NODE){
						if ( _ele.textContent.replace(/\s/g, "").length > 0 ){
							break
						}
					} else if (_ele && _ele.nodeType === Node.ELEMENT_NODE){
						if ( _ele.textContent.replace(/\s/g, "").length > 0 ){
							break
						}
					}
					_ele = _ele.nextSibling;
				}

				//컨텐츠와 택스트가 같이있는경우 
				if (_ele){
					_selecter = Position._getPath(_ele)
					// console.log('## 컨텐츠와 택스트가 같이있는경우 ',_ele,_ele.nodeType == Node.TEXT_NODE)
					let _tx = _ele.textContent
					let _tx_childNode = null
					let _findArray = null
					let _sameNodeFind = false
					let _IsafterAddItem = false
				
					let _temp1 = []
					let _temp2 = []
					let _nodeTempText = null
					let _tempText = ''
					let _subText = null

					// 길이가 원하는만큼 안나왔을때 더찾아야 함. 
					if (_tx.length < limitText){
						_findArray = Position.getTextNodesAll(_spineElement)
						_tx_childNode = _ele.childNodes[0]
						_findArray.forEach(function(textnode){
							if (_tx_childNode && textnode === _tx_childNode){
								console.log('_tx_childNode',_tx_childNode)
								_sameNodeFind = true
								_IsafterAddItem = true
							} else {
								_sameNodeFind = false
							}

							//찾은 노드 뒤로 모든 아이템을 담는다
							if (_IsafterAddItem){
								//white space filter
								_nodeTempText = null
								if (_sameNodeFind){
									_nodeTempText = _tx
									_temp1.push(_nodeTempText)
								} else {
									_nodeTempText = textnode.textContent.replace(/\s/g, "");
									// if (_nodeTempText.length > 0){
									// 	_temp1.push(textnode.textContent)
									// }
									_temp1.push(textnode.textContent)
								}
								_nodeTempText = null
							}
						})

						//filter2: 찾은 텍스트 노드들중 limit값 내에 들어오는 값을 찾음
						_temp1.forEach(function(textnode){
							if (_tempText.length >= limitText){
								return false
							}
							let remainLength = limitText - _tempText.length
							_subText = textnode.substring(remainLength,0) 
							_tempText += _subText + ''
							_temp2.push({'node':textnode,'offset':_subText.length})
						})

						// console.log('길이가 원하는만큼 안나왔을때 더찾아야 함. ')
						return result = {
							'type': 'text',
							'data': _tempText,
							'nodes': _temp2,
							'cfi': _cfi,
							'selecter':_selecter,
							'element': caretRangeStart.startContainer
						}
					} 
					//원하는 길이 만큼 찾았을떄.
					else {
						_tx = _tx.substring(limitText,0) 
						return result = {
							'type': 'text',
							'data': _tx,
							'nodes':[{'node':caretRangeStart.startContainer,'offset':off}],
							'cfi': _cfi,
							'selecter':_selecter,
							'element': caretRangeStart.startContainer
						}
					}
				}
				//컨텐츠만 존재하는경우
				else {
					let __ccc = null
					_children = caretRangeStart.startContainer.children
					if (_children){
						
						for (let child of _children){
							//가장처음 만난 미디어 컨텐츠를 찾아 리턴시킨다.
							if (child.tagName === 'IMG'){
								__ccc = child
								_mediaContents = {
									'name':child.tagName,
									'src':child.src,
								}
								break;
							} else if (child.tagName === 'VIDEO'){
								__ccc = child
								_mediaContents = {
									'name':child.tagName,
									'src':child.src,
								}
								break;
							} else if (child.tagName === 'AUDIO'){
								__ccc = child
								_mediaContents = {
									'name':child.tagName,
									'src':child.src,
								}
								break;
							}
						}
					}
					
					result = {
						'type': Node.ELEMENT_NODE,
						'data': _mediaContents,
						'nodes':[{'node':caretRangeStart.startContainer,'offset':null}],
						'cfi': _cfi,
						'selecter':caretRangeStart.startContainer,
						'element': __ccc
					}
					
				}

				_ele = null
				_mediaContents = null
				return result;
			}
			//첫 아이템이 textNode
			else if (caretRangeStart.startContainer.nodeType === Node.TEXT_NODE){
				
				// _selecter = Position._getPath(_ele)
				if (caretRangeStart.startContainer.parentNode){
					_selecter = Position._getPath(caretRangeStart.startContainer.parentNode)
					// console.log('##TEXT_NODE ',)
				}
				
				let off = caretRangeStart.startOffset
				let tx = caretRangeStart.startContainer.textContent
				//찾은 텍스트중에 최초 기준이되는 값
				let rs = tx.substring(off-1)

				//첫 caretRange에 원하는 길이의 text를 찾앗을 경우
				if (rs.length>limitText) {
					let _rtext = rs.substring(0,limitText);
					result = {
						'type': 'text',
						'data':_rtext,
						'nodes':[{'node':caretRangeStart.startContainer,'offset':off}],
						'cfi': _cfi,
						'selecter':_selecter,
						'element': caretRangeStart.startContainer.parentNode
					}
					return result
				} 
				
				let _sameNodeFind = false
				let _IsafterAddItem = false
				let _findArray = Position.getTextNodesAll(_spineElement)
				let _temp1 = []
				let _temp2 = []
				let _nodeTempText = null
				let _tempText = ''
				let _subText = null

				//filter1: 공백이 아니면서 글자수가 있는 text node를 찾음
				_findArray.forEach(function(textnode,index){
					if (textnode === caretRangeStart.startContainer){
						_sameNodeFind = true
						_IsafterAddItem = true
					} else {
						_sameNodeFind = false
					}
	
					//찾은 노드 뒤로 모든 아이템을 담는다
					if (_IsafterAddItem){
						//white space filter
						_nodeTempText = null
						if (_sameNodeFind){
							_nodeTempText = rs
							_temp1.push(_nodeTempText)
						} else {
							_nodeTempText = textnode.textContent.replace(/\s/g, "");
							if (_nodeTempText.length > 0){
								_temp1.push(textnode.textContent)
							}
							// _temp1.push(textnode.textContent)
						}
						_nodeTempText = null
					}
				})
	
				//filter2: 찾은 텍스트 노드들중 limit값 내에 들어오는 값을 찾음
				_temp1.forEach(function(textnode){
					if (_tempText.length >= limitText){
						return false
					}
					let remainLength = limitText - _tempText.length
					_subText = textnode.substring(remainLength,0) 
					_tempText += _subText + ''
					_temp2.push({'node':textnode,'offset':_subText.length})
				})
				
				
				result = {
					'type': 'text',
					'data': _tempText,
					'nodes': _temp2,
					'cfi': _cfi,
					'selecter':_selecter,
					'element': caretRangeStart.startContainer.parentNode
				}
			}

			return result
		},
		/**
		 * 현재 화면에 보이는 모든 text를 full scan
		 * @returns 
		 */
		fullScanCfi:function(spineno){
			try{
				if (Selection.IsShowSelection()){
					Selection.HideSelection();
				}
				if (Selection.callbackRequestVisible){
					Selection.callbackRequestVisible(false);
				}
				//안쪽 보정값
				var scanCorrect = 0;
				var ele = ele_view_wrapper.get(0);
				var eleRect = ele.getBoundingClientRect();
				var findX = parseInt( scanCorrect + eleRect.left);
				var findY = parseInt(scanCorrect + eleRect.top);
				var findX1 = parseInt(eleRect.left + _viewer_width - scanCorrect);
				var findY1 = parseInt(eleRect.top + _viewer_height - scanCorrect);
				
				
				var caretRangeStart = Util.caretRangeFromPoint(document, findX, findY);
				var caretRangeEnd = Util.caretRangeFromPoint(document, findX1, findY1);

				var i = null;
				var i2 = null;
				if (caretRangeStart == null && caretRangeEnd == null) {
					if (!Selection.IsShowSelection()){
						Selection.ShowSelection();
					}
					if (Selection.callbackRequestVisible){
						Selection.callbackRequestVisible(true);
					}
					return null;
				}
				if(caretRangeStart != null){
					i = this._encodeCFI(document, caretRangeStart.startContainer, caretRangeStart.startOffset, "");
				}
				if(caretRangeEnd != null){
					i2 = this._encodeCFI(document, caretRangeEnd.endContainer, caretRangeEnd.endOffset, "");
				}

				var _result = null;
				// if (typeof spineno === 'number'){
				// 	_result = Position._GetBetweenElementText(spineno, caretRangeStart , caretRangeEnd);
				// } else {
				console.log(caretRangeStart.startContainer,caretRangeEnd.endContainer);
					_result = Position._GetText(i, caretRangeStart.startContainer, 0, i2, caretRangeEnd.endContainer, caretRangeEnd.endOffset,true,true);
				// }

				if (!Selection.IsShowSelection()){
					Selection.ShowSelection();
				}
				if (Selection.callbackRequestVisible){
					Selection.callbackRequestVisible(true);
				}
				return _result;
			}catch(e){
				return null;
			} finally{
				findX = null;
				findY = null;
				findX1 = null;
				findY1 = null;
				caretRangeStart = null;
				caretRangeEnd = null;
			}
		},
		/**
		 * anchor의 src id로 해당 element를 찾음.
		 * @param {*} fileno
		 * @param {*} id 
		 */
		getTocAnchorElement:function(fileno , id) {
			if (id){
				const findElement = $('#bdb-chapter'+fileno).find('#'+id);
	
				if (findElement&&findElement.length>0){
					return findElement.get(0);
				}
			}
			return null;
		},
		/**
		 * 페이지의 왼쪽 상단에 위치한 element를 반환.
		 * @param {*} fileno 
		 * @param {*} JQuerySelector
		 */
		getLastPageElement:function (fileno , JQuerySelector) {
			if (JQuerySelector){
				const findElement = $('#bdb-chapter'+fileno).find(JQuerySelector);
				if (findElement&&findElement.length>0){
					return findElement.get(0);
				}
			}
			return null;
		},
		/**
		 * 해당 element의 모든 text node를 array로 return
		 * @param {*} element 
		 * @returns 
		 */
		getTextNodesAll:function(element) {
			var n, a=[], walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
			while(n=walk.nextNode()){
				if (n.parentNode instanceof HTMLUnknownElement){
					continue;
				}
				a.push(n);
			}
			n = null;
			return a;
		},
		/**
		 * 해당 element의 모든 text node를 array로 return
		 * @param {*} element 
		 * @returns 
		 */
		getTextNodesAllWithObject:function(element) {
			var n, a=[], walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
			let cfi = null
			let count = 0
			let pos = 0
			while(n=walk.nextNode()){
				count++
				if (n.parentNode instanceof HTMLUnknownElement){
					continue;
				}
				cfi = n.parentElement.getAttribute('cfi')
				let t = pos 
				pos += n.textContent.length
				a.push({'node':n,'cfi':cfi,'index':count,'text':n.textContent,'start':t,'end':pos});
			}
			n = null;
			cfi = null
			return a;
		},
		/**
		 * element를 넣으면 그안에 모든 택스트를 가저와 list로 반환한다.
		 */ 
		getTextNodesAllWithObj(element) {
			var n, a=[], walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
			let count = 0
			while(n=walk.nextNode()){
				if (n.parentNode instanceof HTMLUnknownElement){
					continue;
				}
				
				let pos = Array.prototype.indexOf.call(n.parentNode.childNodes, n)

				let start = 0
				if (a.length-1 < 0){

				} else {
					start = a[a.length-1].end
				}
				let end = start + n.textContent.length
				a.push({
					'node': n , 
					'parent': n.parentNode, 
					'indexFromParent': pos,
					'start': start,
					'end': end
				});
			}
			n = null;
			return a;
		},
		getLastPageCfi: function(maxTextNumber) {
			var i = null, i2 = null;
			var caretRangeStart = null;
			var caretRangeEnd = null;
			var strRtnArray = new Array();
			var strRtnLog_Debug = null;
			var maxNumber = 50;
			var range = null;
			if (typeof maxTextNumber === Number){
				maxNumber = maxTextNumber
			}
	
			try {
	
				strRtnArray[0] = new Array(); //'cfi'
				strRtnArray[1] = ''; //'text'
	
				// $('.viewer_wrapper').css('-webkit-user-select', 'auto');
	
				if ( ele_topmenu_container.is(":visible") ) {
					ele_topmenu_container.hide();
					ele_topmenu_container.attr('data', '1');
				}
	
				if( ele_bottommenu_container.is(":visible") ) {
					ele_bottommenu_container.hide();
					ele_bottommenu_container.attr('data', '1');
				}

				//viewer warrper의 좌표를 찾는다.
				var findX = 5 + ele_view_wrapper.offset().left;
				var findY = 5 + ele_view_wrapper.offset().top;
				var findX1 = ele_view_wrapper.offset().left + _columnWidth -1;
				var findY1 = ele_view_wrapper.offset().top + _viewer_height - 1;
				

				//ios에서 샐랙션 옵션을 막아두면 작동하지 않는다.
				caretRangeStart = Util.caretRangeFromPoint(document, findX, findY);
				caretRangeEnd = Util.caretRangeFromPoint(document, findX1, findY1);
	
				if( ele_topmenu_container.attr('data') == '1' ) {
					ele_topmenu_container.show();
					ele_topmenu_container.attr('data', '');
				}
	
				if( ele_bottommenu_container.attr('data') == '1' ) {
					ele_bottommenu_container.show();
					ele_bottommenu_container.attr('data', '');
				}
	

	
				if (caretRangeStart == null && caretRangeEnd == null) {
					strRtnLog_Debug = '스캔해도 CFI를 찾을 수 없음';
					return strRtnArray;
				}

				if(caretRangeStart != null){
					i = this._encodeCFI(document, caretRangeStart.startContainer, caretRangeStart.startOffset, "");
				}

	
				if(caretRangeEnd != null){
					i2 = this._encodeCFI(document, caretRangeEnd.endContainer, caretRangeEnd.endOffset, "");
				}

				strRtnArray[1] =  Position._GetText(i, caretRangeStart.startContainer, 0, i2, caretRangeEnd.endContainer, caretRangeEnd.endOffset);
				
				strRtnArray[1] = strRtnArray[1].replace(/[\n\t\r]{1,}/gi, '');
				strRtnArray[1] = strRtnArray[1].replace(/\s{2,}/gi, ' ');

				if (strRtnArray[1].length > caretRangeStart.startOffset && strRtnArray[1].length > (maxNumber + caretRangeStart.startOffset)) {
					strRtnArray[1] = strRtnArray[1].substr(0, maxNumber);
				}

				if (i != null) {
					strRtnArray[0][2] = caretRangeStart.startOffset;
					strRtnArray[0][1] = i;
					strRtnArray[0][0] = this._getPath(caretRangeStart.startContainer);
					strRtnLog_Debug = i;

					return strRtnArray;
				}
				else {
					if(i2 != null) {
						strRtnArray[0][2] = caretRangeEnd.endOffset;
						strRtnArray[0][1] = i2;
						strRtnLog_Debug = i2;
						strRtnArray[0][0] = this._getPath(caretRangeEnd.startContainer);
						return strRtnArray;
					}
					else {
						strRtnLog_Debug = '스캔해도 CFI를 찾을 수 없음';
						return strRtnArray;
					}
				}
	
			}
			catch (ex) {
				strRtnLog_Debug = '예외로 인한 실패';
				return strRtnArray;
			}
			finally {
				i = null;
				i2 = null;
				caretRangeStart = null;
				caretRangeEnd = null;
				range = null;
				strRtnArray = null;
				strRtnLog_Debug = null;
			}
		},
		GetCfiRects: function(cfi) {
			var rtnDraw = null;
			var nMovePageNo = null;
			var strCfiNext = null;
			var di = null;
			var maxLeft = null;
			var maxTop = null;

			try {
				strCfiNext = Position._PlusCfiCharIndex(cfi, 1);
				if (strCfiNext.indexOf(':0') > -1 && cfi.indexOf(':1') > -1) {
					// 1인 상태에서 Plus하려했으나, 뒤가 없어서 뺐음.(마지막글자임) 이런경우 뒤집음.
					rtnDraw = Position._GetDrawNodeByCfi(strCfiNext, cfi, false, true);


				}
				else {
					rtnDraw = Position._GetDrawNodeByCfi(cfi, strCfiNext, false, true);

				}
				
				return rtnDraw;
			}
			catch (ex) {

				return '';
			}
			finally {
				rtnDraw = null;
				nMovePageNo = null;
				strCfiNext = null;
				di = null;
				maxLeft = null;
				maxTop = null;
			}
		},
		/**
		 * 해당 노드를 jQuery Selector로 변환
		 * @param {node} selectedNode 선택 노드
		 * @returns 문자열 jQuery Selector
		 */
		_getPath: function(selectedNode) {
			var path = '';
			var node = $(selectedNode);

			while(node.length) {
				var realNode = node[0];
				var name = realNode.localName;

				if(realNode.nodeType == 3 || realNode.nodeType == 8) {
					node = node.parent();
					continue;
				}

				if(!name) break;

				name = name.toLowerCase();

				if(name == 'body') break;

				if(realNode.id != null && realNode.id != undefined && realNode.id != '') {

					if(realNode.id.indexOf('bdb-chapter') >= 0) {
						break;
					}

					return name + '#' + realNode.id.replace(/\./g, '\\.') + ( path ? '>' + path : '');
				}

				var parent = node.parent();

				var siblings=parent.children(name.replace(':','\\:'));
				if(siblings.length > 1) {
					name +=':eq('+siblings.index(realNode)+')';
				}
				path = name + (path ? '>' + path : '');
				node = parent;
			}

			return path;
		},
		_PlusCfiCharIndex: function(strCfi, nAddNumber) {
			var isDebug = false;
			var arrI = new Array();
			var temp_number = 0;
			var rtn = null;
			try {
				if (nAddNumber == 0) {
					return strCfi;
				}
	
				if (strCfi.indexOf(':') == -1) {
					return strCfi;
				}
	
				arrI = strCfi.split(':');
	
				rtn = this._decodeCFI(document, strCfi);
				if (rtn == null) {
					return strCfi;
				}
	
				if (nAddNumber > 0) {
					if (parseInt(arrI[1]) + 1 < rtn.node.nodeValue.length) {
						return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
					}
					else {
						// 텍스트 범위 벗어남
						if (parseInt(arrI[1]) == 0){
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
						} else{
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) - 1);
						}
					}
				}
				else {
					if (parseInt(arrI[1]) + 1 < rtn.nodeValue.length) {
						// 텍스트 범위 벗어남
						return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
					}
					else {
						if (parseInt(arrI[1]) == 0){
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
						} else{
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) - 1);
						}
					}
				}
			}
			catch (ex) {
				return strCfi;
			}
			finally {
				isDebug = null;
				arrI = null;
				temp_number = null;
				rtn = null;
			}
		},
		/**
		 * cfi를 큰지 작은지 비교한다.
		 * @param {*} strCfi_A 
		 * @param {*} strCfi_B 
		 * @returns 
		 */
		_CheckCfiCompare: function (strCfi_A, strCfi_B) {
			var isDebug = false;
			var arrRtn = new Array();
			var arrA = null;
			var arrB = null;
			var nMinLength = null;
			var isRevert = null;
			var idx = null;
			var arrSubA = null;
			var arrSubB = null;
			try {
				if (strCfi_A == strCfi_B) {
					arrRtn['Big'] = strCfi_A;
					arrRtn['Small'] = strCfi_B;
					return arrRtn;
				}

				arrA = strCfi_A.split('/');
				arrB = strCfi_B.split('/');
				nMinLength = (arrA.length > arrB.length) ? arrB.length : arrA.length;
				isRevert = false;
				for (idx = 0; idx < nMinLength; idx++) {
					if (idx + 1 == nMinLength) {
						arrSubA = arrA[idx].split(':');
						arrSubB = arrB[idx].split(':');

						if (parseInt(arrSubA[0]) > parseInt(arrSubB[0])) {
							isRevert = true;
							break;
						}
						else if (arrSubA.length > 1 && arrSubB.length > 1
								 && parseInt(arrSubA[0]) == parseInt(arrSubB[0])
								 && parseInt(arrSubA[1]) > parseInt(arrSubB[1])) {
							isRevert = true;
							break;
						}
						else {
							break;
						}
					}
					else if (parseInt(arrA[idx]) == parseInt(arrB[idx])) {
						continue;
					}
					else if (parseInt(arrA[idx]) > parseInt(arrB[idx])) {
						isRevert = true;
						break;
					}
					else if (parseInt(arrA[idx]) < parseInt(arrB[idx])) {
						isRevert = false;
						break;
					}
				}

				if (isRevert) {
					arrRtn['Big'] = strCfi_B.replace('/b/g', 'a');
					arrRtn['Small'] = strCfi_A.replace('/b/g', 'b');
				}
				else {
					arrRtn['Big'] = strCfi_A;
					arrRtn['Small'] = strCfi_B;
				}
				return arrRtn;
			}
			finally {
				isDebug = null;
				arrRtn = null;
				arrA = null;
				arrB = null;
				nMinLength = null;
				isRevert = null;
				idx = null;
				arrSubA = null;
				arrSubB = null;
			}
		},
		/**
		 * cfi로부터 point rect값을 return한다.
		 * @param {*} cfi 
		 * @returns 
		 */
		_pointFromCFI: function (cfi) {
			if (cfi == null || cfi == '') {
				return null;
			}
	
			var isDebug = true;
			var r = null;
			var node = null;
			var ndoc = null;
			var nwin = null;
			var tl_x = null;
			var tl_y = null;
			var br_x = null;
			var br_y = null;
			var range = null;
			var tryList = null;
			var k = null;
			var a = null;
			var nodeLen = null;
			var t = null;
			var startOffset = null;
			var endOffset = null;
			var rect = null;
	
			var strRtnLog_Debug = null;
			try {
				r = this._decodeCFI(document, cfi);
				if (!r || r == '') {
					strRtnLog_Debug = '_decodeCFI fail';
					return null;
				}
	
				node = r.node;
				ndoc = node.ownerDocument;
				if (!ndoc) {
					strRtnLog_Debug = 'ownerDocument fail';
					node = null;
					ndoc = null;
					return null;
				}
	
				nwin = ndoc.defaultView;
				if (typeof r.offset == "number") {
					range = ndoc.createRange();
					tryList = null;
					if (r.forward) {
						tryList = [{ start: 0, end: 0, a: 0.5 }, { start: 0, end: 1, a: 1 }, { start: -1, end: 0, a: 0 }];
					}
					else {
						tryList = [{ start: 0, end: 0, a: 0.5 }, { start: -1, end: 0, a: 0 }, { start: 0, end: 1, a: 1 }];
					}
	
					k = 0;
					nodeLen = node.nodeValue.length;
					do {
						if (k >= tryList.length) {
							strRtnLog_Debug = 'tryList.length fail';
							return null;
						}
	
						t = tryList[k++];
						startOffset = r.offset + t.start;
						endOffset = r.offset + t.end;
						a = t.a;
						//if( startOffset < 0 || endOffset >= nodeLen ) {
						if (startOffset < 0 || endOffset > nodeLen) {
							continue;
						}
						range.setStart(node, startOffset);
						range.setEnd(node, endOffset);
						rects = range.getClientRects();
					}
					while (!rects || !rects.length);
	
					rect = rects[0];
	
					tl_t = rect.top;
					tl_l = rect.left;
					br_b = rect.bottom;
					br_r = rect.right;
					rect = null;
					k = null;
					a = null;
					nodeLen = null;
				}
				else {
					tl_t = node.offsetTop;
					tl_l = node.offsetLeft - nwin.scrollX;
					br_b = node.offsetBottom;
					br_r = node.offsetRight - nwin.scrollX;
	
					if (typeof r.x == "number" && node.offsetWidth) {
						tl_t += (r.x * node.offsetWidth) / 100;
						tl_l += (r.y * node.offsetHeight) / 100;
					}
				}
	
				if (ndoc != document) {
					strRtnLog_Debug = 'ndoc != document fail';
					return null;
				}
	
				return { point: r, tl_t: tl_t, tl_l: tl_l, br_b: br_b, br_r: br_r };
			}
			catch (ex) {
				throw ex;
			}
			finally {
				isDebug = null;
				r = null;
				node = null;
				ndoc = null;
				nwin = null;
				tl_x = null;
				tl_y = null;
				br_x = null;
				br_y = null;
				range = null;
				tryList = null;
				k = null;
				a = null;
				nodeLen = null;
				t = null;
				startOffset = null;
				endOffset = null;
				rect = null;
				strRtnLog_Debug = null;
			}
		},
		/**
		 * 노드와 offset으로 cfi 변환한다.
		 * @param {*} doc 
		 * @param {*} node 
		 * @param {*} offset 
		 * @param {*} tail 
		 * @returns 
		 */
		_encodeCFI: function (doc, node, offset, tail) {
			var isDebug = false;
			var cfi = tail || "";
			var p = null;
			var parent = null;
			var win = null;
			var index = null;
			var child = null;
			try {

				switch (node.nodeType) {
					case 1:
						if (typeof offset == "number") {
							if (node.childNodes.item(offset) != null){
								node = node.childNodes.item(offset);
							}
								
							else {
								if (node.childNodes.length > 0){
									node = node.childNodes.item(node.childNodes.length - 1);
								}
							}
						}
						break;
					default:
						offset = offset || 0;
						while (true) {
							p = node.previousSibling;
							if (!p || p.nodeType == 1) {
								break;
							}
							// CFI r3002
							switch (p.nodeType) {
								case 3:
								case 4:
								case 5:
									offset += p.nodeValue.length;
							}
							node = p;
						}
						cfi = ":" + offset + cfi;
						break;
				}

				while (node !== doc) {
					if (node == null)
						break;

					parent = node.parentNode;
					if (!parent) {
						if (node.nodeType == 9) {
							win = node.defaultView;
							if (win.frameElement) {
								node = win.frameElement;
								cfi = "!" + cfi;
								continue;
							}
						}
						break;
					}


					index = 0;
					child = parent.firstChild;
					while (true) {
						index |= 1;
						if (child.nodeType == 1) {
							index++
						}
						if (child === node) {
							break;
						}
						child = child.nextSibling;
					}
					// CFI r3002
					//if( node.id && node.id.match(/^[-a-zA-Z_0-9.\u007F-\uFFFF]+$/) )
					//{
					//  index = index + "[" + node.id + "]";
					//}
					cfi = "/" + index + cfi;
					node = parent;
				}

				return cfi;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][this._encodeCFI] ' + (ex.number & 0xFFFF));
				ePubEngineLogJS.W.log('[★Exception★][this._encodeCFI] ' + ex);
				return '';
			}
			finally {
				isDebug = null;
				cfi = null;
				p = null;
				parent = null;
				win = null;
				index = null;
				child = null;
			}
		},
		/**
		 * cfi를 노드와 offset으로 다시 변환한다.
		 * @param {*} doc 
		 * @param {*} cfi 
		 * @returns 
		 */
		_decodeCFI: function (doc, cfi , spine) {

			if (cfi == null || cfi == '') {
				return null;
			}

			var isDebug = true;
			var node = doc;
			var error = null;
			var r = null;
			var targetIndex = null;
			var index = null;
			var child = null;
			var offset = null;
			var point = {};
			var len = null;
			var next = null;
			var oldNode;// 마지막이 br 일경우를 위함 임시 저장소


			try {

				while (cfi.length > 0) {
					//if( (r = cfi.match(/^\/(\d+)/)) !== null )
					if ((r = cfi.match(/^\/(\d+)/)) !== null)
						// CFI r3002
						// if( (r = cfi.match(/^\/(\d+)(\[([-a-zA-Z_0-9.\u007F-\uFFFF]+)\])?/)) !== null )
					{
						targetIndex = r[1] - 0;
						index = 0;
						child = node.firstChild;
						try {
							while (true) {
								if (!child) {
									error = "child not found: " + cfi;
									ePubEngineLogJS.W.log('[Selection._decodeCFI] error : ' + error);
									return null;
									//break;
								}

								index |= 1;
								if (child.nodeType === 1) {
									index++;
								}

								if (index === targetIndex) {
									cfi = cfi.substr(r[0].length);
									node = child;

									// br로 끝난 다면 북마크를 위한 임시 텍스트 지정
									if (node.nodeName.toLowerCase() == 'br') {
										node = oldNode;
										node.setAttribute("bookMake", "bookMake");
									}
									break;
								}
								child = child.nextSibling;
								if (node.nodeName.toLowerCase() != 'br') {
									oldNode = child;
								}

								// if (child instanceof HTMLUnknownElement){
								// 	unknownCount++
								// 	index++;
								// 	node = child;
								// 	child = child.nextSibling;
									
								// 	continue
								// }
							}
						}
						finally {
							child = null;
							targetIndex = null;
							index = null;
						}
					}
					else if ((r = cfi.match(/^!/)) !== null) {
						if (node.contentDocument) {
							node = node.contentDocument;
							cfi = cfi.substr(1);
						}
						else {
							error = "Cannot reference " + node.nodeName + "'s content: " + cfi;
						}
					}
					else {
						break;
					}
				}

				if ((r = cfi.match(/^:(\d+)/)) !== null) {
					offset = r[1] - 0;
					cfi = cfi.substr(r[0].length);
				}
				if ((r = cfi.match(/^~(-?\d+(\.\d+)?)/)) !== null) {
					point.time = r[1] - 0;
					cfi = cfi.substr(r[0].length);
				}
				if ((r = cfi.match(/^@(-?\d+(\.\d+)?),(-?\d+(\.\d+)?)/)) !== null) {
					point.x = r[1] - 0;
					point.y = r[3] - 0;
					cfi = cfi.substr(r[0].length);
				}
				if ((r = cfi.match(/^[ab]/)) !== null) {
					point.forward = r[0] == "a";
					cfi = cfi.substr(1);
				}
				// CFI r3002
				//if( (r = cfi.match(/^\[.*(;s=[ab])?.*\]/)) !== null ) // pretty lame
				//{
				//  if( r[1] )
				//  {
				//      point.forward = r[0] == "s=a";
				//      cfi = cfi.substr(1);
				//  }
				//}

				if (offset !== null) {
					while (true) {
						// console.log('## 444')
						// if (node instanceof HTMLUnknownElement){
						// 	node = node.nextSibling;
						// 	continue
						// }
						if (node.nodeValue == null) {
							if (node.childNodes.item(offset) != null)
								node = node.childNodes.item(offset);
							else {
								if (node.childNodes.length > 0)
									node = node.childNodes.item(node.childNodes.length - 1);
							}
							continue;
						}
						else {
							len = node.nodeValue.length;
							if (offset < len || (!point.forward && offset === len)) {
								len = null;
								break;
							}

							next = node.nextSibling;
						}

						if (next == null) {
							offset = len;
							break;
						}

						// CFI v3002 [
						while (true) {
							var type = next.nodeType;
							if (type === 3 || type === 4 || type === 5) {
								break;
							}
							if (type == 1) {
								next = null;
								break;
							}
							next = next.nextSibling;
						}
						//]
						if (!next) {
							if (offset > len) {
								error = "Offset out of range: " + offset;
								offset = len;
							}
							break;
						}
						node = next;
						offset -= len;
					}
					point.offset = offset;
				}

				if (error) {
					ePubEngineLogJS.W.log('[Selection._decodeCFI] error : ' + error);
					return null;
				}
				else if (cfi.length > 0) {
					ePubEngineLogJS.W.log('[Selection._decodeCFI] cfi.length > 0, cfi : ' + cfi);
					return null;
				}


				point.node = node;
				return point;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][Selection._decodeCFI] ' + ex);
				return null;
			}
			finally {
				isDebug = null;
				node = null;
				error = null;
				r = null;
				targetIndex = null;
				index = null;
				child = null;
				offset = null;
				point = null;
				len = null;
				next = null;
			}
		},
		/**
		 * node를 xpath로 변환한다.
		 * @param {*} doc 
		 * @param {*} target 
		 * @returns 
		 */
		_encodeXPath: function (doc, target) {
			var strXPath = '';

			if (target.parentNode == doc) {
				strXPath = '/HTML' + strXPath;
			}
			else {
				var siblingIdx = 0;
				var prevSiblingNode = target.previousSibling;

				while (prevSiblingNode != null) {
					if (prevSiblingNode.tagName == target.tagName)
						siblingIdx++;

					prevSiblingNode = prevSiblingNode.previousSibling;
				}

				if (siblingIdx > 0 && target.nodeType == 1) {
					strXPath = '/' + target.tagName + '[' + (siblingIdx + 1) + ']' + strXPath;
				}
				else if (siblingIdx == 0 && target.nodeType == 1) {
					strXPath = '/' + target.tagName + strXPath;
				}
				else if (siblingIdx == 0 && target.nodeType == 3) {
					strXPath = '/text()' + strXPath;
				}
				else if (siblingIdx > 0 && target.nodeType == 3) {
					strXPath = '/text()' + '[' + (siblingIdx + 1) + ']' + strXPath;
				}

				strXPath = this._encodeXPath(doc, target.parentNode) + strXPath;
			}

			return strXPath;
		},
		/**
		 * 좌표값을 받아 좌표값에 해당하는 xpath,cfi등의 정보를 반환
		 * @param {Object} doc 
		 * @param {Number} x 
		 * @param {Number} y 
		 * @param {Boolean} isWordBoundary 문단 형태로 return받을지에 대한 parameter
		 * @returns 
		 */
		_cfiAt: function (doc, x, y, isWordBoundary) {
			var rtnInfo = null;

			if (Selection.callbackRequestVisible){
				Selection.callbackRequestVisible(false);
			}

			if (doc == null || doc == '') {
				if (Selection.callbackRequestVisible){
					Selection.callbackRequestVisible(true);
				}
				return rtnInfo;
			}

			var isDebug = true;
			var target = null;
			var endTarget = null;
			var cdoc = doc;
			var cwin = cdoc.defaultView;
			var tail = "";
			var offset = null;
			var startOffset = null;
			var endOffset = null;
			var name = null;
			var range = null;
			var px = null;
			var py = null;
			var selText = null;

			rtnInfo = new Array();

			try {
				// [Comment1 Start]
				// while문은, x/y포지션 아래의 element가 iframe/object/embed 태그면, 그 내부의 포지션을 잡아내기 위해서
				// 찾아들어가는 코드, 결과적으로는, iframe이든 아니든 현재 선택될 엘리먼트를 찾아내는 코드
				while (true) {
					target = cdoc.elementFromPoint(x, y);
					if (!target) {
						return null;
					}

					/*
					 * <p>&nbsp</p> 같은 경우 문제가 생기는걸 막기위해 .
					 * 처리하는 부분 공백문자 단순 /br 등
					 *
					 */
					var innerText = Util.Trim(target.textContent);
					if (innerText.length == 0) {
						return null;
					}
					name = target.localName;

					if (name != "iframe" && name != "object" && name != "embed") {
						break;
					}
					var cd = target.contentDocument;
					if (!cd) {
						break;
					}

					x = x + cwin.scrollX - target.offsetLeft;
					y = y + cwin.scrollY - target.offsetTop;
					cdoc = cd;
					cwin = cdoc.defaultView;

					cd = null;
				}
				// [Comment1 End]

				// [Comment2 Start]
				// 엘리먼트는 최종적으로 찾았음. 그 엘리먼트에 따른 처리
				if (name == "video" || name == "audio") {
					//tail = "~" + Position._fstr(target.currentTime);
				}

				if (name == "img" || name == "video") {
					//px = ((x + cwin.scrollX - target.offsetLeft)*100)/target.offsetWidth;
					//py = ((y + cwin.scrollY - target.offsetTop)*100)/target.offsetHeight;
					//tail = tail + "@" + Position._fstr(px) + "," + Position._fstr(py);
					//px = null;
					//py = null;
				}
				else if (name != "audio") {

					if (cdoc.caretRangeFromPoint) {
						range = cdoc.caretRangeFromPoint(x, y);
						
						if (range) {
							target = range.startContainer;
							offset = range.startOffset;
							
							selText = target.textContent;
							
							
							selText = selText.replace(/\s+/g,' ');
							/**
							 * redmine refs #14501
							 * 일본어 하이라이트 안되는 문제로 로직 수정
							 * by junghoon.kim
							 */
							

							if (selText != null) {
								if (isWordBoundary) {
									if (selText.lastIndexOf(' ', offset) >= 0) {
										startOffset = selText.lastIndexOf(' ', offset);
										
										startOffset += 1;
									}
									else {
										startOffset = offset;
									}
								}
								else {
									startOffset = offset;
								}

								

								if (isWordBoundary) {
									if (selText.indexOf(' ', offset + 1) > offset) {
										endOffset = selText.indexOf(' ', offset + 1)
										
									}
									else {
										var endPointIndex = selText.search(/[.?!]/g);

										if (endPointIndex != -1) {
											endOffset = selText.length;
										}
										else if (endOffset + 1 < selText.length) {
											endOffset = offset + 1;
										}
										else {
											endOffset = offset;
										}
									}
								}
								else {
									endOffset = offset;
								}


								//sjhan 211006 위 로직에서 걸러도 여전히 공백이 존재하는 string을 한번더 필터함.
								if (isWordBoundary){
									//text자체가 띄어쓰기 존재할때.
									if (Util.isBlank(selText)){
										var tempStartOffset = startOffset;
										var tempEndOffset = endOffset;
										var tempBackAreaString = selText.substring(tempStartOffset, selText.length);
										var tempFrontAreaString = selText.substring(0, tempEndOffset);
										//뒷쪽 단어중 공백이 존재하는 경우.
										if (Util.isBlank(tempBackAreaString)){

										} else {
											endOffset = selText.length;
										}
		
										//앞쪽 단어중 공백이 존재하는 경우
										if (Util.isBlank(tempFrontAreaString)){

										} else {
											startOffset = 0;
										}

										tempStartOffset = null;
										tempEndOffset = null;
										tempBackAreaString = null;
										tempFrontAreaString = null;
									} 
									//text자체가 띄어쓰기 존재하지 않을때.
									else {
										startOffset = 0;
										endOffset = selText.length;
									}
								}
							}
							else {
								startOffset = offset;
								endOffset = offset;
							}



						}
						else {
						}
						range = null;
					}
					else {
					}
				}
				else {
				}

				rtnInfo[2] = startOffset;
				rtnInfo[1] = this._encodeXPath(doc, target);

				rtnInfo[5] = endOffset;
				rtnInfo[4] = this._encodeXPath(doc, target);

				rtnInfo[0] = this._encodeCFI(doc, target, startOffset, tail);
				rtnInfo[3] = this._encodeCFI(doc, target, endOffset, tail);

				rtnInfo[6] = this._encodeCFI(doc, target, offset, tail);
				rtnInfo[7] = this._encodeXPath(doc, target);
				rtnInfo[8] = offset;

				//ePubEngineLogJS.L.log(rtnInfo[0]);
				if (Selection.callbackRequestVisible){
					Selection.callbackRequestVisible(true);
				}
				return rtnInfo;
			}
			finally {
				isDebug = null;
				target = null;
				cdoc = null;
				cwin = null;
				tail = null;
				offset = null;
				name = null;
				range = null;
				px = null;
				py = null;
				selText = null;
				endOffset = null;
				startOffset = null;
				rtnInfo = null;
			}
		},
		/**
		 * 정한 CFI 시작/끝 값으로 실제 DIV를 그릴 포지션 정보를 리턴 
		 * 무조건 start가 end보다 커야 한다.
		 * @param {string} strStartCfi 시작 CFI값
		 * @param {string} strEndCfi 끝 CFI값
		 * @param {boolean} isMakeText 텍스트값도 추출 (Optional)
		 * @param {boolean} isByPassSameCfi 같은 CFI여도 정보를 가져오기 (Optional)
		 * @returns 
		 */
		_GetDrawNodeByCfi: function (strStartCfi, strEndCfi, isMakeText, isByPassSameCfi) {
			var isDebug = true;
			var startPoint = null;
			var endPoint = null;
			var start = null;
			var end = null;
			var strSelectionID = null;
			var range = null;
			var rects = null;
			var strSelectionText = null;
			var drawList = null;
			var drawList_Idx = null;
			var k = null;
			var rect = null;
			var isDual = null;
			var rect_next = null;
			var dual_idx = null;
			var bookMakeTempNode = null;
			try {
				if (strStartCfi == strEndCfi && isByPassSameCfi == null) {
					ePubEngineLogJS.E.log('시작과 끝이 같습니다.');
					return -7;
				}


				// draw calc
				startPoint = this._pointFromCFI(strStartCfi);
				endPoint = this._pointFromCFI(strEndCfi);

				if (startPoint == null || endPoint == null) {
					ePubEngineLogJS.E.log('계산할 수 없습니다.(-5)');
					return -5;
				}

				start = startPoint.point;
				end = endPoint.point;
				if (start == null || end == null) {
					startPoint = null;
					endPoint = null;
					ePubEngineLogJS.E.log('계산할 수 없습니다.(-6)');
					return -6;
				}


				/**
				 * redmine refs #12638
				 * 챕터 처음의 cfi를 인식 하지 못해서 챕터 끝으로 이동됨
				 * 
				 */
				if ((ele_view_wrapper.get(0) == start.node || start.node == document.getElementsByTagName("BODY")[0])) {
					return -9; // body 및 container 영역
				}

				var bookMake = false;
				try {
					if (start.node.nodeName != '#text') {
						bookMake = (start.node.getAttribute("bookMake") != undefined) ? true : false;
					}
				} catch (e) {
					console.log(e.toString());
				}
				if (bookMake) {
					bookMakeTempNode = document.createTextNode("BookMake");
					start.node.appendChild(bookMakeTempNode);

				}

				strSelectionID = 'Rue_Selection_Container_SEL';
				range = document.createRange();

				if (bookMake && start.node == end.node && start.offset == end.offset) {
					range.selectNodeContents(start.node);
				}
				else {

					if (start.node == end.node && start.offset == end.offset) {
						range.selectNode(start.node);
					}
					else {
						if (typeof start.offset == "number") {
							range.setStart(start.node, start.offset);
						}
						else {
							range.setStartBefore(start.node);
						}

						if (typeof end.offset == "number") {
							range.setEnd(end.node, end.offset);
						}
						else {
							range.setEndBefore(end.node);
						}
					}

				}

				if (isMakeText) {
					strSelectionText = this._GetText(strStartCfi, start.node, start.offset, strEndCfi, end.node, end.offset , true);
				}
				else {
					strSelectionText = '';
				}

				rects = range.getClientRects();
				drawList = new Array();
				drawList_Idx = 0;

				for (k = 0; k < rects.length; k++) {
					rect = rects[k];
					if (/*rect.top < 0 || */rect.width <= 0 || rect.height <= 0) {
						// 내용이 없는 경우 높이 값이 -가 오는 경우는 무시한다 .
						// 내용이 없기 때문에 표시할 필요가 없음.
						// 위치 정보는 - 가 올수 있으므로 조건에서 제외한다.
						continue;
					}


					// 특정 노드에서 rect가 더 많이 넘어오는데 endNode의 right 보다 벗어난 rect는 제거 하기 위해
					if (endPoint.tl_t == rect.top && endPoint.br_b == rect.bottom) {
						if (rect.left > endPoint.br_r) {
							continue;
						}
					}
					/**
					 * redmine refs #16666
					 * br 테그가 있으면 br 바로앞 영역을 건너 뛰어서 셀렉션 되지 않는 문제 수정
					 * 이중으로 셀렉션 되는 문제 방어 코드
					 * by junghoon.kim
					 */
					if (true) {
						isDual = false;
						for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
							// across check
							rect_next = rects[dual_idx];
							if (rect_next) {
								// across check
								if (rect.top >= rect_next.top
									&& rect.bottom <= rect_next.bottom
									&& rect.left <= rect_next.left
									&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										 && rect.bottom >= rect_next.bottom
										 && rect.left >= rect_next.left
										 && rect.right <= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										 && rect.bottom >= rect_next.bottom
										 && rect.left <= rect_next.left
										 && rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								//range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
							}
						}
						if (isDual) {
							continue;
						}
					}
					// else {
						// 하이라이트 시 해당 셀렉션 영역에 br 테그가 있으면 아래 로직에 의해 br 바로앞 영역이 패스 되어
						// 화면에 그려지지 않는 버그로 인해 주석 처리함
						// 아래 로직이 dual 모드시 동작되는 로직 같은데 정확히 파악된 로직은 아니라
						// 다른 사이드 이펙트가 없는지 많은 검토가 필요함

						//                            //kyungmin ios 이미지 사이즈 버그로 인한 수정
						//                            isDual = false;
						//                            for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
						//                                // across check
						//                                rect_next = rects[dual_idx];
						//                                if (rect_next) {
						//                                    // across check
						//                                    if (rect.top >= rect_next.top
						//                                        && rect.bottom <= rect_next.bottom
						//                                        && rect.left <= rect_next.left
						//                                        && rect.right >= rect_next.right) {
						//                                        isDual = true;
						//                                        break;
						//                                    }
						//                                    else if (rect.top <= rect_next.top
						//                                             && rect.bottom >= rect_next.bottom
						//                                             && rect.left >= rect_next.left
						//                                             && rect.right <= rect_next.right) {
						//                                        isDual = true;
						//                                        break;
						//                                    }
						//                                    else if (rect.top <= rect_next.top
						//                                             && rect.bottom >= rect_next.bottom
						//                                             && rect.left <= rect_next.left
						//                                             && rect.right >= rect_next.right) {
						//                                        isDual = true;
						//                                        break;
						//                                    }
						//                                    //range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
						//                                }
						//                            }
						//                            if (isDual) {
						//                                continue;
						//                            }
					// }

					if (rect.width == 0) {
						//LastReadCFI시 width가 0인 것 때문에 CFI 페이지 계산이 전페이지 나오는 현상제거
	
						continue;
					}




					//210803 - sjhan 공백 체크 로직 추가.
					var xx = parseFloat(rect.left)+(parseFloat(rect.width)/2);
					var yy = parseFloat(rect.top)+(parseFloat(rect.height)/2);
					var selnode = document.elementFromPoint(xx,yy);
					if (selnode){
						if (selnode.innerText){
							if (selnode.innerText.replace(/\s+/g,'').length <= 0){
								continue;
							}
						}
					}

					//210917 뷰어 영역 밖으로 샐랙션을 막기위함.
					if (Selection.viewerPadding){
						//현재 뷰어의 rect를 가저와 넘어가버리면 result에 추가하지 않는다.
						var viewerW = parseInt(_viewer_width + Selection.viewerPadding.left);
						var viewerH = parseInt(_viewer_height + Selection.viewerPadding.top);
						var lLeft = parseInt(rect.left);

						if ( parseInt(Selection.viewerPadding.left) > lLeft ||
							 viewerW <= lLeft|| 
							Selection.viewerPadding.top > rect.top ||
							 viewerH < rect.top ){
							targetWidth = null;
							targetHeight = null;
							viewerW=null;
							viewerH=null;
							continue;
						}
					}
					

					drawList[drawList_Idx] = new Array();
					drawList[drawList_Idx]['id'] = parseInt(k);
					drawList[drawList_Idx]['top'] = (parseInt(rect.top) + parseInt(window.scrollY)) + "px";
					drawList[drawList_Idx]['rleft'] = parseInt(rect.left);
					drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";
					drawList[drawList_Idx]['width'] = rect.width + "px";
					drawList[drawList_Idx]['height'] = rect.height + "px";
					drawList[drawList_Idx]['rect'] = rect;


					if (drawList_Idx == 0) {//index의 0번에만 값을 채움
						drawList[drawList_Idx]['SelectionText'] = strSelectionText;
					}

					ePubEngineLogJS.L.log('[Position._GetDrawNodeByCfi] rect.top = ' + rect.top + ',   rect.left = ' + rect.left + ',   window.scrollX = ' + window.scrollX + ',   window.scrollY = ' + window.scrollY + ',   drawList[drawList_Idx][top] = ' + drawList[drawList_Idx]['top'] + ',   drawList[drawList_Idx][left] = ' + drawList[drawList_Idx]['left']);
					drawList_Idx++;
				}
				if (bookMake) {
					start.node.removeChild(bookMakeTempNode);
				}
				if (drawList_Idx == 0) {
					return -8; //검색영역없음
				}

				return drawList;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][Position._GetDrawNodeByCfi] ' + (ex.number & 0xFFFF));
				ePubEngineLogJS.W.log('[★Exception★][Position._GetDrawNodeByCfi] ' + ex);
				return -100; //예외
			}
			finally {
				isDebug = null;
				startPoint = null;
				endPoint = null;
				start = null;
				end = null;
				strSelectionID = null;
				range = null;
				rects = null;
				strSelectionText = null;
				drawList = null;
				drawList_Idx = null;
				k = null;
				rect = null;
				isDual = null;
				rect_next = null;
				dual_idx = null;
			}
		},
		/**
		 * text node가 화면밖으로 나가있는지  체크.
		 * @param {*} textNode 
		 * @returns 
		 */
		_IsOutOfScreenTextNode:function(textNode , isIgnoreWhiteSpace){
		
			if (textNode.nodeType === Node.TEXT_NODE){
				//white space의 경우 계산에서 제외한다.
				if (isIgnoreWhiteSpace){
					if (/^\s*$/.test(textNode.nodeValue)){
						return false;
					}
				}

				var _range = document.createRange();
				_range.selectNode(textNode);
				var _endTextNodeRect = _range.getBoundingClientRect();
				var viewrect = ele_view_wrapper.get(0).getBoundingClientRect();
				var limitWidth = viewrect.left+viewrect.width;
				if (limitWidth<_endTextNodeRect.left||
					_endTextNodeRect.left<viewrect.left||
					_endTextNodeRect.right>viewrect.right){
						
					return true;
				}
			}

			return false;
		},
		/**
		 * 스크린 안쪽으로 들어온지 확인.
		 * @param {*} textNode 
		 * @returns 
		 */
		_IsInScreenTextNode:function(textNode){
			if (textNode.nodeType === Node.TEXT_NODE){
				var update = true;
				var _range = document.createRange();
				_range.selectNode(textNode);
				

				var rects = Array.from(_range.getClientRects());
				if (rects&&rects.length>0){
					var right = Selection.viewerPadding.right;
					var lLeft = rects[0].left;
					if (right < lLeft){
						update = false;
					}
				}
				return update;
			}
			return false;
		},
		/**
		 * 해당 element의 모든 text node를 list로 return
		 * @param {*} element 
		 */
		_GetAllChildNodes:function(element){
			return element.childNodes;
		},
		/**
		 * 현재tag가 화면밖에 존재한다면 
		 * 화면안에 들어오는 이전 태그를 찾아 return
		 * @param {*} target 
		 * @returns 
		 */
		_GetPreviousNode:function(target){
			
			var result = null;
			var prevSiblingNode = target.previousSibling;
			var isNext = false;
			var count = 0;
			while(true) {
				if (count>1000){
					// console.log('timeout');
					break;
				}
				count++;

				
				if (!prevSiblingNode.previousSibling){
					break;
				}

				if (isNext){
					prevSiblingNode = prevSiblingNode.previousSibling;
				} else {
					isNext = true;
				}
				
				


				//난독화 tag는 제외한다.
				if (prevSiblingNode&&
					prevSiblingNode.classList&&
					prevSiblingNode instanceof HTMLUnknownElement){
					continue;
				}

				// text node
				if (prevSiblingNode.nodeType === Node.TEXT_NODE){
					//white space
					if (prevSiblingNode&& prevSiblingNode.textContent&&(/^\s*$/.test(prevSiblingNode.textContent)) ) {
						continue;
					}
					//check out of screen
					if (Position._IsOutOfScreenTextNode(prevSiblingNode)){
						continue;
					} else {
						break;
					}
				} 
				// default node
				else if (prevSiblingNode.nodeType === 1){
					var parent = prevSiblingNode;
					var childnodes = Position._GetAllChildNodes(parent);
					
					prevSiblingNode = childnodes[childnodes.length-1];
					if (prevSiblingNode.nodeType === Node.TEXT_NODE){
						break;
					} else {
						prevSiblingNode = parent.prevSiblingNode;
					}
				}
			}
			result = prevSiblingNode;
			prevSiblingNode = null;
			
			return result;
		},
		/**
		 * 시작지점과 끝나는 지점 사이에 있는 모든 text들을 return
		 * @param {*} caretRangeStart 시작지점 element
		 * @param {*} caretRangeEnd 끝나는 지점 element 
		 */
		_GetBetweenElementText:function(spineno, caretRangeStart , caretRangeEnd){
			// console.log('_GetBetweenElementText',caretRangeStart , caretRangeEnd);
			var _result = null;
			var _root = document.getElementById('bdb-chapter'+spineno);
			var _startElement = caretRangeStart.startContainer;
			var _startOffset = caretRangeStart.startOffset;
			var _endElement = caretRangeEnd.endContainer;
			var _endOffset = caretRangeEnd.endOffset;

			try{
				if (_endElement.nodeType == 3){
					_root = _startElement.parentNode.closest('li.chapter');
				}

				// other than whitespace
				var treeWalkerFilter = null;
				// {  
				// 	acceptNode: function(node) {
				// 	if ( ! /^\s*$/.test(node.data) ) { 
				// 		return NodeFilter.FILTER_ACCEPT; 
				// 	}
				//   }
				// };
				var treeWalker2 = document.createTreeWalker(_root,NodeFilter.SHOW_ALL,treeWalkerFilter,null);
				var count2 = 0;
				var front2 = -1;
				var end2 = -1;
				var _nodeList = [];
				while(treeWalker2.nextNode()) {
					var _currentNode = treeWalker2.currentNode;
					if (_currentNode === _startElement){
						front2 = count2;
					}

					if (_currentNode === _endElement){
						end2 = count2+1;
					}
					_nodeList.push(_currentNode);
					_currentNode = null;
					count2++;
				}
				// console.log('find idx ' , front2 , end2);

				if (end2>0&&front2>0){

					var _nodeList2 = _nodeList.slice(front2 ,end2)
											.flatMap((e,i)=>{
												if (e.nodeType === 1){
													return [];
												} else if (e.nodeType === 3){
													if (e.parentElement instanceof HTMLUnknownElement){
														return [];
													} else {
														if (Position._IsOutOfScreenTextNode(e)){
															return [];
														} else {
															return [{index:i,item:e}];
														}
													}
												} 
												return [{index:i,item:e}];
											});
			
					_result = _nodeList2.map(e=>e.item.textContent).join('');

					//현재상태에서 앞 뒤 엘리먼트들이 현재 페이지에 걸치는지 확인해야한다.

					//front case 노드를 뒤로 이동하면서 확인
					for(let _temp_front2 = front2; _temp_front2<_nodeList.length;_temp_front2--){
						let e = _nodeList[_temp_front2];
						if (e.nodeType === 1){
							
						} else if (e.nodeType === 3){
							if (e.parentElement instanceof HTMLUnknownElement){
								
							} else {
								let _range = document.createRange();
								_range.selectNode(e);
								break;
							}
						} 
					}

					//end case 노드를 앞으로 이동하면서 확인.
					// for(let _temp_end2 = end2; _temp_end2<_nodeList.length;_temp_end2++){
					// 	let e = _nodeList[_temp_end2];
					// 	if (e.nodeType === 1){
							
					// 	} else if (e.nodeType === 3){
					// 		if (e.parentElement.classList.contains('bdb')){
								
					// 		} else {
					// 			let _range = document.createRange();
					// 			_range.selectNode(e);
					// 			console.log('out',e , _temp_end2 ,_range.getClientRects() );
					// 			// break;
					// 		}
					// 	} 
					// }
				}

				


				treeWalker2 = null;
				count2 = null;
				front2 = null;
				end2 = null;
			} catch(e){
				console.error('_GetBetweenElementText',e)
			} finally{
				_root = null;
				_startElement = null;
				_startOffset = null;
				_endElement = null;
				_endOffset = null;
			}
			return _result;
		},
		/**
		 * 
		 * @param {string} startCfi 시작위치
		 * @param {node} start_node 시작 노드
		 * @param {number} start_offset 시작 노드의 오프셋
		 * @param {string} endCfi 끝 위치
		 * @param {node} end_node 끝 노드
		 * @param {number} end_offset 끝 노드의 텍스트 오프셋
		 * @param {number} isIgnoreParentText 자신의 parent의 전체 text를 제외해야하는지 여부
		 * @returns 추출된 텍스트
		 */
		_GetText: function (startCfi, start_node, start_offset, endCfi, end_node, end_offset, isIgnoreParentText) {
			var strText = '';
			var tmpRtnGetParent = '';
			var isDebug = false;
			var arrStart = startCfi.split(':')[0].split('/');
			var arrEnd = endCfi.split(':')[0].split('/');
			var isGetStartText = false;
			var currNode = null;
			var sameIndex = null;
			var StartNode_End = null;
			var EndNode_Start = null;
			var seq = null;
			var tmpStartNode = null;
			var tmpEndNode = null;
			var tempCurrNode = null;

			try {
				if (start_node === end_node) {
					//시작과 끝 노드가 동일
					return start_node.nodeValue.substr(start_offset, end_offset - start_offset);
				}

				// 어디까지 같은지 확인
				for (sameIndex = 0; sameIndex < arrStart.length; sameIndex++) {
					if (arrStart[sameIndex] == arrEnd[sameIndex]) {
						continue;
					}
					else {
						break;
					}
				}

				for (seq = arrStart.length; seq > sameIndex; seq--) {
					if (StartNode_End == null) {
						if (seq - 1 > sameIndex) {
							StartNode_End = start_node.parentNode;
						}
						else {
							StartNode_End = start_node.nextSibling;
							if (StartNode_End == null) {
								return 'not found';
							}
						}
					}
					else {
						if (seq - 1 > sameIndex) {
							StartNode_End = StartNode_End.parentNode;
						}
						else {
							StartNode_End = StartNode_End.nextSibling;
							if (StartNode_End == null) {
								return 'not found';
							}
						}
					}
				}

				for (seq = arrEnd.length; seq > sameIndex; seq--) {
					if (EndNode_Start == null) {
						if (seq - 1 > sameIndex) {
							EndNode_Start = end_node.parentNode;
						}
						else {
							EndNode_Start = end_node.previousSibling;
							if (EndNode_Start == null) {
								return 'not found';
							}
						}
					}
					else {
						if (seq - 1 > sameIndex) {
							EndNode_Start = EndNode_Start.parentNode;
						}
						else {
							EndNode_Start = EndNode_Start.previousSibling;
							if (EndNode_Start == null) {
								return 'not found';
							}
						}
					}
				}

				// if (!isIgnoreParentText){
					// 앞부분부터 중복위치 다음까지  ㄱㄱ
				for (seq = arrStart.length; seq > sameIndex; seq--) {
					if (!isGetStartText) {
						tmpRtnGetParent = this._GetParentText(start_node, start_offset, StartNode_End, true, true);
						strText += tmpRtnGetParent.toString();
						tmpRtnGetParent = null;
						isGetStartText = true;
						currNode = start_node.parentNode;
					}
					else {
						// if (!isIgnoreParentText){
							tmpRtnGetParent = this._GetParentText(currNode, null, StartNode_End, false, true);
							strText += tmpRtnGetParent;
							tmpRtnGetParent = null;
						// }
						currNode = currNode.parentNode;
					}
				}
				// }

				


				// 증복 다음처리  부분의 중간땅

				tmpStartNode = StartNode_End.previousSibling;
				tmpEndNode = EndNode_Start.nextSibling;
				if (tmpStartNode != tmpEndNode) {
					for (tempCurrNode = StartNode_End; tempCurrNode != null && tempCurrNode != tmpEndNode; tempCurrNode = tempCurrNode.nextSibling) {
						var isNotPassNode = false;
						
						if(tempCurrNode.nodeType == 3) {
							isNotPassNode = window.getComputedStyle(tempCurrNode.parentNode).display != 'none';
						}

						if(tempCurrNode.nodeType == 1) {
							isNotPassNode = window.getComputedStyle(tempCurrNode).display != 'none';

							if(isNotPassNode) {
								if(tempCurrNode.className) {
									isNotPassNode = tempCurrNode.className.indexOf('chapter') == -1;
								}
							}

							if(isNotPassNode) {
								if(tempCurrNode.className) {
									isNotPassNode = tempCurrNode.className.indexOf('bdb_para_empty') == -1;
								}
							}
						}

						if (tempCurrNode.nodeType === 3 && isNotPassNode) {
							strText += tempCurrNode.nodeValue;
						}
						else if (tempCurrNode.nodeType === 8) {
							//#comment
						}
						else {
							if(isNotPassNode)
								strText += tempCurrNode.innerText;
						}
					}
				}
				tmpStartNode = null;
				tmpEndNode = null;

				
				// 마지막 처리
				// if (isIgnoreParentText){

				// } 
				// else {
					isGetStartText = false;
					for (var seq = arrEnd.length; seq > sameIndex; seq--) {
						if (!isGetStartText) {
							tmpRtnGetParent = this._GetParentText(end_node, end_offset, EndNode_Start, true, false);
							strText += tmpRtnGetParent;
							tmpRtnGetParent = null;
							isGetStartText = true;
							currNode = end_node.parentNode;
						}
						else {
							if (EndNode_Start == ele_view_wrapper.get(0)) {
								break;
							}
							tmpRtnGetParent = this._GetParentText(currNode, null, EndNode_Start, false, false);
							strText += tmpRtnGetParent;
							tmpRtnGetParent = null;
							currNode = currNode.parentNode;
						}
					}
				// }
				
				

				return strText;
			}
			finally {
				isDebug = null;
				strText = null;
				tmpRtnGetParent = null;
				arrStart = null;
				arrEnd = null;
				isGetStartText = null;
				currNode = null;
				sameIndex = null;
				StartNode_End = null;
				EndNode_Start = null;
				seq = null;
				tmpStartNode = null;
				tmpEndNode = null;
				tempCurrNode = null;
			}
		},
		/**
		 * 특정노드를 제한노드까지 상/하로 움직이면서 텍스트 추출
		 * @param {node} node 검색시작위치노드
		 * @param {number} offset 검색시작위치노드의 offset 값(추출텍스트offset)
		 * @param {*} LimitNode 지정한 노드를 만나면 중지
		 * @param {*} isIncludeSelf 자신의 텍스트 포함여부
		 * @param {*} isOrderBy true면 검색시작부터 뒤로검색, false면 앞으로검색
		 * @returns 추출된 텍스트
		 */
		_GetParentText: function (node, offset, LimitNode, isIncludeSelf, isOrderBy) {
			var pnode = node.parentNode;
			var currNode = null;
			var rtn = '';
			var isFind = null;
			var isNotPassNode = false;

			try {
				if (isOrderBy) {
					isFind = false;
					currNode = pnode.firstChild;
					while (currNode != null) {
						if (currNode === LimitNode) {
							break;
						}

						isNotPassNode = false;

						if(currNode.nodeType == 3) {
							isNotPassNode = window.getComputedStyle(currNode.parentNode).display != 'none';
						}

						if(currNode.nodeType == 1) {
							isNotPassNode = window.getComputedStyle(currNode).display != 'none';
						}

						if (node === currNode) {
							isFind = true;
							if (isIncludeSelf != null && isIncludeSelf == true) {
								if (offset != null && offset == '' && currNode.nodeValue != null && isNotPassNode) {
									rtn = currNode.nodeValue;
									// console.log('### _GetParentText - 1' , currNode,currNode.nodeValue)
									//맨처음 단어일경우 뒤쪽으로 튀어나갈 케이스가 잇어서 한번더 체크해준다.
									if (Selection.viewerPadding){
										var d = document.createRange();
										d.selectNode(currNode);
										d.getClientRects()
										var rects = Array.from(d.getClientRects());
										var filterRects = rects.filter(e=>e.left>=Selection.viewerPadding.left&&e.top>=Selection.viewerPadding.top);
										
										//top,left가 뷰어범위를 안벗어나는 애들로 가저온다.
										if (filterRects&&filterRects.length>0){
											var wholeTextNodeEstimateWidth = rects.map(e=>e.width).reduce((sum,cur)=>sum+cur,0);
											var estimateEachTextSize = wholeTextNodeEstimateWidth/currNode.nodeValue.length;
											var substringCount = filterRects[0].width/estimateEachTextSize;
											substringCount = parseInt(substringCount);
											var startCount = currNode.nodeValue.length - substringCount;
											//밖으로 벗어난 text들의 size를 예측해서 그만큼 다시 잘라준다.
											if (startCount>=0){
												rtn = currNode.nodeValue.substring(startCount);
											}
										}
									}
									
								}
								else {
									if (currNode.nodeValue != null && currNode.nodeValue != null && isNotPassNode) {
										rtn = currNode.nodeValue.substr(offset)
										// console.log('### _GetParentText - 2' , currNode)
									}
								}
							}
						}
						else if (isFind) {
							if (currNode.nodeType === 3 && currNode.nodeValue != null && isNotPassNode) {
								rtn += currNode.nodeValue;
								// console.log('### _GetParentText - 3' , currNode,currNode.nodeValue.length)
							}
							else if (currNode.nodeType === 8) {
								//#comment
							}
							else {
								if(isNotPassNode && currNode.innerText != null){
									rtn += currNode.innerText;
									// console.log('### _GetParentText - 4' , currNode)
								}
							}
						}
						currNode = currNode.nextSibling;
					}
				}
				else {
					isFind = false;
					currNode = pnode.lastChild;
					while (currNode != null) {
						if (currNode === LimitNode) {
							break;
						}

						isNotPassNode = false;

						if(currNode.nodeType == 3) {
							isNotPassNode = window.getComputedStyle(currNode.parentNode).display != 'none';
						}

						if(currNode.nodeType == 1) {
							isNotPassNode = window.getComputedStyle(currNode).display != 'none';
						}

						if (node === currNode) {
							isFind = true;
							//rtn.currCnt = i+1;
							if (isIncludeSelf != null && isIncludeSelf == true) {
								if (offset != null && offset == '' && currNode.nodeValue != null && isNotPassNode) {
									rtn = currNode.nodeValue;
									// console.log('##1' , currNode);
								}
								else {
									if (currNode.nodeValue != null && isNotPassNode){
										var update = Position._IsInScreenTextNode(currNode);
										if (update){
											rtn = currNode.nodeValue.substr(0, offset)
										}
										// if (isFullscan){

										// } else {
										// 	rtn = currNode.nodeValue.substr(0, offset)
										// }

										
									}
								}
							}
						}
						else if (isFind) {
							if (currNode.nodeType === 3 && currNode.nodeValue != null && isNotPassNode) {
								var update = Position._IsInScreenTextNode(currNode);
								if (update){
									rtn = currNode.nodeValue + rtn;
								}
								// if (isFullscan){

								// } else {
								// 	rtn = currNode.nodeValue + rtn;
								// }

							}
							else if (currNode.nodeType === 8) {
								//#comment
							}
							else {
								if(isNotPassNode && currNode.innerText != null) {
									rtn = currNode.innerText + rtn;
								}
							}
						}
						currNode = currNode.previousSibling;
					}
				}

				return rtn;
			}
			finally {
				pnode = null;
				currNode = null;
				rtn = null;
				isFind = null;
			}
		},
		/**
		 * @param {*} x 
		 * @param {*} y 
		 */
		getLeftTopSelector:function(){
			var viewerRect = document.getElementById('contents').getBoundingClientRect();
			var _correctX = viewerRect.width/4;
			var _correctY = viewerRect.height/4;
			var x = viewerRect.left + _correctX;
			var y = viewerRect.top + _correctY;
			var find = document.elementFromPoint(x,y);

			return find;
		},

		nextElement: function(element){
			let _ele = element.nextElementSibling
			while(true){
				if (_ele instanceof HTMLUnknownElement){
					_ele = _ele.nextElementSibling
				} else {
					break
				}
			}

			return _ele
		},
		prevElement: function(element){
			let _ele = element.previousElementSibling
			while(true){
				if (_ele instanceof HTMLUnknownElement){
					_ele = _ele.previousElementSibling
				} else {
					break
				}
			}
			return _ele
		},
		/**
		 * node map cached
		 */
		chapterInfoMap: new Map(),
		getNodeInfo(node){
			let chapter =  Util.findParentChapter(node)
			let li = null
			let currentList = null
			if (chapter >= 0){
				li = document.getElementById('bdb-chapter'+chapter)
				if (Position.chapterInfoMap.has(chapter)){
					currentList = Position.chapterInfoMap.get(chapter)
				} else {
					currentList = Position.getTextNodesAllWithObj(li)
					Position.chapterInfoMap.set(chapter,currentList)
				}
			} 
			return currentList
		},
		/**
		 * sjhan 20220415
		 * cfi 추출
		 * 현재 챕터기준으로 추출한 대상의 
		 * start string index와 
		 * end string index 추출
		 */
		 extractCfi: function(){

			//spine
			let cht = ePubEngineLoaderJS.Data.getCurrentSpine()
			
			//string
			let scif = null
			let ecif = null
			
			//node
			let sNode = null
			let eNode = null

			//get data from spine
			let currentLi = null
			let currentList = null
			let currentLiFullText = null

			let startEleList = null
			let startLiFullText = null

			//result
			let findStart = null
			let findEnd = null

			let _startFromParentIndex = -1
			let _endFromParentIndex = -1
			
			let _startIndexRelativeFromParent = -1
			let _endIndexRelativeFromParent = -1
			let _extractText = null

			let selected =  null
			let selectedText = null

			let firstInfoInParent = null
			let lastInfoInParent = null

			let _start_result = -1;
			let _end_result = -1;
			let result = null
			let _start_chapter = -1
			let _end_chapter = -1
			try {
				//페이지더보기시 추가
				if (Selection.selectionCacheMode === -1){
					scif = Selection.m_strDragStartCfi
					ecif = Selection.m_strDragEndCfi
				} 
				//샐랙션된 친구
				else {
					scif = Selection.selectionCache.start
					ecif = Selection.selectionCache.end
				}


				//시작cfi, 끝cfi기준으로 선택된 텍스트 정보를 가저옴.
				selected =  Selection.getContinueSelectionText(scif , ecif)
				selectedText = selected.textResult


				//node
				sNode = Position._decodeCFI(document, scif)
				eNode = Position._decodeCFI(document, ecif)

				
				//get data from spine

				//뒤쪽
				currentList = Position.getNodeInfo(eNode.node)
				currentLiFullText = currentList.map(e=>[e.node.textContent]).join('')

				//앞쪽
				startEleList = Position.getNodeInfo(sNode.node)
				startLiFullText = startEleList.map(e=>[e.node.textContent]).join('')

				//챕터안 텍스트 노드중 cfi에 해당되는 처음과 끝을 조회
				//start
				for (let e of startEleList){
					if (sNode.node === e.node){
						findStart = e
						break;
					}
				}
				//end
				for (let e of currentList){
					if (eNode.node === e.node){
						findEnd = e
						break;
					}
				}
				endEleList = null
				endEleLi = null
				
				if (sNode.node){
					let ec = Util.findParentChapter(sNode.node)
					if (typeof ec === 'number' && ec >= 0){
						cht = ec
					}
					ec = null
					findStart = new Object()
					findStart['cfiAttr'] = sNode.node.parentElement.getAttribute('cfi')
					findStart['offset'] = sNode.offset
					findStart['chapter'] = cht
					findStart['tagPos'] = sNode.node.parentElement.getAttribute('tag-current')
					_start_chapter = cht
				}

				if (eNode.node){
					let ec = Util.findParentChapter(eNode.node)
					if (typeof ec === 'number' && ec >= 0){
						cht = ec
					}
					findEnd = new Object()
					findEnd['cfiAttr'] = eNode.node.parentElement.getAttribute('cfi')	
					findEnd['offset'] = eNode.offset
					findEnd['chapter'] = cht
					findEnd['tagPos'] = eNode.node.parentElement.getAttribute('tag-current')
					_end_chapter = cht
				}


			
				if (findStart && findEnd){
					let relativeFromParent1 = null
					let relativeFromParent1_condition = false
					let relativeFromParent2 = null
					let relativeFromParent2_condition = false

					for(let ee of startEleList){
						if (!relativeFromParent1_condition){
							if (ee.node.parentNode === sNode.node.parentNode){
								relativeFromParent1 = ee
								relativeFromParent1_condition = true
							}
						}
						if (ee.node == sNode.node){
							firstInfoInParent = ee
							break;
						}	
					}
					for(let ee of currentList){
						if (!relativeFromParent2_condition){
							if (ee.node.parentNode === eNode.node.parentNode){
								relativeFromParent2 = ee
								relativeFromParent2_condition = true
							}
						}
						if (ee.node == eNode.node){
							lastInfoInParent = ee
							break;
						}	
					}

					


					//case1: 같은 텍스트 노드인 경우
					if (sNode.node === eNode.node){
						let _nodes = Position.getTextNodesAllWithObj(sNode.node.parentNode)
						let startParentText = -1
						//parent text node 중 가장 첫번째 아이를 찾음
						for (let ee of currentList){
							if (_nodes[0].node === ee.node){
								startParentText = ee.start
								break;
							}
						}

			
						//전체 문자열에서 몇번째 텍스트인지 확인한다.
						_startFromParentIndex = firstInfoInParent.start + selected.startCfi.offset
						_endFromParentIndex = firstInfoInParent.start + selected.endCfi.offset 

						_extractText = currentLiFullText.substring(_startFromParentIndex , _endFromParentIndex)

						//parent tag로 부터 몇번째 문자열인지.
						 _start_result = _startFromParentIndex - startParentText
						 _end_result = _start_result + _extractText.length

					} 
					//case2: 다른 텍스트 노드인경우
					else {
						//앞
						let startAllnodes = Position.getTextNodesAllWithObj(sNode.node.parentNode)
						let startND = null
						for (let i = startAllnodes.length - 1; i >= 0; i--) {
							if (sNode.node === startAllnodes[i].node){
								startND = startAllnodes[i];
								break;
							}
						}
						if (startND){
							_start_result = startND.start + sNode.offset
						} else {
							throw 'not fount startIndexFromParent'
						}
						//뒤
						let endAllnodes = Position.getTextNodesAllWithObj(eNode.node.parentNode)
						let endND = null
						for (let i = 0; i <endAllnodes.length; i++) {
							if (eNode.node === endAllnodes[i].node){
								endND = endAllnodes[i];
								break;
							}
						}
						if (endND){
							_end_result = endND.start + eNode.offset
						} else {
							throw 'not fount endIndexFromParent'
						}


						selectedText = Position.getStringByNode(
							sNode.node,
							sNode.offset,
							eNode.node,
							eNode.offset)
						startAllnodes = null
						startND = null
						endAllnodes = null
						endND = null
					}
				}
				result = {
					'scif': scif,
					'ecif': ecif,
					'startCfi': findStart ,
					'endCfi': findEnd ,
					'text': selectedText,
					'textFromParent': _extractText,
					'startChapter': _start_chapter,
					'endChapter': _end_chapter,
					'startIndex': _startFromParentIndex ,
					'endIndex': _endFromParentIndex,
					'chapter': cht,
					'startIndexFromParent': _start_result,
					'endIndexFromParent': _end_result,
				}
				return result
			} catch(error){
				console.error('extractCfi error',error)
			} finally {
				//spine
				 cht = null
							
				//string
				 scif = null
				 ecif = null

				//node
				 sNode = null
				 eNode = null

				//get data from spine
				 currentLi = null
				 currentList = null
				 currentLiFullText = null

				//result
				 findStart = null
				 findEnd = null

				 _startFromParentIndex = null
				 _endFromParentIndex = null
				 _extractText = null

				 selected =  null
				 selectedText = null

				 firstInfoInParent = null
				 lastInfoInParent = null
				 result = null
			}
		},
		/**
		 * 두 노드 사이의 모든 택스트를 가저온다.
		 * @param {*} startNode 시작노드
		 * @param {*} startOffset 시작노드의 offset
		 * @param {*} endNode 끝노드
		 * @param {*} endOffset 끝노드의 offset
		 * @returns 
		 */
		 getStringByNode(startNode,startOffset,endNode,endOffset){
			let range = new Range();
			range.setStart(startNode,startOffset);
			range.setEnd(endNode, endOffset);
			let fragment = range.cloneContents();
			let wk = Position.getTextNodesAll(fragment)
			return wk.map(e=>e.textContent).join('')
		},
		/**
		 * attr cfi로 text 추출
		 * @param {*} startCfiAttr 
		 * @param {*} startIndexFromParent 
		 * @param {*} endCfiAttr 
		 * @param {*} endIndexFromParent 
		 * @returns 
		 */
		attrCfiToString:function(startCfiChapter,startCfiAttr,startIndexFromParent,endCfiChapter,endCfiAttr,endIndexFromParent){

			let sElement = document.querySelector('[cfi=\"'+startCfiAttr+'\"]')
			let eElement = document.querySelector('[cfi=\"'+endCfiAttr+'\"]')
		
			if (!sElement || !eElement){
				return;
			}

			let allNodes = null
			let allNodes2 = null
			let sChapterLi = null
			let eChapterLi = null
			let resultStrList = null
			let parentStr = null
	
			let front = null
			let frontFind = null
			let frontRelativeIndex = null
			let back = null
			let backFind = null
			let backRelativeIndex = null
			let rangeList = []
			try {
				//같은 앨리먼트인 경우 바로 뽑아낸다.
				if (sElement===eElement){
					allNodes = Position.getTextNodesAllWithObj(sElement)
					resultStrList = allNodes.flatMap(e=>[e.node.textContent])
					parentStr = resultStrList.join('')
					return parentStr.substring(startIndexFromParent,endIndexFromParent)
				} else {
					sChapterLi = Util.findParent(sElement)
					eChapterLi = Util.findParent(eElement)

					//앞쪽 text node
					front = Position.getTextNodesAllWithObj(sElement)
					frontFind = front.find(e=> startIndexFromParent>=e.start&&startIndexFromParent<e.end)
					frontRelativeIndex = startIndexFromParent - frontFind.start

					//뒤쪽 text node
					back = Position.getTextNodesAllWithObj(eElement)
					backFind = back.find(e=> endIndexFromParent>=e.start&&endIndexFromParent<e.end)
					backRelativeIndex = backFind.node.textContent.length - (backFind.end - endIndexFromParent)

					//같은 챕터내 존재할때.
					if (sChapterLi === eChapterLi){
						// console.log('[same parent]')
						allNodes = Position.getTextNodesAllWithObj(sChapterLi)

						//frontFind,backFind 범위내 모든 textNode를 담는다.
						let addstart = false
						for (let obj of allNodes){
							if (obj.node === frontFind.node){
								addstart = true
							}
							if (addstart){
								rangeList.push(obj)
							}
							if (obj.node === backFind.node){
								break;
							}
						}
					} 
					//다른 챕터내 존재할때.
					else {

						allNodes = Position.getTextNodesAllWithObj(sChapterLi)
						allNodes2 = Position.getTextNodesAllWithObj(eChapterLi)

						//앞쪽 chapter의 textNode중 시작지점부터 전부담는다.
						let addstart = false
						for (let obj of allNodes){
							if (obj.node === frontFind.node){
								addstart = true
							}
							if (addstart){
								rangeList.push(obj)
							}
						}
						//뒤쪽 chapter의 textNode중 끝지점까지 전부담는다.
						for (let obj of allNodes2){
							rangeList.push(obj)
							if (obj.node === backFind.node){
								break;
							}
						}
					}


					//범위내 모든 text를 수집한다.
					resultStrList = rangeList.flatMap((e,idx)=>{
						if (idx === 0){
							return [e.node.textContent.substring(frontRelativeIndex)]
						} else if (idx === rangeList.length-1){
							return [e.node.textContent.substring(0,backRelativeIndex)]
						} else {
							return [e.node.textContent]
						}
					})

					//text를 한줄로 더해준다.
					return resultStrList.join('')
				}
			} catch(err){

			} finally{
				allNodes = null
				allNodes2 = null
				sChapterLi = null
				eChapterLi = null
				resultStrList = null
				parentStr = null
				front = null
				frontFind = null
				frontRelativeIndex = null
				back = null
				backFind = null
				backRelativeIndex = null
				rangeList = null
			}
		},
		/**
		 * 샐랙션이 된상태에서 책을 다음장으로 넘길수 있는지 여부 확인
		 * @returns { Promise } front 앞장으로넘김여부 ,back 뒷장으로 넘김여부
		 */
		 checkTurnOverPoint(){
			const isStrEmpty = function(str){ 
				if (typeof str === 'string' ) {
					if (str.trim().length === 0){
						return true
					} else {
						return false
					}
				} else {
					return false
				}
			}

			let strStart = null
			let strEnd = null
			let start = null
			let end = null

			if (Selection.selectionCacheMode >= 0){
				strStart = Selection.selectionCache.start
				strEnd = Selection.selectionCache.end
			} else {
				strStart = Selection.m_strDragStartCfi
				strEnd = Selection.m_strDragEndCfi
			}
			if (isStrEmpty(strStart) || isStrEmpty(strEnd)){
				return Promise.reject('cfi empty');
			}
			start = Position._decodeCFI(document,strStart)
			end = Position._decodeCFI(document,strEnd)

			if (!start||!end){
				return Promise.reject('decode start or end fail')
			}
			return Finder.checkEndPoint('#contents',start.node,start.offset,end.node,end.offset)
		},
		/**
		 * 노드,offset을 cfiAttr 형식으로 Encode
		 * @param {*} node 
		 * @param {*} offset 
		 * @returns 
		 */
		_EncodeCfiAttr(node,offset){
			let chapter = -1
			let indexFromParent = -1
			let cfiAttr = null
			let allnd = null
			let startND = null
			allnd = Position.getTextNodesAllWithObj(node.parentNode)
			startND = null
			for (let i = allnd.length - 1; i >= 0; i--) {
				if (node === allnd[i].node){
					startND = allnd[i];
					break;
				}
			}
			if (startND){
				indexFromParent = startND.start + offset
			} else {
				throw 'not fount startIndexFromParent'
			}
			chapter = Util.findParentChapter(node)
			cfiAttr = node.parentNode.getAttribute('cfi')
			return { chapter ,cfiAttr, indexFromParent }
		},
		/**
		 * node를 CfiAttr로 변환
		 * @param {*} startNode 
		 * @param {*} startOffset 
		 * @param {*} endNode 
		 * @param {*} endOffset 
		 * @returns 
		 */
		EncodeCfiAttr(startNode,startOffset,endNode,endOffset){
			let startChapter = -1
			let startIndexFromParent = -1
			let startCfiAttr = null
			let startSelector = null

			let endChapter = -1
			let endIndexFromParent = -1
			let endCfiAttr = null
			let endSelector = null

			let startAllnodes = null
			let startND = null

			let endAllnodes = null
			let endND = null


			//앞
			startAllnodes = Position.getTextNodesAllWithObj(startNode.parentNode)
			startND = null
			for (let i = startAllnodes.length - 1; i >= 0; i--) {
				if (startNode === startAllnodes[i].node){
					startND = startAllnodes[i];
					break;
				}
			}
			if (startND){
				startIndexFromParent = startND.start + startOffset
			} else {
				throw 'not fount startIndexFromParent'
			}

			//뒤
			endAllnodes = Position.getTextNodesAllWithObj(endNode.parentNode)
			endND = null
			for (let i = 0; i <endAllnodes.length; i++) {
				if (endNode === endAllnodes[i].node){
					endND = endAllnodes[i];
					break;
				}
			}
			if (endND){
				endIndexFromParent = endND.start + endOffset
			} else {
				throw 'not fount endIndexFromParent'
			}

			//chapter find
			startChapter = Util.findParentChapter(startNode)
			endChapter = Util.findParentChapter(endNode)

			//cfi attr
			startCfiAttr = startNode.parentNode.getAttribute('cfi')
			endCfiAttr = endNode.parentNode.getAttribute('cfi')

			return { startChapter ,startCfiAttr, startIndexFromParent, endChapter, endCfiAttr, endIndexFromParent }
		},
		_DecodeCfiAttr(startChapter ,startCfiAttr, startIndexFromParent){
			let startSelector = null
			let startChapterSelector = null
			let startAllnodes = null
			/**
			 * TODO 이쪽 Selector만 바꾸면 수정가능
			 */
			startSelector = '[cfi="'+startCfiAttr+'"]'
			startChapterSelector = '#bdb-chapter'+startChapter
			
			let sEle = document.querySelector(startChapterSelector+' '+startSelector)

			let start = null
			let startOffset = -1;

			/**
			 * 추출 실패시 Element라도 return
			 */
			if (startIndexFromParent === -1){
				return { node: sEle }
			}

			if (sEle){
				startAllnodes = Position.getTextNodesAllWithObj(sEle)
				for (let i=0; i<startAllnodes.length; i++){
					let e = startAllnodes[i]
					if (e.start <= startIndexFromParent && e.end > startIndexFromParent){
						start = e
						break;
					}
				}
				if (!start ){
					return { node: sEle, offset: -1 }
				}
			} else {
				return null
			}
			if (start){
				startOffset =  startIndexFromParent - start.start
			} 
			return { node: start.node, offset: startOffset }
		},
		/**
		 * CfiAttr을 node,offset으로 변환시켜준다.
		 * @param {*} startChapter 
		 * @param {*} startCfiAttr 
		 * @param {*} startIndexFromParent 
		 * @param {*} endChapter 
		 * @param {*} endCfiAttr 
		 * @param {*} endIndexFromParent 
		 */
		DecodeCfiAttr(startChapter ,startCfiAttr, startIndexFromParent, endChapter, endCfiAttr, endIndexFromParent){

			let startSelector = null
			let endSelector = null
			let startChapterSelector = null
			let endChapterSelector = null

			let startAllnodes = null
			let endAllnodes = null
			/**
			 * TODO 이쪽 Selector만 바꾸면 수정가능
			 */
			startSelector = '[cfi="'+startCfiAttr+'"]'
			endSelector = '[cfi="'+endCfiAttr+'"]'
			startChapterSelector = '#bdb-chapter'+startChapter
			endChapterSelector = '#bdb-chapter'+endChapter
			
			let sEle = document.querySelector(startChapterSelector+' '+startSelector)
			let eEle = document.querySelector(endChapterSelector+' '+endSelector)

			let start = null
			let end = null
			let startOffset = -1;
			let endOffset = -1;

			/**
			 * 추출 실패시 Element라도 return
			 */
			if (startIndexFromParent === -1 && endIndexFromParent === -1){
				return {
					start: {
						node: sEle
					},
					end: {
						node: eEle
					}
				}
			}

			if (sEle && eEle){
				startAllnodes = Position.getTextNodesAllWithObj(sEle)
				for (let i=0; i<startAllnodes.length; i++){
					let e = startAllnodes[i]
					if (e.start <= startIndexFromParent && e.end > startIndexFromParent){
						start = e
						break;
					}
				}
				endAllnodes = Position.getTextNodesAllWithObj(eEle)
				for (let i= endAllnodes.length - 1; i >= 0; i--){
					let e = endAllnodes[i]
					if (e.start < endIndexFromParent && e.end >= endIndexFromParent){
						end = e
						break;
					}
				}
				if (!start || !end){
					return {
						start: {
							node: sEle,
							offset: -1
						},
						end: {
							node: eEle,
							offset: -1
						}
					}
				}
			} else if (sEle){
				startAllnodes = Position.getTextNodesAllWithObj(sEle)
				for (let i=0; i<startAllnodes.length; i++){
					let e = startAllnodes[i]
					if (e.start <= startIndexFromParent && e.end > startIndexFromParent){
						start = e
						break;
					}
				}
			} else if (eEle){
				endAllnodes = Position.getTextNodesAllWithObj(eEle)
				for (let i= endAllnodes.length - 1; i >= 0; i--){
					let e = endAllnodes[i]
					if (e.start < endIndexFromParent && e.end >= endIndexFromParent){
						end = e
						break;
					}
				}
			} else {
				return null
			}

			if (start && end){
				startOffset =  startIndexFromParent - start.start
				endOffset =  endIndexFromParent - end.start
			} else if (start){
				startOffset =  startIndexFromParent - start.start
			} else if (end){
				endOffset =  endIndexFromParent - end.start
			} 


			return {
				start: {
					node: start.node,
					offset: startOffset
				},
				end: {
					node: end.node,
					offset: endOffset
				}
			}
		},
		/**
		 * text 추출
		 * @param {*} startNode 
		 * @param {*} startOffset 
		 * @param {*} endNode 
		 * @param {*} endOffset 
		 * @returns 
		 */
		getTextByNodeOffset(startNode,startOffset,endNode,endOffset){
			let range = document.createRange()
			range.setStart(startNode,startOffset)
			range.setEnd(endNode,endOffset)
			let d = range.cloneContents();
			let l = Position.getTextNodesAll(d)
			let t = l.map(e=>e.textContent).join('')
			return t
		},
		/**
		 * element에서 범위 사이에 있는 textNode의 offset을 반환
		 * @param {*} element 
		 * @param {*} startoffset 
		 * @param {*} endoffset 
		 */
		 ElementRectByOffset: function(element,startoffset,endoffset){
			let list = Position.getTextNodesAllWithObj(element)
			let isStart = false
			let temp = []
			for (let item of list){
				if (item.start <= startoffset && item.end > startoffset ){
					isStart = true
				}
				if (isStart){
					temp.push(item)
					if (item.start <= endoffset && item.end > endoffset){
						break;
					}
				}
			}
			let first = temp[0]
			let end = temp[temp.length - 1]

			console.log(first,end)
			let _startNode = first.node
			let _startoffset = startoffset - first.start
			let _endNode = end.node
			let _endoffset = endoffset - end.start
			if (end.node.textContent.length <= _endoffset){
				_endoffset = end.node.textContent.length
			}

			let d = document.createRange()
			d.setStart(_startNode,_startoffset)
			d.setEnd(_endNode,_endoffset)
			let rects = d.getClientRects()
			return rects
		},
		/**
		 * 엘리먼트내에 string 값이 존재하는지 확인
		 * @param { Element } element 
		 * @param { String } keyword 
		 * @returns 
		 */
		ElementOffsetByKeyword: function(element,keyword){
			let text = Walker.getWalkerList(element).map(e=>e.textContent).join('')
			let start = text.indexOf(keyword)
			if (start === -1){
				return null
			}
			let end = start + keyword.length
			return { start , end }
		},
	};

	/**
	 * annotation object key
	 */
	 const KEY_ANNOTATION_ID = 'id';
	 const KEY_ANNOTATION_SERVERID = 'serverId';
	 const KEY_ANNOTATION_CHAPTER = 'chapter';
	 const KEY_ANNOTATION_CHAPTERNAME = 'chapterName';

	 const KEY_ANNOTATION_START_CFI_CHAPTER = 'startCfiChapter';
	 const KEY_ANNOTATION_STARTCFIATTR = 'startCfiAttr';
	 const KEY_ANNOTATION_STARTINDEXFROMPARENT = 'startIndexFromParent';
	 const KEY_ANNOTATION_START_CFI = 'startCfi';

	 const KEY_ANNOTATION_END_CFI_CHAPTER = 'endCfiChapter';
	 const KEY_ANNOTATION_ENDCFIATTR = 'endCfiAttr';
	 const KEY_ANNOTATION_ENDINDEXFROMPARENT = 'endIndexFromParent';
	 const KEY_ANNOTATION_END_CFI = 'endCfi';

	 const KEY_ANNOTATION_SELECTOR = 'selector';
	 const KEY_ANNOTATION_DATE = 'date';
	 const KEY_ANNOTATION_ANNOTATIONTYPE = 'annotationType';
	 const KEY_ANNOTATION_TEXT = 'text';
	 const KEY_ANNOTATION_DATA = 'data';
	 const KEY_ANNOTATION_COLOR = 'color';
	 const KEY_ANNOTATION_COLORKEY = 'colorKey';
	 const KEY_ANNOTATION_COLORTYPE = 'colorType';
	 const KEY_ANNOTATION_MEMO = 'memo';
	 const KEY_ANNOTATION_PERCENT = 'percent';
	 const KEY_ANNOTATION_STATUS = 'status';
	 const KEY_ANNOTATION_MEDIAPATH = 'mediapath';

	 const KEY_ANNOTATION_STATUS_SYNC 	= 'S';
	 const KEY_ANNOTATION_STATUS_CREATE = 'C';
	 const KEY_ANNOTATION_STATUS_UPDATE = 'U';
	 const KEY_ANNOTATION_STATUS_DELETE = 'D';

	 //only use in cache
	 const KEY_ANNOTATION_DELETE_LIST = 'deleteList';
	/**
	 * sjhan 20220415
	 * 메모 하이라이트
	 */
	const Annotation = Core.Annotation = {
		list: [ ],
		colorList: [
			{ name:'lemon' ,type:0 ,color:'#fbedb8',opacity:0.5 },
			{ name:'palegreen' ,type:0 ,color:'#e1efb8',opacity:0.5 },
			{ name:'lavendar' ,type:0 ,color:'#cec0e9',opacity:0.5 },
			{ name:'skyblue' ,type:0 ,color:'#b1e7f9',opacity:0.5 },
			{ name:'palepink' ,type:0 ,color:'#fbbfbe',opacity:0.5 },
			{ name:'underline' ,type:1 ,color:'#ea3323',opacity:1 },
		],
		memoImg:null,
		_memoImg:`<svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewBox="0 0 10 11">
					<path data-name="빼기 29" d="M2.7 11a2.676 2.676 0 0 1-1.978-.753 2.793 2.793 0 0 1-.725-2v-5.5A2.8 2.8 0 0 1 .719.76 2.667 2.667 0 0 1 2.681 0h4.614a2.682 2.682 0 0 1 1.979.752 2.789 2.789 0 0 1 .726 2v5.5a2.792 2.792 0 0 1-.72 1.991A2.673 2.673 0 0 1 7.317 11zm.082-3.5a.449.449 0 1 0 0 .9h5a.449.449 0 1 0 0-.9zm0-2.444a.448.448 0 1 0 0 .9h5a.448.448 0 1 0 0-.9zm0-2.446a.449.449 0 1 0 0 .9h1.879a.449.449 0 1 0 0-.9z" style="fill:#5e6b9f"/>
				</svg>`,
		currentMemoColor: null,
		currentHighlightColor: null,
		eleDrawLayer: null,
		init: function() {
			Annotation.currentMemoColor = this.colorList[0]
			Annotation.currentHighlightColor = this.colorList[0]
			window.addEventListener("contextmenu", e => e.preventDefault());
			Annotation.eleDrawLayer = document.getElementById('viewer_annotation_layer')
			Annotation.buildMemoImg()
		},
		buildMemoImg: function(){
			Annotation.memoImg = document.createElement('div')
			Annotation.memoImg.style.position = 'absolute'
			Annotation.memoImg.style.width = 11+'px'
			Annotation.memoImg.style.height = 11+'px'
			Annotation.memoImg.style.pointerEvents = 'none'
			Annotation.memoImg.innerHTML = Annotation._memoImg
		},
		getSample: function(){
			return `[{"id":"ed662534-a19a-4757-9c57-86aeea4ad9a2","chapter":37,"startCfiAttr":"/2/4/2/4/2/2/2/76/110/12/1:1","startIndexFromParent":-1,"endCfiAttr":null,"endIndexFromParent":-1,"selector":"p:eq(20)>span","date":"2022-05-30 08:27:01","timestamp":"2022-05-29T23:27:01.528Z","timestampMillisecond":1653866821528,"type":0,"text":"(Lambda School)은 소프트웨어 개발자를 양성하고 졸업생들이 연봉 5만 달러 이상을 받을 수 있는 일자리를 찾을 때까지 교육비","color":"red","colorKey":null,"memo":null,"percent":95.2333333333334},{"id":"dccca719-18c3-4e46-b517-69e35c9b234e","chapter":37,"startCfiAttr":"/60/1","startIndexFromParent":46,"endCfiAttr":"/60/1","endIndexFromParent":183,"selector":null,"date":"2022-05-30 08:26:56","timestamp":"2022-05-29T23:26:56.694Z","timestampMillisecond":1653866816694,"type":1,"text":"사고가 필요하다. 반가운 소식은 진전의 조짐이 보인다는 것이다. 많은 대기업이 미래를 위해 직원 재교육에 투자하고 있다. 컨설팅 업체인 PWC는 2020년 직원 27만 5,000명을 재교육하는 데 30억 달러를 투자할 것이라고 발표했다. 컴퓨터 ","color":"#fbedb8","colorKey":"lemon","memo":null,"percent":94.90000000000006},{"id":"bd3b2df2-839d-453d-950e-ee2eeda95af3","chapter":35,"startCfiAttr":"/4/4/140/1","startIndexFromParent":113,"endCfiAttr":"/4/4/142/1","endIndexFromParent":238,"selector":null,"date":"2022-05-30 08:26:46","timestamp":"2022-05-29T23:26:46.853Z","timestampMillisecond":1653866806853,"type":2,"text":"빠르게 회복될 것이다.\\n\\n      이전의 가격 수준을 찾진 못하겠지만, 수요가 어느 정도 회복되면서 재고가 소비되고 EIU의 산업용금속지수가 5% 상승할 것이다. 특히 중국에서 기반시설에 대규모로 투자하며 구리 등 산업용 금속 공급자들이 한숨 돌릴 것이다. 중국의 경제 회복은 확실히 많은 광산에 전환점이 될 것이다. 생산자들은 또한 미중 무역 분쟁이 자동차 제조사에 타격을 입히지 않길 간절히 바라는데, 전기차 배터리 제조를 위한 광물 수요 상승이 중요하기 ","color":"#b1e7f9","colorKey":"skyblue","memo":"g43g","percent":90.73636363636373},{"id":"216337a6-ea4f-4148-897b-28c2329db6eb","chapter":21,"startCfiAttr":"/146/1","startIndexFromParent":46,"endCfiAttr":"/146/1","endIndexFromParent":111,"selector":null,"date":"2022-05-30 08:26:37","timestamp":"2022-05-29T23:26:37.167Z","timestampMillisecond":1653866797167,"type":2,"text":"으로 자신의 이념을 세웠다. 그는 제2차 세계대전에서 소련의 승전을 기념하는 군사 퍼레이드를 자신의 공식 행진으로 만","color":"#ea3323","colorKey":"underline","memo":"sadad","percent":52.97368421052628},{"id":"a9c7d87d-35a8-4ed5-9e99-2763047aa8bc","chapter":21,"startCfiAttr":"/150/3","startIndexFromParent":3,"endCfiAttr":"/150/3","endIndexFromParent":249,"selector":null,"date":"2022-05-30 08:26:26","timestamp":"2022-05-29T23:26:26.231Z","timestampMillisecond":1653866786231,"type":2,"text":"대통령이 법원과 보안 기관, 선거관리위원회를 장악하고 있기는 하지만,  2021년 러시아 총선은 전쟁터가 되기 십상이다. 푸틴 대통령의 권력 행사 수단인 통합러시아당(United Russia party)에 반대하는 항의표를 결집하려는 나발니의 전술은 크렘린궁의 전지전능한 위상을 무너뜨릴 수도 있다. 더욱 중요한 것은 러시아를 현대 국가로 보는 나발니의 비전이 푸틴 대통령의 구소련식 제국주의적 민족주의보다 더 매력적이라는 사실을 증명할 수","color":"#fbbfbe","colorKey":"palepink","memo":"","percent":52.97368421052628},{"id":"5dc3555d-1335-40b0-9bc2-44bd8c981f4c","chapter":6,"startCfiAttr":"/2/4/2/4/2/2/2/14/154/13:37","startIndexFromParent":-1,"endCfiAttr":null,"endIndexFromParent":-1,"selector":"p:eq(29)","date":"2022-05-30 08:26:21","timestamp":"2022-05-29T23:26:21.921Z","timestampMillisecond":1653866781921,"type":0,"text":"전에는 재택근무를 하는 직원들이 사무실에 출근하는 직원에 비해 소외되는 경향이 있었다. 본인들도 스스로 열등감을 느꼈다. 그러나 코로나","color":"red","colorKey":null,"memo":null,"percent":13.849999999999993},{"id":"55303659-e964-4fbb-8c4c-59538bd5e6cc","chapter":2,"startCfiAttr":"/12/2/3","startIndexFromParent":64,"endCfiAttr":"/12/6/3","endIndexFromParent":147,"selector":null,"date":"2022-05-30 08:26:14","timestamp":"2022-05-29T23:26:14.978Z","timestampMillisecond":1653866774978,"type":1,"text":"의 배포로 초점이 바뀔 것이다.백신 외교가 중요해지고누가, 언제 백신을 얻을지에 대해 각국 내부적으로, 또 세계적으로 싸움이 일어날 것이다.변수:백신의 제공을 거부하는 사람은 얼마나 될까?\\n\\n    \\n\\n    2.불균일한 경기 회복. 국지적 발발과 락다운이 반복되면서각국 경제가 팬데믹의 여파를 벗어나는 회복세는 고르지 못할 것이다.기업에 인공호흡기를 달던 각국 정부는 정책 방향을 바꿔 일자리를 잃은 근로자들을 도울 듯하다.강한 기업과 약한 기업의 격차는 더욱 벌어진다.","color":"#fbedb8","colorKey":"lemon","memo":null,"percent":0.3}]`
		},
		setJsonList: function(str){
			this.list = JSON.parse(str)
		},
		getJsonList:function(){
			return JSON.stringify(Annotation.list)
		},
		/**
		 * 
		 * @param {*} color 
		 * @param {*} type 0:북마크 ,1:하이라이트 2:메모
		 * @param {*} memo 
		 * @returns 
		 */
		 create( type, memo , chapterName, percent){
			return 	new Promise(function(resolve,reject){
				let mColor = '#ec1f2d'
				let mColorKey = 'red'
				let mColorType = 0
				let mPercent = 0
				let scfi = null;
				let ecfi = null;
				if (typeof percent === 'number'){
					mPercent = percent
				}
				if (type === 0){
					Annotation.getBookmarkInScreen(chapterName, percent)
						.then(bookmark=>{
							resolve(bookmark)
						}).catch(err=>{
							reject(err)
						})
					return;
				} else if (type === 1){
					mColor = Annotation.currentHighlightColor.color
					mColorKey = Annotation.currentHighlightColor.name
					mColorType = Annotation.currentHighlightColor.type
				} else if (type === 2){
					mColor = Annotation.currentMemoColor.color
					mColorKey = Annotation.currentMemoColor.name
					mColorType = Annotation.currentHighlightColor.type
				}
				let mMemo = null
				if (memo){
					mMemo = memo
				}


				/**
				 * 병합 체크.
				 */
				let checkMerge = Annotation.checkMerge()
				let existFront = false
				let existBack = false
				if (checkMerge){
					//삭제대상존재
					if (typeof checkMerge.deleteList === 'object' && checkMerge.deleteList.length>0){

					}
					//제일 앞쪽이 selection
					if (typeof checkMerge.smallest === 'number' && checkMerge.smallest === -1){

					} 
					//제일 앞쪽이 기존Annotation내 존재
					else if (typeof checkMerge.smallest === 'object'){
						existFront = true
					}

					//제일 뒤쪽이 selection
					if (typeof checkMerge.biggest === 'number' && checkMerge.biggest === -1){

					} 
					//제일 뒤쪽이 기존Annotation내 존재
					else if (typeof checkMerge.biggest === 'object'){
						existBack = true
					}
				}

				// console.log('##checkMerge ===>',checkMerge)
				let cfiGroup = Position.extractCfi()
				// console.log('##cfiGroup ===>',cfiGroup)
				if (!cfiGroup){
					reject(-1,'cfiGroup null')
					return;
				}
				let date = null
				let obj = new Object()
				let startCfiChapter = -1
				let startCfiAttr = null
				let startIndexFromParent = null
				let endCfiChapter = -1
				let endCfiAttr = null
				let endIndexFromParent = null
				let mText = null
				let reScanStr = null
				if (existFront){
					startCfiChapter = checkMerge.smallest[KEY_ANNOTATION_START_CFI_CHAPTER]
					startCfiAttr = checkMerge.smallest[KEY_ANNOTATION_STARTCFIATTR]
					startIndexFromParent = checkMerge.smallest[KEY_ANNOTATION_STARTINDEXFROMPARENT]
				} else {
					startCfiChapter = cfiGroup.startChapter
					startCfiAttr = cfiGroup.startCfi.cfiAttr
					startIndexFromParent = cfiGroup.startIndexFromParent
				}
				if (existBack){
					endCfiChapter = checkMerge.biggest[KEY_ANNOTATION_END_CFI_CHAPTER]
					endCfiAttr = checkMerge.biggest[KEY_ANNOTATION_ENDCFIATTR]
					endIndexFromParent = checkMerge.biggest[KEY_ANNOTATION_ENDINDEXFROMPARENT]
				} else {
					endCfiChapter = cfiGroup.endChapter
					endCfiAttr = cfiGroup.endCfi.cfiAttr
					endIndexFromParent = cfiGroup.endIndexFromParent
				}

				reScanStr = Position.attrCfiToString(startCfiChapter,startCfiAttr,startIndexFromParent,endCfiChapter,endCfiAttr,endIndexFromParent)

				scfi = cfiGroup.scif;
				ecfi = cfiGroup.ecif;
				//rescan text
				if (existFront || existBack){
					if (reScanStr){
						mText = reScanStr
					} else {
						mText = cfiGroup.text	
					}
				} else {
					mText = cfiGroup.text
				}
				
				let decode = Position.DecodeCfiAttr(startCfiChapter,startCfiAttr,startIndexFromParent,endCfiChapter,endCfiAttr,endIndexFromParent)
				let txt = Position.getTextByNodeOffset(decode.start.node,decode.start.offset,decode.end.node,decode.end.offset)
				if (txt !== mText){
					mText = txt
				}

				date = new Date()
				try {
					obj[KEY_ANNOTATION_ID] 						= new Date().valueOf()
					obj[KEY_ANNOTATION_SERVERID]				= null
					obj[KEY_ANNOTATION_CHAPTER]					= cfiGroup.chapter
					obj[KEY_ANNOTATION_CHAPTERNAME]				= chapterName
					obj[KEY_ANNOTATION_START_CFI_CHAPTER]		= startCfiChapter;
					obj[KEY_ANNOTATION_STARTCFIATTR]			= startCfiAttr
					obj[KEY_ANNOTATION_STARTINDEXFROMPARENT]   	= startIndexFromParent
					obj[KEY_ANNOTATION_START_CFI]   			= scfi
					obj[KEY_ANNOTATION_END_CFI_CHAPTER]			= endCfiChapter;
					obj[KEY_ANNOTATION_ENDCFIATTR]				= endCfiAttr
					obj[KEY_ANNOTATION_ENDINDEXFROMPARENT]		= endIndexFromParent
					obj[KEY_ANNOTATION_END_CFI]   				= ecfi
					obj[KEY_ANNOTATION_SELECTOR]				= null
					obj[KEY_ANNOTATION_DATE]					= date.current()
					obj[KEY_ANNOTATION_ANNOTATIONTYPE]			= type
					obj[KEY_ANNOTATION_TEXT]					= mText
					obj[KEY_ANNOTATION_COLOR]					= mColor
					obj[KEY_ANNOTATION_COLORKEY]				= mColorKey
					obj[KEY_ANNOTATION_COLORTYPE]				= mColorType
					obj[KEY_ANNOTATION_MEMO]					= mMemo
					obj[KEY_ANNOTATION_PERCENT]					= mPercent
					obj[KEY_ANNOTATION_STATUS] 					= KEY_ANNOTATION_STATUS_CREATE
					if (checkMerge){
						obj[KEY_ANNOTATION_DELETE_LIST] 		= checkMerge.deleteList
						if (checkMerge.memo){
							obj[KEY_ANNOTATION_MEMO] = checkMerge.memo
						}
					}
					resolve(obj)
				} catch(error){
					reject(-2,error)
				} finally {
					cfiGroup = null
					date = null
					obj = null
					startCfiAttr = null
					startIndexFromParent = null
					endCfiAttr = null
					endIndexFromParent = null
					reScanStr = null
				}
			});
		},
		createWithUpdate: function( type, memo ,chapterName,percent){
			return Annotation.create(type, memo ,chapterName,percent)
				.then(obj=>Annotation.update(obj))
				.then(list=>{
					return new Promise( function(resolve,reject){
						try{
							let check = Annotation.drawingWithCheck()
							resolve(check)
						} catch(error){
							reject(error)
						}
					})
				})
		},
		/**
		 * data update
		 * @param {*} obj 
		 * @returns 
		 */
		update(obj){
		
			return new Promise(function(resolve,recject){
				try{
					let find = Annotation.list.findIndex(e=>e.id === obj.id)
					if (find >= 0){
						find = obj
						// let date = new Date()
						// Annotation.list[find]['chapter'] = obj['chapter']
						// Annotation.list[find]['chapterName'] = obj['chapterName']
						// Annotation.list[find]['startCfiAttr'] = obj['startCfiAttr']
						// Annotation.list[find]['startIndexFromParent'] = obj['startIndexFromParent']

						// Annotation.list[find]['endCfiAttr'] = obj['endCfiAttr']
						// Annotation.list[find]['endIndexFromParent'] = obj['endIndexFromParent']

						// Annotation.list[find]['date'] = date.current()
						// Annotation.list[find]['timestamp'] = date
						// Annotation.list[find]['timestampMillisecond'] = date.getTime()
						// Annotation.list[find]['type'] = obj['type']
						// Annotation.list[find]['annotationType'] = obj['annotationType']
						// Annotation.list[find]['text'] = obj['text']
						// Annotation.list[find]['color'] = obj['color']
						// Annotation.list[find]['colorKey'] = obj['colorKey']
						// Annotation.list[find]['memo'] = obj['memo']
						// Annotation.list[find]['percent'] = obj['percent']
					} else {
						Annotation.list.push(obj)
						Annotation.drawingWithCheck()
					}
					resolve(Annotation.list)
				} catch(error){
					recject(error)
				}
			})

		},
		/**
		 * 색을 바꿔 obj return
		 * @param {*} obj 
		 * @param {*} colorKey 
		 * @returns 
		 */
		changeColor(obj,colorKey){
			let colorset = Annotation.colorList.find(e=>e.name === colorKey)
			obj.colorKey = colorset.name
			obj.color = colorset.color
			return obj
		},
		/**
		 * update Color
		 * @param {*} obj 기존 object
		 * @param {*} colorKey 색깔키
		 * @returns 
		 */
		updateColor(obj,colorKey){
			let colorset = Annotation.colorList.find(e=>e.name === colorKey)
			obj.colorKey = colorset.name
			obj.color = colorset.color
			return Annotation.update(obj)
		},
		/**
		 * delete
		 * @param {*} id 
		 */
		delete(id){
			return new Promise(function(resolve,reject){
				try{
					let find = Annotation.list.findIndex(e=> e.id === id);
					if (find >=0){
						Annotation.list.splice(find, 1)
					} else {
						reject(-1,'not found')
					}
				} catch(error){
					reject(-2,error)
				}
				resolve(Annotation.list)
			})
		},
		/**
		 * 현재 화면에 annotation이 존재하는지 체크
		 */
		 drawingWithCheck(){
			if (!Annotation.eleDrawLayer){
				return;
			}
			let parents = Annotation.eleDrawLayer
			parents.clearChildren()
			parents = null
			if (!Annotation.list){
				return false
			}
			if (Annotation.list.length === 0){
				return false
			}
			let _encode = null
			let rects = null
			let padding = null
			let isShow = false
			let chapter = ePubEngineLoaderJS.Data.getCurrentSpine()
			let color = null
			let rect_next = null
			let dual_idx = null
			let isDual = false
			let mList = null
			for(let item of Annotation.list){
				// console.log(item)
				if (item.annotationType === 0 || item.annotationType === undefined || item.annotationType === null){
					continue;
				}
				// if (item.chapter === chapter){
				_encode = Annotation.encode(item.chapter,
					 item.startCfiAttr, item.startIndexFromParent,
					 item.endCfiAttr, item.endIndexFromParent,
					 item.startCfiChapter,item.endCfiChapter)
				if (_encode){
					rects = Annotation.rectsFromEncode(_encode)
					_encode = null
					padding = Selection.viewerPadding
					if (rects){
						mList = []
						for (let k=0; k<rects.length; k++){
							let rect = rects[k];
							//210803 - sjhan 공백 체크 로직 추가.
							var xx = rect.left+(rect.width/2);
							var yy = rect.top+(rect.height/2);
							var selnode = document.elementFromPoint(xx,yy);
							if (selnode){
								if (selnode.textContent){
									if (selnode.textContent.replace(/\s+/g,'').length <= 0){
										continue;
									}
								}
							}

							//210917 뷰어 영역 밖으로 샐랙션을 막기위함.
							if (Selection.viewerPadding){
								//현재 뷰어의 rect를 가저와 넘어가버리면 result에 추가하지 않는다.
								var viewerW = _viewer_width + Selection.viewerPadding.left;
								var viewerH = _viewer_height + Selection.viewerPadding.top;
								var lLeft = rect.left;

								if ( Selection.viewerPadding.left > lLeft || viewerW <= lLeft|| 
									Selection.viewerPadding.top > rect.top || viewerH < rect.top ){
									viewerW=null;
									viewerH=null;
									lLeft = null;
									continue;
								}
								viewerW=null;
								viewerH=null;
								lLeft = null;
							}

							if (true) {
								isDual = false;
								for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
									// across check
									rect_next = rects[dual_idx];
									if (rect_next) {
										// across check
										if (rect.top >= rect_next.top
											&& rect.bottom <= rect_next.bottom
											&& rect.left <= rect_next.left
											&& rect.right >= rect_next.right && rect_next.width != 0) {
											isDual = true;
											break;
										}
										else if (rect.top <= rect_next.top
													&& rect.bottom >= rect_next.bottom
													&& rect.left >= rect_next.left
													&& rect.right <= rect_next.right && rect_next.width != 0) {
											isDual = true;
											break;
										}
										else if (rect.top <= rect_next.top
													&& rect.bottom >= rect_next.bottom
													&& rect.left <= rect_next.left
													&& rect.right >= rect_next.right && rect_next.width != 0) {
											isDual = true;
											break;
										}
									}
								}
								if (isDual) {
									continue;
								}
							}
							mList.push(rect)
						}
						Annotation.highlightWithRects(mList, item , 0.4);
					}
				}
				// }
			}
			_encode = null
			rects = null
			padding = null
			chapter = null
			color = null
			rect_next = null
			dual_idx = null
			isDual = false
			mList = null
			return isShow
		},

		/**
		 * 현재 화면에 annotation이 존재하는지 체크
		 */
		 has(){
			if (!Annotation.list){
				return false
			}
			if (Annotation.list.length === 0){
				return false
			}
			let _encode = null
			let rects = null
			let padding = null
			let isShow = false
			let chapter = ePubEngineLoaderJS.Data.getCurrentSpine()
			for(let item of Annotation.list){
				if (item.chapter === chapter){
					_encode = Annotation.encode(item.chapter, item.startCfiAttr, item.startIndexFromParent, item.endCfiAttr, item.endIndexFromParent)
					if (_encode){
						rects = Annotation.rectsFromEncode(_encode)
						padding = Selection.viewerPadding
						if (rects){
							for (let rect of rects){
								if (rect){
									if (ePubEngineCoreJS.Layout.getScrollMode()){
										if (rect.top > padding.top && padding.top+padding.height > rect.bottom  ){
											isShow = true
										}
									} else {
										if (rect.left > padding.left && padding.left+padding.width > rect.left+rect.width  ){
											isShow = true
										}
									}
								}
							}
						}
					}
				}
			}

			return isShow
		},
		/**
		 * rects 정보를 받아 하이라이트 해준다.
		 * @param {*} rects 
		 */
		 highlightWithRects: function(rects , item , opacity){
			if (!Annotation.eleDrawLayer){
				return;
			}
			if (rects.length === 0 ){
				return;
			}

			let wrap = null
			let result = null
			let mimg = null
			try {
				//element
				wrap = document.createElement('div')
				result = Annotation.createSelectionDiv(rects , item ,opacity)
				wrap.appendChild(result)
				//memo icon
				if (item.memo && item.memo.length>0){
					mimg = Annotation.memoImg.cloneNode(true)
					if (wrap.lastElementChild){
						mimg.style.top = parseFloat(wrap.lastElementChild.style.top) - (parseFloat(mimg.style.height)/2) +'px'
						mimg.style.left = parseFloat(wrap.lastElementChild.style.left)+parseFloat(wrap.lastElementChild.style.width)+'px'
					}
					wrap.appendChild(mimg)
				}
				Annotation.eleDrawLayer.appendChild(wrap)
			} catch(error){

			} finally {
				wrap = null
				result = null
				mimg = null
			}
		},
		/**
		 * selection 블럭 element 생성
		 * @param {*} rects 
		 * @param {*} item 
		 * @returns 
		 */
		 createSelectionDiv:function(rects , item ,opacity){
			let mColor = "#38a5e6"
			let drawType = -1
			let _opacity = 0.4
			//color
			if (item){
				mColor = item['color']
			}
			// block,underline
			drawType = Annotation.colorList.find(e=>e.color == mColor).type
			// opacity
			if (typeof opacity === 'number'){
				_opacity = opacity
			}
			let docf = document.createDocumentFragment()
			for (let e of rects){
	
				let p2 = document.createElement('div')
				p2.setAttribute('bdb-anno-type',item.annotationType)
				p2.setAttribute('bdb-anno-id',item.id)
				p2.style.backgroundColor = mColor
				p2.style.pointerEvents = 'none'
				p2.style.position = 'absolute'
				p2.style.opacity = opacity

				//position n size
				let mTop = 0
				let mLeft = 0
				let mWidth = 0
				let mHeight = 0

				mLeft = e.left 
				mWidth = e.width
				if (drawType === 0){
					mHeight = e.height
					mTop = e.top
				} else if (drawType === 1){
					mHeight = 3
					mTop = e.top+e.height
				}

				p2.style.top = mTop + 'px'
				p2.style.left = mLeft + 'px'
				p2.style.width = mWidth + 'px'
				p2.style.height = mHeight + 'px'
				docf.appendChild(p2)

				p2 = null
				mTop = null
				mLeft = null
				mWidth = null
				mHeight = null
			}

			mColor = null
			drawType = null
			_opacity = null
			return docf
		},
		/**
		 * annotation 정보를 rects로 반환
		 * @param {*} encode 
		 * @returns 
		 */
		rectsFromEncode( encode ){
			let range = document.createRange()
			range.setStart(encode.start.node, Math.abs( encode.start.offset ))
			range.setEnd(encode.end.node , Math.abs(encode.end.offset))
			return range.getClientRects()
		},
		/**
		 * annotation 정보를 text node와 offset으로 해석
		 * @param {*} spine 파일번호
		 * @param {*} startCfiAttr 시작 엘리먼트의 cfi
		 * @param {*} startIndexFromParent 시작 엘리먼트로 부터 몇번째 string index
		 * @param {*} endCfiAttr 끝 엘리먼트의 cfi
		 * @param {*} endIndexFromParent 끝 엘리먼트로 부터 몇번째 string index
		 * @returns { Object }
		 */
		 encode (spine, startCfiAttr, startIndexFromParent, endCfiAttr, endIndexFromParent ,startCfiChapter, endCfiChapter){
			// console.log('##encode',spine, startCfiAttr, startIndexFromParent, endCfiAttr, endIndexFromParent)
			let mSpine = document.getElementById('bdb-chapter'+spine)
			let mSpineAttr = null
			if (!mSpine){
				return null
			}
			mSpineAttr = mSpine.getAttribute("loaded")
			if (mSpineAttr != "1"){
				return null
			}
			let currentLi = null
			let currentList = null

			let startEle = null
			let endtEle = null

			let _first1 = null
			let _last1 = null
			let _firstIndex1 = null
			let __lastIndex1 = null


			let parentTagNodes1 = null
			let parentTagNodes2 = null
			
			//시작 index와 끝 index 사이에 모든 node 추출
			let gethering = null
			let endGethering = null
			let nodeGroup1 = null
			
			try {
				if (typeof startCfiChapter === 'number' && typeof endCfiChapter === 'number'){
					currentLi = ele_view_wrapper.get(0).querySelector('#bdb-chapter'+startCfiChapter)
					startEle = currentLi.querySelector('[cfi=\"'+startCfiAttr+'\"]')
					currentLi = ele_view_wrapper.get(0).querySelector('#bdb-chapter'+endCfiChapter)
					endtEle = currentLi.querySelector('[cfi=\"'+endCfiAttr+'\"]')
				} else {
					currentLi = ele_view_wrapper.get(0).querySelector('#bdb-chapter'+spine)
					startEle = currentLi.querySelector('[cfi=\"'+startCfiAttr+'\"]')
					endtEle = currentLi.querySelector('[cfi=\"'+endCfiAttr+'\"]')
				}
				/**
				 * element를 찾지못한경우 스킵
				 */
				 if (!startEle || !endtEle){
					return null
				}
	
				//같은 tag 블록내에 존재함.
				if (startEle === endtEle){
					//시작 index와 끝 index 사이에 모든 node 추출
					parentTagNodes1 = Position.getTextNodesAllWithObj(startEle)
					gethering = false
					endGethering = false
					nodeGroup1 = []
					for (let e of parentTagNodes1){
						if (e.start <= startIndexFromParent && e.end >= startIndexFromParent){
							gethering = true
						}
						if (e.start < endIndexFromParent && e.end >= endIndexFromParent){
							endGethering = true
						}
	
						if (gethering){
							nodeGroup1.push(e)
							if (endGethering){
								break
							}
						}
					}
	
					//추출한 노드들중 처음과 마지막의 node, offset 구함.
					if (nodeGroup1.length  > 0){
						_first1 = nodeGroup1[0]
						_last1 = nodeGroup1[nodeGroup1.length-1]
						_firstIndex1 = startIndexFromParent - _first1.start
						__lastIndex1 = endIndexFromParent - _last1.start
	
						return { 
							'start':{
								'node': _first1.node,
								'offset': _firstIndex1,
							},'end':{
								'node': _last1.node,
								'offset': __lastIndex1,
							} 
						}
					}
				} 
				//다른 tag 블록내에 존재함.
				else {
					parentTagNodes1 = Position.getTextNodesAllWithObj(startEle)
					parentTagNodes2 = Position.getTextNodesAllWithObj(endtEle)
					
					//시작 index와 끝 index 사이에 모든 node 추출
					gethering = false
					endGethering = false
					nodeGroup1 = []
	
					for (let e of parentTagNodes1){
						if (e.start <= startIndexFromParent && e.end >= startIndexFromParent){
							gethering = true
						}
						if (gethering){
							nodeGroup1.push(e)
						}
					}
	
					for (let e of parentTagNodes2){
						nodeGroup1.push(e)
						if (e.start < endIndexFromParent && e.end >= endIndexFromParent){
							endGethering = true
						}
						if (endGethering){
							break;
						}
					}
	
					// console.log('##[다름1] ' , nodeGroup1 , startIndexFromParent , endIndexFromParent)
	
					if (nodeGroup1.length  > 0){
						_first1 = nodeGroup1[0]
						_last1 = nodeGroup1[nodeGroup1.length-1]
						_firstIndex1 = startIndexFromParent - _first1.start
						__lastIndex1 = endIndexFromParent - _last1.start
	
						return { 
							'start':{
								'node': _first1.node,
								'offset': _firstIndex1,
							},'end':{
								'node': _last1.node,
								'offset': __lastIndex1,
							} 
						}
					}
				}
				return null
			} catch(error){
				console.error('',error)
			} 
			finally {
				currentLi = null
				startEle = null
				endtEle = null
				_first1 = null
				_last1 = null
				_firstIndex1 = null
				__lastIndex1 = null
				parentTagNodes1 = null
				parentTagNodes2 = null
				gethering = null
				endGethering = null
				nodeGroup1 = null
				
			}
		},
		/**
		 * 에노테이션 레이어가 존재하는지 체크
		 * @param {*} x 
		 * @param {*} y 
		 * @returns 
		 */
		 pointInRects: function(x,y){
			let isEnd = false
			let axis = null
			let rects = null
			let _encode = null
			for(let item of Annotation.list){
				if (item.annotationType === 0){
					continue;
				}
				_encode = Annotation.encode(
					item.chapter, 
					item.startCfiAttr, 
					item.startIndexFromParent, 
					item.endCfiAttr, 
					item.endIndexFromParent,
					item[KEY_ANNOTATION_START_CFI_CHAPTER],
					item[KEY_ANNOTATION_END_CFI_CHAPTER],
					)
				// console.log(_encode)
				if (_encode){
					rects = Annotation.rectsFromEncode(_encode)
					if (rects){
						for (let rect of rects){
							isEnd = Annotation.isPointInRect(x,y,rect)
							if (isEnd){
								if (rects[0]){
									axis = {
										left: rects[0].left - Selection.viewerPadding.left,
										top: rects[0].top - Selection.viewerPadding.top,
										width: rects[0].width,
										height: rects[0].height,
									}
								}
								let start = _encode.start
								let end = _encode.end
								let startcfi = ePubEngineCoreJS.Position._encodeCFI(document,start.node,start.offset)
								let endcfi = ePubEngineCoreJS.Position._encodeCFI(document,end.node,end.offset)
								return { 'item':item ,'rects':rects,'axis':axis,'startCfi': startcfi,'endCfi':endcfi};
							}
						}
					}

				}
			}
			return isEnd;
		},
		/**
		 * rect내에 x,y 좌표가 존재하는지.
		 * @param {*} x click x 
		 * @param {*} y click y
		 * @param {*} rect rect
		 */
		isPointInRect: function(x,y,rect){
			return x>=rect.x && x<=rect.right && y>=rect.y && y<=rect.bottom
		},
		/**
		 * uuid 생성
		 * @returns 
		 */
		uuidv4() {
			return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
			  (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
			);
		},
		randomColorHex(){
			var letters = '0123456789ABCDEF';
			var color = '#';
			for (var i = 0; i < 6; i++) {
			  color += letters[Math.floor(Math.random() * 16)];
			}
			return color;
		},
		/**
		 * annotaion change color
		 */
		/**
		 * 
		 * @param {*} name 
		 * @returns 
		 */
		 setCurrentMemoColor: function(name){
			return new Promise(function(resolve,reject){
				let find = Annotation.colorList.find(e=>e.name == name)
				if (find){
					Annotation.currentMemoColor = find
					resolve(Annotation.currentMemoColor)
				} else {
					reject('not find \"'+name+'\" in List ')
				}
			})
		},
		setCurrentHighlightColor: function(name){
			return new Promise(function(resolve,reject){
				let find = Annotation.colorList.find(e=>e.name == name)
				if (find){
					Annotation.currentHighlightColor = find
					resolve(Annotation.currentHighlightColor)
				} else {
					reject('not find \"'+name+'\" in List ')
				}
			})
		},
		setCurrentMemoColorFromIndex: function(index){
			return new Promise(function(resolve,reject){
				if (typeof index !== 'number'){
					reject('\"'+index+'"\ not a number')
					return;
				}
				if (index<0 || index > Annotation.colorList.length -1 ){
					reject('\"'+index+'"\ not in range')
					return;
				}
				Annotation.currentMemoColor = Annotation.colorList[index]
				resolve(Annotation.currentMemoColor)
			})
		},
		setCurrentHighlightColorFromIndex: function(index){
			return new Promise(function(resolve,reject){
				if (typeof index !== 'number'){
					reject('\"'+index+'"\ not a number')
					return;
				}
				if (index<0 || index > Annotation.colorList.length -1 ){
					reject('\"'+index+'"\ not in range')
					return;
				}
				Annotation.currentHighlightColor = Annotation.colorList[index]
				resolve(Annotation.currentHighlightColor)
			})
		},
		/**
		 * rects group 생성
		 * @returns 
		 */
		getRectsGroup(){
			if (!Annotation.list){
				return null
			}
			if (Annotation.list.length == 0){
				return null
			}
			let filter = Annotation.list.filter(e=>e['annotationType'] === 1 || e['annotationType'] === 2)
			if (filter.length === 0){
				filter = null
				return null
			}

			let mList = []
			let rects = null
			let ecd = null
			for (let i=0; i<filter.length; i++){
				let item = filter[i]
				try {
					ecd = Annotation.encode(
						item[KEY_ANNOTATION_CHAPTER],
						item[KEY_ANNOTATION_STARTCFIATTR],
						item[KEY_ANNOTATION_STARTINDEXFROMPARENT],
						item[KEY_ANNOTATION_ENDCFIATTR],
						item[KEY_ANNOTATION_ENDINDEXFROMPARENT],
						item[KEY_ANNOTATION_START_CFI_CHAPTER],
						item[KEY_ANNOTATION_END_CFI_CHAPTER])
					if (ecd){
						rects = Annotation.rectsFromEncode(ecd)
						mList.push({
							'id': item['id'],
							'encode': ecd,
							'rects': rects,
							'memo': item[KEY_ANNOTATION_MEMO]
						})
						rects = null
						ecd = null
					}
				} catch(err){
					console.log(err)
				}
			}

			return mList
		},
		/**
		 * 영역이 일치하는지 체크
		 * @param {*} r1 
		 * @param {*} r2 
		 * @returns 
		 */
		intersectRectByRect: function(r1, r2) {
			return !(r2.x >= r1.right || 
						r2.right <= r1.left || 
						r2.top >= r1.bottom ||
						r2.bottom <= r1.top);
		},
		/**
		 * 영역이 일치하는지 체크
		 * @param {*} r1 
		 * @param {*} r2 
		 * @returns 
		 */
		intersectRect: function(r1, r2) {
			return !(r2.x >= r1.x+r1.w || 
					 r2.x+r2.w <= r1.x || 
					 r2.y >= r1.y+r1.h ||
					 r2.y+r2.h <= r1.y);
		},
		/**
		 * 병합 대상이 존재하는지 rect기준으로 체크
		 * @returns 
		 */
		checkMergeCollide: function(){
			//selection
			let strStartCfi = Selection.m_strDragStartCfi
			let strEndCfi = Selection.m_strDragEndCfi
			if (!strStartCfi || !strEndCfi){
				return null;
			}
			if (strStartCfi.length == 0 || strEndCfi.length == 0 ){
				return null;
			}

			let rtnCompare = Position._CheckCfiCompare(strStartCfi, strEndCfi);
			strStartCfi = rtnCompare['Big'];
			strEndCfi = rtnCompare['Small'];
			
			let rtnDraw = Position._GetDrawNodeByCfi(strStartCfi, strEndCfi, true);
			strStartCfi = null
			strEndCfi = null
			if (typeof rtnDraw === 'number'){
				return;
			}
			let rtnDraw2 = rtnDraw.flatMap(e=>[e['rect']])
			if (typeof rtnDraw2 === 'number'){
				return;
			}

			
			
			//annotation
			let group = Annotation.getRectsGroup()
			let dd = document.getElementById('contents')
			let dd2 = dd.getBoundingClientRect()
			let offtop = dd2.top;
			let offLeft = dd2.left;
			let A = null
			let B = null
			let isColl = null
			let isColl2 = null
			let count = 0;
			let resultList = []
			try {
				for (let item of group){
					if (!item.rects){
						continue
					}
					A = {'x':0,'y':0,'w':0,'h':0}
					count = 0
					for (let current of rtnDraw2){
						A.x = current.left - offLeft
						A.y = current.top - offtop
						A.w = current.width
						A.h = current.height
						for (let other of item.rects){
							B = {'x':0,'y':0,'w':0,'h':0}
							B.x = other.left - offLeft
							B.y = other.top - offtop
							B.w = other.width
							B.h = other.height
							isColl = Annotation.intersectRect(A,B)
							isColl2 = Annotation.intersectRect(B,A)
							if (isColl&&isColl2){
								count++
								break;
							}
							B = null
						}
					}
					A = null
					
					if (count>0){
						resultList.push(item)
					}
				}
			} catch(err){

			} finally {
				group = null
				count = null
				A = null
				B = null
				dd = null
				dd2 = null
				offtop = null
				offLeft = null
				isColl = null
				isColl2 = null
			}
			return resultList
		},
		/**
		 * [merge] list를 순서대로 정렬
		 * @param {*} mergeList 
		 * @returns {Object} 앞쪽 순서로 정렬 start: 첫아이템, end: 마지막, delete: 삭제대상 리스트
		 */
		sortNodeOrder:function(mergeList){
			let startNodeAsc = null
			let endNodeAsc = null
			let deleteList = null
			try {
				startNodeAsc = mergeList.flatMap((item)=>[{id: item.id,position: Util.getNodeIndex(Util.findParent(item.encode.start.node),item.encode.start.node),node:  item.encode.start.node, offset: item.encode.start.offset}]).sort((item,item2)=>(item.position === item2.position)?(item.offset === item2.offset)?0:(item.offset > item2.offset)?1:-1:(item.position > item2.position)?1:-1);
				endNodeAsc = mergeList.flatMap((item)=>[{id: item.id,position: Util.getNodeIndex(Util.findParent(item.encode.end.node),item.encode.end.node),node:  item.encode.end.node, offset: item.encode.end.offset}]).sort((item,item2)=>(item.position === item2.position)?(item.offset === item2.offset)?0:(item.offset > item2.offset)?1:-1:(item.position > item2.position)?1:-1);
				deleteList = startNodeAsc.filter(e=>e.id != -1).flatMap(e=>[e.id])
				return { start: startNodeAsc, end: endNodeAsc ,delete: deleteList }
			} catch(err) {
				console.error('ePubEngineCoreJS.Annotation.sortNodeOrder()',err)
			} finally {
				startNodeAsc = null
				endNodeAsc = null
				deleteList = null
			}
		},
		
		/**
		 * [merge] 병합될 대상이 있는 경우 병합하는 맨앞쪽,뒤쪽,삭제대상을 return한다.
		 * @param {*} mergeList 
		 * @returns 
		 */
		findMergeTarget:function(mergeList){
			if (!mergeList){
				return null;
			}
			if (mergeList.length == 0){
				return null;
			}
			//selection 녀석을 넣어서 비교한다.
			mergeList.push({'id': -1, 'encode': {
				'start': Position._decodeCFI(document,Selection.m_strDragStartCfi),
				'end': Position._decodeCFI(document,Selection.m_strDragEndCfi)
				},
			})
			let sortorder =null
			let smallest = null
			let resultSmall = null
			let biggest = null
			let resultBig = null
			let deleteList = null
			let memoList = []
			let memo = null
			try {
				sortorder = Annotation.sortNodeOrder(mergeList)
				if (!sortorder){
					return null;
				}
				
				//가장앞쪽
				smallest = sortorder.start[0]
				//가장뒷쪽
				biggest = sortorder.end[sortorder.end.length-1]
				//삭제될대상
				deleteList = sortorder.delete
				//
				if (deleteList &&  deleteList.length>0){
					for (let del of deleteList){
						let find = mergeList.find(e=> e.memo && e.id === del )
						if (find){
							memoList.push(find.memo)
						}
					}
				}
				if (memoList.length > 0){
					memo = memoList.join('')
				}

				if (!smallest || !biggest){
					return null;
				}
				if (smallest.id === -1){
					resultSmall = -1
				} else {
					resultSmall = Annotation.findById(smallest.id)
				}
				if (biggest.id === -1){
					resultBig = -1
				} else {
					resultBig = Annotation.findById(biggest.id)
				}
				return { 
					'smallest': resultSmall,
					'biggest': resultBig,
					'deleteList': deleteList,
					'memo': memo,
				}
			} catch(err){
				console.error('ePubEngineCoreJS.Annotation.findMergeTarget()',err)
			} finally {
				smallest = null
				biggest = null
				resultSmall = null
				resultBig = null
				deleteList = null
				memoList = null
			}
		},
		/**
		 * 병합대상인지 확인
		 * @returns 
		 */
		checkMerge(){
			let checkMerge = Annotation.checkMergeCollide()
			if (checkMerge && checkMerge.length>0){
				return Annotation.findMergeTarget(checkMerge)
			}
			return false
		},
		/**
		 * conver to response to annotation
		 * @param {*} res 
		 * @returns 
		 */
		responseToObj: function(res){
			let obj = new Object();
			obj[KEY_ANNOTATION_ID]						= res['code']
			obj[KEY_ANNOTATION_SERVERID]				= res['code']
			obj[KEY_ANNOTATION_CHAPTER]					= res['chapterno']
			obj[KEY_ANNOTATION_CHAPTERNAME]				= res['chaptername']

			obj[KEY_ANNOTATION_START_CFI_CHAPTER]		= res['startfileno']
			obj[KEY_ANNOTATION_STARTCFIATTR]			= res['startpath']
			obj[KEY_ANNOTATION_STARTINDEXFROMPARENT]	= res['startpathindex']

			obj[KEY_ANNOTATION_END_CFI_CHAPTER]			= res['endfileno']
			obj[KEY_ANNOTATION_ENDCFIATTR]				= res['endpath']
			obj[KEY_ANNOTATION_ENDINDEXFROMPARENT]		= res['endpathindex']

			obj[KEY_ANNOTATION_SELECTOR]				= res['']
			obj[KEY_ANNOTATION_DATE]					= res['updatedate']
			obj[KEY_ANNOTATION_ANNOTATIONTYPE]			= res['type']
			obj[KEY_ANNOTATION_TEXT]					= res['selectedtext']
			obj[KEY_ANNOTATION_COLOR]					= res['color']
			obj[KEY_ANNOTATION_COLORTYPE]				= res['colortype']
			if (res['colortype'] === 0){
				let find = Annotation.colorList.findIndex(e=>e.color === res['color'])
				if (find>=0){
					obj[KEY_ANNOTATION_COLORKEY]				= Annotation.colorList[find].name
				}
				
			} else if (res['colortype'] === 1){
				let find = Annotation.colorList.filter(e=>e.type === 1).findIndex(e=>e.color === res['color'])
				if (find>=0){
					obj[KEY_ANNOTATION_COLORKEY]				= Annotation.colorList[find].name
				}
			}

			obj[KEY_ANNOTATION_MEMO]					= res['memo']
			obj[KEY_ANNOTATION_PERCENT]					= res['startpercent']
			// obj[KEY_ANNOTATION_STATUS]					= res['S']
			obj[KEY_ANNOTATION_MEDIAPATH]				= res['mediapath']
			obj['ver']									= res['ver']
			if (typeof obj[KEY_ANNOTATION_DATE] === 'string' && obj[KEY_ANNOTATION_DATE].length > 0){
				obj[KEY_ANNOTATION_DATE] = new Date(obj[KEY_ANNOTATION_DATE]).toFormat()
			}
			return Object.assign({},obj)
		},
		responseToList:function(list){
			let result = null
			if (list instanceof Array){
				result = list.flatMap(e=>Annotation.responseToObj(e))
			} 
			return result
		},
		/**
		 * id기준으로 해당 아이템 select
		 * @param {*} id 
		 * @returns 
		 */
		findById:function(id){
			let idx = Annotation.list.findIndex(e=>e.id === id)
			if (idx >= 0){
				return Annotation.list[idx]
			}
			return null
		},
		/**
		 * 북마크
		 */
		CurrentBookmark: null,
		/**
		 * 북마크 리스트
		 * @returns 
		 */
		getBookmarkList:function(){
			return Annotation.list.filter(e=>e.annotationType === 0)
		},
		/**
		 * 북마크가 포함인지
		 */
		isBookmarkContains(){
		   let bList = Annotation.getBookmarkList()
		   if (bList.length === 0){
			return null
		   }
		   let has = null
		   for (let item of bList){
				let decode = Position.DecodeCfiAttr(
					item[KEY_ANNOTATION_START_CFI_CHAPTER],
					item[KEY_ANNOTATION_STARTCFIATTR],
					item[KEY_ANNOTATION_STARTINDEXFROMPARENT],
					item[KEY_ANNOTATION_END_CFI_CHAPTER],
					item[KEY_ANNOTATION_ENDCFIATTR],
					item[KEY_ANNOTATION_ENDINDEXFROMPARENT])
				if (!decode){
					continue;
				}
				let rects = Util.getClientRects(decode.start.node)
				for (let rect of rects){
					if (rect && Util.rectInRange(rect)){
						has = item
						break;
					} 
				}
				if (has){
					break;
				}
		   }
		   return has
		},
		
		/**
		 * 북마크가 있는지 확인하고 북마크를 Published함.
		 */
		checkBookmark(){
			Annotation.CurrentBookmark = Annotation.isBookmarkContains()
			window.dispatchEvent(new CustomEvent('bdbcheckbookmark',{
				detail: { bookmark: Annotation.CurrentBookmark}
			}))
		},
		/**
		 * 현재 스크린에 보이는 북마크 데이터를 추출한다.
		 */
		getBookmarkInScreen(chapterName, percent){
			return new Promise(function(resolve,reject){
				let check = Walker.checkNodesInScreen()

				let date = new Date()
				let obj = new Object()
				obj[KEY_ANNOTATION_ID] 						= date.valueOf()
				obj[KEY_ANNOTATION_SERVERID]				= null

				obj[KEY_ANNOTATION_CHAPTERNAME]				= chapterName

				obj[KEY_ANNOTATION_SELECTOR]				= null
				obj[KEY_ANNOTATION_DATE]					= date.current()
				obj[KEY_ANNOTATION_ANNOTATIONTYPE]			= 0

				obj[KEY_ANNOTATION_COLOR]					= null
				obj[KEY_ANNOTATION_COLORKEY]				= null
				
				obj[KEY_ANNOTATION_PERCENT]					= percent
				obj[KEY_ANNOTATION_STATUS] 					= KEY_ANNOTATION_STATUS_CREATE

				//both
				if (check.code === 0){
					Walker.getScreenPosition().then(rs=>{
						obj[KEY_ANNOTATION_CHAPTER]					= rs.encode.startChapter
						obj[KEY_ANNOTATION_START_CFI_CHAPTER]		= rs.encode.startChapter
						obj[KEY_ANNOTATION_STARTCFIATTR]			= rs.encode.startCfiAttr
						obj[KEY_ANNOTATION_STARTINDEXFROMPARENT]   	= rs.encode.startIndexFromParent
						obj[KEY_ANNOTATION_END_CFI_CHAPTER]			= rs.encode.endChapter
						obj[KEY_ANNOTATION_ENDCFIATTR]				= rs.encode.endCfiAttr
						obj[KEY_ANNOTATION_ENDINDEXFROMPARENT]		= rs.encode.endIndexFromParent
						obj[KEY_ANNOTATION_TEXT]					= rs.text
						obj[KEY_ANNOTATION_MEMO]					= null
						resolve(obj)
					}).catch(err=>{
						reject(err)
					})
				} 
				//text
				else if (check.code === 1){
					Walker.getScreenPosition().then(rs=>{
						obj[KEY_ANNOTATION_CHAPTER]					= rs.encode.startChapter
						obj[KEY_ANNOTATION_START_CFI_CHAPTER]		= rs.encode.startChapter
						obj[KEY_ANNOTATION_STARTCFIATTR]			= rs.encode.startCfiAttr
						obj[KEY_ANNOTATION_STARTINDEXFROMPARENT]   	= rs.encode.startIndexFromParent
						obj[KEY_ANNOTATION_END_CFI_CHAPTER]			= rs.encode.endChapter
						obj[KEY_ANNOTATION_ENDCFIATTR]				= rs.encode.endCfiAttr
						obj[KEY_ANNOTATION_ENDINDEXFROMPARENT]		= rs.encode.endIndexFromParent
						obj[KEY_ANNOTATION_TEXT]					= rs.text
						obj[KEY_ANNOTATION_MEMO]					= null
						resolve(obj)
					}).catch(err=>{
						reject(err)
					})
				} 
				//data
				else if (check.code === 2){
					let item = check.data[0][0].node
					let attrCfi = item.getAttribute('cfi')
					let chapter = Util.findParentChapter(item)
					if (attrCfi){
						obj[KEY_ANNOTATION_CHAPTER]					= chapter
						obj[KEY_ANNOTATION_START_CFI_CHAPTER]		= chapter
						obj[KEY_ANNOTATION_STARTCFIATTR]			= attrCfi
						obj[KEY_ANNOTATION_STARTINDEXFROMPARENT]   	= -1
						obj[KEY_ANNOTATION_END_CFI_CHAPTER]			= chapter
						obj[KEY_ANNOTATION_ENDCFIATTR]				= attrCfi
						obj[KEY_ANNOTATION_ENDINDEXFROMPARENT]		= -1
						obj[KEY_ANNOTATION_TEXT]					= null
						obj[KEY_ANNOTATION_MEMO]					= null
					}
					let data = Util.getBase64Image(item)
					obj[KEY_ANNOTATION_MEMO] = data
					resolve(obj)
				} 
				//error
				else if (check.code === -1){
					reject('checkNodesInScreen fail error')
				} else {
					reject('unknown error')
				}
			})
		},
	};

	/**
	 * selection을 표시할 element
	 */
	const WM_DRAG_INIT = 0;
	const WM_DRAG_START = 1;
	const WM_DRAG_END =  2
	var _selectionElement = document.createElement('div');
	_selectionElement.style.zIndex = 0;
	_selectionElement.style.position = "absolute";
	_selectionElement.style.pointerEvents = 'none';//layer 아래쪽으로 이벤트가 전달안되 none
	_selectionElement.style.opacity = 0.2;

	/**
	 * picker의 왼쪽
	 */
	const _picker_size = 6;
	const _picker_padding = 20;
	const _picker_height_offset = 12;
	var _selector_img_element1 = document.createElement('div');
	_selector_img_element1.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="8" height="35" viewBox="0 0 8 35">
											<g transform="translate(-690 -274)">
												<path data-name="사각형 1979" transform="translate(693 274)" style="fill:#2968b5" d="M0 0h2v30H0z"/>
												<circle data-name="타원 91" cx="4" cy="4" r="4" transform="translate(690 301)" style="fill:#2968b5"/>
											</g>
										</svg>`
	_selector_img_element1.id = 'bdb_img_selector_start';
	_selector_img_element1.setAttribute('bdb-selection-picker','left')
	// _selector_img_element1.src = '/images/static/ic-start.svg'
	_selector_img_element1.style.zIndex = 6;
	_selector_img_element1.style.position = "absolute";
	_selector_img_element1.style.display = 'none';
	_selector_img_element1.style.width ='auto';
	_selector_img_element1.style.height = _picker_size+'px';
	_selector_img_element1.style.padding = _picker_padding+'px';
	_selector_img_element1.style.opacity = "1";

	/**
	 * picker의 오른쪽
	 */
	var _selector_img_element2 = document.createElement('div');
	_selector_img_element2.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="8" height="35" viewBox="0 0 8 35">
											<path data-name="사각형 1979" transform="translate(3)" style="fill:#2968b5" d="M0 0h2v30H0z"/>
											<circle data-name="타원 91" cx="4" cy="4" r="4" transform="translate(0 27)" style="fill:#2968b5"/>
										</svg>`
	_selector_img_element2.id = 'bdb_img_selector_end';
	_selector_img_element2.setAttribute('bdb-selection-picker','right')
	// _selector_img_element2.src = '/images/static/ic-end.svg'
	_selector_img_element2.style.zIndex = 6;
	_selector_img_element2.style.position = "absolute";
	_selector_img_element2.style.display = 'none';
	// _selector_img_element2.style.width = _picker_size+'px';
	_selector_img_element1.style.width ='auto';
	_selector_img_element2.style.height = _picker_size+'px';
	_selector_img_element2.style.padding = _picker_padding+'px';
	_selector_img_element2.style.opacity = "1";


	var Selection = Core.Selection = {
		/**
		 * selection을 표기할 container id
		 */
		selectionContainerID:'viewer_selection',
		setSelectionContainerID:function(sectionParentid){
			Selection.selectionContainerID = sectionParentid
		},
		/**
		 * viewer의 padding rect
		 */
		viewerPadding:null,
		
		/**
		 * picker visible callback
		 */
		callbackShow: null,
		callbackHide: null,
		callbackRequestVisible:null,
		callbackPickerMove:null,
		/**
		 * selection된 영역의 cfi
		 */
		nodeCfi:null,

		/**
		 * selection한 list cache
		 */
		selectionCache: {
			start: null,
			end: null,
		},
		/**
		 * -1: none
		 * 0: next
		 * 1: prev
		 */
		selectionCacheMode: -1,
		/**
		 * 생성된 picker가 실제 터치위치에서 떨어진 offset
		 */
		_pickerOffsetFront:null,
		_pickerOffsetEnd:null,
		_isSelectPicker:false,
		setSelectPicker:function(value){
			Selection._isSelectPicker = value;
		},
		/**
		 * 피커가 사용자에 의해 현재 잡혀있는 상태인지
		 * @returns 
		 */
		getSelectPicker:function(){
			return Selection._isSelectPicker;
		},
		/**
		 * 현재 selection된 cfi값을 return 한다.
		 * @returns 
		 */
		getNodeCfi:function(){
			return Selection.nodeCfi;
		},
	
		/**
		 * selection할수있는 영역의 limit을 주기위해 padding 값을 가저온다.
		 * @param {*} padding 
		 */
		setSelectionPadding:function(padding){
			Selection.viewerPadding = padding;
		},
		/**
		 * 모바일에 사용한 interface를 setup한다.
		 * @param {*} showFunc 
		 * @param {*} hideFunc 
		 * @param {*} callbackRequestVisible 
		 */
		setMobileCallback: function(showFunc, hideFunc,callbackRequestVisible , callbackPickerMove) {
			Selection.callbackShow = showFunc;
			Selection.callbackHide = hideFunc;
			Selection.callbackRequestVisible = callbackRequestVisible;
			Selection.callbackPickerMove = callbackPickerMove;
		},
		/**
		 * 피커사용 유무를 결정한다.
		 * @param {*} value 
		 */
		setUseSelectionPicker:function(value){
			_use_selection_picker = value;
		},
		/**
		 * (not use)selection의 배경색상을 결정
		 * @param {*} color 
		 * @returns 
		 */
		createSelectionBackground: function(color) {
			var xmlns = "http://www.w3.org/2000/svg";
			var svgElem = document.createElementNS(xmlns, "svg");
			var g = document.createElementNS(xmlns, "g");

			svgElem.setAttributeNS(null, "width", "100%");
			svgElem.setAttributeNS(null, "height", "100%");
			svgElem.setAttributeNS(null, "z-index", "-1");

			svgElem.appendChild(g);
			g.setAttributeNS(null, 'opacity', '0.7');

			var rct = document.createElementNS(xmlns, "rect");
			rct.setAttributeNS(null, "x", "0");
			rct.setAttributeNS(null, "y", "0");
			rct.setAttributeNS(null, "width", "100%");
			rct.setAttributeNS(null, "height", "100%");
			rct.setAttributeNS(null, "fill", color);

			g.appendChild(rct);

			return svgElem;
		},
		/**
		 * selection 영역의 모든 selection을 제거한다.
		 */
		DeleteSelection: function () {
			// Selection.selectionPickerRestartInfo = null
			if (!Selection.selection_continue_mode){
				Selection.deleteSelectionCache()
			}
			
			var isDebug = false;
			var sel = document.getElementById(Selection.selectionContainerID);
			var i = null;

			try {
				for (i = 0; i < sel.childNodes.length; i++) {
					if (sel.childNodes[i].id != undefined && sel.childNodes[i].id.indexOf('Rue_Selection_Container_SEL_') == 0 ||
						sel.childNodes[i].id == 'RueSelStartBar' || sel.childNodes[i].id == 'RueSelEndBar' 
						|| sel.childNodes[i].id === 'bdb_img_selector_start' || sel.childNodes[i].id === 'bdb_img_selector_end') {
						sel.childNodes[i].onclick = null;
						sel.removeChild(sel.childNodes[i]);
						i--;
					}
				}
			}
			finally {
				isDebug = null;
				sel = null;
				i = null;
			}
		},
		/**
		 * 현재 selection이 되어있는지 확인한다.
		 * @returns 
		 */
		IsSelected:function(){
			var sele = document.getElementById(Selection.selectionContainerID)
			if (sele && sele.hasChildNodes()){
				return true;
			}
			return false;
		},
		/**
		 * style의 visibility값이 visible인지 hidden인지 확인한다.
		 * @returns 
		 */
		IsShowSelection: function () {
			if (document.getElementById(Selection.selectionContainerID).style.visibility == 'hidden') {
				return false;
			}
			return true;
		},
		/**
		 * selection을 hidden처리한다.
		 */
		HideSelection: function () {
			document.getElementById(Selection.selectionContainerID).style.visibility = 'hidden';
		},
		/**
		 * selection을 visible처리한다.
		 */
		ShowSelection: function () {
			document.getElementById(Selection.selectionContainerID).style.visibility = 'visible';
		},
		/**
		 * 좌표를 받아 view bound내 좌표로 return
		 * @param {*} x 
		 * @param {*} y 
		 * @returns 
		 */
		_getSelectionPoint:function(x, y){
			var pickerX = x;
			var pickerY = y;
			if ( pickerX < Selection.viewerPadding.left ){
				pickerX = Selection.viewerPadding.left;
			} else if (pickerX > Selection.viewerPadding.left+Selection.viewerPadding.width-1){
				pickerX = Selection.viewerPadding.left+Selection.viewerPadding.width-1;
			}

			if ( pickerY < Selection.viewerPadding.top ){
				pickerY = Selection.viewerPadding.top;
			} else if (pickerY > Selection.viewerPadding.top+Selection.viewerPadding.height-1){
				pickerY = Selection.viewerPadding.top+Selection.viewerPadding.height-1;
			}

			return {x:pickerX , y:pickerY};
		},
		/**
		 * picker의 event를 좌표값으로 return
		 * @param {*} e event
		 * @returns 
		 */
		_getPickerPoint:function(e , direction){
			// e.preventDefault()

			var correctX = (direction)?14:-7.5;
			let mX = 0
			let mY = 0
			if (e.touches||e.changedTouches){
				var touch = e.touches[0] || e.changedTouches[0];
				mX = touch.pageX
				mY = touch.pageY
			} else {
				mX = e.pageX
				mY = e.pageY
			}
			var pickerX = mX + correctX;
			var pickerY = mY - _picker_size;
			if ( pickerX < Selection.viewerPadding.left ){
				//왼쪽 padding만큼 하면 그전 element가 스캔되기때문에 좌표를 안쪽으로 +1해준다. 
				pickerX = Selection.viewerPadding.left+1;
			} else if (pickerX > Selection.viewerPadding.left+_viewer_width-1){
				//위와 같은 이치.
				pickerX = Selection.viewerPadding.left+_viewer_width-1;
			}

			if ( pickerY < Selection.viewerPadding.top ){
				pickerY = Selection.viewerPadding.top+1;
			} else if (pickerY > Selection.viewerPadding.top+_viewer_height-1){
				pickerY = Selection.viewerPadding.top+_viewer_height-1;
			}
			return {x:pickerX , y:pickerY};
		},
		/**
		 * 왼쪽.
		 * @param {*} e 
		 */
		_leftOntouchstart: function(e){
			e.preventDefault();
			if (Selection.callbackHide){
				Selection.callbackHide('front' , false , false);
			}
			Selection.setSelectPicker(true);
		
			if(!ele_view_wrapper.hasClass('selectable')) {
				ele_view_wrapper.addClass('selectable');
			}
			var pickerPoint = Selection._getPickerPoint(e , true);
			Selection.OnSelectionReStartFront(pickerPoint.x, pickerPoint.y);

			pickerPoint = null;
			e.stopPropagation();
		},
		/**
		 * 왼쪽 picker move
		 * @param {*} e 
		 */
		_leftOntouchmove: function(e){
			e.preventDefault();
			var pickerPoint = Selection._getPickerPoint(e, true);
			Selection.OnSelectionReStartFront(pickerPoint.x, pickerPoint.y);
			// if (Layout.getScrollMode()){
				if (Selection.callbackPickerMove){
					var isSelectLast = (Selection.viewerPadding.theight-1) === pickerPoint.y;
					var isSelectFirst = (Selection.viewerPadding.top+1) === pickerPoint.y
					Selection.callbackPickerMove(true ,pickerPoint.x, pickerPoint.y ,isSelectLast,isSelectFirst);
				}
			// }
			pickerPoint = null;
			e.stopPropagation();
		},
		/**
		 * 왼쪽 picker end
		 * @param {*} e 
		 */
		_leftOntouchEnd: function(e){
			var pickerPoint = Selection._getPickerPoint(e, true);
			if (Selection.callbackShow){
				var isSelectLast = (Selection.viewerPadding.theight-1) === pickerPoint.y;
				var nodes = Selection.getSelectionNodes(true)
				var node0 = nodes[0].getBoundingClientRect()
				var node1 = nodes[1].getBoundingClientRect()
				var node0_diff = Math.abs(node0.y-pickerPoint.y)
				var node1_diff = Math.abs(node1.y-pickerPoint.y)

				Selection.callbackShow('front', true,isSelectLast, node0_diff < node1_diff);


			 	isSelectLast = null;
				nodes = null;
				node0 = null;
				node1 = null;
				node0_diff = null;
				node1_diff = null;
			}
			Selection.OnSelectionEnd(pickerPoint.x, pickerPoint.y);
			Selection.setSelectPicker(false);
			if(ele_view_wrapper.hasClass('selectable')) {
				ele_view_wrapper.removeClass('selectable');
			}

			e.stopPropagation();
		},
		/**
		 * 오른쪽
		 * @param {*} e 
		 */
		_rightOntouchstart:function(e){
			if (Selection.callbackHide){
				Selection.callbackHide('back' , false , false);
			}
			Selection.setSelectPicker(true);
			
			if(!ele_view_wrapper.hasClass('selectable')) {
				ele_view_wrapper.addClass('selectable');
			}
			var pickerPoint = Selection._getPickerPoint(e, false);
			// Selection.OnSelectionEnd(pickerPoint.x, pickerPoint.y);
			Selection.OnSelectionReStartBack(pickerPoint.x, pickerPoint.y);

			pickerPoint = null;
			e.stopPropagation();
		},
		/**
		 * 오른쪽 피커 move
		 * @param {*} e 
		 */
		_rightOntouchmove:function(e){
			//e.preventDefault();(rila)
			var pickerPoint = Selection._getPickerPoint(e, false);
			Selection.OnSelectionReStartBack(pickerPoint.x, pickerPoint.y);
			// if (Layout.getScrollMode()){
				if (Selection.callbackPickerMove){
					var isSelectLast = (Selection.viewerPadding.theight-1) === pickerPoint.y;
					var isSelectFirst = (Selection.viewerPadding.top+1) === pickerPoint.y
					Selection.callbackPickerMove(false ,pickerPoint.x, pickerPoint.y ,isSelectLast,isSelectFirst);
				}
			// }
			pickerPoint = null;
			e.stopPropagation();
		},
		/**
		 * 오른쪽 피커 end
		 * @param {*} e 
		 */
		_rightOntouchEnd:function(e){
			var pickerPoint = Selection._getPickerPoint(e, false);
			if (Selection.callbackShow){
				var isSelectLast = (Selection.viewerPadding.theight-1) === pickerPoint.y;
				var nodes = Selection.getSelectionNodes(true)
				var node0 = nodes[0].getBoundingClientRect()
				var node1 = nodes[1].getBoundingClientRect()
				var node0_diff = Math.abs(node0.y-pickerPoint.y)
				var node1_diff = Math.abs(node1.y-pickerPoint.y)
				
				Selection.callbackShow('back' , true , isSelectLast, node0_diff < node1_diff);
				
				isSelectLast = null;
				nodes = null;
				node0 = null;
				node1 = null;
				node0_diff = null;
				node1_diff = null;
			}


			Selection.OnSelectionEnd(pickerPoint.x, pickerPoint.y);
			Selection.setSelectPicker(false);

			if(ele_view_wrapper.hasClass('selectable')) {
				ele_view_wrapper.removeClass('selectable');
			}

			pickerPoint = null;
			e.stopPropagation();
		},
		_isStartMouseDown: false,
		_leftOnStart: function(e){
			Selection._isStartMouseDown = true
	
		},
		_leftOnMove: function(e){
			if (!Selection._isStartMouseDown){
				return;
			}

		},
		_leftOnEnd: function(e){
			Selection._isStartMouseDown = false
		
		},
		_leftOnLeave: function(e){
			Selection._isStartMouseDown = false
		
		},

		_rightOnStart: function(e){
			Selection._isStartMouseDown = true
		
		},
		_rightOnMove: function(e){
			if (!Selection._isStartMouseDown){
				return;
			}
			
		},
		_rightOnEnd: function(e){
			Selection._isStartMouseDown = false
	
		},
		_rightOnLeave: function(e){
			Selection._isStartMouseDown = false
		
		},
		/**
		 * 
		 * @param {*} strStartCfi 시작지점의 cfi
		 * @param {*} strEndCfi 끝지점의 cfi
		 * @param {String} strColor string hex color
		 * @returns 
		 */
		DrawSelection: function (strStartCfi, strEndCfi, strColor) {
			var isDebug = false;
			var rtnCompare = null;
			var rtnDraw = null;
			var nRtnDraw_Cnt = null;
			var dl_idx = null;
			var re = null;
			var se = null;
			var pop_top = null;
			var pop_new_top = null;
			var isRes = false;
			var pageH = null;
			var baseLeft = parseInt(ele_view_wrapper.offset().left);
			var baseTop = parseInt(ele_view_wrapper.offset().top);
			var result = new Array();
			var resultIdx = 0;


			try {
				Selection.DeleteSelection();

				if (strColor == null || strColor == '') {
					strColor = 'red';
				}
				if (strStartCfi === undefined || strStartCfi == null || strStartCfi == '') {
					rtnInfo[0] = '-1:'; //시작위치값이 없습니다.
					return rtnInfo;
				}
				if ( strEndCfi === undefined || strEndCfi == null || strEndCfi == '') {
					rtnInfo[0] = '-2:'; //종료위치값이 없습니다.
					return rtnInfo;
				}

				rtnCompare = Position._CheckCfiCompare(strStartCfi, strEndCfi);
				
				strStartCfi = rtnCompare['Big'];
				strEndCfi = rtnCompare['Small'];
				rtnCompare = null;

				rtnDraw = Position._GetDrawNodeByCfi(strStartCfi, strEndCfi, true);
				strStartCfi = null;
				strEndCfi = null;
				Selection.nodeCfi = rtnDraw;
				var selectArea = document.getElementById(Selection.selectionContainerID);
				var doc = document.createDocumentFragment();
				if (rtnDraw.constructor == Array) {
					nRtnDraw_Cnt = rtnDraw.length;

					for (dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
						// if (parseFloat(rtnDraw[dl_idx]['top'].toString(), 10) < 0 || parseFloat(rtnDraw[dl_idx]['height'].toString(), 10) > pageWidth){
						// 	continue;
						// }

						result[resultIdx] = new Array();

						if (Selection.viewerPadding){
							// result[resultIdx]['top'] = rtnDraw[dl_idx]['rect'].top - Selection.viewerPadding.top;
							result[resultIdx]['top'] = rtnDraw[dl_idx]['rect'].top;
							result[resultIdx]['left'] = rtnDraw[dl_idx]['rect'].left - Selection.viewerPadding.left+Selection.viewerPadding.windowLeftRelation;
							result[resultIdx]['width'] = rtnDraw[dl_idx]['rect'].width;
							result[resultIdx]['height'] =rtnDraw[dl_idx]['rect'].height;
						} else {
							result[resultIdx]['top'] = rtnDraw[dl_idx]['rect'].top - baseTop;
							result[resultIdx]['left'] = rtnDraw[dl_idx]['rect'].left - baseLeft;
							result[resultIdx]['width'] = rtnDraw[dl_idx]['rect'].width;
							result[resultIdx]['height'] =rtnDraw[dl_idx]['rect'].height;
						}

						//같은 라인에 있는 이전 아이템의 빈공간만큼 보정.
						result[resultIdx]['width'] += Selection.getFillSelectionEmptySpace(dl_idx, rtnDraw , nRtnDraw_Cnt);


						result['SelectionText'] = rtnDraw[0]['SelectionText'].toString();
						Selection.m_current_visible_selected_text = null;
						Selection.m_current_visible_selected_text = result['SelectionText'];

						re = _selectionElement.cloneNode(true);
						re.id = 'Rue_Selection_Container_SEL_' + rtnDraw[dl_idx]['id'].toString();
						se = re.style;
						se.top = result[resultIdx]['top'] + 'px';
						se.left = result[resultIdx]['left'] + 'px';
						se.width = result[resultIdx]['width'] + 'px';
						se.height = result[resultIdx]['height'] + 'px';
						se.setProperty('background-color', strColor, 'important');
						doc.appendChild(re);

						se = null;
						re = null;

						result['res'] = 0;

						result[resultIdx]['rleft'] = parseFloat(rtnDraw[dl_idx]['rleft'].toString(), 10);
						resultIdx++;
					}

					selectArea.appendChild(doc);
					doc = null;

					if (_use_selection_picker){
						//왼쪽
						selectArea.prepend(_selector_img_element1);
						//-2는 실제 svg 안쪽 ui의 넓이
						_selector_img_element1.style.top = result[0]['top']  -_picker_padding+ 'px';
						_selector_img_element1.style.left = result[0]['left'] - _picker_size - _picker_padding  +'px';
						_selector_img_element1.style.height = result[0]['height'] + _picker_height_offset + 'px';
						_selector_img_element1.setAttribute('tp', result[0]['top']);

						
						//오른쪽
						var lastlength = (result.length === 0)?0:result.length-1
						selectArea.appendChild(_selector_img_element2);
						_selector_img_element2.style.top = result[lastlength]['top'] - _picker_padding + 'px';
						_selector_img_element2.style.left = result[lastlength]['left'] + result[lastlength]['width'] - _picker_padding  - (_picker_height_offset/2) + 'px';
						_selector_img_element2.style.height = result[lastlength]['height'] + _picker_height_offset + 'px';
						lastlength = null;


						// 피커의 touch event를 받아 selection move
						//왼쪽 피커
						// _selector_img_element1.removeEventListener('mousedown', Selection._leftOnStart);
						// _selector_img_element1.removeEventListener('mousemove', Selection._leftOnMove);
						// _selector_img_element1.removeEventListener('mouseup ', Selection._leftOnEnd);
						// _selector_img_element1.removeEventListener('mouseleave ', Selection._leftOnLeave);

						_selector_img_element1.removeEventListener('touchstart', Selection._leftOntouchstart);
						_selector_img_element1.removeEventListener('touchmove', Selection._leftOntouchmove);
						_selector_img_element1.removeEventListener('touchend ', Selection._leftOntouchEnd);
						_selector_img_element1.removeEventListener('touchcancel ', Selection._leftOntouchEnd);

						// _selector_img_element1.addEventListener('mousedown', Selection._leftOnStart, { passive: true });
						// _selector_img_element1.addEventListener('mousemove', Selection._leftOnMove, { passive: true });
						// _selector_img_element1.addEventListener('mouseup', Selection._leftOnEnd, { passive: true });
						// _selector_img_element1.addEventListener('mouseleave ', Selection._leftOnLeave, { passive: true });

						_selector_img_element1.addEventListener('touchstart', Selection._leftOntouchstart, { passive: true });
						_selector_img_element1.addEventListener('touchmove', Selection._leftOntouchmove, { passive: true });
						_selector_img_element1.addEventListener('touchend', Selection._leftOntouchEnd, { passive: true });
						_selector_img_element1.addEventListener('touchcancel ', Selection._leftOntouchEnd, { passive: true });

						//오른쪽 피커
						// _selector_img_element2.removeEventListener('mousedown', Selection._rightOnStart);
						// _selector_img_element2.removeEventListener('mousemove', Selection._rightOnMove);
						// _selector_img_element2.removeEventListener('mouseup ', Selection._rightOnEnd);
						// _selector_img_element2.removeEventListener('mouseleave ', Selection._rightOnLeave);

						_selector_img_element2.removeEventListener('touchstart', Selection._rightOntouchstart);
						_selector_img_element2.removeEventListener('touchmove', Selection._rightOntouchmove);
						_selector_img_element2.removeEventListener('touchend ', Selection._rightOntouchEnd);
						_selector_img_element2.removeEventListener('touchcancel ', Selection._rightOntouchEnd);

						// _selector_img_element2.addEventListener('mousedown', Selection._rightOnStart, { passive: true });
						// _selector_img_element2.addEventListener('mousemove', Selection._rightOnMove, { passive: true });
						// _selector_img_element2.addEventListener('mouseup', Selection._rightOnEnd, { passive: true });
						// _selector_img_element2.addEventListener('mouseleave ', Selection._rightOnLeave, { passive: true });

						_selector_img_element2.addEventListener('touchstart', Selection._rightOntouchstart, { passive: true });
						_selector_img_element2.addEventListener('touchmove', Selection._rightOntouchmove, { passive: true });
						_selector_img_element2.addEventListener('touchend', Selection._rightOntouchEnd, { passive: true });
						_selector_img_element2.addEventListener('touchcancel ', Selection._rightOntouchEnd, { passive: true });
					}


					nRtnDraw_Cnt = null;
					selectArea = null;
					return result;
				}
				else {
					result['res'] = rtnDraw;
					return result;
				}
			}
			catch (ex) {
				ePubEngineLogJS.W.log("exception = " + ex);
			}
			finally {
				isDebug = null;
				rtnInfo = null;
				rtnCompare = null;
				rtnDraw = null;
				nRtnDraw_Cnt = null;
				dl_idx = null;
				re = null;
				se = null;
				pop_top = null;
				pop_new_top = null;
				pageH = null;
				selectArea = null;
			}
		},

		m_isDragStatus: WM_DRAG_INIT,
		m_strDragStartCfi:  '',
		m_strDragEndCfi:  '',
		m_strDragText:  '',
		m_strDragColor:  'var(--key-color0)',
		m_strDragStartXPath:  '',
		m_strDragEndXPath:  '',
		m_strDragStartXPathOffset:  0,
		m_strDragEndXPathOffset:  0,
		m_strDragRegionJson:  '',

		
		m_last_draw_x: 0.0,
		m_last_draw_y: 0.0,
		m_last_evt_x: 0.0,
		m_last_evt_y: 0.0,
		m_bSelect: false,
		m_nTimerCount: 0,
		m_arrDragRect: Array(),
		m_bSelectFront: false,
		m_current_visible_selected_text:'',
		selectionPickerRestartInfo: null,
		_prevent_selection_class_name:'none-selection',
		
		/**
		 * 셀렉션 시작 CFI값 리턴
		 */
		getStartCfi: function () { 
			return Selection.m_strDragStartCfi; 
		},
		/**
		 * 셀렉션 끝 CFI값 리턴
		 */
		getEndCfi: function () { 
			return Selection.m_strDragEndCfi; 
		},
		/**
		 * 시작 XPath값
		 */
		getStartXPath: function () { 
			return Selection.m_strDragStartXPath; 
		},
		/**
		 * 끝 XPath값
		 */
		getEndXPath: function () { 
			return Selection.m_strDragEndXPath; 
		},
		/**
		 * 시작 XPath Offset값
		 */
		getStartXPathOffset: function () { 
			return Selection.m_strDragStartXPathOffset; 
		},
		/**
		 * 끝 XPath Offset값
		 */
		getEndXPathOffset: function () { 
			return Selection.m_strDragEndXPathOffset; 
		},
		/**
		 * 셀렉션 선택된 문자열
		 */
		getSelectionText: function () { 
			return Selection.m_strDragText; 
		},
		/**
		 * 셀렉션 선택된 (array) : 선택된 텍스트의 좌표값
		 */
		getSelectionRect: function () { 
			return Selection.m_arrDragRect; 
		},
		/**
		 * 현재 눈에보이는 selection영역의 text
		 * @returns 
		 */
		getSelectionTextVisible:function () {
			return Selection.m_current_visible_selected_text;
		},
		/**
		 * (not use)selection된 값을 json string형태로 parse
		 * @returns 
		 */
		getSelectionRectJson: function () { 
			return Selection.m_strDragRegionJson;
		},
		/**
		 * 셀렉션 div 색상변경
		 * @param {string} strColor 색상 값 (#efefef)
		 */
		SetDrawSelectionColor: function (strColor) {
			Selection.m_strDragColor = strColor;
		},
		/**
		 * selection된 정보를 json형태로 파싱한다.
		 * @param {*} id 
		 * @param {*} res 
		 * @param {*} color 
		 * @param {*} sentence 
		 * @returns 
		 */
		getDrawSelectionJson: function (id, res, color, sentence) {
			//sentence = sentence.replace(/"/g, "\'");
			sentence = sentence.replace(/\r?\n|\r/g, '');//linebreak때문에 parsejson할때오류가 발생함. 
			var rtnVal = "{\"id\" : \"" + id + "\", \"res\" : \"" + res['res'] + "\", \"color\" : \"" + color + "\", \"sentence\" : \"" + sentence + "\", \"pageWidth\": " + _columnWidth;
			rtnVal += ", \"positions\" : [";

			for (var j = 0; j < res.length; j++) {
				if (j > 0) {
					rtnVal += ',';
				}

				var left = parseInt(res[j]['left'], 10);
				var pageNo = parseInt(left / _columnWidth) + 1;

				rtnVal += "{\"left\" : \"" + res[j]['left'] + "\", \"top\" : \"" + res[j]['top'] + "\", \"width\" : \"" + res[j]['width'] + "\", \"height\" : \"" + res[j]['height'] + "\", \"pageNo\" : \"" + pageNo + "\"}";
			}

			rtnVal += "]}";

			return rtnVal;
		},
		/**
		 * 셀렉션 div 그리기
		 * @param {boolean} wordBoundary 
		 * @returns 
		 */
		OnDrawTimer: function (wordBoundary) {
			var isDebug = true;
			var strCfi_Temp = null;
			var res = null;
			var rtnVal = '';
			var id = '';
			var curPage = 0;
			var isWordBoundary;

			Selection.m_nTimerCount++;
			if (ele_view_wrapper.get(0).classList.contains(Selection._prevent_selection_class_name)){
				ele_view_wrapper.get(0).classList.remove(Selection._prevent_selection_class_name)
			}
			try {
				if (Selection.m_bSelect == false) {
					if (!ele_view_wrapper.get(0).classList.contains(Selection._prevent_selection_class_name)){
						ele_view_wrapper.get(0).classList.add(Selection._prevent_selection_class_name)
					}
					return;
				}

				if (Selection.m_nTimerCount > 1) {
					return;
				}


				
				while (true) {
					// if (Selection.m_last_evt_x == Selection.m_last_draw_x && Selection.m_last_evt_y == Selection.m_last_draw_y) {
					// 	break;
					// }
					Selection.m_last_draw_x = Selection.m_last_evt_x;
					Selection.m_last_draw_y = Selection.m_last_evt_y;
			

					isWordBoundary = wordBoundary;
					//caretPoint가 최상단 element를 가저오기 때문에 샐랙션이 겹처버리면 
					if (Selection.IsShowSelection()){
						Selection.HideSelection();
					}
					strCfi_Temp = Position._cfiAt(document, Selection.m_last_draw_x, Selection.m_last_draw_y, isWordBoundary);
					
					if (!Selection.IsShowSelection()){
						Selection.ShowSelection();
					}
					if (!strCfi_Temp) {
						break;
					}

					if (Selection.m_strDragStartCfi == '') {
						Selection.m_strDragStartCfi = strCfi_Temp[0];
						Selection.m_strDragStartXPath = strCfi_Temp[1];
						Selection.m_strDragStartXPathOffset = strCfi_Temp[2];

						if (Selection.m_strDragEndCfi == '') {
							Selection.m_strDragEndCfi = strCfi_Temp[3];
							Selection.m_strDragEndXPath = strCfi_Temp[4];
							Selection.m_strDragEndXPathOffset = strCfi_Temp[5];
						}

						if (Selection.m_bSelectFront && Selection.m_strDragEndCfi != '') {
							if (Selection.m_strDragStartCfi == Selection.m_strDragEndCfi) {
								break;
							}
					
							res = Selection.DrawSelection(Selection.m_strDragStartCfi, Selection.m_strDragEndCfi, Selection.m_strDragColor);
							Selection.m_arrDragRect[0] = res[0]['rleft'];
							Selection.m_arrDragRect[1] = res[0]['top'];
							Selection.m_arrDragRect[2] = res[0]['width'];
							Selection.m_arrDragRect[3] = res[0]['height'];
							Selection.m_strDragText = res['SelectionText'];
				
							// Selection.m_strDragRegionJson = Selection.getDrawSelectionJson(id, res, Selection.m_strDragColor, Selection.m_strDragText);

							// if (RunaEngine.DeviceInfo.IsAndroid()) {
							// 	window.android.CreateSelectionResult(Selection.m_strDragRegionJson);
							// }
							break;
						}
						else {
							if (Selection.m_strDragStartCfi == Selection.m_strDragEndCfi) {
								break;
							}

							res = Selection.DrawSelection(Selection.m_strDragStartCfi, Selection.m_strDragEndCfi, Selection.m_strDragColor);
							Selection.m_arrDragRect[0] = res[0]['rleft'];
							Selection.m_arrDragRect[1] = res[0]['top'];
							Selection.m_arrDragRect[2] = res[0]['width'];
							Selection.m_arrDragRect[3] = res[0]['height'];
							Selection.m_strDragText = res['SelectionText'];

							// Selection.m_strDragRegionJson = Selection.getDrawSelectionJson(id, res, Selection.m_strDragColor, Selection.m_strDragText);

							// if (RunaEngine.DeviceInfo.IsAndroid()) {
							// 	window.android.CreateSelectionResult(Selection.m_strDragRegionJson);
							// }
							break;
						}
					}
					else {
						if (Selection.m_strDragStartCfi == strCfi_Temp[0] && Selection.m_strDragEndCfi == strCfi_Temp[3]) {
							// ePubEngineLogJS.L.log("cfi same so break  444");
							// break;
						}

						/**
						 * redmine refs #14501
						 * 셀렉션 영역 오류 수정
						 * by junghoon.kim
						 */
						Selection.m_strDragEndCfi = strCfi_Temp[3];
						Selection.m_strDragEndXPath = strCfi_Temp[4];
						Selection.m_strDragEndXPathOffset = strCfi_Temp[5];

						if (Selection.m_strDragStartCfi == Selection.m_strDragEndCfi) {
							// ePubEngineLogJS.L.log("cfi same so break 5555");
							break;
						}

						res = Selection.DrawSelection(Selection.m_strDragStartCfi, Selection.m_strDragEndCfi, Selection.m_strDragColor);
						Selection.m_arrDragRect[0] = res[0]['rleft'];
						Selection.m_arrDragRect[1] = res[0]['top'];
						Selection.m_arrDragRect[2] = res[0]['width'];
						Selection.m_arrDragRect[3] = res[0]['height'];
						Selection.m_strDragText = res['SelectionText'];

						// Selection.m_strDragRegionJson = Selection.getDrawSelectionJson(id, res, Selection.m_strDragColor, Selection.m_strDragText);

						// if (RunaEngine.DeviceInfo.IsAndroid()) {
						// 	window.android.CreateSelectionResult(Selection.m_strDragRegionJson);
						// }
						break;
					}
					break;
				}

				// if (Selection.m_last_evt_x == Selection.m_last_draw_x && Selection.m_last_evt_y == Selection.m_last_draw_y && Selection.m_isDragStatus != WM_DRAG_START) {
					// draw stop
					//window.setTimeout( function() {Selection.OnDrawTimer();},0);
				// }
				// else if (Selection.m_isDragStatus == WM_DRAG_START && Selection.m_nTimerCount == 1) {
					//window.setTimeout( function() {Selection.OnDrawTimer();},500);
				// }
			}
			catch (ex) {
				ePubEngineLogJS.L.log('[★Exception★][Selection.OnDrawTimer] ' + (ex.number & 0xFFFF));
				ePubEngineLogJS.L.log('[★Exception★][Selection.OnDrawTimer] ' + ex);
			}
			finally {
				isDebug = null;
				strCfi_Temp = null;
				rtnDrawSelect = null;
				isWordBoundary = null;
				Selection.m_nTimerCount--;

			}
		},

		/**
		 * selection이 되있는 상태에서 앞쪽 picker로 이동시 작동
		 * @param {*} x 
		 * @param {*} y 
		 * @param {*} wordBoundary 
		 * @returns 
		 */
		OnSelectionReStartFront: function (x, y, wordBoundary) {
			
			Selection.m_bSelect = true;
			Selection.m_isDragStatus = WM_DRAG_START;
			Selection.m_strDragStartCfi = '';
			Selection.m_strDragStartXPath = '';
			Selection.m_strDragStartXPathOffset = '';
			Selection.m_strDragText = '';
			Selection.m_strDragRegionJson = '';
			Selection.m_bSelectFront = true;

			if (x < 0 || y < 0) {
				return true;
			}

			Selection.m_last_evt_x = parseInt(x);
			Selection.m_last_evt_y = parseInt(y);
			// if (Selection.m_nTimerCount != 0 || Selection.m_bSelect == false || Selection.m_isDragStatus != WM_DRAG_START) {
			// 	return false;
			// }
			Selection.OnDrawTimer(wordBoundary);
		},
		/**
		 * selection이 되있는 상태에서 뒤쪽 picker로 이동시 작동
		 * @param {*} x 
		 * @param {*} y 
		 * @param {*} wordBoundary 
		 * @returns 
		 */
		OnSelectionReStartBack: function (x, y, wordBoundary) {
			Selection.m_bSelect = true;
			Selection.m_isDragStatus = WM_DRAG_START;
			Selection.m_strDragEndCfi = '';
			Selection.m_strDragEndXPath = '';
			Selection.m_strDragEndXPathOffset = '';
			Selection.m_strDragText = '';
			Selection.m_strDragRegionJson = '';
			Selection.m_bSelectFront = false;

			if (x < 0 || y < 0) {
				return true;
			}

			Selection.m_last_evt_x = parseInt(x);
			Selection.m_last_evt_y = parseInt(y);
			// if (Selection.m_nTimerCount != 0 || Selection.m_bSelect == false || Selection.m_isDragStatus != WM_DRAG_START) {
			// 	return false;
			// }
			Selection.OnDrawTimer();
		},
		/**
		 * x,y좌표를 받아 OnDrawTimer을 시작시킴.
		 * mousedown , touchstart 이벤트에 적용한다.
		 * @param {*} x 
		 * @param {*} y 
		 * @param {*} wordBoundary 
		 * @returns 
		 */
		OnSelectionStart: function (x, y, wordBoundary) {
			Selection.m_bSelect = true;
			Selection.m_isDragStatus = WM_DRAG_START;
			Selection.m_strDragStartCfi = '';
			Selection.m_strDragEndCfi = '';
			Selection.m_strDragText = '';
			Selection.m_strDragRegionJson = '';

			if (x < 0 || y < 0) {
				return true;
			}
			if (_use_selection_picker&&wordBoundary){
				Selection.setPickerVisible(true)
			}

			Selection.m_last_evt_x = parseInt(x);
			Selection.m_last_evt_y = parseInt(y);

			Selection.OnDrawTimer(wordBoundary);
		},
		/**
		 * x,y좌표를 받아 OnDrawTimer을 시작시킴.
		 * mousemove , touchmove 이벤트에 적용한다.
		 * @param {*} x 
		 * @param {*} y 
		 * @param {*} wordBoundary 
		 * @returns 
		 */
		OnSelectionMove: function (x, y, wordBoundary) {
			if (x < 0 ||  y < 0) {
				return true;
			}
			if (Selection.m_last_evt_x == parseFloat(x) && Selection.m_last_evt_y == parseFloat(y)) {
				return true;
			}
			
			if (Selection.viewerPadding&&_use_selection_picker){
				var point = Selection._getSelectionPoint(x,y);
				Selection.m_last_evt_x = parseInt(point.x);
				Selection.m_last_evt_y = parseInt(point.y);
				point = null;
			} else {
				var max = ele_view_wrapper.offset().left+_viewer_width-1;
				var maxh = ele_view_wrapper.offset().top+_viewer_height-1;
				Selection.m_last_evt_x = parseInt(Math.min(max , x));
				Selection.m_last_evt_y =  parseInt(Math.min(maxh , y));
				// Selection.m_last_evt_x = parseFloat(x);
				// Selection.m_last_evt_y = parseFloat(y);
			}


			if (Selection.m_nTimerCount != 0 || Selection.m_bSelect == false || Selection.m_isDragStatus != WM_DRAG_START) {
				return false;
			}

			Selection.OnDrawTimer(wordBoundary);
			// Selection.SelectionDivInfo();
			return true;
		},
		/**
		 * x,y 좌표를 받아 selection의 끝을 지정함.
		 * @param {*} x 
		 * @param {*} y 
		 * @param {*} wordBoundary 
		 * @returns 
		 */
		OnSelectionEnd: function (x, y, wordBoundary) {
			Selection.m_bSelect = false;
			Selection.m_bSelectFront = false;
			Selection.m_isDragStatus = WM_DRAG_END;

			if (x < 0 || y < 0) {
				return true;
			}
			var max = ele_view_wrapper.offset().left+_viewer_width-10;
			var maxh = ele_view_wrapper.offset().top+_viewer_height-10;
			var _x = parseInt(Math.min(max , x));
			var _y = parseInt(Math.min(maxh , y));
			Selection.m_last_evt_x = _x;
			Selection.m_last_evt_y = _y;
			max = null;
			maxh = null;
			_x = null;
			_y = null;


			var rtnCompare = Position._CheckCfiCompare(Selection.m_strDragStartCfi, Selection.m_strDragEndCfi);
			Selection.m_strDragStartCfi = rtnCompare['Big'];
			Selection.m_strDragEndCfi = rtnCompare['Small'];

			Selection.OnDrawTimer(wordBoundary);
			Selection.OnSelectionResult(x, y, wordBoundary)
			
		},
		OnSelectionResult:function(x, y, wordBoundary){
			if (ele_view_wrapper.get(0).classList.contains('none-selection')){
				ele_view_wrapper.get(0).classList.remove('none-selection')
			}
			Position.checkTurnOverPoint().then(rs=>{
				ele_view_wrapper.get(0).classList.add('none-selection')
				let range = rs.range
				let rects = range.getClientRects()
				let clone = range.cloneContents()
				let wl = Walker.getWalkerList(clone)
				let t = wl.map(e=>e.textContent).join('')
				let result = {
					detail: { text: t, length: t.length , turnPage: rs ,rects: rects}
				}
				window.dispatchEvent(new CustomEvent('bdbSelectionEnd',result))
				range = null
				rects = null
				clone = null
				wl = null
				t = null
				result = null
			}).catch(err=>{
				console.log(err)
				ele_view_wrapper.get(0).classList.add('none-selection')
			})
		},
        /**
         * picker를 숨길지 보일지.
         * @param {*} value 
         */
		 setPickerVisible:function(value){
            if (_use_selection_picker){
                if (value){
                    _selector_img_element1.style.display = 'block';
                    _selector_img_element2.style.display = 'block';
                } else {
                    _selector_img_element1.style.display = 'none';
                    _selector_img_element2.style.display = 'none';
                }
            }
        },
		/**
		 * 피커의 보이기 여부.
		 * @param {*} value 
		 */
        setPickerActive:function(value){
            if (value){
                _selector_img_element1.style.pointerEvents = 'none';
                _selector_img_element2.style.pointerEvents = 'none';
            } else {
                _selector_img_element1.style.pointerEvents = 'all';
                _selector_img_element2.style.pointerEvents = 'all';
            }
        },
		getSelectionChildNodes:function(){
			return document.getElementById(Selection.selectionContainerID)
		},
		/**
		 * 같은 라인에 있는 노드 list를 반환한다.
		 */
		getSelectionInlineNodes:function(){
			var lastChild = document.getElementById(Selection.selectionContainerID).lastChild;
			var lastChildeRects = lastChild.getClientRects();
			var childList = document.getElementById(Selection.selectionContainerID).childNodes;
			childList = Array.from(childList);
			return Array.from(childList.filter(e=>e.getBoundingClientRect().top  === lastChildeRects[0].top));
		},
		/**
		 * 샐랙션한 노드들을 반환한다.
		 * @returns 
		 */
		getSelectionNodes(isOnlyPicker){
			var container = document.querySelector('#'+Selection.selectionContainerID)
			if (container){
				if (isOnlyPicker){
					return container.querySelectorAll('[bdb-selection-picker]')
				}
				var children = container.querySelectorAll('div')
				if (children){
					return children
				}
			}
			return null
		},
		/**
		 * 샐랙션한 노드블럭의 갯수를 반환한다.
		 * @returns 
		 */
		getSelectionNodesLength(){
			var container = document.querySelector('#'+Selection.selectionContainerID)
			if (container){
				var children = container.querySelectorAll('div')
				if (children){
					return children.length
				}
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 마지막을 반환한다.
		 * @returns 
		 */
		getSelectionLastNode(){
			var nodes = Selection.getSelectionNodes()
			if (nodes){
				return nodes[nodes.length -2];
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 마지막의 rects를 반환한다.
		 * @returns 
		 */
		getSelectionLastNodeRects(){
			var last =  Selection.getSelectionLastNode()
			if (last){
				return last.getBoundingClientRect()
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 첫번째를 반환한다.
		 * @returns 
		 */
		getSelectionFirstNode(){
			var nodes = Selection.getSelectionNodes()
			if (nodes){
				return nodes[1];
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 첫번째의 rects를 반환한다.
		 * @returns 
		 */
		getSelectionFirstNodeRects(){
			var last =  Selection.getSelectionFirstNode()
			if (last){
				return last.getBoundingClientRect()
			}
			return null
		},
		/**
		 * 샐랙션된 마지막 node의 rects를 반환한다.
		 * @returns 
		 */
		getLastSelectionNodeRects:function(){
			var inlineNodes = Selection.getSelectionInlineNodes();
			var rectList = [];
			inlineNodes.forEach(e=>{
				rectList.push(e.getBoundingClientRect());
			})
		
			var rects = null;
			if (rectList.length > 0){
				rects = {left:0,top:0,width:0,height:0};
				rects.left = rectList[0].left;
				rects.top = rectList[rectList.length-1].top;
				//첫번째 left에서 마지막 right까지가 넓이.
				rects.width = (rectList.length === 1 )? rectList[0].width : rectList[rectList.length-1].right- rectList[0].left ;
				rects.height = rectList[rectList.length-1].height;
			} 

			return rects;
		},
		/**
		 * 샐랙션사이 빈공간을 채울 float값을 return
		 * @param {*} idx 
		 * @param {*} array 
		 * @param {*} wholeCount 
		 */
		getFillSelectionEmptySpace:function(idx,array,wholeCount){
		
			//마지막 아이템은 채울필요없다.
			if (idx === (wholeCount-1) ){
								
			} else {
				//이전아이템이 필요하기 때문에 0이상부터
				if (idx>0){
					var prevSize = array[idx-1]['rect'].left +array[idx-1]['rect'].width;
					var currentLeft = array[idx]['rect'].left;
					//같은 높이에 있는 애들만 비교한다.
					if (array[idx-1]['rect'].top === array[idx]['rect'].top){
						//앞쪽 아이템의 right와 뒷쪽 아이템의 left가 다르면 뺀값만큼 뒷쪽 width를 보정한다.
						if (prevSize<currentLeft){
							var emptySpace = Math.abs(currentLeft - prevSize);
							return emptySpace;
						}
					}
				}
			}
			return 0;
		},
		/**
		 * 
		 * @param {*} baseOnStart 시작 피커 기준으로 값을 리턴할것인지.
		 */
		comparePickerPosition:function(baseOnStart){
			var nodes = Selection.getSelectionNodes(true)
			if (nodes){
				var startNode = nodes[0]
				var endNode = nodes[1]
				// console.log(nodes ,startNode ,endNode)
				var startNodeRect = startNode.getBoundingClientRect()
				var endNodeRect = endNode.getBoundingClientRect()

				var result = false
				if (baseOnStart){
					if (startNodeRect.top < endNodeRect.top){
						result = true
					}
				} else {
					if (startNodeRect.top > endNodeRect.top){
						result = true
					}
				}
				return result
			}
			return null
		},
		/**
		 * 
		 * @param {*} baseOnStart 시작 피커 기준으로 값을 리턴할것인지.
		 */
		savePickerPostionTemp:function(){
			var nodes = Selection.getSelectionNodes(true)
			if (nodes){
				var startNode = nodes[0]
				var endNode = nodes[1]
				var startNodeRect = startNode.getBoundingClientRect()
				var endNodeRect = endNode.getBoundingClientRect()
				Selection.selectionPickerRestartInfo = {
					start:startNodeRect,
					end:endNodeRect
				}
			}
		},
		savePickerClickEventInfo:function(e,isLeft){
			var touch = e.touches[0] || e.changedTouches[0];
			var pickerX = touch.pageX;
			var pickerY = touch.pageY;

			var result = Selection.comparePickerPosition(isLeft)
			// console.log('isLeft => '+isLeft ,result )
			Selection.selectionPickerRestartInfo = {
				x: pickerX,
				y: pickerY,
				derectionUp: result 
			}

			touch = null
			pickerX = null
			pickerY = null
			result = null
		},
		/**
		 * cache selection
		 */
		_selection_cache:[],
		_selection_cache_delete_pending: false,
		selection_continue_mode: false,
		//selection cache 추가.
		addSelectionCache: function(){
			Selection._selection_cache.push({
				start:ePubEngineCoreJS.Selection.m_strDragStartCfi,
				end:ePubEngineCoreJS.Selection.m_strDragEndCfi,
				text:ePubEngineCoreJS.Selection.m_current_visible_selected_text,
			})
		},
		//select cache 제거
		deleteSelectionCache: function(){
			if (Selection._selection_cache.length>0){
				Selection._selection_cache = []
			}
		},
		/**
		 * 계속 샐랙션 시작
		 */
		startContinuSelection: function(){
			Selection.selection_continue_mode = true
		},
		/**
		 * 계속 샐랙션 종료
		 */
		endContinueSelection: function(){
			Selection.selection_continue_mode = false
			Selection.deleteSelectionCache()
		},


		/**
		 * sjhan 220413
		 * 연속 샐랙션시 연속 text 추출
		 * Selection.getEndCfi()값이 null이 나오는 경우가 있음..
		 * @param { String } startCfi 
		 * @param { String } endCfi 
		 * @returns 
		 */
		 getContinueSelectionText: function(startCfi , endCfi){
			// if(!Selection._selection_cache){
			// 	return null
			// }
			// if (Selection._selection_cache.length == 0){
			// 	return null
			// }

			let textResult = null
			let _start = null
			let _end = null
			let _start_cfi = null
			let _end_cfi = null
			let findFirst = false
			let cElement = null
			let textList = null
			let result = null

			try {
				/**
				 * cfi값을 파라미터로 받지 못하면 현재 잡혀있는 cfi의 시작부터 끝의 문장을 추출한다.
				 */
				if (!startCfi){
					_start = Selection._selection_cache[0].start
				} else {
					_start = startCfi
				}
				if (!endCfi){
					_end = Selection.getEndCfi()
				} else {
					_end = endCfi
				}
				
				_start_cfi = Position._decodeCFI(document , _start)
				_end_cfi = Position._decodeCFI(document , _end)
	
				if (_start_cfi && _end_cfi){
					cElement = document.getElementById('bdb-chapter' + ePubEngineLoaderJS.Data.getCurrentSpine())
					textList = Position.getTextNodesAllWithObj(cElement)
					result = []
					//시작,끝 사이에 있는 모든 textnode 추출
					for (let ele of textList){
						if (ele.node === _start_cfi.node){
							findFirst = true
						}
						if (findFirst){
							result.push(ele)
						}
						if (ele.node === _end_cfi.node){
							break;
						}
					}
					//단어 추출
					for (let i = 0; i<result.length; i++){
						let e2 = result[i]
						let txt = e2.node.textContent
						if (i === 0){
							textResult = txt.substring(_start_cfi.offset)
						} else if (i === result.length-1){
							textResult += txt.substring(0,_end_cfi.offset)
						} else {
							textResult += txt
						}
					}
				}
				return {'textResult':textResult , 'result': result, 'startCfi':_start_cfi , 'endCfi': _end_cfi}
			} catch(error){
				console.error('getContinueSelectionText',error)
			} finally {
				textResult = null
				_start = null
				_end = null
				_start_cfi = null
				_end_cfi = null
				findFirst = false
				cElement = null
				textList = null
				result = null
			}
		},
		/**
		 * selection block에 x,y좌표로부터 위치를 반환해 rects를 반환
		 * @param {*} x 
		 * @param {*} y 
		 */
		 distanceFromPoint(x,y){
			let first = Selection.getSelectionFirstNodeRects()
			let last = Selection.getSelectionLastNodeRects()
			let fDistance = Util.getDistance(x,y,first.left,first.top)
			let lDistance = Util.getDistance(x,y,last.right,last.bottom)
			if (fDistance<lDistance){
				return {isFront:true, rects:first}
			} else {
				return {isFront:false, rects:last}
			}
		},
		/**
		 * x,y point 샐랙션을 재개한다.
		 * @param {*} x 
		 * @param {*} y 
		 */
		ReSelect( x , y){
			if(!ele_view_wrapper.hasClass('selectable')) {
				ele_view_wrapper.addClass('selectable');
			}
			Selection.OnSelectionStart( x, y,true)
			if(ele_view_wrapper.hasClass('selectable')) {
				ele_view_wrapper.removeClass('selectable');
			}
		},
		/**
		 * 다음페이지 이동후 샐랙션을 한다.
		 */
		ContinueSelectionNext(){
			//연속 샐랙션 모드 on
			Selection.selectionCacheMode = 0
			//페이지 이동
			Navi.GotoPageAsync(true).then(rs=>{
				//이동후 결과값 체크
				if (typeof rs === 'object'){
					callbackPosition(rs.left, rs.right);
					//페이지 스캔후 첫 node를 반환.
					Walker.findFirst().then(rs=>{
						let correct = 1
						let rect = rs.rect
						let x1 = rect.left + correct
						let y1 = rect.top + correct
						let x2 = rect.right - correct
						let y2 = rect.bottom - correct
						Selection.ReSelect(x1,y1)
					}).catch((err)=>{
						console.error('findFirst',err)
						if (typeof err === 'string' && ( err === 'fail scan' || err === 'not found' )){
							Selection.ContinueSelectionNext()
						}
					})
				}
			}).catch((err)=>{
				console.error('GotoPageAsync',err)
			})
		}
	};

	var Highlight = Core.Highlight = {
		searchHighlightId: 'bdb_search_id',

		/**
		 * node와 string text로 특정 노드를 찾는다
		 * @param {*} node 
		 * @param {*} word 
		 * @returns 
		 */
		getSearchNodeByWord: function(node, word, forced) {
			// console.log('## getSearchNodeByWord ', node,word)
			var findInfo = null;

			if(node.nodeType == Node.ELEMENT_NODE) {

				var children = node.childNodes;

				for (var i = 0; i < children.length; i++) {
					var child = children[i];
					if (node.parentNode instanceof HTMLUnknownElement || node instanceof HTMLUnknownElement || child instanceof HTMLUnknownElement){
						continue
					}
					findInfo = Highlight.getSearchNodeByWord(child, word);

					if(findInfo) return findInfo;
				}
			}
			else if(node.nodeType == Node.TEXT_NODE) {
				var isNotPassNode = window.getComputedStyle(node.parentNode).display != 'none';

				if(isNotPassNode) {
					var text = node.textContent;
					if (forced){
						findInfo = {node: node, text: text};
						return findInfo;
					}
					if((text.indexOf(word)) != -1) {
						findInfo = {node: node, text: text};
						return findInfo;
					}
				}
			}

			return null;
		},
		/**
		 * 해당 노드에서 검색어를 찾아 cfi로 변환한다.
		 * @param {string} selector jQuery selector
		 * @param {string} word 검색어 또는 찾는 단어
		 */
		findNodeWithText: function(selector, word , forced) {
			var objNode = $(selector);
			var selnode = objNode.get(0);
			var nodeText = Highlight.getSearchNodeByWord(selnode, word , forced);
			var startPos = -1;
			var endPos = -1;
			var range = null;
			var rects = null;
			var drawList = null;
			var drawList_Idx = null;
			var k = null;
			var rect = null;
			var isDual = null;
			var rect_next = null;
			var dual_idx = null;

			try {
				if(nodeText == null) {

					return -8;
				}

				if((startPos = nodeText.text.indexOf(word)) != -1) {
					endPos = startPos + word.length;
				}

				range = document.createRange();

				range.setStart(nodeText.node, startPos);
				range.setEnd(nodeText.node, endPos);

				rects = range.getClientRects();

				drawList = new Array();
				drawList_Idx = 0;

				for (k = 0; k < rects.length; k++) {
					rect = rects[k];
					if (/*rect.top < 0 || */rect.width <= 0 || rect.height <= 0) {
						continue;
					}

					if (true) {
						isDual = false;
						for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
							// across check
							rect_next = rects[dual_idx];
							if (rect_next) {
								// across check
								if (rect.top >= rect_next.top
									&& rect.bottom <= rect_next.bottom
									&& rect.left <= rect_next.left
									&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										&& rect.bottom >= rect_next.bottom
										&& rect.left >= rect_next.left
										&& rect.right <= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										&& rect.bottom >= rect_next.bottom
										&& rect.left <= rect_next.left
										&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
							}
						}
						if (isDual) {
							continue;
						}
					}
					else {
						
					}

					if (rect.width == 0) {
				
						continue;
					}

					drawList[drawList_Idx] = new Array();
					drawList[drawList_Idx]['id'] = parseInt(k);
					drawList[drawList_Idx]['top'] = (parseInt(rect.top) + parseInt(window.scrollY)) + "px";
					drawList[drawList_Idx]['rleft'] = parseInt(rect.left);

					drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";

					drawList[drawList_Idx]['width'] = rect.width + "px";
					drawList[drawList_Idx]['height'] = rect.height + "px";


					drawList_Idx++;
				}
				

				if (drawList_Idx == 0) {
					return -8; //검색영역없음
				}

				return drawList;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][Highlight.findNodeWithText] ' + ex);
				return -100; //예외
			}
			finally {
				isDebug = null;
				strSelectionID = null;
				range = null;
				rects = null;
				drawList = null;
				drawList_Idx = null;
				k = null;
				rect = null;
				isDual = null;
				rect_next = null;
				dual_idx = null;
			}
		},

		clearSearchHighlight: function(){
			let parents = document.getElementById('viewer_highlight_layer')
			while (parents.firstChild) {
				parents.removeChild(parents.firstChild);
			}
		},
		/**
		 * rects 정보를 받아 하이라이트 해준다.
		 * @param {*} rects 
		 */
		highlightWithRects: function(rects , color , opacity){
			let parents = document.getElementById('viewer_highlight_layer')
			// while (parents.firstChild) {
			// 	parents.removeChild(parents.firstChild);
			// }

			if (rects.length === 0 ){
				return;
			}

			let mColor =  "#38a5e6"
			if (color){
				mColor = color
			}
			let _opacity = 0.4
			if (opacity){
				_opacity = opacity
			}
			
			let docf = document.createDocumentFragment()
			for (let e of rects){
				let p2 = document.createElement('div')
				p2.style.backgroundColor = mColor
				p2.style.pointerEvents = 'none'
				p2.style.position = 'absolute'
				p2.style.width = e.width + 'px'
				p2.style.height = e.height + 'px'
				p2.style.top = e.top + 'px'
				p2.style.left = e.left + 'px'
				p2.style.opacity = _opacity
				docf.appendChild(p2)
			}

			
			parents.appendChild(docf)
		},
		/**
		 * 앨리먼트와 offset을 넣으면 range를 리턴
		 * @param {*} startContainer 시작 element
		 * @param {*} startOffset 그 element의 offset start
		 * @param {*} endContainer 마지막 element
		 * @param {*} endOffset 그 마지막 elment의 offset
		 * @returns 
		 */
		getRectsFromElementWithOffset(startContainer , startOffset, endContainer, endOffset){
			let range = document.createRange();
			range.setStart(startContainer, startOffset)
			range.setEnd(endContainer, endOffset)
			return range.getClientRects();
		},
				/**
		 * 해당 노드에서 검색어를 찾아 cfi로 변환한다.
		 * @param {string} selector jQuery selector
		 * @param {string} word 검색어 또는 찾는 단어
		 */
		findNodeWithText2222: function(selector, word) {
			var objNode = $(selector);
			var selnode = objNode.get(0);
			var nodeText = Highlight.getSearchNodeByWord(selnode, word);


			/**
			 * 시작한 노드와 그 노드안에서 해당되는 문자열의 offset이 필요하다.
			 * 끝 노드와 그노드안에 해당되는 문자열 offset이 필요하다.
			 */
			//모든 노드를 가저옴
			let objNodeList = Position.getTextNodesAllWithObject(objNode.get(0))
			let objNodeListMap = objNodeList.flatMap(e=> [e.text])

			//노드를 하나의 텍스트로 정렬
			let objNodeListJoin = objNodeListMap.join("")


			//글자 시작 지점
			let findStringStartIndex = -1

			//글자 종료 시점
			let findStringEndIndex = -1


			//정렬된 텍스트안에 내가 찾고 싶은 키워드가 들어있는지 확인
			//포함
			if (objNodeListJoin.includes(word)){
				findStringStartIndex = objNodeListJoin.indexOf(word);
				findStringEndIndex = findStringStartIndex + word.length

			}
			//TODO 미포함 이전, 다음 노드안에 텍스트가 들어있을 수 있으니 한번더 조회해본다.
			else {
				let tempStr = objNodeListJoin
				let findCount = 0
				let cutEndRange = tempStr.length

				while(true){
					if (cutEndRange <= 0 ){
						break;
					}
					tempStr = tempStr.substring( 0, cutEndRange)
					cutEndRange = tempStr.length -1;
					findCount++
					if (tempStr.includes(word)){
						break;
					}
				}

	
	
				//다음태그를 찾는다.
				let nextNode = objNode.get(0).nextElementSibling
				while(true){
					if (nextNode instanceof HTMLUnknownElement){
						nextNode = nextNode.nextElementSibling
					} else {
						break
					}
				}

			

				findStringStartIndex = 0
			}
			
			//node 찾기
			let find_startObj = null
			let find_endObj = null
			if (findStringStartIndex >= 0 ){
				for(let j = 0; j<objNodeList.length; j++){
					let ob = objNodeList[j]
					//시작 text node 찾기
					if (ob.start <= findStringStartIndex && ob.end > findStringStartIndex){
						find_startObj = ob
					}

					//끝 text node 찾기
					if (ob.start <= findStringEndIndex && ob.end > findStringEndIndex){
						find_endObj = ob
						break;
					}
				}
			}

			if (find_startObj){
				nodeText = find_startObj
			}
			


			// console.log('## nodeText result!! ',word,nodeText,objNodeList,objNodeListMap,objNodeListJoin)
			var startPos = -1;
			var endPos = -1;
			var range = null;
			var rects = null;
			var drawList = null;
			var drawList_Idx = null;
			var k = null;
			var rect = null;
			var isDual = null;
			var rect_next = null;
			var dual_idx = null;

			try {

				if(nodeText == null) return -8;

				if((startPos = nodeText.text.indexOf(word)) != -1) {
					endPos = startPos + word.length;
				}

				//시작 지점
				if (find_startObj){
					// console.log('## 찾음',find_startObj.node , find_endObj.node)
					let _start_idx = findStringStartIndex - find_startObj.start
					startPos = _start_idx

					//같은 노드
					// if (find_startObj.node == find_endObj.node){
					// 	// console.log('## 같은 노드임')
					// 	let _end_idx  = find_startObj.end - find_startObj.start
					// 	endPos = _end_idx
					// }
				}

				range = document.createRange();

				range.setStart(nodeText.node, startPos);
				if (find_endObj){
					range.setEnd(find_endObj.node, find_endObj.node.textContent.length);
				} else {
					if (find_startObj){
						range.setEnd(find_startObj.node, find_startObj.node.textContent.length);
					} else {
						range.setEnd(nodeText.node, endPos);
					}
					
				}
				

				rects = range.getClientRects();

				Highlight.highlightWithRects(rects)

				drawList = new Array();
				drawList_Idx = 0;

				for (k = 0; k < rects.length; k++) {
					rect = rects[k];
					if (/*rect.top < 0 || */rect.width <= 0 || rect.height <= 0) {
						continue;
					}

					if (true) {
						isDual = false;
						for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
							// across check
							rect_next = rects[dual_idx];
							if (rect_next) {
								// across check
								if (rect.top >= rect_next.top
									&& rect.bottom <= rect_next.bottom
									&& rect.left <= rect_next.left
									&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										&& rect.bottom >= rect_next.bottom
										&& rect.left >= rect_next.left
										&& rect.right <= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										&& rect.bottom >= rect_next.bottom
										&& rect.left <= rect_next.left
										&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
							}
						}
						if (isDual) {
							continue;
						}
					}
					else {
						
					}

					if (rect.width == 0) {
						continue;
					}

					drawList[drawList_Idx] = new Array();
					drawList[drawList_Idx]['id'] = parseInt(k);
					drawList[drawList_Idx]['top'] = parseFloat(rect.top) + parseFloat(window.scrollY);
					drawList[drawList_Idx]['rleft'] = parseFloat(rect.left);
					drawList[drawList_Idx]['left'] = parseFloat(rect.left) + parseFloat(window.scrollX);
					drawList[drawList_Idx]['width'] = rect.width;
					drawList[drawList_Idx]['height'] = rect.height;
					drawList_Idx++;
				}
				

				if (drawList_Idx == 0) {
					return -8; //검색영역없음
				}

				return drawList;
			}
			catch (ex) {
				console.error(ex)
				ePubEngineLogJS.W.log('[★Exception★][Highlight.findNodeWithText] ' + ex);
				return -100; //예외
			}
			finally {
				isDebug = null;
				strSelectionID = null;
				range = null;
				rects = null;
				drawList = null;
				drawList_Idx = null;
				k = null;
				rect = null;
				isDual = null;
				rect_next = null;
				dual_idx = null;
				// //sjhan 임시코드..
				// setTimeout(() => {
				// 	Layout._onScrollRequest = false;	
				// }, 500);
			}
		},
		/**
		 * 해당 셀렉터의 영역을 찾는다.
		 * @param {string} selector JQuerySelector
		 * @returns -8 or -100 or rect of array
		 */
		findNode: function(selector) {
			var objNode = $(selector);
			var selnode = objNode.get(0);
			var range = null;
			var rects = null;
			var drawList = null;
			var drawList_Idx = null;
			var k = null;
			var rect = null;
			var isDual = null;
			var rect_next = null;
			var dual_idx = null;

			try {

				range = document.createRange();

				range.selectNode(selnode);
				
				rects = range.getClientRects();

				drawList = new Array();
				drawList_Idx = 0;

				for (k = 0; k < rects.length; k++) {
					rect = rects[k];
					if (/*rect.top < 0 || */rect.width <= 0 || rect.height <= 0) {
						continue;
					}

					if (true) {
						isDual = false;
						for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
							// across check
							rect_next = rects[dual_idx];
							if (rect_next) {
								// across check
								if (rect.top >= rect_next.top
									&& rect.bottom <= rect_next.bottom
									&& rect.left <= rect_next.left
									&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										&& rect.bottom >= rect_next.bottom
										&& rect.left >= rect_next.left
										&& rect.right <= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										&& rect.bottom >= rect_next.bottom
										&& rect.left <= rect_next.left
										&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
							}
						}
						if (isDual) {
							continue;
						}
					}
					else {
						
					}

					if (rect.width == 0) {

						continue;
					}

					drawList[drawList_Idx] = new Array();
					drawList[drawList_Idx]['id'] = parseInt(k);
					drawList[drawList_Idx]['top'] = (parseInt(rect.top) + parseInt(window.scrollY)) + "px";
					drawList[drawList_Idx]['rleft'] = parseInt(rect.left);

					drawList[drawList_Idx]['left'] = (parseInt(rect.left) + parseInt(window.scrollX)) + "px";

					drawList[drawList_Idx]['width'] = rect.width + "px";
					drawList[drawList_Idx]['height'] = rect.height + "px";


					drawList_Idx++;
				}
				

				if (drawList_Idx == 0) {
					return -8; //검색영역없음
				}

				return drawList;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][Highlight.findNodeWithText] ' + ex);
				return -100; //예외
			}
			finally {
				isDebug = null;
				strSelectionID = null;
				range = null;
				rects = null;
				drawList = null;
				drawList_Idx = null;
				k = null;
				rect = null;
				isDual = null;
				rect_next = null;
				dual_idx = null;
			}
		},
		/**
		 * 
		 * @param {*} selector 
		 * @param {*} word 
		 * @returns 
		 */
		getSearchHighlightInfo: function(selector, word) {
			// return this.findNodeWithText(selector, word);
			return this.findNodeWithText2222(selector, word);
		},
		//하이라이트 색깔
		_search_highlight_hex_color: '#38a5e6',
		//하이라이트 opacity
		_search_highlight_opacity: 0.22,
		//element holder
		_highlight_div_ele: null,
		/**
		 * 검색어를 하이라이트한다.
		 * @param {*} selector 
		 * @param {*} word 
		 * @param {*} color 
		 * @returns 
		 */
		createSearchHighlight: function(selector, word, textNode) {

			this.findNodeWithText2222(selector, word, textNode)

			return;
			var isDebug = false;
			var rtnCompare = null;
			var rtnDraw = null;
			var nRtnDraw_Cnt = null;
			var dl_idx = null;
			var re = null;
			var se = null;
			var result = new Array();
			var isRes = false;
			var baseLeft = parseFloat(ele_view_wrapper.offset().left);
			var baseTop = 0;
			let docf = document.createDocumentFragment()
			try {
				let searchElementRects = $(selector).get(0).getClientRects()
				let searchEleIndex = searchElementRects.length-1
				let searchPos = null
				let _pos = null
				let info  = null
				if (searchEleIndex >= 0){
					if (Layout.getScrollMode()){
						
						// searchPos = searchElementRects[searchEleIndex].top
						// _pos = ele_view_wrapper.get(0).scrollTop + searchPos
						// info = Layout.getCurrentPageInfo(_pos)
						//검색에서 계산시 벗어나부분을 보정해준다.
						// if (searchPos < 0){
							// console.error("검색에서 계산시 벗어나부분을 보정해준다.")
							// Navi.GotoPageByPercentAsync(info.info.percent)
						// } 
					} else {
						searchPos = searchElementRects[searchEleIndex].left
						_pos = ele_view_wrapper.get(0).scrollLeft + searchPos
						info = Layout.getCurrentPageInfo(_pos)
						//검색에서 계산시 벗어나부분을 보정해준다.
						if (searchPos < 0){
							console.error("검색에서 계산시 벗어나부분을 보정해준다.")
							Navi.GotoPageByPercentAsync(info.info.percent)
						} 
					}

				}
				searchElementRects = null
				searchEleIndex = null
				searchLeft = null
				_pos = null
				info = null

			
				Highlight.deleteSearchHighlight();

				var rtnDraw = this.findNodeWithText(selector, word);

				if (rtnDraw.constructor == Array) {
					nRtnDraw_Cnt = rtnDraw.length;
					for (dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
						re = Highlight.getSelectionElement(
							'Rue_Highlight_Layer_' + Highlight.searchHighlightId + '_' + rtnDraw[dl_idx]['id'].toString(),
							rtnDraw[dl_idx]['width'].toString(),
							rtnDraw[dl_idx]['height'].toString(),
							parseFloat(rtnDraw[dl_idx]['top'].toString(), 10) - baseTop,
							parseFloat(rtnDraw[dl_idx]['left'].toString(), 10) - Selection.viewerPadding.left + Selection.viewerPadding.windowLeftRelation
						)
						docf.appendChild(re)
						re = null;
					}
					document.getElementById('viewer_highlight_layer').appendChild(docf);
					result['res'] = 0;
					return result; //success
				} 
				
				else {
					if (textNode){
						if (textNode.textContent){
							rtnDraw = this.findNodeWithText(selector, textNode.textContent);
							if (rtnDraw.constructor == Array) {
								nRtnDraw_Cnt = rtnDraw.length;
								for (dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
									re = Highlight.getSelectionElement(
										'Rue_Highlight_Layer_' + Highlight.searchHighlightId + '_' + rtnDraw[dl_idx]['id'].toString(),
										rtnDraw[dl_idx]['width'].toString(),
										rtnDraw[dl_idx]['height'].toString(),
										parseFloat(rtnDraw[dl_idx]['top'].toString(), 10) - baseTop,
										parseFloat(rtnDraw[dl_idx]['left'].toString(), 10) - Selection.viewerPadding.left + Selection.viewerPadding.windowLeftRelation
									)
									docf.appendChild(re)
									re = null;
								}
								document.getElementById('viewer_highlight_layer').appendChild(docf);
								result['res'] = 0;
								return result; //success
							}
						}
					}
				}
			}
			catch(ex) {
				ePubEngineLogJS.W.log(ex);
			}
			finally {
				isDebug = null;
				rtnCompare = null;
				rtnDraw = null;
				nRtnDraw_Cnt = null;
				dl_idx = null;
				re = null;
				se = null;
				docf = null;
			}
		},
		/**
		 * 검색이동후 하이라이트 된 부분을 제거
		 */
		deleteSearchHighlight: function() {
			var isDebug = false;
			var sel = document.getElementById('viewer_highlight_layer');
			var i = null;

			try {
				while (sel.firstChild) {
					sel.removeChild(sel.firstChild);
				}
				for (i = 0; i < sel.childNodes.length; i++) {
					if (sel.childNodes[i].id != undefined && sel.childNodes[i].id.indexOf('Rue_Highlight_Layer_') == 0) {
						sel.childNodes[i].onclick = null;
						sel.removeChild(sel.childNodes[i]);
						i--;
					}
				}
			}
			finally {
				isDebug = null;
				sel = null;
				i = null;
			}
		},
		/**
		 * 하이라이트를 위한 엘리먼트 반환
		 * @param {*} id unique 아이디
		 * @param {*} width 엘리먼트 넓이 지정
		 * @param {*} height 엘리먼트 높이 지정
		 * @param {*} top absolute기준 상단
		 * @param {*} left absolute기준 좌측
		 * @returns 
		 */
		getSelectionElement:function(id, width, height, top, left){
			if (!Highlight._highlight_div_ele){
				const div = document.createElement("div");
				div.style.zIndex = 0;
				div.style.position = "absolute";
				if (Highlight._search_highlight_hex_color == "UNDERLINE") {
					div.style.borderBottom = "thick solid #FF0000";
					div.style.borderBottomWidth = "2px";
					div.style.paddingBottom = "2px";
					div.style.backgroundColor = "";
					div.style.opacity = "1";
				}
				//hex color
				else if (Highlight._search_highlight_hex_color.startsWith('#')) {
					div.style.borderBottom = "";
					div.style.borderBottomWidth = "";
					div.style.paddingBottom = "";
					div.style.backgroundColor = Highlight._search_highlight_hex_color;
					div.style.opacity = Highlight._search_highlight_opacity;
				}
				Highlight._highlight_div_ele = div
			}
			let result = Highlight._highlight_div_ele.cloneNode(true)
			result.id = id
			if (typeof width === 'string'){
				result.style.width = width
			} else {
				result.style.width = width + 'px'
			}
			if (typeof height === 'string'){
				result.style.height = height
			} else {
				result.style.height = height + 'px'
			}
			result.style.top = top + 'px'
			result.style.left = left + 'px'
			return result
		}
	};

	var Info = Core.Info = {
		/**
		 * 좌표로 사용자 상호작용이 가능한 element인지 확인한다.
		 * @param {*} posX 
		 * @param {*} posY 
		 * @returns 
		 */
		GetHitTest: function (posX, posY) {
			var selnode = document.elementFromPoint(posX, posY);
			var hasInteractive = false;

			try {

				hasInteractive = this._CheckInteractive(selnode);

				return hasInteractive;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info.GetHitTest] ' + ex);
				return false;
			}
		},
		/**
		 * 좌표로 해당 이미지의 src를 추출한다.
		 * @param {*} posX 
		 * @param {*} posY 
		 * @returns 
		 */
		GetImageSrc: function (posX, posY) {
			var selnode = document.elementFromPoint(posX, posY);
			var imgpath = '';
			var selnodename = '';

			try {

				if (selnode != null) {
					selnodename = this._NodeName(selnode);

					if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
						return '';
					}

					if (selnode == ele_view_wrapper.get(0)) {
						return '';
					}

					// if (selnodename == "img") {
					// 	if(selnode.src.split("/content")[1])
					// 		selnode.src = "data:image/jpeg;base64," + Layout.syncSteg(baseInfo.token , selnode.src.split("10.200.10.10/content")[1]);
					// 	imgpath = selnode.src;
					// }
				}

				return imgpath;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info.GetImageSrc] ' + ex);
				return '';
			}
			finally {
				selnode = null;
				selnodename = null;
				imgpath = null;
			}
		},
		/**
		 * 해당노드의 이름을 return
		 * @param {*} node 
		 * @returns 
		 */
		_NodeName: function (node) {
			if (node == null) {
				return null;
			}

			var nodename = node.nodeName;

			if (nodename != null) {
				if (nodename != null) {
					nodename = nodename.toLowerCase();
				}
			}
			return nodename;
		},
		/**
		 * 유저 interaction이 가능한 node인지 확인한다.
		 * @param {*} node 
		 * @returns {Boolean}
		 */
		_CheckInteractive: function (node) {
			var selnodename = '';
			var selnode = node;
			var hasInteractive = false;
			var pselnode = null;
			var pselnodename = '';

			try {

				while (selnode != null || !hasInteractive) {
					selnodename = this._NodeName(selnode);

					if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
						break;
					}

					if (selnode == ele_view_wrapper.get(0)) {
						ePubEngineLogJS.W.log('[★Exception★][Info._CheckInteractive] -- '+ selnode.id);
						break;
					}

					if (selnodename == 'a') {
						var c = selnode.getAttribute("href");

						if (c != null) {
							c = c.toLowerCase();
							c = c.replace(/\s/g, "");

							if (c != "#" && c != "javascript:void(0)"
								&& c != "javascript:void(0);") {
								return true;
							}
						}
					}
					if (selnodename == "audio" || selnodename == "video" || selnodename == "embed" || selnodename == "object" || selnodename == "canvas") {
						// ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] -- multimedia');
						return true;
					}

					if (selnode.onmouseup != null || selnode.onclick != null || selnode.ondblclick != null || selnode.ontouchstart != null || selnode.ontouchmove != null || selnode.ontouchend != null) {
						//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] -- onclick');
						return true;
					}

					var nodescrollstyle = document.defaultView.getComputedStyle(selnode, null);

					if (nodescrollstyle != null) {
						if (nodescrollstyle["overflow-x"] == "scroll" || nodescrollstyle["overflow-x"] == "auto") {
							if (selnode.scrollWidth > selnode.clientWidth) {
								hasInteractive = true;
								//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] 22');
								return hasInteractive;
							}
						}

						if (!hasInteractive
							&& (nodescrollstyle["overflow-y"] == "scroll" || nodescrollstyle["overflow-y"] == "auto")) {
							if (selnode.scrollHeight > selnode.clientHeight) {
								hasInteractive = true;
								//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] 33 scrollHeight = ' + selnode.scrollHeight + ' clientHeight =' + selnode.clientHeight);
								return hasInteractive;
							}
						}
					}


					if (!hasInteractive) {
						try {
							if ($(selnode).data("events")) {
								hasInteractive = true;
								return hasInteractive;
							}
						} catch (err) { }
					}

					// over jQuery 1.8
					if (!hasInteractive) {
						try {
							if ($._data(selnode, "events")) {
								hasInteractive = true;
								return hasInteractive;
							}
						} catch (err) { }
					}


					if (selnodename == "img") {
						if (selnode.parentNode != null) {
							pselnode = selnode.parentNode;
							pselnodename = this._NodeName(pselnode);

							if (pselnodename == 'a') {
								var c = pselnode.getAttribute("href");

								if (c != null) {
									c = c.toLowerCase();
									c = c.replace(/\s/g, "");

									//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] a tag -- '+ c);

									if (c != "#" && c != "javascript:void(0)"
										&& c != "javascript:void(0);") {
										return true;
									}
								}
							}
						}
					}


					if (selnode.parentNode != null) {
						hasInteractive = this._CheckInteractive(selnode.parentNode);

						if (hasInteractive)
							return hasInteractive;

					}

					if (selnode.children.length > 0 && selnodename == "div") {
						// for (var i = selnode.children.length - 1; i >= 0; i--) {
						//     hasInteractive = this._CheckInteractive(selnode.children[i]);

						//     ePubEngineLogJS.L.log('[*********][Info._CheckInteractive] -- '+ selnode.children[i] +' , '+ selnode.children[i].innerHTML);

						//     if(hasInteractive)
						//         return hasInteractive;
						// }
						break;
					}
					else {
						break;
					}

				}

				return hasInteractive;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info._CheckInteractive] ' + ex);
				return false;
			}
			finally {
				selnodename = '';
				selnode = null;
				pselnode = null;
				pselnodename = '';
			}

		},
		/**
		 * x,y좌표로 href값을 추출한다.
		 * @param {*} posX 
		 * @param {*} posY 
		 * @returns 
		 */
		GetHrefInfo: function (posX, posY) {

			//Highlight.HideAll();

			var selnode = document.elementFromPoint(posX, posY);
			var strHrefInfo = '';
			var caretRangeStart = null;
			var caretRangeEnd = null;
			var range = null;
			var rangeRect = null;
			var documentFragment = null;

			try {
				strHrefInfo = this._GetHrefInfo(selnode);

				return strHrefInfo;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info.GetHrefInfo] ' + ex);
				return '';
			}
			finally {
				caretRangeStart = null;
				caretRangeEnd = null;
				strHrefInfo = '';
				selnode = null;
				//Highlight.ShowAll();
			}
		},
		/**
		 * node로 href값을 추출한다.
		 * @param {*} node 
		 * @returns 
		 */
		_GetHrefInfo: function (node) {
			var selnodename = '';
			var selnode = node;
			var strHrefInfo = '';
			var pselnode = null;
			var pselnodename = '';

			try {

				while (selnode != null || strHrefInfo != '') {
					selnodename = this._NodeName(selnode);

					//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] -- '+ selnode +', '+selnodename +', '+ selnode.innerHTML);

					if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
						break;
					}

					if (selnode == ele_view_wrapper.get(0)) {
						//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] -- '+ selnode.id);
						break;
					}

					if (selnodename == 'a') {
						var c = selnode.getAttribute("href");
						var d = selnode.getAttribute("href");

						if (c != null) {
							c = c.toLowerCase();
							c = c.replace(/\s/g, "");

							//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] a tag -- '+ c);

							if (c != "#" && c != "javascript:void(0)"
								&& c != "javascript:void(0);") {
								return d;
							}
						}
					}

					if (selnodename == "img") {
						if (selnode.parentNode != null) {
							pselnode = selnode.parentNode;
							pselnodename = this._NodeName(pselnode);

							if (pselnodename == 'a') {
								var c = pselnode.getAttribute("href");
								var d = selnode.getAttribute("href");

								if (c != null) {
									c = c.toLowerCase();
									c = c.replace(/\s/g, "");

									//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] a tag -- '+ c);

									if (c != "#" && c != "javascript:void(0)"
										&& c != "javascript:void(0);") {
										return d;
									}
								}
							}
						}
					}


					if (selnode.parentNode != null) {
						strHrefInfo = this._GetHrefInfo(selnode.parentNode);
					}

					break;

				}

				return strHrefInfo;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info._GetHrefInfo] ' + ex);
				return '';
			}
			finally {
				selnodename = '';
				selnode = null;
				pselnode = null;
				pselnodename = '';
				strHrefInfo = '';
			}

		},
	};


	const Audio = Core.Audio = {
		/**
		 * 오디오 태그가 존재하는지 체크
		 * @returns 
		 */
		checkAudioTag: function(){
			let b = null;
			let a = null;
			let A = null;
			let B = null;
			let isColl = false;
			let isColl2 = false;
			try {
				if (!ePubEngineLoaderJS.Data.$audioTag || !ele_view_wrapper.get(0)){
					return;
				}
				b = ele_view_wrapper.get(0).getBoundingClientRect();
				a = ePubEngineLoaderJS.Data.$audioTag.getBoundingClientRect();
				A = {'x':0,'y':0,'w':0,'h':0}
				A.x = b.left
				A.y = b.top
				A.w = b.width
				A.h = b.height
				B = {'x':0,'y':0,'w':0,'h':0}
				B.x = a.left
				B.y = a.top
				B.w = a.width
				B.h = a.height
				isColl = Annotation.intersectRect(A,B)
				isColl2 = Annotation.intersectRect(B,A)
				if (isColl && isColl2){
					ePubEngineLoaderJS.Data.onAudioCallback('appear',ePubEngineLoaderJS.Data.$audioTag);
				} else {
					ePubEngineLoaderJS.Data.onAudioCallback('disappear',ePubEngineLoaderJS.Data.$audioTag);
				}
				return isColl && isColl2
			} catch(err){

			} finally {
				b = null;
				a = null;
				A = null;
				B = null;
				isColl = null;
				isColl2 = null;
			}
		},
		
	}
	 /**
	 *  SMIL
	 *  @description https://www.w3.org/publishing/epub3/epub-mediaoverlays.html
	 *  
	 */
	 const SMIL = Core.SMIL = {
		 /**
		  * @property media:active-class
		  */
		 activeClass: null,
		 /**
		  * @property media:playback-active-class
		  */
		 playbackClass: null,
		 /**
		  * [
		  * {
		  * fileno: 1, 
		  * seq: [
		  * 	audio: "", audio src
		  * 	begin: "", start time
		  * 	end: "", end time
		  * 	text: "", // id selector
		  * ]},
		  * ]
		  */
		 items: [],
		 /**
		  * last file number
		  */
		 lastFileNo: null,
		 /**
		  * callback interface
		  */
		 callbackSmilFunc: null,
		 /**
		  * HLS 객체
		  */
		 hls: null,
		 /**
		  *  audio Element 객체
		  */
		 eleHLS: null,
		 /**
		  * 재생중인 아이템
		  */
		 currentItem: null,
		 /**
		  * 재생중인 파일번호
		  */
		 currentFileno: -1,
		 
		 _InProgress: false,
		 _isMoved: false,
		 /**
		  * SMIL 데이터 초기화
		  * @param {object} smilInfo SMIL 정보
		  * @param {function(type, data)} callback SMIL callback function
		  */
		 setup: function (smilInfo, callback) {
			 if (smilInfo) {
				 SMIL.activeClass = smilInfo.activeclass;
				 SMIL.playbackClass = smilInfo.playbackactiveclass;
				 for (let item of smilInfo.items) {
					 for (let s of item.seq) {
						 s.audio = ePubEngineLoaderJS.Util.replaceAll(s.audio, "{CONTENT_DOMAIN}", ePubEngineLoaderJS.Data.getBaseUrl());
						 SMIL.items.push(Object.assign(s, { fileno: item.fileno }))
					 }
				 }
				 SMIL.lastFileNo = Math.max.apply(Math, SMIL.items.map(o => o.fileno));
				 SMIL.callbackSmilFunc = callback;
				 SMIL.hls = new Hls({ liveMaxBackBufferLength: 10 });
				 SMIL.eleHLS = document.createElement('audio');
				 SMIL._bindListener();
			 }
		 },
		 /**
		  * 현재 파일에 smil 파일이 존재하는지 여부를 return
		  * @param {*} fileno 
		  * @returns 
		  */
		 checkSmil: function (leftInfo,rightInfo) {

			let fileno = -1;
			fileno = leftInfo.fileno;

			 if (SMIL.items.length === 0) {
				 return;
			 }
			 if (SMIL.currentFileno === fileno) {
				 let fitems = SMIL.items.filter(e => e.fileno === fileno);
				 if (fitems && fitems.length > 0) {
					 SMIL.currentItem = fitems[0];
				 } else {
					 SMIL.currentItem = null;
					 SMIL.eleHLS.currentTime = 0;
				 }
				 if (SMIL.eleHLS.currentTime >= SMIL.currentItem.begin && SMIL.eleHLS.currentTime < SMIL.currentItem.end) {

				 } else {
					 SMIL.eleHLS.currentTime = SMIL.currentItem.begin
				 }
			 } else {
				 SMIL.currentFileno = fileno;
				 let fitems = SMIL.items.filter(e => e.fileno === fileno);
				 if (fitems && fitems.length > 0) {
					 SMIL.currentItem = fitems[0];
					 SMIL.eleHLS.currentTime = SMIL.currentItem.begin
				 } else {
					 SMIL.currentItem = null;
					 SMIL.eleHLS.currentTime = 0;
				 }
			 }

			 if (!SMIL.currentItem) {
				 if (SMIL.callbackSmilFunc) {
					 SMIL.callbackSmilFunc('disappear', SMIL.eleHLS);
				 }
				 return;
			 } else {
				 if (SMIL.callbackSmilFunc) {
					 SMIL.callbackSmilFunc('appear', SMIL.eleHLS);
				 }
			 }

			 //재생중
			 if (SMIL.isPlaying()) {
				 if (SMIL._isMoved) {
					 SMIL.eleHLS.play();
				 }
				 // console.log('재생중',SMIL._isMoved)
			 }
			 //재생중이 아닐때 리셋
			 else {
				 //파일 변경
				 if (SMIL.eleHLS.getAttribute('hlsroot') != SMIL.currentItem.audio) {
					 SMIL.reset();
				 }
			 }
			 console.log(SMIL.currentFileno, SMIL.currentItem);
		 },

		 /**
		  * 정지
		  */
		 stop: function () {
			 let fitems = SMIL.items.filter(e => e.fileno === SMIL.currentFileno);
			 if (fitems && fitems.length > 0) {
				 SMIL.currentItem = fitems[0];
				 SMIL.eleHLS.currentTime = SMIL.currentItem.begin
			 }

			 SMIL.eleHLS.pause();
			 SMIL.clearHighlight();
		 },


		 /**
		  * audio의 현재 상태를 업데이트 시킨다.
		  * @returns 
		  */
		 update: function () {
			 if (SMIL.isPlaying()) {
				 let idx = SMIL._getIndexFromCurrentTime();
				 if (idx >= 0) {
					 if (SMIL.currentItem) {
						 if (SMIL.currentItem != SMIL.items[idx]) {
							 if (SMIL.currentFileno != SMIL.items[idx].fileno) {
								 if (SMIL._isMoved) {
									 console.error('파일번호 변경!', "이동중");
									 return;
								 }
								 SMIL._isMoved = true;
								 Navi.GotoPageByIndex(SMIL.items[idx].fileno)
							 } else {
								 SMIL.currentItem = SMIL.items[idx];
							 }
						 } else {
							 SMIL.currentItem = SMIL.items[idx];
						 }
					 } else {
						 SMIL.currentItem = SMIL.items[idx];
					 }
				 } else {
					 console.error('not found time')
				 }
				 SMIL.updateHighlight();
			 } else {
				 SMIL.clearHighlight();
			 }
		 },
		 /**
		  * 현재 item 기준으로 하이라이트를 업데이트한다.
		  * @returns 
		  */
		 updateHighlight: function () {
			 let flist = null;
			 let chapterElement = null;
			 let doc = null;
			 let target = null;
			 try {
				 if (SMIL.currentFileno < 0) {
					 return false
				 }
				 flist = SMIL.items.filter(e => e.fileno === SMIL.currentFileno);
				 chapterElement = document.querySelector(`#bdb-chapter${SMIL.currentFileno}`)
				 if (!chapterElement) {
					 return false;
				 }
				 //active class
				 if (!SMIL.currentItem || !SMIL.currentItem.text) {
					 return false;
				 }
				 //clear all class
				 target = chapterElement.querySelector('#' + SMIL.currentItem.text);
				 if (target) {
					 requestAnimationFrame(function () {
						 for (let e of flist) {
							 let ele = doc.getElementById(e.text)
							 if (target === ele) {
								 if (target.classList) {
									 if (!target.classList.contains(SMIL.activeClass)) {
										 target.classList.add(SMIL.activeClass);
									 }
								 } else {
									 target.classList.add(SMIL.activeClass);
								 }
							 } else {
								 ele.classList.remove(SMIL.activeClass)
							 }
						 }

					 })
				 }

				 return true;
			 } catch (e) {
				 console.error(e);
			 } finally {
				 //  flist = null;
				 //  iframe = null;
				 //  doc = null;
				 //  target = null;
			 }
		 },
		 /**
		  * 하이라이트 부분을 제거한다.
		  * @returns 
		  */
		 clearHighlight: function () {
			 let flist = null;
			 let chapterElement = null;
			 let doc = null;
			 let target = null;
			 try {
				 if (SMIL.currentFileno < 0) {
					 return false
				 }
				 flist = SMIL.items.filter(e => e.fileno === SMIL.currentFileno);
				 chapterElement = document.querySelector(`#bdb-chapter${SMIL.currentFileno}`)
				 if (!chapterElement) {
					 return false;
				 }
				 //active class
				 if (!SMIL.currentItem || !SMIL.currentItem.text) {
					 return false;
				 }
				 //clear all class
				 target = chapterElement.querySelector('#' + SMIL.currentItem.text);
				 if (target) {
					 for (let e of flist) {
						 let ele = doc.getElementById(e.text)
						 if (ele && ele.classList) {
							 ele.classList.remove(SMIL.activeClass)
						 }
					 }
				 }

				 return true;
			 } catch (e) {

			 } finally {
				 flist = null;
				 chapterElement = null;
				 doc = null;
				 target = null;
			 }
		 },
		 /**
		  * 오디오를 초기화 한다.
		  * @returns 
		  */
		 reset: function () {
			 if (SMIL._InProgress) {
				 if (SMIL.callbackSmilFunc) {
					 SMIL.callbackSmilFunc('inprogress', SMIL.eleHLS)
				 }
				 return;
			 }
			 SMIL._InProgress = true;
			 if (SMIL.eleHLS) {
				 SMIL.eleHLS.pause();
				 SMIL.eleHLS.currentTime = 0;
			 }
			 let item = SMIL.currentItem;
			 if (!item) {
				 SMIL._InProgress = false;
				 return false;
			 }

			 let before = SMIL.eleHLS.getAttribute('hlsroot');
			 if (before != item.audio) {
				 SMIL.eleHLS.setAttribute('hlsroot', item.audio);
				 if (Hls.isSupported()) {
					 if (item.audio.indexOf('blob:') == -1) {
						 SMIL.hls.loadSource(item.audio);
					 }
				 } else if (SMIL.eleHLS.canPlayType('application/vnd.apple.mpegurl')) {
					 SMIL.eleHLS.src = item.audio
				 }
			 } else {
				 console.log('same SRC')
				 SMIL._InProgress = false;
			 }
		 },
		 /**
		  * 재생상태를 체크한다.
		  * @returns 
		  */
		 isPlaying: function () {
			 if (!SMIL.eleHLS) {
				 return false
			 }
			 if (!SMIL.eleHLS.paused) {
				 return true;
			 } else {
				 return false;
			 }
		 },
		 /**
		  * 재생하거나 reset하는 기능
		  * @param {*} fileno 
		  * @returns 
		  */
		 playOrPause: function (fileno) {
			 if (SMIL._InProgress) {
				 if (SMIL.callbackSmilFunc) {
					 SMIL.callbackSmilFunc('inprogress', SMIL.eleHLS)
				 }
				 return false;
			 }
			 if (typeof fileno === 'number') {
				 if (SMIL.currentFileno != fileno) {
					 SMIL.currentFileno = fileno
				 }
			 }
			 if (SMIL.currentItem) {
				 if (SMIL.eleHLS.getAttribute('hlsroot') != SMIL.currentItem.audio) {
					 SMIL.reset();
				 } else {
					 SMIL._InProgress = true;
					 if (SMIL.isPlaying()) {
						 SMIL.eleHLS.pause()
						 SMIL._InProgress = false;
					 } else {
						 SMIL.eleHLS.play().then(rs => {
							 SMIL._InProgress = false;
						 }).catch(e => {
							 SMIL._InProgress = false;
						 });
					 }
				 }
			 }
			 else {
				 SMIL.reset();
			 }
		 },
		 pause: function () {
			 if (SMIL.isPlaying()) {
				 SMIL.eleHLS.pause()
			 }
		 },
		 /**
		  * 
		  * @param {*} audio 시간 점프
		  */
		 seekTo: function (time) {
			 if (typeof time === 'string') {
				 time = parseInt(time);
			 }
			 if (typeof time != 'number') {
				 return;
			 }

			 let src = SMIL.eleHLS.getAttribute('hlsroot')
			 let item = SMIL._getItemFromTime(src, time);
			 if (item) {
				 if (SMIL.currentFileno != item.fileno) {
					 // SMIL._isMoved = true;
					 SMIL.currentItem = item;
					 SMIL.eleHLS.currentTime = time;
					 Navi.GotoPageByPercent(item.fileno);
				 } else {
					 SMIL.currentItem = item;
					 SMIL.eleHLS.currentTime = time;
					 SMIL.currentFileno = SMIL.currentItem.fileno;
				 }
			 } else {
				 console.error('seekTo not found item');
			 }
		 },
		 /**
		  * hls 및 audio 리스너 바인드
		  */
		 _bindListener: function () {
			 if (Hls.isSupported()) {
				 SMIL.hls.attachMedia(SMIL.eleHLS);
				 //  SMIL.hls.on(Hls.Events.MEDIA_ATTACHED, function () {

				 //  })
				 //  SMIL.hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {

				 //  })
				 SMIL.hls.on(Hls.Events.ERROR, function (event, data) {
					 console.error('[SMIL][ERROR]', event, data);
				 })
				 if (typeof SMIL.callbackSmilFunc === 'function') {
					 SMIL.hls.on(Hls.Events.MEDIA_ATTACHED, SMIL.callbackSmilFunc.bind(this, Hls.Events.MEDIA_ATTACHED, SMIL.eleHLS));
					 SMIL.hls.on(Hls.Events.MANIFEST_PARSED, SMIL.callbackSmilFunc.bind(this, Hls.Events.MANIFEST_PARSED, SMIL.eleHLS));
					 SMIL.hls.on(Hls.Events.ERROR, SMIL.callbackSmilFunc.bind(this, Hls.Events.ERROR, SMIL.eleHLS));
				 }
			 } else if (SMIL.eleHLS.canPlayType('application/vnd.apple.mpegurl')) {
				 SMIL.eleHLS.addEventListener('canplay', function () {
					 console.log('[canplay]')
				 });
				 if (typeof SMIL.callbackSmilFunc === 'function') {
					 SMIL.eleHLS.addEventListener('canplay', SMIL.callbackSmilFunc.bind(this, 'canplay', SMIL.eleHLS))
				 }
			 }
			 SMIL.eleHLS.addEventListener('timeupdate', SMIL._onHandleUpdate);
			 SMIL.eleHLS.addEventListener('playing', SMIL._onHandleUpdate);
			 SMIL.eleHLS.addEventListener('ended', SMIL._onHandleUpdate);
			 SMIL.eleHLS.addEventListener('pause', SMIL._onHandleUpdate);
			 SMIL.eleHLS.addEventListener('loadedmetadata', SMIL._onHandleUpdate);
			 if (typeof SMIL.callbackSmilFunc === 'function') {
				 SMIL.eleHLS.addEventListener('timeupdate', SMIL.callbackSmilFunc.bind(this, 'timeupdate', SMIL.eleHLS));
				 SMIL.eleHLS.addEventListener('playing', SMIL.callbackSmilFunc.bind(this, 'playing', SMIL.eleHLS));
				 SMIL.eleHLS.addEventListener('ended', SMIL.callbackSmilFunc.bind(this, 'ended', SMIL.eleHLS));
				 SMIL.eleHLS.addEventListener('pause', SMIL.callbackSmilFunc.bind(this, 'pause', SMIL.eleHLS));
				 SMIL.eleHLS.addEventListener('loadedmetadata', SMIL.callbackSmilFunc.bind(this, 'loadedmetadata', SMIL.eleHLS));
			 }

		 },
		 /**
		   * 현재 오디오 재생시간으로 부터 items의 index를 찾는다.
		   * @returns 
		   */
		 _getIndexFromCurrentTime: function () {
			 let attr = SMIL.eleHLS.getAttribute('hlsroot')
			 let findIndex = SMIL.items.findIndex(e => e.audio === attr && SMIL.eleHLS.currentTime >= e.begin && SMIL.eleHLS.currentTime < e.end);
			 return findIndex
		 },
		 /**
		  * audio 리스너
		  * @param {*} e 
		  * @returns 
		  */
		 _onHandleUpdate: function (e) {
			 let ty = e.type;
			 if (ty === 'timeupdate') {
				 SMIL.update();
			 }
			 /**
			  * Audio 파일 로드완료
			  */
			 else if (ty === 'loadedmetadata') {
				 SMIL._InProgress = false;
				 if (SMIL.currentItem && SMIL.currentFileno >= 0) {
					 SMIL.eleHLS.currentTime = SMIL.currentItem.begin
				 }
				 if (SMIL._isMoved) {
					 SMIL.eleHLS.play().then(rs => {
						 SMIL._isMoved = false;
					 }).catch(err => {
						 SMIL._isMoved = false;
					 });
				 }
			 }
			 /**
			  * Audio 파일 재생완료
			  */
			 else if (ty === 'ended') {
				 if (SMIL._isMoved) {
					 return;
				 }
				 SMIL._isMoved = true;
				 let next_item = SMIL._getNext();
				 //다음 재생아이템으로 이동
				 if (next_item) {
					 Navi.GotoPageByIndex(next_item.fileno);
				 }
				 //다음재생없음
				 else {
					 let fitems = SMIL.items.filter(e => e.fileno === SMIL.currentFileno);
					 if (fitems && fitems.length > 0) {
						 SMIL.currentItem = fitems[0];
						 SMIL.eleHLS.currentTime = SMIL.currentItem.begin
					 }
				 }
			 }
			 else if (ty === 'playing') {

			 }
			 else if (ty === 'pause') {

			 }
		 },
		 /**
		  * 오디오 시간으로 부터 item을 가저온다.
		  * @param {*} src
		  * @param {*} time 
		  */
		 _getItemFromTime: function (src, time) {
			 if (typeof src != 'string') {
				 return null
			 }
			 if (typeof time != 'number') {
				 return null
			 }
			 let idx = SMIL.items.findIndex(e => e.audio === src && e.begin <= time && e.end > time);
			 if (idx >= 0) {
				 return SMIL.items[idx];
			 }
			 return null;
		 },
		 /**
		  * 재생중인 아이템 기준 다음 아이템을 찾아온다.
		  * @returns 
		  */
		 _getNext: function () {
			 if (!SMIL.currentItem) {
				 return null
			 }
			 let idx = SMIL.items.findIndex(e => e === SMIL.currentItem);
			 if (idx >= 0) {
				 idx += 1;
				 return SMIL.items[idx];
			 } else {
				 return null;
			 }
		 },
		 /**
		  * 재생중인 아이템 기준 이전 아이템을 찾아온다.
		  * @returns 
		  */
		 _getPrev: function () {
			 if (!SMIL.currentItem) {
				 return null
			 }
			 let idx = SMIL.items.indexOf(SMIL.currentItem);
			 idx -= 1;
			 return SMIL.items[idx];
		 }
	 }

	// var SMIL = Core.SMIL = {
	// 	oldSelectElementID: null,
	// 	/**
	// 	 * 하이라이트 할 클래스 명
	// 	 */
	// 	activeClass: '',
	// 	playbackClass: '',
	// 	/**
	// 	 * 플레이 할 아이템 
	// 	 * @type [{fileno : 파일번호, seq: [{audio: filepath, begin: 시작시간, end:끝시간, text: 아이디},...] }, ...]
	// 	 */
	// 	items: null,
	// 	/**
	// 	 * SMIL 콜백 
	// 	 * @type function(type, data)
	// 	 * @description
	// 	 * 1. type : timer, data : 잔여 시간 - 타이머 관련
	// 	 * 2. type : timeupdate, data : HTMLAudioElement
	// 	 * 3. type : onplaying, data : HTMLAudioElement
	// 	 * 4. type : onended, data : HTMLAudioElement
	// 	 * 5. type : onpause, data : HTMLAudioElement
	// 	 * 6. type : play, data : HTMLAudioElement
	// 	 */
	// 	callbackSmilFunc: null,
	// 	/**
	// 	 * 해당 파일의 재생 중인 목록 번호
	// 	 */
	// 	currentPlayItemIndex: -1,
	// 	/**
	// 	 * 해당 파일의 전체 재생 목록 수
	// 	 */
	// 	totalPlayItemIndex: 0,
	// 	/**
	// 	 * 재생 중이거나, 멈춤 상태의 재생 리스트
	// 	 * @type {fileno : 파일번호, seq: [{audio: filepath, begin: 시작시간, end:끝시간, text: 아이디},...]}
	// 	 */
	// 	currentPlayItem: null,
	// 	/**
	// 	 * 현재 재생 중인 파일 번호
	// 	 */
	// 	currentPlayFileNo: -1,
	// 	/**
	// 	 * 현재 재생 중인 파일의 재생 시간 (초단위)
	// 	 * @type number
	// 	 */
	// 	currentTime: 0,
	// 	/**
	// 	 * 미디어 플레이어.
	// 	 * @type Audio
	// 	 */
	// 	ele_audio_player: null,
	// 	/**
	// 	 * Hls 클래스
	// 	 * @type Hls
	// 	 */
	// 	hls: null,

	// 	/**
	// 	 * 타이머 지정 시간 (초)
	// 	 */
	// 	timerDuration: 0,
	// 	/**
	// 	 * 타이머 잔여 시간 (초)
	// 	 */
	// 	timerRemain: 0,
	// 	/**
	// 	 * 타이머 동작 여부
	// 	 */
	// 	isTimerActive: false,
	// 	/**
	// 	 * SMIL 데이터 초기화
	// 	 * @param {object} smilInfo SMIL 정보
	// 	 * @param {function(type, data)} smilCallbackFunc SMIL callback function
	// 	 * @see callbackSmilFunc
	// 	 */
	// 	setup: function(smilInfo, smilCallbackFunc) {
	// 		if(smilInfo) {
	// 			if (smilInfo.activeclass){
	// 				SMIL.activeClass = smilInfo.activeclass;
	// 			} else {
	// 				SMIL.activeClass = 'base_smil_active';
	// 			}
	// 			SMIL.activeClass = smilInfo.activeclass;
	// 			SMIL.playbackClass = smilInfo.playbackactiveclass;
	// 			SMIL.items = smilInfo.items;
	// 		}

	// 		SMIL.callbackSmilFunc = smilCallbackFunc;
	// 		SMIL.hls = new Hls({debug: true, enableWorker: true, liveBackBufferLength: 900, maxMaxBufferLength: 10});
	// 	},

	// 	/**
	// 	 * 해당 아이디를 하이라이트 처리 한다.
	// 	 * @param {string} sid 하이라이트 할 아이디
	// 	 */
	// 	highlight: function (sid) {
			
	// 		if (sid != null && sid != '') {
	// 			//이전에 색칠한 부분 지움.
	// 			SMIL.clearHighlight();
				
	// 			SMIL.oldSelectElementID = sid;
	// 			var highlightElement = $('#'+SMIL.oldSelectElementID);

	// 			if(!highlightElement.hasClass(SMIL.activeClass)) {
	// 				$('#'+SMIL.oldSelectElementID).addClass(SMIL.activeClass);
					
	// 				var rects = Highlight.findNode('#'+SMIL.oldSelectElementID);
					

	// 				if(rects.constructor == Array) {
	// 					if (SMIL.callbackSmilFunc) {
	// 						SMIL.callbackSmilFunc('hightlight',{id:SMIL.oldSelectElementID, rect:rects});
	// 					}
	// 				}
	// 				else {
	// 					ePubEngineLogJS.W.log('SMIL.highlight.findNode error : ' + rects)
	// 				}
	// 			}
	// 		}
	// 	},
	// 	/**
	// 	 * 하이라이트를 제거한다.
	// 	 */
	// 	clearHighlight:function(){
	// 		if (SMIL.currentPlayFileNo >= 0){
	// 			ele_pure_view_wrapper.querySelectorAll('.'+SMIL.activeClass).forEach(ele =>{
	// 				ele.classList.remove(SMIL.activeClass);
	// 			});	
	// 		}
	// 	},
	// 	/**
	// 	 * 하이라이트된 영역으로 이동한다.
	// 	 */
	// 	moveToHighlight:function(){
	// 		if (SMIL.oldSelectElementID){
					
	// 			//스크롤
	// 			if (_scrollMode === true ){
	// 				//해당 대상의 앨리먼트로 이동함.
	// 				document.getElementById(SMIL.oldSelectElementID).scrollIntoView();
	// 			} 
	// 			//양면모드  or 단면.
	// 			else {
	// 				var currentScroll =  ele_pure_view_wrapper.scrollLeft; // 현재 스크롤의 이동한 위치.
	// 				var moveElePos = document.getElementById(SMIL.oldSelectElementID).offsetLeft;//하이라이트된 녀석.
	// 				var smilItem = SMIL.getItem(SMIL.currentPlayFileNo);//현재 플레이중인 smil의 item
	// 				var pageInfoItems =  _mapSpineWidth.get(smilItem.chaptor); // 해당챕터의 layout Info
	// 				// 현재 highlight 한대상이 범위내에 있는지 찾음.
	// 				var findItem = SMIL._findInRange(pageInfoItems.pageInfo , moveElePos);
	// 				//현재스크롤된 위치 === 하이라이트된 대상의 포지션의 item.
	// 				if (currentScroll === findItem.item.pos){
	// 					//현재 보여지는 화면임.
	// 				} else {
	// 					//양면.
	// 					if (_pageMode){
	// 						//짝수 왼쪽.
	// 						if (findItem.idx%2 == 0){
	// 							//왼쪽 하이라이트 중이니 이동
	// 							ele_view_wrapper.scrollLeft(findItem.item.pos);
	// 						}  else {
	// 							//오른쪽 재생중 페이지 유지.
	// 						}
	// 					} 
	// 					//단면.
	// 					else {
	// 						ele_view_wrapper.scrollLeft(findItem.item.pos);
	// 					}
	// 				}
	// 				smilItem = null;
	// 				pageInfoItems = null;
	// 				findItem = null;
	// 				currentScroll = null;
	// 				moveElePos = null;
	// 			}

	// 		} else {
	// 			throw 'the smil highlight id is undefined'
	// 		}
	// 	},
	// 	/**
	// 	 * 하이라이트 된 대상이 현위치에서 얼마나 떨어저 있는지 확인한다.
	// 	 * @param {*} list _mapSpineWidth에서 chaptor번호로 찾은뒤 pageInfo(ARRAY type)을 가저와야됨
	// 	 * @param {*} pos 현재 하이라이트 된 대상의 왼쪽 왼쪽으로부터의 스크롤 거리.
	// 	 * @returns 
	// 	 */
	// 	_findInRange:function(list ,pos){
    //         var i = 0, l = list.length;
    //         while(true){
    //             if (i === l){
	// 				return null
    //             }
    //             if (list[i].pos<=pos && list[i+1].pos>pos){
    //                 return {idx:i, item:list[i]};
    //             }
    //             i++;
    //         }
    //         return null
	// 	},
	// 	/**
	// 	 * 미디어 오버레이 모드를 종료 할때 반드시 호출 해야 한다.
	// 	 */
	// 	end: function() {
	// 		SMIL.clearHighlight();

	// 		if(SMIL.ele_audio_player) {
	// 			SMIL.ele_audio_player.pause();
	// 			if (Hls.isSupported()) {
	// 				SMIL.hls.detachMedia(SMIL.ele_audio_player);
	// 			}
	// 			SMIL.ele_audio_player = null;
	// 		}

	// 		SMIL.currentPlayFileNo = -1;
	// 		SMIL.currentPlayItem = null;
	// 		SMIL.currentPlayItemIndex = -1;
	// 		SMIL.currentTime = 0;
	// 	},
	// 	/**
	// 	 * 해당 파일에 SMIL 정보가 있는지 체크 한다.
	// 	 * @param {number} idx 해당 파일 번호
	// 	 * @returns 
	// 	 */
	// 	isSmil: function(idx) {
	// 		if(SMIL.items == null) return false;

	// 		for(var item of SMIL.items) {
	// 			if(item.fileno == idx) {
	// 				return true;
	// 			}
	// 		}

	// 		return false;
	// 	},
	// 	/**
	// 	 * 해당 파일의 smil item을 조회
	// 	 * @param {number} idx 파일번호
	// 	 * @returns object 또는 null 을 리턴.
	// 	 */
	// 	getItem: function(idx) {
	// 		if(SMIL.items == null) return null;

	// 		for(var item of SMIL.items) {
	// 			if(item.fileno == idx) {
	// 				return item;
	// 			}
	// 		}

	// 		return null;
	// 	},
	// 	/**
	// 	 * 해당 경로의 오디오를 지정 시간에 재생한다.
	// 	 * @param {strng} audioSrc 재생할 경로
	// 	 * @param {number} playTime 재생 시작 시간
	// 	 */
	// 	loadNPlay: function(audioSrc, playTime) {
			
			
	// 		if(SMIL.ele_audio_player!=null&&SMIL.ele_audio_player.src == audioSrc) {
	// 			SMIL.ele_audio_player.currentTime = playTime;
	// 			SMIL.ele_audio_player.play();
	// 		}
	// 		else {
	// 			if(SMIL.ele_audio_player) {
	// 				SMIL.ele_audio_player.pause();
	// 				if (Hls.isSupported()) {
	// 					SMIL.hls.detachMedia(SMIL.ele_audio_player);
	// 				}
	// 				SMIL.ele_audio_player = null;
	// 			}
	// 			//create audio with bind Event
	// 			SMIL.ele_audio_player = document.createElement('audio');


	// 			if (Hls.isSupported()) {
	// 				if(audioSrc.indexOf('blob:') == -1) {
	// 					SMIL.hls =  new Hls({debug: true, enableWorker: true, liveBackBufferLength: 900, maxMaxBufferLength: 10});
	// 					SMIL.hls.attachMedia(SMIL.ele_audio_player);

	// 					SMIL.hls.on(Hls.Events.MEDIA_ATTACHED, function () {
	
	
	// 						SMIL.hls.loadSource(audioSrc);
	
	// 						hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {

	
	// 							//parse
	// 							SMIL.callbackSmil('HlsParsed' , null);
	// 							SMIL.ele_audio_player.addEventListener('timeupdate', function () {
	// 								SMIL.callbackSmil('timeupdate', this);
	// 								SMIL.timeUpdate(this);
	// 							});
	// 							SMIL.ele_audio_player.addEventListener('playing',  function () {
	// 								SMIL.callbackSmil('playing', this);
	// 							});
	// 							SMIL.ele_audio_player.addEventListener( 'end', function () {
	// 								SMIL.callbackSmil('end', this);
	// 							});
	// 							SMIL.ele_audio_player.addEventListener('pause' , function () {
	// 								SMIL.callbackSmil('pause', this);
	// 							});
	// 							SMIL.ele_audio_player.addEventListener('loadedmetadata', function () {
	// 								SMIL.callbackSmil('loadedmetadata', this);
	// 							});
	
	// 							var playPromise = SMIL.ele_audio_player.play();
	// 							//현재 파일번호에 해당하는 시작지점으로 이동.
	// 							if (seqItem){
	// 								SMIL.ele_audio_player.currentTime = seqItem.begin;
	// 							}
								
	
	// 							if (playPromise !== undefined) {
	// 								playPromise.then(function() {
	// 									SMIL.callbackSmil('play', this);
	// 								}).catch(function(error) {
	// 									console.log('audio play err :' + error);
	// 								});
	// 							}
	// 						});
	// 					});
						
	// 					SMIL.hls.on(Hls.Events.ERROR, function (event, data) {
	// 						if (data.fatal) {
	// 							switch (data.type) {
	// 							case Hls.ErrorTypes.NETWORK_ERROR:
	// 								// try to recover network error
	// 								ePubEngineLogJS.W.log('fatal network error encountered, try to recover');
	// 								hls.startLoad();
	// 								break;
	// 							case Hls.ErrorTypes.MEDIA_ERROR:
	// 								ePubEngineLogJS.W.log('fatal media error encountered, try to recover');
	// 								hls.recoverMediaError();
	// 								break;
	// 							default:
	// 								// cannot recover
	// 								ePubEngineLogJS.W.log('fatal media error encountered, cant recover');
	// 								hls.destroy();
	// 								SMIL.callbackSmil('HlsError' , error);
	// 								break;
	// 							}
	// 						}
	// 					});
	// 				}
	// 			}
	// 			else if (video.canPlayType('application/vnd.apple.mpegurl')) {
	// 				//alert(' not support');
	// 				SMIL.ele_audio_player.src = audioSrc;
	// 				SMIL.ele_audio_player.addEventListener('timeupdate', function () {

	// 					SMIL.smilCallback('timeupdate', this);
	// 					SMIL.timeUpdate(this);
	// 				});

	// 				SMIL.ele_audio_player.addEventListener('playing',  function () {

	// 					SMIL.smilCallback('onplaying', this);
	// 				});

	// 				SMIL.ele_audio_player.addEventListener( 'end', function () {

	// 					SMIL.smilCallback('onended', this);
	// 				});

	// 				SMIL.ele_audio_player.addEventListener('pause' , function () {

	// 					SMIL.smilCallback('onpause', this);
	// 				});
	// 				SMIL.ele_audio_player.addEventListener('loadedmetadata',function(){
	// 					SMIL.smilCallback('loadedmetadata', this);
	// 				})
	// 				SMIL.ele_audio_player.currentTime = playTime;
	// 				var playPromise = SMIL.ele_audio_player.play();

	// 				if (playPromise !== undefined) {
	// 					playPromise
	// 					.then(function() {
	// 						SMIL.smilCallback('play', this);
	// 					})
	// 					.catch(function(error) {
	// 						ePubEngineLogJS.W.log('audio play err :' + error);
	// 					});
	// 				}
	// 			}
	// 		}
	// 	},
	// 	/**
	// 	 * callback
	// 	 * @param {*} type 
	// 	 * @param {*} obj 
	// 	 */
	// 	smilCallback:function(type , obj){
	// 		if (SMIL.callbackSmilFunc) {
	// 			SMIL.callbackSmilFunc(type, obj);
	// 		}
	// 	},
	// 	/**
	// 	 * 지정한 파일 번호의 미디어 오버레이를 재생한다.
	// 	 * @param {number} idx 파일 번호
	// 	 * @returns 
	// 	 */
	// 	play: function(idx) {
	// 		// 재생 할 퍼알 번호가 이전에 재생 중이였던 파일 번호와 
	// 		// 같으면 이전에 재생 했던 아이템을 재생한다. 이때 마지막 재생 위치 부터
	// 		// 재생 한다.
	// 		if(idx == SMIL.currentPlayFileNo) {
				
	// 			if(SMIL.currentPlayItem) {
	// 				// 지정한 파일 번호의 아이템을 찾는다.
	// 				SMIL.currentPlayItem = SMIL.getItem(idx);

	// 				if(SMIL.currentPlayItem) {
	// 					if(SMIL.currentPlayItemIndex == -1) {
	// 						// 첫번째 아이템을 재생 한다.
	// 						SMIL.currentPlayItemIndex = 0;
	// 						SMIL.totalPlayItemIndex = SMIL.currentPlayItem.seq.length;

	// 						if(SMIL.totalPlayItemIndex > 0) {
	// 							var seqItem = SMIL.currentPlayItem.seq[SMIL.currentPlayItemIndex];

	// 							if(seqItem) {
	// 								var audioSrc = ePubEngineLoaderJS.Util.replaceAll(seqItem.audio, "{CONTENT_DOMAIN}", ePubEngineLoaderJS.Data.getBaseUrl());
	// 								//파일의 가장 처음 아이템 오디오 시작점이므로 여기부터시작
	// 								SMIL.loadNPlay(audioSrc, seqItem.begin);
	// 							}
	// 							else {
	// 								ePubEngineLogJS.W.log('재생 가능한 상세 목록이 없음');
	// 								return false;
	// 							}
	// 						}
	// 						else {
	// 							ePubEngineLogJS.W.log('재생 가능한 목록이 없음');
	// 							return false;
	// 						}
	// 					}
	// 					else {
	// 						SMIL.totalPlayItemIndex = SMIL.currentPlayItem.seq.length;

	// 						if(SMIL.totalPlayItemIndex > 0) {
	// 							var seqItem = SMIL.currentPlayItem.seq[SMIL.currentPlayItemIndex];

	// 							if(seqItem) {
	// 								var audioSrc = ePubEngineLoaderJS.Util.replaceAll(seqItem.audio, "{CONTENT_DOMAIN}", ePubEngineLoaderJS.Data.getBaseUrl());
	// 								SMIL.loadNPlay(audioSrc, SMIL.currentTime);
	// 							}
	// 							else {
	// 								ePubEngineLogJS.W.log('재생 가능한 상세 목록이 없음');
	// 								return false;
	// 							}
	// 						}
	// 						else {
	// 							ePubEngineLogJS.W.log('재생 가능한 목록이 없음');
	// 							return false;
	// 						}
	// 					}
	// 				}
	// 				else {
	// 					ePubEngineLogJS.W.log('해당 파일 번호의 재생 가능한 목록이 없음');
	// 					return false;
	// 				}
	// 			}
	// 			else {
	// 				// 이전 재생된 적이 없으면 첫번째 아이템을 재생한다.
	// 				SMIL.currentPlayItem = SMIL.getItem(idx);

	// 				SMIL.currentPlayFileNo = idx;
	// 				SMIL.currentPlayItemIndex = 0;
	// 				SMIL.totalPlayItemIndex = SMIL.currentPlayItem.seq.length;

	// 				if(SMIL.totalPlayItemIndex > 0) {

	// 					var seqItem = SMIL.currentPlayItem.seq[SMIL.currentPlayItemIndex];

	// 					if(seqItem) {
	// 						var audioSrc = ePubEngineLoaderJS.Util.replaceAll(seqItem.audio, "{CONTENT_DOMAIN}", ePubEngineLoaderJS.Data.getBaseUrl());
	// 						//파일의 가장 처음 아이템 오디오 시작점이므로 여기부터시작
	// 						SMIL.loadNPlay(audioSrc, seqItem.begin);
	// 					}
	// 					else {
	// 						ePubEngineLogJS.W.log('재생 가능한 상세 목록이 없음');
	// 						return false;
	// 					}
	// 				}
	// 				else {
	// 					ePubEngineLogJS.W.log('재생 가능한 목록이 없음');
	// 					return false;
	// 				}
	// 			}
	// 		}
	// 		else {
	// 			SMIL.currentPlayItem = SMIL.getItem(idx);

	// 			if(SMIL.currentPlayItem) {
	// 				SMIL.currentPlayFileNo = idx;
	// 				SMIL.currentPlayItemIndex = 0;
	// 				SMIL.totalPlayItemIndex = SMIL.currentPlayItem.seq.length;

	// 				if(SMIL.totalPlayItemIndex > 0) {

	// 					var seqItem = SMIL.currentPlayItem.seq[SMIL.currentPlayItemIndex];

	// 					if(seqItem) {
	// 						var audioSrc = ePubEngineLoaderJS.Util.replaceAll(seqItem.audio, "{CONTENT_DOMAIN}", ePubEngineLoaderJS.Data.getBaseUrl());
	// 						//파일의 가장 처음 아이템 오디오 시작점이므로 여기부터시작
	// 						SMIL.loadNPlay(audioSrc, seqItem.begin);
	// 					}
	// 					else {
	// 						ePubEngineLogJS.W.log('재생 가능한 상세 목록이 없음');
	// 						return false;
	// 					}
	// 				}
	// 				else {
	// 					ePubEngineLogJS.W.log('재생 가능한 목록이 없음');
	// 					return false;
	// 				}
	// 			}
	// 			else {
	// 				ePubEngineLogJS.W.log('해당 파일 번호의 재생 가능한 목록이 없음');
	// 				return false;
	// 			}
	// 		}

	// 		return true;

	// 	},
	// 	/**
	// 	 * 정지
	// 	 */
	// 	stop: function() {
	// 		if (SMIL.oldSelectElementID != null) {
	// 			$('#'+SMIL.oldSelectElementID).removeClass(this.activeClass);
	// 			SMIL.oldSelectElementID = null;
	// 		}

	// 		if(SMIL.ele_audio_player) {
	// 			try {
	// 				SMIL.ele_audio_player.stop();
	// 			}
	// 			catch(eex) {
	// 				ePubEngineLogJS.W.log(eex);
	// 			}
	// 			if (Hls.isSupported()) {
	// 				SMIL.hls.detachMedia(SMIL.ele_audio_player);
	// 			}
	// 			SMIL.ele_audio_player = null;
	// 		}
	// 	},
	// 	/**
	// 	 * 일시정지
	// 	 */
	// 	pause: function() {
	// 		if (SMIL.oldSelectElementID != null) {
	// 			$('#'+SMIL.oldSelectElementID).removeClass(this.activeClass);
	// 			SMIL.oldSelectElementID = null;
	// 		}

	// 		if(SMIL.ele_audio_player) {
	// 			try {
	// 				SMIL.ele_audio_player.pause();
	// 			}
	// 			catch(eex) {
	// 				ePubEngineLogJS.W.log(eex);
	// 			}
	// 		}
	// 	},
	// 	/**
	// 	 * pause상태에서 재생상태로 바꿔줌
	// 	 */
	// 	restart:function(){
	// 		if (SMIL.oldSelectElementID != null) {
	// 			$('#'+SMIL.oldSelectElementID).removeClass(this.activeClass);
	// 			SMIL.oldSelectElementID = null;
	// 		}

	// 		if(SMIL.ele_audio_player) {
	// 			try {
	// 				SMIL.ele_audio_player.play();
	// 			}
	// 			catch(eex) {
	// 				ePubEngineLogJS.W.log(eex);
	// 			}
	// 		}
	// 	},
	// 	/**
	// 	 * currentTime에 맞는 smil 재생위치로 이동
	// 	 * @param {*} value 
	// 	 */
	// 	moveTo:function(value){
	// 		if (SMIL.oldSelectElementID != null) {
	// 			$('#'+SMIL.oldSelectElementID).removeClass(this.activeClass);
	// 			SMIL.oldSelectElementID = null;
	// 		}

	// 		if(SMIL.ele_audio_player) {
	// 			try {
	// 				SMIL.ele_audio_player.currentTime = value;
	// 			}
	// 			catch(eex) {
	// 				ePubEngineLogJS.W.log(eex);
	// 			}
	// 		}
	// 	},
	// 	/**
	// 	 * 다음 smil item으로 이동
	// 	 * @returns 
	// 	 */
	// 	next: function() {
	// 		var seqItem = SMIL.currentPlayItem.seq[SMIL.currentPlayItemIndex];
	// 		var nextIdx = SMIL.currentPlayItemIndex + 1;
	// 		var nextSeqItem = null;

	// 		if(nextIdx >= SMIL.totalPlayItemIndex) {
	// 			SMIL.end();
	// 			return false;
	// 		}
	// 		else {
	// 			nextSeqItem = SMIL.currentPlayItem.seq[nextIdx];

	// 			if(nextSeqItem) {
	// 				SMIL.currentPlayItemIndex = nextIdx;
	// 				//SMIL.currentPlayItem = nextSeqItem;

	// 				if(nextSeqItem.audio == seqItem.audio) {
	// 					SMIL.highlight(nextSeqItem.text);
	// 				}
	// 				else {
	// 					//오디오 파일이 다른 경우.
	// 					SMIL.loadNPlay(nextSeqItem, nextSeqItem.begin);
	// 					SMIL.highlight(nextSeqItem.text);
	// 				}
	// 			}
	// 			else {
	// 				return false;
	// 			}
	// 		}

	// 		return true;
	// 	},
	// 	/**
	// 	 * 오디오의 실행 시간
	// 	 * @param {Audio} audio 
	// 	 */
	// 	timeUpdate: function(audio) {


	// 		var prevTime = SMIL.currentTime;
	// 		SMIL.currentTime = audio.currentTime;

	// 		if(SMIL.currentPlayItem && SMIL.totalPlayItemIndex >= 0) {

	// 			var seqItem = SMIL.currentPlayItem.seq[SMIL.currentPlayItemIndex];

	// 			//{audio: filepath, begin: 시작시간, end:끝시간, text: 아이디}
	// 			if(seqItem) {
	// 				if(seqItem.begin <= SMIL.currentTime &&  SMIL.currentTime < seqItem.end) {
	// 					SMIL.highlight(seqItem.text);
	// 				}
	// 				else {
	// 					if(SMIL.currentTime >= seqItem.end) {
	// 						SMIL.next();
	// 					}
	// 				}
	// 			}
	// 		}

	// 		//timer check
	// 		if(SMIL.isTimerActive) {
	// 			SMIL.timerRemain -= (SMIL.currentTime - prevTime);

	// 			if(SMIL.timerRemain <= 0) {
	// 				SMIL.timerRemain = 0;
	// 				SMIL.isTimerActive = false;
	// 			}

	// 			if(SMIL.callbackSmilFunc) {
	// 				SMIL.callbackSmilFunc('timer', 0);
	// 			}
	// 		}
	// 	},
	// 	/**
	// 	 * 타이머 지정 또는 해제 한다.
	// 	 * @param {boolean} isActive 타이머 지정 여부
	// 	 * @param {number} timerDuration 타이머 지정 시간 (초)
	// 	 */
	// 	setTimerOnOff: function(isActive, timerDuration) {
	// 		if(isActive) {
	// 			SMIL.timerDuration = timerDuration;
	// 			SMIL.timerRemain = SMIL.timerDuration;
	// 			SMIL.isTimerActive = true;
	// 		}
	// 		else {
	// 			SMIL.timerDuration = 0;
	// 			SMIL.timerRemain = SMIL.timerDuration;
	// 			SMIL.isTimerActive = false;
	// 		}
	// 	},
	// 	/**
	// 	 * smil이 재생중인지 확인한다.
	// 	 * @returns 
	// 	 */
	// 	isPlaying:function(){
	// 		if (SMIL.ele_audio_player){
	// 			return !SMIL.ele_audio_player.paused;
	// 		}
	// 		return false;
	// 	},
	// 	/**
	// 	 * smil checker
	// 	 * @param {*} leftInfo 
	// 	 * @param {*} rightInfo 
	// 	 * @returns 
	// 	 */
	// 	checkSmil: function(leftInfo,rightInfo){
	// 		console.log('[checkSmil]',leftInfo,rightInfo);
	// 		let _fileno = -1;
	// 		if (!SMIL.items){
	// 			return;
	// 		}
	// 		if (!leftInfo){
	// 			return;
	// 		}
	// 		if (typeof leftInfo.fileno != 'number'){
	// 			return;
	// 		}

	// 		_fileno = leftInfo.fileno;
	// 		if (rightInfo){
	// 			if (rightInfo.fileno == 'number'){

	// 			}
	// 		}



	// 		//	스크롤 모드
	// 		if (_scrollMode){

	// 			return;
	// 		}
	// 		//	양면모드
	// 		if (_pageMode){

	// 			return;
	// 		}

	// 		//	단면모드

	// 		return;
	// 	}

	// };



	/**
	 * walker Class
	 */
	 const Walker = Core.Walker = {
		 TargetID: 'contents',
		 Accept: {
			acceptNode:function(node) { 
				if (!(node.parentElement instanceof HTMLUnknownElement )){ 
					if (! /^\s*$/.test(node.data)){
						return NodeFilter.FILTER_ACCEPT; 
					}
				}
			},
			acceptNodeEmpty:function(node) { 
				if (!(node.parentElement instanceof HTMLUnknownElement )){ 
					return NodeFilter.FILTER_ACCEPT; 
				}
			},
			acceptNode2:function(node) {
				if (!(node.nodeType === 1 && typeof node.id === 'string' && node.id.length >0 && node.classList && node.classList.contains('toc'))){
					if ( !(node.nodeType === 1 && node.classList && node.classList.contains('bdb_para_empty'))){
						if (!(node.parentElement instanceof HTMLUnknownElement || node instanceof HTMLUnknownElement)){ 
							if (! /^\s*$/.test(node.data)){
								return NodeFilter.FILTER_ACCEPT; 
							}
						}
					}
				}
			}
		 },
		 /**
		  * treeWalker 생성
		  * @returns 
		  */
		 Build(ele,acceptEmpty){
			let el = null
			 if (ele){
				el = ele
			 } else {
				el = document.getElementById(Walker.TargetID)
			 }
			 if (acceptEmpty){
				return document.createTreeWalker(el,NodeFilter.SHOW_TEXT,Walker.Accept.acceptNodeEmpty,false);
			 } else {
				return document.createTreeWalker(el,NodeFilter.SHOW_TEXT,Walker.Accept.acceptNode,false);
			 }
		 },
		 		 /**
		  * 
		  * @param {*} ele 
		  * @returns 
		  */
		getWalkerList(ele){
			let walk = null
			if (ele){
			walk = Walker.Build(ele);
			} else {
			walk = Walker.Build();
			}
			var nodeList = [];
			while(walk.nextNode()) {
				nodeList.push(walk.currentNode);
			}
			return nodeList;
		},
		 Build2(ele){
			let el = null
			if (ele){
			   el = ele
			} else {
			   el = document.getElementById(Walker.TargetID)
			}
			return document.createTreeWalker(el,NodeFilter.SHOW_ALL,Walker.Accept.acceptNode2,false);
		 },
		 getWalkerList2(ele){
			let walk = null
			if (ele){
			   walk = Walker.Build2(ele);
			} else {
			   walk = Walker.Build2();
			}
			var nodeList = [];
			let e = null
			while(walk.nextNode()) {
				e = walk.currentNode
				if (!((e.nodeType === Node.ELEMENT_NODE && e.id.includes('bdb-chapter')) || (e.tagName === 'A' && e.id.includes('-')))){
					nodeList.push(walk.currentNode);
				}
			}
			return nodeList;
		 },
		 getNodeInScreen(ele){
			let walk = null
			if (ele){
			   walk = Walker.Build2(ele);
			} else {
			   walk = Walker.Build2();
			}
			var nodeList = [];
			let e = null
			while(walk.nextNode()) {
				e = walk.currentNode
				if (!((e.nodeType === Node.ELEMENT_NODE && e.id.includes('bdb-chapter')) || (e.tagName === 'A' && e.id.includes('-')))){
					let cdtion = false
					let rects = Util.getClientRects(e)
					let r = null
					if (rects){
						for (let rect of rects){
							if (Util.rectInRange(rect)){
								cdtion = true
								r = rect
								break;
							}
						}
					}
					if (cdtion){
						nodeList.push({node:e,rect:r});
					}
				}
			}
			return nodeList;
		},
		/**
		 * 
		 * @returns  0: text,data둘다존재, 1: text만존재, 2: data만 존재, -1: 알수없는 에러
		 * 
		 */
		checkNodesInScreen(){
			let b = Walker.getNodeInScreen();
			let tgroup = b.filter(e=>e.node.nodeType === Node.TEXT_NODE)
			let egroup = b.filter(e=>(e.node.nodeType === Node.ELEMENT_NODE && 
				(e.node.tagName === 'IMG'||e.node.tagName === 'VIDEO'||e.node.tagName === 'AUDIO')))
			if (tgroup.length > 0 && egroup.length >0){
				return {code:0,data:[tgroup,egroup]}
			} else if (tgroup.length > 0){
				return {code:1,data:[tgroup]}
			} else if (egroup.length > 0){
				return {code:2,data:[egroup]}
			} else{
				return {code:-1,}
			}
		},
		 /**
		  * 현재 화면에대한 스캔을해 화면내에 존재하는 text list를 반환한다.
		  * @returns 
		  */
		 scanScreen(){
			 let walk = Walker.Build();
			 let rootRect = ele_view_wrapper.get(0).getBoundingClientRect()
			 let range = document.createRange()
			 let n = null
			 let rects = null
			 let csList = []
			 let minX = rootRect.left;
			 let maxX = rootRect.right;
			 let minY = rootRect.top;
			 let maxY = rootRect.bottom;
			 while(walk.nextNode()) {
				n = walk.currentNode
				if (n.nodeType === Node.TEXT_NODE){
					range.selectNode(n)
					rects = range.getClientRects()
					for (let r of rects){

						// && r.top >= minY && r.bottom >= maxY
						if (minX <= r.left &&  r.right <= maxX && r.top >= minY && r.bottom <= maxY){
							csList.push({'n':n,'r':rects,'r1':r})
							break;
						}
					}
				} else if (n.nodeType === Node.ELEMENT_NODE){

				}
			 }
			 if (csList.length > 0){
				if (n.nodeType === Node.TEXT_NODE){
					n = csList[csList.length-1].n
					range.selectNode(n)
					rects = range.getClientRects()
					for (var i = rects.length - 1; i >= 0; i--) {
						let r  = rects[i]
						if (minX <= r.left &&  r.right <= maxX && r.top >= minY && r.bottom <= maxY){
							csList[csList.length-1].r1 = r
							break;
						}
					}
				}
			 }

			 walk = null
			 rootRect = null
			 range = null
			 n = null
			 minX = null
			 maxX = null
			 return csList
		 },
		 /**
		  * 현재 화면을 스캔한다.
		  * @returns 
		  */
		 getScreenPosition(){
			 return new Promise(function(resolve,reject){
				let list = Walker.scanScreen()
				if (list.length === 0 ){
					reject('screen scan Failure')
					return;
				}
				let f = list[0] 
				let l = list[list.length-1]
				let fr = f.r1
				let lr = l.r1
				let p1 = Util.caretRangeFromPoint(document,fr.left+1,fr.top+1)
				let p2 = Util.caretRangeFromPoint(document,lr.right-1,lr.bottom-1)
	
				list[0].offset = p1.endOffset
				list[list.length-1].offset = p2.startOffset
	
				let text = list.map((e,i)=>{
					if (i === 0){
						return e.n.textContent.slice(e.offset)
					} else if (i === list.length-1){
						return e.n.textContent.substring(0,e.offset)
					} else {
						if (e.n.parentElement !== list[i-1].n.parentElement){
							return '\n'+e.n.textContent
						}
						return e.n.textContent
					}
				}).join('')
				let start = list[0]
				let end = list[list.length-1]
	
				let encode = Position.EncodeCfiAttr(start.n,start.offset,end.n,end.offset)
				// let decode = Position.DecodeCfiAttr(encode.startChapter,encode.startCfiAttr,encode.startIndexFromParent,encode.endChapter, encode.endCfiAttr, encode.endIndexFromParent)
	
				
				// console.log('# nodeToCfiAttr',encode)
				// console.log('# cfiAttrToNode',decode)
	

				resolve({ start, end, text , encode })
				return 
			 })
			
		 },
		 /**
		  * 페이지 스캔후 제일 첫 node와 의 node의 rect를 반환한다.
		  * @returns 
		  */
		 findFirst(){
			 return new Promise(function(resolve,reject){
				 let scList = Walker.scanScreen()
				 if (scList.length === 0){
					 reject('fail scan')
					 return
				 }
		 
				 let rootRect = ele_view_wrapper.get(0).getBoundingClientRect()
				 let n = null
				 let re = null
				 let f = false
				 let minX = rootRect.left;
				 let maxX = rootRect.right;
				 for (let sc of scList){
					 for (let r of sc.r){
						 if (minX < r.left &&  r.right < maxX){
							 n = sc.n
							 re = r
							 f = true
							 break;
						 }
					 }
					 if(f){
						 break;
					 }
				 }
				 if (!n || !re){
					 reject('not found')
					 return
				 }
				 let correct = 1
				 let x1 = re.left + correct
				 let y1 = re.top + correct
				 let x2 = re.right - correct
				 let y2 = re.bottom - correct
				 let start = Util.caretRangeFromPoint(document,x1,y1)
				 let end = Util.caretRangeFromPoint(document,x2,y2)
				 if (start && end){
					let range = document.createRange()
					range.setStart(start.startContainer,start.startOffset)
					range.setEnd(end.endContainer,end.endOffset)

				 }

				 resolve({node:n,rect:re})
			 })
		 },
		 getTextInRange(startContainer,startOffset,endContainer,endOffset){
			let list = Walker.Build()
			let text = list.map((e,i)=>{
				if (i === 0){
					return node.textContent.slice(e.offset)
				} else if (i === list.length-1){
					return node.textContent.substring(0,e.offset)
				} else {
					if (node.parentElement !== list[i-1].n.parentElement){
						return '\n'+node.textContent
					}
					return node.textContent
				}
			}).join('')
		 },

	 }
	/**
	 * Util
	 */
	var Util = Core.Util = {

		Trim: function (str) {
            return str.replace(/^\s+|\s+$/g, "")
        },
		/**
		 * 해당 노드가 텍스트 노드인지 여부.
		 * @param {Node} node 해당 노드
		 * @returns true or false
		 */
		isTextNode: function(node) {
			return node.nodeType == 3;
		},
		pointIsInOrAboveRect: function(x, y, rect) {
			return y < rect.bottom && x >= rect.left && x <= rect.right;
		},	
		collapsedRangeToRect: function(range, left) {
			var clientRect = range.getClientRects().length ? range.getClientRects()[range.getClientRects().length - 1] : null;
			if (!clientRect) {
				return null;
			}
			return {
				left   : left || clientRect.left,
				right  : clientRect.right,
				bottom : clientRect.bottom
			};
		},
		/**
		 * 
		 * @param {Node} node 
		 * @param {Range} range 
		 * @param {Number} offset 
		 * @param {Number} x 
		 * @param {Number} y 
		 * @param {Number} lastLeft 
		 * @returns 
		 */
		stepTextNode: function(node, range, offset, x, y, lastLeft) {
			range.setStart(node, offset);
			range.setEnd(node, offset);
			rect = collapsedRangeToRect(range, lastLeft);
			if (rect && pointIsInOrAboveRect(x, y, rect)) {
				if (rect.right - x > x - rect.left) {
					offset--;
				}
				return {
					node  : node,
					index : offset
				};
			}
			if (offset < node.length) {
				return stepTextNode(node, range, ++offset, x, y, rect ? rect.left : null);
			} 
			return null;
		},
		nodeIndex: function(node) {
			var i = 0;
			while ((node = node.previousSibling)) {
				i++;
			}
			return i;
		},
		findOffset: function(node, range, x, y) {
			if (isTextNode(node)) {
				var offset = stepTextNode(node, range, 0, x, y);
				if (offset) {
					return offset;
				}
			} 
			else {
				range.setEndAfter(node);
				var rect = range.getClientRects().length ? range.getClientRects()[range.getClientRects().length - 1] : null;
				if (rect && pointIsInOrAboveRect(x, y, rect)) {
					return {
						node  : node.parentNode,
						index : nodeIndex(node)
					};
				}
			}
	
			if (node.nextSibling) {
				return findOffset(node.nextSibling, range, x, y);
			}
	
			return {
				node  : node.parentNode,
				index : nodeIndex(node)
			};
		},
		fromPointIE: function(x, y, doc) {
			var range = null;
			var el = document.elementFromPoint(x, y);
	
			if(el == null) return null;
	
			if(window.getSelection && document.createRange) {
				range = document.createRange();
				range.selectNodeContents(el);
				range.collapse(true);
	
				var offset = {
					node  : el.firstChild,
					index : -1
				};
				
				if (!offset.node) {
					offset = {
						node  : el.parentNode,
						index : nodeIndex(el)
					};
				} else {
					offset = findOffset(offset.node, range, x, y);
				}
				return create(offset.node, offset.index);
			}
			else {
				return null;
			}
		},
		caretRangeFromPoint: function(doc, x, y) {
			position = null, range = null;
	
			if (typeof doc.caretPositionFromPoint != "undefined") {
				position = doc.caretPositionFromPoint(x, y);
				range = doc.createRange();
				range.setStart(position.offsetNode, position.offset);
				range.collapse(true);
			} 
			else if (typeof doc.caretRangeFromPoint != "undefined") {
				range = doc.caretRangeFromPoint(x, y);
			}
			else if (typeof doc.body.createTextRange != "undefined") {
				range = fromPointIE(x, y, doc);
			}
	
			return range;
		},
		GetNodePageNumber: function(node) {
			left = 0, top = 0;
			nMovePageNo = null;
			chapter_left = null, chapter_top = null;
	
			try {
				if(isChrome55) {
					chapter_top = $(node).prop("offsetTop") - top;
					nMovePageNo = parseInt(parseInt(chapter_top, 10) / this._viewer_height, 10);
				}
				else {
					chapter_left = $(node).prop("offsetLeft") - left;
					nMovePageNo = parseInt(parseInt(chapter_left, 10) / this._columnWidth, 10);
				}
				
				return parseInt(nMovePageNo, 10) + 1;
			}
			catch (ex) {
				return -1;
			}
			finally {
				left = null;
				nMovePageNo = null;
				chapter_left = null;
				top = null;
				chapter_top = null;
			}
		},
		closest: function(num , arr){
			var curr = arr[0];
			var diff = Math.abs(num - curr)
			for (var val = 0; val < arr.length; val++){
				var newdiff = Math.abs(num - arr[val]);
				if (newdiff < diff){
					diff = newdiff;
					curr = arr[val];
				}
			}
			return curr;
		},
		inRange: function(x, min, max) {
			return x >= min && x < max;
		},
		
		getRangeIndex: function(index,array){
			var result = -1
			if (typeof array !== 'undefined' && array.length > 0) {
				array.forEach((ele,idx) => {
					if (Core.Util.inRange(index, array[idx] ,array[idx+1])){
						result = idx
						return
					}
				});
			} else {
				throw 'must array type and array length more than 0'
			}
			return result
		},
		/**
		 * string값 안쪽에 띄어쓰기나 공백이 존재하는 체크한다.
		 * @param {string} value 
		 * @returns 존재 true
		 */
		isBlank:function(value){
			var blank_pattern = /[\s]/g;
			return blank_pattern.test( value ) == true;
		},
		/**
		 * white space인지
		 * @param {*} value 
		 * @returns 
		 */
		isEmpty:function(value){
			return value.replace(/\s+/g,'').length <= 0;
		},
		/**
		 * chapter li를 탐색할때까지 추적한다.
		 * @param {*} node 
		 * @returns 
		 */
		 findParent(node){
			let p = node.parentElement
			let f = null
			try {
				while(true){
					if (p === document || p === document.body){
						break;
					}
					if (p.id.startsWith('bdb-chapter')){
						f = p
						break;
					}
					p = p.parentElement
				}
				return f
			} catch(err){

			} finally {
				p = null
				f = null
			}
		},
		findParentChapter(node){
			let mchapter = -1
			let ec = Util.findParent(node)
			if (ec){
				if (ec.id){
					let mchp = ec.id.replace('bdb-chapter','')
					if (mchp){
						mchapter = parseInt(mchp)
					}
				}
			} 
			return mchapter
		},
		/**
		 * 텍스트노드가 parent노드안에서 몇번째 순서인지.
		 * @param {*} element 
		 * @param {*} textNode 
		 * @returns 
		 */
		 getNodeIndex(element,textNode){
			let n, count=0, walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
			while(n=walk.nextNode()){
				if (n.parentNode instanceof HTMLUnknownElement ){
					continue;
				}
				count++
				if (n === textNode){
					break;
				}
			}
			n = null;
			return count;	
		},
		/**
		 * event객체를 x,y좌표 reuturn
		 * @param {*} event mouse, touch event
		 * @returns 
		 */
		_getTarget: function(event){
			let x = (event.touches&&event.touches.length>0)? event.touches[0].clientX:event.clientX 
			let y = (event.touches&&event.touches.length>0)? event.touches[0].clientY:event.clientY 
			
			return {
				'x': x,
				'y': y
			}
		},
		/**
		 * 
		 * @param {*} string 
		 * @param {*} substring  
		 * @returns 
		 */
		getStringIndexList: function(string, substring){
			var a=[],i=-1;
			while((i=string.indexOf(substring,i+1)) >= 0) a.push(i);
			return a;
		},
		allDescendants (node) {
			let arr = []
			var all = node.getElementsByTagName('*');
			for (let e of all){
				if (e instanceof HTMLUnknownElement){

				} else {
					arr.push(e)
				}
			}
			return arr
		},
		/**	
		 * element or textNode getClientRects
		 * @param {*} ele 
		 * @returns 
		 */
		getClientRects(ele){
			if (!ele){
				return null;
			}
			if (ele.nodeType === Node.ELEMENT_NODE){
				return ele.getClientRects()
			} else if (ele.nodeType === Node.TEXT_NODE){
				let r = document.createRange()
				r.selectNode(ele)
				return r.getClientRects()
			}
			return null
		},
		/**
		 * rect가 뷰어 안쪽에 위치한지 여부
		 * @param {*} rect 
		 * @returns 
		 */
		rectInRange(rect){
			let vr = ele_view_wrapper.get(0).getBoundingClientRect()
			return vr.left <= rect.left && vr.top <= rect.top && vr.right>=rect.right && vr.bottom>=rect.bottom
		},
		/**
		 * node in Range
		 * @param {*} startNode 
		 * @param {*} startOffset 
		 * @param {*} endNode 
		 * @param {*} endOffset 
		 */
		nodesInRange(startNode,startOffset,endNode,endOffset){
			let r = document.createRange()
			r.setStart(startNode,startOffset)
			r.setEnd(endNode,endOffset)
			let rs = r.getClientRects()
			let c = false
			for (let rect of rs){
				if (Util.rectInRange(rect)){
					c = true
					break;
				}
			}
			return c
		},
		getBase64Image(img) {
			var canvas = document.createElement("canvas");
			canvas.width = img.naturalWidth;
			canvas.height = img.naturalHeight;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0);
			var dataURL = canvas.toDataURL("image/png");
			// return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
			return dataURL
		}
	};

	const Finder = {
		scanRange(){
			const getRange = function(x,y){
				let position = null, range = null;
				if (typeof document.caretPositionFromPoint != "undefined") {
					position = document.caretPositionFromPoint(x, y);
					range = document.createRange();
					range.setStart(position.offsetNode, position.offset);
					range.collapse(true);
				} 
				else if (typeof document.caretRangeFromPoint != "undefined") {
					range = document.caretRangeFromPoint(x, y);
				}
				else if (typeof document.body.createTextRange != "undefined") {
					range = fromPointIE(x, y, document);
				}
				return range
			}
			let rect = ele_view_wrapper.get(0).getBoundingClientRect()
			let x1 = rect.left + 5
			let y1 = rect.top + 5
			let x2 = rect.right - 5
			let y2 = rect.bottom - 5
			let mRange = getRange(x1,y1)
			let mRange2 = getRange(x2,y2)
			let range = document.createRange()
			range.setStart(mRange.startContainer,mRange.startOffset)
			range.setEnd(mRange2.endContainer,mRange2.endOffset)


		},
		/**
		 * 뷰어의 양쪽 끝  textNode를 가저온다.
		 * @param {*} target 뷰어 element
		 * @returns 
		 */
		db1: null,
		db2: null,
		scanAll(target){
			let rect = target.getBoundingClientRect()
			let x1 = rect.left + 5
			let y1 = rect.top + 5
			let x2 = rect.left+rect.width - 5
			let y2 = rect.bottom - 5
			let mRange = null
			let mRange2 = null
			const getRange = function(x,y){
				let position = null, range = null;
				if (typeof document.caretPositionFromPoint != "undefined") {
					position = document.caretPositionFromPoint(x, y);
					range = document.createRange();
					range.setStart(position.offsetNode, position.offset);
					range.collapse(true);
				} 
				else if (typeof document.caretRangeFromPoint != "undefined") {
					range = document.caretRangeFromPoint(x, y);
				}
				else if (typeof document.body.createTextRange != "undefined") {
					range = fromPointIE(x, y, document);
				}
				return range
			}
			const isStrEmpty = function(str){ 
				if (typeof str === 'string' && str.trim().length === 0) {
					if (str.trim().length === 0){
						return true
					} else {
						return false
					}
				} else {
					return false
				}
			}
			const getTextNodesAll = function(element) {
				var n, a=[], walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
				while(n=walk.nextNode()){
					if (n.parentNode  && n.parentNode instanceof HTMLUnknownElement){
						continue;
					}
					a.push(n);
				}
				n = null;
				return a;
			}
			const searchNode = function(f,b) {

				
				var n, a=[], walk=document.createTreeWalker(document.getElementById('contents'),NodeFilter.SHOW_TEXT,null,false);
				while(n=walk.nextNode()){
					if (n.parentNode  && n.parentNode instanceof HTMLUnknownElement){
						continue;
					}
					if (n.isEqualNode(f)){
						console.log('equal front',n)
					}
					if (n.isEqualNode(b)){
						console.log('equal back',n)
					}
				}
				n = null;
				return a;
			}
			const rewind = function(startRange,endRange){
				let sc = startRange.startContainer, 
				so = startRange.startOffset,
				ec = endRange.endContainer,
				eo = endRange.endOffset;

				let range = null
				let fg = null
				let list = null
				let front = null
				let back = null
				range = document.createRange()
				range.setStart(sc,so)
				range.setEnd(ec,eo)
				fg = range.cloneContents()
				list = getTextNodesAll(fg)
				front = null
				back = null
				for (let i = 0; i<list.length; i++){
					let node = list[i]
					if (!isStrEmpty(node.textContent)){
						front = node
						break;
					}
				}
				for (let i = list.length-1; i>=0; i--){
					let node = list[i]
					if (!isStrEmpty(node.textContent)){
						back = node
						break;
					}
				}
				if (!front || !back){
					return null
				}
				let startOffset = so
				let endOffset = eo
				return {front,back,startOffset,endOffset}
			}

			return new Promise(function(resolve , reject){
				mRange = getRange(x1,y1)
				mRange2 = getRange(x2,y2)
				if (!mRange || !mRange2){
					reject('not found range')
					return 
				}
				let rw = rewind(mRange,mRange2)
				if (rw){
					resolve(rw)
				} else {
					reject('not found text node')
				}
			})
		},
		scanAll2(target){
			let rect = target.getBoundingClientRect()
			let x1 = rect.left + 5
			let y1 = rect.top + 5
			let x2 = rect.left+rect.width - 5
			let y2 = rect.bottom - 5
			let mRange = null
			let mRange2 = null
			const getRange = function(x,y){
				let position = null, range = null;
				if (typeof document.caretPositionFromPoint != "undefined") {
					position = document.caretPositionFromPoint(x, y);
					range = document.createRange();
					range.setStart(position.offsetNode, position.offset);
					range.collapse(true);
				} 
				else if (typeof document.caretRangeFromPoint != "undefined") {
					range = document.caretRangeFromPoint(x, y);
				}
				else if (typeof document.body.createTextRange != "undefined") {
					range = fromPointIE(x, y, document);
				}
				return range
			}
			return new Promise(function(resolve , reject){
				mRange = getRange(x1,y1)
				mRange2 = getRange(x2,y2)
				if (!mRange || !mRange2){
					reject('not found range')
					return 
				}

				resolve({start:mRange,end:mRange2})
			})
		},
		/**
		 * 전체를 스캔후 파라미터에 해당하는 애들이 front의 끝인지 back의 끝인지 판변하는 함수
		 * @param {*} selector 샐랙터
		 * @param {*} startNode 시작노드
		 * @param {*} startOffset 노드offset
		 * @param {*} endNode 끝노드
		 * @param {*} endOffset 끝노드offset
		 * @returns 
		 */
		checkEndPoint(selector,startNode,startOffset,endNode,endOffset){
			let tg = null
			if (typeof selector === 'string' ){
				if (selector.startsWith('#')){
					tg = document.getElementById(selector.replace('#',''))
				} else if (selector.startsWith('.')){
					tg = document.getElementsByClassName(selector.replace('.',''))[0]
				} 
			} else if (selector instanceof HTMLElement){
				tg = selector
			}
			if (!tg){
				return Promise.reject('not found target element')
			}
			
			return this.scanAll(tg).then(rs=>{
				let cloneS = rs.front.textContent 
				let cloneE = rs.back.textContent
				let range = document.createRange()
				range.setStart(startNode,startOffset)
				range.setEnd(endNode,endOffset)
				let s = startNode.textContent.slice(startOffset)
				let e = endNode.textContent.substring(0,endOffset)
				let front = s === cloneS
				let back = e === cloneE
				return { front,back,range }
			})
		},
	}

	var isIE = null;
	var IEVersion = null;

	var Support = Core.Support = {
		checkMSIE: function() {

			if(isIE == null && IEVersion == null) {
	
				agent = navigator.userAgent.toLowerCase();
				name = navigator.appName;
				word = null;
	
				if( name == "Microsoft Internet Explorer" ) word = "msie ";
				else {
					if(agent.search("trident") > -1) word = "rident/.*rv:"; 
					else if(agent.search("edge/") > -1) word = "edge/"; 
				}
	
				if(word == null) {
					isIE = false;
				}
				else {
					reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" ); 
	
					if (  reg.exec( agent ) != null  ) IEVersion = RegExp.$1 + RegExp.$2; 
	
					isIE = true;
				}
			}
	
			return isIE;
		},
	};

	return Core;

}(ePubEngineLogJS, ePubEngineLoaderJS));


export default ePubEngineCoreJS;