/*
Runa Web ePub FixedLayout engine core V1.0.0.1001
www.bookswage.com
(c) 2004-2021 by Crazy Eric. All rights reserved.
*/


/// <reference path="./epub.fx.loader.js" />
/// <reference path="./epub.log.js" />
/// <reference path="../common/lib/hls.min.js" />


/**
 * ePub FixedLayout Engine core
 */
 var ePubFxEngineCoreJS = ePubFxEngineCoreJS || (function(ePubEngineLogJS, ePubFxEngineLoaderJS, undefined) {
	var ePubFxEngineCoreJs;

    // Native ePubFxEngineCoreJs from window (Browser)
    if (typeof window !== 'undefined' && window.ePubFxEngineCoreJs) {
        ePubFxEngineCoreJs = window.ePubFxEngineCoreJs;
    }

	if (typeof self !== 'undefined' && self.ePubFxEngineCoreJs) {
        ePubFxEngineCoreJs = self.ePubFxEngineCoreJs;
    }

	// Native ePubFxEngineCoreJs from worker
    if (typeof globalThis !== 'undefined' && globalThis.ePubFxEngineCoreJs) {
        ePubFxEngineCoreJs = globalThis.ePubFxEngineCoreJs;
    }

    // Native (experimental IE 11) ePubFxEngineCoreJs from window (Browser)
    if (!ePubFxEngineCoreJs && typeof window !== 'undefined' && window.msePubFxEngineCoreJs) {
        ePubFxEngineCoreJs = window.msePubFxEngineCoreJs;
    }

	var Core = {};
	/**
	 * viewer conainer element
	 */
	var ele_view_wrapper;

	/**
	 * 안쪽 iframe
	 */
	var ele_inner_iframe;
	/**
	 * 뷰어 로딩 시 로딩바를 보여줄 함수
	 * @type function(boolean visible)
	 */
	var loadingFunc;

	/**
	 * 상단 메뉴 컨테이너
	 */
	var ele_topmenu_container;
	/**
	 * 하단 메뉴 컨테이너
	 */
	var ele_bottommenu_container;

	/**
	 * 뷰어 가로 크기
	 */
	var _viewer_width = 0;
	/**
	 * 뷰어 세로 높이
	 */
	var _viewer_height = 0;

	var _initialized = false;

	/**
	 * 페이지 모드 (true : 앙면, false : 단면)
	 */
	var _pageMode = true;
	/**
	 * EPUB 에서 사용되는 콘텐츠의 SPREAD 방식 정의
	 * 0 : none, 1 : landscape, 2 : both, 3 : auto
	 */
	 var _contentSpread = 0;

	 /**
	  * EPUB 3.0 fixed-layout 스펙에서 사용되는 화면 방향
	  * 2: auto, 1 : landscape, 0 : portrait
	  */
	 var _contentOrientation = 2;

	/**
	 * 마지막 위치 (내부 사용용)
	 */
	var lastReadPos;

	/**
	 * 랜더링 높이에 offset
	 */
	var _heightOffset = 200;

	/**
	 * 랜더링 넓이에 offset
	 */
	var _widthOffset = 0;
	/**
	 * 양쪽 Padding 값
	 */
	var _padding = 0;

	/**
	 * 상단 offset 값
	 */
	var _offsetTop = 0;

	var viewerCurrentRatio = 0;

	/**
	 * 위치를 화면에 표시할 callback function
	 * @type function( leftpage, rightpage)
	 */
	var callbackPosition;

	var Layout = Core.Layout = {
		/**
		 * 현재 확대 비율
		 * @returns 
		 */
		getCurrentRatio: function(){
			return viewerCurrentRatio;
		},
		/**
		 * 높이 offset
		 * @param {*} offset 
		 */
		setHeightOffset: function(offset){
			_heightOffset = offset
		},
		/**
		 * 넓이 offset
		 * @param {*} offset 
		 */
		setWidthOffset: function(offset){
			_widthOffset = offset
		},
		/**
		 * 
		 */
		setPadding: function(value){
			_padding = value
		},
		/**
		 * 
		 */
		setOffsetTop: function(value){
			_offsetTop = value
		},
		/**
		 * 환경 설정
		 * @param {JQuery} viewerContainer 뷰어 컨테이너
		 * @param {function (visible) }loadingbarFunc 로딩 바 함수
		 * @param {string} topMenuElement 상단 메뉴
		 * @param {string} bottomMenuElement 하단 메뉴
		 * @param {boolean} pageMode 페이지 모드
		 * @param {number} spread content's spread
		 * @param {number} orientation content's orientation
		 * @param {number} positionCallback 위치를 화면에 표시할 callback function
		 */
		config: function(viewerContainer, loadingbarFunc, topMenuElement, bottomMenuElement, pageMode, items, spread, orientation, smil, contentBaseUrl, authToken, positionCallback, smilCallback) {
			ele_view_wrapper = viewerContainer;
			ele_inner_iframe = ele_view_wrapper.get(0).querySelector('iframe');
			// ele_inner_iframe = ele_inner_iframe.contentDocument || ele_inner_iframe.contentWindow.document;

			loadingFunc = loadingbarFunc;
			ele_topmenu_container = topMenuElement;
			ele_bottommenu_container = bottomMenuElement;

			callbackPosition = positionCallback;

			_pageMode = pageMode;
			_contentSpread = spread;
			_contentOrientation = orientation;


			ePubFxEngineLoaderJS.Data.init(items,spread,orientation,contentBaseUrl,authToken,ele_view_wrapper.attr('id'),null)

			Annotation.init();

			Selection.init();

			if (smil){
				var smilItems = smil['items'];
				if (smilItems){
					smilItems.forEach(item=>{
						if (item.seq){
							item.seq.forEach(seqItem =>{
								seqItem.begin = parseFloat(seqItem.begin);
								seqItem.end = parseFloat(seqItem.end);
							})
						}
					})
					var smilInfo = { activeclass: smil['active-class'], items: smilItems };
					SMIL.setup(smilInfo ,smilCallback);
				}
			}

		},
		/**
		 * 
		 * @param {boolean} pageMode 페이지 모드 (true : 앙면, false : 단면)
		 */
		changeViewModel(pageMode) {
			_pageMode = pageMode;
		},
		/**
		 * 페이지 모드 정보를 리턴한다.
		 * @returns 페이지 모드 (true : 앙면, false : 단면) 
		 */
		getPageMode: function() {
			return _pageMode;
		},
		getViewerHeight: function() {
			return _viewer_height;
		},
		getContentRatio: function(width, height, contentWidth, contentHeight) {
			var ratio = 0;
			var regWidth = 0;
			var reHeight = 0;

			if(contentWidth > contentHeight) {
				ratio = width / contentWidth ;
				regWidth = parseInt(width);
				reHeight = parseInt(contentHeight * ratio);

				if(reHeight > height) {
					ratio = height / contentHeight;
					reHeight = parseInt(height);
					regWidth = parseInt(contentWidth * ratio);
				}
			}
			else {
				ratio =  height / contentHeight;
				reHeight = parseInt(height);
				regWidth = parseInt(contentWidth * ratio);

				if(regWidth > width) {
					ratio = width / contentWidth ;
					regWidth = parseInt(width);
					reHeight = parseInt(contentHeight * ratio);
				}
			}
			
			return {ratio : ratio, width: regWidth, height: reHeight};
		},
		setZoom: function(ele, ratio, baseWidth, baseHeight, ratioWidth, ratioHeight) {
			viewerCurrentRatio = ratio;
			ele.width(baseWidth);
			ele.height(baseHeight);
			ele.css('left', (_viewer_width / 2) - ratioWidth);
			ele.css('top', (_viewer_height - ratioHeight) / 2);
			ele.css({
				'-ms-zoom': ratio,
				'-moz-transform': 'scale('+ratio+')',
				'-moz-transform-origin': '0 0',
				'-o-transform': 'scale('+ratio+')',
				'-o-transform-origin': '0 0',
				'-webkit-transform': 'scale('+ratio+')',
				'-webkit-transform-origin': '0 0'
			});

			ele.attr('zdt', ratio);
		},
		setZoomFit: function(ele, ratio, baseWidth, baseHeight, ratioWidth, ratioHeight) {
			viewerCurrentRatio = ratio;
			ele.width(baseWidth);
			ele.height(baseHeight);
			ele.css('left', (_viewer_width / 2) - ratioWidth);
			ele.css('top', (_viewer_height - ratioHeight) / 2);
			ele.css({
				'-ms-zoom': ratio,
				'-moz-transform': 'scale('+ratio+')',
				'-moz-transform-origin': '0 0',
				'-o-transform': 'scale('+ratio+')',
				'-o-transform-origin': '0 0',
				'-webkit-transform': 'scale('+ratio+')',
				'-webkit-transform-origin': '0 0'
			});

			ele.attr('zdt', ratio);
		},
		setZoomFitCenter: function(ele, ratio, baseWidth, baseHeight, ratioWidth, ratioHeight) {
			viewerCurrentRatio = ratio;
			ele.width(baseWidth);
			ele.height(baseHeight);
			ele.css('left', (_viewer_width - ratioWidth) / 2);
			ele.css('top', (_viewer_height - ratioHeight) / 2);
			ele.css({
				'-ms-zoom': ratio,
				'-moz-transform': 'scale('+ratio+')',
				'-moz-transform-origin': '0 0',
				'-o-transform': 'scale('+ratio+')',
				'-o-transform-origin': '0 0',
				'-webkit-transform': 'scale('+ratio+')',
				'-webkit-transform-origin': '0 0'
			});

			ele.attr('zdt', ratio);
		},
		/**
		 * 화면 재 배열
		 * @returns Promise
		 */
		init: function() {
			return new Promise( function(resolve, reject) {

				try {
					_viewer_width = parseInt(ele_view_wrapper.parent().width()) - _widthOffset*2;
					ele_view_wrapper.width(_viewer_width);

					_viewer_height = (window.innerHeight) - _heightOffset;
					ele_view_wrapper.height(_viewer_height);

					if(_pageMode || _contentSpread != 0) {
						var _viewportsLeftContent = ePubFxEngineLoaderJS.Data.getLefFrame().contents().find('meta[name="viewport"]').attr( 'content' );

						if(_viewportsLeftContent != null && _viewportsLeftContent != undefined && _viewportsLeftContent != '') {
							var viewportLeftInfo = Layout.getViewPort(_viewportsLeftContent);
							ePubEngineLogJS.W.log('viewportLeftInfo : ' +JSON.stringify(viewportLeftInfo));

							var contentRatio = Layout.getContentRatio(_viewer_width / 2, _viewer_height, viewportLeftInfo.width, viewportLeftInfo.height);

							Layout.setZoomFit(ePubFxEngineLoaderJS.Data.getLefFrame(), contentRatio.ratio, viewportLeftInfo.width, viewportLeftInfo.height, contentRatio.width, contentRatio.height);

							ePubFxEngineLoaderJS.Data.getLefFrame().show();
						}
						else {
							Layout.setZoomFit(ePubFxEngineLoaderJS.Data.getLefFrame(), 1, _viewer_width / 2, _viewer_height, _viewer_width / 2, _viewer_height);
						}

						var _viewportsRightContent = ePubFxEngineLoaderJS.Data.getRightFrame().contents().find('meta[name="viewport"]').attr( 'content' );

						if(_viewportsRightContent != null && _viewportsRightContent != undefined && _viewportsRightContent != '') {
							var viewportRightInfo = Layout.getViewPort(_viewportsRightContent);
							ePubEngineLogJS.W.log('viewportRightInfo : ' +JSON.stringify(viewportRightInfo));

							var contentRatio = Layout.getContentRatio(_viewer_width / 2, _viewer_height, viewportRightInfo.width, viewportRightInfo.height);

							Layout.setZoomFit(ePubFxEngineLoaderJS.Data.getRightFrame(), contentRatio.ratio, viewportRightInfo.width, viewportRightInfo.height, 0, contentRatio.height);

							ePubFxEngineLoaderJS.Data.getRightFrame().show();
						}
						else {
							Layout.setZoomFit(ePubFxEngineLoaderJS.Data.getRightFrame(), 1, _viewer_width / 2, _viewer_height, 0, _viewer_height);
						}
					}
					else {
						var _viewportsLeftContent = ePubFxEngineLoaderJS.Data.getLefFrame().contents().find('meta[name="viewport"]').attr( 'content' );

						if(_viewportsLeftContent != null && _viewportsLeftContent != undefined && _viewportsLeftContent != '') {
							var viewportLeftInfo = Layout.getViewPort(_viewportsLeftContent);
							ePubEngineLogJS.W.log('viewportLeftInfo : ' +JSON.stringify(viewportLeftInfo));

							var contentRatio = Layout.getContentRatio(_viewer_width, _viewer_height, viewportLeftInfo.width, viewportLeftInfo.height);

							Layout.setZoomFitCenter(ePubFxEngineLoaderJS.Data.getLefFrame(), contentRatio.ratio, viewportLeftInfo.width, viewportLeftInfo.height, contentRatio.width, contentRatio.height);

							ePubFxEngineLoaderJS.Data.getLefFrame().show();
						}
						else {
							Layout.setZoomFitCenter(ePubFxEngineLoaderJS.Data.getLefFrame(), 1, _viewer_width / 2, _viewer_height, _viewer_width, 0);
						}
					}
					

					_initialized = true;

					resolve();
				}
				catch(ex) {
					ePubEngineLogJS.L.log(ex);
					reject();
				}
			});
		},
		afterInit: function(movePage){
			return new Promise(function(resolve, reject){
				if(Layout.getPageMode() || _contentSpread != 0) {
					var rightPage = movePage + 1;
					if(movePage == 0) {
						movePage = -1;
						rightPage = 0;
					}
					if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
						rightPage = -1;
					}
					callbackPosition(movePage, rightPage)
					ele_view_wrapper.disablescroll('undo');
					return resolve({ left: movePage, right: rightPage })
				}
				else {
					callbackPosition(movePage, -1)
					ele_view_wrapper.disablescroll('undo');
					return resolve({ left: movePage, right: -1 })
				}
			})
		},
		/**
		 * html의 뷰포트 스케일을 가져온다.
		 * @param {string} str viewport content
		 * @returns object : {width: xxx, height: xxx, initial-scale: xxx}
		 */
		 getViewPort: function(str) {

			var viewportInfo = null;
            var arrContent = null;

            try {

                var regWidth = /^width=/i;
                var regHeight = /^height=/i;
                
                arrContent = str.split(",");

                viewportInfo = new Object();

                for (var i = 0; i < arrContent.length; i++) {
                    var arrInfo = arrContent[i].split("=");
                    if (arrInfo.length == 2) {
                        var key = Util.Trim(arrInfo[0]);
                        var value = Util.Trim(arrInfo[1]);

                        if (regWidth.test(key) || regHeight.test(key)) {
                            // 간혹 px 을 포함한 값이 지정됨.
                            value.replace(/px/ig, "");
                        }

                        viewportInfo[key] = value;
                    }

					ePubEngineLogJS.L.log('arrContent[i] : ' + arrContent[i]);
                }

                return viewportInfo;
            }
            catch (ex) {
                ePubEngineLogJS.log('[★Exception★][ePubFxEngineLoaderJS.Loader.getViewPort] ' + ex);
                return null; //예외
            }
            finally {
                viewportInfo = null;
                arrContent = null;
            }
		},
		/*
		* 이미지에 워터마크 삽입요청
		*/
		syncSteg : function(token , send_item) {
			var rdata;
			$.ajax({
				url : "/Api/GetWatermarkImage",
				crossOrigin : true,
				type:'get',
				async: false,
				headers: {
					'authorization': token
				},
				data : {
					url:send_item,
					token:baseInfo.token
				},
				success : function(receive_data) {
					rdata = receive_data;
				},
				error:function () {
						console.log("복호화 실패");
				}
			});
		
			return rdata;
		},

		/**
		 * iframe 안쪽 컨텐츠.
		 * @returns 
		 */
		getInnerContents:function(){
			var ifr = ele_view_wrapper.get(0).querySelector('iframe');
			var doc = ifr.contentDocument || ifr.contentWindow.document;
			return doc.children.item(0)
		}
	};

	var Navi = Core.Navi = {
		GetNodePageNumber: function(node) {
			left = 0, top = 0;
			nMovePageNo = null;
			chapter_left = null, chapter_top = null;
	
			try {
				if(_scrollMode) {
					chapter_top = $(node).prop("offsetTop") - top;
					nMovePageNo = parseInt(parseInt(chapter_top, 10) / _viewer_height, 10);
				}
				else {
					chapter_left = $(node).prop("offsetLeft") - left;
					nMovePageNo = parseInt(parseInt(chapter_left, 10) / _columnWidth, 10);
				}
				
				return parseInt(nMovePageNo, 10) + 1;
			}
			catch (ex) {
				return -1;
			}
			finally {
				left = null;
				nMovePageNo = null;
				chapter_left = null;
				top = null;
				chapter_top = null;
			}
		},
		/**
		 * 이전 페이지 이동
		 * @returns false : 더이상 이동 불가
		 */
		GotoPrevPage: function() {
			return this.GotoPage(false);			
		},
		/**
		 * 다음 페이지 이동
		 * @returns false : 더이상 이동 불가
		 */
		GotoNextPage: function() {
			return this.GotoPage(true);
		},
		/**
		 * 페이지 이동
		 * @param {boolean} direction 방향 false 이면 이전, true 이면 다음.
		 * 
		 * @returns false : 더이상 이동 불가
		 */
		GotoPage: function(direction) {
			var currPage = ePubFxEngineLoaderJS.Data.getLeftSpine();
			var totalPage = ePubFxEngineLoaderJS.Data.getTotalSpine();
			var movePage = currPage;

			if(direction) {
				if(_pageMode || _contentSpread != 0) {
					
					if(currPage <= 0) {
						movePage = 1;
					}
					else {
						movePage = currPage + 2;
					}

					if(movePage >= totalPage) {
						return false;
					}

					if(loadingFunc) loadingFunc(true);

					ePubFxEngineLoaderJS.Data.loadSpineAt(movePage, true)
						.then(function(result) {
							Layout.init()
								.then(function() {

									if(loadingFunc) loadingFunc(false);

									var rightPage = movePage + 1;

									if(movePage == 0) {
										movePage = -1;
										rightPage = 0;
									}

									if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
										rightPage = -1;
									}

									callbackPosition(movePage, rightPage);
								})
								.catch(function() {
									if(loadingFunc) loadingFunc(false);
								});
						})
						.catch(function(err) {
							ePubEngineLogJS.E.log(err);
							if(loadingFunc) loadingFunc(false);

							var rightPage = movePage + 1;

							if(movePage == 0) {
								movePage = -1;
								rightPage = 0;
							}

							if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
								rightPage = -1;
							}

							callbackPosition(movePage, rightPage);
						});
				}
				else {

					movePage = movePage + 1;

					if(movePage >= totalPage) {
						return false;
					}

					if(loadingFunc) loadingFunc(true);

					ePubFxEngineLoaderJS.Data.loadSpineAt(movePage, true)
						.then(function(result) {
							Layout.init()
								.then(function() {
									if(loadingFunc) loadingFunc(false);
									callbackPosition(movePage, -1);
								})
								.catch(function() {
									if(loadingFunc) loadingFunc(false);
								});
						})
						.catch(function(err) {
							ePubEngineLogJS.E.log(err);
							if(loadingFunc) loadingFunc(false);
							callbackPosition(currPage, -1);
						});
				}
			}
			else {
				if(_pageMode || _contentSpread != 0) {

					if(movePage == 0) {
						return false;
					}

					movePage = movePage - 2;

					if(currPage < 0) {
						movePage = 0;
					}
					else {
						if(movePage % 2 == 0) {
							movePage -= 1;
						}
					}

					if(currPage < 0) {
						movePage = 0;
					}

					if(movePage < 0){
						movePage = 0;
					}

					if(loadingFunc) loadingFunc(true);

					ePubFxEngineLoaderJS.Data.loadSpineAt(movePage, true)
						.then(function(result) {
							Layout.init()
								.then(function() {

									if(loadingFunc) loadingFunc(false);

									var rightPage = movePage + 1;

									if(movePage == 0) {
										movePage = -1;
										rightPage = 0;
									}

									if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
										rightPage = -1;
									}

									callbackPosition(movePage, rightPage);
								})
								.catch(function() {
									if(loadingFunc) loadingFunc(false);
								});
						})
						.catch(function(err) {
							ePubEngineLogJS.E.log(err);
							if(loadingFunc) loadingFunc(false);

							var rightPage = movePage + 1;

							if(movePage == 0) {
								movePage = -1;
								rightPage = 0;
							}

							if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
								rightPage = -1;
							}

							callbackPosition(movePage, rightPage);
						});
				}
				else {

					if(movePage == 0) {
						return false;
					}

					movePage = movePage - 1;

					if(movePage < 0) {
						return false;
					}

					if(loadingFunc) loadingFunc(true);

					ePubFxEngineLoaderJS.Data.loadSpineAt(movePage, true)
						.then(function(result) {
							Layout.init()
								.then(function() {
									if(loadingFunc) loadingFunc(false);
									callbackPosition(movePage, -1);
								})
								.catch(function() {
									if(loadingFunc) loadingFunc(false);
								});
						})
						.catch(function(err) {
							ePubEngineLogJS.E.log(err);
							if(loadingFunc) loadingFunc(false);
							callbackPosition(currPage, -1);
						});
				}
			}

			return true;
		},
		/**
		 * 특정 페이지로 이동
		 * @param {number} page 페이지 번호
		 */
		GotoPageByIndex: function(page) {
			var totalPage = ePubFxEngineLoaderJS.Data.getTotalSpine();

			if(_pageMode || _contentSpread != 0) {
				var currPage = page;

				if(currPage % 2 == 0) {
					currPage -= 1;
				}

				if(currPage < 0) currPage = 0;

				if(currPage >= totalPage) {
					return false;
				}

				if(loadingFunc) loadingFunc(true);

				ePubFxEngineLoaderJS.Data.loadSpineAt(currPage, true)
					.then(function(result) {
						Layout.init()
							.then(function() {

								if(loadingFunc) loadingFunc(false);

								var rightPage = currPage + 1;

								if(currPage == 0) {
									currPage = -1;
									rightPage = 0;
								}

								if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
									rightPage = -1;
								}

								callbackPosition(currPage, rightPage);
							})
							.catch(function() {
								if(loadingFunc) loadingFunc(false);
							});
					})
					.catch(function(err) {
						ePubEngineLogJS.E.log(err);
						if(loadingFunc) loadingFunc(false);
						var rightPage = currPage + 1;

						if(currPage == 0) {
							currPage = -1;
							rightPage = 0;
						}

						if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
							rightPage = -1;
						}

						callbackPosition(currPage, rightPage);
					});
			}
			else {

				if(page < 0 || page >= totalPage) {
					return false;
				}

				if(loadingFunc) loadingFunc(true);

				ePubFxEngineLoaderJS.Data.loadSpineAt(page, true)
					.then(function(result) {
						Layout.init()
							.then(function() {
								if(loadingFunc) loadingFunc(false);
								callbackPosition(page, -1);
							})
							.catch(function() {
								if(loadingFunc) loadingFunc(false);
							});
					})
					.catch(function(err) {
						ePubEngineLogJS.E.log(err);
						if(loadingFunc) loadingFunc(false);
						callbackPosition(err, err);
					});
			}
		},
		GotoAnchor: function(idx, id) {
			var totalPage = ePubFxEngineLoaderJS.Data.getTotalSpine();

			if(_pageMode || _contentSpread != 0) {
				var currPage = idx;

				if(currPage % 2 == 0) {
					currPage -= 1;
				}

				if(currPage < 0) currPage = 0;

				if(currPage >= totalPage) {
					return false;
				}

				if(loadingFunc) loadingFunc(true);

				ePubFxEngineLoaderJS.Data.loadSpineAt(currPage, true)
					.then(function(result) {
						Layout.init()
							.then(function() {

								if(loadingFunc) loadingFunc(false);

								var rightPage = movePage + 1;

								if(movePage == 0) {
									movePage = -1;
									rightPage = 0;
								}

								if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
									rightPage = -1;
								}

								callbackPosition(movePage, rightPage);
							})
							.catch(function() {
								if(loadingFunc) loadingFunc(false);
							});
					})
					.catch(function(err) {
						ePubEngineLogJS.E.log(err);
						if(loadingFunc) loadingFunc(false);
						
						var rightPage = movePage + 1;

						if(movePage == 0) {
							movePage = -1;
							rightPage = 0;
						}

						if(rightPage >= ePubFxEngineLoaderJS.Data.getTotalSpine()) {
							rightPage = -1;
						}

						callbackPosition(movePage, rightPage);
					});
			}
			else {

				if(idx < 0 || idx >= totalPage) {
					return false;
				}

				if(loadingFunc) loadingFunc(true);

				ePubFxEngineLoaderJS.Data.loadSpineAt(idx, true)
					.then(function(result) {
						Layout.init()
							.then(function() {
								if(loadingFunc) loadingFunc(false);
								callbackPosition(idx, -1);
							})
							.catch(function() {
								if(loadingFunc) loadingFunc(false);
							});
					})
					.catch(function(err) {
						ePubEngineLogJS.E.log(err);
						if(loadingFunc) loadingFunc(false);
						callbackPosition(idx, -1);
					});
			}
		}
	};

	var Search = Core.Search = {
		/**
		 * 검색된 전체 개수
		 */
		searchItemTotal: 0,
		/**
		 * 검색할 파일 개수
		 */
		searchFileTotal: 0,
		/**
		 * 현재 검색 중인 파일 번호
		 */
		searchFileStatus : -1,

		findTextHeaderTag: '<span class="lyres_tsym">',
		findTextFooterTag: '</span>',

		/**
		 * 검색 시작
		 * @param {string} keyword 검색어
		 * @param {Array} toc 목차 목록
		 * @param {function (list)} callback 콜백 함수
		 * @returns 
		 */
		searchStart: function(keyword, toc, callback) {
			return new Promise(function(resolve, reject) {

				Search.searchFileTotal = ePubFxEngineLoaderJS.Data.getTotalSpine();
				Search.searchItemTotal = 0;
				Search.searchFileStatus = -1;

				var temp = [];
				toc.forEach((r)=>{
					temp.push(parseInt(r.pos))
				})

				Search.searchFunc(keyword, 0, Search.searchFileTotal, toc, temp, callback,resolve);
			});
		},
		searchFunc: function(keyword, idx, total, arrToc, tocs, callback,resolve) {

			if(idx < 0 || idx >= total) {
				resolve();
				return;
			}

			Search.searchFileStatus = idx;

			ePubFxEngineLoaderJS.Data.getSpineData(idx)
				.then(function(spineData) {
					var searchResult = [];
					const parser = new DOMParser();
					var ofData = ePubFxEngineLoaderJS.Util.replaceAll(spineData, "{CONTENT_OBFUCATE}", ' ');
					var spineDoc = parser.parseFromString(ofData, 'text/html');
					var result = Search._search_fast_impl(keyword, spineDoc, 20);
					if (result){
						if (result.items && result.items.length > 0){
							result.items.forEach((rs)=>{
								var spineItem = ePubFxEngineLoaderJS.Data.getSpineItem(idx);
								var page = parseInt(spineItem.percent);
								var v = Core.Util.getRangeIndex(page , tocs);
								rs.toc = v == -1 ? null : arrToc[v];
								rs.page = page;
								searchResult.push(rs);
								Search.searchItemTotal++;
							});
							if(callback) callback(searchResult);
						} else {
							if(callback) callback(searchResult);
						}
					}
					Search.searchFunc(keyword, idx + 1, total, arrToc, tocs, callback,resolve);
				})
				.catch(function(err) {
					Search.searchFunc(keyword, idx + 1, total, arrToc, tocs, callback,resolve);
				});
		},
		SetCfiAttribute: function (spineDocument) {
			
			try {
				// nextSibling만 처리
				var node = spineDocument;
				var cfi = '';
				var idxCfi = 0;
				var idxTag = 0;

				var temp = node.firstChild;
				while (temp != null) {
					idxCfi |= 1;
					if (temp.nodeType == 1) {
						idxCfi++;
						idxTag++;
					}
					
					try {
						temp['cfi'] = cfi + '/' + idxCfi;
						temp['tagPos'] = idxTag;
					}
					catch (ea) {
						ePubEngineLogJS.W.log(ea + ' :' + temp.nodeType + ',' + temp.nodeName);
					}

					idxTag = this._SetCfiAttributeImpl(temp, cfi + '/' + idxCfi, idxTag);

					temp = temp.nextSibling;
				}
				
			}
			catch (ex) {
				ePubEngineLogJS.W.log(ex);
			}

			return idxTag;
		},
		_SetCfiAttributeImpl: function (node, cfiHeader, tagPos) {
			try {
				// nextSibling만 처리
				var cfi = cfiHeader;
				var idxCfi = 0;
				var idxTag = tagPos;

				var temp = node.firstChild;
				while (temp != null) {
					idxCfi |= 1;
					if (temp.nodeType == 1) {
						idxCfi++;
						idxTag++;
					}
					
					try {
						temp['cfi'] = cfi + '/' + idxCfi;
						temp['tagPos'] = idxTag;
					}
					catch (ea) {
						ePubEngineLogJS.W.log(ea + ' :' + temp.nodeType + ',' + temp.nodeName);
					}
					idxTag = this._SetCfiAttributeImpl(temp, cfi + '/' + idxCfi, idxTag);
					temp = temp.nextSibling;
				}
			}
			catch (ex) {
				ePubEngineLogJS.W.log(ex);
			}

			return idxTag;
		},
		_search_fast_impl: function(keyword, spineDocument, resultTextMaxCount) {
			var jsonReturn = {};
            jsonReturn['items'] = new Array();

			try {
				var strContent = spineDocument.body.textContent;
				var strRexSearchWord = '';
				var strRexGapString = '[ \n\r\t\f\v\x0b\xa0]{0,}';

				// check
				if (keyword.length > resultTextMaxCount) {
					jsonReturn['error_code'] = -1;
					jsonReturn['error_msg'] = 'search text too long';
					jsonReturn['total_cnt'] = 0;
					jsonReturn['curr_cnt'] = 0;
					return jsonReturn; //너무 길어서 사용할 수 없음.
				}
				if (keyword.length < 2) {
					jsonReturn['error_code'] = -2;
					jsonReturn['error_msg'] = 'search text too short';
					jsonReturn['total_cnt'] = 0;
					jsonReturn['curr_cnt'] = 0;
					return jsonReturn; //너무 길어서 사용할 수 없음.
				}


				// 0. make match word string
				for (var idxStr = 0; idxStr < keyword.length; idxStr++) {
					if (null != keyword.substr(idxStr, 1).match(new RegExp("[\$\(\)\*\+\.\[\?\\\^\{\|]"))) {
						strRexSearchWord += '\\' + keyword.substr(idxStr, 1);
					}
					else {
						strRexSearchWord += keyword.substr(idxStr, 1);
					}
					strRexSearchWord += strRexGapString;
				}
				strRexSearchWord = strRexSearchWord.substring(0, strRexSearchWord.length - strRexGapString.length);
				//Timer('0. make match word string','E',2);

				//ePubEngineLogJS.W.log('search Regex : ' + strRexSearchWord);

				// 1. make array find word list
				var reg = new RegExp(strRexSearchWord, "gim");
				var arrMatchString = strContent.match(reg);

				var tagTotal = this.SetCfiAttribute(spineDocument);

				// 2. make array find position list

				var nIndexOf_StartPos = 0;
				var nIndexOf_FindPos = 0;
				var nIdxPosition = 0;
				var arrSearchPosition = new Array(); // match count *2 ( start, end )
				
				if (arrMatchString != null) {
					for (var nIdxMatch = 0; nIdxMatch < arrMatchString.length; nIdxMatch++) {
						var strFindString = arrMatchString[nIdxMatch];
						nIndexOf_FindPos = strContent.indexOf(strFindString, nIndexOf_StartPos);

						arrSearchPosition[nIdxPosition] = new Array();
						arrSearchPosition[nIdxPosition]['pos'] = nIndexOf_FindPos;
						arrSearchPosition[nIdxPosition]['findpage'] = false;
						nIdxPosition++;

						arrSearchPosition[nIdxPosition] = new Array();
						arrSearchPosition[nIdxPosition]['pos'] = nIndexOf_FindPos + strFindString.length;
						arrSearchPosition[nIdxPosition]['findpage'] = false;
						nIdxPosition++;

						nIndexOf_StartPos = nIndexOf_FindPos + strFindString.length;
					}
				}
				//Timer('2. make array find position list','E',2);

				// 3. position to cfi array
				//Timer('3. position to cfi array','S',2);
				if (arrMatchString != null) {
					//var iTextNode = document.evaluate("/html/body//text()", document.body, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
					var iTextNode = document.evaluate(".//text()", spineDocument.body, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
					var nSumTotalCount = 0;
					var nSumCurrentStartCount = 0;
					var objPosition = null;
					
					var nIdxArray = 0;
					var nIdxPosition = 0;
					var objTextNode_Idx = 0;
					var nCurrentNodeValueLen = 0;

					for (objTextNode_Idx = 0; objTextNode_Idx < iTextNode.snapshotLength; objTextNode_Idx++) {
						objTextNode = iTextNode.snapshotItem(objTextNode_Idx);

						nCurrentNodeValueLen = objTextNode.nodeValue.length
						nSumTotalCount += nCurrentNodeValueLen;

						//ePubEngineLogJS.L.log('[' + objTextNode.nodeValue.length + '/' + nSumTotalCount + '] ' + objTextNode.nodeValue);

						while (arrSearchPosition.length > nIdxPosition) {

							//ePubEngineLogJS.L.log('[' + nIdxPosition + '/' + arrSearchPosition.length  + '] ');
							//ePubEngineLogJS.L.log('[' + arrSearchPosition[nIdxPosition]['pos'] + '/' + nSumTotalCount + '] ');

							if (arrSearchPosition[nIdxPosition]['pos'] <= nSumTotalCount) {

								//ePubEngineLogJS.L.log('TRUE [' + arrSearchPosition[nIdxPosition]['pos'] + '] ');

								// 여기서 기존 encodeCfi를 구하면 시간이 너무 오래 걸려서, cfi를 속성으로 추가한후에 구함 (이게 바로 FAST CFI)
								arrSearchPosition[nIdxPosition]['cfi'] = objTextNode['cfi'] + ':' + (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount);
								arrSearchPosition[nIdxPosition]['offset'] = (arrSearchPosition[nIdxPosition]['pos'] - nSumCurrentStartCount);
								arrSearchPosition[nIdxPosition]['selector'] = Position._getPath(objTextNode);

								//ePubEngineLogJS.L.log('FALSE [' + arrSearchPosition[nIdxPosition]['pos'] + '] ');


								// 위치 번호 구하기
								arrSearchPosition[nIdxPosition]['tagPos'] = parseInt( parseFloat(objTextNode['tagPos']) / parseFloat(tagTotal) * 100);
								

								// 다음 검색된 텍스트 위치
								nIdxPosition++;
								continue;
							}
							else {
								// 범위벗어나면, 다음 텍스트 노드 가져옴
								nSumCurrentStartCount += nCurrentNodeValueLen;
								break;
							}
						}
					}
				}

				// 4. 결과셋만들기 및 텍스트 추출

				if (arrMatchString != null) {
					for (var i = 0; i < arrSearchPosition.length; i = i + 2) {
						jsonReturn['items'][i / 2] = {};
						jsonReturn['items'][i / 2]['cfi_start'] = arrSearchPosition[i]['cfi'];
						jsonReturn['items'][i / 2]['cfi_end'] = arrSearchPosition[i + 1]['cfi'];
						jsonReturn['items'][i / 2]['selector'] = arrSearchPosition[i]['selector'];
						jsonReturn['items'][i / 2]['startOffset'] = arrSearchPosition[i]['offset'];
						jsonReturn['items'][i / 2]['endOffset'] = arrSearchPosition[i + 1]['offset'];
						jsonReturn['items'][i / 2]['percent'] = arrSearchPosition[i]['tagPos'];

						var nTextLen = parseInt(arrSearchPosition[i + 1]['pos'] - arrSearchPosition[i]['pos']);
						var nTextGap = parseInt((resultTextMaxCount - nTextLen) / 2);

						var strHeaderLimit = '';
						if (arrSearchPosition[i]['pos'] - (nTextGap * 2) <= 0) {
							strHeaderLimit = strContent.substring(0, arrSearchPosition[i]['pos']).replace(/[\n\r\t\f\x0b\xa0]/gm, ""); // 유의 substring
						}
						else {
							strHeaderLimit = strContent.substr(arrSearchPosition[i]['pos'] - (nTextGap * 2), nTextGap * 2)// 유의 substring
						}
						strHeaderLimit = strHeaderLimit.replace(/[\n\r\t\f\x0b\xa0]/gm, "");
						var strFooterLimit = strContent.substr(arrSearchPosition[i + 1]['pos'], nTextGap * 2).replace(/[\n\r\t\f\x0b\xa0]/gm, ""); // gap 2배만큼 많은 텍스트를 얻어와서, 텍스트만 추출해서 Gap만큼만 가져옴
						jsonReturn['items'][i / 2]['result_text'] = strHeaderLimit + Search.findTextHeaderTag + strContent.substr(arrSearchPosition[i]['pos'], nTextLen) + Search.findTextFooterTag + strFooterLimit;

						//ePubEngineLogJS.L.log('id : ' + i + ', ' + jsonReturn['items'][i / 2]['cfi_start'] + ', ' + jsonReturn['items'][i / 2]['cfi_end']);
					}
				}


				jsonReturn['error_code'] = 0;
				jsonReturn['error_msg'] = '';
				jsonReturn['total_cnt'] = (arrMatchString != null && arrMatchString.length > 0) ? arrMatchString.length : 0;
				jsonReturn['curr_cnt'] = (jsonReturn['items'] != null && jsonReturn['items'].length > 0) ? jsonReturn['items'].length : 0;
				return jsonReturn;
			}
			catch (ex) {
				ePubEngineLogJS.E.log(ex);
				jsonReturn['error_code'] = -1;
				jsonReturn['error_msg'] = '';
				return jsonReturn;
			}
		},
	};


	var Position = Core.Position = {
		/**
		 * 마지막 읽은 위치
		 * @returns object {percent: lastReadPosSelector, path: lastReadPosOffset}
		 */
		getLastReadPos: function() {
			cfi = null;
	
			try {
				var total = ePubFxEngineLoaderJS.Data.getTotalSpine();
				var cur = ePubFxEngineLoaderJS.Data.getLeftSpine();
				var curItem = ePubFxEngineLoaderJS.Data.getSpineItem(cur);

				var currPercent = (cur + 1) * 100 / total;

				ePubEngineLogJS.W.log('getLastReadPos : ' + currPercent + ', curItem : ' + curItem.orgpath);

				return {percent: currPercent, path: curItem.orgpath};
			}
			catch(ex) {
				ePubEngineLogJS.W.log(ex);
				return {percent: 0, path: ''};
			}
			finally {
				cfi = null;
			}
		},
		getLastPageCfi: function() {
			var i = null, i2 = null;
			var caretRangeStart = null;
			var caretRangeEnd = null;
			var strRtnArray = new Array();
			var strRtnLog_Debug = null;
			var range = null;
			var maxNumber = 30;
			var lastPos = Position.getLastReadPos();
	
			try {
	
				strRtnArray[0] = new Array(); //'cfi'
				strRtnArray[1] = ''; //'text'
	
				// $('.viewer_wrapper').css('-webkit-user-select', 'auto');
	
				if ( ele_topmenu_container.is(":visible") ) {
					ele_topmenu_container.hide();
					ele_topmenu_container.attr('data', '1');
				}
	
				if( ele_bottommenu_container.is(":visible") ) {
					ele_bottommenu_container.hide();
					ele_bottommenu_container.attr('data', '1');
				}

				//viewer warrper의 좌표를 찾는다.
				var findX = 10;
				var findY = 10;
				var findX1 = ePubFxEngineLoaderJS.Data.getLefFrame().width() - 1;
				var findY1 = ePubFxEngineLoaderJS.Data.getLefFrame().height() - 1;
				
				caretRangeStart = Util.caretRangeFromPoint(ePubFxEngineLoaderJS.Data.getLefFrame().get(0).contentDocument, findX, findY);
				caretRangeEnd = Util.caretRangeFromPoint(ePubFxEngineLoaderJS.Data.getLefFrame().get(0).contentDocument, findX1, findY1);
	
				if( ele_topmenu_container.attr('data') == '1' ) {
					ele_topmenu_container.show();
					ele_topmenu_container.attr('data', '');
				}
	
				if( ele_bottommenu_container.attr('data') == '1' ) {
					ele_bottommenu_container.show();
					ele_bottommenu_container.attr('data', '');
				}
	
				// $('.viewer_wrapper').css('-webkit-user-select', 'none');
	
				if (caretRangeStart == null && caretRangeEnd == null) {
					//console.log(' caretRangeFromPoint: null');
					strRtnLog_Debug = '스캔해도 CFI를 찾을 수 없음';
					return strRtnArray;
				}

				strRtnArray[1] =  Position._GetText(i, caretRangeStart.startContainer, 0, i2, caretRangeEnd.endContainer, caretRangeEnd.endOffset);
				
				strRtnArray[1] = strRtnArray[1].replace(/[\n\t\r]{1,}/gi, '');
				strRtnArray[1] = strRtnArray[1].replace(/\s{2,}/gi, ' ');

				if (strRtnArray[1].length > caretRangeStart.startOffset && strRtnArray[1].length > (maxNumber + caretRangeStart.startOffset)) {
					strRtnArray[1] = strRtnArray[1].substr(0, maxNumber);
				}

				if(strRtnArray[1].length == 0) {
					strRtnArray[1] = ePubFxEngineLoaderJS.Data.getLefFrame().get(0).contentDocument.body.innerText;

					strRtnArray[1] = strRtnArray[1].replace(/[\n\t\r]{1,}/gi, '');
					strRtnArray[1] = strRtnArray[1].replace(/\s{2,}/gi, ' ');

					if (strRtnArray[1].length > caretRangeStart.startOffset && strRtnArray[1].length > (maxNumber + caretRangeStart.startOffset)) {
						strRtnArray[1] = strRtnArray[1].substr(0, maxNumber);
					}
				}

				if(caretRangeStart != null)
					i = this._encodeCFI(document, caretRangeStart.startContainer, caretRangeStart.startOffset, "");
	
				if(caretRangeEnd != null)
					i2 = this._encodeCFI(document, caretRangeEnd.endContainer, caretRangeEnd.endOffset, "");
				
				if (i != null) {
					strRtnArray[0][4] = lastPos.percent;
					strRtnArray[0][3] = lastPos.path;
					strRtnArray[0][2] = caretRangeStart.startOffset;
					strRtnArray[0][1] = i;
					strRtnArray[0][0] = this._getPath(caretRangeStart.startContainer);
					strRtnLog_Debug = i;
					//console.log(strRtnArray);
					return strRtnArray;
				}
				else {
					if(i2 != null) {
						strRtnArray[0][4] = lastPos.percent;
						strRtnArray[0][3] = lastPos.path;
						strRtnArray[0][2] = caretRangeEnd.endOffset;
						strRtnArray[0][1] = i2;
						strRtnLog_Debug = i2;
						strRtnArray[0][0] = this._getPath(caretRangeEnd.startContainer);
						//console.log(strRtnArray);
						return strRtnArray;
					}
					else {
						strRtnLog_Debug = '스캔해도 CFI를 찾을 수 없음';
						return strRtnArray;
					}
				}
	
			}
			catch (ex) {
				//console.log(ex);
				strRtnLog_Debug = '예외로 인한 실패';
				return strRtnArray;
			}
			finally {
				i = null;
				i2 = null;
				caretRangeStart = null;
				caretRangeEnd = null;
				range = null;
				strRtnArray = null;
				strRtnLog_Debug = null;
			}
		},
		/**
		 * 해당 노드를 jQuery Selector로 변환
		 * @param {node} selectedNode 선택 노드
		 * @returns 문자열 jQuery Selector
		 */
		_getPath: function(selectedNode) {
			var path = '';
			var node = $(selectedNode);

			while(node.length) {
				var realNode = node[0];
				var name = realNode.localName;

				if(realNode.nodeType == 3 || realNode.nodeType == 8) {
					node = node.parent();
					continue;
				}

				if(!name) break;

				name = name.toLowerCase();

				if(name == 'body') break;

				if(realNode.id != null && realNode.id != undefined && realNode.id != '') {

					if(realNode.id.indexOf('bdb-chapter') >= 0) {
						break;
					}

					return name + '#' + realNode.id.replace(/\./g, '\\.') + ( path ? '>' + path : '');
				}

				var parent = node.parent();

				var siblings = parent.children(name.replace(':','\\:'));

				if(siblings.length > 1) {
					name +=':eq('+siblings.index(realNode)+')';
				}
				
				path = name + (path ? '>' + path : '');
				node = parent;
			}

			return path;
		},
		_PlusCfiCharIndex: function(strCfi, nAddNumber) {
			var isDebug = false;
			var arrI = new Array();
			var temp_number = 0;
			var rtn = null;
			try {
				if (nAddNumber == 0) {
					return strCfi;
				}
	
				if (strCfi.indexOf(':') == -1) {
					return strCfi;
				}
	
				arrI = strCfi.split(':');
	
				rtn = this._decodeCFI(document, strCfi);
				if (rtn == null) {
					return strCfi;
				}
	
				if (nAddNumber > 0) {
					if (parseInt(arrI[1]) + 1 < rtn.node.nodeValue.length) {
						return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
					}
					else {
						// 텍스트 범위 벗어남
						if (parseInt(arrI[1]) == 0)
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
						else
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) - 1);
					}
				}
				else {
					if (parseInt(arrI[1]) + 1 < rtn.nodeValue.length) {
						// 텍스트 범위 벗어남
						return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
					}
					else {
						if (parseInt(arrI[1]) == 0)
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) + 1);
						else
							return arrI[0] + ':' + parseInt(parseInt(arrI[1]) - 1);
					}
				}
			}
			catch (ex) {
				// console.log(ex);
				return strCfi;
			}
			finally {
				isDebug = null;
				arrI = null;
				temp_number = null;
				rtn = null;
			}
		},
		_CheckCfiCompare: function (strCfi_A, strCfi_B) {
			var isDebug = false;
			var arrRtn = new Array();
			var arrA = null;
			var arrB = null;
			var nMinLength = null;
			var isRevert = null;
			var idx = null;
			var arrSubA = null;
			var arrSubB = null;
			try {
				if (strCfi_A == strCfi_B) {
					arrRtn['Big'] = strCfi_A;
					arrRtn['Small'] = strCfi_B;
					return arrRtn;
				}

				arrA = strCfi_A.split('/');
				arrB = strCfi_B.split('/');
				nMinLength = (arrA.length > arrB.length) ? arrB.length : arrA.length;
				isRevert = false;
				for (idx = 0; idx < nMinLength; idx++) {
					if (idx + 1 == nMinLength) {
						arrSubA = arrA[idx].split(':');
						arrSubB = arrB[idx].split(':');

						if (parseInt(arrSubA[0]) > parseInt(arrSubB[0])) {
							isRevert = true;
							break;
						}
						else if (arrSubA.length > 1 && arrSubB.length > 1
								 && parseInt(arrSubA[0]) == parseInt(arrSubB[0])
								 && parseInt(arrSubA[1]) > parseInt(arrSubB[1])) {
							isRevert = true;
							break;
						}
						else {
							break;
						}
					}
					else if (parseInt(arrA[idx]) == parseInt(arrB[idx])) {
						continue;
					}
					else if (parseInt(arrA[idx]) > parseInt(arrB[idx])) {
						isRevert = true;
						break;
					}
					else if (parseInt(arrA[idx]) < parseInt(arrB[idx])) {
						isRevert = false;
						break;
					}
				}

				if (isRevert) {
					arrRtn['Big'] = strCfi_B.replace('/b/g', 'a');
					arrRtn['Small'] = strCfi_A.replace('/b/g', 'b');
				}
				else {
					arrRtn['Big'] = strCfi_A;
					arrRtn['Small'] = strCfi_B;
				}
				return arrRtn;
			}
			finally {
				isDebug = null;
				arrRtn = null;
				arrA = null;
				arrB = null;
				nMinLength = null;
				isRevert = null;
				idx = null;
				arrSubA = null;
				arrSubB = null;
			}
		},
		/**
		 * cfi 로 특정 영역의 정보를 찾는다.
		 * @param {document} doc cfi가 해당하는 document
		 * @param {*} cfi Canonical Fragment Identifiers
		 * @returns 
		 */
		_pointFromCFI: function (doc, cfi) {
			if (cfi == null || cfi == '') {
				return null;
			}
	
			var isDebug = true;
			var r = null;
			var node = null;
			var ndoc = null;
			var nwin = null;
			var tl_x = null;
			var tl_y = null;
			var br_x = null;
			var br_y = null;
			var range = null;
			var tryList = null;
			var k = null;
			var a = null;
			var nodeLen = null;
			var t = null;
			var startOffset = null;
			var endOffset = null;
			var rect = null;
	
			var strRtnLog_Debug = null;
			try {
				r = this._decodeCFI(doc, cfi);
				if (!r || r == '') {
					strRtnLog_Debug = '_decodeCFI fail';
					return null;
				}
	
				node = r.node;
				ndoc = node.ownerDocument;
				if (!ndoc) {
					strRtnLog_Debug = 'ownerDocument fail';
					node = null;
					ndoc = null;
					return null;
				}
	
				nwin = ndoc.defaultView;
				if (typeof r.offset == "number") {
					range = ndoc.createRange();
					tryList = null;
					if (r.forward) {
						tryList = [{ start: 0, end: 0, a: 0.5 }, { start: 0, end: 1, a: 1 }, { start: -1, end: 0, a: 0 }];
					}
					else {
						tryList = [{ start: 0, end: 0, a: 0.5 }, { start: -1, end: 0, a: 0 }, { start: 0, end: 1, a: 1 }];
					}
	
					k = 0;
					nodeLen = node.nodeValue.length;
					do {
						if (k >= tryList.length) {
							strRtnLog_Debug = 'tryList.length fail';
							return null;
						}
	
						t = tryList[k++];
						startOffset = r.offset + t.start;
						endOffset = r.offset + t.end;
						a = t.a;
						//if( startOffset < 0 || endOffset >= nodeLen ) {
						if (startOffset < 0 || endOffset > nodeLen) {
							continue;
						}
						range.setStart(node, startOffset);
						range.setEnd(node, endOffset);
						rects = range.getClientRects();
					}
					while (!rects || !rects.length);
	
					rect = rects[0];
	
					tl_t = rect.top;
					tl_l = rect.left;
					br_b = rect.bottom;
					br_r = rect.right;
					rect = null;
					k = null;
					a = null;
					nodeLen = null;
				}
				else {
					tl_t = node.offsetTop;
					tl_l = node.offsetLeft - nwin.scrollX;
					br_b = node.offsetBottom;
					br_r = node.offsetRight - nwin.scrollX;
	
					if (typeof r.x == "number" && node.offsetWidth) {
						tl_t += (r.x * node.offsetWidth) / 100;
						tl_l += (r.y * node.offsetHeight) / 100;
					}
				}
	
				// if (ndoc != document) {
				// 	strRtnLog_Debug = 'ndoc != document fail';
				// 	return null;
				// }
	
				return { point: r, tl_t: tl_t, tl_l: tl_l, br_b: br_b, br_r: br_r };
			}
			catch (ex) {
				throw ex;
			}
			finally {
				isDebug = null;
				r = null;
				node = null;
				ndoc = null;
				nwin = null;
				tl_x = null;
				tl_y = null;
				br_x = null;
				br_y = null;
				range = null;
				tryList = null;
				k = null;
				a = null;
				nodeLen = null;
				t = null;
				startOffset = null;
				endOffset = null;
				rect = null;
				strRtnLog_Debug = null;
			}
		},
		_encodeCFISingleShot: function (doc, node, offset, tail) {
			var isDebug = false;
			var cfi = tail || "";
			var p = null;
			var parent = null;
			var win = null;
			var index = null;
			var child = null;
			try {

				switch (node.nodeType) {
					case 1:
						if (typeof offset == "number") {
							if (node.childNodes.item(offset) != null)
								node = node.childNodes.item(offset);
							else {
								if (node.childNodes.length > 0)
									node = node.childNodes.item(node.childNodes.length - 1);
							}
						}
						break;
					default:
						offset = offset || 0;
						while (true) {
							p = node.previousSibling;
							if (!p || p.nodeType == 1) {
								break;
							}
							// CFI r3002
							switch (p.nodeType) {
								case 3:
								case 4:
								case 5:
									offset += p.nodeValue.length;
							}
							node = p;
						}
						cfi = ":" + offset + cfi;
						break;
				}

				while (node !== doc) {
					if (node == null)
						break;

					parent = node.parentNode;
					if (!parent) {
						if (node.nodeType == 9) {
							win = node.defaultView;
							if (win.frameElement) {
								node = win.frameElement;
								cfi = "!" + cfi;
								continue;
							}
						}
						break;
					}


					index = 0;
					child = parent.firstChild;
					while (true) {
						index |= 1;
						if (child.nodeType == 1) {
							if (child.toString() !== '[object HTMLUnknownElement]'){
								index++
							}
						}
						if (child === node) {
							break;
						}
						child = child.nextSibling;
					}
					// CFI r3002
					//if( node.id && node.id.match(/^[-a-zA-Z_0-9.\u007F-\uFFFF]+$/) )
					//{
					//  index = index + "[" + node.id + "]";
					//}
					cfi = "/" + index + cfi;
					node = parent;
				}

				return cfi;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][this._encodeCFI] ' + (ex.number & 0xFFFF));
				ePubEngineLogJS.W.log('[★Exception★][this._encodeCFI] ' + ex);
				return '';
			}
			finally {
				isDebug = null;
				cfi = null;
				p = null;
				parent = null;
				win = null;
				index = null;
				child = null;
			}
		},
		_encodeCFI: function (doc, node, offset, tail) {

			var isDebug = false;
			var cfi = tail || "";
			var p = null;
			var parent = null;
			var win = null;
			var index = null;
			var child = null;
			try {

				switch (node.nodeType) {
					case 1:
						if (typeof offset == "number") {
							if (node.childNodes.item(offset) != null)
								node = node.childNodes.item(offset);
							else {
								if (node.childNodes.length > 0)
									node = node.childNodes.item(node.childNodes.length - 1);
							}
						}
						break;
					default:
						offset = offset || 0;
						while (true) {
							p = node.previousSibling;
							if (!p || p.nodeType == 1) {
								break;
							}
							// CFI r3002
							switch (p.nodeType) {
								case 3:
								case 4:
								case 5:
									offset += p.nodeValue.length;
							}
							node = p;
						}
						cfi = ":" + offset + cfi;
						break;
				}

				while (node !== doc) {
					if (node == null)
						break;

					parent = node.parentNode;
					if (!parent) {
						if (node.nodeType == 9) {
							win = node.defaultView;
							if (win.frameElement) {
								node = win.frameElement;
								cfi = "!" + cfi;
								continue;
							}
						}
						break;
					}


					index = 0;
					child = parent.firstChild;
					while (true) {
						index |= 1;
						if (child.nodeType == 1) {
							index++
						}
						if (child === node) {
							break;
						}
						child = child.nextSibling;
					}
					// CFI r3002
					//if( node.id && node.id.match(/^[-a-zA-Z_0-9.\u007F-\uFFFF]+$/) )
					//{
					//  index = index + "[" + node.id + "]";
					//}
					cfi = "/" + index + cfi;
					node = parent;
				}

				return cfi;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][this._encodeCFI] ' + (ex.number & 0xFFFF));
				ePubEngineLogJS.W.log('[★Exception★][this._encodeCFI] ' + ex);
				return '';
			}
			finally {
				isDebug = null;
				cfi = null;
				p = null;
				parent = null;
				win = null;
				index = null;
				child = null;
			}
		},
		_decodeCFI: function (doc, cfi) {


			if (cfi == null || cfi == '') {
				return null;
			}

			var isDebug = true;
			var node = doc;
			var error = null;
			var r = null;
			var targetIndex = null;
			var index = null;
			var child = null;
			var offset = null;
			var point = {};
			var len = null;
			var next = null;
			var oldNode;// 마지막이 br 일경우를 위함 임시 저장소


			try {

				while (cfi.length > 0) {
					//if( (r = cfi.match(/^\/(\d+)/)) !== null )
					if ((r = cfi.match(/^\/(\d+)/)) !== null)
						// CFI r3002
						// if( (r = cfi.match(/^\/(\d+)(\[([-a-zA-Z_0-9.\u007F-\uFFFF]+)\])?/)) !== null )
					{
						targetIndex = r[1] - 0;
						index = 0;
						child = node.firstChild;
						try {
							while (true) {
								if (!child) {
									error = "child not found: " + cfi;
									ePubEngineLogJS.W.log('[Selection._decodeCFI] error : ' + error);
									return null;
									//break;
								}

								index |= 1;
								if (child.nodeType === 1) {
									index++;
								}

								if (index === targetIndex) {
									cfi = cfi.substr(r[0].length);
									node = child;

									// br로 끝난 다면 북마크를 위한 임시 텍스트 지정
									if (node.nodeName.toLowerCase() == 'br') {
										node = oldNode;
										node.setAttribute("bookMake", "bookMake");
									}
									break;
								}
								child = child.nextSibling;
								if (node.nodeName.toLowerCase() != 'br') {
									oldNode = child;
								}
							}
						}
						finally {
							child = null;
							targetIndex = null;
							index = null;
						}
					}
					else if ((r = cfi.match(/^!/)) !== null) {
						if (node.contentDocument) {
							node = node.contentDocument;
							cfi = cfi.substr(1);
						}
						else {
							error = "Cannot reference " + node.nodeName + "'s content: " + cfi;
						}
					}
					else {
						break;
					}
				}

				if ((r = cfi.match(/^:(\d+)/)) !== null) {
					offset = r[1] - 0;
					cfi = cfi.substr(r[0].length);
				}
				if ((r = cfi.match(/^~(-?\d+(\.\d+)?)/)) !== null) {
					point.time = r[1] - 0;
					cfi = cfi.substr(r[0].length);
				}
				if ((r = cfi.match(/^@(-?\d+(\.\d+)?),(-?\d+(\.\d+)?)/)) !== null) {
					point.x = r[1] - 0;
					point.y = r[3] - 0;
					cfi = cfi.substr(r[0].length);
				}
				if ((r = cfi.match(/^[ab]/)) !== null) {
					point.forward = r[0] == "a";
					cfi = cfi.substr(1);
				}
				// CFI r3002
				//if( (r = cfi.match(/^\[.*(;s=[ab])?.*\]/)) !== null ) // pretty lame
				//{
				//  if( r[1] )
				//  {
				//      point.forward = r[0] == "s=a";
				//      cfi = cfi.substr(1);
				//  }
				//}

				if (offset !== null) {
					while (true) {
						if (node.nodeValue == null) {
							if (node.childNodes.item(offset) != null)
								node = node.childNodes.item(offset);
							else {
								if (node.childNodes.length > 0)
									node = node.childNodes.item(node.childNodes.length - 1);
							}
							continue;
						}
						else {
							len = node.nodeValue.length;
							if (offset < len || (!point.forward && offset === len)) {
								len = null;
								break;
							}

							next = node.nextSibling;
						}

						if (next == null) {
							offset = len;
							break;
						}

						// CFI v3002 [
						while (true) {
							var type = next.nodeType;
							if (type === 3 || type === 4 || type === 5) {
								break;
							}
							if (type == 1) {
								next = null;
								break;
							}
							next = next.nextSibling;
						}
						//]
						if (!next) {
							if (offset > len) {
								error = "Offset out of range: " + offset;
								offset = len;
							}
							break;
						}
						node = next;
						offset -= len;
					}
					point.offset = offset;
				}

				if (error) {
					ePubEngineLogJS.W.log('[Selection._decodeCFI] error : ' + error);
					return null;
				}
				else if (cfi.length > 0) {
					ePubEngineLogJS.W.log('[Selection._decodeCFI] cfi.length > 0, cfi : ' + cfi);
					return null;
				}


				point.node = node;
				return point;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][Selection._decodeCFI] ' + ex);
				return null;
			}
			finally {
				isDebug = null;
				node = null;
				error = null;
				r = null;
				targetIndex = null;
				index = null;
				child = null;
				offset = null;
				point = null;
				len = null;
				next = null;
			}
		},
		_encodeXPath: function (doc, target) {
			var strXPath = '';

			if (target.parentNode == doc) {
				strXPath = '/HTML' + strXPath;
			}
			else {
				var siblingIdx = 0;
				var prevSiblingNode = target.previousSibling;

				while (prevSiblingNode != null) {
					if (prevSiblingNode.tagName == target.tagName)
						siblingIdx++;

					prevSiblingNode = prevSiblingNode.previousSibling;
				}

				if (siblingIdx > 0 && target.nodeType == 1) {
					strXPath = '/' + target.tagName + '[' + (siblingIdx + 1) + ']' + strXPath;
				}
				else if (siblingIdx == 0 && target.nodeType == 1) {
					strXPath = '/' + target.tagName + strXPath;
				}
				else if (siblingIdx == 0 && target.nodeType == 3) {
					strXPath = '/text()' + strXPath;
				}
				else if (siblingIdx > 0 && target.nodeType == 3) {
					strXPath = '/text()' + '[' + (siblingIdx + 1) + ']' + strXPath;
				}

				strXPath = this._encodeXPath(doc, target.parentNode) + strXPath;
			}

			return strXPath;
		},
		_cfiAt: function (doc, x, y, isWordBoundary) {
			var rtnInfo = null;

			if (doc == null || doc == '') {
				return rtnInfo;
			}

			var isDebug = true;
			var target = null;
			var endTarget = null;
			var cdoc = doc;
			var cwin = cdoc.defaultView;
			var tail = "";
			var offset = null;
			var startOffset = null;
			var endOffset = null;
			var name = null;
			var range = null;
			var px = null;
			var py = null;
			var selText = null;
			var rct = null;
			var parentOffsetX = 0, parentOffsetY = 0;

			var frameRatio = 1;

			rtnInfo = new Array();

			try {
				// [Comment1 Start]
				// while문은, x/y포지션 아래의 element가 iframe/object/embed 태그면, 그 내부의 포지션을 잡아내기 위해서
				// 찾아들어가는 코드, 결과적으로는, iframe이든 아니든 현재 선택될 엘리먼트를 찾아내는 코드
				while (true) {
					target = cdoc.elementFromPoint(x, y);
					if (!target) {
						return null;
					}
					//html이 지정되는 경우 return
					if (!target.parentElement){
						return null
					}
					if (target.parentElement.tagName === 'HTML'){
						return null;
					}
					

					name = target.localName;

					/*
					 * <p>&nbsp</p> 같은 경우 문제가 생기는걸 막기위해 .
					 * 처리하는 부분 공백문자 단순 /br 등
					 *
					 */
					if (name == "iframe" ) {
						var cd = target.contentDocument;
						if (!cd) {
							break;
						}

						frameRatio = parseFloat(target.getAttribute('zdt'));

						rct = target.getBoundingClientRect();

						parentOffsetX = cwin.scrollX + rct.left;
						parentOffsetY = cwin.scrollY + rct.top;
						x = (x - parentOffsetX);// * frameRatio;
						y = (y - parentOffsetY); // * frameRatio;
						
						cdoc = cd;
						cwin = cdoc.defaultView;

						target = cdoc.elementFromPoint(x, y);
						if (!target) {

							return null;
						}

						console.warn('iframe cfiat, ', target);

						cd = null;
						break;
					}

					var innerText = Util.Trim(target.innerText);
					if (innerText.length == 0) {
						return null;
					}
					
					if (name != "iframe" && name != "object" && name != "embed") {
						break;
					}
					var cd = target.contentDocument;
					if (!cd) {
						break;
					}

					//x = x + cwin.scrollX - target.offsetLeft;
					//y = y + cwin.scrollY - target.offsetTop;
					cdoc = cd;
					cwin = cdoc.defaultView;

					cd = null;
				}
				// [Comment1 End]

				// [Comment2 Start]
				// 엘리먼트는 최종적으로 찾았음. 그 엘리먼트에 따른 처리
				if (name == "video" || name == "audio") {
					//tail = "~" + Position._fstr(target.currentTime);
				}

				if (name == "img" || name == "video") {
					//px = ((x + cwin.scrollX - target.offsetLeft)*100)/target.offsetWidth;
					//py = ((y + cwin.scrollY - target.offsetTop)*100)/target.offsetHeight;
					//tail = tail + "@" + Position._fstr(px) + "," + Position._fstr(py);
					//px = null;
					//py = null;
				}
				else if (name != "audio") {

					if (cdoc.caretRangeFromPoint) {
						range = cdoc.caretRangeFromPoint(x, y);
						if (range) {
							target = range.startContainer;
							offset = range.startOffset;

							selText = target.textContent;
							ePubEngineLogJS.L.log("selText = " + selText + "offset = " + offset);

							/**
							 * redmine refs #14501
							 * 일본어 하이라이트 안되는 문제로 로직 수정
							 * by junghoon.kim
							 */
							if (selText != null) {
								if (isWordBoundary) {
									if (selText.lastIndexOf(' ', offset) >= 0) {
										startOffset = selText.lastIndexOf(' ', offset);
										ePubEngineLogJS.L.log("isWordBoundary startOffset = " + startOffset);
										startOffset += 1;
									}
									else {
										startOffset = offset;
									}
								}
								else {
									startOffset = offset;
								}

								ePubEngineLogJS.L.log('startOffset : ' + startOffset);

								if (isWordBoundary) {
									if (selText.indexOf(' ', offset + 1) > offset) {
										endOffset = selText.indexOf(' ', offset + 1)
										ePubEngineLogJS.L.log('endOffset !! = ' + endOffset);
									}
									else {
										var endPointIndex = selText.search(/[.?!]/g);

										if (endPointIndex != -1) {
											endOffset = selText.length;
										}
										else if (endOffset + 1 < selText.length) {
											endOffset = offset + 1;
										}
										else {
											endOffset = offset;
										}
									}
								}
								else {
									endOffset = offset;
									ePubEngineLogJS.L.log('endOffset ## = ' + endOffset);
								}

								//ePubEngineLogJS.L.log('endOffset : '+endOffset);
							}
							else {
								startOffset = offset;
								endOffset = offset;
							}



						}
						else {
						}
						range = null;
					}
					else {
					}
				}
				else {
				}

				rtnInfo[2] = startOffset;
				rtnInfo[1] = this._encodeXPath(cdoc, target);

				rtnInfo[5] = endOffset;
				rtnInfo[4] = this._encodeXPath(cdoc, target);

				rtnInfo[0] = this._encodeCFI(cdoc, target, startOffset, tail);
				rtnInfo[3] = this._encodeCFI(cdoc, target, endOffset, tail);

				rtnInfo[6] = this._encodeCFI(cdoc, target, offset, tail);
				rtnInfo[7] = this._encodeXPath(cdoc, target);
				rtnInfo[8] = offset;
				rtnInfo[9] = cdoc;
				rtnInfo[10] = parentOffsetX;
				rtnInfo[11] = parentOffsetY;

				//ePubEngineLogJS.L.log(rtnInfo[0]);
				return rtnInfo;
			}
			finally {
				isDebug = null;
				target = null;
				cdoc = null;
				cwin = null;
				tail = null;
				offset = null;
				name = null;
				range = null;
				px = null;
				py = null;
				selText = null;
				endOffset = null;
				startOffset = null;
				rtnInfo = null;
				rct = null;
			}
		},
		/**
		 * 정한 CFI 시작/끝 값으로 실제 DIV를 그릴 포지션 정보를 리턴 
		 * 무조건 start가 end보다 커야 한다.
		 * @param {document} doc cfi가 해당하는 document
		 * @param {string} strStartCfi 시작 CFI값
		 * @param {string} strEndCfi 끝 CFI값
		 * @param {boolean} isMakeText 텍스트값도 추출 (Optional)
		 * @param {boolean} isByPassSameCfi 같은 CFI여도 정보를 가져오기 (Optional)
		 * @returns 
		 */
		_GetDrawNodeByCfi: function (doc, strStartCfi, strEndCfi, isMakeText, isByPassSameCfi) {
			var isDebug = true;
			var startPoint = null;
			var endPoint = null;
			var start = null;
			var end = null;
			var strSelectionID = null;
			var range = null;
			var rects = null;
			var strSelectionText = null;
			var drawList = null;
			var drawList_Idx = null;
			var k = null;
			var rect = null;
			var isDual = null;
			var rect_next = null;
			var dual_idx = null;
			var bookMakeTempNode = null;
			var iframes = null;
			var frameOffsetX = 0;
			var frameOffsetY = 0;
			var frameRect = null;
			var frameRatio = 1;

			try {
				if (strStartCfi == strEndCfi && isByPassSameCfi == null) {
					ePubEngineLogJS.E.log('시작과 끝이 같습니다.');
					return -7;
				}

				iframes = document.getElementsByTagName('iframe');

				if(iframes) {
					for(var frame of iframes) {
						if(frame.contentDocument == doc) {
							frameRect = frame.getBoundingClientRect();

							frameOffsetX = frameRect.left;
							frameOffsetY = frameRect.top;
							frameRatio = parseFloat(frame.getAttribute('zdt'));
						}
					}
				}


				// draw calc
				startPoint = this._pointFromCFI(doc, strStartCfi);
				endPoint = this._pointFromCFI(doc, strEndCfi);

				if (startPoint == null || endPoint == null) {
					console.error(strStartCfi, strEndCfi);
					ePubEngineLogJS.E.log('계산할 수 없습니다.(-5)');
					return -5;
				}

				start = startPoint.point;
				end = endPoint.point;
				if (start == null || end == null) {
					startPoint = null;
					endPoint = null;
					ePubEngineLogJS.E.log('계산할 수 없습니다.(-6)');
					return -6;
				}


				/**
				 * redmine refs #12638
				 * 챕터 처음의 cfi를 인식 하지 못해서 챕터 끝으로 이동됨
				 * 
				 */
				if ((ele_view_wrapper.get(0) == start.node || start.node == doc.getElementsByTagName("BODY")[0])) {
					return -9; // body 및 container 영역
				}

				var bookMake = false;
				try {
					if (start.node.nodeName != '#text') {
						bookMake = (start.node.getAttribute("bookMake") != undefined) ? true : false;
					}
				} catch (e) {
					console.log(e.toString());
				}
				if (bookMake) {
					bookMakeTempNode = doc.createTextNode("BookMake");
					start.node.appendChild(bookMakeTempNode);

				}

				strSelectionID = 'Rue_Selection_Container_SEL';
				range = doc.createRange();

				if (bookMake && start.node == end.node && start.offset == end.offset) {
					range.selectNodeContents(start.node);
				}
				else {

					if (start.node == end.node && start.offset == end.offset) {
						range.selectNode(start.node);
					}
					else {
						if (typeof start.offset == "number") {
							range.setStart(start.node, start.offset);
						}
						else {
							range.setStartBefore(start.node);
						}

						if (typeof end.offset == "number") {
							range.setEnd(end.node, end.offset);
						}
						else {
							range.setEndBefore(end.node);
						}
					}

				}

				if (isMakeText) {
					strSelectionText = this._GetText(strStartCfi, start.node, start.offset, strEndCfi, end.node, end.offset);
				}
				else {
					strSelectionText = '';
				}

				rects = range.getClientRects();
				drawList = new Array();
				drawList_Idx = 0;

				for (k = 0; k < rects.length; k++) {
					rect = rects[k];
					if (/*rect.top < 0 || */rect.width <= 0 || rect.height <= 0) {
						// 내용이 없는 경우 높이 값이 -가 오는 경우는 무시한다 .
						// 내용이 없기 때문에 표시할 필요가 없음.
						// 위치 정보는 - 가 올수 있으므로 조건에서 제외한다.
						continue;
					}


					// 특정 노드에서 rect가 더 많이 넘어오는데 endNode의 right 보다 벗어난 rect는 제거 하기 위해
					if (endPoint.tl_t == rect.top && endPoint.br_b == rect.bottom) {
						if (rect.left > endPoint.br_r) {
							continue;
						}
					}
					/**
					 * redmine refs #16666
					 * br 테그가 있으면 br 바로앞 영역을 건너 뛰어서 셀렉션 되지 않는 문제 수정
					 * 이중으로 셀렉션 되는 문제 방어 코드
					 * by junghoon.kim
					 */
					if (true) {
						isDual = false;
						for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
							// across check
							rect_next = rects[dual_idx];
							if (rect_next) {
								// across check
								if (rect.top >= rect_next.top
									&& rect.bottom <= rect_next.bottom
									&& rect.left <= rect_next.left
									&& rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										 && rect.bottom >= rect_next.bottom
										 && rect.left >= rect_next.left
										 && rect.right <= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								else if (rect.top <= rect_next.top
										 && rect.bottom >= rect_next.bottom
										 && rect.left <= rect_next.left
										 && rect.right >= rect_next.right && rect_next.width != 0) {
									isDual = true;
									break;
								}
								//range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
							}
						}
						if (isDual) {
							continue;
						}
					}
					else {
						// 하이라이트 시 해당 셀렉션 영역에 br 테그가 있으면 아래 로직에 의해 br 바로앞 영역이 패스 되어
						// 화면에 그려지지 않는 버그로 인해 주석 처리함
						// 아래 로직이 dual 모드시 동작되는 로직 같은데 정확히 파악된 로직은 아니라
						// 다른 사이드 이펙트가 없는지 많은 검토가 필요함

						//                            //kyungmin ios 이미지 사이즈 버그로 인한 수정
						//                            isDual = false;
						//                            for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
						//                                // across check
						//                                rect_next = rects[dual_idx];
						//                                if (rect_next) {
						//                                    // across check
						//                                    if (rect.top >= rect_next.top
						//                                        && rect.bottom <= rect_next.bottom
						//                                        && rect.left <= rect_next.left
						//                                        && rect.right >= rect_next.right) {
						//                                        isDual = true;
						//                                        break;
						//                                    }
						//                                    else if (rect.top <= rect_next.top
						//                                             && rect.bottom >= rect_next.bottom
						//                                             && rect.left >= rect_next.left
						//                                             && rect.right <= rect_next.right) {
						//                                        isDual = true;
						//                                        break;
						//                                    }
						//                                    else if (rect.top <= rect_next.top
						//                                             && rect.bottom >= rect_next.bottom
						//                                             && rect.left <= rect_next.left
						//                                             && rect.right >= rect_next.right) {
						//                                        isDual = true;
						//                                        break;
						//                                    }
						//                                    //range.compareBoundaryPoints(Range.END_TO_END,range1) >= 0)
						//                                }
						//                            }
						//                            if (isDual) {
						//                                continue;
						//                            }
					}

					if (rect.width == 0) {
						//LastReadCFI시 width가 0인 것 때문에 CFI 페이지 계산이 전페이지 나오는 현상제거
						ePubEngineLogJS.L.log("rect width is 0 ");
						continue;
					}

					//210803 - sjhan 공백 체크 로직 추가.
					var xx = parseFloat(rect.left)+(parseFloat(rect.width)/2);
					var yy = parseFloat(rect.top)+(parseFloat(rect.height)/2);
					var selnode = document.elementFromPoint(xx,yy);
					if (selnode){
						if (selnode.innerText){
							if (selnode.innerText.replace(/\s+/g,'').length <= 0){
								continue;
							}
						}
					}

					//ePubEngineLogJS.L.log('_GetDrawNodeByCfi2');
					drawList[drawList_Idx] = new Array();
					drawList[drawList_Idx]['id'] = parseInt(k);
					drawList[drawList_Idx]['top'] = (parseFloat(rect.top * frameRatio + frameOffsetY) + parseFloat(window.scrollY)) + "px";
					drawList[drawList_Idx]['rleft'] = parseFloat(rect.left  * frameRatio + frameOffsetX);

					drawList[drawList_Idx]['left'] = (parseFloat(rect.left  * frameRatio + frameOffsetX) + parseFloat(window.scrollX)) + "px";

					drawList[drawList_Idx]['width'] = parseFloat(rect.width * frameRatio) + "px";
					drawList[drawList_Idx]['height'] = parseFloat(rect.height * frameRatio)+ "px";
					drawList[drawList_Idx]['rect'] = {
						top: (parseFloat(rect.top * frameRatio + frameOffsetY) + parseFloat(window.scrollY)),
						left: (parseFloat(rect.left  * frameRatio + frameOffsetX) + parseFloat(window.scrollX)),
						width: parseFloat(rect.width * frameRatio),
						height: parseFloat(rect.height * frameRatio)
					}

					// ePubEngineLogJS.L.log('drawsList_idx = ' + drawList_Idx + ' width = ' + rect.width);
					if (drawList_Idx == 0) {//index의 0번에만 값을 채움
						drawList[drawList_Idx]['SelectionText'] = strSelectionText;
					}

					ePubEngineLogJS.L.log('[Position._GetDrawNodeByCfi] rect.top = ' + rect.top + ',   rect.left = ' + rect.left + ',   window.scrollX = ' + window.scrollX + ',   window.scrollY = ' + window.scrollY + ',   drawList[drawList_Idx][top] = ' + drawList[drawList_Idx]['top'] + ',   drawList[drawList_Idx][left] = ' + drawList[drawList_Idx]['left']);
					drawList_Idx++;
				}
				if (bookMake) {
					start.node.removeChild(bookMakeTempNode);
				}
				if (drawList_Idx == 0) {
					return -8; //검색영역없음
				}

				return drawList;
			}
			catch (ex) {
				ePubEngineLogJS.W.log('[★Exception★][Position._GetDrawNodeByCfi] ' + (ex.number & 0xFFFF));
				ePubEngineLogJS.W.log('[★Exception★][Position._GetDrawNodeByCfi] ' + ex);
				return -100; //예외
			}
			finally {
				isDebug = null;
				startPoint = null;
				endPoint = null;
				start = null;
				end = null;
				strSelectionID = null;
				range = null;
				rects = null;
				strSelectionText = null;
				drawList = null;
				drawList_Idx = null;
				k = null;
				rect = null;
				isDual = null;
				rect_next = null;
				dual_idx = null;
			}
		},
		/**
		 * 
		 * @param {string} startCfi 시작위치
		 * @param {node} start_node 시작 노드
		 * @param {number} start_offset 시작 노드의 오프셋
		 * @param {string} endCfi 끝 위치
		 * @param {node} end_node 끝 노드
		 * @param {number} end_offset 끝 노드의 텍스트 오프셋
		 * @returns 추출된 텍스트
		 */
		_GetText: function (startCfi, start_node, start_offset, endCfi, end_node, end_offset) {
			var strText = '';
			var tmpRtnGetParent = '';
			var isDebug = false;
			var arrStart = startCfi.split(':')[0].split('/');
			var arrEnd = endCfi.split(':')[0].split('/');
			var isGetStartText = false;
			var currNode = null;
			var sameIndex = null;
			var StartNode_End = null;
			var EndNode_Start = null;
			var seq = null;
			var tmpStartNode = null;
			var tmpEndNode = null;
			var tempCurrNode = null;

			try {
				if (start_node === end_node) {
					//시작과 끝 노드가 동일
					return start_node.nodeValue.substr(start_offset, end_offset - start_offset);
				}

				// 어디까지 같은지 확인
				for (sameIndex = 0; sameIndex < arrStart.length; sameIndex++) {
					if (arrStart[sameIndex] == arrEnd[sameIndex]) {
						continue;
					}
					else {
						break;
					}
				}

				for (seq = arrStart.length; seq > sameIndex; seq--) {
					if (StartNode_End == null) {
						if (seq - 1 > sameIndex) {
							StartNode_End = start_node.parentNode;
						}
						else {
							StartNode_End = start_node.nextSibling;
							if (StartNode_End == null) {
								return 'not found';
							}
						}
					}
					else {
						if (seq - 1 > sameIndex) {
							StartNode_End = StartNode_End.parentNode;
						}
						else {
							StartNode_End = StartNode_End.nextSibling;
							if (StartNode_End == null) {
								return 'not found';
							}
						}
					}
				}

				for (seq = arrEnd.length; seq > sameIndex; seq--) {
					if (EndNode_Start == null) {
						if (seq - 1 > sameIndex) {
							EndNode_Start = end_node.parentNode;
						}
						else {
							EndNode_Start = end_node.previousSibling;
							if (EndNode_Start == null) {
								return 'not found';
							}
						}
					}
					else {
						if (seq - 1 > sameIndex) {
							EndNode_Start = EndNode_Start.parentNode;
						}
						else {
							EndNode_Start = EndNode_Start.previousSibling;
							if (EndNode_Start == null) {
								return 'not found';
							}
						}
					}
				}

				// 앞부분부터 중복위치 다음까지  ㄱㄱ

				for (seq = arrStart.length; seq > sameIndex; seq--) {
					if (!isGetStartText) {
						tmpRtnGetParent = this._GetParentText(start_node, start_offset, StartNode_End, true, true);
						strText += tmpRtnGetParent.toString();
						isGetStartText = true;
						currNode = start_node.parentNode;
						tmpRtnGetParent = null;
					}
					else {
						tmpRtnGetParent = this._GetParentText(currNode, null, StartNode_End, false, true);
						strText += tmpRtnGetParent;
						currNode = currNode.parentNode;
						tmpRtnGetParent = null;
					}
				}

				// 증복 다음처리  부분의 중간땅

				tmpStartNode = StartNode_End.previousSibling;
				tmpEndNode = EndNode_Start.nextSibling;
				if (tmpStartNode != tmpEndNode) {
					for (tempCurrNode = StartNode_End; tempCurrNode != null && tempCurrNode != tmpEndNode; tempCurrNode = tempCurrNode.nextSibling) {
						var isNotPassNode = false;
						
						if(tempCurrNode.nodeType == 3) {
							isNotPassNode = window.getComputedStyle(tempCurrNode.parentNode).display != 'none';
						}

						if(tempCurrNode.nodeType == 1) {
							isNotPassNode = window.getComputedStyle(tempCurrNode).display != 'none';

							if(isNotPassNode) {
								if(tempCurrNode.className) {
									isNotPassNode = tempCurrNode.className.indexOf('bdb_para_empty') == -1;
								}
							}
						}

						if (tempCurrNode.nodeType === 3 && isNotPassNode) {
							strText += tempCurrNode.nodeValue;
						}
						else if (tempCurrNode.nodeType === 8) {
							//#comment
						}
						else {
							if(isNotPassNode)
								strText += tempCurrNode.innerText;
						}
					}
				}
				tmpStartNode = null;
				tmpEndNode = null;

				// 마지막 처리
				isGetStartText = false;
				for (var seq = arrEnd.length; seq > sameIndex; seq--) {
					if (!isGetStartText) {
						tmpRtnGetParent = this._GetParentText(end_node, end_offset, EndNode_Start, true, false);
						strText += tmpRtnGetParent;
						tmpRtnGetParent = null;

						isGetStartText = true;
						currNode = end_node.parentNode;
					}
					else {
						if (EndNode_Start == ele_view_wrapper.get(0)) {
							break;
						}

						tmpRtnGetParent = this._GetParentText(currNode, null, EndNode_Start, false, false);
						strText += tmpRtnGetParent;
						tmpRtnGetParent = null;

						currNode = currNode.parentNode;
					}
				}
				return strText;
			}
			finally {
				isDebug = null;
				strText = null;
				tmpRtnGetParent = null;
				arrStart = null;
				arrEnd = null;
				isGetStartText = null;
				currNode = null;
				sameIndex = null;
				StartNode_End = null;
				EndNode_Start = null;
				seq = null;
				tmpStartNode = null;
				tmpEndNode = null;
				tempCurrNode = null;
			}
		},
		/**
		 * 특정노드를 제한노드까지 상/하로 움직이면서 텍스트 추출
		 * @param {node} node 검색시작위치노드
		 * @param {number} offset 검색시작위치노드의 offset 값(추출텍스트offset)
		 * @param {*} LimitNode 지정한 노드를 만나면 중지
		 * @param {*} isIncludeSelf 자신의 텍스트 포함여부
		 * @param {*} isOrderBy true면 검색시작부터 뒤로검색, false면 앞으로검색
		 * @returns 추출된 텍스트
		 */
		_GetParentText: function (node, offset, LimitNode, isIncludeSelf, isOrderBy) {
			var pnode = node.parentNode;
			var currNode = null;
			var rtn = '';
			var isFind = null;
			var isNotPassNode = false;

			try {
				if (isOrderBy) {
					isFind = false;
					currNode = pnode.firstChild;
					while (currNode != null) {
						if (currNode === LimitNode) {
							break;
						}

						isNotPassNode = false;

						if(currNode.nodeType == 3) {
							isNotPassNode = window.getComputedStyle(currNode.parentNode).display != 'none';
						}

						if(currNode.nodeType == 1) {
							isNotPassNode = window.getComputedStyle(currNode).display != 'none';
						}

						if (node === currNode) {
							isFind = true;
							if (isIncludeSelf != null && isIncludeSelf == true) {
								if (offset != null && offset == '' && currNode.nodeValue != null && isNotPassNode) {
									rtn = currNode.nodeValue;
								}
								else {
									if (currNode.nodeValue != null && currNode.nodeValue != null && isNotPassNode) {
										rtn = currNode.nodeValue.substr(offset)
									}
								}
							}
						}
						else if (isFind) {
							if (currNode.nodeType === 3 && currNode.nodeValue != null && isNotPassNode) {
								rtn += currNode.nodeValue;
							}
							else if (currNode.nodeType === 8) {
								//#comment
							}
							else {
								if(isNotPassNode && currNode.innerText != null)
									rtn += currNode.innerText;
							}
						}
						currNode = currNode.nextSibling;
					}
				}
				else {
					isFind = false;
					currNode = pnode.lastChild;
					while (currNode != null) {
						if (currNode === LimitNode) {
							break;
						}

						isNotPassNode = false;

						if(currNode.nodeType == 3) {
							isNotPassNode = window.getComputedStyle(currNode.parentNode).display != 'none';
						}

						if(currNode.nodeType == 1) {
							isNotPassNode = window.getComputedStyle(currNode).display != 'none';
						}

						if (node === currNode) {
							isFind = true;
							//rtn.currCnt = i+1;
							if (isIncludeSelf != null && isIncludeSelf == true) {
								if (offset != null && offset == '' && currNode.nodeValue != null && isNotPassNode) {
									rtn = currNode.nodeValue;
								}
								else {
									if (currNode.nodeValue != null && isNotPassNode)
										rtn = currNode.nodeValue.substr(0, offset)
								}
							}
						}
						else if (isFind) {
							if (currNode.nodeType === 3 && currNode.nodeValue != null && isNotPassNode) {
								rtn = currNode.nodeValue + rtn;
							}
							else if (currNode.nodeType === 8) {
								//#comment
							}
							else {
								if(isNotPassNode && currNode.innerText != null)
									rtn = currNode.innerText + rtn;
							}
						}
						currNode = currNode.previousSibling;
					}
				}

				return rtn;
			}
			finally {
				pnode = null;
				currNode = null;
				rtn = null;
				isFind = null;
			}
		},
		/**
		 * 해당 element의 모든 text node를 array로 return
		 * @param {*} element 
		 * @returns 
		 */
		getTextNodesAll:function(element) {
			var n, a=[], walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
			while(n=walk.nextNode()){
				if (n.parentNode instanceof HTMLUnknownElement || n.parentNode.toString() === '[object HTMLUnknownElement]'){
					continue;
				}
				a.push(n);
			}
			n = null;
			return a;
		},
		/**
		 * 
		 * @param {*} startNode 
		 * @param {*} startOffset 
		 * @param {*} endNode 
		 * @param {*} endOffset 
		 * @returns 
		 */
		getRangeByNodeOffset(startNode,startOffset,endNode,endOffset){
			let range = document.createRange()
			range.setStart(startNode,startOffset)
			range.setEnd(endNode,endOffset)
			return range;
		},
		/**
		 * range내에 모든 text를 추출
		 * @param {*} range 
		 * @returns 
		 */
		getTextByRange(range){
			let d = range.cloneContents();
			let l = Position.getTextNodesAll(d)
			let t = l.map(e=>e.textContent).join('')
			return t
		},
		/**
		 * text 추출
		 * @param {*} startNode 
		 * @param {*} startOffset 
		 * @param {*} endNode 
		 * @param {*} endOffset 
		 * @returns 
		 */
		getTextByNodeOffset(startNode,startOffset,endNode,endOffset){
			let range = document.createRange()
			range.setStart(startNode,startOffset)
			range.setEnd(endNode,endOffset)
			let d = range.cloneContents();
			let l = Position.getTextNodesAll(d)
			let t = l.map(e=>e.textContent).join('')
			return t
		},
		/**
		 * element를 넣으면 그안에 모든 택스트를 가저와 list로 반환한다.
		 */ 
		getTextNodesAllWithObj(element) {
			var n, a=[], walk=document.createTreeWalker(element,NodeFilter.SHOW_TEXT,null,false);
			let count = 0
			while(n=walk.nextNode()){
				if (n.parentNode instanceof HTMLUnknownElement || n.parentNode.toString() === '[object HTMLUnknownElement]'){
					continue;
				}
				
				let pos = Array.prototype.indexOf.call(n.parentNode.childNodes, n)

				let start = 0
				if (a.length-1 < 0){

				} else {
					start = a[a.length-1].end
				}
				let end = start + n.textContent.length
				a.push({
					'node': n , 
					'parent': n.parentNode, 
					'indexFromParent': pos,
					'start': start,
					'end': end
				});
			}
			n = null;
			return a;
		},
		/**
		 * node를 CfiAttr로 변환
		 * @param {*} startNode 
		 * @param {*} startOffset 
		 * @param {*} endNode 
		 * @param {*} endOffset 
		 * @returns 
		 */
		EncodeCfiAttr(startChapter,startNode,startOffset,endChapter,endNode,endOffset){
			let startCfiChapter = -1
			let startIndexFromParent = -1
			let startCfiAttr = null
			let startSelector = null
			let endCfiChapter = -1
			let endIndexFromParent = -1
			let endCfiAttr = null
			let endSelector = null
			let startAllnodes = null
			let startND = null
			let endAllnodes = null
			let endND = null

			try {
				//앞
				startAllnodes = Position.getTextNodesAllWithObj(startNode.parentNode)
				startND = null
				for (let i = startAllnodes.length - 1; i >= 0; i--) {
					if (startNode === startAllnodes[i].node){
						startND = startAllnodes[i];
						break;
					}
				}
				if (startND){
					startIndexFromParent = startND.start + startOffset
				} else {
					throw 'not fount startIndexFromParent'
				}
				//뒤
				endAllnodes = Position.getTextNodesAllWithObj(endNode.parentNode)
				endND = null
				for (let i = 0; i <endAllnodes.length; i++) {
					if (endNode === endAllnodes[i].node){
						endND = endAllnodes[i];
						break;
					}
				}
				if (endND){
					endIndexFromParent = endND.start + endOffset
				} else {
					throw 'not fount endIndexFromParent'
				}
				//cfi attr
				startCfiAttr = startNode.parentNode.getAttribute('cfi')
				endCfiAttr = endNode.parentNode.getAttribute('cfi')
				if (typeof startChapter === 'string'){
					startCfiChapter = parseInt(startChapter);
				} else if (typeof startChapter === 'number'){
					startCfiChapter = startChapter
				}
				if (typeof endChapter === 'string'){
					endCfiChapter = parseInt(endChapter);
				} else if (typeof endChapter === 'number'){
					endCfiChapter = endChapter
				}
				return { startCfiChapter ,startCfiAttr, startIndexFromParent, endCfiChapter, endCfiAttr, endIndexFromParent }
			} catch(ex){
				console.error(ex);
			} finally {
				startCfiChapter = null;
				startIndexFromParent = null;
				startCfiAttr = null;
				startSelector = null;
				endCfiChapter = null;
				endIndexFromParent = null;
				endCfiAttr = null;
				endSelector = null;
				startAllnodes = null;
				startND = null;
				endAllnodes = null;
				endND = null;
			}
		},
		/**
		 * CfiAttr을 node,offset으로 변환시켜준다.
		 * @param {*} startChapter 
		 * @param {*} startCfiAttr 
		 * @param {*} startIndexFromParent 
		 * @param {*} endChapter 
		 * @param {*} endCfiAttr 
		 * @param {*} endIndexFromParent 
		 */
		 DecodeCfiAttr(startChapter ,startCfiAttr, startIndexFromParent, endChapter, endCfiAttr, endIndexFromParent){
			let startSelector = null
			let endSelector = null
			let startChapterSelector = null
			let endChapterSelector = null
			let startAllnodes = null
			let endAllnodes = null
			let sIframe = null;
			let eIframe = null;
			let sEle = null;
			let eEle = null
			let start = null
			let end = null
			let startOffset = -1;
			let endOffset = -1;
			/**
			 * TODO 이쪽 Selector만 바꾸면 수정가능
			 */
			startSelector = '[cfi="'+startCfiAttr+'"]'
			endSelector = '[cfi="'+endCfiAttr+'"]'
			
			startChapterSelector = ePubFxEngineLoaderJS.Property.getIframeSelector(startChapter)
			endChapterSelector = ePubFxEngineLoaderJS.Property.getIframeSelector(endChapter)

			// document.querySelector('[bdb-fixed-index="2"]').contentDocument.querySelector('[cfi="/20/2/4/1"]')

			sIframe = document.querySelector(startChapterSelector)
			eIframe = document.querySelector(endChapterSelector)
			if (!sIframe || !eIframe){
				return null;
			}

			sEle = sIframe.contentDocument.querySelector(startSelector)
			eEle = eIframe.contentDocument.querySelector(endSelector)

			if (!sEle || !eEle){
				return null;
			}
			start = null
			end = null
			startOffset = -1;
			endOffset = -1;

			try {
				/**
				 * 추출 실패시 Element라도 return
				 */
				if (startIndexFromParent === -1 && endIndexFromParent === -1){
					return {
						start: {
							node: sEle
						},
						end: {
							node: eEle
						}
					}
				}
				if (sEle && eEle){
					startAllnodes = Position.getTextNodesAllWithObj(sEle)
					for (let i=0; i<startAllnodes.length; i++){
						let e = startAllnodes[i]
						if (e.start <= startIndexFromParent && e.end >= startIndexFromParent){
							start = e
							break;
						}
					}
					endAllnodes = Position.getTextNodesAllWithObj(eEle)
					for (let i= endAllnodes.length - 1; i >= 0; i--){
						let e = endAllnodes[i]
						if (e.start <= endIndexFromParent && e.end >= endIndexFromParent){
							end = e
							break;
						}
					}
					if (!start || !end){
						return {
							start: {
								node: sEle,
								offset: -1
							},
							end: {
								node: eEle,
								offset: -1
							}
						}
					}
				} else if (sEle){
					startAllnodes = Position.getTextNodesAllWithObj(sEle)
					for (let i=0; i<startAllnodes.length; i++){
						let e = startAllnodes[i]
						if (e.start <= startIndexFromParent && e.end >= startIndexFromParent){
							start = e
							break;
						}
					}
				} else if (eEle){
					endAllnodes = Position.getTextNodesAllWithObj(eEle)
					for (let i= endAllnodes.length - 1; i >= 0; i--){
						let e = endAllnodes[i]
						if (e.start <= endIndexFromParent && e.end >= endIndexFromParent){
							end = e
							break;
						}
					}
				} else {
					return null
				}
				if (start && end){
					startOffset =  startIndexFromParent - start.start
					endOffset =  endIndexFromParent - end.start
				} else if (start){
					startOffset =  startIndexFromParent - start.start
				} else if (end){
					endOffset =  endIndexFromParent - end.start
				} 
				return {
					start: {
						node: start.node,
						offset: startOffset
					},
					end: {
						node: end.node,
						offset: endOffset
					}
				}
			} catch(e){
				console.error('[core][posion][DecodeCfiAttr]',e)
			} finally {
				startSelector = null;
				endSelector = null;
				startChapterSelector = null;
				endChapterSelector = null;
				startAllnodes = null;
				endAllnodes = null;
				sIframe = null;
				eIframe = null;
				sEle = null;
				eEle = null;
				start = null;
				end = null;
				startOffset = null;
				endOffset = null;
			}
			
		},
	};

	/**
	 * annotation object key
	 */
	const KEY_ANNOTATION_ID = 'id';
	const KEY_ANNOTATION_SERVERID = 'serverId';
	const KEY_ANNOTATION_CHAPTER = 'chapter';
	const KEY_ANNOTATION_CHAPTERNAME = 'chapterName';
	const KEY_ANNOTATION_START_CFI_CHAPTER = 'startCfiChapter';
	const KEY_ANNOTATION_STARTCFIATTR = 'startCfiAttr';
	const KEY_ANNOTATION_STARTINDEXFROMPARENT = 'startIndexFromParent';

	const KEY_ANNOTATION_END_CFI_CHAPTER = 'endCfiChapter';
	const KEY_ANNOTATION_ENDCFIATTR = 'endCfiAttr';
	const KEY_ANNOTATION_ENDINDEXFROMPARENT = 'endIndexFromParent';

	const KEY_ANNOTATION_SELECTOR = 'selector';
	const KEY_ANNOTATION_DATE = 'date';
	const KEY_ANNOTATION_ANNOTATIONTYPE = 'annotationType';
	const KEY_ANNOTATION_TEXT = 'text';
	const KEY_ANNOTATION_DATA = 'data';
	const KEY_ANNOTATION_COLOR = 'color';
	const KEY_ANNOTATION_COLORKEY = 'colorKey';
	const KEY_ANNOTATION_COLORTYPE = 'colorType';
	const KEY_ANNOTATION_MEMO = 'memo';
	const KEY_ANNOTATION_PERCENT = 'percent';
	const KEY_ANNOTATION_STATUS = 'status';
	const KEY_ANNOTATION_MEDIAPATH = 'mediapath';
	const KEY_ANNOTATION_VERSION = 'ver';

	const KEY_ANNOTATION_STATUS_SYNC 	= 'S';
	const KEY_ANNOTATION_STATUS_CREATE = 'C';
	const KEY_ANNOTATION_STATUS_UPDATE = 'U';
	const KEY_ANNOTATION_STATUS_DELETE = 'D';

	//only use in cache
	const KEY_ANNOTATION_DELETE_LIST = 'deleteList';
	/**
	 * 애노테이션 클래스
	 */
	const Annotation = Core.Annotation = {
		list: [ ],
		colorList: [
			{ name:'lemon' ,type:0 ,color:'#fbedb8',opacity:0.5 },
			{ name:'palegreen' ,type:0 ,color:'#e1efb8',opacity:0.5 },
			{ name:'lavendar' ,type:0 ,color:'#cec0e9',opacity:0.5 },
			{ name:'skyblue' ,type:0 ,color:'#b1e7f9',opacity:0.5 },
			{ name:'palepink' ,type:0 ,color:'#fbbfbe',opacity:0.5 },
			{ name:'underline' ,type:1 ,color:'#ea3323',opacity:1 },
		],
		memoImg:null,
		_memoImg:`<svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewBox="0 0 10 11">
					<path data-name="빼기 29" d="M2.7 11a2.676 2.676 0 0 1-1.978-.753 2.793 2.793 0 0 1-.725-2v-5.5A2.8 2.8 0 0 1 .719.76 2.667 2.667 0 0 1 2.681 0h4.614a2.682 2.682 0 0 1 1.979.752 2.789 2.789 0 0 1 .726 2v5.5a2.792 2.792 0 0 1-.72 1.991A2.673 2.673 0 0 1 7.317 11zm.082-3.5a.449.449 0 1 0 0 .9h5a.449.449 0 1 0 0-.9zm0-2.444a.448.448 0 1 0 0 .9h5a.448.448 0 1 0 0-.9zm0-2.446a.449.449 0 1 0 0 .9h1.879a.449.449 0 1 0 0-.9z" style="fill:#5e6b9f"/>
				</svg>`,
		/**
		 * 현재 메모 색상
		 */
		currentMemoColor: null,
		/**
		 * 현재 하이라이트 색상
		 */
		currentHighlightColor: null,
		/**
		 * selection이 그려지는 layer element
		 */
		eleDrawLayer: null,
		/**
		 * 현재 페이지에 해당되는 북마크객체.
		 */
		CurrentBookmark: null,
		/**
		 * 초기화
		 */
		init: function() {
			Annotation.currentMemoColor = this.colorList[0]
			Annotation.currentHighlightColor = this.colorList[0]
			Annotation.eleDrawLayer = document.getElementById('viewer_annotation_layer')
			Annotation.buildMemoImg()
		},
		/**
		 * 메모 엘리먼트를 생성
		 */
		buildMemoImg: function(){
			Annotation.memoImg = document.createElement('div')
			Annotation.memoImg.style.position = 'absolute'
			Annotation.memoImg.style.width = 11+'px'
			Annotation.memoImg.style.height = 11+'px'
			Annotation.memoImg.style.pointerEvents = 'none'
			Annotation.memoImg.innerHTML = Annotation._memoImg
		},
		/**
		 * 현재 샐랙션된 부분의 cfi정보를 추출한다
		 * @returns 
		 */
		extractCfi: function(){
			//string
			let scif = null
			let ecif = null

			//node
			let sNode = null
			let eNode = null

			let selector = null;
			let iframe = null;
			let innerDoc = null;
			let attr = null;
			let enc = null;
			let text = null;
			scif = Selection.getStartAnnotationCfi();
			ecif = Selection.getEndAnnotationCfi();

			if (!scif){
				console.error('start annotation cfi is empty')
				return null;
			}
			if (!ecif){
				console.error('end annotation cfi is empty')
				return null
			}
			if (scif === ecif){
				console.error('start & end annotation cfi cannot same')
				return null
			}
			selector = ePubFxEngineLoaderJS.Property.getIframeSelector(Selection._selected_index);
			iframe = document.querySelector(selector);
			if (!iframe){
				console.error('Not Found Iframe Element')
				return null
			}
			innerDoc = iframe.contentDocument || iframe.contentWindow.document;
			sNode = Position._decodeCFI(innerDoc, scif);
			eNode = Position._decodeCFI(innerDoc, ecif);
			
			// return null;
			attr = iframe.getAttribute(ePubFxEngineLoaderJS.Property.getIframeSelectAttr())
			enc = Position.EncodeCfiAttr(attr,sNode.node,sNode.offset,attr,eNode.node,eNode.offset);
			text = Position.getTextByNodeOffset(sNode.node,sNode.offset,eNode.node,eNode.offset)
			return { enc , text };
		},
		/**
		 * 애노테이션 데이터 생성
		 * @param {*} type 
		 * @param {*} memo 
		 * @param {*} chapterName 
		 * @param {*} fileno 
		 */
		create: function( type, memo , toc, fileno, total){
			return new Promise(function(resolve,reject){

				let mText = null
				let mColor = '#ec1f2d';
				let mColorKey = 'red';
				let mColorType = 0;
				let mMemo = null;
				let obj = new Object();
				let date = null;
				let mPercent = 0;
				let mChapterName = null;
				let mChapter = 0;
				let startCfiChapter = -1;
				let startCfiAttr = null;
				let startIndexFromParent = -1;
				let endCfiChapter = -1;
				let endCfiAttr = null;
				let endIndexFromParent = -1;
				let extCfi = null;

				//북마크
				if (type === 0){
	
				} 
				//하이라이트
				else if (type === 1){
					mColor = Annotation.currentHighlightColor.color
					mColorKey = Annotation.currentHighlightColor.name
					mColorType = Annotation.currentHighlightColor.type
					extCfi = Annotation.extractCfi();
				} 
				//메모
				else if (type === 2){
					mColor = Annotation.currentMemoColor.color
					mColorKey = Annotation.currentMemoColor.name
					mColorType = Annotation.currentHighlightColor.type
					extCfi = Annotation.extractCfi();
				}

				//파일번호
				if (typeof fileno === 'number'){
					mChapter = fileno
				}

				//퍼센트
				if (typeof fileno === 'number' && typeof total === 'number' ){
					mPercent = Util.calcPercent(fileno,total)
				}

				//메모
				if (typeof memo === 'string'){
					mMemo = memo;
				}

				//애노테이션 정보
				if (extCfi){
					if (extCfi.enc){
						startCfiChapter = extCfi.enc[KEY_ANNOTATION_START_CFI_CHAPTER]
						startCfiAttr = extCfi.enc[KEY_ANNOTATION_STARTCFIATTR]
						startIndexFromParent = extCfi.enc[KEY_ANNOTATION_STARTINDEXFROMPARENT]
						endCfiChapter = extCfi.enc[KEY_ANNOTATION_END_CFI_CHAPTER]
						endCfiAttr = extCfi.enc[KEY_ANNOTATION_ENDCFIATTR]
						endIndexFromParent = extCfi.enc[KEY_ANNOTATION_ENDINDEXFROMPARENT]
					}
					if (extCfi.text){
						mText = extCfi.text;
					}
				}

				//목차
				if (toc !== null && toc !== undefined){
					mChapterName = toc.text;
				}

				//make object
				date = new Date()
				obj[KEY_ANNOTATION_ID] 						= date.valueOf()
				obj[KEY_ANNOTATION_SERVERID]				= null
				obj[KEY_ANNOTATION_CHAPTER]					= mChapter
				obj[KEY_ANNOTATION_CHAPTERNAME]				= mChapterName
				obj[KEY_ANNOTATION_START_CFI_CHAPTER]		= startCfiChapter;
				obj[KEY_ANNOTATION_STARTCFIATTR]			= startCfiAttr
				obj[KEY_ANNOTATION_STARTINDEXFROMPARENT]   	= startIndexFromParent
				obj[KEY_ANNOTATION_END_CFI_CHAPTER]			= endCfiChapter;
				obj[KEY_ANNOTATION_ENDCFIATTR]				= endCfiAttr
				obj[KEY_ANNOTATION_ENDINDEXFROMPARENT]		= endIndexFromParent
				obj[KEY_ANNOTATION_SELECTOR]				= null
				obj[KEY_ANNOTATION_DATE]					= date.current()
				obj[KEY_ANNOTATION_ANNOTATIONTYPE]			= type
				obj[KEY_ANNOTATION_TEXT]					= mText
				obj[KEY_ANNOTATION_COLOR]					= mColor
				obj[KEY_ANNOTATION_COLORKEY]				= mColorKey
				obj[KEY_ANNOTATION_COLORTYPE]				= mColorType
				obj[KEY_ANNOTATION_MEMO]					= mMemo
				obj[KEY_ANNOTATION_PERCENT]					= mPercent
				obj[KEY_ANNOTATION_STATUS] 					= KEY_ANNOTATION_STATUS_CREATE

				resolve(obj);
			})
		},
		update: function( obj ){
			return new Promise(function(resolve,recject){
				try{
					let find = Annotation.list.findIndex(e=>e.id === obj.id)
					if (find >= 0){
						find = obj
					} else {
						Annotation.list.push(obj)
					}
					resolve(Annotation.list)
				} catch(error){
					recject(error)
				}
			})
		},
		/**
		 * annotaion change color
		 */
		/**
		 * 
		 * @param {*} name 
		 * @returns 
		 */
		 setCurrentMemoColor: function(name){
			return new Promise(function(resolve,reject){
				let find = Annotation.colorList.find(e=>e.name == name)
				if (find){
					Annotation.currentMemoColor = find
					resolve(Annotation.currentMemoColor)
				} else {
					reject('not find \"'+name+'\" in List ')
				}
			})
		},
		/**
		 * 색을 바꿔 obj return
		 * @param {*} obj 
		 * @param {*} colorKey 
		 * @returns 
		 */
		changeColor(obj,colorKey){
			let colorset = Annotation.colorList.find(e=>e.name === colorKey)
			obj.colorKey = colorset.name
			obj.color = colorset.color
			return obj
		},
		/**
		 * update Color
		 * @param {*} obj 기존 object
		 * @param {*} colorKey 색깔키
		 * @returns 
		 */
		updateColor(obj,colorKey){
			let colorset = Annotation.colorList.find(e=>e.name === colorKey)
			obj.colorKey = colorset.name
			obj.color = colorset.color
			return Annotation.update(obj)
		},
		/**
		 * cfi attr 인자인지 여부 체크
		 * @param {*} item 
		 * @returns 
		 */
		validate:function(item){
			if (!item.startCfiAttr){
				return false;
			}
			if (typeof item.startCfiChapter != 'number'){
				return false;
			}
			if (typeof item.startIndexFromParent != 'number'){
				return false;
			}
			if (item.startCfiChapter < 0){
				return false;
			}
			if (item.startIndexFromParent < 0){
				return false;
			}

			if (!item.endCfiAttr){
				return false;
			}
			if (typeof item.endCfiChapter != 'number'){
				return false;
			}
			if (typeof item.endIndexFromParent != 'number'){
				return false;
			}
			if (item.endCfiChapter < 0){
				return false;
			}
			if (item.endIndexFromParent < 0){
				return false;
			}
			return true;
		},
		/**
		 * annotation drawing
		 * @returns 
		 */
		drawingWithCheck: function(){
			let parents = Annotation.eleDrawLayer
			parents.clearChildren()
			parents = null
			if (!Annotation.list){
				return false
			}
			if (Annotation.list.length === 0){
				return false
			}

			for(let item of Annotation.list){
				//memo,highlight외에는 스킵
				if (item.annotationType === 0 || item.annotationType === undefined || item.annotationType === null){
					continue;
				}
				if (!Annotation.validate(item)){
					continue;
				}
				let decode = Position.DecodeCfiAttr(
					item.startCfiChapter,
					item.startCfiAttr,
					item.startIndexFromParent,
					item.endCfiChapter,
					item.endCfiAttr,
					item.endIndexFromParent)
				if (!decode){
					continue;
				}
				let rects = Annotation.rectsFromDecode(decode);
				if (!rects){
					continue;
				}
				let fRects = Annotation.filterRects(rects);
				if (fRects.length > 0){
					Annotation.highlightWithRects(fRects, item , 0.4);
				}
			}
		},
		/**
		 * 뷰어 범위내에 rects만 걸러서 
		 * @param {*} rects 
		 * @returns 
		 */
		filterRects: function(rects){
			let mList = [];
			let rect = null;
			let rect_next = null;
			let dual_idx = null;
			let isDual = false;
			for (let k=0; k<rects.length; k++){
				rect = rects[k];
				//210803 - sjhan 공백 체크 로직 추가.
				var xx = rect.left+(rect.width/2);
				var yy = rect.top+(rect.height/2);
				var selnode = document.elementFromPoint(xx,yy);
				if (selnode){
					if (selnode.textContent){
						if (selnode.textContent.replace(/\s+/g,'').length <= 0){
							continue;
						}
					}
				}

				//210917 뷰어 영역 밖으로 샐랙션을 막기위함.
				if (Selection.viewerPadding){
					//현재 뷰어의 rect를 가저와 넘어가버리면 result에 추가하지 않는다.
					var viewerW = _viewer_width + Selection.viewerPadding.left;
					var viewerH = _viewer_height + Selection.viewerPadding.top;
					var lLeft = rect.left;

					if ( Selection.viewerPadding.left > lLeft || viewerW <= lLeft|| 
						Selection.viewerPadding.top > rect.top || viewerH < rect.top ){
						viewerW=null;
						viewerH=null;
						lLeft = null;
						continue;
					}
					viewerW=null;
					viewerH=null;
					lLeft = null;
				}

				isDual = false;
				for (dual_idx = k + 1; dual_idx < rects.length; dual_idx++) {
					// across check
					rect_next = rects[dual_idx];
					if (rect_next) {
						// across check
						if (rect.top >= rect_next.top
							&& rect.bottom <= rect_next.bottom
							&& rect.left <= rect_next.left
							&& rect.right >= rect_next.right && rect_next.width != 0) {
							isDual = true;
							break;
						}
						else if (rect.top <= rect_next.top
									&& rect.bottom >= rect_next.bottom
									&& rect.left >= rect_next.left
									&& rect.right <= rect_next.right && rect_next.width != 0) {
							isDual = true;
							break;
						}
						else if (rect.top <= rect_next.top
									&& rect.bottom >= rect_next.bottom
									&& rect.left <= rect_next.left
									&& rect.right >= rect_next.right && rect_next.width != 0) {
							isDual = true;
							break;
						}
					}
				}
				if (isDual) {
					continue;
				}
				mList.push(rect)
			}
			return mList;
		},
		/**
		 * rects 정보를 받아 하이라이트 해준다.
		 * @param {*} rects 
		 */
		highlightWithRects: function(rects , item , opacity){
			if (!Annotation.eleDrawLayer){
				return;
			}
			if (rects.length === 0 ){
				return;
			}
			let wrap = null
			let result = null
			let mimg = null

			
			try {
				//element
				wrap = document.createElement('div')
				result = Annotation.createSelectionDiv(rects , item ,opacity)
				wrap.appendChild(result)
				//memo icon
				if (item.memo && item.memo.length>0){
					mimg = Annotation.memoImg.cloneNode(true)
					if (wrap.lastElementChild){
						mimg.style.top =  parseFloat(wrap.lastElementChild.style.top) - (parseFloat(mimg.style.height)/2) +'px'
						mimg.style.left = parseFloat(wrap.lastElementChild.style.left)+parseFloat(wrap.lastElementChild.style.width)+'px'
					}
					wrap.appendChild(mimg)
				}
				Annotation.eleDrawLayer.appendChild(wrap)
			} catch(error){
				console.error(error)
			} finally {
				wrap = null
				result = null
				mimg = null
			}
		},
		/**
		 * iframe rect를 변환해 넣어준다.
		 * @param {*} rects 
		 * @returns 
		 */
		converIframeRects: function(rects){
			let arr = []
			var iframes = null;
			var frameOffsetX = 0;
			var frameOffsetY = 0;
			var frameRect = null;
			var frameRatio = 1;
			iframes = document.getElementsByTagName('iframe');
			try {
				if(iframes) {
					for(var frame of iframes) {
						if(frame.contentDocument) {
							frameRect = frame.getBoundingClientRect();
							frameOffsetX = frameRect.left;
							frameOffsetY = frameRect.top;
							frameRatio = parseFloat(frame.getAttribute('zdt'));
						}
					}
				}
				for (let e of rects){
					let p2 = {} 
					//position n size
					let mTop = 0
					let mLeft = 0
					let mWidth = 0
					let mHeight = 0
					mLeft = e.left 
					mWidth = e.width
					p2.top = (mTop*frameRatio) + _offsetTop
					p2.left = (mLeft*frameRatio) + frameOffsetX
					p2.width = mWidth*frameRatio
					p2.height = mHeight*frameRatio
					arr.push(p2);
				}
				return arr;
			} catch(err){
				console.error(err);
			} finally {
				arr = null;
				iframes = null;
				frameOffsetX = null;
				frameOffsetY = null;
				frameRect = null;
				frameRatio = null;
			}
		},
		/**
		 * selection 블럭 element 생성
		 * @param {*} rects 
		 * @param {*} item 
		 * @returns 
		 */
		 createSelectionDiv:function(rects , item ,opacity){
			let mColor = "#38a5e6"
			let drawType = -1
			let _opacity = 0.4
			//color
			if (item){
				mColor = item['color']
			}
			// block,underline
			drawType = Annotation.colorList.find(e=>e.color == mColor).type
			// opacity
			if (typeof opacity === 'number'){
				_opacity = opacity
			}
			let docf = document.createDocumentFragment()
			var iframes = null;
			var frameOffsetX = 0;
			var frameOffsetY = 0;
			var frameRect = null;
			var frameRatio = 1;
			var baseTop = parseFloat(ele_view_wrapper.offset().top);
			let prect = ele_view_wrapper.get(0).getBoundingClientRect()
			iframes = document.getElementsByTagName('iframe');
			let cfiIframe = null;
			if (typeof item.startCfiChapter === 'number'){
				cfiIframe = document.querySelector(`[bdb-fixed-index="${item.startCfiChapter}"]`)
			}
			if (cfiIframe){
				if(cfiIframe.contentDocument) {
					frameRect = cfiIframe.getBoundingClientRect();
					frameOffsetX = frameRect.left;
					frameOffsetY = frameRect.top;
					frameRatio = parseFloat(cfiIframe.getAttribute('zdt'));
				}
			}

			
			for (let e of rects){
				let p2 = document.createElement('div')
				p2.setAttribute('bdb-anno-type',item.annotationType)
				p2.setAttribute('bdb-anno-id',item.id)
				p2.style.backgroundColor = mColor
				p2.style.pointerEvents = 'none'
				p2.style.position = 'absolute'
				p2.style.opacity = opacity
				//position n size
				let mTop = 0
				let mLeft = 0
				let mWidth = 0
				let mHeight = 0
				mLeft = e.left 
				mWidth = e.width
				if (drawType === 0){
					mHeight = e.height
					mTop = e.top
				} else if (drawType === 1){
					mHeight = 3
					mTop = e.top+e.height
				}
				p2.style.top = (mTop*frameRatio)+frameOffsetY+window.scrollY-baseTop+_offsetTop+'px'
				p2.style.left = (mLeft*frameRatio)+frameOffsetX+window.scrollX-prect.left+_padding+'px'
				p2.style.width = mWidth*frameRatio + 'px'
				p2.style.height = mHeight*frameRatio + 'px'
				docf.appendChild(p2)
				p2 = null
				mTop = null
				mLeft = null
				mWidth = null
				mHeight = null
			}
			mColor = null
			drawType = null
			_opacity = null
			return docf
		},
		/**
		 * annotation 정보를 rects로 반환
		 * @param {*} decode 
		 * @returns 
		 */
		rectsFromDecode( decode ){
			let range = document.createRange()
			range.setStart(decode.start.node,decode.start.offset)
			range.setEnd(decode.end.node,decode.end.offset)
			return range.getClientRects()
		},
		/**
		 * 북마크 리스트
		 * @returns 
		 */
		getBookmarkList: function(){
			return Annotation.list.filter(e=>e.annotationType === 0);
		},
		getAnnoList: function(){
			return Annotation.list.filter(e=>e.annotationType === 1 || e.annotationType === 2);
		},
		/**
		 * 북마크가 포함인지
		 */
		isBookmarkContains(fileno){
			let bList = Annotation.getBookmarkList()
			if (bList.length === 0){
				return null
			}
			let has = null
			for (let item of bList){
				if (item[KEY_ANNOTATION_CHAPTER] === fileno){
					has = item;
					break;
				}
			}
			return has;
		},
		/**
		 * 북마크가 있는지 확인하고 북마크를 Published함.
		 */
		checkBookmark(fileno){
			Annotation.CurrentBookmark = Annotation.isBookmarkContains(fileno)
			window.dispatchEvent(new CustomEvent('bdbcheckbookmark',{
				detail: { bookmark: Annotation.CurrentBookmark }
			}))
		},
		/**
		 * 현재페이지에 해당되는지 확인.
		 * @param {*} fileno 
		 */
		checkAnnotation: function(fileno){
			if (typeof fileno !== 'number'){
				return null;
			}
			this.checkBookmark(fileno);
		},

		/**
		 * conver to response to annotation
		 * @param {*} res 
		 * @returns 
		 */
		 responseToObj: function(res){
			let obj = new Object();
			obj[KEY_ANNOTATION_ID]						= res['code']
			obj[KEY_ANNOTATION_SERVERID]				= res['code']
			obj[KEY_ANNOTATION_CHAPTER]					= res['chapterno']
			obj[KEY_ANNOTATION_CHAPTERNAME]				= res['chaptername']

			obj[KEY_ANNOTATION_START_CFI_CHAPTER]		= res['startfileno']
			obj[KEY_ANNOTATION_STARTCFIATTR]			= res['startpath']
			obj[KEY_ANNOTATION_STARTINDEXFROMPARENT]	= res['startpathindex']

			obj[KEY_ANNOTATION_END_CFI_CHAPTER]			= res['endfileno']
			obj[KEY_ANNOTATION_ENDCFIATTR]				= res['endpath']
			obj[KEY_ANNOTATION_ENDINDEXFROMPARENT]		= res['endpathindex']

			obj[KEY_ANNOTATION_SELECTOR]				= res['']
			obj[KEY_ANNOTATION_DATE]					= res['updatedate']
			obj[KEY_ANNOTATION_ANNOTATIONTYPE]			= res['type']
			obj[KEY_ANNOTATION_TEXT]					= res['selectedtext']
			obj[KEY_ANNOTATION_COLOR]					= res['color']
			obj[KEY_ANNOTATION_COLORTYPE]				= res['colortype']
			if (res['colortype'] === 0){
				let find = Annotation.colorList.findIndex(e=>e.color === res['color'])
				if (find>=0){
					obj[KEY_ANNOTATION_COLORKEY]				= Annotation.colorList[find].name
				}
				
			} else if (res['colortype'] === 1){
				let find = Annotation.colorList.filter(e=>e.type === 1).findIndex(e=>e.color === res['color'])
				if (find>=0){
					obj[KEY_ANNOTATION_COLORKEY]				= Annotation.colorList[find].name
				}
			}

			obj[KEY_ANNOTATION_MEMO]					= res['memo']
			obj[KEY_ANNOTATION_PERCENT]					= res['startpercent']
			// obj[KEY_ANNOTATION_STATUS]					= res['S']
			obj[KEY_ANNOTATION_MEDIAPATH]				= res['mediapath']
			if (typeof obj[KEY_ANNOTATION_DATE] === 'string' && obj[KEY_ANNOTATION_DATE].length > 0){
				obj[KEY_ANNOTATION_DATE] = new Date(obj[KEY_ANNOTATION_DATE]).toFormat()
			}
			return Object.assign({},obj)
		},
		responseToList:function(list){
			let result = null
			if (list instanceof Array){
				result = list.flatMap(e=>Annotation.responseToObj(e))
			} 
			return result
		},
		/**
		 * 에노테이션 레이어가 존재하는지 체크
		 * @param {*} x 
		 * @param {*} y 
		 * @returns 
		 */
		 pointInRects: function(x,y){
			let isEnd = false
			let axis = null
			let rects = null
			let decode = null

			var iframes = null;
			var frameOffsetX = 0;
			var frameOffsetY = 0;
			var frameRect = null;
			var frameRatio = 1;
			var baseTop = parseFloat(ele_view_wrapper.offset().top);
			let prect = ele_view_wrapper.get(0).getBoundingClientRect()
			iframes = document.getElementsByTagName('iframe');

			if(iframes) {
				for(var frame of iframes) {
					if(frame.contentDocument) {
						frameRect = frame.getBoundingClientRect();
						frameOffsetX = frameRect.left;
						frameOffsetY = frameRect.top;
						frameRatio = parseFloat(frame.getAttribute('zdt'));
					}
				}
			}

			for(let item of Annotation.list){
				if (item.annotationType === 0){
					continue;
				}
				decode = Position.DecodeCfiAttr(
					item.startCfiChapter,
					item.startCfiAttr,
					item.startIndexFromParent,
					item.endCfiChapter,
					item.endCfiAttr,
					item.endIndexFromParent)
				if (decode){
					rects = Annotation.rectsFromDecode(decode)
					if (rects){
						for (let rect of rects){
							isEnd = Annotation.isPointInRect(x,y,rect)
							if (isEnd){
								if (typeof item.startCfiChapter === 'number'){
									cfiIframe = document.querySelector(`[bdb-fixed-index="${item.startCfiChapter}"]`)
									if (cfiIframe){
										if(cfiIframe.contentDocument) {
											frameRect = cfiIframe.getBoundingClientRect();
											frameOffsetX = frameRect.left;
											frameOffsetY = frameRect.top;
											frameRatio = parseFloat(cfiIframe.getAttribute('zdt'));
										}
									}
								}

								if (rects[0]){
									axis = {
										top: (rects[0].top*frameRatio)+frameOffsetY+window.scrollY-baseTop+_offsetTop,
										left: (rects[0].left*frameRatio)+frameOffsetX+window.scrollX-prect.left+_padding,
										width: rects[0].width*frameRatio,
										height: rects[0].height*frameRatio,
									}
								}
								let start = decode.start
								let end = decode.end
								let startcfi = Position._encodeCFI(document,start.node,start.offset)
								let endcfi = Position._encodeCFI(document,end.node,end.offset)
								return { 'item':item ,
										'rects':rects,
										'axis':axis,
										'startCfi': startcfi,
										'endCfi':endcfi,
										'offsetTop': frameOffsetY+window.scrollY+_offsetTop,
										'offsetLeft': frameOffsetX+window.scrollX+_padding,
										'ratio': frameRatio,
										};
							}
						}
					}

				}
			}
			return isEnd;
		},
		/**
		 * rect내에 x,y 좌표가 존재하는지.
		 * @param {*} x click x 
		 * @param {*} y click y
		 * @param {*} rect rect
		 */
		isPointInRect: function(x,y,rect){
			return x>=rect.x && x<=rect.right && y>=rect.y && y<=rect.bottom
		},
	}
	

	const WM_DRAG_INIT = 0;
	const WM_DRAG_START = 1;
	const WM_DRAG_END =  2

	var Selection = Core.Selection = {
		/**
		 * selection한 list cache
		 */
		selectionCache: {
			start: null,
			end: null,
		},
		/**
		 * -1: none
		 * 0: next
		 * 1: prev
		 */
		selectionCacheMode: -1,

		EleSelectionArea: null,
		EleSelectionDiv: null,
		EleSelectionLeft: null,
		EleSelectionRight: null,
		_picker_size: 6,
		_picker_padding: 20,
		_picker_height_offset: 12,

		_selected_index: null,
		/**
		 * selection container 
		 */
		selectionContainerID: 'viewer_selection',
		init: function(){
			Selection.EleSelectionArea = document.getElementById(Selection.selectionContainerID);
			Selection.EleSelectionDiv = Selection.BuildSelection();
			Selection.EleSelectionLeft = Selection.BuildSelectionPicker(true);
			Selection.EleSelectionRight = Selection.BuildSelectionPicker(false);
		},
		/**
		 * selection 몸체
		 * @returns 
		 */
		BuildSelection: function(){
			let _selectionElement = document.createElement('div');
			_selectionElement.style.zIndex = 0;
			_selectionElement.style.position = "absolute";
			_selectionElement.style.pointerEvents = 'none';//layer 아래쪽으로 이벤트가 전달안되 none
			_selectionElement.style.opacity = "0.2";
			_selectionElement.style.pointerEvents = 'none'
			return _selectionElement
		},
		/**
		 * selection 피커
		 * @param {*} isLeft 
		 * @returns 
		 */
		BuildSelectionPicker: function(isLeft){
			let _selector_img_element1 = document.createElement('div');
			_selector_img_element1.style.zIndex = 1;
			_selector_img_element1.style.position = "absolute";
			_selector_img_element1.style.display = 'none';
			_selector_img_element1.style.width = 15+'px';
			_selector_img_element1.style.height = 15+'px';
			_selector_img_element1.style.padding = 20+'px';
			_selector_img_element1.style.opacity = "1";
			if (isLeft){
				_selector_img_element1.id = 'bdb_img_selector_start';
				_selector_img_element1.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="8" height="35" viewBox="0 0 8 35">
														<g transform="translate(-690 -274)">
															<path data-name="사각형 1979" transform="translate(693 274)" style="fill:#2968b5" d="M0 0h2v30H0z"/>
															<circle data-name="타원 91" cx="4" cy="4" r="4" transform="translate(690 301)" style="fill:#2968b5"/>
														</g>
													</svg>`
			} else {
				_selector_img_element1.id = 'bdb_img_selector_end';
				_selector_img_element1.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="8" height="35" viewBox="0 0 8 35">
														<path data-name="사각형 1979" transform="translate(3)" style="fill:#2968b5" d="M0 0h2v30H0z"/>
														<circle data-name="타원 91" cx="4" cy="4" r="4" transform="translate(0 27)" style="fill:#2968b5"/>
													</svg>`
			}
			return _selector_img_element1
		},
		/**
		 * 현재 시작 애노테이션 cfi
		 * @returns 
		 */
		getStartAnnotationCfi: function(){
			if (Selection.selectionCacheMode >= 0){
				return Selection.selectionCache.start
			} else {
				return Selection.m_strDragStartCfi
			}
		},
		/**
		 * 현재 끝 애노테이션 cfi
		 * @returns 
		 */
		getEndAnnotationCfi: function(){
			if (Selection.selectionCacheMode >= 0){
				return Selection.selectionCache.end
			} else {
				return Selection.m_strDragEndCfi
			}
		},
		/**
		 * 연속 샐랙션모드 활성화
		 * @param {*} isPrev 
		 */
		activeCacheMode: function(isPrev){
			if (isPrev){
				Selection.selectionCacheMode = 1
			} else {
				Selection.selectionCacheMode = 0
			}
		},
		/**
		 * 연속 샐랙션 모드 초기화
		 */
		resetCacheMode: function(){
			Selection.selectionCacheMode = -1
		},
		DeleteSelection: function () {
			var isDebug = false;
			var sel = document.getElementById('viewer_selection');
			var i = null;

			try {
				for (i = 0; i < sel.childNodes.length; i++) {
					if (sel.childNodes[i].id != undefined && sel.childNodes[i].id.indexOf('Rue_Selection_Container_SEL_') == 0 ||
						sel.childNodes[i].id == 'RueSelStartBar' || sel.childNodes[i].id == 'RueSelEndBar') {
						sel.childNodes[i].onclick = null;
						sel.removeChild(sel.childNodes[i]);
						i--;
					}
				}
			}
			finally {
				isDebug = null;
				sel = null;
				i = null;
			}
		},
		IsSelected:function(){
			var sele = document.getElementById('viewer_selection')
			if (sele && sele.hasChildNodes()){
				return true;
			}
			return false;
		},
		IsShowSelection: function () {
			if (document.getElementById('viewer_selection').style.visibility == 'hidden') {
				return false;
			}
			return true;
		},
		HideSelection: function () {
			document.getElementById('viewer_selection').style.visibility = 'hidden';
		},
		ShowSelection: function () {
			document.getElementById('viewer_selection').style.visibility = 'visible';
		},
		DrawSelection: function (doc, strStartCfi, strEndCfi, strColor) {
			var isDebug = false;
			var rtnCompare = null;
			var rtnDraw = null;
			var nRtnDraw_Cnt = null;
			var dl_idx = null;
			var re = null;
			var se = null;
			var pop_top = null;
			var pop_new_top = null;
			var isRes = false;
			var pageH = null;
			var baseLeft = parseFloat(ele_view_wrapper.offset().left);
			var baseTop = parseFloat(ele_view_wrapper.offset().top);
			// var imagePixel = imageDpi;
			var result = new Array();
			var resultIdx = 0;

			try {
				this.DeleteSelection();

				if (strColor == null || strColor == '') {
					strColor = 'red';
				}
				if (strStartCfi == null || strStartCfi == '') {
					rtnInfo[0] = '-1:'; //시작위치값이 없습니다.
					return rtnInfo;
				}
				if (strEndCfi == null || strEndCfi == '') {
					rtnInfo[0] = '-2:'; //종료위치값이 없습니다.
					return rtnInfo;
				}

				rtnCompare = Position._CheckCfiCompare(strStartCfi, strEndCfi);
				strStartCfi = rtnCompare['Big'];
				strEndCfi = rtnCompare['Small'];
				rtnCompare = null;
				rtnDraw = Position._GetDrawNodeByCfi(doc, strStartCfi, strEndCfi, true);

				var doc = document.createDocumentFragment();
				let prect = ele_view_wrapper.get(0).getBoundingClientRect()

				if (rtnDraw.constructor == Array) {
					nRtnDraw_Cnt = rtnDraw.length;
					for (let dl_idx = 0; dl_idx < nRtnDraw_Cnt; dl_idx++) {
						
						result[resultIdx] = new Array();
						result[resultIdx]['top'] = parseFloat(rtnDraw[dl_idx]['top'].toString()) - baseTop + _offsetTop;
						result[resultIdx]['left'] = parseFloat(rtnDraw[dl_idx]['left'].toString()) - prect.left + _padding;
						result[resultIdx]['width'] = parseFloat(rtnDraw[dl_idx]['width'].toString());
						result[resultIdx]['height'] = parseFloat(rtnDraw[dl_idx]['height'].toString());
						result[resultIdx]['width'] += Selection.getFillSelectionEmptySpace(dl_idx ,rtnDraw ,nRtnDraw_Cnt);
						
						result['SelectionText'] = rtnDraw[0]['SelectionText'].toString();

						re = Selection.EleSelectionDiv.cloneNode(true);
						re.id = 'Rue_Selection_Container_SEL_' + rtnDraw[dl_idx]['id'].toString();
						
						se = re.style;
						se.top = result[resultIdx]['top'] + 'px';
						se.left = result[resultIdx]['left'] + 'px';
						se.width = result[resultIdx]['width'] + 'px';
						se.height = result[resultIdx]['height'] + 'px';
						se.setProperty('background-color', strColor, 'important');

						doc.appendChild(re);
						se = null;
						re = null;
						result['res'] = 0;
						result[resultIdx]['rleft'] = parseFloat(rtnDraw[dl_idx]['rleft'].toString());
						resultIdx++;
					}
					Selection.EleSelectionArea.appendChild(doc);



					doc = null;
					nRtnDraw_Cnt = null;
					return result;
				}
				else {
					result['res'] = rtnDraw;
					return result;
					//rtnInfo[0] = rtnDraw;
					//return rtnInfo;
				}
			}
			catch (ex) {
				ePubEngineLogJS.W.log("exception = " + ex);
			}
			finally {
				isDebug = null;
				rtnInfo = null;
				rtnCompare = null;
				rtnDraw = null;
				nRtnDraw_Cnt = null;
				dl_idx = null;
				re = null;
				se = null;
				pop_top = null;
				pop_new_top = null;
				pageH = null;
			}
		},

		m_isDragStatus: WM_DRAG_INIT,
		m_strDragStartCfi:  '',
		m_strDragEndCfi:  '',
		m_strDragText:  '',
		m_strDragColor:  '#7b8ed2',
		m_strDragStartXPath:  '',
		m_strDragEndXPath:  '',
		m_strDragStartXPathOffset:  0,
		m_strDragEndXPathOffset:  0,
		m_strDragRegionJson:  '',
		m_selectionDoc: null,

		
		m_last_draw_x: 0.0,
		m_last_draw_y: 0.0,
		m_last_evt_x: 0.0,
		m_last_evt_y: 0.0,
		m_bSelect: false,
		m_nTimerCount: 0,
		m_arrDragRect: Array(),
		m_bSelectFront: false,
		
		/**
		 * 셀렉션 시작 CFI값 리턴
		 */
		getStartCfi: function () { 
			return Selection.m_strDragStartCfi; 
		},
		/**
		 * 셀렉션 끝 CFI값 리턴
		 */
		getEndCfi: function () { 
			return Selection.m_strDragEndCfi; 
		},
		/**
		 * 시작 XPath값
		 */
		getStartXPath: function () { 
			return Selection.m_strDragStartXPath; 
		},
		/**
		 * 끝 XPath값
		 */
		getEndXPath: function () { 
			return Selection.m_strDragEndXPath; 
		},
		/**
		 * 시작 XPath Offset값
		 */
		getStartXPathOffset: function () { 
			return Selection.m_strDragStartXPathOffset; 
		},
		/**
		 * 끝 XPath Offset값
		 */
		getEndXPathOffset: function () { 
			return Selection.m_strDragEndXPathOffset; 
		},
		/**
		 * 셀렉션 선택된 문자열
		 */
		getSelectionText: function () { 
			return Selection.m_strDragText; 
		},
		/**
		 * 셀렉션 선택된 (array) : 선택된 텍스트의 좌표값
		 */
		getSelectionRect: function () { 
			return Selection.m_arrDragRect; 
		},
		getSelectionRectJson: function () { 
			return Selection.m_strDragRegionJson;
		},
		/**
		 * 셀렉션 div 색상변경
		 * @param {string} strColor 색상 값 (#efefef)
		 */
		SetDrawSelectionColor: function (strColor) {
			Selection.m_strDragColor = strColor;
		},
		getDrawSelectionJson: function (id, res, color, sentence) {
			//sentence = sentence.replace(/"/g, "\'");
			sentence = sentence.replace(/\r?\n|\r/g, '');//linebreak때문에 parsejson할때오류가 발생함. 
			var rtnVal = new Object();

			rtnVal.id = id;
			rtnVal.res = res['res'];
			rtnVal.color = color;
			rtnVal.sentence = sentence;
			rtnVal.positions = new Array();

			for (var j = 0; j < res.length; j++) {
				var position = new Object();

				position.left = parseFloat(res[j]['left']);
				position.top = parseFloat(res[j]['top']);
				position.width = parseFloat(res[j]['width']);
				position.height = parseFloat(res[j]['height']);
				position.pageNo = 1;

				rtnVal.positions.push(position);
			}

			return JSON.stringify(rtnVal);
		},
		/**
		 * 셀렉션 div 그리기
		 * @param {boolean} wordBoundary 
		 * @returns 
		 */
		OnDrawTimer: function (doc, wordBoundary) {
			var isDebug = true;
			var strCfi_Temp = null;
			var res = null;
			var rtnVal = '';
			var id = '';
			var curPage = 0;
			var isWordBoundary;

			Selection.m_nTimerCount++;

			try {
				if (Selection.m_bSelect == false) {
					return;
				}

				if (Selection.m_nTimerCount > 1) {
					return;
				}

				while (true) {
					if (Selection.m_last_evt_x == Selection.m_last_draw_x && Selection.m_last_evt_y == Selection.m_last_draw_y) {
						// break;
					}

					Selection.m_last_draw_x = Selection.m_last_evt_x;
					Selection.m_last_draw_y = Selection.m_last_evt_y;

					// Selection.HideSelection();
					// if(Selection.m_strDragStartCfi == '' && Selection.m_strDragEndCfi == '') {
					//     isWordBoundary = true;
					// }
					// else {
					//     isWordBoundary = false;
					// }

					isWordBoundary = wordBoundary;
					strCfi_Temp = Position._cfiAt(doc, Selection.m_last_draw_x, Selection.m_last_draw_y, isWordBoundary);
					
					//Selection.ShowSelection();

					if (strCfi_Temp == null) {
						break;
					}

					if (Selection.m_strDragStartCfi == '') {
						Selection.m_strDragStartCfi = strCfi_Temp[0];
						Selection.m_strDragStartXPath = strCfi_Temp[1];
						Selection.m_strDragStartXPathOffset = strCfi_Temp[2];
						Selection.m_selectionDoc = strCfi_Temp[9];

						if (Selection.m_strDragEndCfi == '') {
							Selection.m_strDragEndCfi = strCfi_Temp[3];
							Selection.m_strDragEndXPath = strCfi_Temp[4];
							Selection.m_strDragEndXPathOffset = strCfi_Temp[5];
						}

						if (Selection.m_bSelectFront && thiDrawings.m_strDragEndCfi != '') {
							if (Selection.m_strDragStartCfi == Selection.m_strDragEndCfi) {
								break;
							}
							res = Selection.DrawSelection(Selection.m_selectionDoc, Selection.m_strDragStartCfi, Selection.m_strDragEndCfi, Selection.m_strDragColor);
							Selection.m_arrDragRect[0] = res[0]['rleft'];
							Selection.m_arrDragRect[1] = res[0]['top'];
							Selection.m_arrDragRect[2] = res[0]['width'];
							Selection.m_arrDragRect[3] = res[0]['height'];
							Selection.m_strDragText = res['SelectionText'];
							Selection.m_strDragRegionJson = Selection.getDrawSelectionJson(id, res, Selection.m_strDragColor, Selection.m_strDragText);
							break;
						}
						else {
							if (Selection.m_strDragStartCfi == Selection.m_strDragEndCfi) {
								ePubEngineLogJS.L.log("cfi same so break2222;");
								break;
							}

							res = Selection.DrawSelection(Selection.m_selectionDoc, Selection.m_strDragStartCfi, Selection.m_strDragEndCfi, Selection.m_strDragColor);
							Selection.m_arrDragRect[0] = res[0]['rleft'];
							Selection.m_arrDragRect[1] = res[0]['top'];
							Selection.m_arrDragRect[2] = res[0]['width'];
							Selection.m_arrDragRect[3] = res[0]['height'];
							Selection.m_strDragText = res['SelectionText'];

							Selection.m_strDragRegionJson = Selection.getDrawSelectionJson(id, res, Selection.m_strDragColor, Selection.m_strDragText);

							// if (RunaEngine.DeviceInfo.IsAndroid()) {
							// 	window.android.CreateSelectionResult(Selection.m_strDragRegionJson);
							// }

							break;
						}
					}
					else {
						if (Selection.m_strDragStartCfi == strCfi_Temp[0] && Selection.m_strDragEndCfi == strCfi_Temp[3]) {
							ePubEngineLogJS.L.log("cfi same so break  444");
							// break;
						}

						/**
						 * redmine refs #14501
						 * 셀렉션 영역 오류 수정
						 * by junghoon.kim
						 */
						Selection.m_strDragEndCfi = strCfi_Temp[3];
						Selection.m_strDragEndXPath = strCfi_Temp[4];
						Selection.m_strDragEndXPathOffset = strCfi_Temp[5];

						if (Selection.m_strDragStartCfi == Selection.m_strDragEndCfi) {
							ePubEngineLogJS.L.log("cfi same so break 5555");
							break;
						}

						res = Selection.DrawSelection(Selection.m_selectionDoc, Selection.m_strDragStartCfi, Selection.m_strDragEndCfi, Selection.m_strDragColor);
						Selection.m_arrDragRect[0] = res[0]['rleft'];
						Selection.m_arrDragRect[1] = res[0]['top'];
						Selection.m_arrDragRect[2] = res[0]['width'];
						Selection.m_arrDragRect[3] = res[0]['height'];
						Selection.m_strDragText = res['SelectionText'];

						Selection.m_strDragRegionJson = Selection.getDrawSelectionJson(id, res, Selection.m_strDragColor, Selection.m_strDragText);
						break;
					}
					break;
				}

				if (Selection.m_last_evt_x == Selection.m_last_draw_x && Selection.m_last_evt_y == Selection.m_last_draw_y && Selection.m_isDragStatus != WM_DRAG_START) {
					// draw stop
					//window.setTimeout( function() {Selection.OnDrawTimer();},0);
				}
				else if (Selection.m_isDragStatus == WM_DRAG_START && Selection.m_nTimerCount == 1) {
					//window.setTimeout( function() {Selection.OnDrawTimer();},500);
				}
			}
			catch (ex) {
				ePubEngineLogJS.L.log('[★Exception★][Selection.OnDrawTimer] ' + (ex.number & 0xFFFF));
				ePubEngineLogJS.L.log('[★Exception★][Selection.OnDrawTimer] ' + ex);
			}
			finally {
				isDebug = null;
				strCfi_Temp = null;
				rtnDrawSelect = null;

				Selection.m_nTimerCount--;

			}
		},
		OnSelectionResultCache: null,
		OnSelectionStart: function (doc, x, y, wordBoundary) {
			Selection.m_bSelect = true;
			Selection.m_isDragStatus = WM_DRAG_START;
			Selection.m_strDragStartCfi = '';
			Selection.m_strDragEndCfi = '';
			Selection.m_strDragText = '';
			Selection.m_strDragRegionJson = '';

			if (parseInt(x) < 0 || parseInt(y) < 0) {
				return true;
			}
			Selection.m_last_evt_x = parseInt(x);
			Selection.m_last_evt_y = parseInt(y);

			Selection.OnDrawTimer(doc, wordBoundary);
		},
		OnSelectionMove: function (doc, x, y, wordBoundary) {
			if (parseInt(x) < 0 || parseInt(y) < 0) {
				return true;
			}

			if (Selection.m_last_evt_x == parseInt(x) && Selection.m_last_evt_y == parseInt(y)) {
				// return true;
			}
			Selection.m_last_evt_x = parseInt(x);
			Selection.m_last_evt_y = parseInt(y);

			if (Selection.m_nTimerCount != 0 || Selection.m_bSelect == false || Selection.m_isDragStatus != WM_DRAG_START) {
				return false;
			}

			Selection.OnDrawTimer(doc, wordBoundary);
			// Selection.SelectionDivInfo();
			return true;
		},
		OnSelectionEnd: function (doc, x, y, wordBoundary) {
			Selection.m_bSelect = false;
			Selection.m_bSelectFront = false;
			Selection.m_isDragStatus = WM_DRAG_END;

			if (parseInt(x) < 0 || parseInt(y) < 0) {
				return true;
			}

			Selection.m_last_evt_x = parseInt(x);
			Selection.m_last_evt_y = parseInt(y);

			var rtnCompare = Position._CheckCfiCompare(Selection.m_strDragStartCfi, Selection.m_strDragEndCfi);
			Selection.m_strDragStartCfi = rtnCompare['Big'];
			Selection.m_strDragEndCfi = rtnCompare['Small'];

			ePubEngineLogJS.L.log("x = " + x + " y = " + y + " Selection.m_strDragStartCfi = " + Selection.m_strDragStartCfi + " Selection.m_strDragEndCfi = " + Selection.m_strDragEndCfi);
			Selection.OnDrawTimer(doc, wordBoundary);


			if (Selection.OnSelectionResultCache ){
				clearTimeout(Selection.OnSelectionResultCache)
				Selection.OnSelectionResultCache = null;
			}

			Selection.OnSelectionResultCache = setTimeout(() => {
				let result = {
					detail: { 
						text: null, 
						length: null,
						rects: null,
						lastX: x,
						lastY: y,
						nearest: null
					}
				}
				if (Selection.m_strDragStartCfi != Selection.m_strDragEndCfi){
					let ds = Position._decodeCFI(doc,Selection.m_strDragStartCfi);
					let de = Position._decodeCFI(doc,Selection.m_strDragEndCfi);
					if (ds && de){
						let range = Position.getRangeByNodeOffset(ds.node,ds.offset,de.node,de.offset)
						if (range){
							let t = Position.getTextByRange(range)
							if (typeof t === 'string'){
								result.detail.text = t;
								result.detail.length = t.length;
							}
							let rects = range.getClientRects();
							if (rects && rects.length > 0){
								result.detail.rects = rects
								let dist = Selection.distanceFromPoint(x,y)
								if (dist){
									result.detail.nearest = dist
								}
							}
						}
					}
				}
				window.dispatchEvent(new CustomEvent('bdbSelectionEnd',result))
			}, 100);
		},
		/**
		 * selection을 그릴 parent id
		 */
		 selectionParentID:'viewer_selection',
		 setSelectionParentID:function(id){
			 Selection.setSelectionParentID = id;
		 },
		/**
		 * 샐랙션사이 빈공간을 채울 float값을 return
		 * @param {*} idx 현재 index
		 * @param {*} array 배열
		 * @param {*} wholeCount 배열의 길이
		 */
		getFillSelectionEmptySpace:function(idx,array,wholeCount){
			
			//마지막 아이템은 채울필요없다.
			if (idx === (wholeCount-1) ){
								
			} else {
				//이전아이템이 필요하기 때문에 0이상부터
				if (idx>0){
					var prevSize = array[idx-1]['rect'].left +array[idx-1]['rect'].width;
					var currentLeft = array[idx]['rect'].left;
					//같은 높이에 있는 애들만 비교한다.
					if (array[idx-1]['rect'].top === array[idx]['rect'].top){
						//앞쪽 아이템의 right와 뒷쪽 아이템의 left가 다르면 뺀값만큼 뒷쪽 width를 보정한다.
						if (prevSize<currentLeft){
							return Math.abs(currentLeft - prevSize);
						} 
						//앞쪽이 더 큰경우 ..
						// else if (prevSize>currentLeft){
						// 	return -Math.abs(currentLeft - prevSize);
						// }
					}
				}
			}
			return 0;
		},
		
		/**
		 * 샐랙션한 노드들을 반환한다.
		 * @returns 
		 */
		 getSelectionNodes(isOnlyPicker){
			var container = document.querySelector('#'+Selection.selectionContainerID)
			if (container){
				if (isOnlyPicker){
					return container.querySelectorAll('img')
				}
				var children = container.querySelectorAll('div')
				if (children){
					return children
				}
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 마지막을 반환한다.
		 * @returns 
		 */
		 getSelectionLastNode(){
			var nodes = Selection.getSelectionNodes()
			if (nodes){
				return nodes[nodes.length -1];
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 마지막의 rects를 반환한다.
		 * @returns 
		 */
		getSelectionLastNodeRects(){
			var last =  Selection.getSelectionLastNode()
			if (last){
				return last.getBoundingClientRect()
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 첫번째를 반환한다.
		 * @returns 
		 */
		 getSelectionFirstNode(){
			var nodes = Selection.getSelectionNodes()
			if (nodes){
				return nodes[0];
			}
			return null
		},
		/**
		 * 샐랙션한 노드중 첫번째의 rects를 반환한다.
		 * @returns 
		 */
		getSelectionFirstNodeRects(){
			var last =  Selection.getSelectionFirstNode()
			if (last){
				return last.getBoundingClientRect()
			}
			return null
		},
		/**
		 * selection block에 x,y좌표로부터 위치를 반환해 rects를 반환
		 * @param {*} x 
		 * @param {*} y 
		 */
		distanceFromPoint(x,y){
			let first = Selection.getSelectionFirstNodeRects()
			let last = Selection.getSelectionLastNodeRects()
			if (!first || !last){
				return null;
			}
			let fDistance = Util.getDistance(x,y,first.left,first.top)
			let lDistance = Util.getDistance(x,y,last.right,last.bottom)
			if (fDistance<lDistance){
				return {isFront:true, rects:first}
			} else {
				return {isFront:false, rects:last}
			}
		},
	};

	var Info = Core.Info = {
		GetFindNode: function(posX, posY) {
			var selnode = document.elementFromPoint(posX, posY);
			var selnodename = '';
			var findDoc = null;
			var rct;

			try {
				selnodename = this._NodeName(selnode);

				if(selnodename == 'iframe') {
					findDoc = selnode.contentDocument;

					rct = selnode.getBoundingClientRect();

					var ratio = selnode.getAttribute("zdt");

					if(ratio != undefined) {
						ratio = parseFloat(ratio);
					}

					posX -= rct.left;
					posY -= rct.top;

					posX /= ratio;
					posY /= ratio;

					selnode = selnode.contentDocument.elementFromPoint(posX, posY);

					return {doc: findDoc, node: selnode};
				}

				return {doc: document, node: selnode};
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info.GetFindNode] ' + ex);
				return false;
			}
			finally {
				selnode = null;
				selnodename = null;
				rct = null;
			}

		},
		GetHitTest: function (doc, posX, posY) {
			var selnode = doc.elementFromPoint(posX, posY);
			var hasInteractive = false;

			try {

				hasInteractive = this._CheckInteractive(doc, selnode);

				return hasInteractive;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info.GetHitTest] ' + ex);
				return false;
			}
		},
		/**
		 * 해당 위치의 이미지 경로를 찾는다.
		 * @param {number} posX left 위치
		 * @param {number} posY top 위치
		 * @returns 이미지 경로를 리턴한다.
		 */
		GetImageSrc: function (doc, posX, posY) {
			var selnode = doc.elementFromPoint(posX, posY);
			var imgpath = '';
			var selnodename = '';

			try {

				if (selnode != null) {
					selnodename = this._NodeName(selnode);

					if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
						return '';
					}

					if (selnode.node == ele_view_wrapper.get(0)) {
						return '';
					}

					if (selnodename == "img") {
						//if(selnode.src.split("/content")[1])
						//	imgpath = Layout.syncSteg(baseInfo.token, selnode.src.split("/content")[1]);
						//else
							imgpath = selnode.src;
					}
				}

				return imgpath;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info.GetImageSrc] ' + ex);
				return '';
			}
			finally {
				selnode = null;
				selnodename = null;
				imgpath = null;
			}
		},
		/**
		 * 노드명을 소문자로 되돌려 준다.
		 * @param {Node} node 
		 * @returns 노드명
		 */
		_NodeName: function (node) {
			if (node == null) {
				return null;
			}

			var nodename = node.nodeName;

			if (nodename != null) {
				if (nodename != null) {
					nodename = nodename.toLowerCase();
				}
			}
			return nodename;
		},
		/**
		 * @type {node: 노드, doc: 해당노드의 document}
		 * @param {object} node 
		 * @returns 
		 */
		_CheckInteractive: function (doc, node) {
			var selnodename = '';
			var selnode = node;
			var hasInteractive = false;
			var pselnode = null;
			var pselnodename = '';

			try {

				selnodename = this._NodeName(selnode);

				if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
					return hasInteractive;
				}

				if (selnode == ele_view_wrapper.get(0)) {
					ePubEngineLogJS.W.log('[★Exception★][Info._CheckInteractive] -- '+ selnode.id);
					return hasInteractive;
				}

				if (selnodename == 'a') {
					var c = selnode.getAttribute("href");

					if (c != null) {
						c = c.toLowerCase();
						c = c.replace(/\s/g, "");

						if (c != "#" && c != "javascript:void(0)"
							&& c != "javascript:void(0);") {
							return true;
						}
					}
				}
				if (selnodename == "audio" || selnodename == "video" || selnodename == "embed" || selnodename == "object" || selnodename == "canvas") {
					// ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] -- multimedia');
					return true;
				}

				try {
					if (selnode.onmouseup != null || selnode.onclick != null || selnode.ondblclick != null || selnode.ontouchstart != null || selnode.ontouchmove != null || selnode.ontouchend != null) {
						//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] -- onclick');
						return true;
					}
				}
				catch(eex) {

				}

				//role check
				var role = selnode.getAttribute('role');
				if(role != undefined && role == 'button') {
					return true;
				}

				var nodescrollstyle = doc.defaultView.getComputedStyle(selnode, null);

				if (nodescrollstyle != null) {
					if (nodescrollstyle["overflow-x"] == "scroll" || nodescrollstyle["overflow-x"] == "auto") {
						if (selnode.scrollWidth > selnode.clientWidth) {
							hasInteractive = true;
							//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] 22');
							return hasInteractive;
						}
					}

					if (!hasInteractive
						&& (nodescrollstyle["overflow-y"] == "scroll" || nodescrollstyle["overflow-y"] == "auto")) {
						if (selnode.scrollHeight > selnode.clientHeight) {
							hasInteractive = true;
							//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] 33 scrollHeight = ' + selnode.scrollHeight + ' clientHeight =' + selnode.clientHeight);
							return hasInteractive;
						}
					}
				}


				if (!hasInteractive) {
					try {
						if ($(selnode).data("events")) {
							hasInteractive = true;
							return hasInteractive;
						}
					} catch (err) { }
				}

				// over jQuery 1.8
				if (!hasInteractive) {
					try {
						if ($._data(selnode, "events")) {
							hasInteractive = true;
							return hasInteractive;
						}
					} catch (err) { }
				}


				if (selnodename == "img") {
					if (selnode.parentNode != null) {
						pselnode = selnode.parentNode;
						pselnodename = this._NodeName(pselnode);

						if (pselnodename == 'a') {
							var c = pselnode.getAttribute("href");

							if (c != null) {
								c = c.toLowerCase();
								c = c.replace(/\s/g, "");

								//ePubEngineLogJS.L.log('[★Exception★][Info._CheckInteractive] a tag -- '+ c);

								if (c != "#" && c != "javascript:void(0)"
									&& c != "javascript:void(0);") {
									return true;
								}
							}
						}
					}
				}


				// if (selnode.node.parentNode != null) {
				// 	hasInteractive = this._CheckInteractive({doc: selnode.doc, node: selnode.node.parentNode});

				// 	if (hasInteractive)
				// 		return hasInteractive;

				// }

				if (selnode.children.length > 0) {
					for (var i = selnode.children.length - 1; i >= 0; i--) {
					    hasInteractive = this._CheckInteractive(doc, selnode.children[i]);

					    ePubEngineLogJS.L.log('[*********][Info._CheckInteractive] -- '+ selnode.children[i] +' , '+ selnode.children[i].innerHTML);

					    if(hasInteractive)
					        return hasInteractive;
					}
				}
				
				return hasInteractive;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info._CheckInteractive] ' + ex);
				return false;
			}
			finally {
				selnodename = '';
				selnode = null;
				pselnode = null;
				pselnodename = '';
			}

		},
		GetHrefInfo: function (doc, posX, posY) {

			//Highlight.HideAll();

			var selnode = doc.elementFromPoint(posX, posY);
			var strHrefInfo = '';

			try {
				strHrefInfo = this._GetHrefInfo(doc, selnode);

				return strHrefInfo;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info.GetHrefInfo] ' + ex);
				return '';
			}
			finally {
				strHrefInfo = '';
				selnode = null;
				//Highlight.ShowAll();
			}
		},
		_GetHrefInfo: function (doc, node) {
			var selnodename = '';
			var selnode = node;
			var strHrefInfo = '';
			var pselnode = null;
			var pselnodename = '';

			try {

				while (selnode.node != null || strHrefInfo != '') {
					selnodename = this._NodeName(selnode);

					//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] -- '+ selnode.node +', '+selnodename +', '+ selnode.node.innerHTML);

					if (selnodename == 'body' || selnodename == 'html' || selnodename == '#text') {
						break;
					}

					if (selnode == ele_view_wrapper.get(0)) {
						//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] -- '+ selnode.node.id);
						break;
					}

					if (selnodename == 'a') {
						var c = selnode.getAttribute("href");
						var d = selnode.getAttribute("href");

						if (c != null) {
							c = c.toLowerCase();
							c = c.replace(/\s/g, "");

							//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] a tag -- '+ c);

							if (c != "#" && c != "javascript:void(0)"
								&& c != "javascript:void(0);") {
								return d;
							}
						}
					}

					if (selnodename == "img") {
						if (selnode.parentNode != null) {
							pselnode = selnode.parentNode;
							pselnodename = this._NodeName(pselnode);

							if (pselnodename == 'a') {
								var c = pselnode.getAttribute("href");
								var d = selnode.getAttribute("href");

								if (c != null) {
									c = c.toLowerCase();
									c = c.replace(/\s/g, "");

									//ePubEngineLogJS.L.log('[★Exception★][Info._GetHrefInfo] a tag -- '+ c);

									if (c != "#" && c != "javascript:void(0)"
										&& c != "javascript:void(0);") {
										return d;
									}
								}
							}
						}
					}


					if (selnode.parentNode != null) {
						strHrefInfo = this._GetHrefInfo(doc, selnode.parentNode);
					}

					break;

				}

				return strHrefInfo;
			}
			catch (ex) {
				ePubEngineLogJS.E.log('[★Exception★][Info._GetHrefInfo] ' + ex);
				return '';
			}
			finally {
				selnodename = '';
				selnode = null;
				pselnode = null;
				pselnodename = '';
				strHrefInfo = '';
			}

		},
	};

	/**
	 *  SMIL
	 *  @description https://www.w3.org/publishing/epub3/epub-mediaoverlays.html
	 *  
	 */
	 const SMIL = Core.SMIL = {
		 /**
		  * @property media:active-class
		  */
		 activeClass: null,
		 /**
		  * @property media:playback-active-class
		  */
		 playbackClass: null,
		 /**
		  * [
		  * {
		  * fileno: 1, 
		  * seq: [
		  * 	audio: "", audio src
		  * 	begin: "", start time
		  * 	end: "", end time
		  * 	text: "", // id selector
		  * ]},
		  * ]
		  */
		 items: [],
		/**
		 * last file number
		 */
		 lastFileNo: null,
		 /**
		  * callback interface
		  */
		 callbackSmilFunc: null,
		 /**
		  * HLS 객체
		  */
		 hls: null,
		 /**
		  *  audio Element 객체
		  */
		 eleHLS: null,
		 /**
		  * 재생중인 아이템
		  */
		 currentItem: null,
		 /**
		  * 재생중인 파일번호
		  */
		 currentFileno: -1,
		 _InProgress: false,
		 _isMoved: false,
		 /**
		  * SMIL 데이터 초기화
		  * @param {object} smilInfo SMIL 정보
		  * @param {function(type, data)} callback SMIL callback function
		  */
		 setup: function (smilInfo, callback) {
			 if (smilInfo) {
				 SMIL.activeClass = smilInfo.activeclass;
				 SMIL.playbackClass = smilInfo.playbackactiveclass;
				 for (let item of smilInfo.items) {
					 for (let s of item.seq) {
						 s.audio = ePubFxEngineLoaderJS.Util.replaceAll(s.audio, "{CONTENT_DOMAIN}", ePubFxEngineLoaderJS.Data.getBaseUrl());
						 SMIL.items.push(Object.assign(s, { fileno: item.fileno }))
					 }
				 }
				 SMIL.lastFileNo = Math.max.apply(Math, SMIL.items.map(o => o.fileno));
				 SMIL.callbackSmilFunc = callback;
				 SMIL.hls = new Hls({ liveMaxBackBufferLength: 10 });
				 SMIL.eleHLS = document.createElement('audio');
				 SMIL._bindListener();
			 }
		 },
		 /**
		  * 현재 파일에 smil 파일이 존재하는지 여부를 return
		  * @param {*} fileno 
		  * @returns 
		  */
		 checkSmil: function (fileno) {
			if (SMIL.items.length === 0){
				return;
			}
			if (SMIL.currentFileno === fileno){
				let fitems = SMIL.items.filter(e=>e.fileno === fileno);
				if (fitems && fitems.length > 0){
					SMIL.currentItem = fitems[0];
				} else {
					SMIL.currentItem = null;
					SMIL.eleHLS.currentTime = 0;
				}
				if (SMIL.eleHLS.currentTime >= SMIL.currentItem.begin && SMIL.eleHLS.currentTime < SMIL.currentItem.end) {

				} else {
					SMIL.eleHLS.currentTime = SMIL.currentItem.begin
				}
			} else {
				SMIL.currentFileno = fileno;
				let fitems = SMIL.items.filter(e=>e.fileno === fileno);
				if (fitems && fitems.length > 0){
					SMIL.currentItem = fitems[0];	
					SMIL.eleHLS.currentTime = SMIL.currentItem.begin
				} else {
					SMIL.currentItem = null;
					SMIL.eleHLS.currentTime = 0;
				}
			}

			if (!SMIL.currentItem){
				if (SMIL.callbackSmilFunc){
					SMIL.callbackSmilFunc('disappear',SMIL.eleHLS);
				}
				return;
			} else {
				if (SMIL.callbackSmilFunc){
					SMIL.callbackSmilFunc('appear',SMIL.eleHLS);
				}
			}

			//재생중
			if (SMIL.isPlaying()){
				if (SMIL._isMoved){
					SMIL.eleHLS.play();
				}
				// console.log('재생중',SMIL._isMoved)
			} 
			//재생중이 아닐때 리셋
			else {
				//파일 변경
				if (SMIL.eleHLS.getAttribute('hlsroot') != SMIL.currentItem.audio) {
					SMIL.reset();
				} 
			}
			console.log(SMIL.currentFileno,SMIL.currentItem);
		 },
		 
		 /**
		  * 정지
		  */
		 stop: function () {
			let fitems = SMIL.items.filter(e=>e.fileno === SMIL.currentFileno);
			if (fitems && fitems.length > 0){
				SMIL.currentItem = fitems[0];
				SMIL.eleHLS.currentTime = SMIL.currentItem.begin
			} 

			 SMIL.eleHLS.pause();
			 SMIL.clearHighlight();
		 },
		 

		 /**
		  * audio의 현재 상태를 업데이트 시킨다.
		  * @returns 
		  */
		 update: function () {
			if (SMIL.isPlaying()){
				let idx = SMIL._getIndexFromCurrentTime();
				if (idx >= 0){
					if (SMIL.currentItem){
						if (SMIL.currentItem != SMIL.items[idx]){
							if (SMIL.currentFileno != SMIL.items[idx].fileno){
								if (SMIL._isMoved){
									console.error('파일번호 변경!',"이동중");
									return;
								}
								SMIL._isMoved = true;
								Navi.GotoPageByIndex(SMIL.items[idx].fileno)
							} else {
								SMIL.currentItem = SMIL.items[idx];
							}
						} else {
							SMIL.currentItem = SMIL.items[idx];
						}
					} else {
						SMIL.currentItem = SMIL.items[idx];
					}
				} else {
					console.error('not found time')
				}
				SMIL.updateHighlight();
			} else {
				SMIL.clearHighlight();
			}
		 },
		 /**
		  * 현재 item 기준으로 하이라이트를 업데이트한다.
		  * @returns 
		  */
		 updateHighlight: function () {
			 let flist = null;
			 let iframe = null;
			 let doc = null;
			 let target = null;
			 try {
				 if (SMIL.currentFileno < 0) {
					 return false
				 }
				 flist = SMIL.items.filter(e => e.fileno === SMIL.currentFileno);
				 iframe = document.querySelector(`[bdb-fixed-index="${SMIL.currentFileno}"]`)
				 if (!iframe) {
					 return false;
				 }
				 //active class
				 if (!SMIL.currentItem || !SMIL.currentItem.text) {
					 return false;
				 }
				 //clear all class
				 doc = iframe.contentDocument || iframe.contentWindow.document;
				 target = doc.querySelector('#' + SMIL.currentItem.text);
				 if (target) {
					 requestAnimationFrame(function(){
						for (let e of flist) {
							let ele = doc.getElementById(e.text)
							if (target === ele) {
								if (target.classList) {
									if (!target.classList.contains(SMIL.activeClass)) {
										target.classList.add(SMIL.activeClass);
									}
								} else {
									target.classList.add(SMIL.activeClass);
								}
							} else {
								ele.classList.remove(SMIL.activeClass)
							}
						}

					 })
				 }

				 return true;
			 } catch (e) {
				console.error(e);
			 } finally {
				//  flist = null;
				//  iframe = null;
				//  doc = null;
				//  target = null;
			 }
		 },
		 /**
		  * 하이라이트 부분을 제거한다.
		  * @returns 
		  */
		 clearHighlight: function () {
			 let flist = null;
			 let iframe = null;
			 let doc = null;
			 let target = null;
			 try {
				 if (SMIL.currentFileno < 0) {
					 return false
				 }
				 flist = SMIL.items.filter(e => e.fileno === SMIL.currentFileno);
				 iframe = document.querySelector(`[bdb-fixed-index="${SMIL.currentFileno}"]`)
				 if (!iframe) {
					 return false;
				 }
				 //active class
				 if (!SMIL.currentItem || !SMIL.currentItem.text) {
					 return false;
				 }
				 //clear all class
				 doc = iframe.contentDocument || iframe.contentWindow.document;
				 target = doc.querySelector('#' + SMIL.currentItem.text);
				 if (target) {
					 for (let e of flist) {
						 let ele = doc.getElementById(e.text)
						 if (ele && ele.classList) {
							 ele.classList.remove(SMIL.activeClass)
						 }
					 }
				 }

				 return true;
			 } catch (e) {

			 } finally {
				 flist = null;
				 iframe = null;
				 doc = null;
				 target = null;
			 }
		 },
		 /**
		  * 오디오를 초기화 한다.
		  * @returns 
		  */
		 reset: function () {
			 if (SMIL._InProgress) {
				 if (SMIL.callbackSmilFunc) {
					 SMIL.callbackSmilFunc('inprogress', SMIL.eleHLS)
				 }
				 return;
			 }
			 SMIL._InProgress = true;
			 if (SMIL.eleHLS) {
				 SMIL.eleHLS.pause();
				 SMIL.eleHLS.currentTime = 0;
			 }
			 let item = SMIL.currentItem;
			 if (!item) {
				 SMIL._InProgress = false;
				 return false;
			 }

			 let before = SMIL.eleHLS.getAttribute('hlsroot');
			 if (before != item.audio){
				SMIL.eleHLS.setAttribute('hlsroot', item.audio);
				if (Hls.isSupported()) {
					if (item.audio.indexOf('blob:') == -1) {
						SMIL.hls.loadSource(item.audio);
					}
				} else if (SMIL.eleHLS.canPlayType('application/vnd.apple.mpegurl')) {
					SMIL.eleHLS.src = item.audio
				}
			 } else {
				 console.log('same SRC')
				SMIL._InProgress = false;
			 }
		 },
		 /**
		  * 재생상태를 체크한다.
		  * @returns 
		  */
		 isPlaying: function () {
			 if (!SMIL.eleHLS) {
				 return false
			 }
			 if (!SMIL.eleHLS.paused) {
				 return true;
			 } else {
				 return false;
			 }
		 },
		 /**
		  * 재생하거나 reset하는 기능
		  * @param {*} fileno 
		  * @returns 
		  */
		 playOrPause: function (fileno) {
			 if (SMIL._InProgress) {
				 if (SMIL.callbackSmilFunc) {
					 SMIL.callbackSmilFunc('inprogress', SMIL.eleHLS)
				 }
				 return false;
			 }
			 if (typeof fileno === 'number'){
				 if (SMIL.currentFileno != fileno){
					SMIL.currentFileno = fileno
				 }
			 }
			 if (SMIL.currentItem) {
				 if (SMIL.eleHLS.getAttribute('hlsroot') != SMIL.currentItem.audio) {
					 SMIL.reset();
				 } else {
					 SMIL._InProgress = true;
					 if (SMIL.isPlaying()) {
						 SMIL.eleHLS.pause()
						 SMIL._InProgress = false;
					 } else {
						 SMIL.eleHLS.play().then(rs => {
							 SMIL._InProgress = false;
						 }).catch(e => {
							 SMIL._InProgress = false;
						 });
					 }
				 }
			 } 
			 else {
				 SMIL.reset();
			 }
		 },
		 pause: function () {
			 if (SMIL.isPlaying()) {
				 SMIL.eleHLS.pause()
			 }
		 },
		 /**
		  * 
		  * @param {*} audio 시간 점프
		  */
		 seekTo: function(time){
			if (typeof time === 'string'){
				time = parseInt(time);
			}
			if (typeof time != 'number'){
				return;
			}

			let src = SMIL.eleHLS.getAttribute('hlsroot')
			let item = SMIL._getItemFromTime(src,time);
			if (item){
				if (SMIL.currentFileno != item.fileno){
					// SMIL._isMoved = true;
					SMIL.currentItem = item;
					SMIL.eleHLS.currentTime = time;
					Navi.GotoPageByIndex(item.fileno);
				} else {
					SMIL.currentItem = item;
					SMIL.eleHLS.currentTime = time;
					SMIL.currentFileno = SMIL.currentItem.fileno;
				}
			} else {
				console.error('seekTo not found item');
			}
		 },
		 /**
		  * hls 및 audio 리스너 바인드
		  */
		  _bindListener: function () {
			if (Hls.isSupported()) {
				SMIL.hls.attachMedia(SMIL.eleHLS);
			   //  SMIL.hls.on(Hls.Events.MEDIA_ATTACHED, function () {
					
			   //  })
			   //  SMIL.hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
					
			   //  })
				SMIL.hls.on(Hls.Events.ERROR, function (event, data) {
					console.error('[SMIL][ERROR]', event, data);
				})
				if (typeof SMIL.callbackSmilFunc === 'function') {
					SMIL.hls.on(Hls.Events.MEDIA_ATTACHED, SMIL.callbackSmilFunc.bind(this, Hls.Events.MEDIA_ATTACHED, SMIL.eleHLS));
					SMIL.hls.on(Hls.Events.MANIFEST_PARSED, SMIL.callbackSmilFunc.bind(this, Hls.Events.MANIFEST_PARSED, SMIL.eleHLS));
					SMIL.hls.on(Hls.Events.ERROR, SMIL.callbackSmilFunc.bind(this, Hls.Events.ERROR, SMIL.eleHLS));
				}
			} else if (SMIL.eleHLS.canPlayType('application/vnd.apple.mpegurl')) {
				SMIL.eleHLS.addEventListener('canplay', function () {
					console.log('[canplay]')
				});
				if (typeof SMIL.callbackSmilFunc === 'function') {
					SMIL.eleHLS.addEventListener('canplay', SMIL.callbackSmilFunc.bind(this, 'canplay', SMIL.eleHLS))
				}
			}
			SMIL.eleHLS.addEventListener('timeupdate', SMIL._onHandleUpdate);
			SMIL.eleHLS.addEventListener('playing', SMIL._onHandleUpdate);
			SMIL.eleHLS.addEventListener('ended', SMIL._onHandleUpdate);
			SMIL.eleHLS.addEventListener('pause', SMIL._onHandleUpdate);
			SMIL.eleHLS.addEventListener('loadedmetadata', SMIL._onHandleUpdate);
			if (typeof SMIL.callbackSmilFunc === 'function') {
				SMIL.eleHLS.addEventListener('timeupdate', SMIL.callbackSmilFunc.bind(this, 'timeupdate', SMIL.eleHLS));
				SMIL.eleHLS.addEventListener('playing', SMIL.callbackSmilFunc.bind(this, 'playing', SMIL.eleHLS));
				SMIL.eleHLS.addEventListener('ended', SMIL.callbackSmilFunc.bind(this, 'ended', SMIL.eleHLS));
				SMIL.eleHLS.addEventListener('pause', SMIL.callbackSmilFunc.bind(this, 'pause', SMIL.eleHLS));
				SMIL.eleHLS.addEventListener('loadedmetadata', SMIL.callbackSmilFunc.bind(this, 'loadedmetadata', SMIL.eleHLS));
			}

		},
		/**
		  * 현재 오디오 재생시간으로 부터 items의 index를 찾는다.
		  * @returns 
		  */
		_getIndexFromCurrentTime: function(){
			let attr = SMIL.eleHLS.getAttribute('hlsroot')
			let findIndex = SMIL.items.findIndex(e => e.audio === attr && SMIL.eleHLS.currentTime >= e.begin && SMIL.eleHLS.currentTime < e.end);
			return findIndex
		},
		/**
		 * audio 리스너
		 * @param {*} e 
		 * @returns 
		 */
		 _onHandleUpdate: function (e) {
			let ty = e.type;
			if (ty === 'timeupdate') {
			   SMIL.update();
			} 
			/**
			 * Audio 파일 로드완료
			 */
			else if (ty === 'loadedmetadata') {
			   SMIL._InProgress = false;
			   if (SMIL.currentItem && SMIL.currentFileno >= 0){
				   SMIL.eleHLS.currentTime = SMIL.currentItem.begin
			   }
			   if (SMIL._isMoved){
				   SMIL.eleHLS.play().then(rs=>{
					   SMIL._isMoved = false;
				   }).catch(err=>{
					   SMIL._isMoved = false;
				   });
			   }
		   }
		   /**
			* Audio 파일 재생완료
			*/
		   else if (ty === 'ended') {
			   if (SMIL._isMoved){
				   return;
			   }
			   SMIL._isMoved = true;
			   let next_item = SMIL._getNext();
			   //다음 재생아이템으로 이동
			   if (next_item){
				   Navi.GotoPageByIndex(next_item.fileno);
			   } 
			   //다음재생없음
			   else {
				   let fitems = SMIL.items.filter(e=>e.fileno === SMIL.currentFileno);
				   if (fitems && fitems.length > 0){
					   SMIL.currentItem = fitems[0];
					   SMIL.eleHLS.currentTime = SMIL.currentItem.begin
				   } 
			   }
		   } 
		   else if (ty === 'playing') {

		   } 
		   else if (ty === 'pause') {

		   } 
		},
		/**
		 * 오디오 시간으로 부터 item을 가저온다.
		 * @param {*} src
		 * @param {*} time 
		 */
		_getItemFromTime: function(src,time){
			if (typeof src != 'string'){
				return null
			}
			if (typeof time != 'number'){
				return null
			}
			let idx = SMIL.items.findIndex(e=>e.audio === src && e.begin <= time && e.end > time);
			if (idx>=0){
				return SMIL.items[idx];
			}
			return null;
		},
		/**
		 * 재생중인 아이템 기준 다음 아이템을 찾아온다.
		 * @returns 
		 */
		_getNext: function () {
			 if (!SMIL.currentItem) {
				 return null
			 }
			 let idx = SMIL.items.findIndex(e=> e === SMIL.currentItem);
			 if (idx >= 0){
				idx += 1;
				return SMIL.items[idx];
			 } else {
				 return null;
			 }
		},
		/**
		 * 재생중인 아이템 기준 이전 아이템을 찾아온다.
		 * @returns 
		 */
		_getPrev: function () {
			 if (!SMIL.currentItem) {
				 return null
			 }
			 let idx = SMIL.items.indexOf(SMIL.currentItem);
			 idx -= 1;
			 return SMIL.items[idx];
		}
	 }
	
	var Util = Core.Util = {
		Trim: function (str) {
            return str.replace(/^\s+|\s+$/g, "")
        },
		/**
		 * 해당 노드가 텍스트 노드인지 여부.
		 * @param {Node} node 해당 노드
		 * @returns true or false
		 */
		isTextNode: function(node) {
			return node.nodeType == 3;
		},
		pointIsInOrAboveRect: function(x, y, rect) {
			return y < rect.bottom && x >= rect.left && x <= rect.right;
		},	
		collapsedRangeToRect: function(range, left) {
			var clientRect = range.getClientRects().length ? range.getClientRects()[range.getClientRects().length - 1] : null;
			if (!clientRect) {
				return null;
			}
			return {
				left   : left || clientRect.left,
				right  : clientRect.right,
				bottom : clientRect.bottom
			};
		},
		/**
		 * 
		 * @param {Node} node 
		 * @param {Range} range 
		 * @param {Number} offset 
		 * @param {Number} x 
		 * @param {Number} y 
		 * @param {Number} lastLeft 
		 * @returns 
		 */
		stepTextNode: function(node, range, offset, x, y, lastLeft) {
			range.setStart(node, offset);
			range.setEnd(node, offset);
			rect = collapsedRangeToRect(range, lastLeft);
			if (rect && pointIsInOrAboveRect(x, y, rect)) {
				if (rect.right - x > x - rect.left) {
					offset--;
				}
				return {
					node  : node,
					index : offset
				};
			}
			if (offset < node.length) {
				return stepTextNode(node, range, ++offset, x, y, rect ? rect.left : null);
			} 
			return null;
		},
		nodeIndex: function(node) {
			var i = 0;
			while ((node = node.previousSibling)) {
				i++;
			}
			return i;
		},
		findOffset: function(node, range, x, y) {
			if (isTextNode(node)) {
				var offset = stepTextNode(node, range, 0, x, y);
				if (offset) {
					return offset;
				}
			} 
			else {
				range.setEndAfter(node);
				var rect = range.getClientRects().length ? range.getClientRects()[range.getClientRects().length - 1] : null;
				if (rect && pointIsInOrAboveRect(x, y, rect)) {
					return {
						node  : node.parentNode,
						index : nodeIndex(node)
					};
				}
			}
	
			if (node.nextSibling) {
				return findOffset(node.nextSibling, range, x, y);
			}
	
			return {
				node  : node.parentNode,
				index : nodeIndex(node)
			};
		},
		fromPointIE: function(x, y, doc) {
			var range = null;
			var el = document.elementFromPoint(x, y);
	
			if(el == null) return null;
	
			if(window.getSelection && document.createRange) {
				range = document.createRange();
				range.selectNodeContents(el);
				range.collapse(true);
	
				var offset = {
					node  : el.firstChild,
					index : -1
				};
				
				if (!offset.node) {
					offset = {
						node  : el.parentNode,
						index : nodeIndex(el)
					};
				} else {
					offset = findOffset(offset.node, range, x, y);
				}
				return create(offset.node, offset.index);
			}
			else {
				return null;
			}
		},
		caretRangeFromPoint: function(doc, x, y) {
			position = null, range = null;
	
			if (typeof doc.caretPositionFromPoint != "undefined") {
				position = doc.caretPositionFromPoint(x, y);
				range = doc.createRange();
				range.setStart(position.offsetNode, position.offset);
				range.collapse(true);
			} 
			else if (typeof doc.caretRangeFromPoint != "undefined") {
				range = doc.caretRangeFromPoint(x, y);
			}
			else if (typeof doc.body.createTextRange != "undefined") {
				range = fromPointIE(x, y, doc);
			}
	
			return range;
		},
		GetNodePageNumber: function(node) {
			left = 0, top = 0;
			nMovePageNo = null;
			chapter_left = null, chapter_top = null;
	
			try {
				if(isChrome55) {
					chapter_top = $(node).prop("offsetTop") - top;
					nMovePageNo = parseInt(parseInt(chapter_top, 10) / this._viewer_height, 10);
				}
				else {
					chapter_left = $(node).prop("offsetLeft") - left;
					nMovePageNo = parseInt(parseInt(chapter_left, 10) / this._columnWidth, 10);
				}
				
				return parseInt(nMovePageNo, 10) + 1;
			}
			catch (ex) {
				return -1;
			}
			finally {
				left = null;
				nMovePageNo = null;
				chapter_left = null;
				top = null;
				chapter_top = null;
			}
		},
		inRange: function(x, min, max) {
			return x >= min && x <= max;
		},
	
		getRangeIndex: function(index,array){
			var result = -1
			if (typeof array !== 'undefined' && array.length > 0) {
				array.forEach((ele,idx) => {
					if (Core.Util.inRange(index, array[idx] ,array[idx+1])){
						result = idx
						return
					}
				});
			} else {
				throw 'must array type and array length more than 0'
			}
			return result
		},
		/**
		 * x,y 좌표들사이의 거리
		 * @param {*} x1 
		 * @param {*} y1 
		 * @param {*} x2 
		 * @param {*} y2 
		 * @returns 
		 */
		 getDistance(x1, y1, x2, y2){
			let y = x2 - x1;
			let x = y2 - y1;
			return Math.sqrt(x * x + y * y);
		},
		/**
         * 퍼센트 계산
         * @param {*} partialValue 부분 값
         * @param {*} totalValue 전체 값
         * @returns 
         */
		calcPercent: function(partialValue, totalValue){
			let r = (100 * partialValue) / totalValue
			if (typeof r === 'number'){
				if (r > 100){
					return 100
				} 
				return r
			} else {
				return 0
			}
		},
		outerHeight(el) {
			var height = el.offsetHeight;
			var style = window.getComputedStyle(el);
			height += parseInt(style.marginTop) + parseInt(style.marginBottom);
			return height;
		},
		/**
		 * chapter li를 탐색할때까지 추적한다.
		 * @param {*} node 
		 * @returns 
		 */
		findParent(node){
			let p = node.parentElement
			let f = null
			try {
				while(true){
					console.log(p);
					if (p === document || p === document.body){
						break;
					}
					p = p.parentElement
				}
				return p
			} catch(err){

			} finally {
				p = null
				f = null
			}
		},
	};

	var isIE = null;
	var IEVersion = null;

	var Support = Core.Support = {
		checkMSIE: function() {

			if(isIE == null && IEVersion == null) {
	
				agent = navigator.userAgent.toLowerCase();
				name = navigator.appName;
				word = null;
	
				if( name == "Microsoft Internet Explorer" ) word = "msie ";
				else {
					if(agent.search("trident") > -1) word = "rident/.*rv:"; 
					else if(agent.search("edge/") > -1) word = "edge/"; 
				}
	
				if(word == null) {
					isIE = false;
				}
				else {
					reg = new RegExp( word + "([0-9]{1,})(\\.{0,}[0-9]{0,1})" ); 
	
					if (  reg.exec( agent ) != null  ) IEVersion = RegExp.$1 + RegExp.$2; 
	
					isIE = true;
				}
			}
	
			return isIE;
		},
	}

	return Core;

}(ePubEngineLogJS, ePubFxEngineLoaderJS));