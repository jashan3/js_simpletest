/*
Runa Web ePub FixedLayout engine loader V1.0.0.1001
www.bookswage.com
(c) 2004-2021 by Crazy Eric. All rights reserved.
*/


/// <reference path="./epub.log.js" />
/// <reference path="../common/contentHelper.js" />
/// <reference path="../common/lib/jquery.waitforimages.min.js" />
/// <reference path="../common/lib/hls.min.js" />

/**
 * ePub FixedLayout Engine Loader
 */
 var ePubFxEngineLoaderJS = ePubFxEngineLoaderJS || (function(ContentHelperJS, undefined) {
	var ePubFxEngineLoaderJs;

    // Native ePubFxEngineLoaderJs from window (Browser)
    if (typeof window !== 'undefined' && window.ePubFxEngineLoaderJs) {
        ePubFxEngineLoaderJs = window.ePubFxEngineLoaderJs;
    }

	if (typeof self !== 'undefined' && self.ePubFxEngineLoaderJs) {
        ePubFxEngineLoaderJs = self.ePubFxEngineLoaderJs;
    }

	// Native ePubFxEngineLoaderJs from worker
    if (typeof globalThis !== 'undefined' && globalThis.ePubFxEngineLoaderJs) {
        ePubFxEngineLoaderJs = globalThis.ePubFxEngineLoaderJs;
    }

    // Native (experimental IE 11) ePubFxEngineLoaderJs from window (Browser)
    if (!ePubFxEngineLoaderJs && typeof window !== 'undefined' && window.msePubFxEngineLoaderJs) {
        ePubFxEngineLoaderJs = window.msePubFxEngineLoaderJs;
    }

	var Loader = {};
	/**
	 * 로딩 완료 여부
	 */
    var readyContent = false;
	/**
	 * 콘텐츠 내용이 들어갈 상위 태그의 아이디.
	 */
	var contentConainerId = '';
	/**
	 * 모든 spine item
	 */
	var spineAllItems = new Array();
	/**
	 * 순서대로 로딩할 챕터
	 */
	var spineItems = new Array();
	/**
	 * 기타 다른 챕터
	 */
	var spineNoItems = new Array();

	/**
	 * EPUB 에서 사용되는 콘텐츠의 SPREAD 방식 정의
	 * 0 : none, 1 : landscape, 2 : both, 3 : auto
	 */
	var _contentSpread = 0;

	/**
	 * EPUB 3.0 fixed-layout 스펙에서 사용되는 화면 방향
	 * 2: auto, 1 : landscape, 0 : portrait
	 */
	var _contentOrientation = 2;
	/**
	 * 페이지 모드 (true : 앙면, false : 단면)
	 */
	 var _pageMode = true;

	/**
     * 현재 보고 있는 spine
     */
	var leftSpine = -1;
	var rightSpine = -1;
	/**
	 * 전체 파일 수
	 */
	var totalSpine = 0; 
	var totalPage = 0;

	/**
	 * 왼쪽 프레임
	 * @type JQuery<HTMLElement>
	 */
	var _ele_left_Frame = null;
	/**
	 * 오른쪽 프레임
	 * @type JQuery<HTMLElement>
	 */
	var _ele_right_Frame = null;

	/**
	 * 오디오 콜백 함수.
	 */
	var callbackAudioFunc = null;
	/**
	 * 오디오, 비디오 element 
	 */
	var mediaItems = new Array();

	/**
	 * 콘텐츠 기본 경로
	 */
	var contentBaseUrl = '';
	 /**
	  * 인증 토큰
	  */
	var authToken = '';

	/**
	 * index attr
	 */
	const IFRAME_SELECTOR_ATTR = 'bdb-fixed-index';

	const Property = Loader.Property = {
		getIframeSelectAttr(){
			return IFRAME_SELECTOR_ATTR;
		},
		getIframeSelector(index){
			return `[${IFRAME_SELECTOR_ATTR}="${index}"]`
		}
	}

	var Data = Loader.Data = {

		getOriginSpineItem(idx) {
			if(idx < spineAllItems.length) {
				return spineAllItems[idx];
			}
			return null;
		},
		/**
		 * 렌더링이 될 전체 파일 수
		 * @returns 렌더링이 될 전체 파일 수
		 */
		getTotalSpine: function() {
			return totalSpine;
		},
		/**
		 * 해당 파일의 정보를 리턴한다.
		 * @param {number} idx 파일번호
		 * @returns 
		 */
		getSpineItem: function(idx) {
			if(idx < spineItems.length) {
				return spineItems[idx];
			}
			return null;
		},
		getFindSpineItemIndex: function(path) {
			var i = 0;

			if(!path || path == '') return 0;

			for(i = 0; i < totalSpine; i++) {
				var spine = spineItems[i];

				if(spine.orgpath === path) {
					return i;
				}
			}

			return 0;
		},
		/**
		 * 왼쪽 기준으로 현재 로딩된 파일 번호.
		 * @returns 현재 로딩된 파일 번호
		 */
		getLeftSpine: function() {
			return leftSpine;
		},
		getLefFrame: function() {
			return _ele_left_Frame;
		},
		getRightFrame: function() {
			return _ele_right_Frame;
		},
		getBaseUrl: function() {
			return contentBaseUrl;
		},
		/**
		 * 파일을 로딩 하기 위한 기본 데이터 준비.
		 * @param {object} items path, fileno, option, linear, bgcolor, percent
		 * @param {number} spread content's spread
		 * @param {number} orientation content's orientation
		 * @param {string} baseUrl content's base url
		 * @param {string} token 인증을 위한 토큰
		 * @param {string} containerId 콘텐츠를 넣을 부모 엘리먼트 아이디
		 * @param {function(tpe, element)} audioCallback 오디오 콜백 함수, type: timeupdate, onplaying, onended, onpause
		 */
		init: function(items, spread, orientation, baseUrl, token, containerId, audioCallback) {
			contentBaseUrl = baseUrl;
			authToken = token;
			contentConainerId = containerId;
			_contentSpread = spread;
			_contentOrientation = orientation;
			totalSpine = 0;
			callbackAudioFunc = audioCallback;

			theDomain = document.domain;

			for(var item of items) {
				/**
				 * 암호화된 콘텐츠
				 */
				item.contents = '';
				/**
				 * 데이터를 원격지에서 가져왔는지 여부.
				 */
				item.isLoaded = false;

				if(item.linear == 1) {
					item.subIndex = spineNoItems.length;
					spineNoItems.push(item);
				}
				else {
					item.subIndex = spineItems.length;
					spineItems.push(item);
					totalSpine++;
				}

				spineAllItems.push(item);
			}

			Obfucator.init();
		},
		reset: function() {
			spineItems = undefined;
			contentBaseUrl = '';
		},
		/**
		 * 난독화전 element tag에 cfi를 넣는다.
		 * @param {*} data 
		 */
		addCfiAttribute: function(doc){
			let _path = null
			let count = 0
			let eleList = doc.getElementsByTagName('*')
			for (let nd of eleList){
				if (nd instanceof HTMLUnknownElement || nd.toString() === '[object HTMLUnknownElement]'){
					continue;
				}
				if (nd.tagName === 'BODY' ||
					nd.tagName === 'TITLE' ||
					nd.tagName === 'META' ||
					nd.tagName === 'LINK' ||
					nd.tagName === 'SCRIPT'||
					nd.tagName === 'STYLE' ||
					nd.tagName === 'HTML' ||
					nd.tagName === 'HEAD' 
				){
					continue;
				}
				count ++;
				_path = ePubFxEngineCoreJS.Position._encodeCFISingleShot( doc , nd, nd.textContent.length, "")
				nd.setAttribute('cfi',_path)
				nd.setAttribute('tag-total',eleList.length)
				nd.setAttribute('tag-current',count)
			}
			_path = null
			return doc;
		},
		/**
		 * 콘텐츠를 추가한다.
		 * @param {number} curChapterNum 챕터 순번
		 * @param {string} data 챕터 데이터
		 * @returns 
		 */
		addedContents: function(curChapterNum, data, isLeft) {
			return new Promise( function(resolve, reject) {
				
				let strDom = new DOMParser();
				let doc = strDom.parseFromString(data, 'text/html');
				let parseDoc = Data.addCfiAttribute(doc);
				
				// console.table('[doc]',doc,parseDoc.documentElement.innerHTML);
				var obfuData = Obfucator.obfucator(parseDoc.documentElement.innerHTML);

				//body를 찾자
				var bodyStart = obfuData.indexOf('</head');

				if(bodyStart == -1) {
					bodyStart = obfuData.indexOf('</HEAD');
				}
				//make inject head 
				var originString = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
				var scriptDomain = '<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">';
				scriptDomain += '<meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">'
				// scriptDomain += '<script src="/script/common/lib/jquery-3.4.1.min.js"></script>'
				scriptDomain += '<link rel="stylesheet" href="/stylesheets/epub/fv_sub_style.css">'
				scriptDomain += '<script>window.location.origin = "'+originString+'";</script>'

				obfuData =  obfuData.substring(0, bodyStart -1) + scriptDomain + obfuData.substring(bodyStart);
				
				if(isLeft) {
					_ele_left_Frame.attr('srcdoc', obfuData);
					_ele_left_Frame.attr(IFRAME_SELECTOR_ATTR, curChapterNum);

					_ele_left_Frame.on('load', function() {
						// Data.addCfiAttribute(this);
						_ele_left_Frame.contents().find("video").filter(function () {
							//$(this).removeAttr("preload");
							$(this).removeAttr("autoplay");
	
							$(this).attr('ruaIdx', mediaItems.length);
							
							mediaItems.push(this);
	
							if (Hls.isSupported()) {
								var hls = new Hls({liveMaxBackBufferLength: 10});
	
								var src = this.src;
	
								if(src == undefined) {
									src = this.currentSrc;
								}
								else if(src == '') {
									src = $(this).find('Source:first').attr('src');
									$(this).find('Source:first').attr('type', 'application/x-mpegURL');
								}
								
								if(src && src.indexOf('blob:') == -1) {
									this.src = src;

									hls.attachMedia(this);
									
									hls.on(Hls.Events.MEDIA_ATTACHED, function () {
										ePubEngineLogJS.L.log("hls.js are now bound together !");
		
										hls.loadSource(src);
		
										hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {
											ePubEngineLogJS.L.log('video MANIFEST_PARSED manifest loaded, found ' + data.levels.length + ' quality level');
										});
									});
									
									hls.on(Hls.Events.ERROR, function (event, data) {
										if (data.fatal) {
											switch (data.type) {
											case Hls.ErrorTypes.NETWORK_ERROR:
												// try to recover network error
												ePubEngineLogJS.W.log('fatal network error encountered, try to recover');
												hls.startLoad();
												break;
											case Hls.ErrorTypes.MEDIA_ERROR:
												ePubEngineLogJS.W.log('fatal media error encountered, try to recover');
												hls.recoverMediaError();
												break;
											default:
												// cannot recover
												ePubEngineLogJS.W.log('fatal media error encountered, cant recover');
												hls.destroy();
												break;
											}
										}
									});
								}
							}
							else if (this.canPlayType('application/vnd.apple.mpegurl')) {
								//alert(' not support');
							}
	
							$(this).get(0).onplaying = function () {
								
								var idx = this.getAttribute('ruaIdx');
	
								// 재생을 누른 녀석 말고는 전부 정지 한다.
								for(var a of mediaItems ) {
									var cIdx = a.getAttribute('ruaIdx');
	
									if(cIdx !== idx) {
										a.pause();
									}
								}
							};
						});
	
						_ele_left_Frame.contents().find("audio").filter(function () {
							//$(this).removeAttr("preload");
							$(this).removeAttr("autoplay");
	
							$(this).attr('ruaIdx', mediaItems.length);
							
							mediaItems.push(this);
	
							if (Hls.isSupported()) {
								var hls = new Hls({liveMaxBackBufferLength: 10});
	
								var src = this.src;
	
								if(src == undefined) {
									src = this.currentSrc;
								}
								else if(src == '') {
									src = $(this).find('Source:first').attr('src');
								}
								
								if(src && src.indexOf('blob:') == -1) {
									hls.attachMedia(this);

									hls.on(Hls.Events.MEDIA_ATTACHED, function () {
										ePubEngineLogJS.L.log("hls.js are now bound together !");
		
										hls.loadSource(src);
		
										hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {
											ePubEngineLogJS.L.log('video MANIFEST_PARSED manifest loaded, found ' + data.levels.length + ' quality level');
										});
									});
									
									hls.on(Hls.Events.ERROR, function (event, data) {
										if (data.fatal) {
											switch (data.type) {
											case Hls.ErrorTypes.NETWORK_ERROR:
												// try to recover network error
												ePubEngineLogJS.W.log('fatal network error encountered, try to recover');
												hls.startLoad();
												break;
											case Hls.ErrorTypes.MEDIA_ERROR:
												ePubEngineLogJS.W.log('fatal media error encountered, try to recover');
												hls.recoverMediaError();
												break;
											default:
												// cannot recover
												ePubEngineLogJS.W.log('fatal media error encountered, cant recover');
												hls.destroy();
												break;
											}
										}
									});
								}
							}
							else if (this.canPlayType('application/vnd.apple.mpegurl')) {
								//alert(' not support');
							}
	
							$(this).get(0).ontimeupdate = function () {
								var idx = this.getAttribute('ruaIdx');
								
								ePubEngineLogJS.L.log('audio ['+idx+'], timeupdate : '+ this.currentTime +' / ' + this.duration);
	
								if (callbackAudioFunc) {
									callbackAudioFunc('timeupdate', this);
								}
							};
							$(this).get(0).onplaying = function () {
								
								var idx = this.getAttribute('ruaIdx');
	
								ePubEngineLogJS.L.log('audio ['+idx+'], onplaying : '+ this.currentTime +' / ' + this.duration);
	
								if (callbackAudioFunc) {
									callbackAudioFunc('onplaying', this);
								}
	
								// 재생을 누른 녀석 말고는 전부 정지 한다.
								for(var a of mediaItems ) {
									var cIdx = a.getAttribute('ruaIdx');
	
									if(cIdx !== idx) {
										a.pause();
									}
								}
							};
							$(this).get(0).onended = function () {
								var idx = this.getAttribute('ruaIdx');
								
								ePubEngineLogJS.L.log('audio ['+idx+'], onended : '+ this.currentTime +' / ' + this.duration);
	
								if (this.currentTime >= this.duration && this.paused)
									this.currentTime = 0;
	
								if (callbackAudioFunc) {
									callbackAudioFunc('onended', this);
								}
							};
							$(this).get(0).onpause = function () {
								var idx = this.getAttribute('ruaIdx');
								
								ePubEngineLogJS.L.log('audio ['+idx+'], onpause : '+ this.currentTime +' / ' + this.duration);
	
								if (callbackAudioFunc) {
									callbackAudioFunc('onpause', this);
								}
							};
	
							return true;
						});

						_ele_left_Frame.get(0).contentDocument.oncontextmenu = function() {
							return false;
						}
						// text selection disable
						_ele_left_Frame.get(0).contentDocument.onselectstart = function() {
							return false;
						}

						var ifr = _ele_left_Frame.get(0);
						var doc = ifr.contentDocument || ifr.contentWindow.document;
						//iframe document로 접근하면 mouseup이벤트에 접근 불가하다.
						var bd = doc.querySelector('body');

						$(bd).off('mousewheel').on('mousewheel', function(e) {
							var evt = Util.copyEvent(e, _ele_left_Frame);
							$("#"+contentConainerId).trigger(evt);
						});

						$(bd).off('mousedown touchstart').on('mousedown touchstart', function(e) {
							var evt = Util.copyEvent(e, _ele_left_Frame);
							$("#"+contentConainerId).trigger(evt);
						});

						$(bd).off('mouseup touchend').on('mouseup touchend', function(e) {
							var evt = Util.copyEvent(e, _ele_left_Frame);
							$("#"+contentConainerId).trigger(evt);
						});

						$(bd).off('mousemove touchmove').on('mousemove touchmove', function(e) {
							var evt = Util.copyEvent(e, _ele_left_Frame);
							$("#"+contentConainerId).trigger(evt);
						});

						$(bd).off('mouseleave touchcancel').on('mouseleave touchcancel', function(e) {
							var evt = Util.copyEvent(e, _ele_left_Frame);
							evt.type = 'iframemouseout';
							$("#"+contentConainerId).trigger(evt);
						});

						//copy key disable
						$(_ele_left_Frame.get(0).contentDocument).keydown(function(e) {
		
							if(e.ctrlKey && (65 == e.keyCode || 97 == e.keyCode)) {
								e.preventDefault();
								return;
							}

							var evt = Util.copyEvent(e, _ele_left_Frame);
							$(document).trigger(evt);
						});
						
						_ele_left_Frame.contents().find('body').waitForImages({
							finished: function() {
								resolve();
							},
							each: function() {
								if (this.tagName === 'IMG'){
									let role = this.getAttribute("role");
									if (!role||(role&&role!='button')){
										let parent = this.parentElement;
										if (parent){
											let cList = parent.classList;
											if (cList&& cList.contains('_idGenObjectStyle-Disabled')){
												parent.style.pointerEvents = 'none';
											}
										}
									}
								}
								//이미지에 워터마크 넣는 부분
								//if(this.src.split("/content")[1])
								//	this.src = "data:image/gif;base64," + ePubFxEngineCoreJS.Layout.syncSteg(baseInfo.token, this.src.split("/content")[1]);
								
								ePubEngineLogJS.L.log('loader wait image : ['+curChapterNum+'] :: ');
							},
							waitForAll: true
						});
					});
				}
				else {
					_ele_right_Frame.attr('srcdoc', obfuData);
					_ele_right_Frame.attr(IFRAME_SELECTOR_ATTR, curChapterNum);
					_ele_right_Frame.on('load', function() {
						// Data.addCfiAttribute(this);
						_ele_right_Frame.contents().find("video").filter(function () {
							//$(this).removeAttr("preload");
							$(this).removeAttr("autoplay");
	
							$(this).attr('ruaIdx', mediaItems.length);
							
							mediaItems.push(this);
	
							if (Hls.isSupported()) {
								var hls = new Hls({liveMaxBackBufferLength: 10});
	
								var src = this.src;
	
								if(src == undefined) {
									src = this.currentSrc;
								}
								else if(src == '') {
									src = $(this).find('Source:first').attr('src');
									$(this).find('Source:first').attr('type', 'application/x-mpegURL');
								}
								
								if(src && src.indexOf('blob:') == -1) {
									hls.attachMedia(this);

									hls.on(Hls.Events.MEDIA_ATTACHED, function () {
										ePubEngineLogJS.L.log("hls.js are now bound together !");
		
										hls.loadSource(src);
		
										hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {
											ePubEngineLogJS.L.log('video MANIFEST_PARSED manifest loaded, found ' + data.levels.length + ' quality level');
										});
									});
									
									hls.on(Hls.Events.ERROR, function (event, data) {
										if (data.fatal) {
											switch (data.type) {
											case Hls.ErrorTypes.NETWORK_ERROR:
												// try to recover network error
												ePubEngineLogJS.W.log('fatal network error encountered, try to recover');
												hls.startLoad();
												break;
											case Hls.ErrorTypes.MEDIA_ERROR:
												ePubEngineLogJS.W.log('fatal media error encountered, try to recover');
												hls.recoverMediaError();
												break;
											default:
												// cannot recover
												ePubEngineLogJS.W.log('fatal media error encountered, cant recover');
												hls.destroy();
												break;
											}
										}
									});
								}
							}
							else if (this.canPlayType('application/vnd.apple.mpegurl')) {
								//alert(' not support');
							}
	
							$(this).get(0).onplaying = function () {
								
								var idx = this.getAttribute('ruaIdx');
	
								// 재생을 누른 녀석 말고는 전부 정지 한다.
								for(var a of mediaItems ) {
									var cIdx = a.getAttribute('ruaIdx');
	
									if(cIdx !== idx) {
										a.pause();
									}
								}
							};
							
							return true;
						});
	
						_ele_right_Frame.contents().find("audio").filter(function () {
							//$(this).removeAttr("preload");
							$(this).removeAttr("autoplay");
	
							$(this).attr('ruaIdx', mediaItems.length);
							
							mediaItems.push(this);
	
							if (Hls.isSupported()) {
								var hls = new Hls({liveMaxBackBufferLength: 10});
	
								var src = this.src;
	
								if(src == undefined) {
									src = this.currentSrc;
								}
								else if(src == '') {
									src = $(this).find('Source:first').attr('src');
								}
								
								if(src && src.indexOf('blob:') == -1) {
									hls.attachMedia(this);

									hls.on(Hls.Events.MEDIA_ATTACHED, function () {
										ePubEngineLogJS.L.log("hls.js are now bound together !");
		
										hls.loadSource(src);
		
										hls.on(Hls.Events.MANIFEST_PARSED,function(event, data) {
											ePubEngineLogJS.L.log('audio MANIFEST_PARSED manifest loaded, found ' + data.levels.length + ' quality level');
										});
									});
									
									hls.on(Hls.Events.ERROR, function (event, data) {
										if (data.fatal) {
											switch (data.type) {
											case Hls.ErrorTypes.NETWORK_ERROR:
												// try to recover network error
												ePubEngineLogJS.W.log('fatal network error encountered, try to recover');
												hls.startLoad();
												break;
											case Hls.ErrorTypes.MEDIA_ERROR:
												ePubEngineLogJS.W.log('fatal media error encountered, try to recover');
												hls.recoverMediaError();
												break;
											default:
												// cannot recover
												ePubEngineLogJS.W.log('fatal media error encountered, cant recover');
												hls.destroy();
												break;
											}
										}
									});
								}
							}
							else if (this.canPlayType('application/vnd.apple.mpegurl')) {
								//alert(' not support');
							}
	
							$(this).get(0).ontimeupdate = function () {
								var idx = this.getAttribute('ruaIdx');
								
								ePubEngineLogJS.L.log('audio ['+idx+'], timeupdate : '+ this.currentTime +' / ' + this.duration);
	
								if (callbackAudioFunc) {
									callbackAudioFunc('timeupdate', this);
								}
							};
							$(this).get(0).onplaying = function () {
								
								var idx = this.getAttribute('ruaIdx');
	
								ePubEngineLogJS.L.log('audio ['+idx+'], onplaying : '+ this.currentTime +' / ' + this.duration);
	
								if (callbackAudioFunc) {
									callbackAudioFunc('onplaying', this);
								}
	
								// 재생을 누른 녀석 말고는 전부 정지 한다.
								for(var a of mediaItems ) {
									var cIdx = a.getAttribute('ruaIdx');
	
									if(cIdx !== idx) {
										a.pause();
									}
								}
							};
							$(this).get(0).onended = function () {
								var idx = this.getAttribute('ruaIdx');
								
								ePubEngineLogJS.L.log('audio ['+idx+'], onended : '+ this.currentTime +' / ' + this.duration);
	
								if (this.currentTime >= this.duration && this.paused)
									this.currentTime = 0;
	
								if (callbackAudioFunc) {
									callbackAudioFunc('onended', this);
								}
							};
							$(this).get(0).onpause = function () {
								var idx = this.getAttribute('ruaIdx');
								
								ePubEngineLogJS.L.log('audio ['+idx+'], onpause : '+ this.currentTime +' / ' + this.duration);
	
								if (callbackAudioFunc) {
									callbackAudioFunc('onpause', this);
								}
							};
	
							return true;
						});

						_ele_right_Frame.get(0).contentDocument.oncontextmenu = function() {
							return false;
						}
						//text selection disable
						_ele_right_Frame.get(0).contentDocument.onselectstart = function() {
							return false;
						}

						//$(_ele_right_Frame.get(0).contentDocument).disableSelection();

						// $(_ele_right_Frame.get(0).contentDocument).on('click', function(e) {
						// 	e.preventDefault();
						// 	var evt = Util.copyEvent(e, _ele_right_Frame);
						// 	$("#"+contentConainerId).parent().trigger(evt);
						// });

						$(_ele_right_Frame.get(0).contentDocument).off('mousewheel').on('mousewheel', function(e) {
							// e.preventDefault();
							var evt = Util.copyEvent(e, _ele_right_Frame);
							$("#"+contentConainerId).trigger(evt);
						});

						$(_ele_right_Frame.get(0).contentDocument).off('mousedown touchstart').on('mousedown touchstart', function(e) {
							// e.preventDefault();
							var evt = Util.copyEvent(e, _ele_right_Frame);
							$("#"+contentConainerId).trigger(evt);
						});
	
						$(_ele_right_Frame.get(0).contentDocument).off('mouseup touchend').on('mouseup touchend', function(e) {
							var evt = Util.copyEvent(e, _ele_right_Frame);
							$("#"+contentConainerId).trigger(evt);
						});

						$(_ele_right_Frame.get(0).contentDocument).off('mousemove touchmove').on('mousemove touchmove', function(e) {
							// e.preventDefault();
							var evt = Util.copyEvent(e, _ele_right_Frame);
							$("#"+contentConainerId).trigger(evt);
						});

						$(_ele_right_Frame.get(0).contentDocument).off('mouseleave touchcancel').on('mouseleave touchcancel', function(e) {
							var evt = Util.copyEvent(e, _ele_right_Frame);
							evt.type = 'iframemouseout';
							$("#"+contentConainerId).trigger(evt);
						});

						//copy key disable
						$(_ele_right_Frame.get(0).contentDocument).keydown(function(e) {
		
							if(e.ctrlKey && (65 == e.keyCode || 97 == e.keyCode)) {
								e.preventDefault();
								return;
							}
		
							var evt = Util.copyEvent(e, _ele_right_Frame);
							$(document).trigger(evt);
						});
						
						_ele_right_Frame.contents().find('body').waitForImages({
							finished: function() {
								resolve();
							},
							each: function() {
								if (this.tagName === 'IMG'){
									let role = this.getAttribute("role");
									if (!role||(role&&role!='button')){
										let parent = this.parentElement;
										if (parent){
											let cList = parent.classList;
											if (cList&& cList.contains('_idGenObjectStyle-Disabled')){
												parent.style.pointerEvents = 'none';
											}
											// console.log(parent);
										}
									}
								}
								ePubEngineLogJS.L.log('loader wait image : ['+curChapterNum+'] :: ');
							},
							waitForAll: true
						});
					});
				}
			});
		},
		/**
		 * 처음 페이지를 로딩하는 함수
		 * @param {number} index 파일번호
		 * @param {boolean} pageMode 페이지모드
		 * @returns 
		 */
		loadSpines: function(index, pageMode) {
			leftSpine = index;
			_pageMode = pageMode;

			audioItems = [];

			this.makeContentTag();

			var isEnd = false;
			var isLeft = true;

			if(_contentSpread != 0) {
				
				if(index == 0) {
					isLeft = false;
					isEnd = true;
				}
				else {
					var item = spineItems[index];

					// page-spread-left, page-spread-right, page-spread-center, 공백
					if(item.option && item.option == 'page-spread-left') {
						isLeft = true;
					}
					else if(item.option && item.option == 'page-spread-right') {
						isLeft = false;
					}
					else if(item.option && item.option == 'page-spread-center') {
						isLeft = true;
					}
					else {
						isLeft = true;
					}
				}
			}
			else {
				if(_pageMode) {
					if(index == 0) {
						isLeft = false;
						isEnd = true;
					}
				}
				else {
					isLeft = true;
					isEnd = false;
				}
			}

			_ele_left_Frame.hide();
			_ele_left_Frame.attr('srcdoc', '');
			_ele_left_Frame.html('');

			if(_ele_right_Frame) {
				_ele_right_Frame.hide();
				_ele_right_Frame.attr('srcdoc', '');
				_ele_right_Frame.html('');
			}
			
			return this.loadSpine(index, isLeft, isEnd);
		},
		/**
		 * 기본 영역을 만들기 위해 태그를 생성한다.
		 */
		makeContentTag: function() {
			var contents = $("#"+contentConainerId);
	
			contents.empty();
	
			if(_ele_left_Frame != null) {
				_ele_left_Frame = null;
			}

			_ele_left_Frame = $('<iframe id="bdbLeftFrame" class="bdbiframe" scrolling="no" border="no" maginwidth="0" marginheight="0" frameborder="0" width="100" height="100"/>');
			contents.append(_ele_left_Frame);

			if(_ele_right_Frame != null ) {
				_ele_right_Frame = null;
			}

			if(_pageMode || _contentSpread != 0) {
				_ele_right_Frame = $('<iframe id="bdbRightFrame" class="bdbiframe" scrolling="no" border="no" maginwidth="0" marginheight="0" frameborder="0" width="100" height="100"/>');
				contents.append(_ele_right_Frame);
			}
		},
		/**
		 * 지정한 챕터의 내용을 다운로드 및 추가 한다.
		 * @param {number} idx 파일번호
		 * @param {boolean} isLeft 양면 일때 오른쪽 왼쪽 옵션
		 * @returns resolve, reject 으로 로딩한 챕터 번호를 리턴한다.
		 */
		loadSpineAt: function(idx, isLeft) {
			leftSpine = idx;			
			audioItems = [];

			var isEnd = false;

			if(_contentSpread != 0) {
				
				if(idx == 0) {
					isLeft = false;
					isEnd = true;
				}
				else {
					var item = spineItems[idx];

					// page-spread-left, page-spread-right, page-spread-center, 공백
					if(item.option && item.option == 'page-spread-left') {
						isLeft = true;
					}
					else if(item.option && item.option == 'page-spread-right') {
						isLeft = false;
					}
					else if(item.option && item.option == 'page-spread-center') {
						isLeft = true;
					}
					else {
						isLeft = true;
					}
				}
			}
			else {
				if(_pageMode) {
					if(idx == 0) {
						isLeft = false;
						isEnd = true;
					}
				}
				else {
					isLeft = true;
					isEnd = false;
				}
			}

			_ele_left_Frame.hide();
			_ele_left_Frame.attr('srcdoc', '');
			_ele_left_Frame.html('');

			if(_ele_right_Frame) {
				_ele_right_Frame.hide();
				_ele_right_Frame.attr('srcdoc', '');
				_ele_right_Frame.html('');
			}

			return Data.loadSpine(idx, isLeft, isEnd);
		},
		/**
		 * 지정한 챕터의 내용을 다운로드 및 추가 한다.
		 * @param {number} idx 파일번호
		 * @param {boolean} isLeft 양면 일때 오른쪽 왼쪽 옵션
		 * @param {boolean} isEnd 해당 파일 로딩 후 종료
		 * @returns resolve, reject 으로 로딩한 챕터 번호를 리턴한다.
		 */
		loadSpine: function(idx, isLeft, isEnd) {

			return new Promise( function(resolve, reject) {
				
				if(idx < 0 || idx >= totalSpine) {
					
					reject(idx);
					return;
				}

				var item = spineItems[idx];

				if(item == undefined) {
					console.log('invalid index : '+idx);
					return reject(idx);
				}

				
				// 해당 챕터를 로딩
				if(item.isLoaded) {
					var decrypted = Util.decryptContents(item.contents);

					// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
					// {CONTENT_IMG_API}&url={CONTENT_DOMAIN}
					decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

					Data.addedContents(idx, decrypted, isLeft).then(function() {

						if(isEnd) {
							resolve(idx);
							return;
						}

						if(idx + 1 < 0 || idx + 1 >= totalSpine) {					
							resolve(idx);
							return;
						}
						
						if(_contentSpread != 0) {
							var nextItem = spineItems[idx + 1];
							var moreSpineSide = false;

							if(nextItem.option && nextItem.option == 'page-spread-left') {
								moreSpineSide = true;
							}
							else if(nextItem.option && nextItem.option == 'page-spread-right') {
								moreSpineSide = false;
							}
							else if(nextItem.option && nextItem.option == 'page-spread-center') {
								moreSpineSide = false;
							}
							else {
								moreSpineSide = false;
							}

							Data.loadSpine(idx + 1, moreSpineSide, true)
								.then(function(data) {
									resolve(data);
								})
								.catch(function(err) {
									ePubEngineLogJS.L.log(err);
									reject(idx + 1);
								});

						}
						else {
							if(_pageMode) {
								Data.loadSpine(idx + 1, false, true)
									.then(function(data) {
										resolve(data);
									})
									.catch(function(err) {
										ePubEngineLogJS.L.log(err);
										reject(idx + 1);
									});
							}
							else {
								if(_contentSpread != 0) {
									Data.loadSpine(idx + 1, false, true)
										.then(function(data) {
											resolve(data);
										})
										.catch(function(err) {
											ePubEngineLogJS.L.log(err);
											reject(idx + 1);
										});
								}
								else {
									resolve(idx);
								}
							}
						}
					})
					.catch(function(err) {
						console.log(err);
						reject(-1);
					});
				}
				else {
					Util.getUrl(contentBaseUrl, item.path, authToken)
					.then(function(data) {

						item.contents = data.value;
						item.isLoaded = true;

						var decrypted = Util.decryptContents(data.value);

						// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
						// {CONTENT_IMG_API}&url={CONTENT_DOMAIN}
						decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
						decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

						Data.addedContents(idx, decrypted, isLeft ).then(function() {
							if(isEnd) {
								resolve(idx);
								return;
							}

							if(idx + 1 < 0 || idx + 1 >= totalSpine) {					
								resolve(idx);
								return;
							}

							if(_contentSpread != 0) {
								var nextItem = spineItems[idx + 1];
								var moreSpineSide = false;

								if(nextItem.option && nextItem.option == 'page-spread-left') {
									moreSpineSide = true;
								}
								else if(nextItem.option && nextItem.option == 'page-spread-right') {
									moreSpineSide = false;
								}
								else if(nextItem.option && nextItem.option == 'page-spread-center') {
									moreSpineSide = false;
								}
								else {
									moreSpineSide = false;
								}
	
								Data.loadSpine(idx + 1, moreSpineSide, true)
									.then(function(data) {
										resolve(data);
									})
									.catch(function(err) {
										ePubEngineLogJS.L.log(err);
										reject(idx + 1);
									});
	
							}
							else {
								if(_pageMode) {
									Data.loadSpine(idx + 1, false, true)
										.then(function(data) {
											resolve(data);
										})
										.catch(function(err) {
											ePubEngineLogJS.L.log(err);
											reject(idx + 1);
										});
								}
								else {
									if(_contentSpread != 0) {
										Data.loadSpine(idx + 1, false, true)
											.then(function(data) {
												resolve(data);
											})
											.catch(function(err) {
												ePubEngineLogJS.L.log(err);
												reject(idx + 1);
											});
									}
									else {
										resolve(idx);
									}
								}
							}
						});
					})
					.catch(function(err) {
						console.log(err);
						reject(-1);
					});
				}
			});
		},
		/**
		 * 해당 데이터의 복호화된 데이터만 가져온다.
		 * @param {number} idx 파일 번호
		 * @returns null or string
		 */
		getSpineData: function(idx) {

			return new Promise( function(resolve, reject) {				
				var item = spineItems[idx];

				if(item == undefined) {
					console.log('invalid index : '+idx);
					return reject(null);
				}

				if(item.isLoaded) {
					var decrypted = Util.decryptContents(item.contents);

					// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
					// {CONTENT_IMG_API}&url={CONTENT_DOMAIN}
					decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
					decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

					resolve(decrypted);
				}
				else {
					// 해당 챕터를 로딩
					Util.getUrl(contentBaseUrl, item.path, authToken)
						.then(function(data) {
							item.contents = data.value;
							item.isLoaded = true;

							var decrypted = Util.decryptContents(data.value);

							// 임시 방편으로 직접 이미지를 로딩 하지만, 서비스시에는 이미지 Api를 사용한다.
							// {CONTENT_IMG_API}&url={CONTENT_DOMAIN}
							decrypted = Util.replaceAll(decrypted, "{CONTENT_IMG_API}&url={CONTENT_DOMAIN}", contentBaseUrl);
							decrypted = Util.replaceAll(decrypted, "{CONTENT_DOMAIN}", contentBaseUrl);

							resolve(decrypted);
						})
						.catch(function(err) {
							console.log(err);
							reject(null);
						});
				}
			});
		},
	};

	/**
	 * 난독화 관련
	 */
	var Obfucator = Loader.Obfucator = {
		/**
		 * 초기화
		 */
		init: function() {
			this.arrObfucator = new Array();
			this.arrEngChar = "abcdefghi0123456789jklmnopqrstuvwxyz";

			for(var i = 45032; i < 55204; i++) {
				this.arrObfucator.push(String.fromCharCode(i));
			}
		},
		/**
		 * 난독화 태그 생성
		 * @returns 난독화를 위한 태그 생성
		 */
		makeTag: function() {
			var result = '';
			tagLen = parseInt(Math.random() * 6) + 3;    
	
			for (var i = 0; i < tagLen; i++) {
				idx = parseInt(Math.random() * this.arrEngChar.length);
				while(parseInt(this.arrEngChar.charAt(idx)) >= 0 && i == 0) {
					idx = parseInt(Math.random() * this.arrEngChar.length);
				}
				result += this.arrEngChar.charAt(idx);
			}
	
			return result;
		},
		/**
		 * 난독화
		 * @param {string} data 서버로부터 받은 콘텐츠 데이터
		 * @returns 난독화 된 콘텐츠 데이터
		 */
		obfucator: function(data) {
			var strResult = "";

			arr = data.split('{CONTENT_OBFUCATE}');
	
			for (var i=0; i<arr.length; i++){
				isEnable = Math.floor(Math.random() * 10);

				if(isEnable > 5) {
					charNum = parseInt(Math.random() * 5) + 1;
					d = "";
		
					for(var j = 0; j < charNum; j++) {
						num = parseInt(Math.random() * this.arrObfucator.length);
						d += this.arrObfucator[num];
					}
					tag = this.makeTag();
		
					obs = "<"+tag+" class='bdb' style='display:none;'>"+d+"</"+tag+">";
		
					strResult += arr[i];
					strResult += obs;
					strResult += ' ';
				}
				else {
					strResult += arr[i];
					strResult += ' ';					
				}
			}
			return strResult;
		}
	};

	var Util = Loader.Util = {
		replaceAll: function(str, searchString, replaceString) {
			return str.split(searchString).join(replaceString);
		},
		Trim: function (str) {
            return str.replace(/^\s+|\s+$/g, "")
        },
		/**
		 * 콘텐츠 복호화
		 * @param {string} data 암호화 데이터
		 * @returns 복호화된 문자열
		 */
		decryptContents: function(data) {
			return ContentHelperJS.D.ps(data);
		},
		getUrl: function(baseUrl, url, token) {
			return new Promise( function(resolve, reject) {
				$.ajax( {
					_retrycnt: 0,
					_retrymax: 4,
					_retryinterval: 100,
					crossOrigin : true,
					type:'get',
					async: true,
					url: url.replace('{CONTENT_DOMAIN}', baseUrl),
					dataType: "jsonp",
					jsonpCallback:'DataCallback',
					headers: {
						'authorization': token
					},
					success: function(data) {
						if(data != undefined) {
							resolve(data);
						}
						else {
							this._retrycnt++;
							if((this._retrycnt <= this._retrymax)) {
								var rtnThis = this;

								return void setTimeout(function() {
									$.ajax(rtnThis);
								}, this._retryinterval);
							}
							//reject(data);
						}
					},
					error: function(request,status,error) {
						console.log('error Get json');
						console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);

						this._retrycnt++;
						if((this._retrycnt <= this._retrymax)) {
							var rtnThis = this;
							rtnThis.url = url.replace('{CONTENT_DOMAIN}', baseUrl);

							return void setTimeout(function() {
								$.ajax(rtnThis);
							}, this._retryinterval);
						}
						else {
							reject(error);
						}
					}
				});
			});
		},
		/**
		 * 
		 * @param {JQuery.EventStatic} e 이벤트
		 */
		copyEvent: function(e, frame) {
			var evt = new jQuery.Event(e.handleObj.type);

			evt.frame = frame;
			
			evt.altKey = e.altKey;
			evt.bubbles = e.bubbles;
			evt.button = e.button;
			evt.buttons = e.buttons;
			evt.cancelable = e.cancelable;
			evt.changedTouches = e.changedTouches;
			evt.char = e.char;
			evt.charCode = e.charCode;
			evt.clientX = e.clientX;// + frame.offset().left;
			evt.clientY = e.clientY;// + frame.offset().top;;
			evt.code = e.code;
			evt.ctrlKey = e.ctrlKey;
			evt.detail = e.detail;
			evt.data = e.data;
			evt.eventPhase = e.eventPhase;
			evt.key = e.key;
			evt.keyCode = e.keyCode;
			evt.metaKey = e.metaKey;
			evt.offsetX = e.offsetX;// + frame.offset().left;
			evt.offsetY = e.offsetY;// + frame.offset().top;
			evt.pageX = e.pageX;
			evt.pageY = e.pageY;
			evt.pointerId = e.pointerId;
			evt.pointerType = e.pointerType;
			evt.screenX = e.screenX;
			evt.screenY = e.screenY;
			evt.shiftKey = e.shiftKey;

			evt.deltaX = e.deltaX, 
			evt.deltaY = e.deltaX, 
			evt.deltaFactor = e.deltaX;

			evt.targetTouches = e.targetTouches;
			evt.target = e.target;
			evt.timeStamp = e.timeStamp;
			evt.view = e.view;
			evt.which = e.which;

			evt.currentTarget = e.currentTarget;
			evt.data = e.currentTarget;

			evt.changedTouches = e.changedTouches;
			evt.originalEvent = e.originalEvent;
			evt.isDuplicated = true;
			evt.hasClick = e.target.tagName === 'BUTTON'||e.target.getAttribute('role') === 'button'
			evt.src = e.target.src;
			evt.href = e.target.href;
			return evt;
		},
	};

	return Loader;

}(ContentHelperJS));