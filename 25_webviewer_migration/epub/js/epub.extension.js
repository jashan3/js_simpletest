
/**
 * prototype 모음
 */
if (!String.format) {
	String.prototype.format = function() {
		var formatted = this;
		for( var arg in arguments ) {
			formatted = formatted.replace("{" + arg + "}", arguments[arg]);
		}
		return formatted;
	}
}

if(!String.string) {
	String.prototype.string = function (len) { var s = '', i = 0; while (i++ < len) { s += this; } return s; };
}

if(!String.zf) {
	String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };
}

if(!Number.zf) {
	Number.prototype.zf = function (len) { return this.toString().zf(len); };
}

if(!Date.format) {
	Date.prototype.format = function(f) {
		if (!this.valueOf()) return " ";
	 
		var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
		var d = this;
		 
		return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
			switch ($1) {
				case "yyyy": return d.getFullYear();
				case "yy": return (d.getFullYear() % 1000).zf(2);
				case "MM": return (d.getMonth() + 1).zf(2);
				case "dd": return d.getDate().zf(2);
				case "E": return weekName[d.getDay()];
				case "HH": return d.getHours().zf(2);
				case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
				case "mm": return d.getMinutes().zf(2);
				case "ss": return d.getSeconds().zf(2);
				case "a/p": return d.getHours() < 12 ? "오전" : "오후";
				default: return $1;
			}
		});
	};
}

if(!Date.current){
    Date.prototype.current = function(){
        let month = this.getMonth() + 1;
        let day = this.getDate();
        let hour = this.getHours();
        let minute = this.getMinutes();
        let second = this.getSeconds();

        month = month >= 10 ? month : '0' + month;
        day = day >= 10 ? day : '0' + day;
        hour = hour >= 10 ? hour : '0' + hour;
        minute = minute >= 10 ? minute : '0' + minute;
        second = second >= 10 ? second : '0' + second;

        return this.getFullYear() + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    }
}



String.prototype.createElementFromHTML = function(){
    var div = document.createElement('div');
    div.innerHTML = this.trim();
    return div.firstChild; 
}
/**
 * string -> element
 * @param {*} htmlString 
 * @returns 
 */
function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();
    return div.firstChild; 
}

/**
 * 최대넓이 ,최대 높이에 따라 리사이즈.
 * @param {*} maxWidth 
 * @param {*} maxHeight 
 */
 Image.prototype.resized = function( maxWidth, maxHeight ){
    var srcWidth = this.width;
    var srcHeight = this.height;
    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
    var result = { width: srcWidth*ratio, height: srcHeight*ratio };
    this.width = result.width;
    // this.height = result.height;
}

/**
 * 찾으려는 문자열의 시작 index array
 * @param {*} searchKeyword 
 * @returns 
 */
String.prototype.indexOfAll = function(searchKeyword){

    var startingIndices = [];

    var indexOccurence = this.indexOf(searchKeyword, 0);

    while(indexOccurence >= 0) {
        startingIndices.push({'start':indexOccurence,'end':indexOccurence+searchKeyword.length});

        indexOccurence = this.indexOf(searchKeyword, indexOccurence + 1);
    }

    return startingIndices
}

if (!Date.prototype.formatedDate){
    Date.prototype.formatedDate = function(){
        // let list = [this.getFullYear() ,(this.getMonth() + 1) , this.getDate() , this.getHours() ,this.getMinutes() , this.getSeconds()]
        return this.getFullYear() + ". " + (this.getMonth() + 1) + ". " + this.getDate();
    }
}


if (!String.prototype.hex2rgba){
    String.prototype.hex2rgba = function(opacity){
        let hex = this
        return 'rgba(' + (hex = hex.replace('#', '')).match(new RegExp('(.{' + hex.length/3 + '})', 'g')).map(function(l) { return parseInt(hex.length%2 ? l+l : l, 16) }).concat(isFinite(opacity) ? opacity : 1).join(',') + ')';
    }
}


if( typeof Element.prototype.clearChildren === 'undefined' ) {
    Object.defineProperty(Element.prototype, 'clearChildren', {
      configurable: true,
      enumerable: false,
      value: function() {
        while(this.firstChild) this.removeChild(this.lastChild);
      }
    });
}


if (typeof String.prototype.findAllIndex !== 'function'){
    /**
     * string 중 searchKeyword에 해당하는 모든 index를 찾는다
     * @param {*} searchKeyword 찾을대상
     * @returns {Array} {s ,e} start , end
     */
    String.prototype.findAllIndex = function(searchKeyword){
        if (typeof searchKeyword === 'string' && searchKeyword.length > 0 ){
            var a=[],i=-1,l= searchKeyword.length;
            while((i=this.indexOf(searchKeyword,i+1)) >= 0) a.push({s:i,e:i+l});
            return a;
        } else {
            return []
        }
    }
}