/**
 * epub.type.js
 * last update 220316 20:00 sjhan
 */
const EpubViewMode = Object.freeze({
    DUAL:   Symbol(0),
    SINGLE:  Symbol(2),
    SCROLL: Symbol(3)
});
const SearchEngineType = Object.freeze({
    VIEWER:   Symbol(0),
    DAUM: Symbol(1),
	WIKI: Symbol(2),
});
const ClickType = Object.freeze({
    DEFAULT:   Symbol(0),
    PLUS:  Symbol(2),
    MINUS: Symbol(3),
});
const MenuType = Object.freeze({
    TOC:   Symbol(0),
    BOOKMARK:  Symbol(1),
    SEARCH: Symbol(2),
	SETTING: Symbol(3),
    VIEWSETTING: Symbol(4),
    FONT: Symbol(4),
});

const SearchEvent = Object.freeze({
    noResult:   Symbol(0),
    needMoreText:  Symbol(1),
	needKeyword:  Symbol(2),
    emptyResult: Symbol(3),
	hasResult: Symbol(4),
	clearInput: Symbol(5),
	initSearch: Symbol(6),
	dictionarySearch: Symbol(7),
});

const BookmarkSort = Object.freeze({
    RECENT:   Symbol('date'),
    PAGE:  Symbol('percent'),
});

const BookmarkEvent = Object.freeze({
    ADD:   Symbol(0),
    DELETE:  Symbol(1),
	DELETE_ALL:   Symbol(0),
    SORTING:  Symbol(1),
});

const HistoryType = Object.freeze({
    Neither:   Symbol(0),
    Next:  Symbol(1),
	Prev:   Symbol(2),
    Both:  Symbol(3),
});

/**
 * setting UI
 */
const SettingUIType = Object.freeze({
    Counter:        Symbol(0),
    Select:         Symbol(1),
	Dropdown:       Symbol(2),
    ImageSelect:    Symbol(3),
});

const AnnotationTabType = Object.freeze({
    All:            Symbol('all'),
    Bookmark:         Symbol('bookmark'),
	Highlight:       Symbol('highlight'),
    Memo:    Symbol('memo'),
});


/**
 * 
 */
const OrderSort = Object.freeze({
    RECENT:   Symbol('date'),
    PAGE:  Symbol('percent'),
});

const ColorSort = Object.freeze({
    All:   Symbol('all'),
    Lemon:  Symbol('lemon'),
    Palegreen:  Symbol('palegreen'),
    Lavendar:  Symbol('lavendar'),
    Skyblue:  Symbol('skyblue'),
    Palepink:  Symbol('palepink'),
    Underline:  Symbol('underline'),
});