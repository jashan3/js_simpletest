/*
Runa Web ePub engine core V1.0.0.1001
www.bookswage.com
(c) 2004-2021 by Crazy Eric. All rights reserved.
last update 220316 20:00 sjhan
*/


/**
 * ePub Engine Log
 */
 var ePubEngineLogJS = ePubEngineLogJS || (function(undefined) {
	var ePubEngineLogJs;

    // Native ePubEngineLogJs from window (Browser)
    if (typeof window !== 'undefined' && window.ePubEngineLogJs) {
        ePubEngineLogJs = window.ePubEngineLogJs;
    }

	if (typeof self !== 'undefined' && self.ePubEngineLogJs) {
        ePubEngineLogJs = self.ePubEngineLogJs;
    }

	// Native ePubEngineLogJs from worker
    if (typeof globalThis !== 'undefined' && globalThis.ePubEngineLogJs) {
        ePubEngineLogJs = globalThis.ePubEngineLogJs;
    }

    // Native (experimental IE 11) ePubEngineLogJs from window (Browser)
    if (!ePubEngineLogJs && typeof window !== 'undefined' && window.msePubEngineLogJs) {
        ePubEngineLogJs = window.msePubEngineLogJs;
    }

	var Log = {};

	var g_console_log = false;

	/**
	 * log for debug
	 */
	var D = Log.D = {
		log: function(msg) {
			if(g_console_log)
				console.debug('[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
		}
	};

	/**
	 * log for information
	 */
	var V = Log.V = {
		log: function(msg) {
			if(g_console_log)
				console.info('[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
		}
	};

	/**
	 * log for warning
	 */
	var W = Log.W = {
		log: function(msg) {
			if(g_console_log)
				console.warn('[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
		}
	};

	/**
	 * log for error
	 */
	var E = Log.E = {
		log: function(msg) {
			if(g_console_log)
				console.error('[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
		}
	};

	var L = Log.L = {
		log: function(msg) {
			if(g_console_log)
				console.log('[W:' + (document.width !== undefined ? document.width : window.innerWidth) + ',H:' + (document.height !== undefined ? document.height : window.innerHeight) + '] ' + msg);
		}
	};

	return Log;

}());