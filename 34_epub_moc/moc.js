document.addEventListener("DOMContentLoaded",function(e){
    const parent = document.getElementById("contents");
    const nextBtn = document.getElementById("next-btn");
    const prevBtn = document.getElementById("prev-btn");
    const pageInfos = [];

    function calc(){
        const pRect = parent.getBoundingClientRect();
        const _viewer_width = pRect.width;
        const list = Array.from(parent.children).filter(item=>item.tagName == "LI").map(item=>{
            return { 
                pos: item.clientHeight, 
                id: item.id,
                loaded: item.getAttribute("loaded"),
                perWidth: _viewer_width,
                pages: Math.ceil( item.clientHeight/_viewer_width )
            }
        })
        .flatMap(item=>{
            let arr = []
            for (let index = 0; index < item.pages; index++) {
                const newobj = {...item}
                newobj.page = _viewer_width*index
                arr.push(newobj);
            }
            return arr;
        })
        console.log(list)
    }
    calc();

    let currentPageIndex = 0;

    function move(direction = true){
        //ul tag
        const pRect = parent.getBoundingClientRect();
        const _viewer_width = pRect.width;
        const maxScroll = parent.scrollWidth;
        const curLeft = parent.scrollLeft;
        const extraPos = 0;
        const gotoPos = direction ? curLeft + ( _viewer_width + extraPos) : curLeft - (_viewer_width + extraPos);
        
        //first
        if (gotoPos < 0 ) return -1;
        //last
        else if (maxScroll <= gotoPos) return 1;

        
        parent.scrollLeft = Math.ceil( gotoPos );
        console.table({gotoPos,_viewer_width,curLeft});
        return 0;
    }

    prevBtn.addEventListener('click',move.bind(this,false));
    nextBtn.addEventListener('click',move.bind(this,true));
})