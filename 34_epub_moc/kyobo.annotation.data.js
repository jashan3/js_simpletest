
const KyoboSyncData  = {
    Prefix: {
        BOOKMARK: 'FLK_BOOKMARK_',
        HIGHLIGHT: 'FLK_ANNOTATION_',
        MEMO: 'FLK_ANNOTATION_',
        POSITION: 'FLK_READPOSITION_'
    },
    Type: {
        BOOKMARK: 'FLK_BOOKMARK_TYPE_TEXT',
        HIGHLIGHT: 'FLK_ANNOTATION_TYPE_HIGHLIGHT',
        MEMO: 'FLK_ANNOTATION_TYPE_MEMO',
        POSITION: 'FLK_DATA_TYPE_READPOSITION'
    }
}
Object.freeze(KyoboSyncData)

function BaseSyncDTO(){
    /**@type {Number}*/
    this.id = -1;
    /**@type {String}*/
    this.color = '';
    /**@type {String}*/
    this.text = '';
    /**@type {String}*/
    this.creation_time = '';
    /**@type {String}*/
    this.type = '';
    /**@type {String}*/
    this.file = '';
    /**@type {Number}*/
    this.percent = -1;
    /**@type {String}*/
    this.chapter_name = '';
    /**@type {String}*/
    this.model = 'WEB';
    /**@type {String}*/
    this.os_version = '';
}

function BookmarkSyncDTO(){
    this.FLK_BOOKMARK_ID = 1693443885435;
    this.FLK_BOOKMARK_COLOR = 1;
    this.FLK_BOOKMARK_TEXT = "";
    this.FLK_BOOKMARK_MODEL = "";
    this.FLK_BOOKMARK_CREATION_TIME = "";
    this.FLK_BOOKMARK_OS_VERSION = "";
    this.FLK_BOOKMARK_TEXT_INDEX = 0;
    
    this.FLK_BOOKMARK_PATH = "div#feelingk_bookcontent>p:eq(19)",
    this.FLK_BOOKMARK_TYPE = "FLK_BOOKMARK_TYPE_TEXT",
    this.FLK_BOOKMARK_FILE = "OEBPS/Text/Section0021.xhtml",
    this.FLK_BOOKMARK_PERCENT = 25,
    this.FLK_BOOKMARK_CHAPTER_NAME = ""
}

function AnnoSyncDTO(){
    this.start_element_path = '';
    this.start_char_offset = -1;
    this.start_child_index = -1;
    this.end_element_path = '';
    this.end_char_offset = -1;
    this.end_child_index = -1;
    this.page = -1;
    this.color = -1;
}

function PositionSyncDTO(){
    this.path = '';
    this.text_index = -1;
}