



window.addEventListener('DOMContentLoaded',function(){

    let isStart = false;
    let selectionMode = false;
    let tartgetMode = false;
    let annotationList = [];
    const smode = this.document.getElementById('selection-mode');
    const ele_atc = this.document.getElementById('annotation-targe-check');
    smode.onclick = (e) => {
        selectionMode = !selectionMode;
        smode.textContent = selectionMode;
    }
    ele_atc.onclick = (e) => {
        tartgetMode = !tartgetMode;
        ele_atc.textContent = tartgetMode;
    }
    
    this.window.addEventListener('mousedown',function(e){
        isStart = true;
    })
    this.window.addEventListener('mousemove',function(e){
        if (tartgetMode){
            let tg = null;
            for (let index = 0; index < annotationList.length; index++) {
                const item = annotationList[index];
                const rs = checkObj(e,item.rects);
                if (rs){
                    tg = {...item}
                    break;
                }
            }

            console.log('####',tg);
        }
        if (!isStart){
            return
        }
    })
    let iii = 0;
    this.window.addEventListener('mouseup',function(e){
        isStart = false;
        if (!selectionMode){
            return;
        }
        const rects = getRects();
        const sortRects = getSortRects(rects);
        console.log(rects);
        iii++
        annotationList.push({ id: iii, rects: sortRects })

        relead(annotationList);
    })
})

function checkObj(e, rects){
    if (e instanceof MouseEvent){
        let x = e.clientX;
        let y = e.clientY;
        const sortRects = getSortRects(rects);
        const first = sortRects.find(item=>item.first);
        const last = sortRects.find(item=>item.last);
        // let testresult = test(x,y,sortRects);
        // return testresult;
        if (x >= first.left && y >= first.top && x <= last.right && y <= last.bottom){
            return true;
        }
        if (y >= first.bottom && y<= last.top){
            return true;
        }
    }
    if (e instanceof PointerEvent){

    }
    return false;
}

function relead(list){
    const selectionwarpper = document.getElementById('selection-warpper');
    selectionwarpper.innerHTML = '';
    list.forEach((item)=>{
        drawSelection(item.id, item.rects);
    })
}

function drawSelection(idx,rects){
    const selectionwarpper = document.getElementById('selection-warpper');
    const dof = document.createElement('div');
    for (let index = 0; index < rects.length; index++) {
        const rect = rects[index];
        const div = buildSelection(rect);
        dof.appendChild(div);
    }
    dof.id = 'anno_idx_'+idx;
    selectionwarpper.appendChild(dof);
}

function buildSelection(rect){
    const div =  document.createElement('div')
    div.style.backgroundColor = 'red';
    if (rect.first){
        div.style.backgroundColor = 'green';
    }
    if (rect.last){
        div.style.backgroundColor = 'blue';
    }
    div.style.opacity = 0.3;
    div.style.position = 'absolute'
    div.style.top = rect.top + 'px';
    div.style.left =  rect.left + 'px';
    div.style.width =  rect.width + 'px';
    div.style.height =  rect.height + 'px';
    return div;
}

function getSelectionNode(){
  const selection = window.getSelection();
  if (selection.rangeCount > 0) {
    const range = selection.getRangeAt(0);
    const selectedRange = range.cloneRange();
    return selectedRange;
  }
  return null;
}

function getSortRects(rects){
    rects.sort((rectA, rectB) => {
      const bottomA = rectA.top + rectA.height;
      const bottomB = rectB.top + rectB.height;
      if (bottomA !== bottomB) {
        return bottomB - bottomA;
      } else {
        const rightA = rectA.left + rectA.width;
        const rightB = rectB.left + rectB.width;
        return rightB - rightA;
      }
    });
    rects[0].last = true;
    rects.sort((rectA, rectB) => {
      if (rectA.top !== rectB.top) {
        return rectA.top - rectB.top;
      } else {
        return rectA.left - rectB.left;
      }
    });
    rects[0].first = true;
    return rects;
}

function getRects() {
  const selectionNode = getSelectionNode();
  if (selectionNode){
    const range = document.createRange();
    range.setStart(selectionNode.startContainer,selectionNode.startOffset);
    range.setEnd(selectionNode.endContainer,selectionNode.endOffset);
    const rects = range.getClientRects();
    let list = []
    for (let index = 0; index < rects.length; index++) {
        const rect = rects[index];
        const exists = list.some(obj=> obj.top === rect.top &&obj.left === rect.left &&obj.width === rect.width &&obj.height === rect.height)
        if (!exists){
            list.push(rect);
        }
    }
    return list;
  }
  return null;
}


function getTextNodes(element) {
  const walker = document.createTreeWalker(element, NodeFilter.SHOW_TEXT, null, false);
  const textNodes = [];
  while (walker.nextNode()) {
    textNodes.push(walker.currentNode);
  }
  return textNodes;
}


function getTextNodeRects(textNodes) {
  const rects = [];
  textNodes.forEach(textNode => {
    const range = document.createRange();
    range.selectNodeContents(textNode);
    const textRects = range.getClientRects();
    for (let i = 0; i < textRects.length; i++) {
      rects.push(textRects[i]);
    }
  });
  return rects;
}


function test2(x, y,rects) {
  let insideAnyRect = rects.some(rect => x >= rect.left && x <= rect.right && y >= rect.top && y <= rect.bottom);

  if (insideAnyRect) return true;

  let filteredRects = rects.filter(rect => x >= rect.left && x <= rect.right);

  if (!filteredRects.length) return false;

  let minY = Math.min(...filteredRects.map(rect => rect.top));
  let maxY = Math.max(...filteredRects.map(rect => rect.bottom));

  if (y >= minY && y <= maxY) {
      let firstRowRects = rects.filter(rect => rect.y === rects[0].y);
      let firstRowMaxRight = Math.max(...firstRowRects.map(rect => rect.right));
      let firstRowBottom = firstRowRects[0].bottom;

      if (x < firstRowMaxRight && y < firstRowBottom) return true;
      
      let lastRowRects = rects.filter(rect => rect.y === rects[rects.length - 1].y);
      let lastRowMinLeft = Math.min(...lastRowRects.map(rect => rect.left));
      let lastRowTop = lastRowRects[0].top;

      if (x > lastRowMinLeft && y > lastRowTop) return true;
  }

  return false;
}

function test(x, y,rects) {
  let filteredRects = rects.filter(rect => x >= rect.left && x <= rect.right);
  if (!filteredRects.length) return false;

  for (let rect of filteredRects) {
      if (y >= rect.top && y <= rect.bottom) {
          return true;
      }
  }

  let minY = Math.min(filteredRects.map(rect => rect.top));
  let maxY = Math.max(filteredRects.map(rect => rect.bottom));

  return y >= minY && y <= maxY;
}